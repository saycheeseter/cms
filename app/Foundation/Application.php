<?php

namespace App\Foundation;

use Illuminate\Foundation\Application as LaravelApplication;

class Application extends LaravelApplication
{
    /**
     * Used during unit tests to clear out all the data structures
     * that may contain references.
     * Without this phpunit runs out of memory.
     */
    public function flush()
    {
        unset($this->bootingCallbacks);
        unset($this->bootedCallbacks);
        unset($this->finishCallbacks);
        unset($this->shutdownCallbacks);
        unset($this->middlewares);
        unset($this->serviceProviders);
        unset($this->loadedProviders);
        unset($this->deferredServices);
        unset($this->bindings);
        unset($this->instances);
        unset($this->reboundCallbacks);
        unset($this->resolved);
        unset($this->aliases);
    }
}