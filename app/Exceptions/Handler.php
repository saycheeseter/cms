<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\Core\Exceptions\MultipleLoginException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Modules\Core\Exceptions\LoginScheduleException;

use Lang;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
        LoginScheduleException::class,
        MultipleLoginException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof AuthorizationException) {
            return $this->unauthorized($request);
        }

        if ($exception instanceof LoginScheduleException) {
            return $this->invalidLoginSchedule($request);
        }

        if ($exception instanceof MultipleLoginException) {
            return $this->multipleLoginExceptionResponse($request);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }

    /**
     * Convert an unauthorization exception into an unauthorization response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function unauthorized($request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'isSuccessful' => false,
                'values' => [],
                'message' => 'You are not authorized to access this page.',
                'errors' => [
                    'action.failed' => Lang::get('core::error.unauthorized.action')
                ]
            ], Response::HTTP_FORBIDDEN);
        }

        return response()->view('core::errors.permission');
    }

    protected function invalidLoginSchedule($request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'isSuccessful' => false,
                'values' => [],
                'message' => 'Your login schedule is not now.',
                'errors' => [
                    'action.failed' => Lang::get('core::error.invalid.login.schedule')
                ]
            ], Response::HTTP_FORBIDDEN);
        }

        return redirect('/logout');
    }

    protected function multipleLoginExceptionResponse($request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'isSuccessful' => false,
                'values' => [],
                'message' => 'Request failed.',
                'errors' => [
                    'action.failed' => Lang::get('core::info.you.have.been.logged.out')
                ]
            ], Response::HTTP_FORBIDDEN);
        }

        return redirect('/logout?revoked=true');
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        if ($e->response) {
            return $e->response;
        }

        return $request->expectsJson()
            ? response()->json([
                'isSuccessful' => false,
                'values' => [],
                'message' => 'Request failed.',
                'errors' => $e->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY)
            : $this->invalid($request, $e);
    }
}
