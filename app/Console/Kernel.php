<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Modules\Core\Console\InitializeProject::class,
        \Modules\Core\Console\CleanAttachmentTempDirectory::class,
        \Modules\Core\Console\GeneratePHPUnitBat::class,
        \Modules\Core\Console\CreateBranchPermission::class,
        \Modules\Core\Console\ProjectDeploy::class,
        \Modules\Core\Console\ProjectEnvSetup::class,
        \Modules\Core\Console\DatabaseArchive::class,
        \Modules\Core\Console\UpdateCreateCustomReportPermissionSection::class,
        \Modules\Core\Console\ProjectClearTransactionals::class,
        \Modules\Core\Console\ProjectDumpUDFStructure::class,
        \Modules\Core\Console\GenerateSeederCommand::class,
        \Modules\Core\Console\ProjectCreateTerminal::class,
        \Modules\Core\Console\ProjectSetAdminLicenseKey::class,
        \Modules\Core\Console\ProjectCreateBegInvFromCSV::class,
        \Modules\Core\Console\ProjectExportLang::class,
        \Modules\Core\Console\ProjectImportLang::class,
        \Modules\Core\Console\ProjectRevertInvAdjust::class,
        \Modules\Core\Console\ProjectRecomputeStockCard::class,
        \Modules\Core\Console\ProjectZeroTransactionPosted::class,
        \Modules\Core\Console\ProjectRecomputeSerialLogs::class,
        \Modules\Core\Console\ProjectClearPosTransactions::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('project:clean-attachment-temp')->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        //require base_path('routes/console.php');
    }
}
