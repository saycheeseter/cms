<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\Member;
use Modules\Core\Entities\ModuleSeries;
use Tests\Functional\FunctionalTestCase;
use Lang;

class MemberControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_member_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['rates', 'permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_full_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->full_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_card_id_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->card_id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'card_id'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection(array_wrap($dummies[2]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_barcode_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->barcode,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'barcode'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        sleep(60);

        $added = Factory::times(2)->create($this->factory());

        $filters = [
            'filters' => [
                [
                    'value' => $added[0]->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $added->count(),
                            'count'        => $added->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection($added, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'members' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_empty_rate_head_id_full_name_barcode_status_birth_date_registration_date_or_expiration_date_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['rate_head_id'] = '';
        $data['full_name'] = '';
        $data['barcode'] = '';
        $data['status'] = '';
        $data['birth_date'] = '';
        $data['registration_date'] = '';
        $data['expiration_date'] = '';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'rate_head_id' => [
                    Lang::get('core::validation.rate.required')
                ],
                'full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'barcode' => [
                    Lang::get('core::validation.barcode.required')
                ],
                'status' => [
                    Lang::get('core::validation.status.required')
                ],
                'registration_date' => [
                    Lang::get('core::validation.registration.date.required')
                ],
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.required')
                ],
                'birth_date' => [
                    Lang::get('core::validation.birth.date.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_a_non_existing_rate_head_id_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $rates = $this->createMemberRates();

        $data['rate_head_id'] = $rates->last()->id + 1;

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'rate_head_id' => [
                    Lang::get('core::validation.member.rate.doesnt.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_existing_full_name_card_id_or_barcode_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['full_name'] = $existing->full_name;
        $data['card_id'] = $existing->card_id;
        $data['barcode'] = $existing->barcode;

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'full_name' => [
                    Lang::get('core::validation.full.name.unique')
                ],
                'card_id' => [
                    Lang::get('core::validation.card.id.unique')
                ],
                'barcode' => [
                    Lang::get('core::validation.barcode.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_a_non_numeric_barcode_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['barcode'] = 'Foo1827123';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'barcode' => [
                    Lang::get('core::validation.barcode.numeric'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_a_non_boolean_status_value_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['status'] = 'Foo291823';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'status' => [
                    Lang::get('core::validation.status.numeric'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_invalid_date_format_for_birth_date_registration_date_or_expiration_date_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['birth_date'] = date('Y-m-d H:i:s', strtotime($data['birth_date']));
        $data['registration_date'] = date('Y-m-d H:i:s', strtotime($data['registration_date']));
        $data['expiration_date'] = date('Y-m-d H:i:s', strtotime($data['expiration_date']));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'birth_date' => [
                    Lang::get('core::validation.birth.date.invalid.format')
                ],
                'registration_date' => [
                    Lang::get('core::validation.registration.date.format')
                ],
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_expiration_date_that_is_before_the_registration_date_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $temp = $data['registration_date'];
        $data['registration_date'] = $data['expiration_date'];
        $data['expiration_date'] = $temp;

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.invalid.range'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['email'] = 'Foo';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'email' => [
                    Lang::get('core::validation.email.invalid'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_member()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'member' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => 1]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => 1]
            )
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('member', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_member_with_an_empty_card_id_rate_head_id_full_name_barcode_status_birth_date_registration_date_or_expiration_date_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['card_id'] = '';
        $data['rate_head_id'] = '';
        $data['full_name'] = '';
        $data['barcode'] = '';
        $data['status'] = '';
        $data['birth_date'] = '';
        $data['registration_date'] = '';
        $data['expiration_date'] = '';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'rate_head_id' => [
                    Lang::get('core::validation.rate.required')
                ],
                'full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'barcode' => [
                    Lang::get('core::validation.barcode.required')
                ],
                'status' => [
                    Lang::get('core::validation.status.required')
                ],
                'registration_date' => [
                    Lang::get('core::validation.registration.date.required')
                ],
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.required')
                ],
                'birth_date' => [
                    Lang::get('core::validation.birth.date.required')
                ],
                'card_id' => [
                    Lang::get('core::validation.card.id.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_a_non_existing_rate_head_id_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['rate_head_id'] = $created->rate_head_id + 1;

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'rate_head_id' => [
                    Lang::get('core::validation.member.rate.doesnt.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_an_existing_full_name_card_id_or_barcode_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['full_name'] = $existing->full_name;
        $data['card_id'] = $existing->card_id;
        $data['barcode'] = $existing->barcode;

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'full_name' => [
                    Lang::get('core::validation.full.name.unique')
                ],
                'card_id' => [
                    Lang::get('core::validation.card.id.unique')
                ],
                'barcode' => [
                    Lang::get('core::validation.barcode.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_a_non_numeric_barcode_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['barcode'] = 'Foo1827123';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'barcode' => [
                    Lang::get('core::validation.barcode.numeric'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_a_non_boolean_status_value_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['status'] = 'Foo291823';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'status' => [
                    Lang::get('core::validation.status.numeric'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_an_invalid_date_format_for_birth_date_registration_date_or_expiration_date_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['birth_date'] = date('Y-m-d H:i:s', strtotime($data['birth_date']));
        $data['registration_date'] = date('Y-m-d H:i:s', strtotime($data['registration_date']));
        $data['expiration_date'] = date('Y-m-d H:i:s', strtotime($data['expiration_date']));

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'birth_date' => [
                    Lang::get('core::validation.birth.date.invalid.format')
                ],
                'registration_date' => [
                    Lang::get('core::validation.registration.date.format')
                ],
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_an_expiration_date_that_is_before_the_registration_date_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $temp = $data['registration_date'];
        $data['registration_date'] = $data['expiration_date'];
        $data['expiration_date'] = $temp;

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'expiration_date' => [
                    Lang::get('core::validation.expiration.date.invalid.range'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['email'] = 'Foo';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'email' => [
                    Lang::get('core::validation.email.invalid'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_member()
    {
        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $created->rate_head_id
        ]);

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'member' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => $created->id]
            )
        ];

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('member', $expected['database']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_member_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_member_should_return_the_details_of_the_specified_id()
    {
        $created = Factory::create($this->factory());

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([
                'member' => $this->item($created, 'infoTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_member_should_trigger_a_validation_and_return_an_error()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('member', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_member_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = Factory::create($this->factory());

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('member', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_member_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('member', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_member_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_member_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_member_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $rates->last()->id
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_member_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory(), [
            'rate_head_id' => $created->rate_head_id
        ]);

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_member_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_system_code_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'members' => $this->collection($dummies, 'basicTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.members.found' => Lang::get('core::error.no.members.found')
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_member_with_the_same_full_name_card_id_or_barcode_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $expected = [
            'response' => [
                'member' => array_merge(
                    $this->item($created, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function storing_a_member_with_an_empty_card_id_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_member_series()
    {
        $rates = $this->createMemberRates();

        $build = Factory::build($this->factory(), [
            'card_id' => '',
            'rate_head_id' => $rates->last()->id
        ]);

        $map = array_search(get_class($build), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->series_column)
            ->first();

        $data = $this->only($build->toArray());

        $expected = [
            'database' => array_merge(
                $data,
                ['card_id' => $series->series]
            )
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('member', $expected['database']);
    }

    private function uri()
    {
        return 'member';
    }

    private function factory()
    {
        return 'member';
    }

    private function only($data)
    {
        $data = array_only($data, [
            'rate_head_id',
            'card_id',
            'barcode',
            'full_name',
            'mobile',
            'telephone',
            'address',
            'email',
            'gender',
            'birth_date',
            'registration_date',
            'expiration_date',
            'status',
            'memo'
        ]);

        $data['birth_date'] = date('Y-m-d', strtotime($data['birth_date']));
        $data['registration_date'] = date('Y-m-d', strtotime($data['registration_date']));
        $data['expiration_date'] = date('Y-m-d', strtotime($data['expiration_date']));

        return $data;
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'full_name'     => $model->full_name,
            'member_type'   => $model->rate->name,
            'card_id'       => $model->card_id,
            'barcode'       => $model->barcode,
            'mobile'        => $model->mobile,
            'telephone'     => $model->telephone
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'rate_head_id'          => $model->rate_head_id,
            'card_id'               => $model->card_id,
            'barcode'               => $model->barcode,
            'full_name'             => $model->full_name,
            'mobile'                => $model->mobile,
            'telephone'             => $model->telephone,
            'address'               => $model->address,
            'email'                 => $model->email,
            'gender'                => $model->gender,
            'birth_date'            => $model->birth_date->toDateString(),
            'registration_date'     => $model->registration_date->toDateString(),
            'expiration_date'       => $model->expiration_date->toDateString(),
            'status'                => $model->status,
            'memo'                  => $model->memo
        ];
    }

    private function createMemberRates()
    {
        return Factory::times(3)->create('member_rate');
    }

    private function dummies($times = 4)
    {
        return Factory::times($times)->create($this->factory());
    }
}