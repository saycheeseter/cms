<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\ModuleSeries;
use Tests\Functional\FunctionalTestCase;
use Lang;
use DB;

class CategoryControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_category_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection($dummies, 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection(array_wrap($dummies[3]), 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection(array_wrap($dummies[1]), 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        sleep(60);

        $added = Factory::times(2)->create($this->factory());

        $filters = [
            'filters' => [
                [
                    'value' => $added[0]->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection($added, 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_parent_id_filter_should_return_the_children_of_the_specified_parent_category()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [],
            'parent_id' => $dummies[2]->id
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection($dummies[2]->children, 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_parent_id_and_code_name_or_updated_at_filter_should_return_the_children_of_the_specified_parent_category_matching_the_indicated_code_or_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->children[1]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name',
                ]
            ],
            'parent_id' => $dummies[2]->id
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection([$dummies[2]->children[1]], 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => $this->collection($dummies, 'treeTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_in_a_specific_children_of_a_category_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name',
                ]
            ],
            'parent_id' => $dummies[1]->id
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'categories' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_category_with_an_empty_name_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory(), [
            'name' => ''
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'name' => [
                    Lang::get('core::validation.name.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_category_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory(), [
            'code' => $created->code,
            'name' => $created->name
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'name' => [
                    Lang::get('core::validation.name.unique')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_category_with_a_non_existing_parent_id_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory(), [
            'parent_id' => $this->faker->numberBetween(100, 1000)
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'parent_id' => [
                    Lang::get('core::validation.parent.doesnt.exists')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_category_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_category()
    {
        $build = Factory::build($this->factory());

        $id = Category::all()->last()->id;

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'category' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => ($id + 1)]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => ($id + 1)]
            )
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('category', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_category_with_an_empty_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $created->code = '';
        $created->name = '';

        $data = $this->only($created->toArray());

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.required')
                ],
                'name' => [
                    Lang::get('core::validation.name.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_category_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $created = Factory::create($this->factory());

        $created->code = $existing->code;
        $created->name = $existing->name;

        $data = $this->only($created->toArray());

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'name' => [
                    Lang::get('core::validation.name.unique')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_category_with_a_non_existing_parent_id_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $created->parent_id = $this->faker->numberBetween(1000, 10000);

        $data = $this->only($created->toArray());

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'parent_id' => [
                    Lang::get('core::validation.parent.doesnt.exists')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_category_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_category()
    {
        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'category' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => $created->id]
            )
        ];

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('category', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_category_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('category', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_category_with_a_children_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $created->children()->save(Factory::create($this->factory()));

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'category.cant.delete' => Lang::get('core::validation.parent.cannot.delete')
            ]));

        $this->assertDatabaseHas('category', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_category_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = Factory::create($this->factory());

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('category', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_category_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('category', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_category_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function storing_a_new_category_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_category_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_category_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_system_code_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'categories' => $this->collection($dummies, 'basicTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.categories.found' => Lang::get('core::error.no.categories.found')
            ]));
    }

    /**
     * @test
     */
    public function storing_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_category_series()
    {
        $build = Factory::build($this->factory(), [
            'code' => ''
        ]);

        $map = array_search(get_class($build), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->series_column)
            ->first();

        $data = $this->only($build->toArray());

        $expected = [
            'database' => array_merge(
                $data,
                ['code' => $series->series]
            )
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('category', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_an_existing_category_with_the_same_code_or_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $expected = [
            'response' => [
                'category' => array_merge(
                    $this->item($created, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    private function uri()
    {
        return 'category';
    }

    private function factory()
    {
        return 'category';
    }

    private function only($data)
    {
        return array_only($data, ['code', 'name', 'parent_id']);
    }

    protected function treeTransformer($model)
    {
        $data = [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id,
            'children'      => []
        ];

        foreach ($model->children as $key => $child) {
            $data['children'][] = $this->treeTransformer($child);
        }

        return $data;
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id
        ];
    }

    private function dummies($times = 4, $children = 2)
    {
        // Need minus one since there's a seeded data for category
        $data = Factory::times(($times - 1))->create($this->factory())
            ->each(function($category) use ($children) {
                $collection = [];

                for ($i = 0; $i < $children; $i++) {
                    $collection[] = Factory::build($this->factory());
                }

                if (count($collection) === 0) {
                    return;
                }

                $category->children()->saveMany($collection);
            });

        $data->prepend(Category::first());

        return $data;
    }
}