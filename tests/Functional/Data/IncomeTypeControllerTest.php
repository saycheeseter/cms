<?php

namespace Tests\Functional\Data;

class IncomeTypeControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'income-type';
    }

    protected function factory()
    {
        return 'income_type';
    }
}