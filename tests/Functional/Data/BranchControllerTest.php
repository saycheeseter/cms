<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Entities\BranchPrice;
use Modules\Core\Entities\BranchSetting;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\Counter;
use Modules\Core\Entities\Damage;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Entities\Payment;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\ProductBranchDetail;
use Modules\Core\Entities\ProductBranchInfo;
use Modules\Core\Entities\ProductBranchSummary;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Tests\Functional\FunctionalTestCase;
use Lang;

class BranchControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_branch_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_name_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection(array_wrap($dummies[2]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_business_name_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->business_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'business_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        sleep(60);

        $added = $this->create();

        $filters = [
            'filters' => [
                [
                    'value' => $added->info->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection(array_wrap($added->info), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'branches' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_branch_with_an_empty_name_or_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.name', '');
        array_set($data, 'details.0.name', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.required'),
                ],
                'details.0.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.required'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_an_empty_details_array_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', []);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.details.min.one')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.name', $existing->info->name);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'info.name' => [
                    Lang::get('core::validation.name.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_a_duplicate_detail_code_or_detail_name_in_details_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details.0.name', array_get($data, 'details.1.name'));
        array_set($data, 'details.0.code', array_get($data, 'details.1.code'));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.code' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.code.array.unique'), 1)
                ],
                'details.1.code' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.code.array.unique'), 2)
                ],
                'details.0.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.array.unique'), 1)
                ],
                'details.1.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.array.unique'), 2)
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_non_array_details_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_branch()
    {
        $build = $this->build();

        $data = $this->format($build);

        $ids = [
            'branch' => 2,
            'branch_detail' => 2
        ];

        $expected = [
            'response' => [
                'branch' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $ids['branch']]
                )
            ],
            'database' => [
                'branch' => array_merge(
                    $data['info'],
                    ['id' => $ids['branch']]
                ),
                'branch_detail' => array_map(function($item) use (&$ids) {
                    $format = array_merge($item, [
                        'id' => ($ids['branch_detail'])
                    ]);

                    $ids['branch_detail']++;

                    return $format;
                }, $data['details'])
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('branch', $expected['database']['branch']);
        $this->assertDatabaseHave('branch_detail', $expected['database']['branch_detail']);
    }

    /**
     * @test
     */
    public function updating_a_branch_with_an_empty_code_name_detail_code_or_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.code', '');
        array_set($data, 'info.name', '');
        array_set($data, 'details.0.code', '');
        array_set($data, 'details.0.name', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.required'),
                ],
                'info.name' => [
                    Lang::get('core::validation.name.required'),
                ],
                'details.0.code' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.code.required'), 1)
                ],
                'details.0.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.required'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_branch_with_an_empty_details_array_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'details', []);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.details.min.one')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_branch_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.name', $existing->info->name);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'info.name' => [
                    Lang::get('core::validation.name.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_branch_with_a_duplicate_detail_code_or_detail_name_in_details_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'details.0.name', array_get($data, 'details.1.name'));
        array_set($data, 'details.0.code', array_get($data, 'details.1.code'));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.code' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.code.array.unique'), 1)
                ],
                'details.1.code' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.code.array.unique'), 2)
                ],
                'details.0.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.array.unique'), 1)
                ],
                'details.1.name' => [
                    $this->validationMessageForDetails(Lang::get('core::validation.name.array.unique'), 2)
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_branch_with_non_array_details_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'details', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_branch_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_branch()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $expected = [
            'response' => [
                'branch' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $created->info->id]
                )
            ],
            'database' => [
                'branch' => array_merge(
                    $data['info'],
                    ['id' => $created->info->id]
                ),
                'branch_detail' => $data['details']
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('branch', $expected['database']['branch']);
        $this->assertDatabaseHave('branch_detail', $expected['database']['branch_detail']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_branch_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_branch_should_return_the_details_of_the_specified_id()
    {
        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([
                'branch'  => $this->item($created->info, 'infoTransformer'),
                'details' => $this->collection($created->details, 'detailsTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_branch_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('branch', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_branch_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = $this->create();

        $this->deleteJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('branch', ['id' => $created->info->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_branch_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('branch', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_branch_detail_of_a_non_existing_branch_should_return_an_error_message()
    {
        $this->getJson($this->uri() . '/5/details')
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_branch_detail_of_an_existing_branch_should_return_the_corresponding_label_and_id_of_the_branch_details()
    {
        $created = $this->create();

        $this->getJson($this->uri() . '/'.$created->info->id.'/details')
            ->assertExactJson($this->successfulResponse([
                'details' => $this->collection($created->details, 'detailChosenTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_branch_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_branch_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_branch_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = $this->build();

        $data = $this->format($build);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_branch_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_branch_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->info->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_branch_with_the_same_code_or_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                ['branch' => $this->item($created->info,'basicTransformer')],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function updating_a_branch_and_removing_an_data_in_details_should_soft_delete_the_row_in_branch_detail_table()
    {
        $created = $this->create();

        $build = $this->build();

        $removed = $created->details->pop();

        $build->details->pop();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertSoftDeleted('branch_detail', ['id' => $removed->id]);
    }

    /**
     * @test
     */
    public function updating_a_branch_and_adding_a_new_data_in_details_should_insert_a_new_row_in_branch_detail_table()
    {
        $created = $this->create();

        $added = Factory::build($this->factory('details'));

        $created->details->push($added);

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertDatabaseHas('branch_detail', array_merge(
            $data['details'][2],
            ['id' => ($data['details'][1]['id'] + 1)]
        ));
    }

    /**
     * @test
     */
    public function storing_a_branch_with_an_empty_code_or_branch_detail_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_branch_series()
    {
        $build = $this->build([
            'info' => [
                'code' => ''
            ],
            'details' => [
                'code' => ''
            ]
        ]);

        $map = [
            'info' => $this->getMorphMapKey(get_class($build->info)),
            'details' => $this->getMorphMapKey(get_class($build->details->first()))
        ];

        $ids = [
            'info' => 2,
            'details' => 2
        ];

        $series = [
            'info' => ModuleSeries::where('type', $map['info'])
                ->where('column', $build->info->series_column)
                ->first()
                ->series,
            'details' => ModuleSeries::where('type', $map['details'])
                ->where('column', $build->details->first()->series_column)
                ->first()
                ->series,
        ];

        $data = $this->format($build);

        $expected = [
            'database' => [
                'branch' => array_merge(
                    $data['info'],
                    [
                        'id' => $ids['info'],
                        'code' => $series['info']
                    ]
                ),
                'branch_detail' => array_map(function($item) use (&$series, &$ids) {
                    $format = array_merge(
                        $item,
                        [
                            'id' => $ids['details'],
                            'code' => $series['details']
                        ]
                    );

                    $ids['details']++;
                    $series['details']++;

                    return $format;
                }, $data['details'])
            ]
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('branch', $expected['database']['branch']);
        $this->assertDatabaseHave('branch_detail', $expected['database']['branch_detail']);
    }

    /**
     * @test
     */
    public function storing_a_new_branch_should_also_generate_a_new_summary_data_for_the_created_branch_and_location()
    {
        $this->generateSummaries();

        $build = $this->build();

        $data = $this->format($build);

        $ids = [
            'info' => 2,
            'details' => [2, 3]
        ];

        $branchSettings = BranchSetting::where('branch_id', 1)->get();
        $branchPrices = BranchPrice::where('branch_id', 1)->get();
        $branchInfo = ProductBranchInfo::where('branch_id', 1)->get();
        $branchModuleSeries = $this->getBranchModuleSeries();
        $products = Product::get();

        $expected = [
            'branch_settings' => [],
            'product_branch_prices' => [],
            'product_branch_info' => [],
            'branch_module_series'  => [],
            'product_branch_summary' => [],
            'product_branch_detail' => []
        ];

        foreach ($branchSettings as $setting) {
            $expected['branch_settings'][] = [
                'branch_id' => $ids['info'],
                'key'       => $setting->key,
                'value'     => $setting->value,
                'type'      => $setting->type,
                'comments'  => $setting->comments
            ];
        }

        foreach ($branchPrices as $branchPrice) {
            $expected['product_branch_prices'][] = [
                'branch_id'       => $ids['info'],
                'product_id'      => $branchPrice->product_id,
                'purchase_price'  => (string) $branchPrice->purchase_price,
                'selling_price'   => (string) $branchPrice->selling_price,
                'wholesale_price' => (string) $branchPrice->wholesale_price,
                'price_a'         => (string) $branchPrice->price_a,
                'price_b'         => (string) $branchPrice->price_b,
                'price_c'         => (string) $branchPrice->price_c,
                'price_d'         => (string) $branchPrice->price_d,
                'price_e'         => (string) $branchPrice->price_e,
                'editable'        => (string) $branchPrice->editable,
                'copy_from'       => 1
            ];
        }

        foreach ($branchInfo as $info) {
            $expected['product_branch_info'][] = [
                'branch_id'  => $ids['info'],
                'product_id' => $info->product_id,
                'status'     => $info->status
            ];
        }

        foreach ($branchModuleSeries as $series) {
            $expected['branch_module_series'][] = [
                'branch_id' => $ids['info'],
                'type'      => $series,
                'column'    => 'sheet_number',
                'series'    => sprintf("1%s0000001", str_pad($ids['info'],3, '0', STR_PAD_LEFT))
            ];
        }

        foreach ($products as $product) {
            foreach ($ids['details'] as $detail) {
                $expected['product_branch_summary'][] = [
                    'branch_id'         => $ids['info'],
                    'branch_detail_id'  => $detail,
                    'product_id'        => $product->id,
                    'qty'               => 0,
                    'reserved_qty'      => 0,
                    'requested_qty'     => 0,
                    'current_cost'      => 0,
                ];

                $expected['product_branch_detail'][] = [
                    'product_id'       => $product->id,
                    'branch_detail_id' => $detail,
                    'min'              => 0,
                    'max'              => 0
                ];
            }
        }

        $this->postJson($this->uri(), $data);
        $this->assertDatabaseHave('branch_settings', $expected['branch_settings']);
        $this->assertDatabaseHave('branch_price', $expected['product_branch_prices']);
        $this->assertDatabaseHave('product_branch_info', $expected['product_branch_info']);
        $this->assertDatabaseHave('branch_module_series', $expected['branch_module_series']);
        $this->assertDatabaseHave('product_branch_summary', $expected['product_branch_summary']);
        $this->assertDatabaseHave('product_branch_detail', $expected['product_branch_detail']);
    }

    /**
     * @test
     */
    public function deleting_an_existing_branch_should_also_delete_the_corresponding_summaries_in_database()
    {
        $this->generateSummaries();

        $created = $this->create();

        $branchSettings = BranchSetting::where('branch_id', $created->info->id)->get();
        $branchPrices = BranchPrice::where('branch_id', $created->info->id)->get();
        $branchInfo = ProductBranchInfo::where('branch_id', $created->info->id)->get();
        $branchModuleSeries = BranchModuleSeries::where('branch_id', $created->info->id)->get();
        $productBranchSummary = ProductBranchSummary::whereIn('branch_detail_id', $created->details->pluck('id')->toArray())->get();
        $productBranchDetail = ProductBranchDetail::whereIn('branch_detail_id', $created->details->pluck('id')->toArray())->get();

        $expected = [
            'branch_settings' => $branchSettings->toArray(),
            'product_branch_prices' => $branchPrices->toArray(),
            'product_branch_info' => $branchInfo->toArray(),
            'branch_module_series'  => $branchModuleSeries->toArray(),
            'product_branch_summary' => $productBranchSummary->toArray(),
            'product_branch_detail' => $productBranchDetail->toArray()
        ];

        $this->delete($this->uri().'/'.$created->info->id);
        $this->assertDatabaseHaveMissing('branch_settings', $expected['branch_settings']);
        $this->assertDatabaseHaveMissing('branch_price', $expected['product_branch_prices']);
        $this->assertDatabaseHaveMissing('product_branch_info', $expected['product_branch_info']);
        $this->assertDatabaseHaveMissing('branch_module_series', $expected['branch_module_series']);
        $this->assertDatabaseHaveMissing('product_branch_summary', $expected['product_branch_summary']);
        $this->assertDatabaseHaveMissing('product_branch_detail', $expected['product_branch_detail']);
    }

    private function uri()
    {
        return 'branch';
    }

    private function factory($key)
    {
        $factories = [
            'info' => 'branch',
            'details' => 'branch_detail'
        ];

        return $factories[$key];
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'                 => $model->id,
            'code'               => $model->code,
            'name'               => $model->name,
            'address'            => $model->address ?? '',
            'contact'            => $model->contact ?? '',
            'business_name'      => $model->business_name ?? ''
        ];
    }

    protected function detailsTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }

    protected function detailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }

    private function format($model)
    {
        $info = $model->info->toArray();

        return [
            'info' => [
                'code'          => $info['code'],
                'name'          => $info['name'],
                'contact'       => $info['contact'],
                'address'       => $info['address'],
                'business_name' => $info['business_name']
            ],
            'details' => array_map(function($item) {
                return array_only($item, [
                    'id',
                    'code',
                    'name',
                    'contact',
                    'address',
                    'business_name',
                ]);
            }, $model->details->toArray())
        ];
    }

    private function build($data = [])
    {
        $branch = Factory::build($this->factory('info'), $data['info'] ?? []);

        $details = [];

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::build($this->factory('details'), $data['details'] ?? []);
        }

        return (object) [
            'info' => $branch,
            'details' => collect($details)
        ];
    }

    private function create($data = [])
    {
        $branch = Factory::create($this->factory('info'), $data['info'] ?? []);

        $details = [];

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::create($this->factory('details'), array_merge(
                ['branch_id' => $branch->id],
                $data['details'] ?? []
            ));
        }

        return (object) [
            'info' => $branch,
            'details' => collect($details)
        ];
    }

    private function dummies($times = 4)
    {
        $collection = [];

        for ($i = 1; $i <= ($times - 1); $i++) {
            $collection[] = $this->create();
        }

        $collection = array_prepend($collection, $this->initial());

        return collect($collection);
    }

    private function initial()
    {
        $branch = Branch::with('details')->first();

        return (object) [
            'info' => $branch,
            'details' => $branch->details
        ];
    }

    private function validationMessageForDetails($message, $row = null)
    {
        return sprintf('%s[%s]. %s',
            Lang::get('core::error.row'),
            $row,
            $message
        );
    }

    private function generateSummaries()
    {
        $product = Factory::create('product');

        Factory::create('branch_price', [
            'branch_id' => 1,
            'product_id' => $product->id
        ]);

        Factory::create('product_branch_info', [
            'branch_id' => 1,
            'product_id' => $product->id
        ]);

        Factory::create('product_branch_detail', [
            'branch_detail_id' => 1,
            'product_id' => $product->id
        ]);

        Factory::create('product_branch_summary', [
            'branch_id' => 1,
            'branch_detail_id' => 1,
            'product_id' => $product->id
        ]);
    }

    private function getBranchModuleSeries()
    {
        return [
            $this->getMorphMapKey(Purchase::class),
            $this->getMorphMapKey(PurchaseInbound::class),
            $this->getMorphMapKey(PurchaseReturn::class),
            $this->getMorphMapKey(PurchaseReturnOutbound::class),
            $this->getMorphMapKey(Damage::class),
            $this->getMorphMapKey(DamageOutbound::class),
            $this->getMorphMapKey(Sales::class),
            $this->getMorphMapKey(SalesOutbound::class),
            $this->getMorphMapKey(SalesReturn::class),
            $this->getMorphMapKey(SalesReturnInbound::class),
            $this->getMorphMapKey(StockRequest::class),
            $this->getMorphMapKey(StockDelivery::class),
            $this->getMorphMapKey(StockDeliveryOutbound::class),
            $this->getMorphMapKey(StockDeliveryInbound::class),
            $this->getMorphMapKey(StockReturn::class),
            $this->getMorphMapKey(StockReturnOutbound::class),
            $this->getMorphMapKey(StockReturnInbound::class),
            $this->getMorphMapKey(InventoryAdjust::class),
            $this->getMorphMapKey(ProductConversion::class),
            $this->getMorphMapKey(Payment::class),
            $this->getMorphMapKey(Collection::class),
            $this->getMorphMapKey(Counter::class),
        ];
    }

    private function getMorphMapKey($class)
    {
        return array_search($class, Relation::morphMap());
    }
}