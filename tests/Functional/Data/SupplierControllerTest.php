<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Entities\Supplier;
use Modules\Core\Enums\FlowType;
use Tests\Functional\FunctionalTestCase;
use Lang;

class SupplierControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_supplier_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_full_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->full_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_chinese_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->chinese_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'chinese_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection(array_wrap($dummies[2]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_memo_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->memo,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'memo'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        sleep(60);

        $added = Factory::times(2)->create($this->factory());

        $filters = [
            'filters' => [
                [
                    'value' => $added[0]->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $added->count(),
                            'count'        => $added->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection($added, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'suppliers' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_an_empty_full_name_status_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['status'] = '';
        $data['full_name'] = '';
        $data['term'] = '';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'status' => [
                    Lang::get('core::validation.status.required')
                ],
                'term' => [
                    Lang::get('core::validation.term.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_an_existing_full_name_or_code_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['code'] = $existing->code;
        $data['full_name'] = $existing->full_name;

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'full_name' => [
                    Lang::get('core::validation.full.name.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_a_non_numeric_status_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['status'] = 'Foo';
        $data['term'] = 'Bar';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'status' => [
                    Lang::get('core::validation.status.numeric')
                ],
                'term' => [
                    Lang::get('core::validation.term.numeric')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['email'] = 'Foo';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'email' => [
                    Lang::get('core::validation.email.invalid')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_supplier()
    {
        $build = Factory::build($this->factory());

        $id = Supplier::all()->last()->id;

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'supplier' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => ($id + 1)]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => ($id + 1)]
            )
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('supplier', $expected['database']);
    }

    /**
     * @test
     */
    public function storing_a_supplier_with_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_supplier_series()
    {
        $build = Factory::build($this->factory(), [
            'code' => ''
        ]);

        $map = array_search(get_class($build), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->series_column)
            ->first();

        $data = $this->only($build->toArray());

        $expected = [
            'database' => array_merge(
                $data,
                ['code' => $series->series]
            )
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('supplier', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_supplier_with_an_empty_code_full_name_status_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['term'] = '';
        $data['code'] = '';
        $data['full_name'] = '';
        $data['status'] = '';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.required')
                ],
                'full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'status' => [
                    Lang::get('core::validation.status.required')
                ],
                'term' => [
                    Lang::get('core::validation.term.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_supplier_with_an_existing_code_or_full_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['code'] = $existing->code;
        $data['full_name'] = $existing->full_name;

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'full_name' => [
                    Lang::get('core::validation.full.name.unique')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_supplier_with_a_non_numeric_status_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['status'] = 'Foo';
        $data['term'] = 'Bar';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'status' => [
                    Lang::get('core::validation.status.numeric')
                ],
                'term' => [
                    Lang::get('core::validation.term.numeric')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_supplier_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['email'] = 'Foo';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'email' => [
                    Lang::get('core::validation.email.invalid')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_supplier_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_supplier()
    {
        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'supplier' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => $created->id]
            )
        ];

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('supplier', $expected['database']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_supplier_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_supplier_should_return_the_details_of_the_specified_id()
    {
        $created = Factory::create($this->factory());

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([
                'supplier' => $this->item($created, 'infoTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_supplier_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('supplier', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_supplier_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = Factory::create($this->factory());

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('supplier', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_supplier_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('supplier', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_supplier_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_supplier_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_supplier_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_supplier_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_supplier_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_system_code_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'suppliers' => $this->collection($dummies, 'basicTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.suppliers.found' => Lang::get('core::error.no.suppliers.found')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_balance_info_of_a_non_existing_supplier_should_return_an_error_message()
    {
        $this->getJson($this->uri().'/'.rand(100, 500).'/balance')
            ->assertExactJson($this->errorResponse([
                'supplier.not.found' => Lang::get('core::error.supplier.not.found')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_balance_info_of_an_existing_supplier_should_display_the_computed_amount_based_on_the_summation_of_supplier_balance_flow()
    {
        $created = Factory::create($this->factory());

        $balance1 = Factory::create('supplier_balance_flow', [
            'supplier_id' => $created->id,
            'flow_type' => FlowType::IN
        ]);

        $balance2 = Factory::create('supplier_balance_flow', [
            'supplier_id' => $created->id,
            'flow_type' => FlowType::IN
        ]);

        $balance3 = Factory::create('supplier_balance_flow', [
            'supplier_id' => $created->id,
            'flow_type' => FlowType::OUT
        ]);

        $sum = Decimal::create(0, setting('monetary.precision'));

        $sum = $balance1->amount
            ->add($balance2->amount)
            ->sub($balance3->amount);

        $this->getJson($this->uri().'/'.$created->id.'/balance')
            ->assertExactJson($this->successfulResponse([
                'balance' => [
                    'amount' => number_format((string) $sum, setting('monetary.precision'))
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_supplier_with_the_same_code_or_full_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $expected = [
            'response' => [
                'supplier' => array_merge(
                    $this->item($created, 'basicTransformer'),
                    ['id' => $created->id]
                )
            ],
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    private function uri()
    {
        return 'supplier';
    }

    private function factory()
    {
        return 'supplier';
    }

    private function only($data)
    {
        return array_only($data, [
            'code',
            'full_name',
            'chinese_name',
            'owner_name',
            'contact',
            'address',
            'landline',
            'telephone',
            'fax',
            'mobile',
            'email',
            'website',
            'term',
            'status',
            'contact_person',
            'memo'
        ]);
    }

    protected function basicTransformer($model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'full_name'    => $model->full_name,
            'chinese_name' => $model->chinese_name,
            'contact'      => $model->contact
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'contact'        => $model->contact,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'telephone'      => $model->telephone,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'term'           => $model->term,
            'status'         => $model->status,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'owner_name'     => $model->owner_name
        ];
    }

    private function dummies($times = 4)
    {
        $data = Factory::times(($times - 1))->create($this->factory());

        $data->prepend(Supplier::first());

        return $data;
    }
}