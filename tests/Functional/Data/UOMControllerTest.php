<?php

namespace Tests\Functional\Data;

use Modules\Core\Entities\UOM;

class UOMControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'uom';
    }

    protected function factory()
    {
        return 'uom';
    }

    protected function initial()
    {
        return UOM::first();
    }
}