<?php

namespace Tests\Functional\Data;

use Modules\Core\Entities\Brand;

class BrandControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'brand';
    }

    protected function factory()
    {
        return 'brand';
    }

    protected function initial()
    {
        return Brand::first();
    }
}