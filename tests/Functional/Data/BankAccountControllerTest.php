<?php

namespace Tests\Functional\Data;

use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Tests\Functional\FunctionalTestCase;
use Lang;

class BankAccountControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_bank_account_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['banks', 'permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'accounts' => $this->collection($dummies, 'transformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_an_account_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->account_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'account_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'accounts' => $this->collection(array_wrap($dummies[3]), 'transformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_an_account_number_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->account_number,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'account_number'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'accounts' => $this->collection(array_wrap($dummies[1]), 'transformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'accounts' => $this->collection($dummies, 'transformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'account_name',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'accounts' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_bank_account_with_an_empty_bank_id_account_name_or_account_number_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['bank_id'] = '';
        $data['account_name'] = '';
        $data['account_number'] = '';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'bank_id' => [
                    Lang::get('core::validation.bank.required'),
                ],
                'account_name' => [
                    Lang::get('core::validation.account.name.required'),
                ],
                'account_number' => [
                    Lang::get('core::validation.account.number.required'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_bank_account_with_a_non_existing_bank_should_trigger_a_validation_and_return_an_error()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $data['bank_id'] = $this->faker->numberBetween(100, 1000);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'bank_id' => [
                    Lang::get('core::validation.bank.must.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_bank_account_with_an_existing_account_number_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $banks = $this->createBanks();

        $build = Factory::build($this->factory(), [
            'bank_id' => $banks->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['account_number'] = $existing->account_number;

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'account_number' => [
                    Lang::get('core::validation.account.number.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_bank_account_with_a_non_numeric_opening_balance_should_trigger_a_validation_and_return_an_error()
    {
        $banks = $this->createBanks();

        $build = Factory::build($this->factory(), [
            'bank_id' => $banks->last()->id
        ]);

        $data = $this->only($build->toArray());

        $data['opening_balance'] = 'Foo';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'opening_balance' => [
                    Lang::get('core::validation.invalid.opening.balance'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_bank_account_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_bank_account()
    {
        $banks = $this->createBanks();

        $build = Factory::build($this->factory(), [
            'bank_id' => $banks->last()->id
        ]);

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'account' => array_merge(
                    $this->item($build, 'transformer'),
                    ['id' => 1]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => 1]
            )
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('bank_account', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_bank_account_with_an_empty_bank_id_account_name_or_account_number_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['bank_id'] = '';
        $data['account_name'] = '';
        $data['account_number'] = '';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'bank_id' => [
                    Lang::get('core::validation.bank.required'),
                ],
                'account_name' => [
                    Lang::get('core::validation.account.name.required'),
                ],
                'account_number' => [
                    Lang::get('core::validation.account.number.required'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_bank_account_with_a_non_existing_bank_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['bank_id'] = $this->faker->numberBetween(100, 1000);

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'bank_id' => [
                    Lang::get('core::validation.bank.must.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_bank_account_with_an_existing_account_number_should_trigger_a_validation_and_return_an_error()
    {
        $existing = Factory::create($this->factory());

        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['account_number'] = $existing->account_number;

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'account_number' => [
                    Lang::get('core::validation.account.number.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_bank_account_with_a_non_numeric_opening_balance_should_trigger_a_validation_and_return_an_error()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['opening_balance'] = 'Foo';

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->errorResponse([
                'opening_balance' => [
                    Lang::get('core::validation.invalid.opening.balance'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_bank_account_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_bank_account()
    {
        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory(), [
            'bank_id' => $created->bank_id
        ]);

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'account' => array_merge(
                    $this->item($build, 'transformer'),
                    ['id' => $created->id]
                )
            ],
            'database' => array_merge(
                $data,
                ['id' => $created->id]
            )
        ];

        $this->patchJson($this->uri().'/'.$created->id, $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('bank_account', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_bank_account_should_trigger_a_validation_and_return_an_error()
    {
        $banks = $this->createBanks();

        $build = Factory::build($this->factory(), [
            'bank_id' => $banks->last()->id
        ]);

        $data = $this->only($build->toArray());

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('bank_account', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_bank_account_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = Factory::create($this->factory());

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('bank_account', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_bank_account_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('bank_account', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_bank_account_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function storing_a_new_bank_account_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $banks = $this->createBanks();

        $build = Factory::build($this->factory(), [
            'bank_id' => $banks->last()->id
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_bank_account_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory(), [
            'bank_id' => $created->bank_id
        ]);

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_bank_account_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_bank_account_with_the_same_account_number_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $expected = [
            'response' => [
                'account' => array_merge(
                    $this->item($created, 'transformer'),
                    ['id' => $created->id]
                )
            ],
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    private function uri()
    {
        return 'bank-account';
    }

    private function factory()
    {
        return 'bank_account';
    }

    private function only($data)
    {
        return array_only($data, [
            'bank_id',
            'account_name',
            'account_number',
            'opening_balance'
        ]);
    }

    protected function transformer($model)
    {
        return [
            'id'                => $model->id,
            'bank_id'           => $model->bank_id,
            'account_name'      => $model->account_name,
            'account_number'    => $model->account_number,
            'opening_balance'   => $model->number()->opening_balance,
        ];
    }

    private function createBanks()
    {
        return Factory::times(3)->create('bank');
    }

    private function dummies($times = 4)
    {
        return Factory::times($times)->create($this->factory());
    }
}