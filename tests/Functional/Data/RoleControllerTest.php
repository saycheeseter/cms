<?php

namespace Tests\Functional\Data;

use DateTime;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Entities\Permission;
use Tests\Functional\FunctionalTestCase;
use Lang;

class RoleControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_role_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'roles' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'roles' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'roles' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'roles' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name',
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'roles' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function accessing_the_role_create_page_should_successfully_display_the_page()
    {
        $this->get($this->uri().'/create')
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['sections', 'permissions']);
    }

    /**
     * @test
     */
    public function storing_a_role_with_an_empty_name_or_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.name', '');
        array_set($data, 'permissions', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.required'),
                ],
                'permissions' => [
                    Lang::get('core::validation.permissions.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_role_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.code', $existing->code);
        array_set($data, 'info.name', $existing->name);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique'),
                ],
                'info.name' => [
                    Lang::get('core::validation.name.unique'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_role_with_a_non_array_permission_input_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'permissions', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'permissions' => [
                    Lang::get('core::validation.permissions.invalid.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_role_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_role()
    {
        $build = $this->build();

        $data = $this->format($build);

        $expected = [
            'response' => [
                'role' => array_merge(
                    $this->item($build, 'infoTransformer'),
                    ['id' => 1]
                )
            ],
            'database' => [
                'role' => [
                    'id'   => 1,
                    'code' => $build->code,
                    'name' => $build->name
                ],
                'permission_role' => []
            ]
        ];

        foreach ($data['permissions'] as $permission) {
            $expected['database']['permission_role'][] = [
                'permission_id' => $permission,
                'role_id' => 1
            ];
        }

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('role', $expected['database']['role']);
        $this->assertDatabaseHave('permission_role', $expected['database']['permission_role']);
    }

    /**
     * @test
     */
    public function accessing_the_role_view_page_should_successfully_display_the_page()
    {
        $created = $this->create();

        $this->get($this->uri().'/'.$created->id.'/edit')
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['sections', 'permissions']);
    }

    /**
     * @test
     */
    public function updating_a_role_with_an_empty_code_name_apply_or_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created, ['apply' => '']);

        array_set($data, 'info.name', '');
        array_set($data, 'info.code', '');
        array_set($data, 'permissions', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.required'),
                ],
                'info.name' => [
                    Lang::get('core::validation.name.required'),
                ],
                'permissions' => [
                    Lang::get('core::validation.permissions.required')
                ],
                'apply' => [
                    Lang::get('core::validation.apply.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_role_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.code', $existing->code);
        array_set($data, 'info.name', $existing->name);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique'),
                ],
                'info.name' => [
                    Lang::get('core::validation.name.unique'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_role_with_a_non_array_permission_input_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'permissions', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'permissions' => [
                    Lang::get('core::validation.permissions.invalid.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_role_with_a_non_boolean_apply_option_input_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'apply', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'apply' => [
                    Lang::get('core::validation.apply.invalid'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_role_with_a_correct_input_should_save_the_data_in_database()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $expected = [
            'response' => [],
            'database' => [
                'role' => [
                    'id'   => $created->id,
                    'code' => $build->code,
                    'name' => $build->name
                ],
                'permission_role' => []
            ]
        ];

        foreach ($data['permissions'] as $permission) {
            $expected['database']['permission_role'][] = [
                'permission_id' => $permission,
                'role_id' => $created->id
            ];
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('role', $expected['database']['role']);
        $this->assertDatabaseHave('permission_role', $expected['database']['permission_role']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_removing_some_existing_permissions_in_the_specified_role_should_remove_the_corresponding_role_id_and_permission_id_in_the_permission_role_table()
    {
        $created = $this->create();

        $permissionsToRemove = $created->permissions->random(2);

        $created->permissions = $this->extractPermissions($created->permissions, $permissionsToRemove);

        $data = $this->format($created);

        $expected = [];

        foreach ($data['permissions'] as $permission) {
            $expected['database']['permission_role']['have'][] = [
                'permission_id' => $permission,
                'role_id' => $created->id
            ];
        }

        foreach ($permissionsToRemove as $permission) {
            $expected['database']['permission_role']['doesnt_have'][] = [
                'permission_id' => $permission->id,
                'role_id' => $created->id
            ];
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);

        $this->assertDatabaseHave('permission_role', $expected['database']['permission_role']['have']);
        $this->assertDatabaseHaveMissing('permission_role', $expected['database']['permission_role']['doesnt_have']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_adding_new_permissions_in_the_specified_role_should_add_the_corresponding_role_id_and_permission_id_in_the_permission_role_table_and_retain_its_existing_set_of_data()
    {
        $created = $this->create();

        $permissionsToAdd = $this->getRandomPermissionSet(
            $created->permissions
                ->random(2)
                ->pluck('alias')
                ->toArray()
        );

        $created->permissions = $created->permissions->merge($permissionsToAdd);

        $data = $this->format($created);

        $expected = [];

        foreach ($data['permissions'] as $permission) {
            $expected['database']['permission_role'][] = [
                'permission_id' => $permission,
                'role_id'       => $created->id
            ];
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);
        $this->assertDatabaseHave('permission_role', $expected['database']['permission_role']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_adding_a_permission_role_with_a_false_apply_option_should_not_affect_the_permissions_of_the_existing_users_with_the_corresponding_role()
    {
        $created = $this->create();

        $permissionsToAdd = $this->getRandomPermissionSet(
            $created->permissions
                ->random(2)
                ->pluck('alias')
                ->toArray()
        );

        $created->permissions = $created->permissions->merge($permissionsToAdd);

        $user = Factory::create('user', [
            'role_id' => $created->id
        ]);

        $this->givePermissions($user->id, 1, ['view_stock_delivery']);

        $data = $this->format($created);

        $expected = [];

        foreach ($data['permissions'] as $permission) {
            $expected['database']['branch_permission_user'][] = [
                'permission_id' => $permission,
                'user_id'       => $user->id,
                'branch_id'     => 1
            ];
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);

        $this->assertDatabaseHaveMissing('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_removing_a_permission_role_with_a_false_apply_option_should_not_affect_the_permissions_of_the_existing_users_with_the_corresponding_role()
    {
        $created = $this->create();

        $permissionsToRemove = $created->permissions->random(2);

        $created->permissions = $this->extractPermissions($created->permissions, $permissionsToRemove);

        $user = Factory::create('user', [
            'role_id' => $created->id
        ]);

        $this->givePermissions($user->id, 1, $permissionsToRemove->pluck('alias')->toArray());

        $data = $this->format($created);

        $expected = [];

        foreach ($permissionsToRemove as $permission) {
            $expected['database']['branch_permission_user'][] = [
                'permission_id' => $permission->id,
                'user_id'       => $user->id,
                'branch_id'     => 1
            ];
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);

        $this->assertDatabaseHave('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_adding_a_permission_role_with_a_true_apply_option_should_also_add_the_new_permissions_to_users_with_the_corresponding_role()
    {
        $created = $this->create();

        $permissionsToAdd = $this->getRandomPermissionSet(
            $created->permissions
                ->pluck('alias')
                ->toArray()
        );

        $created->permissions = $created->permissions->merge($permissionsToAdd);

        $users = Factory::times(2)->create('user', [
            'role_id' => $created->id
        ]);

        foreach ($users as $user) {
            $this->givePermissions($user->id, 1, ['view_stock_delivery']);
        }

        $data = $this->format($created, ['apply' => true]);

        $expected = [];

        foreach ($permissionsToAdd as $permission) {
            foreach ($users as $user) {
                $expected['database']['branch_permission_user'][] = [
                    'permission_id' => $permission->id,
                    'user_id'       => $user->id,
                    'branch_id'     => 1
                ];
            }
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);
        $this->assertDatabaseHave('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function updating_a_role_and_removing_a_permission_role_with_a_true_apply_option_should_also_remove_the_selected_permissions_to_users_with_the_corresponding_role()
    {
        $created = $this->create();

        $permissionsToRemove = $created->permissions->random(2);

        $created->permissions = $this->extractPermissions($created->permissions, $permissionsToRemove);

        $users = Factory::times(2)->create('user', [
            'role_id' => $created->id
        ]);

        foreach ($users as $user) {
            $this->givePermissions($user->id, 1, $permissionsToRemove->pluck('alias')->toArray());
        }

        $data = $this->format($created, ['apply' => true]);

        $expected = [];

        foreach ($permissionsToRemove as $permission) {
            foreach ($users as $user) {
                $expected['database']['branch_permission_user'][] = [
                    'permission_id' => $permission->id,
                    'user_id'       => $user->id,
                    'branch_id'     => 1
                ];
            }
        }

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data);

        $this->assertDatabaseHaveMissing('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_role_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_role_should_return_the_details_of_the_specified_id()
    {
        $created = Factory::create($this->factory());

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([
                'role' => $this->item($created, 'infoTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_role_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('role', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_role_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = $this->create();

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('role', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_role_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('role', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_role_with_an_assigned_user_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        Factory::times(2)->create('user', [
            'role_id' => $created->id
        ]);

        $this->deleteJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'role.already.used' => Lang::get('core::validation.role.already.used')
            ]));

        $this->assertDatabaseHas('role', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function accessing_the_role_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_role_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_role_view_page_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_role_create_page_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->getJson($this->uri().'/create')
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_role_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = $this->build();

        $data = $this->format($build);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_role_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_role_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_the_list_of_permissions_of_a_non_existing_role_should_return_an_unauthorized_error_message()
    {
        $this->getJson($this->uri().'/5/permissions')
            ->assertExactJson($this->errorResponse([
                'record.not.found' => Lang::get('core::error.record.not.found')
            ]));

        $this->assertDatabaseMissing('role', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_the_list_of_permissions_of_an_existing_role_should_return_corresponding_permissions_of_the_role()
    {
        $created = $this->create();

        $permissions = $created->permissions->pluck('id')->toArray();

        $this->getJson($this->uri().'/'.$created->id.'/permissions')
            ->assertExactJson($this->successfulResponse([
                'permissions' => $permissions
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_role_with_the_same_code_or_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                [],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function storing_a_role_with_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_role_series()
    {
        $build = $this->build(['code' => '']);

        $map = array_search(get_class($build), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->series_column)
            ->first();

        $data = $this->format($build);

        $expected = [
            'database' => [
                'id'   => 1,
                'code' => $series->series,
                'name' => $build->name
            ]
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('role', $expected['database']);
    }

    private function uri()
    {
        return 'role';
    }

    private function factory()
    {
        return 'role';
    }

    private function format($model, $data = ['apply' => false])
    {
        $format = [
            'info' => [
                'code' => $model->code,
                'name' => $model->name
            ],
            'permissions' => $model->permissions->pluck('id')->toArray()
        ];

        return array_merge($format, $data);
    }

    protected function infoTransformer($model)
    {
        return [
            'id'          => $model->id,
            'code'        => $model->code,
            'name'        => $model->name,
            'permissions' => $model->permissions->pluck('id')->toArray()
        ];
    }

    protected function basicTransformer($model)
    {
        return [
            'id'   => $model->id,
            'code' => $model->code,
            'name' => $model->name
        ];
    }

    private function build($data = [])
    {
        $role = Factory::build($this->factory(), $data);

        $role->permissions = $this->getRandomPermissionSet();

        return $role;
    }

    private function create($data = [])
    {
        $role = Factory::create($this->factory(), $data);

        $role->permissions()->sync($this->getRandomPermissionSet());

        return $role;
    }

    private function dummies($times = 4)
    {
        $that = $this;

        $data = Factory::times($times)->create($this->factory())
            ->each(function($role) use ($that) {
                $role->permissions()->sync($that->getRandomPermissionSet());
            });

        return $data;
    }

    private function getRandomPermissionSet($exclude = [])
    {
        $aliases = [
            'login',
            'view_brand',
            'view_category',
            'create_supplier',
            'create_uom',
            'create_user',
            'view_product',
            'view_damage',
            'view_purchase_order',
            'view_sales',
            'view_sales_outbound'
        ];

        if (count($exclude) > 0) {
            for ($i = count($aliases) - 1; $i >= 0; $i--) {
                if (in_array($aliases[$i], $exclude)) {
                    unset($aliases[$i]);
                }
            }
        }

        $aliases = array_values($aliases);

        return Permission::whereIn('alias', array_random($aliases, 4))->get();
    }

    private function extractPermissions($data, $basis)
    {
        return $data->reject(function ($item, $key) use ($basis) {
            $found = $basis->search(function($permission, $index) use ($item) {
                return $permission->id == $item->id;
            });

            return is_numeric($found);
        });
    }
}