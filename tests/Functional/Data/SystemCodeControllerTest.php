<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Lang;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Traits\ManagesDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Tests\Functional\FunctionalTestCase;
use App;
use DB;

abstract class SystemCodeControllerTest extends FunctionalTestCase
{
    use ManagesDispatcher;

    /**
     * @test
     */
    public function accessing_a_system_code_page_will_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['title', 'permissions']);
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value'    => $data[0]->code,
                    'operator' => '=',
                    'join'     => 'AND',
                    'column'   => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => 1,
                        'count'        => 1,
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => [
                    $this->item($data[0], 'transformer')
                ]
            ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value'    => $data[2]->name,
                    'operator' => '=',
                    'join'     => 'AND',
                    'column'   => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => 1,
                        'count'        => 1,
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => [
                    $this->item($data[2], 'transformer')
                ]
            ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value'    => $data[3]->remarks,
                    'operator' => '=',
                    'join'     => 'AND',
                    'column'   => 'remarks'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => 1,
                        'count'        => 1,
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => [
                    $this->item($data[3], 'transformer')
                ]
            ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_updated_at_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->dummies();

        sleep(60);

        $added = Factory::times(2)->create($this->factory());

        $filters = [
            'filters' => [
                [
                    'value'    => $added[0]->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join'     => 'AND',
                    'column'   => 'updated_at'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => $added->count(),
                        'count'        => $added->count(),
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => $this->collection($added, 'transformer')
            ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value'    => $data[3]->id,
                    'operator' => '=',
                    'join'     => 'AND',
                    'column'   => 'id'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => 4,
                        'count'        => 4,
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => $this->collection($data, 'transformer')
            ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->dummies();

        $filters = [
            'filters' => [
                [
                    'value'    => 'Foo',
                    'operator' => '=',
                    'join'     => 'AND',
                    'column'   => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri() . '?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'meta'         => [
                    'pagination' => [
                        'total'        => 0,
                        'count'        => 0,
                        'per_page'     => 10,
                        'current_page' => 1,
                        'total_pages'  => 1,
                        'links'        => []
                    ]
                ],
                'system_codes' => []
            ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function storing_an_empty_name_will_trigger_a_validation_and_return_an_error_response()
    {
        $build = Factory::build($this->factory(), [
            'name' => '',
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'name' => [
                    Lang::get('core::validation.name.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $dummies = $this->dummies();

        $build = Factory::build($this->factory(), [
            'code' => $dummies[1]->code,
            'name' => $dummies[1]->name
        ]);

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'name' => [
                    Lang::get('core::validation.name.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_unique_code_and_name_will_successfully_save_the_input_in_database_and_return_a_successful_response()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'system_code' => array_merge(
                    $this->item($build, 'transformer'),
                    ['id' => 15]
                )
            ],
            'database' => array_merge(
                $data,
                ['type_id' => $build->type_id]
            )
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('system_code', $expected['database']);
    }

    /**
     * @test
     */
    public function storing_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_system_code_series()
    {
        $build = Factory::build($this->factory(), [
            'code' => ''
        ]);

        $map = array_search(get_class($build), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', 'code')
            ->first();

        $data = $this->only($build->toArray());

        $expected = [
            'database' => array_merge(
                $data,
                [
                    'type_id' => $build->type_id,
                    'code'    => $series->series
                ]
            )
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('system_code', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_an_empty_code_or_name_will_trigger_a_validation_and_return_an_error_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['code'] = '';
        $data['name'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.required')
                ],
                'name' => [
                    Lang::get('core::validation.name.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_an_existing_code_or_name_will_trigger_a_validation_and_return_an_error_response()
    {
        $dummies = $this->dummies();

        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $data['code'] = $dummies[1]->code;
        $data['name'] = $dummies[1]->name;

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'code' => [
                    Lang::get('core::validation.code.unique')
                ],
                'name' => [
                    Lang::get('core::validation.name.unique')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_a_unique_code_and_name_will_successfully_update_the_data_in_database_and_return_a_successful_response()
    {
        $created = Factory::create($this->factory());
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $expected = [
            'response' => [
                'system_code' => array_merge(
                    $this->item($build, 'transformer'),
                    ['id' => $created->id]
                )
            ],
            'database' => array_merge(
                $data,
                [
                    'type_id' => $created->type_id,
                    'id' => $created->id
                ]
            )
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('system_code', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_resource_will_trigger_an_error_response()
    {
        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/100', $this->uri()), $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('system_code',
            array_merge(
                $data,
                ['type_id' => $build->type_id]
            )
        );
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_resource_will_trigger_an_error_response()
    {
        $this->deleteJson(sprintf('%s/100', $this->uri()))
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('system_code', ['id' => 100]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_resource_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson(
                $this->successfulResponse([], Lang::get('core::success.deleted'))
            );

        $this->assertSoftDeleted('system_code', ['id' => $created->id]);
    }

    /**
     * @test
     */
    public function accessing_the_index_page_without_view_permission_will_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function saving_a_new_resource_without_create_permission_will_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_resource_without_update_permission_will_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $build = Factory::build($this->factory());

        $data = $this->only($build->toArray());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_resource_without_delete_permission_will_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = Factory::create($this->factory());

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_system_code_data()
    {
        $dummies = $this->dummies();

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'system_codes' => $this->collection($dummies, 'transformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.system.code.found' => Lang::get('core::error.no.system.code.found')
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_the_same_code_or_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = Factory::create($this->factory());

        $data = $this->only($created->toArray());

        $expected = [
            'response' => [
                'system_code' => array_merge(
                    $this->item($created, 'transformer'),
                    ['id' => $created->id]
                )
            ],
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    protected function transformer($model)
    {
        return [
            'id'      => $model->id,
            'code'    => $model->code,
            'name'    => $model->name,
            'remarks' => $model->remarks,
        ];
    }

    protected function dummies($times = 4)
    {
        $data = collect([]);

        if (method_exists($this, 'initial')) {
            $times--;

            $data->prepend($this->initial());
        }

        $data = $data->merge(Factory::times($times)->create($this->factory()));

        return $data;
    }

    protected function only($data)
    {
        return array_only($data, ['code', 'name', 'remarks']);
    }

    protected abstract function uri();
    protected abstract function factory();
}