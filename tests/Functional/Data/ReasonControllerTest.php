<?php

namespace Tests\Functional\Data;

class ReasonControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'reason';
    }

    protected function factory()
    {
        return 'reason';
    }
}