<?php

namespace Tests\Functional\Data;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\User;
use Modules\Core\Enums\UserType;
use Tests\Functional\FunctionalTestCase;
use Hash;
use Lang;
use Auth;

class UserControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_user_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => []

        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_username_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->username,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'username'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_full_name_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[0]->full_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection(array_wrap($dummies[0]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_position_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->position,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'position'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection(array_wrap($dummies[2]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_name()
    {
        $dummies = $this->dummies()->pluck('info');

        sleep(60);

        $added = $this->create();

        $filters = [
            'filters' => [
                [
                    'value' => $added->info->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection(array_wrap($added->info), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function superadmin_accounts_should_not_be_searchable_upon_sending_a_search_parameters()
    {
        $superadmin = Factory::create($this->factory(), [
            'type' => UserType::SUPERADMIN
        ]);

        $filters = [
            'filters' => [
                [
                    'value' => $superadmin->full_name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'full_name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'users' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function accessing_the_user_create_page_should_successfully_display_the_page()
    {
        $this->get($this->uri().'/create')
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['branches', 'sections', 'roles', 'permissions']);
    }

    /**
     * @test
     */
    public function storing_a_user_with_an_empty_username_password_full_name_status_or_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.username', '');
        array_set($data, 'info.password', '');
        array_set($data, 'info.full_name', '');
        array_set($data, 'info.status', '');
        array_set($data, 'permissions', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.username' => [
                    Lang::get('core::validation.username.required')
                ],
                'info.password' => [
                    Lang::get('core::validation.password.required')
                ],
                'info.full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'info.status' => [
                    Lang::get('core::validation.status.required')
                ],
                'permissions' => [
                    Lang::get('core::validation.permissions.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_an_existing_code_username_or_full_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.username', $existing->info->username);
        array_set($data, 'info.full_name', $existing->info->full_name);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique'),
                ],
                'info.username' => [
                    Lang::get('core::validation.username.unique'),
                ],
                'info.full_name' => [
                    Lang::get('core::validation.full.name.unique'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_password_less_than_six_characters_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.password', $this->faker->lexify('?????'));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.password' => [
                    Lang::get('core::validation.password.min.length')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_non_boolean_status_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.status', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.status' => [
                    Lang::get('core::validation.status.numeric')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.email', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.email' => [
                    Lang::get('core::validation.email.invalid')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_non_existing_role_id_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'info.role_id', $this->faker->numberBetween(100, 1000));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.role_id' => [
                    Lang::get('core::validation.role.doesnt.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_non_array_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'permissions', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'permissions' => [
                    Lang::get('core::validation.permissions.invalid.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_an_empty_permission_branch_or_code_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $data['permissions'] = [
            [
                'branch' => '',
                'code' => []
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.branch' => [
                    'validation.required',
                ],
                'permissions.0.code' => [
                    'validation.required',
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_non_existing_permission_branch_or_code_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $data['permissions'] = [
            [
                'branch' => $this->faker->numberBetween(100, 1000),
                'code' => [
                    $this->faker->numberBetween(999, 2000)
                ]
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.branch' => [
                    'validation.exists_in',
                ],
                'permissions.0.code.0' => [
                    'validation.exists',
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_non_array_permission_code_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $data['permissions'][0]['code'] = 'Foo';

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.code' => [
                    'validation.array',
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_user_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_user()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $id = ($created->info->id + 1);

        $expected = [
            'response' => [
                'user' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $id]
                )
            ],
            'database' => [
                'user' => array_merge(
                    array_except($data['info'], 'password'),
                    ['id' => $id]
                ),
                'branch_permission_user' => $this->formatPermissionsToFlat($data['permissions'], $id)
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('user', $expected['database']['user']);
        $this->assertDatabaseHave('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function accessing_the_user_view_page_should_successfully_display_the_page()
    {
        $created = $this->create();

        $this->get($this->uri().'/'.$created->info->id.'/edit')
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['branches', 'sections', 'roles', 'permissions']);
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_empty_code_username_password_full_name_status_or_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.code', '');
        array_set($data, 'info.username', '');
        array_set($data, 'info.password', '');
        array_set($data, 'info.full_name', '');
        array_set($data, 'info.status', '');
        array_set($data, 'permissions', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.required')
                ],
                'info.username' => [
                    Lang::get('core::validation.username.required')
                ],
                'info.full_name' => [
                    Lang::get('core::validation.full.name.required')
                ],
                'info.status' => [
                    Lang::get('core::validation.status.required')
                ],
                'permissions' => [
                    Lang::get('core::validation.permissions.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_existing_code_username_or_full_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.username', $existing->info->username);
        array_set($data, 'info.full_name', $existing->info->full_name);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    Lang::get('core::validation.code.unique'),
                ],
                'info.username' => [
                    Lang::get('core::validation.username.unique'),
                ],
                'info.full_name' => [
                    Lang::get('core::validation.full.name.unique'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_password_less_than_six_characters_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.password', $this->faker->lexify('?????'));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.password' => [
                    Lang::get('core::validation.password.min.length')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_non_boolean_status_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.status', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.status' => [
                    Lang::get('core::validation.status.numeric')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_invalid_email_format_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.email', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.email' => [
                    Lang::get('core::validation.email.invalid')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_non_existing_role_id_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.role_id', $this->faker->numberBetween(100, 1000));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.role_id' => [
                    Lang::get('core::validation.role.doesnt.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_non_array_permissions_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'permissions', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'permissions' => [
                    Lang::get('core::validation.permissions.invalid.format'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_empty_permission_branch_or_code_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        $data['permissions'] = [
            [
                'branch' => '',
                'code' => []
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.branch' => [
                    'validation.required',
                ],
                'permissions.0.code' => [
                    'validation.required',
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_non_existing_permission_branch_or_code_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        $data['permissions'] = [
            [
                'branch' => $this->faker->numberBetween(100, 1000),
                'code' => [
                    $this->faker->numberBetween(999, 2000)
                ]
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.branch' => [
                    'validation.exists_in',
                ],
                'permissions.0.code.0' => [
                    'validation.exists',
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_non_array_permission_code_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        $data['permissions'][0]['code'] = 'Foo';

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'permissions.0.code' => [
                    'validation.array',
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_empty_password_should_not_be_validated()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'info.password', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse([
                'user' => $this->item($created->info, 'basicTransformer'),
            ], Lang::get('core::success.updated')));
    }

    /**
     * @test
     */
    public function updating_a_user_and_removing_some_existing_permissions_should_remove_the_corresponding_user_id_branch_id_and_permission_id_in_branch_permission_user_table()
    {
        $created = $this->create();

        $removed = [
            'user_id' => $created->info->id,
            'branch_id' => array_get($created->permissions, '0.branch'),
            'permission_id' => array_get($created->permissions, '0.code.0')
        ];

        array_forget($created->permissions, '0.code.0');

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);

        $this->assertDatabaseMissing('branch_permission_user', $removed);
    }

    /**
     * @test
     */
    public function updating_a_user_and_adding_new_existing_permissions_should_add_the_corresponding_user_id_branch_id_and_permission_id_in_branch_permission_user_table()
    {
        $created = $this->create();

        $permission = Permission::where('alias', 'view_inventory_adjust')->first();

        $added = [
            'user_id' => $created->info->id,
            'branch_id' => array_get($created->permissions, '0.branch'),
            'permission_id' => $permission->id
        ];

        array_set($created->permissions, '0.code.4', $permission->id);

        $data = $this->format($created);


        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);

        $this->assertDatabaseHas('branch_permission_user', $added);
    }

    /**
     * @test
     */
    public function updating_a_user_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_user()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $id = $created->info->id;

        $expected = [
            'response' => [
                'user' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $id]
                )
            ],
            'database' => [
                'user' => array_merge(
                    array_except($data['info'], 'password'),
                    ['id' => $id]
                ),
                'branch_permission_user' => $this->formatPermissionsToFlat($data['permissions'], $id)
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('user', $expected['database']['user']);
        $this->assertDatabaseHave('branch_permission_user', $expected['database']['branch_permission_user']);
    }

    /**
     * @test
     */
    public function updating_a_user_with_an_empty_password_should_not_change_the_existing_saved_password_in_database()
    {
        $created = $this->create([
            'password' => 121586
        ]);

        $data = $this->format($created);

        array_set($data, 'info.password', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);

        $user = User::find($created->info->id);

        $this->assertTrue(Hash::check('121586', $user->password));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_user_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_user_should_return_the_details_of_the_specified_id()
    {
        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([
                'user'        => $this->item($created->info, 'infoTransformer'),
                'permissions' => $this->item($created->info, 'permissionTransformer'),
                'attachment'  => $this->collection($created->info->attachments, 'attachmentTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_user_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('user', ['id' => 5]);
    }

    /**
     * @test
     */
    public function updating_a_superadmin_account_should_trigger_a_validation_and_return_an_error()
    {
        $superadmin = $this->create(['type' => UserType::SUPERADMIN]);

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/'.$superadmin->info->id, $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseHas('user', array_merge(
            ['id' => $superadmin->info->id],
            $this->format($superadmin)['info']
        ));
    }

    /**
     * @test
     */
    public function deleting_a_user_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = $this->create();

        $this->deleteJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('user', ['id' => $created->info->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_user_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('user', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_superadmin_account_should_trigger_a_validation_and_return_an_error()
    {
        $superadmin = $this->create(['type' => UserType::SUPERADMIN]);

        $this->deleteJson($this->uri().'/'.$superadmin->info->id)
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseHas('user', ['id' => $superadmin->info->id]);
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_an_empty_old_password_or_new_password_should_trigger_a_validation_and_return_an_error()
    {
        $credentials = [
            'old_password' => '',
            'new_password' => '',
            'new_password_confirmation' => ''
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->errorResponse([
                'old_password' => [
                    Lang::get('core::validation.old.password.required'),
                ],
                'new_password' => [
                    Lang::get('core::validation.new.password.required'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_a_new_password_less_than_six_characters_should_trigger_a_validation_and_return_an_error()
    {
        $credentials = [
            'old_password' => '121586',
            'new_password' => 'Foo',
            'new_password_confirmation' => 'Foo'
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->errorResponse([
                'new_password' => [
                    Lang::get('core::validation.new.password.min.length'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_a_different_value_for_new_password_and_confirmation_should_trigger_a_validation_and_return_an_error()
    {
        $credentials = [
            'old_password' => '121586',
            'new_password' => 'LoremIpsum',
            'new_password_confirmation' => 'IpsumLorem'
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->errorResponse([
                'new_password' => [
                    Lang::get('core::validation.confirm.password.incorrect'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_a_different_old_password_should_trigger_a_validation_and_return_an_error()
    {
        $credentials = [
            'old_password' => '123456',
            'new_password' => 'LoremIpsum',
            'new_password_confirmation' => 'LoremIpsum'
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->errorResponse([
                'old.password.invalid' => Lang::get('core::validation.old.password.incorrect')
            ]));
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_the_same_old_and_new_password_should_trigger_a_validation_and_return_an_error()
    {
        $credentials = [
            'old_password' => '121586',
            'new_password' => '121586',
            'new_password_confirmation' => '121586'
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->errorResponse([
                'cant.change.same.password' => Lang::get('core::validation.cant.change.same.password')
            ]));
    }

    /**
     * @test
     */
    public function changing_the_current_password_with_a_valid_input_should_successfully_change_the_password_in_database()
    {
        $credentials = [
            'old_password' => '121586',
            'new_password' => 'LoremIpsum',
            'new_password_confirmation' => 'LoremIpsum'
        ];

        $this->patchJson($this->uri().'/change-password', $credentials)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.password.changed')));

        $this->assertTrue(Hash::check('LoremIpsum', Auth::user()->password));
    }

    /**
     * @test
     */
    public function accessing_the_user_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_user_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_user_view_page_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function accessing_the_user_create_page_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->getJson($this->uri().'/create')
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_user_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = $this->build();

        $data = $this->format($build);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_user_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_user_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->info->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_user_with_the_same_code_username_or_full_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                ['user' => $this->item($created->info,'basicTransformer')],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function storing_a_user_with_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_user_series()
    {
        $users = User::get();

        $build = $this->build(['code' => '']);

        $map = array_search(get_class($build->info), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->info->series_column)
            ->first();

        $data = $this->format($build);

        $expected = [
            'database' => [
                'id'   => ($users->last()->id + 1),
                'code' => $series->series,
                'full_name' => $build->info->full_name
            ]
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('user', $expected['database']);
    }

    private function uri()
    {
        return 'user';
    }

    private function factory()
    {
        return 'user';
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'full_name'     => $model->full_name,
            'username'      => $model->username,
            'position'      => $model->position,
            'status'        => $model->presenter()->status
        ];
    }

    protected function permissionTransformer($model)
    {
        $attributes = [];

        $branches = $model->branches()->distinct()->get();

        foreach ($branches as $key => $branch) {
            $permissions = $this->filterPermissionsByBranch($model->permissions, $branch->id);

            $attributes[] = [
                'branch' => $branch->id,
                'code'   => $permissions
            ];
        }

        return $attributes;
    }

    protected function attachmentTransformer($model)
    {
        $path = sprintf('storage/%s', $model->filepath);

        return [
            'link'      => asset($path),
            'key'       => $model->key,
            'original'  => $model->filename,
            'status'    => 'saved',
            'attribute' => getimagesize($path)
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'full_name'     => $model->full_name,
            'username'      => $model->username,
            'status'        => $model->status,
            'position'      => $model->position ?? '',
            'address'       => $model->address ?? '',
            'telephone'     => $model->telephone ?? '',
            'mobile'        => $model->mobile ?? '',
            'email'         => $model->email ?? '',
            'memo'          => $model->memo ?? '',
            'role_id'       => $model->role_id
        ];
    }

    private function filterPermissionsByBranch($permissions, $branchId)
    {
        $filtered = $permissions->filter(function ($value, $key) use ($branchId) {
            return $value->pivot->branch_id == $branchId;
        });

        return array_pluck($filtered->toArray(), 'id');
    }

    private function build($data = [])
    {
        $user = Factory::build($this->factory(), $data);

        $permissions = $this->getRandomPermissionSet();

        return (object) [
            'info' => $user,
            'permissions' => $permissions
        ];
    }

    private function create($data = [])
    {
        $user = Factory::create($this->factory(), $data);

        $permissions = $this->getRandomPermissionSet();

        foreach ($this->formatPermissionsToPivot($permissions) as $set) {
            $user->permissions()->sync($set);
        }

        return (object) [
            'info' => $user,
            'permissions' => $permissions
        ];
    }

    private function dummies($times = 4)
    {
        $that = $this;

        $data = [];

        Factory::times(($times - 1))->create($this->factory())
            ->each(function($user) use ($that, &$data) {
                $permissions = $that->getRandomPermissionSet();

                foreach ($that->formatPermissionsToPivot($permissions) as $set) {
                    $user->permissions()->attach($set);
                }

                $data[] = (object) [
                    'info' => $user,
                    'permissions' => $permissions
                ];
            });

        return collect($data)->prepend($this->initial());
    }

    private function format($model)
    {
        return [
            'info' => [
                'code'      => $model->info->code,
                'username'  => $model->info->username,
                'full_name' => $model->info->full_name,
                'password'  => $model->info->password,
                'status'    => $model->info->status,
                'telephone' => $model->info->telephone,
                'mobile'    => $model->info->mobile,
                'position'  => $model->info->position,
                'email'     => $model->info->email,
                'address'   => $model->info->address,
                'memo'      => $model->info->memo,
                'role_id'   => $model->info->role_id,
            ],
            'permissions' => $model->permissions
        ];
    }

    private function formatPermissionsToPivot($permissions)
    {
        $collection = array();

        foreach ($permissions as $permission) {
            $formatted = array();

            foreach ($permission['code'] as $code) {
                $formatted = array_add($formatted, $code, array(
                    'branch_id' => $permission['branch']
                ));
            }

            $collection[] = $formatted;
        }

        return $collection;
    }

    private function formatPermissionsToFlat($permissions, $userId)
    {
        $collection = [];

        foreach ($permissions as $permission) {
            foreach ($permission['code'] as $code) {
                $collection[] = [
                    'user_id' => $userId,
                    'branch_id' => $permission['branch'],
                    'permission_id' => $code
                ];
            }
        }

        return $collection;
    }
    private function getRandomPermissionSet($exclude = [])
    {
        Factory::create('branch');

        $aliases = [
            'login',
            'view_brand',
            'view_category',
            'create_supplier',
            'create_uom',
            'create_user',
            'view_product',
            'view_damage',
            'view_purchase_order',
            'view_sales',
            'view_sales_outbound'
        ];

        if (count($exclude) > 0) {
            for ($i = count($aliases) - 1; $i >= 0; $i--) {
                if (in_array($aliases[$i], $exclude)) {
                    unset($aliases[$i]);
                }
            }
        }

        $aliases = array_values($aliases);

        $branches = Branch::take(2)->get();

        $collection = [];

        foreach ($branches as $branch) {
            $permissions = Permission::whereIn('alias', array_random($aliases, 4))->get();

            $collection[] = [
                'branch' => $branch->id,
                'code' => $permissions->pluck('id')->toArray()
            ];
        }

        return $collection;
    }

    private function initial()
    {
        $user = User::where('type', '<>', UserType::SUPERADMIN)->first();

        return (object) [
            'info' => $user,
            'permissions' => $this->item($user, 'permissionTransformer')
        ];
    }
}