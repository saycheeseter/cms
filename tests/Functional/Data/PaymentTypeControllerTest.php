<?php

namespace Tests\Functional\Data;

class PaymentTypeControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'payment-type';
    }

    protected function factory()
    {
        return 'payment_type';
    }
}