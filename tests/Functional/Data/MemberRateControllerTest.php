<?php

namespace Tests\Functional\Data;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Tests\Functional\FunctionalTestCase;
use Lang;
use DB;

class MemberRateControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_member_rate_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => []

        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_name_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_memo_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->memo,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'memo'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        sleep(60);

        $added = $this->create();

        $filters = [
            'filters' => [
                [
                    'value' => $added->info->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => $this->collection(array_wrap($added->info), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'rates' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_an_empty_name_amount_from_amount_to_date_from_date_to_amount_per_point_or_discount_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.name', '');
        array_set($data, 'details.0.amount_from', '');
        array_set($data, 'details.0.amount_to', '');
        array_set($data, 'details.0.date_from', '');
        array_set($data, 'details.0.date_to', '');
        array_set($data, 'details.0.amount_per_point', '');
        array_set($data, 'details.0.discount', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.required')
                ],
                'details.0.amount_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.required')
                ],
                'details.0.amount_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.required')
                ],
                'details.0.date_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.required')
                ],
                'details.0.date_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.required')
                ],
                'details.0.amount_per_point' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.required')
                ],
                'details.0.discount' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_an_empty_details_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'details', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.details.min.one')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_an_existing_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.name', $existing->info->name);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.unique')
                ]
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_a_non_numeric_amount_from_amount_to_amount_per_point_or_discount_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'details.0.amount_from', 'FOO');
        array_set($data, 'details.0.amount_to', 'FOO');
        array_set($data, 'details.0.amount_per_point', 'FOO');
        array_set($data, 'details.0.discount', 'FOO');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.amount_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.invalid.format')
                ],
                'details.0.amount_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.invalid.format')
                ],
                'details.0.amount_per_point' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.invalid.format')
                ],
                'details.0.discount' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_a_incorrect_date_format_for_date_from_or_date_to_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'details.0.date_from', Carbon::parse(array_get($data, 'details.0.date_from'))->toDateTimeString());
        array_set($data, 'details.0.date_to', Carbon::parse(array_get($data, 'details.0.to'))->toDateTimeString());

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.date_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.invalid.format')
                ],
                'details.0.date_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_an_overlapping_date_range_in_details_set_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $previousDateTo = Carbon::parse(array_get($data, 'details.0.date_to'));

        array_set($data,'details.1.date_from', $previousDateTo->subDay()->format('Y-m-d'));
        array_set($data,'details.1.date_to', $previousDateTo->addDays(10)->format('Y-m-d'));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.date_to.overlap.state' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap.state')
                ],
                'details.1.date_from.overlap.state' => [
                    Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.from.overlap.state')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_an_invalid_date_from_and_date_to_range_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        $temp = array_get($data, 'details.0.date_from');

        array_set($data, 'details.0.date_from', array_get($data, 'details.0.date_to'));
        array_set($data, 'details.0.date_to', $temp);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.invalid.date.range' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.invalid.date.range')
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_member_rate_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_member_rate()
    {
        $build = $this->build();

        $data = $this->format($build);

        $expected = [
            'response' => [
                'rate' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => 1]
                )
            ],
            'database' => [
                'member_rate' => array_merge(
                    $data['info'],
                    ['id' => 1]
                ),
                'member_rate_detail' => array_map(function($item) {
                    return array_merge($item, [
                        'date_from' => Carbon::parse($item['date_from'])->startOfDay(),
                        'date_to' => Carbon::parse($item['date_to'])->endOfDay(),
                    ]);
                }, $data['details'])
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('member_rate', $expected['database']['member_rate']);
        $this->assertDatabaseHave('member_rate_detail', $expected['database']['member_rate_detail']);
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_an_empty_name_amount_from_amount_to_date_from_date_to_amount_per_point_or_discount_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.name', '');
        array_set($data, 'details.0.amount_from', '');
        array_set($data, 'details.0.amount_to', '');
        array_set($data, 'details.0.date_from', '');
        array_set($data, 'details.0.date_to', '');
        array_set($data, 'details.0.amount_per_point', '');
        array_set($data, 'details.0.discount', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.required')
                ],
                'details.0.amount_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.required')
                ],
                'details.0.amount_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.required')
                ],
                'details.0.date_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.required')
                ],
                'details.0.date_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.required')
                ],
                'details.0.amount_per_point' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.required')
                ],
                'details.0.discount' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.required')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_an_empty_details_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'details', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    Lang::get('core::validation.details.min.one')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_an_existing_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.name', $existing->info->name);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    Lang::get('core::validation.name.unique')
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_a_non_numeric_amount_from_amount_to_amount_per_point_or_discount_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'details.0.amount_from', 'FOO');
        array_set($data, 'details.0.amount_to', 'FOO');
        array_set($data, 'details.0.amount_per_point', 'FOO');
        array_set($data, 'details.0.discount', 'FOO');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.amount_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.invalid.format')
                ],
                'details.0.amount_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.invalid.format')
                ],
                'details.0.amount_per_point' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.invalid.format')
                ],
                'details.0.discount' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_a_incorrect_date_format_for_date_from_or_date_to_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'details.0.date_from', Carbon::parse(array_get($data, 'details.0.date_from'))->toDateTimeString());
        array_set($data, 'details.0.date_to', Carbon::parse(array_get($data, 'details.0.to'))->toDateTimeString());

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.date_from' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.invalid.format')
                ],
                'details.0.date_to' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.invalid.format')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_an_overlapping_date_range_in_details_set_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        $previousDateTo = Carbon::parse(array_get($data, 'details.0.date_to'));

        array_set($data,'details.1.date_from', $previousDateTo->subDay()->format('Y-m-d'));
        array_set($data,'details.1.date_to', $previousDateTo->addDays(10)->format('Y-m-d'));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.date_to.overlap.state' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap.state')
                ],
                'details.1.date_from.overlap.state' => [
                    Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.from.overlap.state')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_an_invalid_date_from_and_date_to_range_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        $temp = array_get($data, 'details.0.date_from');

        array_set($data, 'details.0.date_from', array_get($data, 'details.0.date_to'));
        array_set($data, 'details.0.date_to', $temp);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.invalid.date.range' => [
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.invalid.date.range')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_member_rate()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $expected = [
            'response' => [
                'rate' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $created->info->id]
                )
            ],
            'database' => [
                'member_rate' => array_merge(
                    $data['info'],
                    ['id' => $created->info->id]
                ),
                'member_rate_detail' => array_map(function($item) {
                    return array_merge($item, [
                        'date_from' => Carbon::parse($item['date_from'])->startOfDay(),
                        'date_to' => Carbon::parse($item['date_to'])->endOfDay(),
                    ]);
                }, $data['details'])
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('member_rate', $expected['database']['member_rate']);
        $this->assertDatabaseHave('member_rate_detail', $expected['database']['member_rate_detail']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_member_rate_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_member_rate_should_return_the_details_of_the_specified_id()
    {
        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([
                'rate'       => $this->item($created->info, 'infoTransformer'),
                'details'    => $this->collection($created->details, 'detailsTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_member_rate_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('member_rate', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_member_rate_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = $this->create();

        $this->deleteJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('member_rate', ['id' => $created->info->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_member_rate_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('member_rate', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_member_rate_used_by_a_member_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        Factory::create('member', [
            'rate_head_id' => $created->info->id
        ]);

        $this->deleteJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'rate.in.used' => Lang::get('core::validation.delete.used.rate')
            ]));

        $this->assertDatabaseHas('member_rate', ['id' => $created->info->id]);
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_member_rates_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'memberRates' => $this->collection($dummies, 'basicTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.member.rates.found' => Lang::get('core::error.no.member.rates.found')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_member_rate_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_member_rate_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_member_rate_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = $this->build();

        $data = $this->format($build);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_member_rate_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_member_rate_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->info->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_member_rate_with_the_same_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                ['rate' => $this->item($created->info,'basicTransformer')],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function updating_a_member_rate_with_a_date_from_that_is_equal_to_todays_date_should_set_not_set_the_field_to_the_start_of_the_data()
    {
        $created = $this->create();

        $build = $this->build();

        $created->details->pop();
        $build->details->pop();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                [
                    'id' => $created['id'],
                    'date_from' => Carbon::now()->format('Y-m-d')
                ]
            );
        }, $data['details'], $created->details->toArray()));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);

        $detail = DB::table('member_rate_detail')->where('id', $data['details'][0]['id'])->first();

        $this->assertTrue(
            Carbon::parse($detail->date_from)->greaterThan(Carbon::parse($data['details'][0]['date_from']))
        );
    }

    /**
     * @test
     */
    public function updating_a_member_rate_and_removing_an_data_in_details_should_soft_delete_the_row_in_member_rate_detail_table()
    {
        $created = $this->create();

        $build = $this->build();

        $removed = $created->details->pop();

        $build->details->pop();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertSoftDeleted('member_rate_detail', ['id' => $removed->id]);
    }

    /**
     * @test
     */
    public function updating_a_member_rate_and_adding_a_new_data_in_details_should_insert_a_new_row_in_member_rate_detail_table()
    {
        $created = $this->create();

        $added = Factory::build($this->factory('details'), [
            'date_from' => Carbon::now()->addYear()->toDateString(),
            'date_to' => Carbon::now()->addYear()->addDay()->toDateString()
        ]);

        $created->details->push($added);

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertDatabaseHas('member_rate_detail', array_merge(
            $data['details'][2],
            [
                'id' => ($data['details'][1]['id'] + 1),
                'date_from' => Carbon::parse($data['details'][2]['date_from'])->startOfDay()->toDateTimeString(),
                'date_to' => Carbon::parse($data['details'][2]['date_to'])->endOfDay()->toDateTimeString(),
            ]
        ));
    }

    private function uri()
    {
        return 'member-rate';
    }

    private function factory($key)
    {
        $factories = [
            'info' => 'member_rate',
            'details' => 'member_rate_detail'
        ];

        return $factories[$key];
    }

    protected function basicTransformer($model)
    {
        return [
            'id'     => $model->id,
            'name'   => $model->name,
            'memo'   => $model->memo ?? '',
            'status' => $model->presenter()->status
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'memo'          => $model->memo ?? '',
            'status'        => $model->status
        ];
    }

    protected function detailsTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'member_rate_head_id'   => $model->member_rate_head_id,
            'amount_from'           => $model->number()->amount_from,
            'amount_to'             => $model->number()->amount_to,
            'date_from'             => $model->date_from->toDateString(),
            'date_to'               => $model->date_to->toDateString(),
            'amount_per_point'      => $model->number()->amount_per_point,
            'discount'              => $model->number()->discount
        ];
    }

    private function format($model)
    {
        $info = $model->info->toArray();

        return [
            'info' => [
                'name'   => $info['name'],
                'memo'   => $info['memo'],
                'status' => $info['status'],
            ],
            'details' => array_map(function($item) {
                $data = array_only($item, [
                    'id',
                    'amount_from',
                    'amount_to',
                    'date_from',
                    'date_to',
                    'amount_per_point',
                    'discount',
                ]);

                $data['date_from'] = Carbon::parse($data['date_from'])->format('Y-m-d');
                $data['date_to'] = Carbon::parse($data['date_to'])->format('Y-m-d');

                return $data;
            }, $model->details->toArray())
        ];
    }

    private function build($data = [])
    {
        $rate = Factory::build($this->factory('info'), $data['info'] ?? []);

        $details = [];

        $multiplier = 1;

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::build($this->factory('details'), array_merge([
                'date_from' => $this->faker->dateTimeBetween($multiplier.' days', ($multiplier + 10).' days'),
                'date_to' => $this->faker->dateTimeBetween(($multiplier + 11).' days', ($multiplier + 20).' days')
            ], $data['details'] ?? []));

            $multiplier += 30;
        }

        return (object) [
            'info' => $rate,
            'details' => collect($details)
        ];
    }

    private function create($data = [])
    {
        $rate = Factory::create($this->factory('info'), $data['info'] ?? []);

        $details = [];

        $multiplier = 1;

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::create($this->factory('details'), array_merge([
                'date_from' => $this->faker->dateTimeBetween($multiplier.' days', ($multiplier + 10).' days'),
                'date_to' => $this->faker->dateTimeBetween(($multiplier + 11).' days', ($multiplier + 20).' days'),
                'member_rate_head_id' => $rate->id
            ], $data['details'] ?? []));

            $multiplier += 30;
        }

        return (object) [
            'info' => $rate,
            'details' => collect($details)
        ];
    }

    private function dummies($times = 4)
    {
        $collection = [];

        for ($i = 1; $i <= $times; $i++) {
            $collection[] = $this->create();
        }

        return collect($collection);
    }
}