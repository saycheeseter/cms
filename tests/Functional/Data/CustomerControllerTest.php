<?php

namespace Tests\Functional\Data;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Response;
use Laracasts\TestDummy\Factory;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Entities\User;
use Modules\Core\Enums\CustomerPricingType;
use Modules\Core\Enums\FlowType;
use Tests\Functional\FunctionalTestCase;
use Lang;

class CustomerControllerTest extends FunctionalTestCase
{
    /**
     * @test
     */
    public function accessing_the_customer_page_should_successfully_display_the_page()
    {
        $this->get($this->uri())
            ->assertStatus(Response::HTTP_OK)
            ->assertViewHasAll(['salesmen', 'permissions']);
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_without_a_filter_will_should_return_all_the_undeleted_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => []
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_code_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[3]->code,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'code'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => $this->collection(array_wrap($dummies[3]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_name_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[1]->name,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => $this->collection(array_wrap($dummies[1]), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_updated_at_filter_should_return_a_list_of_data_based_on_the_matched_value()
    {
        $dummies = $this->dummies()->pluck('info');

        sleep(60);

        $added = $this->create();

        $filters = [
            'filters' => [
                [
                    'value' => $added->info->updated_at->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND',
                    'column' => 'updated_at'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 1,
                            'count'        => 1,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => $this->collection(array_wrap($added->info), 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_exists_should_be_return_all_the_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => $dummies[2]->id,
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'id'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => $dummies->count(),
                            'count'        => $dummies->count(),
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => $this->collection($dummies, 'basicTransformer')
                ], '')
            );
    }

    /**
     * @test
     */
    public function accessing_the_index_resource_with_a_filter_that_doesnt_match_any_existing_data_should_return_an_empty_value()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'filters' => [
                [
                    'value' => 'Foo',
                    'operator' => '=',
                    'join' => 'AND',
                    'column' => 'name'
                ]
            ]
        ];

        $this->getJson($this->uri().'?'.http_build_query($filters))
            ->assertExactJson(
                $this->successfulResponse([
                    'meta'         => [
                        'pagination' => [
                            'total'        => 0,
                            'count'        => 0,
                            'per_page'     => 10,
                            'current_page' => 1,
                            'total_pages'  => 1,
                            'links'        => []
                        ]
                    ],
                    'customers' => []
                ], Lang::get('core::info.no.results.found'))
            );
    }

    /**
     * @test
     */
    public function storing_a_customer_with_an_empty_name_pricing_type_salesman_id_or_branch_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.name', '');
        array_set($data, 'info.pricing_type', '');
        array_set($data, 'info.salesman_id', '');
        array_set($data, 'details.0.name', '');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.name' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.name.required')),
                ],
                'info.pricing_type' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.type.required'))
                ],
                'info.salesman_id' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.salesman.required'))
                ],
                'details.0.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.required'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_an_empty_branch_detail_array_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'details', []);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.details.min.one')),
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.name', $existing->info->name);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.code.unique'))
                ],
                'info.name' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.name.unique'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_a_non_numeric_credit_limit_pricing_rate_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.credit_limit', 'FOO');
        array_set($data, 'info.pricing_rate', 'FOO');
        array_set($data, 'info.term', 'FOO');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.credit_limit' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.credit.limit.numeric'))
                ],
                'info.pricing_rate' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.rate.numeric'))
                ],
                'info.term' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.term.numeric'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_a_non_existing_pricing_type_option_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.pricing_type', (CustomerPricingType::PRICE_E + 1));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.pricing_type' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.type.invalid'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_a_non_existing_salesman_id_should_trigger_a_validation_and_return_an_error()
    {
        $users = User::all();

        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'info.salesman_id', ($users->last()->id + 1));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'info.salesman_id' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.salesman.doesnt.exists'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_the_same_branch_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details.1.name', array_get($data, 'details.0.name'));

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.array.unique'), 1)
                ],
                'details.1.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.array.unique'), 2)
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_the_an_invalid_branch_detail_email_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        unset($data['details'][1]);

        array_set($data, 'details.0.email', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.email' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.email.invalid'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_the_non_array_details_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', 'Foo');

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.invalid.format'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_customer()
    {
        $build = $this->build();

        $data = $this->format($build);

        $ids = [
            'customer' => 1,
            'customer_detail' => 1
        ];

        $expected = [
            'response' => [
                'customer' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $ids['customer']]
                )
            ],
            'database' => [
                'customer' => array_merge(
                    $data['info'],
                    ['id' => $ids['customer']]
                ),
                'customer_detail' => array_map(function($item) use (&$ids) {
                    $format = array_merge($item, [
                        'id' => ($ids['customer_detail'])
                    ]);

                    $ids['customer_detail']++;

                    return $format;
                }, $data['details'])
            ]
        ];

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ));

        $this->assertDatabaseHas('customer', $expected['database']['customer']);
        $this->assertDatabaseHave('customer_detail', $expected['database']['customer_detail']);
    }

    /**
     * @test
     */
    public function updating_a_customer_with_an_empty_code_name_pricing_type_salesman_id_or_branch_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.name', '');
        array_set($data, 'info.code', '');
        array_set($data, 'info.pricing_type', '');
        array_set($data, 'info.salesman_id', '');
        array_set($data, 'details.0.name', '');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.code.required')),
                ],
                'info.name' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.name.required')),
                ],
                'info.pricing_type' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.type.required'))
                ],
                'info.salesman_id' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.salesman.required'))
                ],
                'details.0.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.required'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_an_empty_branch_detail_array_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'details', []);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.details.min.one')),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_an_existing_code_or_name_should_trigger_a_validation_and_return_an_error()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.code', $existing->info->code);
        array_set($data, 'info.name', $existing->info->name);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.code' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.code.unique'))
                ],
                'info.name' => [
                    $this->validationMessageForCustomerDetail(Lang::get('core::validation.name.unique'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_a_non_numeric_credit_limit_pricing_rate_or_term_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.credit_limit', 'FOO');
        array_set($data, 'info.pricing_rate', 'FOO');
        array_set($data, 'info.term', 'FOO');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.credit_limit' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.credit.limit.numeric'))
                ],
                'info.pricing_rate' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.rate.numeric'))
                ],
                'info.term' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.term.numeric'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_a_non_existing_pricing_type_option_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.pricing_type', (CustomerPricingType::PRICE_E + 1));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.pricing_type' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.pricing.type.invalid'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_a_non_existing_salesman_id_should_trigger_a_validation_and_return_an_error()
    {
        $users = User::all();

        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'info.salesman_id', ($users->last()->id + 1));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'info.salesman_id' => [
                    $this->validationMessageForAdvanceInfo(Lang::get('core::validation.salesman.doesnt.exists'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_the_same_branch_detail_name_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'details.1.name', array_get($data, 'details.0.name'));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.array.unique'), 1)
                ],
                'details.1.name' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.name.array.unique'), 2)
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_the_an_invalid_branch_detail_email_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        unset($data['details'][1]);

        array_set($data, 'details.0.email', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details.0.email' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.email.invalid'), 1)
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_the_non_array_details_should_trigger_a_validation_and_return_an_error()
    {
        $created = $this->create();

        $data = $this->format($created);

        array_set($data, 'details', 'Foo');

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'details' => [
                    $this->validationMessageForCustomerBranches(Lang::get('core::validation.invalid.format'))
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_customer_with_a_correct_input_should_save_the_data_in_database_and_return_the_the_details_of_the_saved_customer()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $expected = [
            'response' => [
                'customer' => array_merge(
                    $this->item($build->info, 'basicTransformer'),
                    ['id' => $created->info->id]
                )
            ],
            'database' => [
                'customer' => array_merge(
                    $data['info'],
                    ['id' => $created->info->id]
                ),
                'customer_detail' => $data['details']
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));

        $this->assertDatabaseHas('customer', $expected['database']['customer']);
        $this->assertDatabaseHave('customer_detail', $expected['database']['customer_detail']);
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_a_non_existing_customer_should_trigger_a_validation_and_return_an_error()
    {
        $this->getJson($this->uri().'/'.rand(100, 500))
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_show_resource_with_an_existing_customer_should_return_the_details_of_the_specified_id()
    {
        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([
                'customer' => $this->item($created->info, 'infoTransformer'),
                'details'  => $this->collection($created->details, 'detailsTransformer')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_customer_should_trigger_a_validation_and_return_an_error()
    {
        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson($this->uri().'/5', $data)
            ->assertExactJson($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]));

        $this->assertDatabaseMissing('customer', ['id' => 5]);
    }

    /**
     * @test
     */
    public function deleting_a_customer_should_soft_delete_the_data_in_database_and_return_a_success_message()
    {
        $created = $this->create();

        $this->deleteJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->successfulResponse([], Lang::get('core::success.deleted')));

        $this->assertSoftDeleted('customer', ['id' => $created->info->id]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_customer_should_trigger_a_validation_and_return_an_error()
    {
        $this->deleteJson($this->uri().'/5')
            ->assertExactJson($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]));

        $this->assertDatabaseMissing('customer', ['id' => 5]);
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_valid_ids_should_return_the_corresponding_customers_data()
    {
        $dummies = $this->dummies()->pluck('info');

        $filters = [
            'ids' => $dummies->pluck('id')->toArray()
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->successfulResponse([
                'customers' => $this->collection($dummies, 'basicTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_find_many_resource_with_a_non_existing_ids_should_trigger_a_validation_and_return_an_error()
    {
        $filters = [
            'ids' => array_random(range(50, 100), 5)
        ];

        $this->getJson($this->uri() . '/m?' . http_build_query($filters))
            ->assertExactJson($this->errorResponse([
                'no.customers.found' => Lang::get('core::error.no.customers.found')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_branch_detail_of_a_non_existing_customer_should_return_an_error_message()
    {
        $this->getJson($this->uri() . '/1/details')
            ->assertExactJson($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_branch_detail_of_an_existing_customer_should_return_the_corresponding_label_and_id_of_the_branch_details()
    {
        $created = $this->create();

        $this->getJson($this->uri() . '/'.$created->info->id.'/details')
            ->assertExactJson($this->successfulResponse([
                'details' => $this->collection($created->details, 'detailChosenTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_customer_index_page_without_view_permission_should_redirect_the_user_to_page_not_found()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $this->get($this->uri())->assertViewIs('core::errors.permission');
    }

    /**
     * @test
     */
    public function accessing_the_customer_info_without_view_detail_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->getJson($this->uri().'/'.$created->info->id)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function storing_a_new_customer_without_create_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $build = $this->build();

        $data = $this->format($build);

        $this->postJson($this->uri(), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_customer_without_update_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $build = $this->build();

        $data = $this->format($build);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function deleting_an_existing_customer_without_delete_permission_should_return_an_unauthorized_error_message()
    {
        $user = Factory::create('user');

        $this->givePermissions($user->id, 1, ['login']);

        $this->login($user->id, 1);

        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri(), $created->info->id))
            ->assertExactJson($this->errorResponse([
                'action.failed' => Lang::get('core::error.unauthorized.action')
            ], 'You are not authorized to access this page.'));
    }

    /**
     * @test
     */
    public function updating_an_existing_customer_with_the_same_code_or_name_should_not_trigger_a_validation_and_should_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data)
            ->assertExactJson($this->successfulResponse(
                ['customer' => $this->item($created->info,'basicTransformer')],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function updating_a_customer_and_removing_an_data_in_details_should_soft_delete_the_row_in_customer_detail_table()
    {
        $created = $this->create();

        $build = $this->build();

        $removed = $created->details->pop();

        $build->details->pop();

        $data = $this->format($build);

        array_set($data, 'details', array_map(function($build, $created) {
            return array_merge(
                $build,
                ['id' => $created['id']]
            );
        }, $data['details'], $created->details->toArray()));

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertSoftDeleted('customer_detail', ['id' => $removed->id]);
    }

    /**
     * @test
     */
    public function updating_a_customer_and_adding_a_new_data_in_details_should_insert_a_new_row_in_customer_detail_table()
    {
        $created = $this->create();

        $added = Factory::build($this->factory('details'));

        $created->details->push($added);

        $data = $this->format($created);

        $this->patchJson(sprintf('%s/%s', $this->uri(), $created->info->id), $data);
        $this->assertDatabaseHas('customer_detail', array_merge(
            $data['details'][2],
            ['id' => ($data['details'][1]['id'] + 1)]
        ));
    }

    /**
     * @test
     */
    public function storing_a_customer_with_an_empty_code_should_be_allowed_and_should_auto_generate_a_value_based_on_the_current_customer_series()
    {
        $build = $this->build(['info' => [
            'code' => ''
        ]]);

        $map = array_search(get_class($build->info), Relation::morphMap());

        $series = ModuleSeries::where('type', $map)
            ->where('column', $build->info->series_column)
            ->first();

        $data = $this->format($build);

        $expected = [
            'database' => array_merge(
                $data['info'],
                [
                    'id' => 1,
                    'code' => $series->series
                ]
            )
        ];

        $this->postJson($this->uri(), $data);

        $this->assertDatabaseHas('customer', $expected['database']);
    }

    /**
     * @test
     */
    public function accessing_the_balance_info_of_a_non_existing_customer_should_return_an_error_message()
    {
        $this->getJson($this->uri().'/'.rand(100, 500).'/balance')
            ->assertExactJson($this->errorResponse([
                'customer.not.found' => Lang::get('core::error.customer.not.found')
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_balance_info_of_an_existing_customer_should_display_the_computed_amount_based_on_the_summation_of_customer_balance_flow()
    {
        $created = Factory::create($this->factory('info'));

        $balance1 = Factory::create('customer_balance_flow', [
            'customer_id' => $created->id,
            'flow_type' => FlowType::IN
        ]);

        $balance2 = Factory::create('customer_balance_flow', [
            'customer_id' => $created->id,
            'flow_type' => FlowType::IN
        ]);

        $balance3 = Factory::create('customer_balance_flow', [
            'customer_id' => $created->id,
            'flow_type' => FlowType::OUT
        ]);

        $sum = Decimal::create(0, setting('monetary.precision'));

        $sum = $balance1->amount
            ->add($balance2->amount)
            ->sub($balance3->amount);

        $this->getJson($this->uri().'/'.$created->id.'/balance')
            ->assertExactJson($this->successfulResponse([
                'balance' => [
                    'amount' => number_format((string) $sum, setting('monetary.precision'))
                ]
            ]));
    }

    private function uri()
    {
        return 'customer';
    }

    private function factory($key)
    {
        $factories = [
            'info' => 'customer',
            'details' => 'customer_detail'
        ];

        return $factories[$key];
    }

    protected function basicTransformer($model)
    {
        return [
            'id'       => $model->id,
            'code'     => $model->code,
            'name'     => $model->name,
            'salesman' => $model->salesman->full_name,
            'term'     => $model->term,
            'website'  => $model->website,
            'memo'     => $model->memo
        ];
    }

    protected function infoTransformer($model)
    {
        return [
            'id'                        => $model->id,
            'name'                      => $model->name,
            'code'                      => $model->code,
            'memo'                      => $model->memo,
            'credit_limit'              => $model->number()->credit_limit,
            'pricing_type'              => $model->pricing_type,
            'historical_change_revert'  => $model->historical_change_revert,
            'historical_default_price'  => $model->historical_default_price,
            'pricing_rate'              => $model->number()->pricing_rate,
            'salesman_id'               => $model->salesman_id,
            'website'                   => $model->website,
            'term'                      => $model->term,
            'status'                    => $model->status
        ];
    }

    protected function detailsTransformer($model)
    {
        return [
            'id'             => $model->id,
            'name'           => $model->name,
            'address'        => $model->address,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'contact_person' => $model->contact_person,
            'tin'            => $model->tin
        ];
    }

    protected function detailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }

    private function format($model)
    {
        $info = $model->info->toArray();

        return [
            'info' => [
                'code'                     => $info['code'],
                'name'                     => $info['name'],
                'memo'                     => $info['memo'],
                'credit_limit'             => $info['credit_limit'],
                'pricing_type'             => $info['pricing_type'],
                'pricing_rate'             => $info['pricing_rate'],
                'historical_change_revert' => $info['historical_change_revert'],
                'historical_default_price' => $info['historical_default_price'],
                'salesman_id'              => $info['salesman_id'],
                'website'                  => $info['website'],
                'term'                     => $info['term'],
                'status'                   => $info['status'],
            ],
            'details' => array_map(function($item) {
                return array_only($item, [
                    'id',
                    'name',
                    'address',
                    'chinese_name',
                    'owner_name',
                    'contact',
                    'landline',
                    'fax',
                    'mobile',
                    'email',
                    'contact_person',
                    'tin',
                ]);
            }, $model->details->toArray())
        ];
    }

    private function build($data = [])
    {
        $customer = Factory::build($this->factory('info'), $data['info'] ?? []);

        $details = [];

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::build($this->factory('details'), $data['details'] ?? []);
        }

        return (object) [
            'info' => $customer,
            'details' => collect($details)
        ];
    }

    private function create($data = [])
    {
        $customer = Factory::create($this->factory('info'), $data['info'] ?? []);

        $details = [];

        for ($i = 1; $i <= 2; $i++) {
            $details[] = Factory::create($this->factory('details'), array_merge(
                ['head_id' => $customer->id],
                $data['details'] ?? []
            ));
        }

        return (object) [
            'info' => $customer,
            'details' => collect($details)
        ];
    }

    private function dummies($times = 4)
    {
        $collection = [];

        for ($i = 1; $i <= $times; $i++) {
            $collection[] = $this->create();
        }

        return collect($collection);
    }

    private function validationMessageForCustomerDetail($message)
    {
        return sprintf('%s : %s',
            Lang::get('core::label.customer.detail'),
            $message
        );
    }

    private function validationMessageForAdvanceInfo($message)
    {
        return sprintf('%s : %s',
            Lang::get('core::label.advance.info'),
            $message
        );
    }

    private function validationMessageForCustomerBranches($message, $row = null)
    {
        if (is_null($row)) {
            return sprintf('%s : %s',
                Lang::get('core::label.customer.branches'),
                $message
            );
        }

        return sprintf('%s : %s[%s]. %s',
            Lang::get('core::label.customer.branches'),
            Lang::get('core::error.row'),
            $row,
            $message
        );
    }
}