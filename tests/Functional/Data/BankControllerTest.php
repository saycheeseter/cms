<?php

namespace Tests\Functional\Data;

class BankControllerTest extends SystemCodeControllerTest
{
    protected function uri()
    {
        return 'bank';
    }

    protected function factory()
    {
        return 'bank';
    }
}