<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\Assertions;
use Tests\Traits\Authentication;
use Tests\Traits\DataTransformer;
use Tests\Traits\GivesUserPermissions;
use Tests\Traits\JsonResponse;
use Tests\Traits\Migrations;
use Tests\Traits\PHPSettingsOverride;
use Tests\Traits\ResetTableIndex;
use Tests\Traits\SmartSeeder;
use Tests\Traits\UpdatesApplicationSettings;

class FunctionalTestCase extends TestCase
{
    use DatabaseTransactions,
        Assertions,
        Authentication,
        Migrations,
        SmartSeeder,
        JsonResponse,
        DataTransformer,
        UpdatesApplicationSettings,
        PHPSettingsOverride,
        GivesUserPermissions;
}