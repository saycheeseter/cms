<?php

$factory('Modules\Core\Entities\ExcelTemplate', 'excel_template', [
    'module' => $faker->unique()->numberBetween(10, 1000),
    'name' => $faker->unique()->name,
    'format' => '{}'
]);