<?php

$factory('Modules\Core\Entities\Permission', 'permission', [
    'module_id' => 'factory:permission_module',
    'name' => $faker->word,
    'alias' => $faker->word,
    'translation' => $faker->word,
    'sequence' => $faker->numberBetween(1, 1000)
]);