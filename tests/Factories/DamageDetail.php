<?php

$factory('Modules\Core\Entities\DamageDetail', 'damage_detail', [
    'transaction_id' => 'factory:damage',
    'product_id' => 'factory:product',
    'unit_id' => 'factory:uom',
    'unit_qty' => $faker->numberBetween(1, 50),
    'qty' => $faker->numberBetween(1, 50),
    'oprice' => $faker->numberBetween(1000, 10000),
    'price' => $faker->numberBetween(1000, 10000),
    'discount1' => 0,
    'discount2' => 0,
    'discount3' => 0,
    'discount4' => 0,
    'remarks' => $faker->sentence(),
    'group_type' => 0,
    'product_type' => 0,
    'manageable' => 0
]);
