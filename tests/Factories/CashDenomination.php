<?php
$factory('Modules\Core\Entities\CashDenomination', 'cash_denomination', [
    'terminal_number' => 1,
    'type' => 1,
    'create_date' =>  $faker->date(),
    'created_from' => 1,
    'created_by' => 1,
    'b1000' => $faker->numberBetween(1, 10),
    'b500' => $faker->numberBetween(1, 10),
    'b200' => $faker->numberBetween(1, 10),
    'b100' => $faker->numberBetween(1, 10),
    'b50' => $faker->numberBetween(1, 10),
    'b20' => $faker->numberBetween(1, 10),
    'c10' => $faker->numberBetween(1, 10),
    'c5' => $faker->numberBetween(1, 10),
    'c1' => $faker->numberBetween(1, 10),
    'c25c' => $faker->numberBetween(1, 10),
    'c10c' => $faker->numberBetween(1, 10),
    'c5c' => $faker->numberBetween(1, 10)
]);