<?php

$factory('Modules\Core\Entities\User', 'user', [
    'code' => $faker->unique()->numberBetween(10, 1000),
    'username' => $faker->unique()->userName,
    'password' => 'helloworld',
    'full_name' => $faker->unique()->name,
    'position' => $faker->jobTitle,
    'address' => $faker->streetAddress,
    'telephone' => $faker->phoneNumber,
    'mobile' => $faker->phoneNumber,
    'email' => $faker->email,
    'memo' => $faker->sentence,
    'type' => 1,
    'status' => 1,
    'created_from' => 1,
    'created_by' => 1,
    'role_id' => null
]);