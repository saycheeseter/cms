<?php

$factory('Modules\Core\Entities\Role', 'role', [
    'code' => $faker->unique()->numberBetween(10, 1000),
    'name' => $faker->unique()->userName,
]);