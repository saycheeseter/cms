<?php

$factory('Modules\Core\Entities\GenericSetting', 'generic_setting', [
    'key' => $faker->unique()->word(),
    'value' => $faker->word(),
    'type' => 'string',
    'comments' => $faker->sentence()
]);
