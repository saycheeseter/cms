<?php

$factory('Modules\Core\Entities\Counter', 'counter', [
    'created_from'     => 1,
    'created_by'       => 1,
    'created_for'      => 1,
    'customer_id'      => 'factory:customer',
    'transaction_date' => $faker->date(),
    'remarks'          => $faker->sentence()
]);
