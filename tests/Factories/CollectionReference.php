<?php

$factory('Modules\Core\Entities\CollectionReference', 'collection_reference', [
    'transaction_id' => 'factory:collection',
    'sales_outbound_id' => 'factory:sales_outbound',
    'collectible' => $faker->numberBetween(1000, 10000),
]);
