<?php

$factory('Modules\Core\Entities\Category', 'category', [
    'code' => $faker->unique()->numberBetween(1, 100000),
    'name' => $faker->unique()->name,
    'parent_id' => null,
    'created_by' => 1
]);