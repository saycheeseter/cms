<?php 

$factory('Modules\Core\Entities\CustomerDetail', 'customer_detail', [
    'head_id' => 'factory:customer',
    'name' => $faker->unique()->name,
    'address' => $faker->streetAddress,
    'chinese_name' => '',
    'owner_name' => $faker->name,
    'contact' => $faker->phoneNumber,
    'landline' => $faker->phoneNumber,
    'fax' => '',
    'mobile' => $faker->phoneNumber,
    'email' => $faker->email,
    'contact_person' => $faker->name, 
    'tin' => $faker->randomNumber
]);