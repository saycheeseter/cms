<?php

$factory('Modules\Core\Entities\ProductConversion', 'product_conversion', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'approval_status' => '1',
    'group_type' => 2,
    'product_id' => 'factory:product_group',
    'qty' => $faker->numberBetween(1, 50)
]);
