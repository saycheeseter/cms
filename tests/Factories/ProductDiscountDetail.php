<?php

$factory('Modules\Core\Entities\ProductDiscountDetail', 'product_discount_detail', [
    'product_discount_id' => 'factory:product_discount',
    'entity_type' => 1,
    'entity_id' => $faker->numberBetween(10, 1000),
    'discount_type' => 1,
    'discount_value' => $faker->numberBetween(10, 1000),
    'qty' => $faker->numberBetween(10, 1000),
]);