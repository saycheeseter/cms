<?php

$factory('Modules\Core\Entities\StockRequest', 'stock_request', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'request_to' => '1',
    'remarks' => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
]);
