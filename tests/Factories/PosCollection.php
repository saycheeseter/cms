<?php
$factory('Modules\Core\Entities\PosCollection', 'pos_collection', [
    'payment_method' => 2,
    'amount' => $faker->numberBetween(1, 50)
]);