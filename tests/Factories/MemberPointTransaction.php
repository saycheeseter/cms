<?php
$factory('Modules\Core\Entities\MemberPointTransaction', 'member_point_transaction', [
    'member_id' => 'factory:member',
    'reference_type' => $faker->numberBetween(1, 3),
    'reference_number' => $faker->numberBetween(100, 99999),
    'amount' => $faker->numberBetween(1, 50),
    'transaction_date' => date('Y-m-d', mt_rand(strtotime('1990-01-1'), strtotime(date('Y-m-d')))),
    'created_from' => 'factory:branch',
    'created_by' => 1
]);