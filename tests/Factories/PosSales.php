<?php
$factory('Modules\Core\Entities\PosSales', 'pos_sales_walk_in', [
    'cashier' => 1,
    'created_for' => 1,
    'for_location' => 1,
    'salesman_id' => 1,
    'transaction_date' => $faker->date(),
    'customer_type' => 0,
    'customer_walk_in_name' => $faker->name(),
    'total_amount' => $faker->numberBetween(1, 50),
    'transaction_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'or_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'terminal_number' => $faker->numberBetween(1, 5),
    'is_non_vat' => $faker->numberBetween(0, 1),
    'is_wholesale' => $faker->numberBetween(0, 1)
]);

$factory('Modules\Core\Entities\PosSales', 'pos_sales_customer', [
    'cashier' => 1,
    'customer_id' => 1,
    'customer_detail_id' => 1,
    'created_for' => 1,
    'for_location' => 1,
    'salesman_id' => 1,

    'transaction_date' => $faker->date(),
    'customer_type' => 1,

    'total_amount' => $faker->numberBetween(1, 50),
    'transaction_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'or_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'terminal_number' => $faker->numberBetween(1, 5),
    'is_non_vat' => $faker->numberBetween(0, 1),
    'is_wholesale' => $faker->numberBetween(0, 1)
]);

$factory('Modules\Core\Entities\PosSales', 'pos_sales_member', [
    'cashier' => 4,
    'created_for' => 2,
    'for_location' => 2,
    'salesman_id' => 4,
    'transaction_date' => '2005-11-26 00:00:00.000',
    'customer_type' => 1,

    'member_id' => 2,

    'total_amount' => 139998.000000,
    'transaction_number' => '1001010005',
    'or_number' => 'OR00100015',
    'terminal_number' => $faker->numberBetween(1, 5),
    'is_non_vat' => $faker->numberBetween(0, 1),
    'is_wholesale' => $faker->numberBetween(0, 1)
]);

$factory('Modules\Core\Entities\PosSales', 'pos_sales_senior', [
    'cashier' => 1,
    'created_for' => 1,
    'for_location' => 1,
    'salesman_id' => 1,
    'transaction_date' => $faker->date(),
    'customer_type' => 0,

    'senior_number' =>$faker->numberBetween(100101, 200202),
    'senior_name' => $faker->name(),

    'total_amount' => $faker->numberBetween(1, 50),
    'transaction_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'or_number' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'terminal_number' => $faker->numberBetween(1, 5),
    'is_non_vat' => $faker->numberBetween(0, 1),
    'is_wholesale' => $faker->numberBetween(0, 1)
]);