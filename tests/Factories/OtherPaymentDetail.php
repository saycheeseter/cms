<?php

$factory('Modules\Core\Entities\OtherPaymentDetail', 'other_payment_detail', [
    'transaction_id' => 'factory:other_income',
    'type' => 'factory:income_type',
    'amount' => $faker->randomFloat(2, 10, 1000),
]);