<?php
$factory('Modules\Core\Entities\PosSalesDetail', 'pos_sales_detail', [
    'transaction_id' => 3,
    'product_id' => 2,
    'unit_id' => 1,
    'unit_qty' => 1.000000,
    'qty' => 1.000000,
    'oprice' => 1.000000,
    'price' => 3.000000,
    'discount1' => 0.000000,
    'discount2' => 0.000000,
    'discount3' => 0.000000,
    'discount4' => 0.000000,
    'vat' => 0.360000,
    'is_senior' => 0,
]);