<?php

$factory('Modules\Core\Entities\ProductStockCard', 'stock_card', [
    'previous_id' => null,
    'created_for' => 'factory:branch',
    'for_location' => 'factory:branch_detail',
    'product_id' => 'factory:product',
    'sequence_no' => $faker->numberBetween(1, 10),
    'reference_type' => 1,
    'reference_id' => 'factory:purchase_inbound',
    'transaction_date' => $faker->dateTime(),
    'qty' => $faker->numberBetween(1, 999),
    'price' => $faker->numberBetween(1, 999),
    'amount' => $faker->numberBetween(1, 999),
    'cost' => $faker->numberBetween(1, 999),
    'pre_inventory' => $faker->numberBetween(1, 999),
    'pre_avg_cost' => $faker->numberBetween(1, 999),
    'pre_amount' => $faker->numberBetween(1, 999),
    'post_inventory' => $faker->numberBetween(1, 999),
    'post_avg_cost' => $faker->numberBetween(1, 999),
    'post_amount' => $faker->numberBetween(1, 999)
]);
