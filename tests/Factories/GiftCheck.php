<?php
$factory('Modules\Core\Entities\GiftCheck', 'gift_check', [
    'code' => $faker->numberBetween(100000000, 199999999),
    'check_number' => $faker->numberBetween(100000000, 199999999),
    'amount' => $faker->numberBetween(1, 99999),
    'date_from' => $faker->dateTimeBetween('-20 years', 'now'),
    'date_to' => $faker->dateTimeBetween('now', '+ 20 years'),
    'status' => 1,
    'created_from' => 1,
    'created_by' => 1
]);