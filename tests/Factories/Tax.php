<?php

$factory('Modules\Core\Entities\Tax', 'tax', [
    'name' => $faker->name,
    'percentage' =>  $faker->numberBetween(10, 100),
    'is_inclusive' => 1
]);
