<?php

$factory('Modules\Core\Entities\ProductDiscountTarget', 'product_discount_target', [
    'product_discount_id' => 'factory:product_discount',
    'target_id' => $faker->numberBetween(10, 1000),
    'target_type' => 1
]);