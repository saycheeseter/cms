<?php

$factory('Modules\Core\Entities\OtherIncomeDetail', 'other_income_detail', [
    'transaction_id' => 'factory:other_income',
    'type' => 'factory:income_type',
    'amount' => $faker->randomFloat(2, 10, 1000),
]);