<?php

$factory('Modules\Core\Entities\StockReturnDetail', 'stock_return_detail', [
    'transaction_id' => 'factory:stock_delivery',
    'product_id' => 'factory:product',
    'unit_id' => 'factory:uom',
    'unit_qty' => $faker->numberBetween(1, 50),
    'qty' => $faker->numberBetween(1, 50),
    'oprice' => $faker->numberBetween(1000, 10000),
    'price' => $faker->numberBetween(1000, 10000),
    'discount1' => 0,
    'discount2' => 0,
    'discount3' => 0,
    'discount4' => 0,
    'remarks' => $faker->sentence()
]);
