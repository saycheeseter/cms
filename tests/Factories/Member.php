<?php

$factory('Modules\Core\Entities\Member', 'member', [
    'rate_head_id' => 'factory:member_rate',
    'card_id' => $faker->unique()->numberBetween(100000000, 199999999),
    'barcode' => $faker->unique()->numberBetween(100000000, 199999999),
    'full_name' => $faker->unique()->name,
    'mobile' => $faker->phoneNumber,
    'telephone' => $faker->phoneNumber,
    'address' => $faker->streetAddress,
    'email' => $faker->email,
    'gender' => $faker->numberBetween(0, 1),
    'birth_date' => $faker->date('Y-m-d', '1990-01-01'),
    'registration_date' => date('Y-m-d', mt_rand(strtotime('1990-01-1'), strtotime(date('Y-m-d')))),
    'expiration_date'=> date('Y-m-d', mt_rand(strtotime(date('Y-m-d')), strtotime('2030-01-01'))),
    'status' => $faker->numberBetween(0, 1),
    'memo' => $faker->sentence,
    'created_from' => 'factory:branch',
    'created_by' => 1
]);