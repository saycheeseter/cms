<?php

$factory('Modules\Core\Entities\Product', 'product_group', [
    'stock_no' => $faker->numberBetween(10, 1000),
    'chinese_name' => $faker->firstName,
    'supplier_sku' => $faker->city,
    'name' => $faker->name,
    'status' => 1,
    'created_by' => 1,
    'created_from' => 1,
    'supplier_id' => 'factory:supplier',
    'brand_id' => 'factory:brand',
    'category_id' => 'factory:category',
    'senior' => 0,
    'manage_type' => 0,
    'type' => 2,
    'group_type' => 2,
    'manageable' => 1
]);
