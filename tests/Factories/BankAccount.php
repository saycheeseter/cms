<?php

$factory('Modules\Core\Entities\BankAccount', 'bank_account', [
    'created_from' => 'factory:branch',
    'bank_id'     => 'factory:bank',
    'account_number' => $faker->unique()->bankAccountNumber,
    'account_name' => $faker->name,
    'opening_balance' => $faker->numberBetween(1000, 10000)
]);
