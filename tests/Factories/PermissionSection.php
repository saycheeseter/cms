<?php

$factory('Modules\Core\Entities\PermissionSection', 'permission_section', [
    'name' => $faker->word,
    'alias' => $faker->word,
    'translation' => $faker->word
]);