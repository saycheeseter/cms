<?php

$factory('Modules\Core\Entities\Damage', 'damage', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'reason_id' => 'factory:reason',
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'remarks' => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'approval_status' => '1',
]);
