<?php

$factory('Modules\Core\Entities\PermissionModule', 'permission_module', [
    'section_id' => 'factory:permission_section',
    'name' => $faker->word,
    'alias' => $faker->word,
    'translation' => $faker->word
]);