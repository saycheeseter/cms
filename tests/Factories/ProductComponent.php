<?php

$factory('Modules\Core\Entities\ProductComponent', 'product_component', [
    'group_id' => 'factory:product_group',
    'component_id' => 'factory:component',
    'qty' => $faker->numberBetween(1, 50),
]);
