<?php

$factory('Modules\Core\Entities\Branch', 'branch', [
    'code' => $faker->unique()->numberBetween(10, 1000),
    'name' => $faker->unique()->city,
    'address' => $faker->streetAddress,
    'contact' => $faker->phoneNumber,
    'business_name' => $faker->name,
]);