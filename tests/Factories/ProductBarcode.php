<?php

$factory('Modules\Core\Entities\ProductBarcode', 'product_barcode', [
    'product_id' => 'factory:product',
    'uom_id'     => 1,
    'code'     => $faker->numberBetween(1000, 1000000000),
    'memo' => $faker->name
]);
