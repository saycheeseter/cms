<?php

$factory('Modules\Core\Entities\PaymentDetail', 'payment_detail', [
    'transaction_id' => 'factory:payment',
    'payment_method' => '2',
    'amount' => $faker->numberBetween(1000, 10000),
]);
