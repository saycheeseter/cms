<?php

$factory('Modules\Core\Entities\CounterDetail', 'counter_detail', [
    'transaction_id' => 'factory:counter',
    'reference_id' => 'factory:sales_outbound',
    'remaining_amount' => $faker->numberBetween(1, 50)
]);
