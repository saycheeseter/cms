<?php

$factory('Modules\Core\Entities\PurchaseReturnOutbound', 'purchase_return_outbound', [
    'created_from'     => '1',
    'created_by'       => '1',
    'modified_by'      => '1',
    'audited_by'       => '1',
    'supplier_id'      => 'factory:supplier',
    'payment_method'   => 'factory:payment_method',
    'requested_by'     => '1',
    'created_for'      => '1',
    'for_location'     => '1',
    'transaction_type' => '0',
    'term'             => $faker->numberBetween(1, 1000),
    'remarks'          => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date'     => $faker->dateTime(),
    'approval_status'  => '1',
    'reference_id'     => null,
]);
