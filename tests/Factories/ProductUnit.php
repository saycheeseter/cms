<?php

$factory('Modules\Core\Entities\ProductUnit', 'product_unit', [
    'product_id' => 'factory:product',
    'uom_id'     => 1,
    'qty'     => $faker->numberBetween(10, 999),
    'length' => $faker->numberBetween(10, 999),
    'width' => $faker->numberBetween(10, 999),
    'height' => $faker->numberBetween(10, 999),
    'weight' => $faker->numberBetween(10, 999),
    'default' => 1
]);
