<?php

$factory('Modules\Core\Entities\ProductBranchSummary', 'product_branch_summary', [
    'product_id' => 'factory:product',
    'branch_id' => 1,
    'branch_detail_id' => 1,
    'qty' => $faker->numberBetween(1, 10000),
    'reserved_qty' => 0,
    'requested_qty' => 0,
    'current_cost' => 0
]);
