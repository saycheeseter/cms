<?php

$factory('Modules\Core\Entities\ReportBuilder', 'report_builder', [
    'group_id' => $faker->unique()->numberBetween(1, 100),
    'name' => $faker->unique()->name,
    'format' => array()
]);