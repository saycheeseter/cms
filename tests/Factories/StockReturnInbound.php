<?php

$factory('Modules\Core\Entities\StockReturnInbound', 'stock_return_inbound', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'delivery_from' => null,
    'delivery_from_location' => null,
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'transaction_type' => '0',
    'remarks' => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'approval_status' => '1',
    'reference_id' => null,
]);
