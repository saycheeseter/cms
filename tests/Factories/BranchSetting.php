<?php

$factory('Modules\Core\Entities\BranchSetting', 'branch_setting', [
    'branch_id' => 'factory:branch',
    'key' => $faker->unique()->word(),
    'value' => $faker->word(),
    'type' => 'string',
    'comments' => $faker->sentence()
]);
