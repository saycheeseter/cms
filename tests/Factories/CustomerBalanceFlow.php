<?php

$factory('Modules\Core\Entities\CustomerBalanceFlow', 'customer_balance_flow', [
    'customer_id' => 'factory:customer',
    'transactionable_id' => 'factory:collection',
    'transactionable_type' => 'collection',
    'flow_type' => 0,
    'transaction_date' => $faker->dateTime(),
    'amount' => $faker->numberBetween(1000, 10000),
]);