<?php

$factory('Modules\Core\Entities\Sales', 'sales', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'salesman_id' => 'factory:user',
    'customer_id' => null,
    'customer_detail_id' => null,
    'customer_type' => 0,
    'customer_walk_in_name' => $faker->name,
    'created_for' => '1',
    'for_location' => '1',
    'remarks' => $faker->sentence(),
    'requested_by' => 'factory:user',
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'term' => $faker->numberBetween(1, 1000),
    'approval_status' => '1',
]);
