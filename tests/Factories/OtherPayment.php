<?php

$factory('Modules\Core\Entities\OtherPayment', 'other_payment', [
    'reference_name' => $faker->name,
    'transaction_date' => $faker->dateTime(),
    'payment_method' => 'factory:payment_method',
    'remarks' => $faker->sentence,
    'created_from' => 1,
    'created_by' => 1
]);