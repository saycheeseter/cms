<?php

$factory('Modules\Core\Entities\ProductDiscount', 'product_discount', [
    'code' => $faker->numberBetween(10, 1000),
    'name' => $faker->firstName,
    'valid_from' => $faker->date(),
    'valid_to' => $faker->date(),
    'status' => $faker->numberBetween(0, 1),
    'mon' => $faker->numberBetween(0, 1),
    'tue' => $faker->numberBetween(0, 1),
    'wed' => $faker->numberBetween(0, 1),
    'thur' => $faker->numberBetween(0, 1),
    'fri' => $faker->numberBetween(0, 1),
    'sat' => $faker->numberBetween(0, 1),
    'sun' => $faker->numberBetween(0, 1),
    'target_type' => 1,
    'discount_scheme' => 1,
    'select_type' => 1,
    'created_for' => 1,
    'created_from' => 1,
    'created_by' => 1,
    'status' => 1
]);