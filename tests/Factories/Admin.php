<?php

$factory('Modules\Core\Entities\User', 'admin', [
    'code' => $faker->numberBetween(10, 1000),
    'username' => 'admin',
    'password' => 'admin123',
    'full_name' => 'admin',
    'position' => '',
    'type' => 1,
    'status' => 1,
]);