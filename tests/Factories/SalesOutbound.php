<?php

$factory('Modules\Core\Entities\SalesOutbound', 'sales_outbound', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'salesman_id' => 'factory:user',
    'customer_id' => null,
    'customer_detail_id' => null,
    'customer_type' => 0,
    'customer_walk_in_name' => $faker->name,
    'term' => $faker->numberBetween(1, 1000),
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'transaction_type' => '0',
    'remarks' => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'approval_status' => '1',
    'reference_id' => null,
]);
