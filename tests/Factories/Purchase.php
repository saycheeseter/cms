<?php

$factory('Modules\Core\Entities\Purchase', 'purchase', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'supplier_id' => 'factory:supplier',
    'requested_by' => '1',
    'payment_method' => 'factory:payment_method',
    'created_for' => '1',
    'for_location' => '1',
    'deliver_to' => '1',
    'remarks' => $faker->sentence(),
    'deliver_until' => $faker->date(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'term' => $faker->numberBetween(1, 1000),
    'approval_status' => '1',
]);
