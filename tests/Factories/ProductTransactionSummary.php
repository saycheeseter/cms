<?php

$factory('Modules\Core\Entities\ProductTransactionSummary', 'transaction_summary', [
    'product_id' => 'factory:product',
    'created_for' => 'factory:branch',
    'for_location' => 'factory:branch_detail',
    'purchase_inbound' => $faker->numberBetween(1, 999),
    'stock_delivery_inbound' => $faker->numberBetween(1, 999),
    'stock_return_inbound' => $faker->numberBetween(1, 999),
    'sales_return_inbound' => $faker->numberBetween(1, 999),
    'adjustment_increase' => $faker->numberBetween(1, 999),
    'purchase_return_outbound' => $faker->numberBetween(1, 999),
    'stock_delivery_outbound' => $faker->numberBetween(1, 999),
    'stock_return_outbound' => $faker->numberBetween(1, 999),
    'sales_outbound' => $faker->numberBetween(1, 999),
    'damage_outbound' => $faker->numberBetween(1, 999),
    'adjustment_decrease' => $faker->numberBetween(1, 999),
    'transaction_date' => $faker->date(),
]);
