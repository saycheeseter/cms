<?php

$factory('Modules\Core\Entities\MemberRate', 'member_rate', [
    'name' => $faker->unique()->name,
    'memo' => $faker->sentence,
    'status' => 1,
    'created_from' => 1,
    'created_by' => 1
]);