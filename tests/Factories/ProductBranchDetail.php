<?php

$factory('Modules\Core\Entities\ProductBranchDetail', 'product_branch_detail', [
    'product_id'       => 'factory:product',
    'branch_detail_id' => 'factory:branch_detail',
    'min'              => $faker->numberBetween(1, 100),
    'max'              => $faker->numberBetween(101, 200)
]);
