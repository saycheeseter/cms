<?php

$factory('Modules\Core\Entities\UserDefinedField', 'udf', [
    'module_id' => $faker->numberBetween(1, 18),
    'label  ' => $faker->word(),
    'type' => $faker->numberBetween(1, 5),
    'visible' => $faker->numberBetween(0, 1),
]);
