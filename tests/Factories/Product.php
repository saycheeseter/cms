<?php

$factory('Modules\Core\Entities\Product', 'product', [
    'stock_no' => $faker->numberBetween(10, 1000),
    'chinese_name' => $faker->firstName,
    'supplier_sku' => $faker->city,
    'name' => $faker->name,
    'status' => 1,
    'created_by' => 1,
    'created_from' => 1,
    'supplier_id' => 1,
    'brand_id' => 1,
    'category_id' => 1,
    'senior' => 0,
    'manage_type' => 0,
    'type' => 1,
    'group_type' => 1,
    'manageable' => 1
]);
