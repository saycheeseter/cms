<?php

$factory('Modules\Core\Entities\Bank', 'bank', [
    'code' => $faker->unique()->numberBetween(10, 1000),
    'name' => $faker->unique()->name,
    'remarks' => $faker->sentence($nbWords = 6, $variableNbWords = true)
]);
