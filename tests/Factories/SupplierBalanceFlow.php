<?php

$factory('Modules\Core\Entities\SupplierBalanceFlow', 'supplier_balance_flow', [
    'supplier_id' => 'factory:supplier',
    'transactionable_id' => 'factory:payment',
    'transactionable_type' => 'payment',
    'flow_type' => 0,
    'transaction_date' => $faker->dateTime(),
    'amount' => $faker->numberBetween(1000, 10000),
]);