<?php

$factory('Modules\Core\Entities\Collection', 'collection', [
    'created_from' => '1',
    'created_by' => '1',
    'created_for' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'customer_id' => 'factory:customer',
    'customer_type' => 1,
    'audited_date' => $faker->dateTime(),
    'transaction_date' => $faker->dateTime(),
    'approval_status' => '1',
    'remarks' => $faker->sentence(),
]);
