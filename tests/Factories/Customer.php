<?php 

$factory('Modules\Core\Entities\Customer', 'customer', [
    'name' => $faker->unique()->name,
    'code' => $faker->unique()->numberBetween(10, 1000),
    'memo' => $faker->sentence(),
    'credit_limit' => $faker->numberBetween(10, 1000),
    'pricing_type' => $faker->numberBetween(1, 10),
    'historical_change_revert' => 1,
    'historical_default_price' => 0,
    'pricing_rate' => $faker->numberBetween(1, 10),
    'salesman_id' => 1,
    'website' => $faker->sentence(),
    'term' => $faker->numberBetween(10, 1000),
    'status' => 1,
    'created_from' => 1,
    'created_by' => 1
]);