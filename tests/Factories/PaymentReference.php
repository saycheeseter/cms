<?php

$factory('Modules\Core\Entities\PaymentReference', 'payment_reference', [
    'transaction_id' => 'factory:payment',
    'purchase_inbound_id' => 'factory:purchase_inbound',
    'payable' => $faker->numberBetween(1000, 10000),
]);
