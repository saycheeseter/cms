<?php

$factory('Modules\Core\Entities\Supplier', 'supplier', [
    'code' => $faker->unique()->numberBetween(10, 1000),
    'full_name' => $faker->unique()->name,
    'chinese_name' => $faker->name,
    'owner_name' => $faker->name,
    'contact' => $faker->numberBetween(1000, 100000),
    'address' => $faker->address,
    'landline' => $faker->phoneNumber,
    'telephone' => $faker->phoneNumber,
    'fax' => '',
    'mobile' => '',
    'email' => '',
    'website' => '',
    'term' => '1',
    'status' => '1',
    'contact_person' => $faker->name,
    'memo' => $faker->sentence,
    'created_from' => 'factory:branch',
    'created_by' => '1',
    'modified_by' => '1'
]);