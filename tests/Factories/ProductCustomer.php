<?php

$factory('Modules\Core\Entities\ProductCustomer', 'product_customer', [
    'customer_id' => 'factory:customer',
    'product_id' => 'factory:product',
    'latest_unit_id' => 'factory:uom',
    'latest_unit_qty' => $faker->numberBetween(1, 100),
    'highest_price' => $faker->numberBetween(1000, 10000),
    'lowest_price' => $faker->numberBetween(1, 999),
    'latest_price' => $faker->numberBetween(1, 10000),
    'latest_fields_updated_date' => $faker->dateTime(),
    'remarks' => null,
]);
