<?php

$factory('Modules\Core\Entities\BranchPrice', 'branch_price', [
    'branch_id'       => 'factory:branch',
    'product_id'      => 'factory:product',
    'purchase_price'  => $faker->numberBetween(100, 999),
    'selling_price'   => $faker->numberBetween(100, 999),
    'wholesale_price' => $faker->numberBetween(100, 999),
    'price_a'         => $faker->numberBetween(100, 999),
    'price_b'         => $faker->numberBetween(100, 999),
    'price_c'         => $faker->numberBetween(100, 999),
    'price_d'         => $faker->numberBetween(100, 999),
    'price_e'         => $faker->numberBetween(100, 999),
    'editable'        => 1,
    'copy_from'       => null
]);
