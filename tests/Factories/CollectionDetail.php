<?php

$factory('Modules\Core\Entities\CollectionDetail', 'collection_detail', [
    'transaction_id' => 'factory:collection',
    'payment_method' => '2',
    'amount' => $faker->numberBetween(1000, 10000),
]);
