<?php

$factory('Modules\Core\Entities\ProductBranchInfo', 'product_branch_info', [
    'product_id' => 'factory:product',
    'branch_id' =>  'factory:branch',
    'status' => 1
]);
