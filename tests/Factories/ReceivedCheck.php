<?php

$factory('Modules\Core\Entities\ReceivedCheck', 'received_check', [
    'created_from' => 1,
    'created_by' => 1,
    'transactionable_id' => 'factory:collection_detail',
    'transactionable_type' => 'collection_detail',
    'bank_account_id' => 'factory:bank_account',
    'check_no' => $faker->sentence($nbWords = 1, $variableNbWords = true),
    'check_date' => $faker->date(),
    'amount' => $faker->numberBetween(1, 50),
    'transaction_date' => $faker->date(),
    'remarks' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'transfer_date' => $faker->date(),
    'status' => 2,
]);