<?php

$factory('Modules\Core\Entities\IncomeType', 'income_type', [
    'code' => $faker->numberBetween(10, 1000),
    'name' => $faker->name,
    'remarks' => $faker->sentence($nbWords = 6, $variableNbWords = true)
]);