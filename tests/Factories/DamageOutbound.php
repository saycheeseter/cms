<?php

$factory('Modules\Core\Entities\DamageOutbound', 'damage_outbound', [
    'created_from' => '1',
    'created_by' => '1',
    'modified_by' => '1',
    'audited_by' => '1',
    'reason_id' => 'factory:reason',
    'requested_by' => '1',
    'created_for' => '1',
    'for_location' => '1',
    'transaction_type' => '0',
    'remarks' => $faker->sentence(),
    'transaction_date' => $faker->dateTime(),
    'audited_date' => $faker->dateTime(),
    'approval_status' => '1',
    'reference_id' => null,
]);
