<?php

$factory('Modules\Core\Entities\MemberRateDetail', 'member_rate_detail', [
    'member_rate_head_id' => 'factory:member_rate',
    'amount_from' => $faker->randomFloat(2, 10, 500),
    'amount_to' => $faker->randomFloat(2, 501, 1000),
    'date_from' => $faker->dateTimeBetween('-1 month', 'now'),
    'date_to' => $faker->dateTimeBetween('now', '+1 month'),
    'amount_per_point' => $faker->numberBetween(10, 50),
    'discount' => $faker->numberBetween(10, 100)
]);