<?php

$factory('Modules\Core\Entities\InventoryAdjustDetail', 'inventory_adjust_detail', [
    'transaction_id' => 'factory:inventory_adjust',
    'product_id' => 'factory:product',
    'unit_id' => 'factory:uom',
    'unit_qty' => $faker->numberBetween(1, 50),
    'pc_qty' => $faker->numberBetween(1, 50),
    'qty' => $faker->numberBetween(1, 50),
    'price' => $faker->numberBetween(1000, 10000),
    'inventory' => $faker->numberBetween(1000, 10000),
    'remarks' => $faker->sentence(),
]);
