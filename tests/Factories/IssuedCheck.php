<?php

$factory('Modules\Core\Entities\IssuedCheck', 'issued_check', [
    'created_from' => 1,
    'created_by' => 1,
    'transactionable_id' => 'factory:payment_detail',
    'transactionable_type' => 'payment_detail',
    'bank_account_id' => 'factory:bank_account',
    'check_no' => $faker->sentence($nbWords = 1, $variableNbWords = true),
    'check_date' => $faker->date(),
    'amount' => $faker->numberBetween(1, 50),
    'transaction_date' => $faker->date(),
    'remarks' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'transfer_date' => $faker->date(),
    'status' => 2,
]);