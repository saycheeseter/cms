<?php

$factory('Modules\Core\Entities\ProductConversionDetail', 'product_conversion_detail', [
    'transaction_id' => 'factory:product_conversion',
    'component_id' => 'factory:component',
    'required_qty' => $faker->numberBetween(1, 50)
]);
