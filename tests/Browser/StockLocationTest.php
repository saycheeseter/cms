<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\StockLocation;
use Illuminate\Support\Str;
use Lang;

class StockLocationTest extends DuskTestCase
{
    private $factory = 'stock_location';

    public function setUp()
    {
        parent::setUp();

        $this->branch();
    }

    /**
     * @test
     */
    public function accessing_stock_location_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/stock-location')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function stock_location_page_should_display_by_default_the_list_of_branch_and_its_detail()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->assertVisible('@tree.branch.first')
                    ->assertVisible('@tree.branch.first.detail.first')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_branch_node_will_not_display_any_data()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->click('@tree.branch.first')
                    ->pause(2000)
                    ->assertMissing('@table')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_branch_detail_node_will_display_the_table_and_its_list_of_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(2000)
                    ->assertVisible('@table')
                    ->assertVisible('@table.first.code')
                    ->assertVisible('@table.second.code')
                    ->assertVisible('@table.third.code')
                    ->assertVisible('@table.fourth.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_branch_node_after_clicking_the_branch_detail_node_will_hide_the_table_and_its_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(2000)
                    ->assertVisible('@table')
                    ->click('@tree.branch.first')
                    ->assertMissing('@table')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_filters_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filter.code', 'Code')
                    ->assertSeeIn('@filter.name', 'Name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.code')
                    ->click('@filter.select')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[2]->code)
                    ->click('@filter.search')
                    ->pause(3000)
                    ->assertInputValue('@table.first.code', $data[2]->code)
                    ->assertInputValue('@table.first.name', $data[2]->name)
                    ->assertInputValue('@table.first.sequence.no', $data[2]->sequence_number)
                    ->assertInputValue('@table.second.code', '')
                    ->assertMissing('@table.third.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.select')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[1]->name)
                    ->click('@filter.search')
                    ->pause(3000)
                    ->assertInputValue('@table.first.code', $data[1]->code)
                    ->assertInputValue('@table.first.name', $data[1]->name)
                    ->assertInputValue('@table.first.sequence.no', $data[1]->sequence_number)
                    ->assertInputValue('@table.second.code', '')
                    ->assertMissing('@table.third.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.select')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', 'SL 99')
                    ->click('@filter.search')
                    ->pause(3000)
                    ->assertInputValue('@table.first.code', '')
                    ->assertInputValue('@table.first.name', '')
                    ->assertInputValue('@table.first.sequence.no', '')
                    ->assertMissing('@table.second.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_a_non_numeric_value_in_sequence_no_is_not_allowed() 
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.first.sequence.no', 'H')
                    ->assertInputValue('@table.first.sequence.no', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_code_name_or_sequence_no_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.required'),
                        Lang::get('core::validation.name.required'),
                        Lang::get('core::validation.sequence.number.required'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_code_name_or_sequence_no_will_trigger_an_error()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->pause(3000)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.second.code', $data->code)
                    ->type('@table.second.name', $data->name)
                    ->type('@table.second.sequence.no', $data->sequence_number)
                    ->click('@table.second.action')
                    ->pause(3000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.unique'),
                        Lang::get('core::validation.name.unique'),
                        Lang::get('core::validation.sequence.number.unique'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_and_return_a_success_message()
    {
        $data = $this->build();

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->type('@table.first.sequence.no', $data->sequence_number)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertInputValue('@table.first.sequence.no', $data->sequence_number)
                    ->assertVisible('@table.second.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_code_name_or_sequence_no_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->clear('@table.first.code')
                    ->clear('@table.first.name')
                    ->clear('@table.first.sequence.no')
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.required'),
                        Lang::get('core::validation.name.required'),
                        Lang::get('core::validation.sequence.number.required'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_code_name_or_sequence_no_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.first.code', $data[1]->code)
                    ->type('@table.first.name', $data[1]->name)
                    ->type('@table.first.sequence.no', $data[1]->sequence_number)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.unique'),
                        Lang::get('core::validation.name.unique'),
                        Lang::get('core::validation.sequence.number.unique'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_proper_format_will_successfully_update_the_data_and_return_a_success_message()
    {   
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->type('@table.first.sequence.no', $data->sequence_number)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertInputValue('@table.first.sequence.no', $data->sequence_number)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function created_data_on_one_branch_detail_should_not_appear_at_the_other_branch_detail()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->type('@table.first.sequence.no', $data->sequence_number)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->click('@tree.branch.first.detail.second')
                    ->pause(3000)
                    ->assertInputValue('@table.first.code', '')
                    ->assertInputValue('@table.first.name', '')
                    ->assertInputValue('@table.first.sequence.no', '')
                    ->assertMissing('@table.second.code')
                    ->assertMissing('@table.second.name')
                    ->assertMissing('@table.second.sequence.no')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirm_message_should_appear_upon_clicking_the_delete_icon()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertVisible('@confirm')
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.stock.location')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.stock.location'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_and_clicking_ok_button_will_delete_the_data_and_return_a_success_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertInputValueIsNot('@table.first.code', $data[0]->code)
                    ->assertInputValueIsNot('@table.first.name', $data[0]->name)
                    ->assertInputValueIsNot('@table.first.sequence.no', $data[0]->sequence_number)
                    ->assertInputValue('@table.first.code', $data[1]->code)
                    ->assertInputValue('@table.first.name', $data[1]->name)
                    ->assertInputValue('@table.first.sequence.no', $data[1]->sequence_number)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_row_of_the_data_will_return_a_no_results_found_message()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertInputValue('@table.first.code', '')
                    ->assertInputValue('@table.first.name', '')
                    ->assertInputValue('@table.first.sequence.no', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_and_clicking_cancel_button_will_not_delete_the_data()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new StockLocation)
                    ->click('@tree.branch.first.detail.first')
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->click('@confirm.cancel')
                    ->pause(1000)
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertInputValue('@table.first.sequence.no', $data->sequence_number)
                    ->assertMissing('@table.third.code')
                    ->logout();
        });
    }

    private function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'created_from' => 1,
            'branch_detail_id' => 1
        ));
    }

    private function build($data = [])
    {
        return Factory::build($this->factory, array_merge(array(
            'created_from' => 1,
            'branch_detail_id' => 1
        ), $data));
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, array_merge(array(
            'created_from' => 1,
            'branch_detail_id' => 1
        ), $data));
    }

    private function branch()
    {
        Factory::create('branch_detail', array(
            'branch_id' => 1
        ));
    }
}
