<?php

namespace Tests;

use Laravel\Dusk\Browser;
use Tests\Browser\Traits\Interaction;
use Tests\Browser\Traits\MakeAssertions;
use Tests\Browser\Traits\Authentication;

class NSIBrowser extends Browser
{
    use Interaction,
        MakeAssertions,
        Authentication;

    protected $userId = 1;
    protected $branchId = 1;
    protected $guard = null;
    
    /**
     * Set default user for authentication
     * 
     * @param  int $id
     * @return $this    
     */
    public function user($id)
    {
        $this->userId = $id;

        return $this;
    }

    /**
     * Set default branch for authentication
     * 
     * @param  int $id
     * @return $this    
     */
    public function branch($id)
    {
        $this->branchId = $id;

        return $this;
    }

    /**
     * Set default guard for authentication
     * 
     * @param  string $guard
     * @return $this    
     */
    public function guard($guard)
    {
        $this->guard = $guard;

        return $this;
    }
}