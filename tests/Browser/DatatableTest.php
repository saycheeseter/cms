<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Datatable;
use Illuminate\Support\Str;
use Lang;

class DatatableTest extends DuskTestCase
{

    private $factory = 'brand';

    /**
     * @test
     */
    public function visiting_specific_page_will_display_pagination_details()
    {
        $this->collection(100);

        $data = $this->paginationData(100);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->assertInputValue('@pagination.per', $data['per'])
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->assertInputValue('@pagination.current', '1')
                    ->assertVisible('@pagination.previous')
                    ->assertVisible('@pagination.next')
                    ->pause(2000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function table_with_no_data_will_return_default_pagination_values()
    {
        $data = $this->paginationData(0);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->assertInputValue('@pagination.current', '1')
                    ->pause(2000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function data_added_that_do_not_passes_the_validation_will_not_update_pagination_data()
    {
        $this->collection(4);

        $data = $this->paginationData(4, 4);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.per', '4')
                    ->keys('@pagination.per', '{enter}')
                    ->pause(2000)
                    ->type('@fifth.row.code', '')
                    ->type('@fifth.row.name', 'Foo')
                    ->type('@fifth.row.remarks', 'Lorem')
                    ->click('@fifth.row.command')
                    ->pause(3000)
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->assertInputValue('@pagination.current', '1')
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function data_added_that_passes_the_validation_and_did_not_maxed_record_per_page_will_not_update_pagination_data()
    {
        $this->collection(4);

        $data = $this->paginationData(5, 5);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.per', $data['per'])
                    ->keys('@pagination.per', '{enter}')
                    ->pause(2000)
                    ->type('@fifth.row.code', '1')
                    ->type('@fifth.row.name', 'Foo')
                    ->type('@fifth.row.remarks', 'Lorem')
                    ->click('@fifth.row.command')
                    ->pause(3000)
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->assertInputValue('@pagination.current', $data['last'])
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function data_added_that_passes_the_validation_and_maxed_the_total_number_of_record_per_page_will_update_pagination_data_and_redirect_to_the_last_page()
    {
        $this->collection(12);

        $data = $this->paginationData(13, 4);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.per', $data['per'])
                    ->keys('@pagination.per', '{enter}')
                    ->pause(2000)
                    ->type('@fifth.row.code', '1')
                    ->type('@fifth.row.name', 'Foo')
                    ->type('@fifth.row.remarks', 'Lorem')
                    ->click('@fifth.row.command')
                    ->pause(3000)
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->assertInputValue('@pagination.current', $data['last'])
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function updating_record_per_page_setting_will_update_total_number_of_pages()
    {
        $this->collection(100);

        $data = $this->paginationData(100, 3);

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.per', $data['per'])
                    ->keys('@pagination.per', '{enter}')
                    ->pause(3000)
                    ->assertSeeIn('@pagination.total',  $data['total'])
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function encoding_a_page_number_greater_than_total_number_of_pages_will_trigger_invalid_page_message()
    {
        $this->collection();

        $data = $this->paginationData();

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.current','2')
                    ->keys('@pagination.current','{enter}')
                    ->pause(3000)
                    ->assertSee('Invalid Page')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function clicking_next_page_when_on_the_last_page_will_trigger_invalid_page_message()
    {
        $this->collection();

        $data = $this->paginationData();

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->click('@pagination.next')
                    ->pause(3000)
                    ->assertSee('Invalid Page')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function clicking_previous_page_when_on_the_first_page_will_trigger_invalid_page_message()
    {
        $this->collection();

        $data = $this->paginationData();

        $this->browse(function ($browser) use($data){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->click('@pagination.previous')
                    ->pause(3000)
                    ->assertSee('Invalid Page')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function clicking_next_page_will_update_current_page_field_and_change_list_of_displayed_data()
    {
        $record = $this->collection(100);

        $data = $this->paginationData(100);

        $this->browse(function ($browser) use($data, $record){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->click('@pagination.next')
                    ->pause(3000)
                    ->assertDontSee('Invalid Page')
                    ->assertInputValue('@pagination.current', '2')
                    ->assertInputValue('@first.row.code', $record[10]->code)
                    ->assertInputValue('@first.row.name', $record[10]->name)
                    ->assertInputValue('@first.row.remarks', $record[10]->remarks)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function clicking_previous_page_will_update_current_page_field_and_change_list_of_displayed_data()
    {
        $record = $this->collection(100);

        $data = $this->paginationData(100);

        $this->browse(function ($browser) use($data, $record){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->click('@pagination.next')
                    ->pause(3000)
                    ->click('@pagination.previous')
                    ->pause(3000)
                    ->assertDontSee('Invalid Page')
                    ->assertInputValue('@pagination.current', '1')
                    ->assertInputValue('@first.row.code', $record[0]->code)
                    ->assertInputValue('@first.row.name', $record[0]->name)
                    ->assertInputValue('@first.row.remarks', $record[0]->remarks)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
    * @test
    */
    public function updating_current_page_field_and_hitting_enter_key_will_change_list_of_displayed_data()
    {
        $record = $this->collection(100);

        $data = $this->paginationData(100);

        $this->browse(function ($browser) use($data, $record){
            $browser->authenticate()
                    ->visit(new Datatable)
                    ->pause(2000)
                    ->type('@pagination.current', '2')
                    ->keys('@pagination.current', '{enter}')
                    ->pause(3000)
                    ->assertDontSee('Invalid Page')
                    ->assertInputValue('@first.row.code', $record[10]->code)
                    ->assertInputValue('@first.row.name', $record[10]->name)
                    ->assertInputValue('@first.row.remarks', $record[10]->remarks)
                    ->pause(1000)
                    ->logout();
        });
    }

    private function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    private function paginationData($count = 4, $per = 10)
    {
        $total = ceil($count / $per);
        $last = 1;

        if($count <= 0){
            $total = 1;
        }
        return [
            'total' => $total,
            'per' => $per,
            'last' => $total,
        ];
    }
}

