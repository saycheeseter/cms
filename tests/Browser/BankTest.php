<?php

namespace Tests\Browser;

class BankTest extends SystemCodeTest
{
    protected $url = '/bank';
    protected $page = 'Bank';
    protected $factory = 'bank';
}
