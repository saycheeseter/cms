<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Member;
use Lang;
use Carbon\Carbon;
use Illuminate\Support\Str;

class MemberTest extends DuskTestCase
{
    private $factory = 'member';

    private $rates;

    public function setUp()
    {
        parent::setUp();

        $this->rates();
    }

    /**
     * @test
     */
    public function accessing_member_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit('/member')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filters.full.name', 'Full Name')
                    ->assertSeeIn('@filters.card.id', 'Card ID')
                    ->assertSeeIn('@filters.barcode', 'Barcode')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_full_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.full.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]->full_name)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@first.row.full.name', $data[0]->full_name)
                    ->assertSeeIn('@first.row.member.type', $data[0]->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data[0]->card_id)
                    ->assertSeeIn('@first.row.barcode', $data[0]->barcode)
                    ->assertSeeIn('@first.row.mobile', $data[0]->mobile)
                    ->assertSeeIn('@first.row.telephone', $data[0]->telephone)
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.member.type')
                    ->assertMissing('@second.row.card.id')
                    ->assertMissing('@second.row.barcode')
                    ->assertMissing('@second.row.mobile')
                    ->assertMissing('@second.row.telephone')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_card_id_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.card.id')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[1]->card_id)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@first.row.full.name', $data[1]->full_name)
                    ->assertSeeIn('@first.row.member.type', $data[1]->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data[1]->card_id)
                    ->assertSeeIn('@first.row.barcode', $data[1]->barcode)
                    ->assertSeeIn('@first.row.mobile', $data[1]->mobile)
                    ->assertSeeIn('@first.row.telephone', $data[1]->telephone)
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.member.type')
                    ->assertMissing('@second.row.card.id')
                    ->assertMissing('@second.row.barcode')
                    ->assertMissing('@second.row.mobile')
                    ->assertMissing('@second.row.telephone')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_barcode_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.barcode')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[2]->barcode)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@first.row.full.name', $data[2]->full_name)
                    ->assertSeeIn('@first.row.member.type', $data[2]->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data[2]->card_id)
                    ->assertSeeIn('@first.row.barcode', $data[2]->barcode)
                    ->assertSeeIn('@first.row.mobile', $data[2]->mobile)
                    ->assertSeeIn('@first.row.telephone', $data[2]->telephone)
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.member.type')
                    ->assertMissing('@second.row.card.id')
                    ->assertMissing('@second.row.barcode')
                    ->assertMissing('@second.row.mobile')
                    ->assertMissing('@second.row.telephone')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.full.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '99999')
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.member.type')
                    ->assertMissing('@second.row.card.id')
                    ->assertMissing('@second.row.barcode')
                    ->assertMissing('@second.row.mobile')
                    ->assertMissing('@second.row.telephone')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function modal_fields_will_be_reset_upon_close()
    {
        $data = $this->build();

        $now = Carbon::now()->toDateString();

        $this->browse(function ($browser) use ($data, $now) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@card.id', $data->card_id)
                    ->type('@barcode', $data->barcode)
                    ->type('@full.name', $data->full_name)
                    ->selectChosen('@rates', (string) $data->rate_head_id)
                    ->type('@telephone', $data->telephone)
                    ->check('@status', 0)
                    ->selectDatepicker('@birth.date', $data->birth_date)
                    ->type('@mobile', $data->mobile)
                    ->radio('@gender', $data->gender)
                    ->selectDatepicker('@reg.date', $data->registration_date)
                    ->type('@email', $data->email)
                    ->selectDatepicker('@expiration.date', $data->expiration_date)
                    ->type('@address', $data->address)
                    ->type('@memo', $data->memo)
                    ->click('@close.detail')
                    ->click('@create')
                    ->pause(1000)
                    ->assertInputValue('@card.id', '')
                    ->assertInputValue('@barcode', '')
                    ->assertInputValue('@full.name', '')
                    ->assertSelectedChosen('@rates', '1')
                    ->assertInputValue('@telephone', '')
                    ->assertChecked('@status', '1')
                    ->assertDatepicker('@birth.date', $now)
                    ->assertInputValue('@mobile', '')
                    ->assertRadioSelected('@gender', 0)
                    ->assertDatepicker('@reg.date', $now)
                    ->assertDatepicker('@expiration.date', $now)
                    ->assertInputValue('@email', '')
                    ->assertInputValue('@address', '')
                    ->assertInputValue('@memo', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_non_numeric_values_in_card_id_and_barcode_is_not_allowed()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@card.id', 'H')
                    ->type('@barcode', 'H')
                    ->assertInputValue('@card.id', '')
                    ->assertInputValue('@barcode', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function form_modal_should_have_a_default_value_upon_creation()
    {
        $now = Carbon::now()->toDateString();

        $this->browse(function ($browser) use ($now) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->assertSelectedChosen('@rates', '1')
                    ->assertChecked('@status', '1')
                    ->assertDatepicker('@birth.date', $now)
                    ->assertDatepicker('@reg.date', $now)
                    ->assertDatepicker('@expiration.date', $now)
                    ->assertRadioSelected('@gender', 0)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_card_id_barcode_full_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.full.name.required'))
                    ->assertSee(Lang::get('core::validation.card.id.required'))
                    ->assertSee(Lang::get('core::validation.barcode.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_card_id_barcode_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.card.id.numeric'))
                    ->assertSee(Lang::get('core::validation.barcode.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_expiration_date_less_than_or_equal_the_regisration_date_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->selectDatepicker('@expiration.date', Carbon::now()->subDays(30)->toDateString())
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.expiration.date.invalid.range'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_full_name_barcode_or_card_id_will_trigger_an_error()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@full.name', $data->full_name)
                    ->type('@card.id', $data->card_id)
                    ->type('@barcode', $data->barcode)
                    ->selectDatepicker('@expiration.date', $data->expiration_date)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.full.name.unique'))
                    ->assertSee(Lang::get('core::validation.card.id.unique'))
                    ->assertSee(Lang::get('core::validation.barcode.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_proper_format_data_will_successfully_save_the_data_and_return_a_success_message()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@card.id', $data->card_id)
                    ->type('@barcode', $data->barcode)
                    ->type('@full.name', $data->full_name)
                    ->selectChosen('@rates', (string) $data->rate_head_id)
                    ->type('@telephone', $data->telephone)
                    ->check('@status', 0)
                    ->selectDatepicker('@birth.date', $data->birth_date)
                    ->type('@mobile', $data->mobile)
                    ->radio('@gender', $data->gender)
                    ->selectDatepicker('@reg.date', $data->registration_date)
                    ->type('@email', $data->email)
                    ->selectDatepicker('@expiration.date', $data->expiration_date)
                    ->type('@address', $data->address)
                    ->type('@memo', $data->memo)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(1000)
                    ->assertSeeIn('@first.row.full.name', $data->full_name)
                    ->assertSeeIn('@first.row.member.type', $data->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data->card_id)
                    ->assertSeeIn('@first.row.barcode', $data->barcode)
                    ->assertSeeIn('@first.row.mobile', $data->mobile)
                    ->assertSeeIn('@first.row.telephone', $data->telephone)
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.member.type')
                    ->assertMissing('@second.row.card.id')
                    ->assertMissing('@second.row.barcode')
                    ->assertMissing('@second.row.mobile')
                    ->assertMissing('@second.row.telephone')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_column_of_the_data_will_show_a_modal_with_its_corresponding_full_info()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@second.row.full.name')
                    ->pause(1000)
                    ->assertInputValue('@card.id', $data[1]->card_id)
                    ->assertInputValue('@barcode', $data[1]->barcode)
                    ->assertInputValue('@full.name', $data[1]->full_name)
                    ->assertSelectedChosen('@rates', (string) $data[1]->rate_head_id)
                    ->assertInputValue('@telephone', $data[1]->telephone)
                    ->assertChecked('@status', $data[1]->status)
                    ->assertDatepicker('@birth.date', $data[1]->birth_date)
                    ->assertInputValue('@mobile', $data[1]->mobile)
                    ->assertRadioSelected('@gender', $data[1]->gender)
                    ->assertDatepicker('@reg.date', $data[1]->registration_date)
                    ->assertDatepicker('@expiration.date', $data[1]->expiration_date)
                    ->assertInputValue('@email', $data[1]->email)
                    ->assertInputValue('@address', $data[1]->address)
                    ->assertInputValue('@memo', $data[1]->memo)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_card_id_barcode_full_name_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@second.row.full.name')
                    ->pause(1000)
                    ->clear('@full.name')
                    ->clear('@card.id')
                    ->clear('@barcode')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.full.name.required'))
                    ->assertSee(Lang::get('core::validation.card.id.required'))
                    ->assertSee(Lang::get('core::validation.barcode.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_non_numeric_card_id_barcode_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.full.name')
                    ->pause(1000)
                    ->clear('@full.name')
                    ->clear('@card.id')
                    ->clear('@barcode')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.card.id.numeric'))
                    ->assertSee(Lang::get('core::validation.barcode.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_expiration_date_less_than_or_equal_the_regisration_date_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@second.row.full.name')
                    ->pause(2000)
                    ->selectDatepicker('@expiration.date', Carbon::parse($data[1]->registration_date)->subDays(30)->toDateString())
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.expiration.date.invalid.range'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_full_name_barcode_or_card_id_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@second.row.full.name')
                    ->pause(3000)
                    ->type('@full.name', $data[0]->full_name)
                    ->type('@card.id', $data[0]->card_id)
                    ->type('@barcode', $data[0]->barcode)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.full.name.unique'))
                    ->assertSee(Lang::get('core::validation.card.id.unique'))
                    ->assertSee(Lang::get('core::validation.barcode.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_proper_format_data_will_successfully_update_the_data_and_return_a_success_message()
    {
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.full.name')
                    ->pause(1000)
                    ->type('@card.id', $data->card_id)
                    ->type('@barcode', $data->barcode)
                    ->type('@full.name', $data->full_name)
                    ->selectChosen('@rates', (string) $data->rate_head_id)
                    ->type('@telephone', $data->telephone)
                    ->check('@status', 0)
                    ->selectDatepicker('@birth.date', $data->birth_date)
                    ->type('@mobile', $data->mobile)
                    ->radio('@gender', $data->gender)
                    ->selectDatepicker('@reg.date', $data->registration_date)
                    ->type('@email', $data->email)
                    ->selectDatepicker('@expiration.date', $data->expiration_date)
                    ->type('@address', $data->address)
                    ->type('@memo', $data->memo)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(1000)
                    ->assertSeeIn('@first.row.full.name', $data->full_name)
                    ->assertSeeIn('@first.row.member.type', $data->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data->card_id)
                    ->assertSeeIn('@first.row.barcode', $data->barcode)
                    ->assertSeeIn('@first.row.mobile', $data->mobile)
                    ->assertSeeIn('@first.row.telephone', $data->telephone)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_table_list()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertVisible('@confirm')
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.member')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.member'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_table_list_will_hide_the_confirm_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@cancel')
                    ->pause(1000)
                    ->assertMissing('@confirm')
                    ->assertSeeIn('@first.row.full.name', $data[0]->full_name)
                    ->assertSeeIn('@first.row.member.type', $data[0]->rate->name)
                    ->assertSeeIn('@first.row.card.id', $data[0]->card_id)
                    ->assertSeeIn('@first.row.barcode', $data[0]->barcode)
                    ->assertSeeIn('@first.row.mobile', $data[0]->mobile)
                    ->assertSeeIn('@first.row.telephone', $data[0]->telephone)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_table_list_will_delete_the_data_in_table()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertMissing('@confirm')
                    ->assertDontSeeIn('@first.row.full.name', $data[0]->full_name)
                    ->assertDontSeeIn('@first.row.card.id', $data[0]->card_id)
                    ->assertDontSeeIn('@first.row.barcode', $data[0]->barcode)
                    ->assertDontSeeIn('@first.row.mobile', $data[0]->mobile)
                    ->assertDontSeeIn('@first.row.telephone', $data[0]->telephone)
                    ->assertSeeIn('@first.row.full.name', $data[1]->full_name)
                    ->assertSeeIn('@first.row.card.id', $data[1]->card_id)
                    ->assertSeeIn('@first.row.barcode', $data[1]->barcode)
                    ->assertSeeIn('@first.row.mobile', $data[1]->mobile)
                    ->assertSeeIn('@first.row.telephone', $data[1]->telephone)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_row_of_the_table_list_will_return_a_no_results_found()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Member)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@first.row.full.name')
                    ->assertMissing('@first.row.member.type')
                    ->assertMissing('@first.row.card.id')
                    ->assertMissing('@first.row.barcode')
                    ->assertMissing('@first.row.mobile')
                    ->assertMissing('@first.row.telephone')
                    ->logout();
        });
    }

    private function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'rate_head_id' => $this->rates[rand(0, 1)]->id
        ));
    }

    private function build($data = [])
    {
        return Factory::build($this->factory, array_merge(array(
            'rate_head_id' => $this->rates[rand(0, 1)]->id
        ), $data));
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, array_merge(array(
            'rate_head_id' => $this->rates[rand(0, 1)]->id
        ), $data));
    }

    private function rates()
    {
        $this->rates = Factory::times(2)->create('member_rate');
    }
}
