<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Category;
use Illuminate\Support\Str;
use Lang;

class CategoryTest extends DuskTestCase
{
    private $factory = 'category';

    /**
     * @test
     */
    public function accessing_category_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit('/category')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_filters_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(2000)
                    ->click('@filters')
                    ->pause(2000)
                    ->assertSeeIn('@filter.code', 'Code')
                    ->assertSeeIn('@filter.name', 'Name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function upon_loading_of_page_root_category_should_be_visible()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(2000)
                    ->assertVisible('@tree.first')
                    ->assertVisible('@tree.second')
                    ->assertMissing('@tree.first.child.first')
                    ->assertMissing('@tree.second.child.first')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_category_and_selecting_a_filter_by_code_would_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.code')
                    ->pause(1000)
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[0]->code)
                    ->click('@filter.search')
                    ->pause(2000)
                    ->assertInputValue('@table.first.code', $data[0]->code)
                    ->assertInputValue('@table.first.name', $data[0]->name)
                    ->assertInputValue('@table.second.code', '')
                    ->assertInputValue('@table.second.name', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_category_and_selecting_a_filter_by_name_would_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[0]->name)
                    ->click('@filter.search')
                    ->pause(2000)
                    ->assertInputValue('@table.first.code', $data[0]->code)
                    ->assertInputValue('@table.first.name', $data[0]->name)
                    ->assertInputValue('@table.second.code', '')
                    ->assertInputValue('@table.second.name', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_category_and_selecting_a_filter_that_do_not_match_any_data_will_return_no_results()
    {
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data->code)
                    ->click('@filter.search')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertInputValue('@table.first.code', '')
                    ->assertInputValue('@table.first.name', '')
                    ->assertMissing('@table.second.code')
                    ->assertMissing('@table.second.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_parent_category_would_show_its_child_categories()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@tree.first.collapse')
                    ->pause(1000)
                    ->click('@tree.first')
                    ->pause(1000)
                    ->assertSeeIn('@tree.first.child.first', $data[0]->children[0]->code." ".$data[0]->children[0]->name)
                    ->assertInputValue('@table.first.code', $data[0]->children[0]->code)
                    ->assertInputValue('@table.first.name', $data[0]->children[0]->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_category_with_an_empty_code_or_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->click('@table.first.action')
                    ->pause(2000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.required'),
                        Lang::get('core::validation.name.required')
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_category_code_or_name_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->type('@table.third.code', $data[0]->code)
                    ->type('@table.third.name', $data[0]->name)
                    ->click('@table.third.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_data_with_correct_format_will_successfully_save_the_data_and_be_included_on_the_category_tree()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertSeeIn('@tree.first', $data->code." ".$data->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_child_category_with_proper_format_will_successfully_save_the_data_and_be_included_under_the_parent_category_childrens()
    {
        $this->collection(1);

        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->click('@tree.first')
                    ->click('@tree.first.collapse')
                    ->pause(1000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertSeeIn('@tree.first.child.first', $data->code." ".$data->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_record_with_an_empty_code_or_name_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->clear('@table.first.code')
                    ->clear('@table.first.name')
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.required'),
                        Lang::get('core::validation.name.required'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_record_with_an_existing_name_or_code_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->type('@table.first.code', $data[1]->code)
                    ->type('@table.first.name', $data[1]->name)
                    ->click('@table.first.action')
                    ->pause(2000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.unique'),
                        Lang::get('core::validation.name.unique')
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_parent_record_with_correct_format_will_successfully_update_the_data_and_updates_the_parent_node_label()
    {
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertSeeIn('@tree.first', $data->code." ".$data->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_child_record_with_correct_format_will_successfully_update_the_data_and_updates_the_child_node_label()
    {
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(1000)
                    ->click('@tree.first')
                    ->click('@tree.first.collapse')
                    ->pause(1000)
                    ->type('@table.first.code', $data->code)
                    ->type('@table.first.name', $data->name)
                    ->click('@table.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->assertInputValue('@table.first.code', $data->code)
                    ->assertInputValue('@table.first.name', $data->name)
                    ->assertSeeIn('@tree.first.child.first', $data->code." ".$data->name)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_list_will_show_a_confirmation_message()
    {
        $this->collection(1);

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->assertVisible('@confirm')
                    ->assertSeeInArray('@confirm', array(
                        Str::upper(Lang::get('core::label.delete.category')),
                        Lang::get('core::confirm.delete.category')
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_list_and_selecting_the_cancel_button_on_the_data_will_hide_the_confirmation_message()
    {
        $this->collection(1);

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->click('@confirm.cancel')
                    ->pause(2000)
                    ->assertMissing('@confirm')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_list_and_selecting_the_ok_button_will_delete_the_data_return_a_success_message_and_remove_it_from_the_category_tree()
    {
        $data = $this->collection(1);

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertInputValue('@table.first.code', $data[1]->code)
                    ->assertInputValue('@table.first.name', $data[1]->name)
                    ->assertSeeIn('@tree.first', $data[1]->code.' '.$data[1]->name)
                    ->assertMissing('@tree.second')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_a_category_with_a_sub_category_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Category)
                    ->pause(3000)
                    ->click('@table.first.action')
                    ->pause(3000)
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.parent.cannot.delete'))
                    ->logout();
        });
    }

    private function build($parent = null)
    {
        return Factory::build($this->factory, array(
            'parent_id' => $parent
        ));
    }

    private function create($parent = null)
    {
        return Factory::create($this->factory, array(
            'parent_id' => $parent
        ));
    }

    private function collection($layer = 2)
    {
        $parents = Factory::times(2)->create($this->factory);

        for ($i=0; $i < count($parents); $i++) { 
            $parent = $parents[$i]->id;

            for ($x=1; $x < $layer; $x++) { 
                $child = Factory::create($this->factory, array(
                    'parent_id' => $parent
                ));

                $parent = $child->id;
            }

            $parent = $parents[$i]->id;
        }

        return $parents;
    }
}
