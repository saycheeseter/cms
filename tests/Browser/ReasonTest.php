<?php

namespace Tests\Browser;

class ReasonTest extends SystemCodeTest
{
    protected $url = '/reason';
    protected $page = 'Reason';
    protected $factory = 'reason';
}
