<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Settings;
use Illuminate\Support\Str;
use Lang;
use DB;

class SettingsTest extends DuskTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function accessing_settings_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/settings')
                    ->pause(2000)
                    ->assertPathIs('/');
        });
    }

     /**
     * @test
     */
    public function accessing_the_system_settings_tab_will_show_general_settings()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Settings)
                    ->pause(2000)
                    ->assertVisible('@setting.general.no.pos.terminals')
                    ->assertVisible('@setting.general.monetary.precision')
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function clicking_purchase_order_section_will_show_all_purchase_settings()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Settings)
                    ->pause(2000)
                    ->click('@section.purchase.order')
                    ->assertVisible('@setting.purchase.order.action.approve')
                    ->assertVisible('@setting.purchase.order.action.for.approval')
                    ->assertVisible('@setting.purchase.order.action.revert.approved')
                    ->assertVisible('@setting.purchase.order.action.revert.declined')
                    ->assertVisible('@setting.purchase.order.disable.invoice.yes')
                    ->assertVisible('@setting.purchase.order.disable.invoice.no')
                    ->assertVisible('@setting.purchase.order.price.history.based.yes')
                    ->assertVisible('@setting.purchase.order.price.history.based.no')
                    ->assertVisible('@setting.purchase.order.big.qty.history.based.yes')
                    ->assertVisible('@setting.purchase.order.big.qty.history.based.no')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.toggle')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.days')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.not.received.toggle')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.not.received.days')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.received.toggle')
                    ->assertVisible('@setting.purchase.order.warning.already.purchased.received.days')
                    ->pause(2000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_non_numeric_value_on_no_of_pos_terminals_and_monetary_precision_is_not_allowed()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Settings)
                    ->pause(2000)
                    ->clear('@setting.general.no.pos.terminals')
                    ->clear('@setting.general.monetary.precision')
                    ->type('@setting.general.no.pos.terminals', 'AAA')
                    ->type('@setting.general.monetary.precision', 'AAA')
                    ->pause(2000)
                    ->assertInputValue('@setting.general.no.pos.terminals', '')
                    ->assertInputValue('@setting.general.monetary.precision', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_non_numeric_value_on_days_before_warning_on_purchase_settings_is_not_allowed()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Settings)
                    ->pause(2000)
                    ->click('@section.purchase.order')
                    ->pause(2000)
                    ->clear('@setting.purchase.order.warning.already.purchased.days')
                    ->clear('@setting.purchase.order.warning.already.purchased.not.received.days')
                    ->clear('@setting.purchase.order.warning.already.purchased.received.days')
                    ->type('@setting.purchase.order.warning.already.purchased.days', '')
                    ->type('@setting.purchase.order.warning.already.purchased.not.received.days', '')
                    ->type('@setting.purchase.order.warning.already.purchased.received.days', '')
                    ->pause(2000)
                    ->assertInputValue('@setting.purchase.order.warning.already.purchased.days', '')
                    ->assertInputValue('@setting.purchase.order.warning.already.purchased.not.received.days', '')
                    ->assertInputValue('@setting.purchase.order.warning.already.purchased.received.days', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updatings_with_correct_data_will_trigger_successful_response()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Settings)
                    ->pause(2000)
                    ->type('@setting.general.no.pos.terminals', '1')
                    ->type('@setting.general.monetary.precision', '2')
                    ->pause(2000)
                    ->click('@section.purchase.order')
                    ->click('@setting.purchase.order.action.approve')
                    ->click('@setting.purchase.order.action.for.approval')
                    ->click('@setting.purchase.order.action.revert.approved')
                    ->click('@setting.purchase.order.action.revert.declined')
                    ->click('@setting.purchase.order.disable.invoice.yes')
                    ->click('@setting.purchase.order.price.history.based.yes')
                    ->click('@setting.purchase.order.big.qty.history.based.yes')
                    ->click('@setting.purchase.order.warning.already.purchased.toggle')
                    ->type('@setting.purchase.order.warning.already.purchased.days', '2')
                    ->click('@setting.purchase.order.warning.already.purchased.not.received.toggle')
                    ->type('@setting.purchase.order.warning.already.purchased.not.received.days', '2')
                    ->click('@setting.purchase.order.warning.already.purchased.received.toggle')
                    ->type('@setting.purchase.order.warning.already.purchased.received.days', '2')
                    ->pause(2000)
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(2000)
                    ->logout();
        });
    }
}
