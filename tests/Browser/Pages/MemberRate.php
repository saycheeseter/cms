<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class MemberRate extends Page
{
    public function url()
    {
        return '/member-rate';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/member-rate')
                ->assertTitle(Lang::get('core::label.member.rate'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = '#table-modal tbody';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = [
            'list' => '#confirm-list',
            'detail' => '#confirm-detail',
        ];

        $table = [
            'list' => '#table-list tbody',
            'detail' => '#table-modal tbody',
        ];

        $modal = '#modal-detail';

        return [

            '@filters' => '#controls div[name="filters"]',

            '@filter.search' => $filter['search'],
            '@filter.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filter.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),

            '@filter.name' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filter.memo' => sprintf('%s li:nth-child(2)', $filter['options']),

            '@filter.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filter.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filter.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),

            '@create' => '#controls div[name="add"]',

            '@list.confirm' => $confirm['list'],
            '@list.confirm.ok' => sprintf('%s input[name="ok"]', $confirm['list']),
            '@list.confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm['list']),

            '@list.first.name' => $this->table($table['list'])->row(1)->cell(1)->element('span')->get(),
            '@list.first.memo' => $this->table($table['list'])->row(1)->cell(2)->element('span')->get(),
            '@list.first.active' => $this->table($table['list'])->row(1)->cell(3)->element('span')->get(),
            '@list.first.action' => $this->table($table['list'])->row(1)->cell(4)->element('img')->get(),

            '@list.second.name' => $this->table($table['list'])->row(2)->cell(1)->element('span')->get(),
            '@list.second.memo' => $this->table($table['list'])->row(2)->cell(2)->element('span')->get(),
            '@list.second.active' => $this->table($table['list'])->row(2)->cell(3)->element('span')->get(),
            '@list.second.action' => $this->table($table['list'])->row(2)->cell(4)->element('img')->get(),

            '@head.name' => 'input[name="name"]',
            '@head.active' => 'active',
            '@head.memo' => 'textarea[name="memo"]',

            '@detail.confirm' => $confirm['detail'],
            '@detail.confirm.ok' => sprintf('%s input[name="ok"]', $confirm['detail']),
            '@detail.confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm['detail']),

            '@detail.first.amount.from' => $this->table($table['detail'])->row(1)->cell(1)->element('input')->get(),
            '@detail.first.amount.to' => $this->table($table['detail'])->row(1)->cell(2)->element('input')->get(),
            '@detail.first.date.from' => $this->table($table['detail'])->row(1)->cell(3)->element('div.date-from')->get(),
            '@detail.first.date.to' => $this->table($table['detail'])->row(1)->cell(4)->element('div.date-to')->get(),
            '@detail.first.discount' => $this->table($table['detail'])->row(1)->cell(5)->element('input')->get(),
            '@detail.first.amount.per.point' => $this->table($table['detail'])->row(1)->cell(6)->element('input')->get(),
            '@detail.first.action' => $this->table($table['detail'])->row(1)->cell(7)->element('img')->get(),

            '@detail.second.amount.from' => $this->table($table['detail'])->row(2)->cell(1)->element('input')->get(),
            '@detail.second.amount.to' => $this->table($table['detail'])->row(2)->cell(2)->element('input')->get(),
            '@detail.second.date.from' => $this->table($table['detail'])->row(2)->cell(3)->element('div.date-from')->get(),
            '@detail.second.date.to' => $this->table($table['detail'])->row(2)->cell(4)->element('div.date-to')->get(),
            '@detail.second.discount' => $this->table($table['detail'])->row(2)->cell(5)->element('input')->get(),
            '@detail.second.amount.per.point' => $this->table($table['detail'])->row(2)->cell(6)->element('input')->get(),
            '@detail.second.action' => $this->table($table['detail'])->row(2)->cell(7)->element('img')->get(),

            '@detail.third.amount.from' => $this->table($table['detail'])->row(3)->cell(1)->element('input')->get(),
            '@detail.third.amount.to' => $this->table($table['detail'])->row(3)->cell(2)->element('input')->get(),
            '@detail.third.date.from' => $this->table($table['detail'])->row(3)->cell(3)->element('div.date-from')->get(),
            '@detail.third.date.to' => $this->table($table['detail'])->row(3)->cell(4)->element('div.date-to')->get(),
            '@detail.third.discount' => $this->table($table['detail'])->row(3)->cell(5)->element('input')->get(),
            '@detail.third.amount.per.point' => $this->table($table['detail'])->row(3)->cell(6)->element('input')->get(),
            '@detail.third.action' => $this->table($table['detail'])->row(3)->cell(7)->element('img')->get(),

            '@save' => '#save',
            '@modal.detail' => $modal,
            '@close.detail' => sprintf('%s button.close', $modal),
        ];
    }
}
