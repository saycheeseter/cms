<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class BankAccount extends Page
{
    public function url()
    {
        return '/bank-account';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/bank-account')
                ->assertTitle(Lang::get('core::label.bank.account'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = '#table-list tbody';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = '#confirm';

        return [
            '@first.row.bank' => $this->table($table)->row(1)->cell(1)->element('div.chosen[name="banks"]')->get(),
            '@first.row.account.number' => $this->table($table)->row(1)->cell(2)->element('input')->get(),
            '@first.row.account.name' => $this->table($table)->row(1)->cell(3)->element('input')->get(),
            '@first.row.opening.balance' => $this->table($table)->row(1)->cell(4)->element('input')->get(),
            '@first.row.command' => $this->table($table)->row(1)->cell(5)->element('img')->get(),

            '@second.row.bank' => $this->table($table)->row(2)->cell(1)->element('div.chosen[name="banks"]')->get(),
            '@second.row.account.number' => $this->table($table)->row(2)->cell(2)->element('input')->get(),
            '@second.row.account.name' => $this->table($table)->row(2)->cell(3)->element('input')->get(),
            '@second.row.opening.balance' => $this->table($table)->row(2)->cell(4)->element('input')->get(),
            '@second.row.command' => $this->table($table)->row(2)->cell(5)->element('img')->get(),

            '@third.row.bank' => $this->table($table)->row(3)->cell(1)->element('div.chosen[name="banks"]')->get(),
            '@third.row.account.number' => $this->table($table)->row(3)->cell(2)->element('input')->get(),
            '@third.row.account.name' => $this->table($table)->row(3)->cell(3)->element('input')->get(),
            '@third.row.opening.balance' => $this->table($table)->row(3)->cell(4)->element('input')->get(),
            '@third.row.command' => $this->table($table)->row(3)->cell(5)->element('img')->get(),

            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.account.number' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.account.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filters.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filters.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filters.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),

            '@confirm' => $confirm,
            '@ok' => sprintf('%s input[name="ok"]', $confirm),
            '@cancel' => sprintf('%s input[name="cancel"]', $confirm)
        ];
    }
}
