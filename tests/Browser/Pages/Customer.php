<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Customer extends Page
{
    public function url()
    {
        return '/customer';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/customer')
                ->assertTitle(Lang::get('core::label.customer'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = [
            'list' => '#table-list tbody',
            'detail' => '#table-modal tbody'
        ];

        $modal = '#modal-detail';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = [
            'list' => '#confirm-list',
            'detail' => '#confirm-detail'
        ];

        return [
            '@list.first.row.code' => $this->table($table['list'])->row(1)->cell(1)->element('span')->get(),
            '@list.first.row.name' => $this->table($table['list'])->row(1)->cell(2)->element('span')->get(),
            '@list.first.row.command' => $this->table($table['list'])->row(1)->cell(3)->element('img')->get(),
            '@list.second.row.code' => $this->table($table['list'])->row(2)->cell(1)->element('span')->get(),
            '@list.second.row.name' => $this->table($table['list'])->row(2)->cell(2)->element('span')->get(),
            '@list.second.row.command' => $this->table($table['list'])->row(2)->cell(3)->element('img')->get(),
            
            '@detail.first.row.name' => $this->table($table['detail'])->row(1)->cell(1)->element('span')->get(),
            '@detail.first.row.address' => $this->table($table['detail'])->row(1)->cell(2)->element('span')->get(),
            '@detail.first.row.email' => $this->table($table['detail'])->row(1)->cell(11)->element('span')->get(),
            '@detail.first.row.command' => $this->table($table['detail'])->row(1)->cell(12)->element('img')->get(),
            '@detail.second.row.name' => $this->table($table['detail'])->row(2)->cell(1)->element('span')->get(),
            '@detail.second.row.address' => $this->table($table['detail'])->row(2)->cell(2)->element('span')->get(),
            '@detail.second.row.email' => $this->table($table['detail'])->row(2)->cell(11)->element('span')->get(),
            '@detail.second.row.command' => $this->table($table['detail'])->row(2)->cell(12)->element('img')->get(),

            '@create' => '#controls div[name="add"]',
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filters.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filters.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filters.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),
            
            '@confirm.list' => $confirm['list'],
            '@ok.list' => sprintf('%s input[name="ok"]', $confirm['list']),
            '@cancel.list' => sprintf('%s input[name="cancel"]', $confirm['list']),
            '@confirm.detail' => $confirm['detail'],
            '@ok.detail' => sprintf('%s input[name="ok"]', $confirm['detail']),
            '@cancel.detail' => sprintf('%s input[name="cancel"]', $confirm['detail']),
            '@save' => sprintf('%s button[name="save"]', $modal),
            '@basic' => sprintf('%s div[name="basic"]', $modal),
            '@advance' => sprintf('%s div[name="advance"]', $modal),
            '@branches' => sprintf('%s div[name="branches"]', $modal),
            '@close.detail' => sprintf('%s button.close', $modal),
            '@customer.detail.section' => sprintf('%s section.customer-info', $modal),
            '@customer.advance.section' => sprintf('%s section.advance-info', $modal),
            '@customer.branches.section' => sprintf('%s section.customer-branches', $modal),
            '@modal.detail' => $modal,

            '@name' => sprintf('%s input[name="name"]', $modal),
            '@code' => sprintf('%s input[name="code"]', $modal),
            '@price.basis' => sprintf('%s div.chosen[name="price-basis"]', $modal),
            '@pricing.rate' => sprintf('%s input[name="pricing-rate"]', $modal),
            '@historical.revert.element' => sprintf('%s input[name="historical-revert"]', $modal),
            '@historical.default.element' => sprintf('%s input[name="historical-default"]', $modal),
            '@historical.revert' => "historical-revert",
            '@historical.default' => "historical-default",
            '@salesman' => sprintf('%s div.chosen[name="salesman"]', $modal),
            '@status' => sprintf('%s input[name="status"]', $modal),
            '@memo' => sprintf('%s textarea[name="memo"]', $modal),
            '@website' => sprintf('%s input[name="website"]', $modal),
            '@credit.limit' => sprintf('%s input[name="credit-limit"]', $modal),
            '@term' => sprintf('%s input[name="term"]', $modal),
            '@branch.name' => sprintf('%s input[name="branch-name"]', $modal),
            '@branch.address' => sprintf('%s textarea[name="branch-address"]', $modal),
            '@branch.chinese.name' => sprintf('%s input[name="branch-chinese-name"]', $modal),
            '@branch.email' => sprintf('%s input[name="branch-email"]', $modal),
            '@branch.create' => sprintf('%s input[name="branch-new"]', $modal),
            '@branch.update' => sprintf('%s input[name="branch-update"]', $modal),
            '@branch.clear' => sprintf('%s input[name="branch-clear"]', $modal),
        ];
    }
}
