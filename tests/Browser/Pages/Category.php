<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Category extends Page
{
    public function url()
    {
        return '/category';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/category')
                ->assertTitle(Lang::get('core::label.category'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
         $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $table = '#table-list tbody';
        $tree  = '#tree li ul';
        $confirm = '#confirm';

        $parent = [
            'first' => sprintf('%s li:nth-child(1)', $tree),
            'second' => sprintf('%s li:nth-child(2)', $tree),
        ];

        return [
            '@filters' => '#controls div[name="filters"]',

            '@filter.search' => $filter['search'],
            '@filter.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filter.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filter.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filter.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filter.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filter.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filter.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filter.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filter.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filter.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),

            '@tree.first' => sprintf('%s span', $parent['first']),
            '@tree.first.collapse' => sprintf('%s div.tree-node', $parent['first']),
            '@tree.first.child.first' => sprintf('%s ul li:nth-child(1) span', $parent['first']),
            '@tree.first.child.first.collapse' => sprintf('%s ul li:nth-child(1) div.tree-node', $parent['first']),
            '@tree.first.child.second' => sprintf('%s ul li:nth-child(2) span', $parent['first']),
            '@tree.first.child.second.collapse' => sprintf('%s ul li:nth-child(2) div.tree-node', $parent['first']),
            '@tree.first.child.first.child.first' => sprintf('%s ul li:nth-child(1) ul li:nth-child(1) span', $parent['first']),
            '@tree.first.child.first.child.first.collapse' => sprintf('%s ul li:nth-child(1) ul li:nth-child(1) div.tree-node', $parent['first']),
            '@tree.first.child.first.child.second' => sprintf('%s ul li:nth-child(1) ul li:nth-child(2) span', $parent['first']),
            '@tree.first.child.first.child.second.collapse' => sprintf('%s ul li:nth-child(1) ul li:nth-child(2) div.tree-node', $parent['first']),

            '@tree.second' => sprintf('%s span', $parent['second']),
            '@tree.second.collapse' => sprintf('%s div.tree-node', $parent['second']),
            '@tree.second.child.first' => sprintf('%s ul li:nth-child(1) span', $parent['second']),
            '@tree.second.child.first.collapse' => sprintf('%s ul li:nth-child(1) div.tree-node', $parent['second']),
            '@tree.second.child.second' => sprintf('%s ul li:nth-child(2) span', $parent['second']),
            '@tree.second.child.second.collapse' => sprintf('%s ul li:nth-child(2) div.tree-node', $parent['second']),
            '@tree.second.child.first.child.first' => sprintf('%s ul li:nth-child(1) ul li:nth-child(1) span', $parent['second']),
            '@tree.second.child.first.child.first.collapse' => sprintf('%s ul li:nth-child(1) ul li:nth-child(1) div.tree-node', $parent['second']),
            '@tree.second.child.first.child.second' => sprintf('%s ul li:nth-child(1) ul li:nth-child(2) span', $parent['second']),
            '@tree.second.child.first.child.second.collapse' => sprintf('%s ul li:nth-child(1) ul li:nth-child(2) div.tree-node', $parent['second']),

            '@table.first.code' => $this->table($table)->row(1)->cell(1)->element('input')->get(),
            '@table.first.name' => $this->table($table)->row(1)->cell(2)->element('input')->get(),
            '@table.first.action' => $this->table($table)->row(1)->cell(3)->element('img')->get(),
            '@table.second.code' => $this->table($table)->row(2)->cell(1)->element('input')->get(),
            '@table.second.name' => $this->table($table)->row(2)->cell(2)->element('input')->get(),
            '@table.second.action' => $this->table($table)->row(2)->cell(3)->element('img')->get(),
            '@table.third.code' => $this->table($table)->row(3)->cell(1)->element('input')->get(),
            '@table.third.name' => $this->table($table)->row(3)->cell(2)->element('input')->get(),
            '@table.third.action' => $this->table($table)->row(3)->cell(3)->element('img')->get(),

            '@confirm' => '#confirm',
            '@confirm.ok' => sprintf('%s input[name="ok"]', $confirm),
            '@confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm),
        ];
    }
}
