<?php 

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class SalesReturn extends Invoice
{
    public function url()
    {
        return '/sales-return';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/sales-return')
                ->assertTitle(Lang::get('core::label.sales.return.list'));
    }

    protected function listElements() 
    {
        $table = [
            'tbody' => '#table-list tbody',
            'thead' => '#table-list thead'
        ];

        return [
            '@columns.settings.modal' => '#column-settings',
            '@columns.settings.transaction.date' => '#column-settings ul li:nth-child(1) span',
            '@columns.settings.created.from' => '#column-settings ul li:nth-child(2) span',
            '@columns.settings.created.for' => '#column-settings ul li:nth-child(3) span',
            '@columns.settings.salesman' => '#column-settings ul li:nth-child(4) span',
            '@columns.settings.customer' => '#column-settings ul li:nth-child(5) span',
            '@columns.settings.customer.detail' => '#column-settings ul li:nth-child(6) span',
            '@columns.settings.remarks' => '#column-settings ul li:nth-child(7) span',
            '@columns.settings.created.by' => '#column-settings ul li:nth-child(8) span',
            '@columns.settings.created.date' => '#column-settings ul li:nth-child(9) span',
            '@columns.settings.audit.by' => '#column-settings ul li:nth-child(10) span',
            '@columns.settings.audit.date' => '#column-settings ul li:nth-child(11) span',
            '@columns.settings.deleted' => '#column-settings ul li:nth-child(12) span',
            '@columns.settings.approval.status' => '#column-settings ul li:nth-child(13) span',
            '@columns.settings.transaction.status' => '#column-settings ul li:nth-child(14) span',
            '@columns.settings.amount' => '#column-settings ul li:nth-child(15) span',

            '@columns.settings.modal.checkbox' => '#column-settings',
            '@columns.settings.transaction.date.checkbox' => '#column-settings ul li:nth-child(1) input[type="checkbox"]',
            '@columns.settings.created.from.checkbox' => '#column-settings ul li:nth-child(2) input[type="checkbox"]',
            '@columns.settings.created.for.checkbox' => '#column-settings ul li:nth-child(3) input[type="checkbox"]',
            '@columns.settings.salesman.checkbox' => '#column-settings ul li:nth-child(4) input[type="checkbox"]',
            '@columns.settings.customer.checkbox' => '#column-settings ul li:nth-child(5) input[type="checkbox"]',
            '@columns.settings.customer.detail.checkbox' => '#column-settings ul li:nth-child(6) input[type="checkbox"]',
            '@columns.settings.remarks.checkbox' => '#column-settings ul li:nth-child(7) input[type="checkbox"]',
            '@columns.settings.created.by.checkbox' => '#column-settings ul li:nth-child(8) input[type="checkbox"]',
            '@columns.settings.created.date.checkbox' => '#column-settings ul li:nth-child(9) input[type="checkbox"]',
            '@columns.settings.audit.by.checkbox' => '#column-settings ul li:nth-child(10) input[type="checkbox"]',
            '@columns.settings.audit.date.checkbox' => '#column-settings ul li:nth-child(11) input[type="checkbox"]',
            '@columns.settings.deleted.checkbox' => '#column-settings ul li:nth-child(12) input[type="checkbox"]',
            '@columns.settings.approval.status.checkbox' => '#column-settings ul li:nth-child(13) input[type="checkbox"]',
            '@columns.settings.transaction.status.checkbox' => '#column-settings ul li:nth-child(14) input[type="checkbox"]',
            '@columns.settings.amount.checkbox' => '#column-settings ul li:nth-child(15) input[type="checkbox"]',

            '@columns.settings.apply.selection' => '#column-settings div.modal-footer button',

            '@headers.checkall' => $this->table($table['thead'])->row(1)->header(1)->element("input")->get(),
            '@headers.transaction.date' => $this->table($table['thead'])->row(1)->header(2)->element("div")->get(),
            '@headers.sheet.no' => $this->table($table['thead'])->row(1)->header(3)->element("div")->get(),
            '@headers.created.from' => $this->table($table['thead'])->row(1)->header(4)->element("div")->get(),
            '@headers.created.for' => $this->table($table['thead'])->row(1)->header(5)->element("div")->get(),
            '@headers.salesman' => $this->table($table['thead'])->row(1)->header(6)->element("div")->get(),
            '@headers.customer' => $this->table($table['thead'])->row(1)->header(7)->element("div")->get(),
            '@headers.customer.detail' => $this->table($table['thead'])->row(1)->header(8)->element("div")->get(),
            '@headers.remarks' => $this->table($table['thead'])->row(1)->header(9)->element("div")->get(),
            '@headers.created.by' => $this->table($table['thead'])->row(1)->header(10)->element("div")->get(),
            '@headers.created.date' => $this->table($table['thead'])->row(1)->header(11)->element("div")->get(),
            '@headers.audit.by' => $this->table($table['thead'])->row(1)->header(12)->element("div")->get(),
            '@headers.audit.date' => $this->table($table['thead'])->row(1)->header(13)->element("div")->get(),
            '@headers.deleted' => $this->table($table['thead'])->row(1)->header(14)->element("div")->get(),
            '@headers.approval.status' => $this->table($table['thead'])->row(1)->header(15)->element("div")->get(),
            '@headers.transaction.status' => $this->table($table['thead'])->row(1)->header(16)->element("div")->get(),
            '@headers.amount' => $this->table($table['thead'])->row(1)->header(17)->element("div")->get(),

            '@first.row.checkbox' => $this->table($table['tbody'])->row(1)->cell(1)->element("input")->get(),
            '@first.row.transaction.date' => $this->table($table['tbody'])->row(1)->cell(2)->element("span")->get(),
            '@first.row.sheet.no' => $this->table($table['tbody'])->row(1)->cell(3)->element("span")->get(),
            '@first.row.created.from' => $this->table($table['tbody'])->row(1)->cell(4)->element("span")->get(),
            '@first.row.created.for' => $this->table($table['tbody'])->row(1)->cell(5)->element("span")->get(),
            '@first.row.salesman' => $this->table($table['tbody'])->row(1)->cell(6)->element("span")->get(),
            '@first.row.customer' => $this->table($table['tbody'])->row(1)->cell(7)->element("span")->get(),
            '@first.row.customer.detail' => $this->table($table['tbody'])->row(1)->cell(8)->element("span")->get(),
            '@first.row.remarks' => $this->table($table['tbody'])->row(1)->cell(9)->element("span")->get(),
            '@first.row.created.by' => $this->table($table['tbody'])->row(1)->cell(10)->element("span")->get(),
            '@first.row.created.date' => $this->table($table['tbody'])->row(1)->cell(11)->element("span")->get(),
            '@first.row.audit.by' => $this->table($table['tbody'])->row(1)->cell(12)->element("span")->get(),
            '@first.row.audit.date' => $this->table($table['tbody'])->row(1)->cell(13)->element("span")->get(),
            '@first.row.deleted' => $this->table($table['tbody'])->row(1)->cell(14)->element("span")->get(),
            '@first.row.approval.status' => $this->table($table['tbody'])->row(1)->cell(15)->element("span")->get(),
            '@first.row.transaction.status' => $this->table($table['tbody'])->row(1)->cell(16)->element("span")->get(),
            '@first.row.amount' => $this->table($table['tbody'])->row(1)->cell(17)->element("span")->get(),
            '@first.row.delete.button' => $this->table($table['tbody'])->row(1)->cell(18)->element("div")->get(),

            '@second.row.checkbox' => $this->table($table['tbody'])->row(1)->cell(1)->element("input")->get(),
            '@second.row.transaction.date' => $this->table($table['tbody'])->row(1)->cell(2)->element("span")->get(),
            '@second.row.sheet.no' => $this->table($table['tbody'])->row(1)->cell(3)->element("span")->get(),
            '@second.row.created.from' => $this->table($table['tbody'])->row(1)->cell(4)->element("span")->get(),
            '@second.row.created.for' => $this->table($table['tbody'])->row(1)->cell(5)->element("span")->get(),
            '@second.row.salesman' => $this->table($table['tbody'])->row(1)->cell(6)->element("span")->get(),
            '@second.row.customer' => $this->table($table['tbody'])->row(1)->cell(7)->element("span")->get(),
            '@second.row.customer.detail' => $this->table($table['tbody'])->row(1)->cell(8)->element("span")->get(),
            '@second.row.remarks' => $this->table($table['tbody'])->row(1)->cell(9)->element("span")->get(),
            '@second.row.created.by' => $this->table($table['tbody'])->row(1)->cell(10)->element("span")->get(),
            '@second.row.created.date' => $this->table($table['tbody'])->row(1)->cell(11)->element("span")->get(),
            '@second.row.audit.by' => $this->table($table['tbody'])->row(1)->cell(12)->element("span")->get(),
            '@second.row.audit.date' => $this->table($table['tbody'])->row(1)->cell(13)->element("span")->get(),
            '@second.row.deleted' => $this->table($table['tbody'])->row(1)->cell(14)->element("span")->get(),
            '@second.row.approval.status' => $this->table($table['tbody'])->row(1)->cell(15)->element("span")->get(),
            '@second.row.transaction.status' => $this->table($table['tbody'])->row(1)->cell(16)->element("span")->get(),
            '@second.row.amount' => $this->table($table['tbody'])->row(1)->cell(17)->element("span")->get(),
            '@second.row.delete.button' => $this->table($table['tbody'])->row(1)->cell(18)->element("div")->get(),

            '@third.row.checkbox' => $this->table($table['tbody'])->row(3)->cell(1)->element("input")->get(),
            '@fourth.row.checkbox' => $this->table($table['tbody'])->row(4)->cell(1)->element("input")->get(),

            '@delete.invoice.modal' => '#confirm',
            '@delete.invoice.ok' => '#confirm div.modal-footer input[name="ok"]'
        ];
    }

    protected function detailElements() 
    {
        $info = '#main-info';

        return [
            '@container.main.info' => $info,

            '@main.info.sheet.no' => sprintf('%s span[name="sheet-no"]', $info),
            '@main.info.term' => sprintf('%s input[name="term"]', $info),
            '@main.info.remarks' => sprintf('%s textarea[name="remarks"]', $info),
            '@main.info.date.time' => sprintf('%s div[name="date-time"]', $info),

            '@main.info.salesman' => sprintf('%s div.chosen[name="salesman"]', $info),
            '@main.info.customer' => sprintf('%s div.chosen[name="customer"]', $info),
            '@main.info.customer.detail' => sprintf('%s div.chosen[name="customer-detail"]', $info),
            '@main.info.created.for' => sprintf('%s div.chosen[name="created-for"]', $info),
            '@main.info.requested.by' => sprintf('%s div.chosen[name="requested-by"]', $info),
            '@main.info.customer.type' => "customer-type",
            '@main.info.customer.walk.in.name' => sprintf('%s input[name="customer-walk-in-name"]', $info),

            '@warning.modal' => '#warning',
            '@warning.ok' => '#warning input[name="ok"]',
            '@warning.close' => '#warning div.modal-header button',
            '@warning.cancel' => '#warning input[name="cancel"]'
        ];
    }
}