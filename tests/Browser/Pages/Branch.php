<?php
namespace Tests\Browser\Pages;
use Laravel\Dusk\Browser;
use Lang;
class Branch extends Page
{
    public function url()
    {
        return '/branch';
    }
    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/branch')
                ->assertTitle('Branch');
    }
    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = [
            'list' => '#table-list tbody',
            'detail' => '#table-modal tbody'
        ];

        $modal = '#modal-detail';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = [
            'list' => '#confirm-list',
            'detail' => '#confirm-detail'
        ];

        return [
            '@create' => '#controls div[name="add"]',
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.business.name' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.select' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.condition.first.label' => $this->table($filter['conditions'])->row(1)->cell(1)->get(),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),

            '@list.body.first.code' => $this->table($table['list'])->row(1)->cell(1)->element('span')->get(),
            '@list.body.first.name' => $this->table($table['list'])->row(1)->cell(2)->element('span')->get(),
            '@list.body.first.address' => $this->table($table['list'])->row(1)->cell(3)->element('span')->get(),
            '@list.body.first.contact' => $this->table($table['list'])->row(1)->cell(4)->element('span')->get(),
            '@list.body.first.business.name' => $this->table($table['list'])->row(1)->cell(5)->element('span')->get(),
            '@list.body.first.action' => $this->table($table['list'])->row(1)->cell(6)->element('img')->get(),

            '@list.body.second.code' => $this->table($table['list'])->row(2)->cell(1)->element('span')->get(),
            '@list.body.second.name' => $this->table($table['list'])->row(2)->cell(2)->element('span')->get(),
            '@list.body.second.address' => $this->table($table['list'])->row(2)->cell(3)->element('span')->get(),
            '@list.body.second.contact' => $this->table($table['list'])->row(2)->cell(4)->element('span')->get(),
            '@list.body.second.business.name' => $this->table($table['list'])->row(2)->cell(5)->element('span')->get(),
            '@list.body.second.action' => $this->table($table['list'])->row(2)->cell(6)->element('img')->get(),

            '@list.body.third.code' => $this->table($table['list'])->row(3)->cell(1)->element('span')->get(),
            '@list.body.third.name' => $this->table($table['list'])->row(3)->cell(2)->element('span')->get(),
            '@list.body.third.address' => $this->table($table['list'])->row(3)->cell(3)->element('span')->get(),
            '@list.body.third.contact' => $this->table($table['list'])->row(3)->cell(4)->element('span')->get(),
            '@list.body.third.business.name' => $this->table($table['list'])->row(3)->cell(5)->element('span')->get(),
            '@list.body.third.action' => $this->table($table['list'])->row(3)->cell(6)->element('img')->get(),

            '@head.code' => sprintf('%s input[name="info-code"]', $modal),
            '@head.name' => sprintf('%s input[name="info-name"]', $modal),
            '@head.contact' => sprintf('%s input[name="info-contact"]', $modal),
            '@head.address' => sprintf('%s textarea[name="info-address"]', $modal),
            '@head.business.name' => sprintf('%s input[name="info-business-name"]', $modal),

            '@detail.first.code' => $this->table($table['detail'])->row(1)->cell(1)->element('input')->get(),
            '@detail.first.name' => $this->table($table['detail'])->row(1)->cell(2)->element('input')->get(),
            '@detail.first.contact' => $this->table($table['detail'])->row(1)->cell(3)->element('input')->get(),
            '@detail.first.address' => $this->table($table['detail'])->row(1)->cell(4)->element('input')->get(),
            '@detail.first.business.name' => $this->table($table['detail'])->row(1)->cell(5)->element('input')->get(),
            '@detail.first.action' => $this->table($table['detail'])->row(1)->cell(6)->element('img')->get(),

            '@detail.second.code' => $this->table($table['detail'])->row(2)->cell(1)->element('input')->get(),
            '@detail.second.name' => $this->table($table['detail'])->row(2)->cell(2)->element('input')->get(),
            '@detail.second.contact' => $this->table($table['detail'])->row(2)->cell(3)->element('input')->get(),
            '@detail.second.address' => $this->table($table['detail'])->row(2)->cell(4)->element('input')->get(),
            '@detail.second.business.name' => $this->table($table['detail'])->row(2)->cell(5)->element('input')->get(),
            '@detail.second.action' => $this->table($table['detail'])->row(2)->cell(6)->element('img')->get(),

            '@detail.third.code' => $this->table($table['detail'])->row(3)->cell(1)->element('input')->get(),
            '@detail.third.name' => $this->table($table['detail'])->row(3)->cell(2)->element('input')->get(),
            '@detail.third.contact' => $this->table($table['detail'])->row(3)->cell(3)->element('input')->get(),
            '@detail.third.address' => $this->table($table['detail'])->row(3)->cell(4)->element('input')->get(),
            '@detail.third.business.name' => $this->table($table['detail'])->row(3)->cell(5)->element('input')->get(),
            '@detail.third.action' => $this->table($table['detail'])->row(3)->cell(6)->element('img')->get(),

            '@close.detail' => sprintf('%s button.close', $modal),
            '@save' => sprintf('%s input[name="save"]', $modal),
            '@modal.detail' => $modal,
            '@list.confirm' => $confirm['list'],
            '@list.confirm.ok' => sprintf('%s input[name="ok"]', $confirm['list']),
            '@list.confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm['list']),
            '@detail.confirm' => $confirm['detail'],
            '@detail.confirm.ok' => sprintf('%s input[name="ok"]', $confirm['detail']),
            '@detail.confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm['detail']),
        ];
    }
}
