<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

abstract class SystemCode extends Page
{
    abstract public function title();

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitle($this->title());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = '#table-list tbody';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = '#confirm';

        return [
            '@first.row.code' => $this->table($table)->row(1)->cell(1)->element('div > input[type=text]')->get(),
            '@first.row.name' => $this->table($table)->row(1)->cell(2)->element('div > input[type=text]')->get(),
            '@first.row.remarks' => $this->table($table)->row(1)->cell(3)->element('div > input[type=text]')->get(),
            '@first.row.remarks' => $this->table($table)->row(1)->cell(3)->element('div > input[type=text]')->get(),
            '@first.row.command' => $this->table($table)->row(1)->cell(4)->element('div > img')->get(),
            '@second.row.code' => $this->table($table)->row(2)->cell(1)->element('div > input[type=text]')->get(),
            '@second.row.name' => $this->table($table)->row(2)->cell(2)->element('div > input[type=text]')->get(),
            '@second.row.remarks' => $this->table($table)->row(2)->cell(3)->element('div > input[type=text]')->get(),
            '@second.row.remarks' => $this->table($table)->row(2)->cell(3)->element('div > input[type=text]')->get(),
            '@second.row.command' => $this->table($table)->row(2)->cell(4)->element('div > img')->get(),
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.remarks' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filters.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filters.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filters.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),
            '@confirm' => $confirm,
            '@ok' => sprintf('%s input[name="ok"]', $confirm),
            '@cancel' => sprintf('%s input[name="cancel"]', $confirm),
        ];
    }
}
