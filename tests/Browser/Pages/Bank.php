<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Bank extends SystemCode
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/bank';
    }

    /**
     * Get the title for the page.
     *
     * @return string
     */
    public function title()
    {
        return Lang::get('core::label.bank');
    }
}
