<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Login extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitle(Lang::get('core::label.login'));
    }

    /**
     * Fill username, password and click login button
     * 
     * @param  Browser $browser 
     * @param  string  $username
     * @param  string  $password
     */
    public function fill(Browser $browser, $username = '', $password = '')
    {
        $browser->type('@username', $username)
                ->type('@password', $password);
    }

    /**
     * Click login and wait for 3 seconds
     * 
     */
    public function loginAndWait(Browser $browser)
    {
        $this->clickAndWait($browser, '@login');
    }

    /**
     * Click auth button and wait for 3 seconds
     * 
     */
    public function authAndWait(Browser $browser)
    {
        $this->clickAndWait($browser, '@auth');
    }

    /**
     * Check if there are values in the list of branches on modal
     * 
     * @param  Browser $browser
     * @param  string  $field
     */
    public function assertBranches(Browser $browser, $field)
    {
        $options = $browser->selectOption($field);

        $browser->assertTrue(count($options) > 0);
    }

    /**
     * Click the specific selector and wait for 3 seconds
     * 
     * @param  Browser $browser 
     * @param  string  $selector
     */
    private function clickAndWait(Browser $browser, $selector)
    {
        $browser->click($selector)
                ->pause(3000);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@username' => 'input[name=username]',
            '@password' => 'input[name=password]',
            '@login' => 'input[name=login]',
            '@modal' => '#branch-modal',
            '@branches' => 'select[name=branches]',
            '@auth' => 'input[name=auth]',
            '@ss' => 'button[name=logout]'
        ];
    }
}
