<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Settings extends Page
{
    public function url()
    {
        return '/settings';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/settings')
                ->assertTitle(Lang::get('core::label.settings'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        return array_merge(
            array(
                '@save' => '#save > div'
            ),
            $this->sectionElements(),
            $this->settingsElements()
        );
    }

    private function sectionElements()
    {

        $section = '#system_section ';

        return [
            '@section.general' => sprintf("%s li:nth-child(1)", $section),
            '@section.product' => sprintf("%s li:nth-child(2)", $section),
            '@section.purchase.order' => sprintf("%s li:nth-child(3)", $section),
        ];
    }

    private function settingsElements()
    {
        $settings = [
            'general' => "#general_settings",
            'purchase_order' => "#purchase_order_settings ",
        ];

        $purchase_order_setting = [
            'approval' => sprintf('%s #approval', $settings['purchase_order']),
            'disable_invoice' => sprintf('%s #disable_invoice', $settings['purchase_order']),
            'price_history_based' => sprintf('%s #price_history_based ', $settings['purchase_order']),
            'big_qty_history_based' => sprintf('%s #big_qty_history_based', $settings['purchase_order']),
            'warning_already_purchased' => sprintf('%s #warning_already_purchased ', $settings['purchase_order']),
            'warning_already_purchased_not_received' => sprintf('%s #warning_already_purchased_not_received', $settings['purchase_order']),
            'warning_already_purchased_received' => sprintf('%s #warning_already_purchased_received', $settings['purchase_order']),
        ];

        return [
            '@setting.general.no.pos.terminals' => sprintf('%s input[name="total_number_pos_terminals"]', $settings['general']),
            '@setting.general.monetary.precision' => sprintf('%s input[name="monetary_precision"]', $settings['general']),
            '@setting.purchase.order.action.approve' => sprintf('%s input[name="purchase_control_action_approve"]', $purchase_order_setting['approval']),
            '@setting.purchase.order.action.for.approval' => sprintf('%s input[name="purchase_control_action_for_approval"]', $purchase_order_setting['approval']),
            '@setting.purchase.order.action.revert.approved' => sprintf('%s input[name="purchase_control_action_revert_approved"]', $purchase_order_setting['approval']),
            '@setting.purchase.order.action.revert.declined' => sprintf('%s input[name="purchase_control_action_revert_declined"]', $purchase_order_setting['approval']),
            '@setting.purchase.order.disable.invoice.yes' => sprintf('%s input[name="purchase_disable_invoice_field_yes"]', $purchase_order_setting['disable_invoice']),
            '@setting.purchase.order.disable.invoice.no' => sprintf('%s input[name="purchase_disable_invoice_field_no"]', $purchase_order_setting['disable_invoice']),
            '@setting.purchase.order.price.history.based.yes' => sprintf('%s input[name="purchase_historical_price_yes"]', $purchase_order_setting['price_history_based']),
            '@setting.purchase.order.price.history.based.no' => sprintf('%s input[name="purchase_historical_price_no"]', $purchase_order_setting['price_history_based']),
            '@setting.purchase.order.big.qty.history.based.yes' => sprintf('%s input[name="purchase_historical_unit_yes"]', $purchase_order_setting['big_qty_history_based']),
            '@setting.purchase.order.big.qty.history.based.no' => sprintf('%s input[name="purchase_historical_unit_no"]', $purchase_order_setting['big_qty_history_based']),
            '@setting.purchase.order.warning.already.purchased.toggle' => sprintf('%s input[name="purchase_warning_already_purchased_toggle"]', $purchase_order_setting['warning_already_purchased']),
            '@setting.purchase.order.warning.already.purchased.days' => sprintf('%s input[name="purchase_warning_already_purchased_days"]', $purchase_order_setting['warning_already_purchased']),
            '@setting.purchase.order.warning.already.purchased.not.received.toggle' => sprintf('%s input[name="purchase_warning_not_received_toggle"]', $purchase_order_setting['warning_already_purchased_not_received']),
            '@setting.purchase.order.warning.already.purchased.not.received.days' => sprintf('%s input[name="purchase_warning_not_received_days"]', $purchase_order_setting['warning_already_purchased_not_received']),
            '@setting.purchase.order.warning.already.purchased.received.toggle' => sprintf('%s input[name="purchase_warning_purchased_and_received_toggle"]', $purchase_order_setting['warning_already_purchased_received']),
            '@setting.purchase.order.warning.already.purchased.received.days' => sprintf('%s input[name="purchase_warning_purchased_and_received_days"]', $purchase_order_setting['warning_already_purchased_received']),
        ];
    }
}
