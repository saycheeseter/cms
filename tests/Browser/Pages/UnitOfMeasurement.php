<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class UnitOfMeasurement extends SystemCode
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/uom';
    }

    /**
     * Get the title for the page.
     *
     * @return string
     */
    public function title()
    {
        return Lang::get('core::label.uom');
    }
}
