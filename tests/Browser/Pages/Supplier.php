<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Supplier extends Page
{
    public function url()
    {
        return '/supplier';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/supplier')
                ->assertTitle(Lang::get('core::label.supplier'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = '#table-list tbody';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = '#confirm';

        return [
            '@first.row.code' => $this->table($table)->row(1)->cell(1)->element('span')->get(),
            '@first.row.name' => $this->table($table)->row(1)->cell(2)->element('span')->get(),
            '@first.row.chinese.name' => $this->table($table)->row(1)->cell(3)->element('span')->get(),
            '@first.row.contact' => $this->table($table)->row(1)->cell(4)->element('span')->get(),
            '@first.row.command' => $this->table($table)->row(1)->cell(5)->element('img')->get(),
            '@second.row.code' => $this->table($table)->row(2)->cell(1)->element('span')->get(),
            '@second.row.name' => $this->table($table)->row(2)->cell(2)->element('span')->get(),
            '@second.row.chinese.name' => $this->table($table)->row(2)->cell(3)->element('span')->get(),
            '@second.row.contact' => $this->table($table)->row(2)->cell(4)->element('span')->get(),
            '@second.row.command' => $this->table($table)->row(2)->cell(5)->element('img')->get(),
            '@create' => '#controls div[name="add"]',
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.full.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.chinese.name' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.memo' => sprintf('%s li:nth-child(4)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filters.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filters.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filters.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),
            '@confirm' => $confirm,
            '@ok' => sprintf('%s input[name="ok"]', $confirm),
            '@cancel' => sprintf('%s input[name="cancel"]', $confirm),
            '@code' => 'input[name="code"]',
            '@full.name' => 'input[name="full_name"]',
            '@term' => 'input[name="term"] ',
            '@email' => 'input[name="email"]',
            '@save' => 'button[name="save"]',
            '@basic' => 'div[name="basic"]',
            '@advance' => 'div[name="advance"]',
        ];
    }
}
