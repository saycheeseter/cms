<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class UserPermission extends Page
{
    public function url()
    {
        return '/user';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/user')
                ->assertTitle(Lang::get('core::label.user.list'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        return array_merge(
            $this->listElements(),
            $this->detailElements()
        );
    }

    private function listElements()
    {
        $table = '#table-list tbody';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $confirm = '#confirm';

        return [
            '@create' => '#controls div[name="create"]',

            '@filters' => '#controls div[name="filter"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.code' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.full.name' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.user.name' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.position' => sprintf('%s li:nth-child(4)', $filter['options']),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input')->get(),

            '@first.row.code' => $this->table($table)->row(1)->cell(1)->element('span')->get(),
            '@first.row.full.name' => $this->table($table)->row(1)->cell(2)->element('span')->get(),
            '@first.row.user.name' => $this->table($table)->row(1)->cell(3)->element('span')->get(),
            '@first.row.delete' => $this->table($table)->row(1)->cell(6)->element('img')->get(),

            '@second.row.code' => $this->table($table)->row(2)->cell(1)->element('span')->get(),
            '@second.row.full.name' => $this->table($table)->row(2)->cell(2)->element('span')->get(),
            '@second.row.user.name' => $this->table($table)->row(2)->cell(3)->element('span')->get(),
            '@second.row.delete' => $this->table($table)->row(2)->cell(6)->element('img')->get(),

            '@confirm' => $confirm,
            '@confirm.ok' => sprintf('%s input[name="ok"]', $confirm),
            '@confirm.cancel' => sprintf('%s input[name="cancel"]', $confirm),
        ];
    }

    private function detailElements()
    {
        $permissionContainer = 'div.user-permissions';

        $branchChosen = $permissionContainer.' div.chosen[name="branch"]';

        return [
            '@save' => '#controls div[name="save"]',

            '@code' => 'input[name="code"]',
            '@user.name' => 'input[name="user-name"]',
            '@full.name' => 'input[name="full-name"]',
            '@password' => 'input[name="password"]',
            '@email' => 'input[name="email"]',
            '@telephone' => 'input[name="telephone"]',
            '@mobile' => 'input[name="mobile"]',
            '@address' => 'textarea[name="address"]',
            '@memo' => 'textarea[name="memo"]',

            '@tab.info' => 'div[name="info"]',
            '@tab.permissions' => 'div[name="permissions"]',

            '@branch' => 'div.chosen[name="branch"]',
            '@sections' => '#sections',
            
            '@section.pos' => 'li[name="pos"]',
            '@section.pos.checkbox' => 'input[name="pos"]',

            '@section.general' => 'li[name="general"]',
            '@section.general.checkbox' => 'input[name="general"]',

            '@section.data' => 'li[name="data"]',
            '@section.data.checkbox' => 'input[name="data"]',

            '@login.permission' => 'input.permissions[value="1"]',
            '@view.pbc.permission' => 'input.permissions[value="2"]',
            '@view.detail.pbc.permission' => 'input.permissions[value="3"]',
            '@create.pbc.permission' => 'input.permissions[value="4"]',
            '@edit.pbc.permission' => 'input.permissions[value="5"]',
            '@delete.pbc.permission' => 'input.permissions[value="6"]',
        ];
    }
}
