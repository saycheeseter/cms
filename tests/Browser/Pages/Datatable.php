<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Datatable extends Page
{
    public function url()
    {
        return '/brand';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/brand')
                ->assertTitle('Brand');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        $pagination = 'div.grid-pagination';

        $table = '#table-list tbody';

        $page = [
            'per' => sprintf('%s  div.per-page input[type="text"]', $pagination),
            'previous' => sprintf('%s input[name="previous"]', $pagination),
            'next' => sprintf('%s input[name="next"]', $pagination),
            'current' => sprintf('%s  div.current-page input[type="text"]', $pagination),
            'total' => sprintf('%s span.total-pages', $pagination),
        ];

        $confirm = '#confirm';

        return [
            '@pagination.previous' => $page['previous'],
            '@pagination.next' => $page['next'],
            '@pagination.current' =>  $page['current'],
            '@pagination.total' =>  $page['total'],
            '@pagination.per' =>  $page['per'],

            '@first.row.code' => $this->table($table)->row(1)->cell(1)->element('div > input[type=text]')->get(),
            '@first.row.name' => $this->table($table)->row(1)->cell(2)->element('div > input[type=text]')->get(),
            '@first.row.remarks' => $this->table($table)->row(1)->cell(3)->element('div > input[type=text]')->get(),
            '@first.row.command' => $this->table($table)->row(1)->cell(4)->element('div > img')->get(),

            '@fifth.row.code' => $this->table($table)->row(5)->cell(1)->element('div > input[type=text]')->get(),
            '@fifth.row.name' => $this->table($table)->row(5)->cell(2)->element('div > input[type=text]')->get(),
            '@fifth.row.remarks' => $this->table($table)->row(5)->cell(3)->element('div > input[type=text]')->get(),
            '@fifth.row.command' => $this->table($table)->row(5)->cell(4)->element('div > img')->get(),

            '@confirm' => $confirm,
            '@ok' => sprintf('%s input[name="ok"]', $confirm),
            '@cancel' => sprintf('%s input[name="cancel"]', $confirm),
        ];
    }
}
