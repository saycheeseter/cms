<?php 

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class StockDelivery extends Invoice
{
    public function url()
    {
        return '/stock-delivery';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/stock-delivery')
                ->assertTitle(Lang::get('core::label.stock.deliveries.to.other.branches'));
    }

    protected function listElements() 
    {
        $table = '#table-list tbody';
        $tableHeader = '#table-list thead';

        return [
            '@columns.settings.modal' => '#column-settings',
            '@columns.settings.transaction.date' => '#column-settings ul li:nth-child(1) span',
            '@columns.settings.created.for' => '#column-settings ul li:nth-child(2) span',
            '@columns.settings.deliver.to' => '#column-settings ul li:nth-child(3) span',
            '@columns.settings.remarks' => '#column-settings ul li:nth-child(4) span',
            '@columns.settings.requested.by' => '#column-settings ul li:nth-child(5) span',
            '@columns.settings.created.by' => '#column-settings ul li:nth-child(6) span',
            '@columns.settings.created.date' => '#column-settings ul li:nth-child(7) span',
            '@columns.settings.delivery.date' => '#column-settings ul li:nth-child(8) span',
            '@columns.settings.audit.by' => '#column-settings ul li:nth-child(9) span',
            '@columns.settings.audit.date' => '#column-settings ul li:nth-child(10) span',
            '@columns.settings.deleted' => '#column-settings ul li:nth-child(11) span',
            '@columns.settings.approval.status' => '#column-settings ul li:nth-child(12) span',
            '@columns.settings.transaction.status' => '#column-settings ul li:nth-child(13) span',
            '@columns.settings.amount' => '#column-settings ul li:nth-child(14) span',

            '@columns.settings.transaction.date.checkbox' => '#column-settings ul li:nth-child(1) input[type="checkbox"]',
            '@columns.settings.created.for.checkbox' => '#column-settings ul li:nth-child(2) input[type="checkbox"]',
            '@columns.settings.deliver.to.checkbox' => '#column-settings ul li:nth-child(3) input[type="checkbox"]',
            '@columns.settings.remarks.checkbox' => '#column-settings ul li:nth-child(4) input[type="checkbox"]',
            '@columns.settings.requested.by.checkbox' => '#column-settings ul li:nth-child(5) input[type="checkbox"]',
            '@columns.settings.created.by.checkbox' => '#column-settings ul li:nth-child(6) input[type="checkbox"]',
            '@columns.settings.created.date.checkbox' => '#column-settings ul li:nth-child(7) input[type="checkbox"]',
            '@columns.settings.delivery.date.checkbox' => '#column-settings ul li:nth-child(8) input[type="checkbox"]',
            '@columns.settings.audit.by.checkbox' => '#column-settings ul li:nth-child(9) input[type="checkbox"]',
            '@columns.settings.audit.date.checkbox' => '#column-settings ul li:nth-child(10) input[type="checkbox"]',
            '@columns.settings.deleted.checkbox' => '#column-settings ul li:nth-child(11) input[type="checkbox"]',
            '@columns.settings.approval.status.checkbox' => '#column-settings ul li:nth-child(12) input[type="checkbox"]',
            '@columns.settings.transaction.status.checkbox' => '#column-settings ul li:nth-child(13) input[type="checkbox"]',
            '@columns.settings.amount.checkbox' => '#column-settings ul li:nth-child(14) input[type="checkbox"]',

            '@columns.settings.apply.selection' => '#column-settings div.modal-footer button',

            '@headers.checkall' => $this->table($tableHeader)->row(1)->header(1)->element("input")->get(),
            '@headers.transaction.date' => $this->table($tableHeader)->row(1)->header(2)->element("div")->get(),
            '@headers.sheet.no' => $this->table($tableHeader)->row(1)->header(3)->element("div")->get(),
            '@headers.created.for' => $this->table($tableHeader)->row(1)->header(4)->element("div")->get(),
            '@headers.deliver.to' => $this->table($tableHeader)->row(1)->header(5)->element("div")->get(),
            '@headers.remarks' => $this->table($tableHeader)->row(1)->header(6)->element("div")->get(),
            '@headers.requested.by' => $this->table($tableHeader)->row(1)->header(7)->element("div")->get(),
            '@headers.created.by' => $this->table($tableHeader)->row(1)->header(8)->element("div")->get(),
            '@headers.created.date' => $this->table($tableHeader)->row(1)->header(9)->element("div")->get(),
            '@headers.delivery.date' => $this->table($tableHeader)->row(1)->header(10)->element("div")->get(),
            '@headers.audit.by' => $this->table($tableHeader)->row(1)->header(11)->element("div")->get(),
            '@headers.audit.date' => $this->table($tableHeader)->row(1)->header(12)->element("div")->get(),
            '@headers.deleted' => $this->table($tableHeader)->row(1)->header(13)->element("div")->get(),
            '@headers.approval.status' => $this->table($tableHeader)->row(1)->header(14)->element("div")->get(),
            '@headers.transaction.status' => $this->table($tableHeader)->row(1)->header(15)->element("div")->get(),
            '@headers.amount' => $this->table($tableHeader)->row(1)->header(16)->element("div")->get(),

            '@first.row.checkbox' => $this->table($table)->row(1)->cell(1)->element("input")->get(),
            '@first.row.transaction.date' => $this->table($table)->row(1)->cell(2)->element("span")->get(),
            '@first.row.sheet.no' => $this->table($table)->row(1)->cell(3)->element("span")->get(),
            '@first.row.created.for' => $this->table($table)->row(1)->cell(4)->element("span")->get(),
            '@first.row.deliver.to' => $this->table($table)->row(1)->cell(5)->element("span")->get(),
            '@first.row.remarks' => $this->table($table)->row(1)->cell(6)->element("span")->get(),
            '@first.row.requested.by' => $this->table($table)->row(1)->cell(7)->element("span")->get(),
            '@first.row.created.by' => $this->table($table)->row(1)->cell(8)->element("span")->get(),
            '@first.row.created.date' => $this->table($table)->row(1)->cell(9)->element("span")->get(),
            '@first.roww.delivery.date' => $this->table($tableHeader)->row(1)->cell(10)->element("span")->get(),
            '@first.row.audit.by' => $this->table($table)->row(1)->cell(11)->element("span")->get(),
            '@first.row.audit.date' => $this->table($table)->row(1)->cell(12)->element("span")->get(),
            '@first.row.deleted' => $this->table($table)->row(1)->cell(13)->element("span")->get(),
            '@first.row.approval.status' => $this->table($table)->row(1)->cell(14)->element("span")->get(),
            '@first.row.transaction.status' => $this->table($table)->row(1)->cell(15)->element("span")->get(),
            '@first.row.amount' => $this->table($table)->row(1)->cell(16)->element("span")->get(),
            '@first.row.delete.button' => $this->table($table)->row(1)->cell(17)->element("div")->get(),

            '@second.row.checkbox' => $this->table($table)->row(2)->cell(1)->element("input")->get(),
            '@second.row.transaction.date' => $this->table($table)->row(2)->cell(2)->element("span")->get(),
            '@second.row.sheet.no' => $this->table($table)->row(2)->cell(3)->element("span")->get(),
            '@second.row.created.for' => $this->table($table)->row(2)->cell(4)->element("span")->get(),
            '@second.row.deliver.to' => $this->table($table)->row(2)->cell(5)->element("span")->get(),
            '@second.row.remarks' => $this->table($table)->row(2)->cell(6)->element("span")->get(),
            '@second.row.requested.by' => $this->table($table)->row(2)->cell(7)->element("span")->get(),
            '@second.row.created.by' => $this->table($table)->row(2)->cell(8)->element("span")->get(),
            '@second.row.created.date' => $this->table($table)->row(2)->cell(9)->element("span")->get(),
            '@second.roww.delivery.date' => $this->table($tableHeader)->row(2)->cell(10)->element("span")->get(),
            '@second.row.audit.by' => $this->table($table)->row(2)->cell(11)->element("span")->get(),
            '@second.row.audit.date' => $this->table($table)->row(2)->cell(12)->element("span")->get(),
            '@second.row.deleted' => $this->table($table)->row(2)->cell(13)->element("span")->get(),
            '@second.row.approval.status' => $this->table($table)->row(2)->cell(14)->element("span")->get(),
            '@second.row.transaction.status' => $this->table($table)->row(2)->cell(15)->element("span")->get(),
            '@second.row.amount' => $this->table($table)->row(2)->cell(16)->element("span")->get(),
            '@second.row.delete.button' => $this->table($table)->row(2)->cell(17)->element("div")->get(),

            '@third.row.checkbox' => $this->table($table)->row(3)->cell(1)->element("input")->get(),
            '@fourth.row.checkbox' => $this->table($table)->row(4)->cell(1)->element("input")->get(),

            '@delete.invoice.modal' => '#confirm',
            '@delete.invoice.ok' => '#confirm div.modal-footer input[name="ok"]'
        ];
    }

    protected function detailElements() 
    {
        $containers = [
            'mainInfo' => '#main-info',
        ];

        $chosen = [
            'requested_by' => $containers['mainInfo'].' div.chosen[name="requested-by"]',
            'created_for' => $containers['mainInfo'].' div.chosen[name="created-for"]',
            'deliver_to' => $containers['mainInfo'].' div.chosen[name="deliver-to"]'
        ];

        return [
            '@container.main.info' => $containers['mainInfo'],

            '@main.info.sheet.no' => sprintf('%s span[name="sheet-no"]', $containers['mainInfo']),
            '@main.info.remarks' => sprintf('%s textarea[name="remarks"]', $containers['mainInfo']),
            '@main.info.delivery.date' => sprintf('%s div[name="delivery-date"]', $containers['mainInfo']),
            '@main.info.date.time' => sprintf('%s div[name="date-time"]', $containers['mainInfo']),
            '@main.info.stock.request.invoice' => sprintf('%s input[name="stock-request-invoice"]', $containers['mainInfo']),
            '@main.info.stock.request.import' => sprintf('%s input[name="stock-request-import"]', $containers['mainInfo']),

            '@main.info.date.time.mask' => sprintf('%s div.mask', $containers['mainInfo']),

            '@main.info.requested.by' => $chosen['requested_by'],
            '@main.info.created.for' => $chosen['created_for'],
            '@main.info.deliver.to' => $chosen['created_for'],

            '@warning.modal' => '#warning',
            '@warning.yes' => '#warning input[name="ok"]',
            '@warning.close' => '#warning div.modal-header button',
            '@warning.cancel' => '#warning input[name="cancel"]'
        ];
    }
}