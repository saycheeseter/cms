<?php 

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Purchase extends Invoice
{
    public function url()
    {
        return '/purchase';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/purchase')
                ->assertTitle(Lang::get('core::label.purchase.order.list'));
    }

    protected function listElements() 
    {
        $table = '#table-list tbody';
        $tableHeader = '#table-list thead';

        return [
            '@columns.settings.modal' => '#column-settings',
            '@columns.settings.transaction.date' => '#column-settings ul li:nth-child(1) span',
            '@columns.settings.order.from' => '#column-settings ul li:nth-child(2) span',
            '@columns.settings.order.for' => '#column-settings ul li:nth-child(3) span',
            '@columns.settings.supplier' => '#column-settings ul li:nth-child(4) span',
            '@columns.settings.remarks' => '#column-settings ul li:nth-child(5) span',
            '@columns.settings.requested.by' => '#column-settings ul li:nth-child(6) span',
            '@columns.settings.deliver.until' => '#column-settings ul li:nth-child(7) span',
            '@columns.settings.created.by' => '#column-settings ul li:nth-child(8) span',
            '@columns.settings.created.date' => '#column-settings ul li:nth-child(9) span',
            '@columns.settings.audit.by' => '#column-settings ul li:nth-child(10) span',
            '@columns.settings.audit.date' => '#column-settings ul li:nth-child(11) span',
            '@columns.settings.deleted' => '#column-settings ul li:nth-child(12) span',
            '@columns.settings.approval.status' => '#column-settings ul li:nth-child(13) span',
            '@columns.settings.transaction.status' => '#column-settings ul li:nth-child(14) span',
            '@columns.settings.amount' => '#column-settings ul li:nth-child(15) span',

            '@columns.settings.transaction.date.checkbox' => '#column-settings ul li:nth-child(1) input[type="checkbox"]',
            '@columns.settings.order.from.checkbox' => '#column-settings ul li:nth-child(2) input[type="checkbox"]',
            '@columns.settings.order.for.checkbox' => '#column-settings ul li:nth-child(3) input[type="checkbox"]',
            '@columns.settings.supplier.checkbox' => '#column-settings ul li:nth-child(4) input[type="checkbox"]',
            '@columns.settings.remarks.checkbox' => '#column-settings ul li:nth-child(5) input[type="checkbox"]',
            '@columns.settings.requested.by.checkbox' => '#column-settings ul li:nth-child(6) input[type="checkbox"]',
            '@columns.settings.deliver.until.checkbox' => '#column-settings ul li:nth-child(7) input[type="checkbox"]',
            '@columns.settings.created.by.checkbox' => '#column-settings ul li:nth-child(8) input[type="checkbox"]',
            '@columns.settings.created.date.checkbox' => '#column-settings ul li:nth-child(9) input[type="checkbox"]',
            '@columns.settings.audit.by.checkbox' => '#column-settings ul li:nth-child(10) input[type="checkbox"]',
            '@columns.settings.audit.date.checkbox' => '#column-settings ul li:nth-child(11) input[type="checkbox"]',
            '@columns.settings.deleted.checkbox' => '#column-settings ul li:nth-child(12) input[type="checkbox"]',
            '@columns.settings.approval.status.checkbox' => '#column-settings ul li:nth-child(13) input[type="checkbox"]',
            '@columns.settings.transaction.status.checkbox' => '#column-settings ul li:nth-child(14) input[type="checkbox"]',
            '@columns.settings.amount.checkbox' => '#column-settings ul li:nth-child(15) input[type="checkbox"]',

            '@columns.settings.apply.selection' => '#column-settings div.modal-footer button',

            '@headers.checkall' => $this->table($tableHeader)->row(1)->header(1)->element("input")->get(),
            '@headers.transaction.date' => $this->table($tableHeader)->row(1)->header(2)->element("div")->get(),
            '@headers.sheet.no' => $this->table($tableHeader)->row(1)->header(3)->element("div")->get(),
            '@headers.order.from' => $this->table($tableHeader)->row(1)->header(4)->element("div")->get(),
            '@headers.order.for' => $this->table($tableHeader)->row(1)->header(5)->element("div")->get(),
            '@headers.supplier' => $this->table($tableHeader)->row(1)->header(6)->element("div")->get(),
            '@headers.remarks' => $this->table($tableHeader)->row(1)->header(7)->element("div")->get(),
            '@headers.requested.by' => $this->table($tableHeader)->row(1)->header(8)->element("div")->get(),
            '@headers.deliver.until' => $this->table($tableHeader)->row(1)->header(9)->element("div")->get(),
            '@headers.created.by' => $this->table($tableHeader)->row(1)->header(10)->element("div")->get(),
            '@headers.created.date' => $this->table($tableHeader)->row(1)->header(11)->element("div")->get(),
            '@headers.audit.by' => $this->table($tableHeader)->row(1)->header(12)->element("div")->get(),
            '@headers.audit.date' => $this->table($tableHeader)->row(1)->header(13)->element("div")->get(),
            '@headers.deleted' => $this->table($tableHeader)->row(1)->header(14)->element("div")->get(),
            '@headers.approval.status' => $this->table($tableHeader)->row(1)->header(15)->element("div")->get(),
            '@headers.transaction.status' => $this->table($tableHeader)->row(1)->header(16)->element("div")->get(),
            '@headers.amount' => $this->table($tableHeader)->row(1)->header(17)->element("div")->get(),

            '@first.row.checkbox' => $this->table($table)->row(1)->cell(1)->element("input")->get(),
            '@first.row.transaction.date' => $this->table($table)->row(1)->cell(2)->element("span")->get(),
            '@first.row.sheet.no' => $this->table($table)->row(1)->cell(3)->element("span")->get(),
            '@first.row.order.from' => $this->table($table)->row(1)->cell(4)->element("span")->get(),
            '@first.row.order.for' => $this->table($table)->row(1)->cell(5)->element("span")->get(),
            '@first.row.supplier' => $this->table($table)->row(1)->cell(6)->element("span")->get(),
            '@first.row.remarks' => $this->table($table)->row(1)->cell(7)->element("span")->get(),
            '@first.row.requested.by' => $this->table($table)->row(1)->cell(8)->element("span")->get(),
            '@first.row.deliver.until' => $this->table($table)->row(1)->cell(9)->element("span")->get(),
            '@first.row.created.by' => $this->table($table)->row(1)->cell(10)->element("span")->get(),
            '@first.row.created.date' => $this->table($table)->row(1)->cell(11)->element("span")->get(),
            '@first.row.audit.by' => $this->table($table)->row(1)->cell(12)->element("span")->get(),
            '@first.row.audit.date' => $this->table($table)->row(1)->cell(13)->element("span")->get(),
            '@first.row.deleted' => $this->table($table)->row(1)->cell(14)->element("span")->get(),
            '@first.row.approval.status' => $this->table($table)->row(1)->cell(15)->element("span")->get(),
            '@first.row.transaction.status' => $this->table($table)->row(1)->cell(16)->element("span")->get(),
            '@first.row.amount' => $this->table($table)->row(1)->cell(17)->element("span")->get(),
            '@first.row.delete.button' => $this->table($table)->row(1)->cell(18)->element("div")->get(),

            '@second.row.checkbox' => $this->table($table)->row(2)->cell(1)->element("input")->get(),
            '@second.row.transaction.date' => $this->table($table)->row(2)->cell(2)->element("span")->get(),
            '@second.row.sheet.no' => $this->table($table)->row(2)->cell(3)->element("span")->get(),
            '@second.row.order.from' => $this->table($table)->row(2)->cell(4)->element("span")->get(),
            '@second.row.order.for' => $this->table($table)->row(2)->cell(5)->element("span")->get(),
            '@second.row.supplier' => $this->table($table)->row(2)->cell(6)->element("span")->get(),
            '@second.row.remarks' => $this->table($table)->row(2)->cell(7)->element("span")->get(),
            '@second.row.requested.by' => $this->table($table)->row(2)->cell(8)->element("span")->get(),
            '@second.row.deliver.until' => $this->table($table)->row(2)->cell(9)->element("span")->get(),
            '@second.row.created.by' => $this->table($table)->row(2)->cell(10)->element("span")->get(),
            '@second.row.created.date' => $this->table($table)->row(2)->cell(11)->element("span")->get(),
            '@second.row.audit.by' => $this->table($table)->row(2)->cell(12)->element("span")->get(),
            '@second.row.audit.date' => $this->table($table)->row(2)->cell(13)->element("span")->get(),
            '@second.row.deleted' => $this->table($table)->row(2)->cell(14)->element("span")->get(),
            '@second.row.approval.status' => $this->table($table)->row(2)->cell(15)->element("span")->get(),
            '@second.row.transaction.status' => $this->table($table)->row(2)->cell(16)->element("span")->get(),
            '@second.row.amount' => $this->table($table)->row(2)->cell(17)->element("span")->get(),
            '@second.row.delete.button' => $this->table($table)->row(2)->cell(18)->element("div")->get(),

            '@third.row.checkbox' => $this->table($table)->row(3)->cell(1)->element("input")->get(),
            '@fourth.row.checkbox' => $this->table($table)->row(4)->cell(1)->element("input")->get(),

            '@delete.invoice.modal' => '#confirm',
            '@delete.invoice.ok' => '#confirm div.modal-footer input[name="ok"]'
        ];
    }

    protected function detailElements() 
    {
        $containers = [
            'mainInfo' => '#main-info',
        ];

        $chosen = [
            'supplier' => $containers['mainInfo'].' div.chosen[name="supplier"]',
            'requested_by' => $containers['mainInfo'].' div.chosen[name="requested-by"]',
            'payment_method' => $containers['mainInfo'].' div.chosen[name="payment-method"]',
            'created_for' => $containers['mainInfo'].' div.chosen[name="created-for"]',
            'deliver_to' => $containers['mainInfo'].' div.chosen[name="deliver-to"]'
        ];

        return [
            '@container.main.info' => $containers['mainInfo'],

            '@main.info.sheet.no' => sprintf('%s span[name="sheet-no"]', $containers['mainInfo']),
            '@main.info.term' => sprintf('%s input[name="term"]', $containers['mainInfo']),
            '@main.info.remarks' => sprintf('%s textarea[name="remarks"]', $containers['mainInfo']),
            '@main.info.deliver.until' => sprintf('%s div[name="deliver-until"]', $containers['mainInfo']),
            '@main.info.date.time' => sprintf('%s div[name="date-time"]', $containers['mainInfo']),

            '@chosen.supplier' => $chosen['supplier'],
            '@chosen.supplier.dropdown' => sprintf('%s div.selected', $chosen['supplier']),
            '@chosen.supplier.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['supplier']),
            '@chosen.requested.by' => $chosen['requested_by'],
            '@chosen.requested.by.dropdown' => sprintf('%s div.selected', $chosen['requested_by']),
            '@chosen.requested.by.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['requested_by']),
            '@chosen.payment.method' => $chosen['payment_method'],
            '@chosen.payment.method.dropdown' => sprintf('%s div.selected', $chosen['payment_method']),
            '@chosen.payment.method.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['payment_method']),
            '@chosen.order.for' => $chosen['created_for'],
            '@chosen.order.for.dropdown' => sprintf('%s div.selected', $chosen['created_for']),
            '@chosen.order.for.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['created_for']),
            '@chosen.deliver.to' => $chosen['created_for'],
            '@chosen.deliver.to.dropdown' => sprintf('%s div.selected', $chosen['deliver_to']),
            '@chosen.deliver.to.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['deliver_to']),

            '@warning.modal' => '#warning',
            '@warning.yes' => '#warning input[name="ok"]',
            '@warning.close' => '#warning div.modal-header button',
            '@warning.cancel' => '#warning input[name="cancel"]'
        ];
    }
}