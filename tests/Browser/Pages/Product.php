<?php 

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Product extends Page
{
    public function url()
    {
        return '/product';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/product')
                ->assertTitle(Lang::get('core::product.list'));
    }

    public function elements() 
    {
        return array_merge(
            $this->listElements(),
            $this->detailElements()
        );
    }

    private function listElements() 
    {
        $table = '#tablelist tbody';
        $tableHeader = '#tablelist thead';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        return [
            '@header.row.unit.barcode' => '#tablelist thead tr th:nth-child(1) div',
            '@first.row.unit.barcode' => $this->table($table)->row(1)->cell(1)->element('span')->get(),
            '@first.row.name' => $this->table($table)->row(1)->cell(2)->element('span')->get(),
            '@first.row.inventory' => $this->table($table)->row(1)->cell(7)->element('span')->get(),
            '@first.row.delete' => $this->table($table)->row(1)->cell(8)->element('img')->get(),
            '@create' => '#controls div[name="create"]',
            '@columns.settings' => '#controls div[name="columns"]',
            '@columns.modal' => '#column-modal',
            '@columns.unit.barcode' => '#column-modal ul li:nth-child(1) span',
            '@columns.chinese.name' => '#column-modal ul li:nth-child(2) span',
            '@columns.brand' => '#column-modal ul li:nth-child(3) span',
            '@columns.category' => '#column-modal ul li:nth-child(4) span',
            '@columns.supplier' => '#column-modal ul li:nth-child(5) span',
            '@columns.inventory' => '#column-modal ul li:nth-child(6) span',
            '@columns.unit.barcode.checkbox' => '#column-modal ul li:nth-child(1) input[type="checkbox"]',
            '@columns.apply.selection' => '#column-modal > div.modal-wrapper > div > div.modal-footer > button',
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.stock.no' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.barcode' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.name' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.chinese.name' => sprintf('%s li:nth-child(4)', $filter['options']),
            '@filters.product.status' => sprintf('%s li:nth-child(5)', $filter['options']),
            '@filters.supplier' => sprintf('%s li:nth-child(6)', $filter['options']),
            '@filters.category' => sprintf('%s li:nth-child(7)', $filter['options']),
            '@filters.brand' => sprintf('%s li:nth-child(8)', $filter['options']),
            '@filters.date.created' => sprintf('%s li:nth-child(9)', $filter['options']),
            '@filters.date.modified' => sprintf('%s li:nth-child(10)', $filter['options']),
            '@filters.inventory.status' => sprintf('%s li:nth-child(11)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            
            '@confirm.delete' => '#confirm',
            '@confirm.close' => '#confirm > div.modal-wrapper > div > div.modal-header > button > span',
            '@confirm.cancel' => '#confirm > div.modal-wrapper > div > div.modal-footer > input.c-btn.c-dk-red',
            '@confirm.ok' => '#confirm > div.modal-wrapper > div > div.modal-footer > input.c-btn.c-dk-blue'
        ];
    }

    private function detailElements() 
    {
        $containers = [
            'basicInfo' => 'div.basic-info',
            'advancedInfo' => 'div.advanced-info',
            'productUnit' => 'div.product-unit',
            'productUnitBarcode' => 'div.product-unit-barcode',
            'branchPrice' => 'div.branch-price',
            'branchInfo' => 'div.branch-info',
            'branchDetail' => 'div.branch-detail',
        ];

        $tables = [
            'branchPrice' => '#branch-price-table tbody',
            'branchInfo' => '#branch-info-table tbody',
            'branchDetail' => '#branch-detail-table tbody',
            'productUnit' => '#product-unit-table tbody',
            'productUnitBarcode' => '#unit-barcode-table tbody'
        ];

        $chosen = [
            'supplier' => $containers['basicInfo'].' div.chosen[name="supplier"]',
            'brand' => $containers['advancedInfo'].' div.chosen[name="brand"]',
            'category' => $containers['advancedInfo'].' div.chosen[name="category"]',
            'tax' => $containers['advancedInfo'].' div.chosen[name="tax"]',
            'uom' => $containers['productUnit'].' div.chosen[name="uom"]',
            'barcodeUOM' => $containers['productUnitBarcode'].' div.chosen[name="barcodeUOM"]' 
        ];

        return [
            '@tab.advanced.info' => '#tabs div:nth-child(1)',
            '@tab.product.unit' => '#tabs div:nth-child(2)',
            '@tab.unit.barcode' => '#tabs div:nth-child(3)',
            '@tab.branch.price' => '#tabs div:nth-child(4)',
            '@tab.branch.info' => '#tabs div:nth-child(5)',
            '@tab.branch.detail' => '#tabs div:nth-child(6)',
            
            '@container.basic.info' => $containers['basicInfo'],
            '@container.advanced.info' => $containers['advancedInfo'],
            '@container.product.unit' => $containers['productUnit'],
            '@container.product.unit.barcode' => $containers['productUnitBarcode'],
            '@container.branch.price' => $containers['branchPrice'],
            '@container.branch.info' => $containers['branchInfo'],
            '@container.branch.detail' => $containers['branchDetail'],
            
            '@product.unit.first.row.check' => $this->table($tables['productUnit'])->row(1)->cell(8)->element('img')->get(),
            '@product.unit.first.second.check' => $this->table($tables['productUnit'])->row(2)->cell(8)->element('img')->get(),
            
            '@branch.price.first.row.branch.label' => $this->table($tables['branchPrice'])->row(1)->cell(1)->element('span')->get(),
            '@branch.price.second.row.branch.label' => $this->table($tables['branchPrice'])->row(2)->cell(1)->element('span')->get(),
            '@branch.price.third.row.branch.label' => $this->table($tables['branchPrice'])->row(3)->cell(1)->element('span')->get(),
            
            '@branch.info.first.row.branch.label' => $this->table($tables['branchInfo'])->row(1)->cell(1)->element('span')->get(),
            '@branch.info.second.row.branch.label' => $this->table($tables['branchInfo'])->row(2)->cell(1)->element('span')->get(),
            '@branch.info.third.row.branch.label' => $this->table($tables['branchInfo'])->row(3)->cell(1)->element('span')->get(),
            
            '@branch.detail.first.row.branch.label' => $this->table($tables['branchDetail'])->row(1)->cell(1)->element('span')->get(),
            '@branch.detail.second.row.branch.label' => $this->table($tables['branchDetail'])->row(2)->cell(1)->element('span')->get(),
            '@branch.detail.third.row.branch.label' => $this->table($tables['branchDetail'])->row(3)->cell(1)->element('span')->get(),
            '@branch.detail.first.row.detail.label' => $this->table($tables['branchDetail'])->row(1)->cell(2)->element('span')->get(),
            '@branch.detail.second.row.detail.label' => $this->table($tables['branchDetail'])->row(2)->cell(2)->element('span')->get(),
            '@branch.detail.third.row.detail.label' => $this->table($tables['branchDetail'])->row(3)->cell(2)->element('span')->get(),
            
            '@chosen.supplier.dropdown' => sprintf('%s div.selected', $chosen['supplier']),
            '@chosen.supplier.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['supplier']),
            '@chosen.brand.dropdown' => sprintf('%s div.selected', $chosen['brand']),
            '@chosen.brand.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['brand']),
            '@chosen.category.dropdown' => sprintf('%s div.selected', $chosen['category']),
            '@chosen.category.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['category']),
            '@chosen.tax.dropdown' => sprintf('%s div.selected', $chosen['tax']),
            '@chosen.tax.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['tax']),
            '@chosen.uom.dropdown' => sprintf('%s div.selected', $chosen['uom']),
            '@chosen.uom.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['uom']),
            '@chosen.uom.second.option' => sprintf('%s ul li:nth-child(2)', $chosen['uom']),
            '@chosen.barcodeUOM.dropdown' => sprintf('%s div.selected', $chosen['barcodeUOM']),
            '@chosen.barcodeUOM.first.option' => sprintf('%s ul li:nth-child(1)', $chosen['barcodeUOM']),

            '@product.unit.confirm.ok' => '#uom-confirm > div.modal-wrapper > div > div.modal-footer > input.c-btn.c-dk-blue',

            '@save' => '#controls div[name="save"]',

            '@stock.no' => '#stock-no',
            '@product.name' => '#product-name',

            '@product.unit.first.row.qty' => $this->table($tables['productUnit'])->row(1)->cell(2)->element('input')->get(),
            '@product.unit.first.row.length' => $this->table($tables['productUnit'])->row(1)->cell(3)->element('input')->get(),
            '@product.unit.first.row.width' => $this->table($tables['productUnit'])->row(1)->cell(4)->element('input')->get(),
            '@product.unit.first.row.height' => $this->table($tables['productUnit'])->row(1)->cell(5)->element('input')->get(),
            '@product.unit.first.row.weight' => $this->table($tables['productUnit'])->row(1)->cell(6)->element('input')->get(),

            '@unit.barcode.first.row.uom' => $this->table($tables['productUnitBarcode'])->row(1)->cell(1)->element('div.selected')->get(),
            '@unit.barcode.first.row.barcode' => $this->table($tables['productUnitBarcode'])->row(1)->cell(2)->element('input')->get(),
            '@unit.barcode.first.row.memo' => $this->table($tables['productUnitBarcode'])->row(1)->cell(3)->element('input')->get(),
            '@unit.barcode.first.row.check' => $this->table($tables['productUnitBarcode'])->row(1)->cell(4)->element('img')->get(),
            '@unit.barcode.second.row.uom' => $this->table($tables['productUnitBarcode'])->row(2)->cell(1)->element('div')->get(),
            '@unit.barcode.second.row.barcode' => $this->table($tables['productUnitBarcode'])->row(2)->cell(2)->element('input')->get(),
            '@unit.barcode.third.row.uom' => $this->table($tables['productUnitBarcode'])->row(3)->cell(1)->element('div')->get(),
            '@unit.barcode.third.row.barcode' => $this->table($tables['productUnitBarcode'])->row(3)->cell(2)->element('input')->get(),

            '@branch.price.first.row.cost' => $this->table($tables['branchPrice'])->row(1)->cell(2)->element('input')->get(),
            '@branch.price.first.row.wholesale' => $this->table($tables['branchPrice'])->row(1)->cell(3)->element('input')->get(),
            '@branch.price.first.row.retail' => $this->table($tables['branchPrice'])->row(1)->cell(4)->element('input')->get(),

            '@branch.price.second.row.cost' => $this->table($tables['branchPrice'])->row(2)->cell(2)->element('input')->get(),
            '@branch.price.second.row.wholesale' => $this->table($tables['branchPrice'])->row(2)->cell(3)->element('input')->get(),
            '@branch.price.second.row.retail' => $this->table($tables['branchPrice'])->row(2)->cell(4)->element('input')->get(),

            '@branch.price.third.row.cost' => $this->table($tables['branchPrice'])->row(3)->cell(2)->element('input')->get(),
            '@branch.price.third.row.wholesale' => $this->table($tables['branchPrice'])->row(3)->cell(3)->element('input')->get(),
            '@branch.price.third.row.retail' => $this->table($tables['branchPrice'])->row(3)->cell(4)->element('input')->get(),

            '@branch.detail.first.row.min' => $this->table($tables['branchDetail'])->row(1)->cell(3)->element('input')->get(),
            '@branch.detail.first.row.max' => $this->table($tables['branchDetail'])->row(1)->cell(4)->element('input')->get(),
        ];
    }
}