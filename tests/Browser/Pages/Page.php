<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Traits\TableSelector;

abstract class Page extends BasePage
{
    use TableSelector;
}
