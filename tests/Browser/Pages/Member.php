<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

class Member extends Page
{
    public function url()
    {
        return '/member';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/member')
                ->assertTitle(Lang::get('core::label.member'));
    }

    /**
     * Get the element shortcutsx for the page.
     *
     * @return array
     */
    public function elements()
    {
        $table = '#table-list tbody';
        $modal = '#details';
        $confirm = '#confirm';

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        return [
            '@first.row.full.name' => $this->table($table)->row(1)->cell(1)->element('span')->get(),
            '@first.row.member.type' => $this->table($table)->row(1)->cell(2)->element('span')->get(),
            '@first.row.card.id' => $this->table($table)->row(1)->cell(3)->element('span')->get(),
            '@first.row.barcode' => $this->table($table)->row(1)->cell(4)->element('span')->get(),
            '@first.row.mobile' => $this->table($table)->row(1)->cell(5)->element('span')->get(),
            '@first.row.telephone' => $this->table($table)->row(1)->cell(6)->element('span')->get(),
            '@first.row.command' => $this->table($table)->row(1)->cell(7)->element('img')->get(),
            '@second.row.full.name' => $this->table($table)->row(2)->cell(1)->element('span')->get(),
            '@second.row.member.type' => $this->table($table)->row(2)->cell(2)->element('span')->get(),
            '@second.row.card.id' => $this->table($table)->row(2)->cell(3)->element('span')->get(),
            '@second.row.barcode' => $this->table($table)->row(2)->cell(4)->element('span')->get(),
            '@second.row.mobile' => $this->table($table)->row(2)->cell(5)->element('span')->get(),
            '@second.row.telephone' => $this->table($table)->row(2)->cell(6)->element('span')->get(),
            '@second.row.command' => $this->table($table)->row(2)->cell(7)->element('img')->get(),
            
            '@create' => '#controls div[name="add"]',
            '@filters' => '#controls div[name="filters"]',
            '@filters.search' => $filter['search'],
            '@filters.put' => sprintf('%s button:nth-child(1)', $filter['actions']),
            '@filters.remove' => sprintf('%s button:nth-child(2)', $filter['actions']),
            '@filters.full.name' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@filters.card.id' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@filters.barcode' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@filters.condition.first.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@filters.condition.first.value' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input[type=text]')->get(),
            '@filters.condition.first.condition' => $this->table($filter['conditions'])->row(1)->cell(4)->element('select')->get(),
            '@filters.condition.second.operation' => $this->table($filter['conditions'])->row(2)->cell(2)->element('select')->get(),
            '@filters.condition.second.value' => $this->table($filter['conditions'])->row(2)->cell(3)->element('input')->get(),
            '@filters.condition.second.condition' => $this->table($filter['conditions'])->row(2)->cell(4)->element('select')->get(),

            '@confirm' => $confirm,
            '@ok' => sprintf('%s input[name="ok"]', $confirm),
            '@cancel' => sprintf('%s input[name="cancel"]', $confirm),
            '@save' => sprintf('%s button[name="save"]', $modal),
            '@close.detail' => sprintf('%s button.close', $modal),
            '@modal.detail' => $modal,

            '@card.id' => sprintf('%s input[name="card-id"]', $modal),
            '@barcode' => sprintf('%s input[name="barcode"]', $modal),
            '@full.name' => sprintf('%s input[name="full-name"]', $modal),
            '@rates' => sprintf('%s div.chosen[name="rates"]', $modal),
            '@telephone' => sprintf('%s input[name="telephone"]', $modal),
            '@status' => sprintf('%s input[name="status"]', $modal),
            '@birth.date' => sprintf('%s div.birth-date', $modal),
            '@mobile' => sprintf('%s input[name="mobile"]', $modal),
            '@gender' => sprintf('gender', $modal),
            '@reg.date' => sprintf('%s div.registration-date', $modal),
            '@email' => sprintf('%s input[name="email"]', $modal),
            '@expiration.date' => sprintf('%s div.expiration-date', $modal),
            '@address' => sprintf('%s textarea[name="address"]', $modal),
            '@memo' => sprintf('%s textarea[name="memo"]', $modal),
        ];
    }
}
