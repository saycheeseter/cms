<?php 

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Lang;

abstract class Invoice extends Page
{
    abstract protected function listElements();
    abstract protected function detailElements();

    public function elements()
    {
        return array_merge(
            $this->listElements(),
            $this->detailElements(),
            $this->invoiceListElements(),
            $this->invoiceDetailElements()
        );
    }

    /**
     * returns list of elements for invoice detail
     * 
     * @return array
     */
    protected function invoiceListElements()
    {
        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        return [
            '@invoice.list.controls.add.new.transaction' => '#controls div[name="add-new-transaction"]',
            '@invoice.list.controls.filters' => '#controls div[name="filters"]',
            '@invoice.list.controls.column.settings' => '#controls div[name="column-settings"]',
            '@invoice.list.controls.export.excel' => '#controls div[name="export-excel"]',
            '@invoice.list.controls.approve' => '#controls div[name="approve"]',
            '@invoice.list.controls.decline' => '#controls div[name="decline"]',

            '@invoice.list.filters.modal' => '#filter-modal',

            '@invoice.list.filters.add.one' => '#filter-modal div.filter-buttons button:nth-child(1)',
            '@invoice.list.filters.remove.one' => '#filter-modal div.filter-buttons button:nth-child(2)',
            '@invoice.list.filters.add.all' => '#filter-modal div.filter-buttons button:nth-child(3)',
            '@invoice.list.filters.remove.all' => '#filter-modal div.filter-buttons button:nth-child(4)',

            '@invoice.list.filters.sheet.no' => sprintf('%s li:nth-child(1)', $filter['options']),
            '@invoice.list.filters.remarks' => sprintf('%s li:nth-child(2)', $filter['options']),
            '@invoice.list.filters.transaction.date.time' => sprintf('%s li:nth-child(3)', $filter['options']),
            '@invoice.list.filters.approval.status' => sprintf('%s li:nth-child(4)', $filter['options']),
            '@invoice.list.filters.branch' => sprintf('%s li:nth-child(5)', $filter['options']),
            '@invoice.list.filters.status' => sprintf('%s li:nth-child(6)', $filter['options']),
            '@invoice.list.filters.deleted' => sprintf('%s li:nth-child(7)', $filter['options']),

            '@invoice.list.filters.first.default.condition.label' => $this->table($filter['conditions'])->row(1)->cell(1)->get(),
            '@invoice.list.filters.first.default.condition.operation' => $this->table($filter['conditions'])->row(1)->cell(2)->element('select')->get(),
            '@invoice.list.filters.first.default.condition.input' => $this->table($filter['conditions'])->row(1)->cell(3)->element('input')->get(),
            '@invoice.list.filters.first.default.condition.select' => $this->table($filter['conditions'])->row(1)->cell(3)->element('select')->get(),

            '@invoice.list.filters.second.default.condition.label' => '#filter-modal div.pull-right div ul li:nth-child(2) table tr td:nth-child(1)',
            '@invoice.list.filters.third.default.condition.label' => '#filter-modal div.pull-right div ul li:nth-child(3) table tr td:nth-child(1)',
            '@invoice.list.filters.fourth.default.condition.label' => '#filter-modal div.pull-right div ul li:nth-child(4) table tr td:nth-child(1)',

            '@invoice.list.filters.search' => $filter['search'],
        ];
    }

    /**
     * returns list of elements for invoice detail
     * 
     * @return array
     */
    protected function invoiceDetailElements()
    {
        $datatable = [
            'productSelector' => [
                'body' => '#product-selector tbody',
                'head' => '#product-selector thead'
            ],
            'transactionDetail' => [
                'body' => '#detail-list tbody',
                'head' => '#detail-list thead'
            ]
        ];

        $filter = [
            'options' => '#filter-modal div.modal-body div:nth-child(1) ul',
            'actions' => '#filter-modal div.modal-body div:nth-child(2)',
            'conditions' => '#filter-modal div.modal-body div:nth-child(3) table',
            'search' => '#filter-modal > div.modal-wrapper > div > div.modal-footer > button',
        ];

        $chosen = [
            'uom' => $datatable['transactionDetail']['body'].' div.chosen[name="uom"]'
        ];

        return [
            '@add.product' => '#controls div[name="add-product"]',
            '@column.settings' => '#controls div[name="column-settings"]',
            '@import' => '#controls div[name="import"]',
            '@import.field' => 'input[name="reference-number"]',
            '@import.button' => 'input[name="import-reference"]',
            '@print' => '#controls div[name="print"]',
            '@save' => '#controls div[name="save"]',
            '@for.approval' => '#controls div[name="for-approval"]',
            '@approve' => '#controls div[name="approve"]',
            '@decline' => '#controls div[name="decline"]',
            '@revert' => '#controls div[name="revert"]',

            '@product.selector' => '#product-selector',
            '@product.selector.add' => '#product-selector-controls div[name="add"]',
            '@product.selector.add.continue' => '#product-selector-controls div[name="add-continue"]',
            '@product.selector.column.settings' => '#product-selector-controls div[name="column-settings"]',
            '@product.selector.search' => '#product-selector-controls div[name="search"]',
            '@product.selector.close' => '#product-selector div.modal-header button',

            '@transaction.columns.barcode.checkbox' => '#details-column-modal ul li:nth-child(1) input[type="checkbox"]',
            '@transaction.columns.chinese.name.checkbox' => '#details-column-modal ul li:nth-child(2) input[type="checkbox"]',
            '@transaction.columns.discount1.checkbox' => '#details-column-modal ul li:nth-child(3) input[type="checkbox"]',
            '@transaction.columns.discount2.checkbox' => '#details-column-modal ul li:nth-child(4) input[type="checkbox"]',
            '@transaction.columns.discount3.checkbox' => '#details-column-modal ul li:nth-child(5) input[type="checkbox"]',
            '@transaction.columns.discount4.checkbox' => '#details-column-modal ul li:nth-child(6) input[type="checkbox"]',
            '@transaction.columns.remarks.checkbox' => '#details-column-modal ul li:nth-child(7) input[type="checkbox"]',

            '@transaction.columns.apply.selection' => '#details-column-modal div.modal-footer button',

            '@product.selector.first.row.checkbox' => $this->table($datatable['productSelector']['body'])->row(1)->cell(1)->element('input')->get(),
            '@product.selector.first.row.barcode' => $this->table($datatable['productSelector']['body'])->row(1)->cell(2)->element('span')->get(),
            '@product.selector.first.row.name' => $this->table($datatable['productSelector']['body'])->row(1)->cell(3)->element('span')->get(),
            '@product.selector.first.row.chinese.name' => $this->table($datatable['productSelector']['body'])->row(1)->cell(4)->element('span')->get(),
            '@product.selector.first.row.brand' => $this->table($datatable['productSelector']['body'])->row(1)->cell(5)->element('span')->get(),
            '@product.selector.first.row.category' => $this->table($datatable['productSelector']['body'])->row(1)->cell(6)->element('span')->get(),
            '@product.selector.first.row.inventory' => $this->table($datatable['productSelector']['body'])->row(1)->cell(7)->element('span')->get(),

            '@transaction.detail.first.row.barcode' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(1)->element('span')->get(),
            '@transaction.detail.first.row.product.name' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(2)->element('span')->get(),
            '@transaction.detail.first.row.chinese.name' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(3)->element('span')->get(),
            '@transaction.detail.first.row.qty' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(4)->element('input')->get(),
            '@transaction.detail.first.row.uom' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(5)->element('div.chosen')->get(),
            '@transaction.detail.first.row.unit.specs' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(6)->element('span')->get(),
            '@transaction.detail.first.row.total.qty' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(7)->element('span')->get(),
            '@transaction.detail.first.row.oprice' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(8)->element('span')->get(),
            '@transaction.detail.first.row.price' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(9)->element('input')->get(),
            '@transaction.detail.first.row.discount1' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(10)->element('input')->get(),
            '@transaction.detail.first.row.discount2' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(11)->element('input')->get(),
            '@transaction.detail.first.row.discount3' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(12)->element('input')->get(),
            '@transaction.detail.first.row.discount4' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(13)->element('input')->get(),
            '@transaction.detail.first.row.amount' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(14)->element('span')->get(),
            '@transaction.detail.first.row.remarks' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(15)->element('input')->get(),
            '@transaction.detail.first.row.confirm' => $this->table($datatable['transactionDetail']['body'])->row(1)->cell(16)->element('img')->get(),

            '@transaction.detail.second.row.barcode' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(1)->element('span')->get(),
            '@transaction.detail.second.row.product.name' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(2)->element('span')->get(),
            '@transaction.detail.second.row.chinese.name' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(3)->element('span')->get(),
            '@transaction.detail.second.row.qty' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(4)->element('input')->get(),
            '@transaction.detail.second.row.uom' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(5)->element('div.chosen')->get(),
            '@transaction.detail.second.row.unit.specs' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(6)->element('span')->get(),
            '@transaction.detail.second.row.total.qty' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(7)->element('span')->get(),
            '@transaction.detail.second.row.oprice' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(8)->element('span')->get(),
            '@transaction.detail.second.row.price' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(9)->element('input')->get(),
            '@transaction.detail.second.row.discount1' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(10)->element('input')->get(),
            '@transaction.detail.second.row.discount2' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(11)->element('input')->get(),
            '@transaction.detail.second.row.discount3' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(12)->element('input')->get(),
            '@transaction.detail.second.row.discount4' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(13)->element('input')->get(),
            '@transaction.detail.second.row.amount' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(14)->element('span')->get(),
            '@transaction.detail.second.row.remarks' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(15)->element('input')->get(),
            '@transaction.detail.second.row.confirm' => $this->table($datatable['transactionDetail']['body'])->row(2)->cell(16)->element('img')->get(),

            '@summary.total.qty' => 'span[name="total-qty"]',
            '@summary.total.amount' => 'span[name="total-amount"]',
            '@summary.last.modified' => 'span[name="modified-date"]',
            '@summary.date.created' => 'span[name="date-created"]',
            '@summary.created.by' => 'span[name="created-by"]',
            '@summary.modified.by' => 'span[name="modified-by"]',

            '@status' => '#status-label',
        ];
    }
}