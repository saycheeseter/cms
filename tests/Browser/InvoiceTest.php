<?php 

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Illuminate\Support\Str;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\Voided;
use Carbon\Carbon;
use Lang;

abstract class InvoiceTest extends DuskTestCase
{
    protected $factory;
    protected $uri;
    protected $page;
    protected $products;

    public function setUp() 
    {
        parent::setUp();

        $this->references();
    }

    /**
     * @test
     */
    public function accessing_transaction_list_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit($that->uri)
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function accessing_transaction_create_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit(sprintf('%s/create', $that->uri))
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function accessing_transaction_edit_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit(sprintf('%s/1/edit', $that->uri))
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function accessing_transaction_list_page_with_no_transactions_will_display_mesage()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit($that->uri)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function click_add_new_transaction_will_redirect_to_create_page()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->assertPathIs(sprintf('%s/create', $that->uri))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_transaction_filters_will_display_filter_modal() 
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->assertVisible('@invoice.list.filters.modal')

                    ->assertSeeIn('@invoice.list.filters.sheet.no', Lang::get('core::label.sheet.no'))
                    ->assertSeeIn('@invoice.list.filters.remarks', Lang::get('core::label.remarks'))
                    ->assertSeeIn('@invoice.list.filters.transaction.date.time', Lang::get('core::label.transaction.date.time'))
                    ->assertSeeIn('@invoice.list.filters.approval.status', Lang::get('core::label.approval.status'))
                    ->assertSeeIn('@invoice.list.filters.branch', Lang::get('core::label.branch'))
                    ->assertSeeIn('@invoice.list.filters.status', Lang::get('core::label.status'))
                    ->assertSeeIn('@invoice.list.filters.deleted', Lang::get('core::label.deleted'))

                    ->assertSeeIn('@invoice.list.filters.first.default.condition.label', Lang::get('core::label.transaction.date.time'))
                    ->assertSelected('@invoice.list.filters.first.default.condition.operation', '>=')
                    ->assertSeeIn('@invoice.list.filters.second.default.condition.label', Lang::get('core::label.transaction.date.time'))
                    ->assertSeeIn('@invoice.list.filters.third.default.condition.label', Lang::get('core::label.branch'))
                    ->assertSeeIn('@invoice.list.filters.fourth.default.condition.label', Lang::get('core::label.deleted'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_no_filters_and_clicking_the_search_will_return_a_collection_of_all_data()
    {
        $that = $this;

        $data = $this->collection(2);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertSeeIn('@second.row.sheet.no', $data[1]['head']->sheet_number)

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_sheet_no_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.sheet.no')
                    ->click('@invoice.list.filters.add.one')
                    ->type('@invoice.list.filters.first.default.condition.input', $data[0]['head']->sheet_number)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_remarks_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.remarks')
                    ->click('@invoice.list.filters.add.one')
                    ->type('@invoice.list.filters.first.default.condition.input', $data[0]['head']->remarks)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_transaction_date_time_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2, array(
            array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.transaction.date.time')
                    ->click('@invoice.list.filters.add.one')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertMissing('@first.row.sheet.no')
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_approval_status_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2, array(
            array(
                'approval_status' => (string)ApprovalStatus::APPROVED
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.approval.status')
                    ->click('@invoice.list.filters.add.one')
                    ->select('@invoice.list.filters.first.default.condition.select', $data[0]['head']->approval_status)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_branch_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $branch = Factory::create('branch');

        $data = $this->collection(2, array(
            array(
                'created_from' => $branch->id
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.branch')
                    ->click('@invoice.list.filters.add.one')
                    ->select('@invoice.list.filters.first.default.condition.select', $data[0]['head']->created_from)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_status_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2, array(
            array(
                'transaction_status' => (string) TransactionStatus::INCOMPLETE
            ),
            array(
                'transaction_status' => (string) TransactionStatus::COMPLETE
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.status')
                    ->click('@invoice.list.filters.add.one')
                    ->select('@invoice.list.filters.first.default.condition.select', $data[0]['head']->transaction_status)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_deleted_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;

        $data = $this->collection(2, array(
            array(
                'deleted_at' => Carbon::today()
            ),
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')

                    ->click('@invoice.list.filters.deleted')
                    ->click('@invoice.list.filters.add.one')
                    ->select('@invoice.list.filters.first.default.condition.select', (string)Voided::YES)
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)

                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertMissing('@second.row.sheet.no')

                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function approving_draft_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DRAFT
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.draft'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.approve')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.approve', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function approving_for_approval_transaction_will_change_approval_status_successfully()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::FOR_APPROVAL
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.for.approval'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.approve')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.approved'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function approving_approved_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::APPROVED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.approved'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.approve')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.approve', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function approving_declined_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DECLINED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.declined'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.approve')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.approve', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function approving_deleted_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'deleted_at' => Carbon::today()
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.yes'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.approve')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.transaction.number.deleted', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function declining_draft_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DRAFT
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.draft'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.decline')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.decline', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function declining_for_approval_transaction_will_change_approval_status_successfully()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::FOR_APPROVAL
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.for.approval'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.decline')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.declined'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function declining_approved_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::APPROVED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.approved'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.decline')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.decline', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function declining_declined_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DECLINED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.declined'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.decline')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.cannot.set.to.decline', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function declining_deleted_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'deleted_at' => Carbon::today()
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.yes'))
                    ->click('@headers.checkall')
                    ->click('@invoice.list.controls.decline')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.transaction.number.deleted', ['number' => $data[0]['head']->sheet_number]))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_draft_transaction_will_display_success_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DRAFT
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.no'))
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.draft'))
                    ->click('@first.row.delete.button')
                    ->pause(1000)
                    ->assertVisible('@delete.invoice.modal')
                    ->click('@delete.invoice.ok')
                    ->pause(1500)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.yes'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_deleted_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'deleted_at' => Carbon::today()
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.yes'))
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.draft'))
                    ->click('@first.row.delete.button')
                    ->pause(1000)
                    ->assertVisible('@delete.invoice.modal')
                    ->click('@delete.invoice.ok')
                    ->pause(1500)
                    ->assertSee(Lang::get('core::validation.transaction.deleted'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_for_approval_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::FOR_APPROVAL
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.no'))
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.for.approval'))
                    ->click('@first.row.delete.button')
                    ->pause(1000)
                    ->assertVisible('@delete.invoice.modal')
                    ->click('@delete.invoice.ok')
                    ->pause(1500)
                    ->assertSee(Lang::get('core::validation.cannot.update.transaction'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_approved_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::APPROVED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.no'))
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.approved'))
                    ->click('@first.row.delete.button')
                    ->pause(1000)
                    ->assertVisible('@delete.invoice.modal')
                    ->click('@delete.invoice.ok')
                    ->pause(1500)
                    ->assertSee(Lang::get('core::validation.cannot.update.transaction'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_declined_transaction_will_display_error_message()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'approval_status' => (string) ApprovalStatus::DECLINED
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.deleted', Lang::get('core::label.no'))
                    ->assertSeeIn('@first.row.approval.status', Lang::get('core::label.declined'))
                    ->click('@first.row.delete.button')
                    ->pause(1000)
                    ->assertVisible('@delete.invoice.modal')
                    ->click('@delete.invoice.ok')
                    ->pause(1500)
                    ->assertSee(Lang::get('core::validation.cannot.update.transaction'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_transaction_in_list_will_redirect_to_edit_page()
    {
        $that = $this;

        $data = $this->collection(1, [
            array(
                'transaction_date' => Carbon::today()
            ),
        ]);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@first.row.transaction.date')
                    ->pause(2000)
                    ->assertPathIs($that->uri.'/'.$data[0]['head']['id'].'/edit')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function click_add_new_transaction_order_transaction_will_redirect_to_create_page_and_have_current_date_and_time()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->assertPathIs(sprintf('%s/create', $that->uri))
                    ->assertVisible('@container.main.info')
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.new')))
                    ->assertDatepicker('@main.info.date.time', Carbon::today()->toDateString())
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_transaction_without_product_in_detail_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_product_will_display_product_selector()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->assertVisible('@product.selector')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_on_product_selector_without_selected_product_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.add')
                    ->click('@product.selector.close')
                    ->pause(500)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_on_product_selector_will_add_product_to_detail()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->assertMissing('@product.selector')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_and_continue_on_product_selector_will_add_product_to_detail_and_hide_product_selector()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add.continue')
                    ->pause(1000)
                    ->assertVisible('@product.selector')
                    ->click('@product.selector.close')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_transaction_will_save_succesfully_and_redirect_to_edit()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->assertMissing('@product.selector')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->type('@transaction.detail.first.row.qty', 1)
                    ->click('@transaction.detail.first.row.confirm')
                    ->click('@save')
                    ->pause(5000)
                    ->assertPathIs(sprintf('%s/1/edit', $that->uri))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_new_transaction_will_save_succesfully_and_appear_in_transaction_list()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->assertMissing('@product.selector')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->type('@transaction.detail.first.row.qty', 1)
                    ->click('@transaction.detail.first.row.confirm')
                    ->click('@save')
                    ->pause(5000)
                    ->assertPathIs(sprintf('%s/1/edit', $that->uri))
                    ->visit($that->uri)
                    ->assertVisible('@first.row.sheet.no')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function editing_detail_row_without_finalizing_and_saving_will_display_error_message()
    {
        $that = $this;
        $data = $this->collection(1);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->type('@transaction.detail.first.row.qty', 2)
                    ->click('@save')
                    ->assertSee(Lang::get('core::validation.finalize.all.rows'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_status_of_draft_to_for_approval_will_display_success_message_and_refresh()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string)ApprovalStatus::DRAFT
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.draft')))
                    ->click('@for.approval')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(2000)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.for.approval')))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_status_of_for_approval_to_approved_will_display_success_message_and_refresh()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string)ApprovalStatus::FOR_APPROVAL
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.for.approval')))
                    ->click('@approve')
                    ->pause(500)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.approved')))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_status_of_for_approval_to_declined_will_display_success_message_and_refresh()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string)ApprovalStatus::FOR_APPROVAL
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.for.approval')))
                    ->click('@decline')
                    ->pause(500)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.declined')))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function reverting_declined_transaction_will_display_success_message_and_refresh()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string)ApprovalStatus::DECLINED
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.declined')))
                    ->click('@revert')
                    ->pause(500)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertSeeIn('@status', Str::upper(Lang::get('core::label.draft')))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function importing_an_existing_transaction_will_add_products_to_detail_successfully()
    {
        $that = $this;
        $data = $this->collection(1);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->assertMissing('@product.selector')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->click('@import')
                    ->type('@import.field', $data[0]['head']['sheet_number'])
                    ->click('@import.button')
                    ->pause(2000)
                    ->assertSeeIn('@transaction.detail.second.row.product.name', $that->products[0]['head']->name)
                    ->assertValue('@transaction.detail.second.row.qty', $data[0]['detail']->qty)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function importing_blank_space_will_display_error_message()
    {
        $that = $this;

         $this->browse(function ($browser)  use ($that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@import')
                    ->click('@import.button')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.transaction.not.found'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function importing_a_non_existing_transaction_will_display_error_message()
    {
        $that = $this;

         $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@import')
                    ->type('@import.field', 'RANDOM')
                    ->click('@import.button')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.transaction.not.found'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function editing_approved_transaction_will_display_disabled_fields()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string) ApprovalStatus::APPROVED
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertDisabled('@main.info.remarks')
                    ->assertVisible('@main.info.date.time.mask')
                    ->assertDisabled('@transaction.detail.first.row.qty')
                    ->assertMissing('@add.product')
                    ->assertMissing('@import')
                    ->assertMissing('@revert')
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function editing_declined_transaction_will_display_disabled_fields()
    {
        $that = $this;
        $data = $this->collection(1, array(
            array(
                'approval_status' => (string) ApprovalStatus::DECLINED
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->assertDisabled('@main.info.remarks')
                    ->assertVisible('@main.info.date.time.mask')
                    ->assertDisabled('@transaction.detail.first.row.qty')
                    ->assertMissing('@add.product')
                    ->assertMissing('@import')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_qty_will_autocompute_correct_value()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->assertMissing('@product.selector')
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->type('@transaction.detail.first.row.qty', 25)
                    ->assertSeeIn('@summary.total.qty', 25);
            $total = $browser->text('@transaction.detail.first.row.amount');
            $browser->assertSeeIn('@summary.total.amount', $total)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_uom_will_change_barcode_and_unit_qty()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->click('@column.settings')
                    ->click('@transaction.columns.barcode.checkbox')
                    ->click('@transaction.columns.apply.selection')
                    ->assertSeeIn('@transaction.detail.first.row.barcode', $that->products[0]['product_barcode'][0]->code)
                    ->assertSeeIn('@transaction.detail.first.row.unit.specs', $that->products[0]['product_unit'][0]->qty)
                    ->selectChosen('@transaction.detail.first.row.uom', (string) $that->products[0]['product_unit'][1]->uom_id)
                    ->assertSeeIn('@transaction.detail.first.row.barcode', $that->products[0]['product_barcode'][1]->code)
                    ->assertSeeIn('@transaction.detail.first.row.unit.specs', $that->products[0]['product_unit'][1]->qty)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function editing_transaction_and_saving_will_display_success_message_and_reflect_on_list()
    {
        $that = $this;
        $data = $this->collection(1);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->visit(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->pause(1500)
                    ->type('@main.info.remarks', 'New Remarks')
                    ->click('@save')
                    ->visit($that->uri)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.remarks', 'New Remarks')
                    ->pause(2000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_transaction_with_zero_quantity_product_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::validation.qty.greater.than.zero'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_transaction_with_blank_price_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->clear('@transaction.detail.first.row.price')
                    ->click('@transaction.detail.first.row.confirm')
                    ->assertSee(Lang::get('core::validation.price.numeric'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_transaction_with_blank_qty_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->clear('@transaction.detail.first.row.qty')
                    ->click('@transaction.detail.first.row.confirm')
                    ->assertSee(Lang::get('core::validation.qty.numeric'))
                    ->pause(1000)
                    ->logout();
        });
    }

    protected function collection($times = 1, $data = [])
    {
        $return = [];

        for ($i=0; $i < $times; $i++) { 
            if(!array_key_exists($i, $data)){
                $data[$i] = [];
            }
            $return[] = $this->create(1, $data[$i]);
        }

        return $return;
    }

    protected function create($range = 1, $data = [])
    {
        $head = Factory::create($this->factory['head'], $data);
        $detail = Factory::create($this->factory['detail'], [
            'transaction_id' => $head['id'],
            'product_id' => $this->products[0]['head']->id,
            'unit_id' => $this->products[0]['product_unit'][0]->uom_id,
            'unit_qty' => $this->products[0]['product_unit'][0]->qty,
        ]);

        return [
            'head' => $head->fresh(),
            'detail' => $detail
        ];
    }

    protected function build($range = 1, $data = [])
    {
        $head = Factory::build($this->factory['head'], $data);
        $detail = Factory::build($this->factory['detail'], [
            'transaction_id' => $head['id'],
            'product_id' => $this->products[0]['head']->id,
            'unit_id' => $this->products[0]['product_unit'][0]->uom_id,
            'unit_qty' => $this->products[0]['product_unit'][0]->qty,
        ]);

        return [
            'head' => $head->fresh(),
            'detail' => $detail
        ];
    }

    protected function references()
    {
        $this->products = $this->createProduct();
    }

    protected function createProduct($times = 3)
    {
        $uom = Factory::times(2)->create('uom');
        $products = [];

        for ($i=0; $i < $times; $i++) { 
            $product = Factory::create('product');

            $productBarcode[] = Factory::create('product_barcode', ['product_id'=> $product['id'], 'uom_id' => $uom[0]['id']]);
            $productBarcode[] = Factory::create('product_barcode', ['product_id'=> $product['id'], 'uom_id' => $uom[1]['id']]);
            
            $productUnit[] = Factory::create('product_unit', ['product_id'=> $product['id'], 'uom_id' => $uom[0]['id']]);
            $productUnit[] = Factory::create('product_unit', ['product_id'=> $product['id'], 'uom_id' => $uom[1]['id']]);

            $branchPrice = Factory::create('branch_price', ['product_id'=> $product['id'], 'branch_id' => '1']);
            
            $products[] = collect([
                'head' => $product,
                'product_barcode' => collect($productBarcode),
                'product_unit' => collect($productUnit),
                'branch_price' => $branchPrice
            ]);
        }

        return collect($products);
    }

    abstract public function clicking_transaction_in_transaction_list_will_redirect_to_edit_page_with_correct_details();
}