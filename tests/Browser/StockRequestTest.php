<?php

namespace Tests\Browser;

use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\StockRequest;
use Carbon\Carbon;
use Lang;

class StockRequestTest extends InvoiceTest
{
    protected $branches;
    protected $factory = array(
            'head' => 'stock_request',
            'detail' => 'stock_request_detail'
        );

    protected $uri = '/stock-request-to';
    protected $page = StockRequest::class;

    public function setUp()
    {
        parent::setUp();

        $this->auth();
    }

    /**
     * @test
     */
    public function accessing_stock_request_list_page_with_default_filters_date_branch_and_deleted_stats_will_display_requests()
    {
        $that = $this;

        $data = $this->collection(1, array(
            array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->pause(3000)
                    ->assertSeeIn('@first.row.transaction.date', $data[0]['head']->transaction_date->toDateTimeString())
                    ->assertSeeIn('@first.row.created.for', $data[0]['head']->for->name)
                    ->assertSeeIn('@first.row.request.to', $data[0]['head']->to->name)
                    ->assertSeeIn('@first.row.remarks', $data[0]['head']->remarks)
                    ->assertSeeIn('@first.row.requested.by', $data[0]['head']->requested->full_name)
                    ->assertSeeIn('@first.row.created.by', $data[0]['head']->creator->full_name)
                    ->assertText('@first.row.audit.date', '')
                    ->assertSeeIn('@first.row.approval.status', $data[0]['head']->presenter()->approval)
                    ->assertText('@first.row.transaction.status', $data[0]['head']->presenter()->transaction)
                    ->assertSeeIn('@first.row.amount', $data[0]['head']->number()->total_amount)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function toggling_column_visibility_will_show_and_hide_columns()
    {
        $that = $this;

        $data = $this->collection(1, array(
            array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->assertVisible('@columns.settings.modal')
                    ->click('@columns.settings.created.date.checkbox')
                    ->click('@columns.settings.audit.by.checkbox')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)

                    ->assertVisible('@headers.transaction.date')
                    ->assertVisible('@headers.sheet.no')
                    ->assertVisible('@headers.created.for')
                    ->assertVisible('@headers.request.to')
                    ->assertVisible('@headers.remarks')
                    ->assertVisible('@headers.requested.by')
                    ->assertVisible('@headers.created.by')
                    ->assertVisible('@headers.created.date')
                    ->assertVisible('@headers.audit.by')
                    ->assertVisible('@headers.audit.date')
                    ->assertVisible('@headers.deleted')
                    ->assertVisible('@headers.approval.status')
                    ->assertVisible('@headers.transaction.status')
                    ->assertVisible('@headers.amount')
                    ->pause(1000)

                    ->click('@invoice.list.controls.column.settings')
                    ->assertVisible('@columns.settings.modal')

                    ->click('@columns.settings.transaction.date.checkbox')
                    ->click('@columns.settings.created.for.checkbox')
                    ->click('@columns.settings.request.to.checkbox')
                    ->click('@columns.settings.remarks.checkbox')
                    ->click('@columns.settings.requested.by.checkbox')
                    ->click('@columns.settings.created.by.checkbox')
                    ->click('@columns.settings.created.date.checkbox')
                    ->click('@columns.settings.audit.by.checkbox')
                    ->click('@columns.settings.audit.date.checkbox')
                    ->click('@columns.settings.approval.status.checkbox')
                    ->click('@columns.settings.transaction.status.checkbox')
                    ->click('@columns.settings.amount.checkbox')
                    ->click('@columns.settings.deleted.checkbox')
                    ->pause(1000)
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)

                    ->assertMissing('@headers.transaction.date')
                    ->assertVisible('@headers.sheet.no')
                    ->assertMissing('@headers.created.for')
                    ->assertMissing('@headers.request.to')
                    ->assertMissing('@headers.remarks')
                    ->assertMissing('@headers.requested.by')
                    ->assertMissing('@headers.created.by')
                    ->assertMissing('@headers.created.date')
                    ->assertMissing('@headers.audit.by')
                    ->assertMissing('@headers.audit.date')
                    ->assertMissing('@headers.deleted')
                    ->assertMissing('@headers.approval.status')
                    ->assertMissing('@headers.transaction.status')
                    ->assertMissing('@headers.amount')

                    ->pause(1000)
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function clicking_column_settings_will_display_column_settings_modal_and_default_selected_columns()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.column.settings')
                    ->assertVisible('@columns.settings.modal')

                    ->assertSeeIn('@columns.settings.transaction.date', Lang::get('core::label.transaction.date'))
                    ->assertSeeIn('@columns.settings.created.for', Lang::get('core::label.created.for'))
                    ->assertSeeIn('@columns.settings.request.to', Lang::get('core::label.request.to'))
                    ->assertSeeIn('@columns.settings.remarks', Lang::get('core::label.remarks'))
                    ->assertSeeIn('@columns.settings.requested.by', Lang::get('core::label.request.by'))
                    ->assertSeeIn('@columns.settings.created.by', Lang::get('core::label.created.by'))
                    ->assertSeeIn('@columns.settings.created.date', Lang::get('core::label.created.date'))
                    ->assertSeeIn('@columns.settings.audit.by', Lang::get('core::label.audit.by'))
                    ->assertSeeIn('@columns.settings.audit.date', Lang::get('core::label.audit.date'))
                    ->assertSeeIn('@columns.settings.deleted', Lang::get('core::label.deleted'))
                    ->assertSeeIn('@columns.settings.approval.status', Lang::get('core::label.approval.status'))
                    ->assertSeeIn('@columns.settings.transaction.status', Lang::get('core::label.transaction.status'))
                    ->assertSeeIn('@columns.settings.amount', Lang::get('core::label.amount'))

                    ->pause(1000)
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function clicking_transaction_in_transaction_list_will_redirect_to_edit_page_with_correct_details()
    {
        $that = $this;

        $data = $this->collection(1);

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@invoice.list.controls.filters')
                    ->click('@invoice.list.filters.remove.all')
                    ->click('@invoice.list.filters.search')
                    ->pause(2000)
                    ->assertVisible('@first.row.sheet.no')
                    ->click('@first.row.sheet.no')
                    ->pause(5000)
                    ->assertPathIs(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertDatepicker('@main.info.date.time', $data[0]['head']->transaction_date->toDateString())
                    ->assertSeeIn('@main.info.sheet.no', $data[0]['head']->sheet_number)
                    ->assertSelectedChosen('@main.info.created.for', $data[0]['head']->created_for)
                    ->assertValue('@main.info.remarks', $data[0]['head']->remarks)
                    ->assertSelectedChosen('@main.info.request.to', $data[0]['head']->request_to)
                    ->assertSelectedChosen('@main.info.requested.by', $data[0]['head']->requested_by)

                    ->assertSeeIn('@transaction.detail.first.row.product.name', $that->products[0]['head']->name)
                    ->assertValue('@transaction.detail.first.row.qty', $data[0]['detail']->qty)

                    ->assertSeeIn('@summary.total.qty', $data[0]['detail']->qty)
                    ->assertSeeIn('@summary.total.amount', $data[0]['head']->number()->total_amount)
                    ->assertSeeIn('@summary.last.modified', (string)$data[0]['head']['updated_at'])
                    ->assertSeeIn('@summary.date.created', (string)$data[0]['head']['created_at'])
                    ->assertSeeIn('@summary.created.by', $data[0]['head']->creator->full_name)
                    ->assertSeeIn('@summary.modified.by', $data[0]['head']->modifier->full_name)

                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_created_for_and_confirming_will_clear_transaction_detail()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.created.for', (string) $that->branches[1]->id)
                    ->pause(1500)
                    ->assertVisible('@warning.modal')
                    ->click('@warning.yes')
                    ->assertMissing('@transaction.detail.first.row.product.name')
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_created_for_and_canceling_will_revert_to_previous_created_for()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->maximize()
                    ->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1500)
                    ->selectChosen('@main.info.created.for', (string) $that->branches[1]->id)
                    ->assertVisible('@warning.modal')
                    ->click('@warning.cancel')
                    ->assertSelectedChosen('@main.info.created.for', '1')
                    ->pause(1000)
                    ->logout();
        });
    }

    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(4)->create('branch');
    }
}
