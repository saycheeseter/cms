<?php

namespace Tests\Browser;

class UnitOfMeasurementTest extends SystemCodeTest
{
    protected $url = '/uom';
    protected $page = 'UnitOfMeasurement';
    protected $factory = 'uom';
}
