<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\Browser\Pages\Login;
use Laracasts\TestDummy\Factory;
use Lang;

class LoginTest extends DuskTestCase
{
    /**
     * @test
     */
    public function entering_an_empty_username_or_password_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->fill()
                    ->loginAndWait()
                    ->assertSee(Lang::get('core::validation.username.required'))
                    ->assertSee(Lang::get('core::validation.password.required'));
        });
    }

    /**
     * @test
     */
    public function entering_an_invalid_username_or_password_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->fill('superadmin', '11223344')
                    ->loginAndWait()
                    ->assertSee(Lang::get('core::validation.auth.failed'));
        });
    }

    /**
     * @test
     */
    public function logging_in_with_valid_credentials_but_no_assigned_branch_will_trigger_an_error()
    {
        $password = '123456';

        $user = Factory::create('user', array(
            'username' => 'admin',
            'password' => $password,
            'full_name' => 'Admin',
            'modified_by' => null
        ));

        $this->browse(function ($browser) use ($user, $password) {
            $browser->visit(new Login)
                    ->fill($user->username, $password)
                    ->loginAndWait()
                    ->assertSee(Lang::get('core::validation.no.branch.found'));
        });
    }

    /**
     * @test
     */
    public function logging_in_with_valid_credentials_and_assigned_branch_will_display_the_modal_with_a_list_of_assigned_branches()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->fill('superadmin', '121586')
                    ->loginAndWait()
                    ->assertVisible('@modal')
                    ->assertBranches('@branches');
        });
    }

    /**
     * @test
     */
    public function selecting_a_branch_will_successfully_login_the_user_and_redirect_to_dashboard()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->fill('superadmin', '121586')
                    ->loginAndWait()
                    ->select('@branches', '1')
                    ->authAndWait()
                    ->assertPathIs('/dashboard')
                    ->assertTitle(Lang::get('core::label.dashboard'));
        });
    }

    /**
     * @test
     */
    public function logging_out_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit('/dashboard')
                    ->pause(3000)
                    ->click('button[name=logout]')
                    ->pause(3000)
                    ->assertPathIs('/')
                    ->assertTitle(Lang::get('core::label.login'));
        });
    }

    /**
     * @test
     */
    public function accessing_login_page_while_still_authenticated_will_redirect_the_user_to_dashboard()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit('/')
                    ->pause(3000)
                    ->assertPathIs('/dashboard')
                    ->assertTitle(Lang::get('core::label.dashboard'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function entering_an_invalid_username_or_password_five_times_will_trigger_the_throttle_filter()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->fill('superadmin', '11223344')
                    ->loginAndWait()
                    ->loginAndWait()
                    ->loginAndWait()
                    ->loginAndWait()
                    ->loginAndWait()
                    ->loginAndWait()
                    ->assertSee(Lang::get('core::validation.auth.throttle', [
                        'seconds' => 60
                    ]));
        });
    }
}
