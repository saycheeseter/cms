<?php 

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Product;
use Lang;
use DB;

class ProductTest extends DuskTestCase
{
    protected $factory = 'product';

    private $references = [];
    private $collection = [];
    private $filterColumns = array(
        'stockNo' => 'Stock No.',
        'barcode' => 'Barcode',
        'name' => 'Name',
        'chineseName' => 'Chinese Name',
        'productStatus' => 'Product Status',
        'supplier' => 'Supplier',
        'category' => 'Category',
        'brand' => 'Brand',
        'dateCreated' => 'Date Created',
        'dateModified' => 'Date Modified',
        'inventoryStatus' => 'Inventory Status'
    );

    private $columnSettings = array(
        'unitBarcode' => 'Unit Barcode',
        'chineseName' => 'Chinese Name',
        'brand' => 'Brand',
        'category' => 'Category',
        'supplier' => 'Supplier',
        'inventory' => 'Inventory'
    );

    public function setUp() 
    {
        parent::setUp();

        $this->references();
    }

    
    /**
     * @test
     */
    public function accessing_product_list_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit('/product')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function accessing_product_list_page_will_display_list_of_products()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->assertSeeIn('@first.row.unit.barcode', $that->collection[0]['barcode'][0]['code'].','.$that->collection[0]['barcode'][1]['code'].','.$that->collection[0]['barcode'][2]['code'])
                    ->assertSeeIn('@first.row.name', $that->collection[0]['info']['name'])
                    ->assertSeeIn('@first.row.inventory', $that->collection[0]['inventory'][0]['qty'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function accessing_product_list_page_with_no_products_will_display_mesage()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_new_product_will_redirect_to_create_page()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->click('@create')
                    ->assertPathIs('/product/create')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_column_settings_will_display_column_settings_modal_and_columns()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@columns.settings')
                    ->assertVisible('@columns.modal')
                    ->assertSeeIn('@columns.unit.barcode', $that->columnSettings['unitBarcode'])
                    ->assertSeeIn('@columns.chinese.name', $that->columnSettings['chineseName'])
                    ->assertSeeIn('@columns.brand', $that->columnSettings['brand'])
                    ->assertSeeIn('@columns.category', $that->columnSettings['category'])
                    ->assertSeeIn('@columns.supplier', $that->columnSettings['supplier'])
                    ->assertSeeIn('@columns.inventory', $that->columnSettings['inventory'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function unchecking_column_in_column_settings_will_hide_column()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@columns.settings')
                    ->click('@columns.unit.barcode.checkbox')
                    ->click('@columns.apply.selection')
                    ->assertMissing('@header.row.unit.barcode')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function checking_column_in_column_settings_will_show_column()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@columns.settings')
                    ->click('@columns.unit.barcode.checkbox')
                    ->click('@columns.apply.selection')
                    ->pause(1500)
                    ->assertMissing('@header.row.unit.barcode')
                    ->click('@columns.settings')
                    ->click('@columns.unit.barcode.checkbox')
                    ->click('@columns.apply.selection')
                    ->assertSeeIn('@header.row.unit.barcode', $that->columnSettings['unitBarcode'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_filters_will_display_filter_modal() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@filters')
                    ->assertSeeIn('@filters.stock.no', $that->filterColumns['stockNo'])
                    ->assertSeeIn('@filters.barcode', $that->filterColumns['barcode'])
                    ->assertSeeIn('@filters.name', $that->filterColumns['name'])
                    ->assertSeeIn('@filters.chinese.name', $that->filterColumns['chineseName'])
                    ->assertSeeIn('@filters.product.status', $that->filterColumns['productStatus'])
                    ->assertSeeIn('@filters.supplier', $that->filterColumns['supplier'])
                    ->assertSeeIn('@filters.category', $that->filterColumns['category'])
                    ->assertSeeIn('@filters.brand', $that->filterColumns['brand'])
                    ->assertSeeIn('@filters.date.created', $that->filterColumns['dateCreated'])
                    ->assertSeeIn('@filters.date.modified', $that->filterColumns['dateModified'])
                    ->assertSeeIn('@filters.inventory.status', $that->filterColumns['inventoryStatus'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;
        
        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $that->collection[0]['info']['name'])
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.unit.barcode', $that->collection[0]['barcode'][0]['code'].','.$that->collection[0]['barcode'][1]['code'].','.$that->collection[0]['barcode'][2]['code'])
                    ->assertSeeIn('@first.row.name', $that->collection[0]['info']['name'])
                    ->assertSeeIn('@first.row.inventory', $that->collection[0]['inventory'][0]['qty'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $that = $this;
        
        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', 'sample string')
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@first.row.unit.barcode')
                    ->assertMissing('@first.row.name')
                    ->assertMissing('@first.row.inventory')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_row_will_redirect_to_detail_page() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@first.row.unit.barcode')
                    ->assertPathIs('/product/1/edit')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_delete_button_will_display_delete_confirmation() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->assertVisible('@confirm.delete')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_x_button_or_cancel_in_confirm_delete_will_hide_confirm_modal() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->click('@confirm.close')
                    ->pause(1000)
                    ->assertMissing('@confirm.delete')
                    ->click('@first.row.delete')
                    ->click('@confirm.cancel')
                    ->pause(1000)
                    ->assertMissing('@confirm.delete')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_ok_in_confirm_delete_will_delete_product()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertMissing('@first.row.unit.barcode')
                    ->assertMissing('@first.row.name')
                    ->assertMissing('@first.row.inventory')
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function accessing_product_detail_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->visit('/product/create')
                    ->pause(3000)
                    ->assertPathIs('/')
                    ->visit('/product/1/edit')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    //PRODUCT DETAILS

    /**
     * @test
     */
    public function accessing_create_product_will_display_basic_info_and_advanced_info_tab() 
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->assertVisible('@container.basic.info')
                    ->assertVisible('@container.advanced.info')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_tabs_will_switch_tabs() 
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.advanced.info')
                    ->assertVisible('@container.advanced.info')
                    ->pause(500)
                    ->click('@tab.product.unit')
                    ->assertVisible('@container.product.unit')
                    ->pause(500)
                    ->click('@tab.unit.barcode')
                    ->assertVisible('@container.product.unit.barcode')
                    ->pause(500)
                    ->click('@tab.branch.price')
                    ->assertVisible('@container.branch.price')
                    ->pause(500)
                    ->click('@tab.branch.info')
                    ->assertVisible('@container.branch.info')
                    ->pause(500)
                    ->click('@tab.branch.detail')
                    ->assertVisible('@container.branch.detail')
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_new_product_will_show_existing_branches_on_branches_tabs() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->pause(3000)
                    ->visit('/product/create')
                    ->click('@tab.branch.price')
                    ->assertSeeIn('@branch.price.first.row.branch.label', 'Main')
                    ->assertSeeIn('@branch.price.second.row.branch.label', $this->references['branch'][1]['name'])
                    ->assertSeeIn('@branch.price.third.row.branch.label', $this->references['branch'][2]['name'])
                    ->click('@tab.branch.info')
                    ->assertSeeIn('@branch.info.first.row.branch.label', 'Main')
                    ->assertSeeIn('@branch.info.second.row.branch.label', $this->references['branch'][1]['name'])
                    ->assertSeeIn('@branch.info.third.row.branch.label', $this->references['branch'][2]['name'])
                    ->click('@tab.branch.detail')
                    ->assertSeeIn('@branch.detail.first.row.branch.label', 'Main')
                    ->assertSeeIn('@branch.detail.second.row.branch.label', $this->references['branch'][1]['name'])
                    ->assertSeeIn('@branch.detail.third.row.branch.label', $this->references['branch'][2]['name'])
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_new_product_will_show_existing_branches_and_branch_detail_on_branches_detail_tabs() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.branch.detail')
                    ->assertSeeIn('@branch.detail.first.row.detail.label', 'Main')
                    ->assertSeeIn('@branch.detail.second.row.detail.label', $this->references['branchDetail'][0]['name'])
                    ->assertSeeIn('@branch.detail.third.row.detail.label', $this->references['branchDetail'][1]['name'])
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_new_product_will_show_chosen_with_existing_data() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@chosen.supplier.dropdown')
                    ->assertSeeIn('@chosen.supplier.first.option', $this->references['supplier']['full_name'])
                    ->click('@chosen.brand.dropdown')
                    ->assertSeeIn('@chosen.brand.first.option', $this->references['brand']['name'])
                    ->click('@chosen.category.dropdown')
                    ->assertSeeIn('@chosen.category.first.option', $this->references['category']['name'])
                    ->click('@chosen.tax.dropdown')
                    ->assertSeeIn('@chosen.tax.first.option', 'VAT')
                    ->click('@tab.product.unit')
                    ->click('@chosen.uom.dropdown')
                    ->assertSeeIn('@chosen.uom.first.option', $this->references['uom'][0]['name'])
                    ->assertSeeIn('@chosen.uom.second.option', $this->references['uom'][1]['name'])
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function adding_product_unit_will_show_uom_in_product_unit_barcode() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.product.unit')
                    ->click('@chosen.uom.dropdown')
                    ->click('@chosen.uom.second.option')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->assertSeeIn('@chosen.barcodeUOM.first.option', $this->references['uom'][1]['name'])
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function adding_product_unit_duplicate_will_show_error_message() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.product.unit')
                    ->click('@product.unit.first.row.check')
                    ->click('@product.unit.first.second.check')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.uom.in.use'))
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_product_unit_will_delete_uom_in_product_unit_barcode() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.product.unit')
                    ->click('@chosen.uom.dropdown')
                    ->click('@chosen.uom.second.option')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->assertSeeIn('@chosen.barcodeUOM.first.option', $this->references['uom'][1]['name'])
                    ->click('@tab.product.unit')
                    ->click('@product.unit.first.row.check')
                    ->click('@product.unit.confirm.ok')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->assertMissing('@chosen.barcodeUOM.first.option')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_new_product_without_required_input_will_display_error_message() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::product.info.name.required'))
                    ->assertSee(Lang::get('core::product.info.stock_no.required'))
                    ->assertSee(Lang::get('core::validation.barcodes.min.one'))
                    ->assertSee(Lang::get('core::validation.taxes.min.one'))
                    ->assertSee(Lang::get('core::validation.units.min.one'))
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_new_product_with_correct_details_will_show_success_message() 
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->type('@stock.no', '1001')
                    ->type('@product.name', 'product 1')
                    ->click('@chosen.tax.dropdown')
                    ->click('@chosen.tax.first.option')
                    ->click('@tab.product.unit')
                    ->type('@product.unit.first.row.qty', '1')
                    ->type('@product.unit.first.row.length', '1')
                    ->type('@product.unit.first.row.width', '1')
                    ->type('@product.unit.first.row.height', '1')
                    ->type('@product.unit.first.row.weight', '1')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->click('@chosen.barcodeUOM.first.option')
                    ->type('@unit.barcode.first.row.barcode', '1')
                    ->type('@unit.barcode.first.row.memo', '1')
                    ->click('@unit.barcode.first.row.check')
                    ->click('@tab.branch.price')
                    ->type('@branch.price.first.row.cost', '1')
                    ->type('@branch.price.first.row.wholesale', '1')
                    ->type('@branch.price.first.row.retail', '1')
                    ->type('@branch.price.second.row.cost', '1')
                    ->type('@branch.price.second.row.wholesale', '1')
                    ->type('@branch.price.second.row.retail', '1')
                    ->type('@branch.price.third.row.cost', '1')
                    ->type('@branch.price.third.row.wholesale', '1')
                    ->type('@branch.price.third.row.retail', '1')
                    ->click('@save')
                    ->pause(1500)
                    ->visit('/product')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.unit.barcode', '1')
                    ->assertSeeIn('@first.row.name', 'product 1')
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_a_new_product_with_existing_name_or_code_will_show_error_message() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->type('@stock.no', '1001')
                    ->type('@product.name', 'product 1')
                    ->click('@chosen.tax.dropdown')
                    ->click('@chosen.tax.first.option')
                    ->click('@tab.product.unit')
                    ->type('@product.unit.first.row.qty', '1')
                    ->type('@product.unit.first.row.length', '1')
                    ->type('@product.unit.first.row.width', '1')
                    ->type('@product.unit.first.row.height', '1')
                    ->type('@product.unit.first.row.weight', '1')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->click('@chosen.barcodeUOM.first.option')
                    ->type('@unit.barcode.first.row.barcode', '1')
                    ->type('@unit.barcode.first.row.memo', '1')
                    ->click('@unit.barcode.first.row.check')
                    ->click('@tab.branch.price')
                    ->type('@branch.price.first.row.cost', '1')
                    ->type('@branch.price.first.row.wholesale', '1')
                    ->type('@branch.price.first.row.retail', '1')
                    ->type('@branch.price.second.row.cost', '1')
                    ->type('@branch.price.second.row.wholesale', '1')
                    ->type('@branch.price.second.row.retail', '1')
                    ->type('@branch.price.third.row.cost', '1')
                    ->type('@branch.price.third.row.wholesale', '1')
                    ->type('@branch.price.third.row.retail', '1')
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::product.info.name.unique'))
                    ->assertSee(Lang::get('core::product.info.stock_no.unique'))
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function creating_a_new_product_with_existing_unit_barcode_will_show_error_message() 
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->type('@stock.no', '1002')
                    ->type('@product.name', 'product 2')
                    ->click('@chosen.tax.dropdown')
                    ->click('@chosen.tax.first.option')
                    ->click('@tab.product.unit')
                    ->type('@product.unit.first.row.qty', '1')
                    ->type('@product.unit.first.row.length', '1')
                    ->type('@product.unit.first.row.width', '1')
                    ->type('@product.unit.first.row.height', '1')
                    ->type('@product.unit.first.row.weight', '1')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->click('@chosen.barcodeUOM.first.option')
                    ->type('@unit.barcode.first.row.barcode', '1001')
                    ->type('@unit.barcode.first.row.memo', '1')
                    ->click('@unit.barcode.first.row.check')
                    ->click('@tab.branch.price')
                    ->type('@branch.price.first.row.cost', '1')
                    ->type('@branch.price.first.row.wholesale', '1')
                    ->type('@branch.price.first.row.retail', '1')
                    ->type('@branch.price.second.row.cost', '1')
                    ->type('@branch.price.second.row.wholesale', '1')
                    ->type('@branch.price.second.row.retail', '1')
                    ->type('@branch.price.third.row.cost', '1')
                    ->type('@branch.price.third.row.wholesale', '1')
                    ->type('@branch.price.third.row.retail', '1')
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::product.barcodes.code.unique'))
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_product_will_redirect_to_edit_and_display_correct_details()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->click('@first.row.unit.barcode')
                    ->pause(3000)
                    ->assertPathIs('/product/1/edit')
                    ->assertInputValue('@stock.no', $this->collection[0]['info']['stock_no'])
                    ->assertInputValue('@product.name', $this->collection[0]['info']['name'])
                    ->assertSelectedChosen('@chosen.supplier.dropdown', $this->references['supplier']['id'])
                    ->assertSelectedChosen('@chosen.brand.dropdown', $this->references['brand']['id'])
                    ->assertSelectedChosen('@chosen.category.dropdown', $this->references['category']['id'])
                    ->assertSelectedChosen('@chosen.tax.dropdown', '1')
                    ->click('@tab.product.unit')
                    ->assertInputValue('@product.unit.first.row.qty', $this->collection[0]['unit'][0]['qty'])
                    ->assertInputValue('@product.unit.first.row.length', $this->collection[0]['unit'][0]['length'])
                    ->assertInputValue('@product.unit.first.row.width', $this->collection[0]['unit'][0]['width'])
                    ->assertInputValue('@product.unit.first.row.height', $this->collection[0]['unit'][0]['height'])
                    ->assertInputValue('@product.unit.first.row.weight', $this->collection[0]['unit'][0]['weight'])
                    ->click('@tab.unit.barcode')
                    ->assertInputValue('@unit.barcode.first.row.barcode', $this->collection[0]['barcode'][0]['code'])
                    ->assertInputValue('@unit.barcode.second.row.barcode', $this->collection[0]['barcode'][1]['code'])
                    ->assertInputValue('@unit.barcode.third.row.barcode', $this->collection[0]['barcode'][2]['code'])
                    ->click('@tab.branch.price')
                    ->assertInputValue('@branch.price.first.row.cost', $this->collection[0]['branchPrice'][0]['purchase_price'])
                    ->assertInputValue('@branch.price.first.row.wholesale', $this->collection[0]['branchPrice'][0]['wholesale_price'])
                    ->assertInputValue('@branch.price.first.row.retail', $this->collection[0]['branchPrice'][0]['selling_price'])
                    ->pause(500)
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function removing_a_product_unit_used_by_a_barcode_and_saving_it_will_trigger_an_error()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->click('@first.row.unit.barcode')
                    ->assertPathIs('/product/1/edit')
                    ->pause(3000)
                    ->click('@tab.product.unit')
                    ->click('@product.unit.first.row.check')
                    ->pause(1500)
                    ->click('@product.unit.confirm.ok')
                    ->pause(1500)
                    ->click('@tab.unit.barcode')
                    ->assertInputValue('@unit.barcode.first.row.uom', $this->collection[0]['barcode'][0]['uom'])
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::product.barcodes.uom.required'))
                    ->pause(1500)
                    ->logout();
        });
    }


    /**
     * @test
     */
    public function editing_a_product_with_correct_details_will_display_success_message_and_succesfully_save()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->click('@first.row.unit.barcode')
                    ->assertPathIs('/product/1/edit')
                    ->pause(3000)
                    ->click('@tab.unit.barcode')
                    ->clear('@unit.barcode.first.row.barcode')
                    ->clear('@unit.barcode.second.row.barcode')
                    ->clear('@unit.barcode.third.row.barcode')
                    ->type('@unit.barcode.first.row.barcode', '2001')
                    ->type('@unit.barcode.second.row.barcode', '2002')
                    ->type('@unit.barcode.third.row.barcode', '2003')
                    ->pause(1500)
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(1500)
                    ->visit('/product')
                    ->pause(3000)
                    ->assertSeeIn('@first.row.unit.barcode', '2001,2002,2003')
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function editing_a_product_with_incorrect_min_and_max_will_display()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->click('@first.row.unit.barcode')
                    ->assertPathIs('/product/1/edit')
                    ->pause(3000)
                    ->click('@tab.branch.detail')
                    ->clear('@branch.detail.first.row.min')
                    ->clear('@branch.detail.first.row.max')
                    ->type('@branch.detail.first.row.min', '100')
                    ->type('@branch.detail.first.row.max', '50')
                    ->click('@save')
                    ->pause(1500)
                    ->assertSee(Lang::get('core::validation.min.max.incorrect'))
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function input_non_numeric_in_number_field_will_not_input_anything()
    {
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->click('@tab.product.unit')
                    ->type('@product.unit.first.row.qty', 'this is a string')
                    ->assertInputValue('@product.unit.first.row.qty', '')
                    ->pause(500)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_product_unit_with_product_barcode_and_saving_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new Product)
                    ->visit('/product/create')
                    ->pause(3000)
                    ->type('@stock.no', '1001')
                    ->type('@product.name', 'product 1')
                    ->click('@chosen.tax.dropdown')
                    ->click('@chosen.tax.first.option')
                    ->click('@tab.product.unit')
                    ->type('@product.unit.first.row.qty', '1')
                    ->type('@product.unit.first.row.length', '1')
                    ->type('@product.unit.first.row.width', '1')
                    ->type('@product.unit.first.row.height', '1')
                    ->type('@product.unit.first.row.weight', '1')
                    ->click('@product.unit.first.row.check')
                    ->click('@tab.unit.barcode')
                    ->click('@chosen.barcodeUOM.dropdown')
                    ->click('@chosen.barcodeUOM.first.option')
                    ->type('@unit.barcode.first.row.barcode', '1')
                    ->type('@unit.barcode.first.row.memo', '1')
                    ->click('@unit.barcode.first.row.check')
                    ->click('@tab.branch.price')
                    ->type('@branch.price.first.row.cost', '1')
                    ->type('@branch.price.first.row.wholesale', '1')
                    ->type('@branch.price.first.row.retail', '1')
                    ->type('@branch.price.second.row.cost', '1')
                    ->type('@branch.price.second.row.wholesale', '1')
                    ->type('@branch.price.second.row.retail', '1')
                    ->type('@branch.price.third.row.cost', '1')
                    ->type('@branch.price.third.row.wholesale', '1')
                    ->type('@branch.price.third.row.retail', '1')
                    ->click('@tab.product.unit')
                    ->click('@product.unit.first.row.check')
                    ->click('@product.unit.confirm.ok')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.units.min.one'))
                    ->assertSee(Lang::get('core::product.barcodes.uom.required'))
                    ->pause(500)
                    ->logout();
        });
    }

    private function references() 
    {
        $this->references['supplier'] = Factory::create('supplier', array(
            'code' => '1',
            'full_name' => 'Supplier 1',
            'created_from' => '1'
        ));

        $this->references['brand'] = Factory::create('brand', array(
            'code' => '1',
            'name' => 'Brand 1'
        ));

        $this->references['uom'][] = Factory::create('uom', array(
            'code' => '1',
            'name' => 'UOM 1'
        ));

        $this->references['uom'][] = Factory::create('uom', array(
            'code' => '2',
            'name' => 'UOM 2'
        ));

        $this->references['uom'][] = Factory::create('uom', array(
            'code' => '3',
            'name' => 'UOM 3'
        ));

        $this->references['category'] = Factory::create('category', array(
            'code' => '1',
            'name' => 'Category 1'
        ));

        $this->references['branch'][] = DB::table('branch')->get();

        $this->references['branch'][] = Factory::create('branch', array(
            'code' => '2',
            'name' => 'Branch 2'
        ));

        $this->references['branch'][] = Factory::create('branch', array(
            'code' => '3',
            'name' => 'Branch 3'
        ));

        $this->references['taxes'] = DB::table('tax')->get();

        $this->references['branchDetail'][] = Factory::create('branch_detail', array(
            'branch_id' => $this->references['branch'][1]['id'],
            'code' => '2',
            'name' => 'Branch 2 Detail'
        ));

        $this->references['branchDetail'][] = Factory::create('branch_detail', array(
            'branch_id' => $this->references['branch'][2]['id'],
            'code' => '3',
            'name' => 'Branch 3 Detail'
        ));
    }

    private function collection() 
    {
        $this->collection[0]['info'] = Factory::create($this->factory, array(
            'stock_no' => '1001',
            'name' => 'product 1',
            'supplier_id' => $this->references['supplier']['id'],
            'brand_id' => $this->references['brand']['id'],
            'category_id' => $this->references['category']['id']
        ));

        $this->collection[0]['unit'][] = Factory::create('product_unit', array(
            'product_id' => $this->collection[0]['info']['id'],
            'uom_id' => $this->references['uom'][0]['id']
        ));

        $this->collection[0]['unit'][] = Factory::create('product_unit', array(
            'product_id' => $this->collection[0]['info']['id'],
            'uom_id' => $this->references['uom'][1]['id']
        ));

        $this->collection[0]['unit'][] = Factory::create('product_unit', array(
            'product_id' => $this->collection[0]['info']['id'],
            'uom_id' => $this->references['uom'][2]['id']
        ));

        $this->collection[0]['barcode'][] = Factory::create('product_barcode', array(
            'uom_id' => $this->references['uom'][0]['id'],
            'product_id' => $this->collection[0]['info']['id'],
            'code' => '1001'
        ));

        $this->collection[0]['barcode'][] = Factory::create('product_barcode', array(
            'uom_id' => $this->references['uom'][1]['id'],
            'product_id' => $this->collection[0]['info']['id'],
            'code' => '1002'
        ));

        $this->collection[0]['barcode'][] = Factory::create('product_barcode', array(
            'uom_id' => $this->references['uom'][2]['id'],
            'product_id' => $this->collection[0]['info']['id'],
            'code' => '1003'
        ));

        $this->collection[0]['branchPrice'][] = Factory::create('branch_price', array(
            'branch_id' => '1',
            'product_id' => $this->collection[0]['info']['id']
        ));

        $this->collection[0]['branchPrice'][] = Factory::create('branch_price', array(
            'branch_id' => $this->references['branch'][1]['id'],
            'product_id' => $this->collection[0]['info']['id']
        ));

        $this->collection[0]['branchPrice'][] = Factory::create('branch_price', array(
            'branch_id' => $this->references['branch'][2]['id'],
            'product_id' => $this->collection[0]['info']['id']
        ));

        $this->collection[0]['branchInfo'][] = Factory::create('product_branch_info', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_id' => '1',
        ));

        $this->collection[0]['branchInfo'][] = Factory::create('product_branch_info', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_id' => $this->references['branch'][1]['id'],
        ));

        $this->collection[0]['branchInfo'][] = Factory::create('product_branch_info', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_id' => $this->references['branch'][2]['id'],
        ));

        $this->collection[0]['branchDetail'][] = Factory::create('product_branch_detail', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_detail_id' => $this->references['branchDetail'][0]['id'],
            'min' => '1',
            'max' => '10'
        ));

        $this->collection[0]['branchDetail'][] = Factory::create('product_branch_detail', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_detail_id' => $this->references['branchDetail'][1]['id'],
            'min' => '2',
            'max' => '20'
        ));

        $this->collection[0]['inventory'][] = Factory::create('product_branch_summary', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_id' => '1'
        ));

        $this->collection[0]['inventory'][] = Factory::create('product_branch_summary', array(
            'product_id' => $this->collection[0]['info']['id'],
            'branch_id' => $this->references['branch'][1]['id']
        ));

        $this->collection[0]['tax'] = DB::table('product_tax')->insert([
            'product_id' => $this->collection[0]['info']['id'],
            'tax_id' => '1'
        ]);
    }
}