<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Customer;
use Illuminate\Support\Str;
use Modules\Core\Enums\SellingPriceBasis;
use Lang;

class CustomerTest extends DuskTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->Auth();
    }

    /**
     * @test
     */
    public function accessing_customer_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/customer')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(2000)
                    ->assertSeeIn('@filters.code', 'Code')
                    ->assertSeeIn('@filters.name', 'Name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]->code)
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@list.first.row.code', $data[0]->code)
                    ->assertSeeIn('@list.first.row.name', $data[0]->name)
                    ->assertMissing('@list.second.row.code')
                    ->assertMissing('@list.second.row.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]->name)
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@list.first.row.code', $data[0]->code)
                    ->assertSeeIn('@list.first.row.name', $data[0]->name)
                    ->assertMissing('@list.second.row.code')
                    ->assertMissing('@list.second.row.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '3')
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@list.first.row.code')
                    ->assertMissing('@list.first.row.name')
                    ->assertMissing('@list.second.row.code')
                    ->assertMissing('@list.second.row.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function upon_creating_new_data_fields_shown_should_have_a_default_value()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->assertSelectedChosen('@price.basis', (string) SellingPriceBasis::HISTORICAL)
                    ->assertVisible('@historical.revert.element')
                    ->assertVisible('@historical.default.element')
                    ->assertRadioSelected('@historical.revert', 1)
                    ->assertRadioSelected('@historical.default', 0)
                    ->assertSelectedChosen('@salesman', '1')
                    ->assertChecked('@status')
                    ->click('@branches')
                    ->assertVisible('@detail.first.row.name')
                    ->assertSeeIn('@detail.first.row.name', 'Main Office')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_selling_price_basis_other_than_historical_will_hide_the_historical_options()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PRICE_A)
                    ->assertMissing('@historical.revert.element')
                    ->assertMissing('@historical.default.element')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_selling_price_basis_other_than_historical_will_reset_the_historical_options_to_default()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->radio('@historical.revert', 0)
                    ->radio('@historical.default', 1)
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PRICE_A)
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::HISTORICAL)
                    ->assertRadioSelected('@historical.revert', 1)
                    ->assertRadioSelected('@historical.default', 0)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_the_percentage_option_in_price_basis_will_show_the_pricing_rate()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->assertVisible('@pricing.rate')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_selling_price_basis_other_than_percentage_will_reset_the_pricing_rate_to_default()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->type('@pricing.rate', 100)
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::WHOLESALE)
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->assertInputValue('@pricing.rate', 0)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_branch_record_in_branch_section_will_change_the_button_to_update_and_display_the_data_in_form()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.name')
                    ->assertVisible('@branch.update')
                    ->assertMissing('@branch.create')
                    ->assertInputValue('@branch.name', 'Main Office')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_clear_button_in_branch_section_will_clear_the_form_and_change_the_button_to_create_new()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.name')
                    ->click('@branch.clear')
                    ->assertVisible('@branch.create')
                    ->assertMissing('@branch.update')
                    ->assertInputValue('@branch.name', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function form_field_should_be_cleared_after_creating_a_new_data_in_branch_detail()
    {
        $data = $this->build();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.name', $data['branch']->name)
                    ->click('@branch.create')
                    ->assertInputValue('@branch.name', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function form_modal_default_tab_should_always_be_in_customer_detail()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@close.detail')
                    ->click('@create')
                    ->assertVisible('@customer.detail.section')
                    ->assertMissing('@customer.advance.section')
                    ->assertMissing('@customer.branches.section')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_a_non_numeric_value_in_term_credit_limit_or_pricing_rate_is_not_allowed()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->clear('@pricing.rate')
                    ->clear('@term')
                    ->clear('@credit.limit')
                    ->type('@pricing.rate', 'H')
                    ->type('@term', 'H')
                    ->type('@credit.limit', 'H')
                    ->assertInputValue('@pricing.rate', '')
                    ->assertInputValue('@term', '')
                    ->assertInputValue('@credit.limit', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_name_in_branch_detail_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@branch.create')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_invalid_email_in_branch_detail_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.email', 'google.com')
                    ->click('@branch.create')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.email.invalid'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_name_within_the_list_in_branch_detail_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.name', 'Main Office')
                    ->click('@branch.create')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.name.array.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_unique_name_in_branch_detail_will_successfully_be_added_below_the_table()
    {
        $data = $this->build();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.name', $data['branch']->name)
                    ->click('@branch.create')
                    ->assertSeeIn('@detail.second.row.name', $data['branch']->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_name_in_branch_detail_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.name')
                    ->clear('@branch.name')
                    ->click('@branch.update')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_invalid_email_in_branch_detail_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.name')
                    ->type('@branch.email', 'google.com')
                    ->click('@branch.update')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.email.invalid'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_name_within_the_list_in_branch_detail_will_trigger_an_error()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.name', $data['branch']->name)
                    ->click('@branch.create')
                    ->click('@detail.first.row.name')
                    ->clear('@branch.name')
                    ->type('@branch.name', $data['branch']->name)
                    ->click('@branch.update')
                    ->assertSee(Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.name.array.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function corresponding_row_will_be_updated_successfully_upon_updating_the_data_with_a_unique_name()
    {
        $data1 = $this->build();
        $data2 = $this->build();

        $this->browse(function ($browser) use($data1, $data2) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->type('@branch.name', $data1['branch']->name)
                    ->click('@branch.create')
                    ->click('@detail.first.row.name')
                    ->clear('@branch.name')
                    ->type('@branch.name', $data2['branch']->name)
                    ->click('@branch.update')
                    ->assertSeeIn('@detail.first.row.name', $data2['branch']->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_branch_detail()
    {

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.command')
                    ->pause(1000)
                    ->assertVisible('@confirm.detail')
                    ->assertSeeIn('@confirm.detail', Str::upper(Lang::get('core::label.delete.customer')))
                    ->assertSeeIn('@confirm.detail', Lang::get('core::confirm.delete.customer'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_branch_detail_will_hide_the_confirm_message()
    {

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.command')
                    ->pause(1000)
                    ->click('@cancel.detail')
                    ->assertMissing('@confirm.detail')
                    ->assertVisible('@detail.first.row.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_branch_detail_will_delete_the_data_in_table()
    {

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.command')
                    ->pause(1000)
                    ->click('@ok.detail')
                    ->assertMissing('@confirm.detail')
                    ->assertMissing('@detail.first.row.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_code_or_name_customer_info_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@save')
                    ->assertSee(Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_term_credit_limit_or_pricing_rate_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->clear('@pricing.rate')
                    ->clear('@term')
                    ->clear('@credit.limit')
                    ->click('@save')
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.rate.numeric'))
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.term.numeric'))
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.credit.limit.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_be_saved_and_return_a_successfull_message_close_the_modal_and_add_the_data_in_table_list()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@name', $data['info']->name)
                    ->type('@code', $data['info']->code)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) $data['info']->pricing_type)
                    ->clear('@term')
                    ->type('@term', $data['info']->term)
                    ->clear('@credit.limit')
                    ->type('@credit.limit', $data['info']->formatter()->credit_limit)
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(3000)
                    ->assertMissing('@modal.detail')
                    ->assertVisible('@list.first.row.code')
                    ->assertSeeIn('@list.first.row.code', $data['info']->code)
                    ->assertSeeIn('@list.first.row.name', $data['info']->name)
                    ->click('@list.first.row.name')
                    ->pause(2000)
                    ->assertInputValue('@code', $data['info']->code)
                    ->assertInputValue('@name', $data['info']->name)
                    ->click('@advance')
                    ->assertSelectedChosen('@price.basis', (string) $data['info']->pricing_type)
                    ->assertInputValue('@term', $data['info']->term)
                    ->assertInputValue('@credit.limit', $data['info']->formatter()->credit_limit)
                    ->click('@branches')
                    ->assertSeeIn('@detail.first.row.name', 'Main Office')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_data_in_table_list_will_show_the_form_modal_and_display_its_full_info()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.name')
                    ->pause(2000)
                    ->assertInputValue('@code', $data['info']->code)
                    ->assertInputValue('@name', $data['info']->name)
                    ->click('@advance')
                    ->assertSelectedChosen('@price.basis', (string) $data['info']->pricing_type)
                    ->assertSelectedChosen('@salesman', (string) $data['info']->salesman_id)
                    ->assertChecked('@status', $data['info']->status)
                    ->assertInputValue('@memo', $data['info']->memo)
                    ->assertInputValue('@website', $data['info']->website)
                    ->assertInputValue('@term', $data['info']->term)
                    ->assertInputValue('@credit.limit', $data['info']->formatter()->credit_limit)
                    ->click('@branches')
                    ->assertSeeIn('@detail.first.row.name', $data['branch']->name)
                    ->assertSeeIn('@detail.first.row.address', $data['branch']->address)
                    ->assertSeeIn('@detail.first.row.email', $data['branch']->email)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_code_or_name_customer_info_will_trigger_an_error()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->clear('@code')
                    ->clear('@name')
                    ->click('@save')
                    ->assertSee(Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_non_numeric_term_credit_limit_or_pricing_rate_will_trigger_an_error()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) SellingPriceBasis::PERCENT_WHOLESALE)
                    ->clear('@pricing.rate')
                    ->clear('@term')
                    ->clear('@credit.limit')
                    ->click('@save')
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.rate.numeric'))
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.term.numeric'))
                    ->assertSee(Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.credit.limit.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_proper_format_will_successfully_be_saved_and_return_a_successfull_message_close_the_modal_and_add_the_data_in_table_list()
    {
        $created = $this->create();

        $builded = $this->build();

        $this->browse(function ($browser) use ($created, $builded) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->type('@name', $builded['info']->name)
                    ->type('@code', $builded['info']->code)
                    ->click('@advance')
                    ->selectChosen('@price.basis', (string) $builded['info']->pricing_type)
                    ->clear('@term')
                    ->type('@term', $builded['info']->term)
                    ->clear('@credit.limit')
                    ->type('@credit.limit', $builded['info']->formatter()->credit_limit)
                    ->click('@branches')
                    ->type('@branch.name', $builded['branch']->name)
                    ->type('@branch.email', $builded['branch']->email)
                    ->type('@branch.address', $builded['branch']->address)
                    ->click('@branch.create')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(3000)
                    ->assertMissing('@modal.detail')
                    ->assertVisible('@list.first.row.code')
                    ->assertSeeIn('@list.first.row.code', $builded['info']->code)
                    ->assertSeeIn('@list.first.row.name', $builded['info']->name)
                    ->click('@list.first.row.name')
                    ->pause(2000)
                    ->assertInputValue('@code', $builded['info']->code)
                    ->assertInputValue('@name', $builded['info']->name)
                    ->click('@advance')
                    ->assertSelectedChosen('@price.basis', (string) $builded['info']->pricing_type)
                    ->assertInputValue('@term', $builded['info']->term)
                    ->assertInputValue('@credit.limit', $builded['info']->formatter()->credit_limit)
                    ->click('@branches')
                    ->assertSeeIn('@detail.first.row.name', $created['branch']->name)
                    ->assertSeeIn('@detail.first.row.address', $created['branch']->address)
                    ->assertSeeIn('@detail.first.row.email', $created['branch']->email)
                    ->assertSeeIn('@detail.second.row.name', $builded['branch']->name)
                    ->assertSeeIn('@detail.second.row.address', $builded['branch']->address)
                    ->assertSeeIn('@detail.second.row.email', $builded['branch']->email)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function branch_detail_data_should_still_be_retained_if_the_delete_process_is_confirmed_but_not_saved()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.command')
                    ->click('@ok.detail')
                    ->click('@close.detail')
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->click('@branches')
                    ->assertVisible('@detail.first.row.name')
                    ->assertVisible('@detail.first.row.address')
                    ->assertVisible('@detail.first.row.email')
                    ->assertSeeIn('@detail.first.row.name', $data['branch']->name)
                    ->assertSeeIn('@detail.first.row.address', $data['branch']->address)
                    ->assertSeeIn('@detail.first.row.email', $data['branch']->email)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function branch_detail_data_should_still_be_deleted_if_the_delete_process_is_confirmed_and_saved()
    {
        $created = $this->create();
        $builded = $this->build();

        $this->browse(function ($browser) use ($created, $builded) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->click('@branches')
                    ->click('@detail.first.row.command')
                    ->click('@ok.detail')
                    ->type('@branch.name', $builded['branch']->name)
                    ->type('@branch.address', $builded['branch']->address)
                    ->type('@branch.email', $builded['branch']->email)
                    ->click('@branch.create')
                    ->click('@save')
                    ->pause(4000)
                    ->click('@list.first.row.code')
                    ->pause(1000)
                    ->click('@branches')
                    ->assertVisible('@detail.first.row.name')
                    ->assertVisible('@detail.first.row.address')
                    ->assertVisible('@detail.first.row.email')
                    ->assertDontSeeIn('@detail.first.row.name', $created['branch']->name)
                    ->assertDontSeeIn('@detail.first.row.address', $created['branch']->address)
                    ->assertDontSeeIn('@detail.first.row.email', $created['branch']->email)
                    ->assertSeeIn('@detail.first.row.name', $builded['branch']->name)
                    ->assertSeeIn('@detail.first.row.address', $builded['branch']->address)
                    ->assertSeeIn('@detail.first.row.email', $builded['branch']->email)
                    ->assertMissing('@detail.second.row.name')
                    ->assertMissing('@detail.second.row.address')
                    ->assertMissing('@detail.second.row.email')
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_table_list()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.command')
                    ->pause(1000)
                    ->assertVisible('@confirm.list')
                    ->assertSeeIn('@confirm.list', Str::upper(Lang::get('core::label.delete.customer')))
                    ->assertSeeIn('@confirm.list', Lang::get('core::confirm.delete.customer'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_table_list_will_hide_the_confirm_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.command')
                    ->pause(1000)
                    ->click('@cancel.list')
                    ->pause(1000)
                    ->assertMissing('@confirm.list')
                    ->assertSeeIn('@list.first.row.name', $data[0]->name)
                    ->assertSeeIn('@list.first.row.code', $data[0]->code)
                    ->assertSeeIn('@list.second.row.name', $data[1]->name)
                    ->assertSeeIn('@list.second.row.code', $data[1]->code)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_table_list_will_delete_the_data_in_table()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.command')
                    ->pause(1000)
                    ->click('@ok.list')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertMissing('@confirm.list')
                    ->assertDontSeeIn('@list.first.row.name', $data[0]->name)
                    ->assertDontSeeIn('@list.first.row.code', $data[0]->code)
                    ->assertSeeIn('@list.first.row.name', $data[1]->name)
                    ->assertSeeIn('@list.first.row.code', $data[1]->code)
                    ->assertMissing('@list.second.row.name')
                    ->assertMissing('@list.second.row.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_row_of_the_table_list_will_return_a_no_results_found()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Customer)
                    ->pause(3000)
                    ->click('@list.first.row.command')
                    ->pause(1000)
                    ->click('@ok.list')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@list.first.row.name')
                    ->assertMissing('@list.first.row.code')
                    ->logout();
        });
    }

    private function build($info = array(), $detail = array())
    {
        $customer = Factory::build('customer', array());
        
        $branch = Factory::build('customer_detail', array_merge(array(
            'head_id' => $customer->id
        ), $detail));

        return array(
            'info' => $customer,
            'branch' => $branch
        );
    }

    private function create($info = array(), $detail = array())
    {
        $customer = Factory::create('customer', array());
        
        $branch = Factory::create('customer_detail', array_merge(array(
            'head_id' => $customer->id
        ), $detail));

        return array(
            'info' => $customer,
            'branch' => $branch
        );
    }

    private function collection()
    {
        $customer1 = Factory::create('customer');

        $detail1 = Factory::create('customer_detail', array(
            'head_id' => $customer1->id
        ));

        $customer2 = Factory::create('customer');

        $detail2 = Factory::create('customer_detail', array(
            'head_id' => $customer2->id
        ));

        return array($customer1, $customer2);
    }
}
