<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\MemberRate;
use Illuminate\Support\Str;
use Modules\Core\Enums\Status;
use Lang;

class MemberRateTest extends DuskTestCase
{
    private $factory = 'member_rate';

    /**
     * @test
     */
    public function accessing_member_rate_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/member-rate')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(2000)
                    ->click('@filters')
                    ->pause(2000)
                    ->assertSeeIn('@filter.name', 'Name')
                    ->assertSeeIn('@filter.memo', 'Memo')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[3]['info']->name)
                    ->click('@filter.search')
                    ->pause(2000)
                    ->assertSeeIn('@list.first.name', $data[3]['info']->name)
                    ->assertSeeIn('@list.first.memo', $data[3]['info']->memo)
                    ->assertSeeIn('@list.first.active', $that->status($data[3]['info']->status))
                    ->assertMissing('@list.second.name')
                    ->assertMissing('@list.second.memo')
                    ->assertMissing('@list.second.active')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_memo_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.memo')
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', $data[1]['info']->memo)
                    ->click('@filter.search')
                    ->pause(2000)
                    ->assertSeeIn('@list.first.name', $data[1]['info']->name)
                    ->assertSeeIn('@list.first.memo', $data[1]['info']->memo)
                    ->assertSeeIn('@list.first.active', $that->status($data[1]['info']->status))
                    ->assertMissing('@list.second.name')
                    ->assertMissing('@list.second.memo')
                    ->assertMissing('@list.second.active')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filter.name')
                    ->click('@filter.put')
                    ->select('@filter.condition.first.operation', '=')
                    ->type('@filter.condition.first.value', 'Rate 3')
                    ->click('@filter.search')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@list.first.name')
                    ->assertMissing('@list.first.memo')
                    ->assertMissing('@list.first.active')
                    ->assertMissing('@list.second.name')
                    ->assertMissing('@list.second.memo')
                    ->assertMissing('@list.second.active')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_new_button_will_open_create_modal()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->assertVisible('@modal.detail')
                    ->assertChecked('@head.active')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_a_non_numeric_value_in_amount_from_amount_to_discount_or_amount_per_point_is_not_allowed()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@detail.first.amount.from', 'H')
                    ->type('@detail.first.amount.to', 'H')
                    ->type('@detail.first.discount', 'H')
                    ->type('@detail.first.amount.per.point', 'H')
                    ->assertInputValue('@detail.first.amount.from', '')
                    ->assertInputValue('@detail.first.amount.to', '')
                    ->assertInputValue('@detail.first.discount', '')
                    ->assertInputValue('@detail.first.amount.per.point', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_record_from_list_will_display_record_full_info_on_modal()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.second.name')
                    ->pause(3000)
                    ->assertInputValue('@head.name', $data[1]['info']->name)
                    ->assertChecked('@head.active', $data[1]['info']->status)
                    ->assertInputValue('@head.memo', $data[1]['info']->memo)
                    ->assertInputValue('@detail.first.amount.from', $data[1]['detail'][0]->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data[1]['detail'][0]->formatter()->amount_to)
                    ->assertDatepicker('@detail.first.date.from', $data[1]['detail'][0]->date_from->format('Y-m-d'))
                    ->assertDatepicker('@detail.first.date.to', $data[1]['detail'][0]->date_to->format('Y-m-d'))
                    ->assertInputValue('@detail.first.discount', $data[1]['detail'][0]->formatter()->discount)
                    ->assertInputValue('@detail.first.amount.per.point', $data[1]['detail'][0]->formatter()->amount_per_point)
                    ->assertInputValue('@detail.second.amount.from', $data[1]['detail'][1]->formatter()->amount_from)
                    ->assertInputValue('@detail.second.amount.to', $data[1]['detail'][1]->formatter()->amount_to)
                    ->assertDatepicker('@detail.second.date.from', $data[1]['detail'][1]->date_from->format('Y-m-d'))
                    ->assertDatepicker('@detail.second.date.to', $data[1]['detail'][1]->date_to->format('Y-m-d'))
                    ->assertInputValue('@detail.second.discount', $data[1]['detail'][1]->formatter()->discount)
                    ->assertInputValue('@detail.second.amount.per.point', $data[1]['detail'][1]->formatter()->amount_per_point)
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function saving_an_empty_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_detail_of_amount_from_amount_to_date_from_date_to_discount_or_amount_per_point_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.details.amount.from.required'),
                        Lang::get('core::validation.details.amount.to.required'),
                        Lang::get('core::validation.details.date.from.required'),
                        Lang::get('core::validation.details.date.to.required'),
                        Lang::get('core::validation.details.discount.required'),
                        Lang::get('core::validation.details.amount.per.point.required'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_existing_name_will_trigger_an_error_response()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@head.name', $data[0]['info']->name)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_overlapping_dates_will_trigger_an_error_response()
    {
        $collection = $this->collection();
        $builded = $this->build();

        $this->browse(function ($browser)  use ($builded, $collection) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@head.name', $builded['info']->name)
                    ->type('@head.memo', $builded['info']->memo)
                    ->type('@detail.first.amount.from', $builded['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $builded['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', $collection[0]['detail'][0]->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $collection[0]['detail'][0]->date_to->format('Y-m-d'))
                    ->type('@detail.first.discount', $builded['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $builded['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap'))
                    ->logout();

        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_overlapping_dates_in_list_will_trigger_an_error_response()
    {
        $data = $this->build();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.memo', $data['info']->memo)
                    ->type('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', $data['detail']->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data['detail']->date_to->format('Y-m-d'))
                    ->type('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->type('@detail.second.amount.from', $data['detail']->formatter()->amount_from)
                    ->type('@detail.second.amount.to', $data['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.second.date.from', $data['detail']->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.second.date.to', $data['detail']->date_to->format('Y-m-d'))
                    ->type('@detail.second.discount', $data['detail']->formatter()->discount)
                    ->type('@detail.second.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->click('@detail.second.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.overlap.state'),
                        Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap.state'),
                        Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.from.overlap.state'),
                        Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.to.overlap.state'),
                    ))
                    ->logout();

        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_an_incorrect_date_from_and_date_to_range_will_trigger_an_error_response()
    {
        $data = $this->build();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.memo', $data['info']->memo)
                    ->type('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', $data['detail']->date_to->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data['detail']->date_from->format('Y-m-d'))
                    ->type('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.date.from.invalid.range'),
                        Lang::get('core::validation.date.to.invalid.range'),
                    ))
                    ->logout();

        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_complete_data_will_successfully_save_the_data_and_return_a_success_message()
    {
        $that = $this;
        $data = $this->build();

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(2000)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.memo', $data['info']->memo)
                    ->check('@head.active', $data['info']->status)
                    ->type('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', $data['detail']->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data['detail']->date_to->format('Y-m-d'))
                    ->type('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(3000)
                    ->assertSeeIn('@list.first.name', $data['info']->name)
                    ->assertSeeIn('@list.first.memo', $data['info']->memo)
                    ->assertSeeIn('@list.first.active', $that->status($data['info']->status))
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->assertInputValue('@head.name', $data['info']->name)
                    ->assertChecked('@head.active', $data['info']->status)
                    ->assertInputValue('@head.memo', $data['info']->memo)
                    ->assertInputValue('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->assertDatepicker('@detail.first.date.from', $data['detail']->date_from->format('Y-m-d'))
                    ->assertDatepicker('@detail.first.date.to', $data['detail']->date_to->format('Y-m-d'))
                    ->assertInputValue('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->assertInputValue('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_name_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->clear('@head.name')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

   /**
     * @test
     */
    public function updating_the_data_with_an_empty_detail_of_amount_from_amount_to_discount_or_amount_per_point_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->clear('@detail.first.amount.from')
                    ->clear('@detail.first.amount.to')
                    ->clear('@detail.first.discount')
                    ->clear('@detail.first.amount.per.point')
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.details.amount.from.required'),
                        Lang::get('core::validation.details.amount.to.required'),
                        Lang::get('core::validation.details.discount.required'),
                        Lang::get('core::validation.details.amount.per.point.required'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_record_with_an_existing_name_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->type('@head.name', $data[1]['info']->name)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_record_with_an_overlapping_dates_will_trigger_an_error_response()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.second.name')
                    ->pause(2000)
                    ->selectDatepicker('@detail.first.date.from', $data[0]['detail'][0]->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data[0]['detail'][0]->date_to->format('Y-m-d'))
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap'))
                    ->logout();

        });
    }

    /**
     * @test
     */
    public function updating_the_record_with_an_overlapping_dates_in_list_will_trigger_an_error_response()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.second.name')
                    ->pause(2000)
                    ->selectDatepicker('@detail.first.date.from', $data[1]['detail'][1]->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data[1]['detail'][1]->date_to->format('Y-m-d'))
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.overlap.state'),
                        Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap.state'),
                        Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.from.overlap.state'),
                        Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.to.overlap.state'),
                    ))
                    ->pause(5000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_record_with_an_incorrect_date_from_and_date_to_range_will_trigger_an_error_response()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->selectDatepicker('@detail.first.date.from', $data[0]['detail'][0]->date_to->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data[0]['detail'][0]->date_from->format('Y-m-d'))
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.date.from.invalid.range'),
                        Lang::get('core::validation.date.to.invalid.range'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_record_with_a_complete_data_and_correct_format_will_trigger_successful_response()
    {
        $that = $this;
        $this->collection();
        $data = $this->build();

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.memo', $data['info']->memo)
                    ->check('@head.active', $data['info']->status)
                    ->type('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', '2030-04-26')
                    ->selectDatepicker('@detail.first.date.to', '2030-04-30')
                    ->type('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->click('@detail.second.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(3000)
                    ->assertSeeIn('@list.first.name', $data['info']->name)
                    ->assertSeeIn('@list.first.memo', $data['info']->memo)
                    ->assertSeeIn('@list.first.active', $that->status($data['info']->status))
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->assertInputValue('@head.name', $data['info']->name)
                    ->assertChecked('@head.active', $data['info']->status)
                    ->assertInputValue('@head.memo', $data['info']->memo)
                    ->assertInputValue('@detail.first.amount.from', $data['detail']->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data['detail']->formatter()->amount_to)
                    ->assertDatepicker('@detail.first.date.from', '2030-04-26')
                    ->assertDatepicker('@detail.first.date.to', '2030-04-30')
                    ->assertInputValue('@detail.first.discount', $data['detail']->formatter()->discount)
                    ->assertInputValue('@detail.first.amount.per.point', $data['detail']->formatter()->amount_per_point)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function corresponding_row_will_be_updated_successfully_upon_updating_the_data_with_a_unique_code_and_name()
    {
        $data1 = $this->build();
        $data2 = $this->build();

        $this->browse(function ($browser) use($data1, $data2) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@detail.first.amount.from', $data1['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data1['detail']->formatter()->amount_to)
                    ->selectDatepicker('@detail.first.date.from', $data1['detail']->date_from->format('Y-m-d'))
                    ->selectDatepicker('@detail.first.date.to', $data1['detail']->date_to->format('Y-m-d'))
                    ->type('@detail.first.discount', $data1['detail']->formatter()->discount)
                    ->type('@detail.first.amount.per.point', $data1['detail']->formatter()->amount_per_point)
                    ->click('@detail.first.action')
                    ->type('@detail.first.amount.from', $data2['detail']->formatter()->amount_from)
                    ->type('@detail.first.amount.to', $data2['detail']->formatter()->amount_to)
                    ->click('@detail.first.action')
                    ->assertInputValue('@detail.first.amount.from', $data2['detail']->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data2['detail']->formatter()->amount_to)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_rate_detail()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertVisible('@detail.confirm')
                    ->assertSeeIn('@detail.confirm', Str::upper(Lang::get('core::label.delete.rate')))
                    ->assertSeeIn('@detail.confirm', Lang::get('core::confirm.delete.rate'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_rate_detail_will_hide_the_confirm_message()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.cancel')
                    ->assertMissing('@detail.confirm')
                    ->assertVisible('@detail.first.amount.from')
                    ->assertVisible('@detail.second.amount.from')
                    ->assertVisible('@detail.third.amount.from')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_rate_detail_will_delete_the_data_in_table()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->assertMissing('@detail.confirm')
                    ->pause(1000)
                    ->assertMissing('@detail.third.amount.from')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function rate_detail_data_should_still_be_retained_if_the_delete_process_is_confirmed_but_not_saved()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(1000)
                    ->click('@close.detail')
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->assertInputValue('@detail.first.amount.from', $data['detail'][0]->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data['detail'][0]->formatter()->amount_to)
                    ->assertDatepicker('@detail.first.date.from', $data['detail'][0]->date_from->format('Y-m-d'))
                    ->assertDatepicker('@detail.first.date.to', $data['detail'][0]->date_to->format('Y-m-d'))
                    ->assertInputValue('@detail.first.discount', $data['detail'][0]->formatter()->discount)
                    ->assertInputValue('@detail.first.amount.per.point', $data['detail'][0]->formatter()->amount_per_point)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function rate_detail_data_should_be_deleted_if_the_delete_process_is_confirmed_and_saved()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->click('@detail.confirm.ok')
                    ->click('@save')
                    ->pause(5000)
                    ->click('@list.first.name')
                    ->pause(3000)
                    ->assertInputValue('@detail.first.amount.from', $data['detail'][1]->formatter()->amount_from)
                    ->assertInputValue('@detail.first.amount.to', $data['detail'][1]->formatter()->amount_to)
                    ->assertDatepicker('@detail.first.date.from', $data['detail'][1]->date_from->format('Y-m-d'))
                    ->assertDatepicker('@detail.first.date.to', $data['detail'][1]->date_to->format('Y-m-d'))
                    ->assertInputValue('@detail.first.discount', $data['detail'][1]->formatter()->discount)
                    ->assertInputValue('@detail.first.amount.per.point', $data['detail'][1]->formatter()->amount_per_point)
                    ->assertMissing('@detail.third.amount.from')
                    ->assertMissing('@detail.third.amount.to')
                    ->assertMissing('@detail.third.date.from')
                    ->assertMissing('@detail.third.date.to')
                    ->assertMissing('@detail.third.discount')
                    ->assertMissing('@detail.third.amount.per.point')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_no_details_will_trigger_an_error()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.memo', $data['info']->memo)
                    ->check('@head.active', $data['info']->status)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_with_no_details_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.second.name')
                    ->pause(3000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(2000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_list_and_selecting_the_cancel_button_on_the_data_will_hide_the_confirmation_message()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.action')
                    ->pause(3000)
                    ->click('@list.confirm.cancel')
                    ->pause(2000)
                    ->assertMissing('@list.confirm')
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function clicking_the_delete_icon_on_list_and_selecting_the_ok_button_will_delete_the_data_and_return_a_success_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.action')
                    ->pause(3000)
                    ->click('@list.confirm.ok')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertDontSeeIn('@list.first.name', $data[0]['info']->name)
                    ->assertDontSeeIn('@list.first.memo', $data[0]['info']->memo)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_detail_and_selecting_the_cancel_button_will_delete_the_data_and_return_a_success_message()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.name')
                    ->pause(2000)
                    ->click('@detail.first.action')
                    ->click('@detail.confirm.cancel')
                    ->pause(2000)
                    ->assertMissing('@detail.confirm')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_already_used_rate_will_trigger_an_error_response()
    {
        $this->createMember();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new MemberRate)
                    ->pause(3000)
                    ->click('@list.first.action')
                    ->pause(3000)
                    ->click('@list.confirm.ok')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.delete.used.rate'))
                    ->logout();
        });
    }

    private function build($info = array(), $detail = array())
    {
        $rate = Factory::build('member_rate', $info);

        $detail = Factory::build('member_rate_detail', array_merge(array(
            'member_rate_head_id' => $rate->id
        ), $detail));

        return array(
            'info' => $rate,
            'detail' => $detail
        );
    }

    private function create($info = array(), $detail = array())
    {
        $rate = Factory::create('member_rate', $info);

        $detail = Factory::times(2)->create('member_rate_detail', array_merge(array(
            'member_rate_head_id' => $rate->id
        ), $detail));

        return array(
            'info' => $rate,
            'detail' => $detail
        );
    }

    protected function createMember()
    {
        $data = $this->create();

        Factory::create('member', [
            'full_name' => 'Member 1',
            'rate_head_id' => $data['info']->id,
            'card_id' => '111',
            'barcode' => '111',
            'mobile' => '111',
            'telephone' => '111'
        ]);
    }

    private function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    private function status($status)
    {
        return $status === Status::ACTIVE 
            ? Lang::get('core::label.active')
            : Lang::get('core::label.inactive');
    }
}
