<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\DamageOutbound;
use Carbon\Carbon;
use Lang;

class DamageOutboundTest extends InventoryChainTest
{
    protected $factory = array(
            'head' => 'damage_outbound',
            'detail' => 'damage_outbound_detail',
            'invoice_head' => 'damage',
            'invoice_detail' => 'damage_detail'
        );

    protected $uri = '/damage-outbound';
    protected $page = DamageOutbound::class;

    public function setUp()
    {
        parent::setUp();

        $this->auth();
    }

    /**
     * @test
     */
    public function clicking_column_settings_will_display_column_settings_modal_and_default_selected_columns()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(1500)
                    ->click('@inventory.chain.list.controls.column.settings')
                    ->assertVisible('@columns.settings.modal')
                    ->assertSeeIn('@columns.settings.transaction.date', Lang::get('core::label.transaction.date'))
                    ->assertSeeIn('@columns.settings.created.for', Lang::get('core::label.created.for'))
                    ->assertSeeIn('@columns.settings.for.location', Lang::get('core::label.for.location'))
                    ->assertSeeIn('@columns.settings.remarks', Lang::get('core::label.remarks'))
                    ->assertSeeIn('@columns.settings.reason', Lang::get('core::label.reason'))
                    ->assertSeeIn('@columns.settings.requested.by', Lang::get('core::label.request.by'))
                    ->assertSeeIn('@columns.settings.created.by', Lang::get('core::label.created.by'))
                    ->assertSeeIn('@columns.settings.created.date', Lang::get('core::label.created.date'))
                    ->assertSeeIn('@columns.settings.audit.by', Lang::get('core::label.audit.by'))
                    ->assertSeeIn('@columns.settings.audit.date', Lang::get('core::label.audit.date'))
                    ->assertSeeIn('@columns.settings.deleted', Lang::get('core::label.deleted'))
                    ->assertSeeIn('@columns.settings.approval.status', Lang::get('core::label.approval.status'))
                    ->assertSeeIn('@columns.settings.reference', Lang::get('core::label.reference'))
                    ->assertSeeIn('@columns.settings.amount', Lang::get('core::label.amount'))
                    ->pause(1000)
                    ->logout();
        });
    }

    
}