<?php

namespace Tests\Browser;

class BrandTest extends SystemCodeTest
{
    protected $url = '/brand';
    protected $page = 'Brand';
    protected $factory = 'brand';
}
