<?php

namespace Tests;

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Traits\CreatesApplication;
use Tests\Browser\Traits\DatabaseMigrations;
use Tests\Browser\Traits\CreatesAuthInstance;
use Faker\Factory as Faker;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication,
        DatabaseMigrations,
        CreatesAuthInstance;

    protected $faker;
    
    public function __construct()
    {
        $this->faker = Faker::create();

        Factory::$factoriesPath = 'Modules/Core/Tests/Factories';
    }

    public function setUp()
    {
        parent::setUp();

        $this->runDatabaseMigrations();

        $this->resize();
    }

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()
        );
    }

    /**
     * Create a new Browser instance.
     *
     * @param  \Facebook\WebDriver\Remote\RemoteWebDriver  $driver
     * @return \Tests\NSIBrowser
     */
    protected function newBrowser($driver)
    {
        return new NSIBrowser($driver);
    }

    protected function resize()
    {
        $this->browse(function (NSIBrowser $browser) {
            $browser->resize(1366, 768);
        });
    }
}
