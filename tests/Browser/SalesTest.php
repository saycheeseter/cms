<?php 

namespace Tests\Browser;

use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Sales;
use Carbon\Carbon;
use Modules\Core\Enums\Customer;
use Lang;

class SalesTest extends InvoiceTest
{
    protected $customers;

    protected $factory = array(
        'head' => 'sales',
        'detail' => 'sales_detail'
    );

    protected $uri = '/sales';
    protected $page = Sales::class;

    public function setUp() 
    {
        parent::setUp();

        $this->auth();
    }

    /**
     * @test
     */
    public function accessing_the_sales_list_page_with_transaction_date_branch_and_deleted_status_as_the_default_filters_will_display_the_list_of_filtered_transactions()
    {
        $that = $this;
        $data = $this->collection(1, array(
            0 => array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(3000)
                    ->assertSeeIn('@first.row.transaction.date', $data[0]['head']->transaction_date->toDateTimeString())
                    ->assertSeeIn('@first.row.sheet.no', $data[0]['head']->sheet_number)
                    ->assertSeeIn('@first.row.created.from', $data[0]['head']->for->name)
                    ->assertSeeIn('@first.row.salesman', $data[0]['head']->salesman->full_name)
                    ->assertSeeIn('@first.row.customer', $data[0]['head']->customer->name)
                    ->assertSeeIn('@first.row.customer.detail', $data[0]['head']->customerDetail->name)
                    ->assertSeeIn('@first.row.remarks', $data[0]['head']->remarks)
                    ->assertSeeIn('@first.row.created.by', $data[0]['head']->creator->full_name)
                    ->assertText('@first.row.audit.date', '')
                    ->assertSeeIn('@first.row.approval.status', $data[0]['head']->presenter()->approval)
                    ->assertText('@first.row.transaction.status', $data[0]['head']->presenter()->transaction)
                    ->assertSeeIn('@first.row.amount', $data[0]['head']->number()->total_amount)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_column_settings_button_will_display_the_column_settings_modal_and_the_default_selected_columns()
    {
        $that = $this;
        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(3000)
                    ->click('@invoice.list.controls.column.settings')
                    ->assertVisible('@columns.settings.modal')
                    ->assertSeeIn('@columns.settings.transaction.date', Lang::get('core::label.transaction.date'))
                    ->assertSeeIn('@columns.settings.created.from', Lang::get('core::label.created.from'))
                    ->assertSeeIn('@columns.settings.created.for', Lang::get('core::label.created.for'))
                    ->assertSeeIn('@columns.settings.salesman', Lang::get('core::label.salesman'))
                    ->assertSeeIn('@columns.settings.customer', Lang::get('core::label.customer'))
                    ->assertSeeIn('@columns.settings.customer.detail', Lang::get('core::label.customer.detail'))
                    ->assertSeeIn('@columns.settings.remarks', Lang::get('core::label.remarks'))
                    ->assertSeeIn('@columns.settings.created.by', Lang::get('core::label.created.by'))
                    ->assertSeeIn('@columns.settings.created.date', Lang::get('core::label.created.date'))
                    ->assertSeeIn('@columns.settings.audit.by', Lang::get('core::label.audit.by'))
                    ->assertSeeIn('@columns.settings.audit.date', Lang::get('core::label.audit.date'))
                    ->assertSeeIn('@columns.settings.deleted', Lang::get('core::label.deleted'))
                    ->assertSeeIn('@columns.settings.approval.status', Lang::get('core::label.approval.status'))
                    ->assertSeeIn('@columns.settings.transaction.status', Lang::get('core::label.transaction.status'))
                    ->assertSeeIn('@columns.settings.amount', Lang::get('core::label.amount'))
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function checking_or_unchecking_the_options_in_the_sales_list_column_settings_will_show_or_hide_table_corresponding_columns_in_table()
    {
        $that = $this;
        $data = $this->collection(1, array(
            0 => array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(3000)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.created.for.checkbox')
                    ->click('@columns.settings.created.date.checkbox')
                    ->click('@columns.settings.audit.by.checkbox')
                    ->click('@columns.settings.deleted.checkbox')
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->assertVisible('@headers.created.for')
                    ->assertVisible('@headers.created.date')
                    ->assertVisible('@headers.audit.by')
                    ->assertVisible('@headers.deleted')
                    ->pause(1000)
                    ->click('@invoice.list.controls.column.settings')
                    ->click('@columns.settings.transaction.date.checkbox')
                    ->click('@columns.settings.created.from.checkbox')
                    ->click('@columns.settings.created.for.checkbox')
                    ->click('@columns.settings.salesman.checkbox')
                    ->click('@columns.settings.customer.checkbox')
                    ->click('@columns.settings.customer.detail.checkbox')
                    ->click('@columns.settings.remarks.checkbox')
                    ->click('@columns.settings.created.by.checkbox')
                    ->click('@columns.settings.created.date.checkbox')
                    ->click('@columns.settings.audit.by.checkbox')
                    ->click('@columns.settings.audit.date.checkbox')
                    ->click('@columns.settings.approval.status.checkbox')
                    ->click('@columns.settings.transaction.status.checkbox')
                    ->click('@columns.settings.amount.checkbox')
                    ->click('@columns.settings.deleted.checkbox')
                    ->click('@columns.settings.apply.selection')
                    ->pause(1000)
                    ->assertMissing('@headers.transaction.date')
                    ->assertVisible('@headers.sheet.no')
                    ->assertMissing('@headers.created.from')
                    ->assertMissing('@headers.created.for')
                    ->assertMissing('@headers.salesman')
                    ->assertMissing('@headers.customer')
                    ->assertMissing('@headers.customer.detail')
                    ->assertMissing('@headers.remarks')
                    ->assertMissing('@headers.created.by')
                    ->assertMissing('@headers.created.date')
                    ->assertMissing('@headers.audit.by')
                    ->assertMissing('@headers.audit.date')
                    ->assertMissing('@headers.deleted')
                    ->assertMissing('@headers.approval.status')
                    ->assertMissing('@headers.transaction.status')
                    ->assertMissing('@headers.amount')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_transaction_in_transaction_list_will_redirect_to_edit_page_with_correct_details()
    {
        $that = $this;
        $data = $this->collection(1, array(
            0 => array(
                'transaction_date' => Carbon::today()
            )
        ));

        $this->browse(function ($browser)  use ($data, $that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->pause(3000)
                    ->click('@first.row.sheet.no')
                    ->pause(5000)
                    ->assertPathIs(sprintf('%s/%s/edit', $that->uri, $data[0]['head']->id))
                    ->assertDatepicker('@main.info.date.time', $data[0]['head']->transaction_date->toDateString())
                    ->assertSeeIn('@main.info.sheet.no', $data[0]['head']->sheet_number)
                    ->assertSelectedChosen('@main.info.customer', $data[0]['head']->customer_id)
                    ->assertSelectedChosen('@main.info.customer.detail', $data[0]['head']->customer_detail_id)
                    ->assertValue('@main.info.remarks', $data[0]['head']->remarks)
                    ->assertSelectedChosen('@main.info.requested.by', $data[0]['head']->requested_by)
                    ->assertSelectedChosen('@main.info.salesman', $data[0]['head']->salesman_id)
                    ->assertRadioSelected('@main.info.customer.type', Customer::REGULAR)
                    ->assertValue('@main.info.term', $data[0]['head']->term)
                    ->assertSelectedChosen('@main.info.created.for', $data[0]['head']->created_for)
                    ->assertSeeIn('@transaction.detail.first.row.product.name', $data[0]['detail']->product->name)
                    ->assertValue('@transaction.detail.first.row.qty', $data[0]['detail']->qty)
                    ->assertSelectedChosen('@transaction.detail.first.row.uom', $data[0]['detail']->unit_id)
                    ->assertSeeIn('@transaction.detail.first.row.oprice', $data[0]['detail']->number()->oprice)
                    ->assertInputValue('@transaction.detail.first.row.price', $data[0]['detail']->number()->price)
                    ->assertSeeIn('@summary.total.qty', $data[0]['detail']->qty)
                    ->assertSeeIn('@summary.total.amount', $data[0]['head']->number()->total_amount)
                    ->assertSeeIn('@summary.last.modified', (string)$data[0]['head']['updated_at'])
                    ->assertSeeIn('@summary.date.created', (string)$data[0]['head']['created_at'])
                    ->assertSeeIn('@summary.created.by', $data[0]['head']->creator->full_name)
                    ->assertSeeIn('@summary.modified.by', $data[0]['head']->modifier->full_name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_the_customer_type_field_to_walk_in_will_hide_the_customer_selection_and_show_the_walk_in_customer_field()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->pause(1000)
                    ->assertVisible('@main.info.customer.walk.in.name')
                    ->assertMissing('@main.info.customer')
                    ->assertMissing('@main.info.customer.detail')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_the_customer_type_field_to_regular_will_hide_the_walk_in_customer_field_and_show_the_customer_selection()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->radio('@main.info.customer.type', Customer::REGULAR)
                    ->pause(1000)
                    ->assertMissing('@main.info.customer.walk.in.name')
                    ->assertVisible('@main.info.customer')
                    ->assertVisible('@main.info.customer.detail')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changin_the_customer_type_will_always_clear_its_previous_field_or_selection()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->type('@main.info.customer.walk.in.name', $that->faker->name)
                    ->radio('@main.info.customer.type', Customer::REGULAR)
                    ->selectChosen('@main.info.customer', $that->customers[1]['info']->id)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->pause(1000)
                    ->assertInputValue('@main.info.customer.walk.in.name', '')
                    ->radio('@main.info.customer.type', Customer::REGULAR)
                    //->assertSelectedChosen('@main.info.customer', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_the_created_for_field_while_theres_already_an_encoded_product_will_show_a_warning_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1000)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.created.for', (string) $that->branch->id)
                    ->pause(1500)
                    ->assertVisible('@warning.modal')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function cancelling_the_warning_message_upon_changing_the_created_for_field_will_not_delete_the_products_and_revert_its_previous_value()
    {
        $that = $this;
        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.created.for', (string) $that->branch->id)
                    ->pause(1500)
                    ->click('@warning.cancel')
                    ->pause(1000)
                    ->assertVisible('@transaction.detail.first.row.product.name')
                    ->assertSelectedChosen('@main.info.created.for', "1")
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_the_customer_field_while_theres_already_an_encoded_product_will_show_a_warning_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1000)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.customer', (string) $that->customers[1]['info']->id)
                    ->pause(1500)
                    ->assertVisible('@warning.modal')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function cancelling_the_warning_message_upon_changing_the_customer_field_will_not_delete_the_products_and_revert_its_previous_value()
    {
        $that = $this;
        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.customer', (string) $that->customers[1]['info']->id)
                    ->pause(1500)
                    ->click('@warning.cancel')
                    ->pause(1000)
                    ->assertVisible('@transaction.detail.first.row.product.name')
                    ->assertSelectedChosen('@main.info.created.for', "1")
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function changing_the_customer_type_field_while_theres_already_an_encoded_product_will_show_a_warning_message()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1000)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->pause(1500)
                    ->assertVisible('@warning.modal')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function cancelling_the_warning_message_upon_changing_the_customer_type_field_will_not_delete_the_products_and_revert_its_previous_value()
    {
        $that = $this;
        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1500)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->radio('@main.info.customer.type', Customer::WALK_IN)
                    ->pause(1500)
                    ->click('@warning.cancel')
                    ->pause(1000)
                    ->assertVisible('@transaction.detail.first.row.product.name')
                    ->assertSelectedChosen('@main.info.created.for', "1")
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function confirming_the_warning_message_upon_changing_the_created_for_customer_type_or_customer_field_will_delete_the_products()
    {
        $that = $this;
        $this->browse(function ($browser)  use ($that) {
            $browser->authenticate()
                    ->visit(new $that->page)
                    ->click('@invoice.list.controls.add.new.transaction')
                    ->pause(1000)
                    ->selectChosen('@main.info.customer', (string) $that->customers[0]['info']->id)
                    ->pause(1500)
                    ->click('@add.product')
                    ->pause(1000)
                    ->click('@product.selector.first.row.checkbox')
                    ->click('@product.selector.add')
                    ->pause(1000)
                    ->selectChosen('@main.info.created.for', (string) $that->branch->id)
                    ->pause(1500)
                    ->click('@warning.ok')
                    ->pause(1000)
                    ->assertMissing('@transaction.detail.first.row.product.name')
                    ->logout();
        });
    }

    protected function create($range = 1, $data = [])
    {
        $customer = $this->customers[rand(0, 1)];

        $head = Factory::create($this->factory['head'], array_merge(array(
            'customer_id' => $customer['info']->id,
            'customer_detail_id' => $customer['details'][rand(0, 1)]->id,
            'customer_type' => Customer::REGULAR
        ), $data));

        $detail = Factory::create($this->factory['detail'], [
            'transaction_id' => $head['id'],
            'product_id' => $this->products[0]['head']->id,
            'unit_id' => $this->products[0]['product_unit'][0]->uom_id,
            'unit_qty' => $this->products[0]['product_unit'][0]->qty,
        ]);

        return [
            'head' => $head->fresh(),
            'detail' => $detail
        ];
    }

    protected function references()
    {
        parent::references();

        for ($i=0; $i < 2; $i++) { 
            $customer = Factory::create('customer');
            
            $branches = Factory::times(2)->create('customer_detail', array(
                'head_id' => $customer->id
            ));

            $this->customers[$i] = array(
                'info' => $customer,
                'details' => $branches
            );
        }
        
        $this->branch = Factory::create('branch');
    }
}