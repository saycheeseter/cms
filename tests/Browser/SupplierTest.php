<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Supplier;
use Illuminate\Support\Str;
use Lang;

class SupplierTest extends DuskTestCase
{
    private $factory = 'supplier';

    /**
     * @test
     */
    public function accessing_supplier_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/supplier')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filters.code', 'Code')
                    ->assertSeeIn('@filters.full.name', 'Full Name')
                    ->assertSeeIn('@filters.chinese.name', 'Chinese Name')
                    ->assertSeeIn('@filters.memo', 'Memo')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]->code)
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.code', $data[0]->code)
                    ->assertSeeIn('@first.row.name', $data[0]->full_name)
                    ->assertSeeIn('@first.row.chinese.name', $data[0]->chinese_name)
                    ->assertSeeIn('@first.row.contact', $data[0]->contact)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_full_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@filters')
                    ->click('@filters.full.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]->full_name)
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSeeIn('@first.row.code', $data[0]->code)
                    ->assertSeeIn('@first.row.name', $data[0]->full_name)
                    ->assertSeeIn('@first.row.chinese.name', $data[0]->chinese_name)
                    ->assertSeeIn('@first.row.contact', $data[0]->contact)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '3')
                    ->click('@filters.search')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@first.row.code')
                    ->assertMissing('@first.row.name')
                    ->assertMissing('@first.row.chinese.name')
                    ->assertMissing('@first.row.contact')
                    ->assertMissing('@first.row.command')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_code_full_name_or_term_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@create')
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.full.name.required'))
                    ->assertSee(Lang::get('core::validation.term.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_full_name_will_trigger_an_error()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@code', $data->code)
                    ->type('@full.name', $data->full_name)
                    ->type('@term', $data->term)
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.full.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_incorrect_email_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->click('@advance')
                    ->type('@email', 'FOO')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.email.invalid'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_a_non_numeric_value_in_term_field_is_not_allowed()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@term', 'F')
                    ->pause(1000)
                    ->assertInputValue('@term', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_unique_code_and_full_name_with_term_will_successfully_save_the_data_and_return_a_success_message()
    {
        $data = $this->data();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@code', $data['code'])
                    ->type('@full.name', $data['full_name'])
                    ->type('@term', $data['term'])
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(2000)
                    ->assertSeeIn('@first.row.code', $data['code'])
                    ->assertSeeIn('@first.row.name', $data['full_name'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_full_name_or_term_will_trigger_an_error()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.code')
                    ->pause(3000)
                    ->clear('@code')
                    ->clear('@full.name')
                    ->clear('@term')
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.full.name.required'))
                    ->assertSee(Lang::get('core::validation.term.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_or_full_name_or_username_will_trigger_an_error()
    {
        $data = $this->create();

        $this->create([
            'code' => '2',
            'full_name' => 'Sample Supplier 2',
            'term' => 'Username 2',
        ]);

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@second.row.code')
                    ->pause(3000)
                    ->type('@code', $data->code)
                    ->type('@full.name', $data->full_name)
                    ->type('@term', $data->term)
                    ->click('@save')
                    ->pause(2000)
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.full.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect_email_will_trigger_an_error()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.code')
                    ->pause(3000)
                    ->click('@advance')
                    ->type('@email', 'FOO')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.email.invalid'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_unique_code_and_full_name_will_successfully_save_the_data_and_return_a_success_message()
    {
        $this->create();

        $this->create([
            'code' => '2',
            'full_name' => 'Sample Supplier 2',
            'term' => '2',
        ]);

        $data = $this->data([
            'code' => '3',
            'full_name' => 'Sample Supplier 3',
            'term' => '3'
        ]);

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@second.row.code')
                    ->pause(3000)
                    ->type('@code', $data['code'])
                    ->type('@full.name', $data['full_name'])
                    ->type('@term', $data['term'])
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(3000)
                    ->assertSeeIn('@second.row.code', $data['code'])
                    ->assertSeeIn('@second.row.name', $data['full_name'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_the_data_will_show_a_confirm_message()
    {   
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.supplier')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.supplier'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_and_selecting_the_cancel_button_on_the_data_will_hide_the_confirmation_message()
    {   
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@cancel')
                    ->pause(2000)
                    ->assertMissing('@confirm')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_data_will_return_a_no_results_found()
    {   
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_and_confirming_the_message_will_successfully_delete_the_data_and_return_a_success_message()
    {   
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Supplier)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->logout();
        });
    }

    private function data($data = [])
    {
        return array_merge(array(
            'code' => '1',
            'full_name' => 'Sample Supplier 1',
            'term' => '1'
        ), $data);
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, array_merge(array(
            'code' => '1',
            'full_name' => 'Sample Supplier 1',
            'term' => '1'
        ), $data));
    }

    private function collection()
    {
        return array(
            Factory::create($this->factory, array(
                'code' => '1',
                'full_name' => 'Sample Supplier 1',
                'term' => '1'
            )),
            Factory::create($this->factory, array(
                'code' => '2',
                'full_name' => 'Sample Supplier 2',
                'term' => '2'
            ))
        );
    }
}
