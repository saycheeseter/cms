<?php 

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Lang;

abstract class InventoryChainTest extends DuskTestCase
{
    protected $factory;
    protected $uri;
    protected $page;
    protected $products;
    protected $invoices;

    public function setUp() 
    {
        parent::setUp();

        $this->references();
    }

    /**
     * @test
     */
    public function accessing_transaction_list_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit($that->uri)
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    protected function collection($times = 1, $data = [])
    {
        $return = [];

        for ($i=0; $i < $times; $i++) { 
            if(!array_key_exists($i, $data)){
                $data[$i] = [];
            }
            $return[] = $this->create(1, $data[$i]);
        }

        return $return;
    }

    protected function create($range = 1, $data = [])
    {
        $head = Factory::create($this->factory['head'], $data);
        $detail = Factory::create($this->factory['detail'], [
            'transaction_id' => $head['id'],
            'product_id' => $this->products[0]['head']->id,
            'unit_id' => $this->products[0]['product_unit'][0]->uom_id,
            'unit_qty' => $this->products[0]['product_unit'][0]->qty,
        ]);

        return [
            'head' => $head->fresh(),
            'detail' => $detail
        ];
    }

    protected function references()
    {
        $this->products = $this->createProduct();
        $this->invoices = $this->createInvoices();
    }

    protected function createProduct($times = 3)
    {
        $uom = Factory::times(2)->create('uom');
        $products = [];

        for ($i=0; $i < $times; $i++) { 
            $product = Factory::create('product');

            $productBarcode[] = Factory::create('product_barcode', ['product_id'=> $product['id'], 'uom_id' => $uom[0]['id']]);
            $productBarcode[] = Factory::create('product_barcode', ['product_id'=> $product['id'], 'uom_id' => $uom[1]['id']]);

            $productUnit[] = Factory::create('product_unit', ['product_id'=> $product['id'], 'uom_id' => $uom[0]['id']]);
            $productUnit[] = Factory::create('product_unit', ['product_id'=> $product['id'], 'uom_id' => $uom[1]['id']]);

            $branchPrice = Factory::create('branch_price', ['product_id'=> $product['id'], 'branch_id' => '1']);
            
            $products[] = collect([
                'head' => $product,
                'product_barcode' => collect($productBarcode),
                'product_unit' => collect($productUnit),
                'branch_price' => $branchPrice
            ]);
        }

        return collect($products);
    }

    protected function createInvoices($range = 1, $data = [])
    {
        $head = Factory::create($this->factory['invoice_head'], $data);
        $detail = Factory::create($this->factory['invoice_detail'], [
            'transaction_id' => $head['id'],
            'product_id' => $this->products[0]['head']->id,
            'unit_id' => $this->products[0]['product_unit'][0]->uom_id,
            'unit_qty' => $this->products[0]['product_unit'][0]->qty,
        ]);

        return [
            'head' => $head->fresh(),
            'detail' => $detail
        ];
    }
}