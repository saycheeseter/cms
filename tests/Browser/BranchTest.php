<?php

namespace Tests\Browser;
use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\Branch;
use Illuminate\Support\Str;
use Lang;

class BranchTest extends DuskTestCase
{
    private $factory = 'branch';

    /**
    * @test
    */
    public function accessing_branch_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/branch')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filters.code', 'Code')
                    ->assertSeeIn('@filters.name', 'Name')
                    ->assertSeeIn('@filters.business.name', 'Business Name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.select')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[2]['info']->code)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@list.body.first.code', $data[2]['info']->code)
                    ->assertSeeIn('@list.body.first.name', $data[2]['info']->name)
                    ->assertSeeIn('@list.body.first.address', $data[2]['info']->address)
                    ->assertSeeIn('@list.body.first.contact', $data[2]['info']->contact)
                    ->assertSeeIn('@list.body.first.business.name', $data[2]['info']->business_name)
                    ->assertMissing('@list.body.second.code')
                    ->assertMissing('@list.body.second.name')
                    ->assertMissing('@list.body.second.address')
                    ->assertMissing('@list.body.second.contact')
                    ->assertMissing('@list.body.second.business.name')
                    ->logout();
        });
    }

     /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.name')
                    ->click('@filters.select')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[3]['info']->name)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@list.body.first.code', $data[3]['info']->code)
                    ->assertSeeIn('@list.body.first.name', $data[3]['info']->name)
                    ->assertSeeIn('@list.body.first.address', $data[3]['info']->address)
                    ->assertSeeIn('@list.body.first.contact', $data[3]['info']->contact)
                    ->assertSeeIn('@list.body.first.business.name', $data[3]['info']->business_name)
                    ->assertMissing('@list.body.second.code')
                    ->assertMissing('@list.body.second.name')
                    ->assertMissing('@list.body.second.address')
                    ->assertMissing('@list.body.second.contact')
                    ->assertMissing('@list.body.second.business.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_business_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.business.name')
                    ->click('@filters.select')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[0]['info']->business_name)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSeeIn('@list.body.first.code', $data[0]['info']->code)
                    ->assertSeeIn('@list.body.first.name', $data[0]['info']->name)
                    ->assertSeeIn('@list.body.first.address', $data[0]['info']->address)
                    ->assertSeeIn('@list.body.first.contact', $data[0]['info']->contact)
                    ->assertSeeIn('@list.body.first.business.name', $data[0]['info']->business_name)
                    ->assertMissing('@list.body.second.code')
                    ->assertMissing('@list.body.second.name')
                    ->assertMissing('@list.body.second.address')
                    ->assertMissing('@list.body.second.contact')
                    ->assertMissing('@list.body.second.business.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_new_button_will_open_create_modal()
    {
       $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->assertVisible('@modal.detail')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_record_from_list_will_display_record_full_info_on_modal()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(3000)
                    ->assertInputValue('@head.code', $data[0]['info']->code)
                    ->assertInputValue('@head.name', $data[0]['info']->name)
                    ->assertInputValue('@head.contact', $data[0]['info']->contact)
                    ->assertInputValue('@head.address', $data[0]['info']->address)
                    ->assertInputValue('@head.business.name', $data[0]['info']->business_name)
                    ->assertInputValue('@detail.first.code', $data[0]['detail'][0]->code)
                    ->assertInputValue('@detail.first.name', $data[0]['detail'][0]->name)
                    ->assertInputValue('@detail.first.contact', $data[0]['detail'][0]->contact)
                    ->assertInputValue('@detail.first.address', $data[0]['detail'][0]->address)
                    ->assertInputValue('@detail.first.business.name', $data[0]['detail'][0]->business_name)
                    ->assertInputValue('@detail.second.code', $data[0]['detail'][1]->code)
                    ->assertInputValue('@detail.second.name', $data[0]['detail'][1]->name)
                    ->assertInputValue('@detail.second.contact', $data[0]['detail'][1]->contact)
                    ->assertInputValue('@detail.second.address', $data[0]['detail'][1]->address)
                    ->assertInputValue('@detail.second.business.name', $data[0]['detail'][1]->business_name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_head_code_and_head_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_detail_code_and_detail_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->pause(3000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_head_code_and_head_name_and_detail_code_and_detail_name_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@head.code', $data[0]['info']->code)
                    ->type('@head.name', $data[0]['info']->name)
                    ->type('@head.contact', $data[0]['info']->contact)
                    ->type('@head.address', $data[0]['info']->address)
                    ->type('@head.business.name', $data[0]['info']->business_name)
                    ->type('@detail.first.code', $data[0]['detail'][0]->code)
                    ->type('@detail.first.name', $data[0]['detail'][0]->name)
                    ->type('@detail.first.contact', $data[0]['detail'][0]->contact)
                    ->type('@detail.first.address', $data[0]['detail'][0]->address)
                    ->type('@detail.first.business.name', $data[0]['detail'][0]->business_name)
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.unique'))
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_record_with_no_details_will_trigger_an_error()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(3000)
                    ->type('@head.code', $data['info']->code)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.contact', $data['info']->contact)
                    ->type('@head.address', $data['info']->address)
                    ->type('@head.business.name', $data['info']->business_name)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_trigger_successful_response()
    {
        $data = $this->build();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@head.code', $data['info']->code)
                    ->type('@head.name', $data['info']->name)
                    ->type('@head.contact', $data['info']->contact)
                    ->type('@head.address', $data['info']->address)
                    ->type('@head.business.name', $data['info']->business_name)
                    ->type('@detail.first.code', $data['detail']->code)
                    ->type('@detail.first.name', $data['detail']->name)
                    ->type('@detail.first.contact', $data['detail']->contact)
                    ->type('@detail.first.address', $data['detail']->address)
                    ->type('@detail.first.business.name', $data['detail']->business_name)
                    ->click('@detail.first.action')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(3000)
                    ->assertSeeIn('@list.body.second.code', $data['info']->code)
                    ->assertSeeIn('@list.body.second.name', $data['info']->name)
                    ->assertSeeIn('@list.body.second.contact', $data['info']->contact)
                    ->assertSeeIn('@list.body.second.address', $data['info']->address)
                    ->assertSeeIn('@list.body.second.business.name', $data['info']->business_name)
                    ->pause(1000)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_an_empty_head_code_and_head_name_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(3000)
                    ->clear('@head.code')
                    ->clear('@head.name')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_an_empty_detail_code_and_detail_name_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(3000)
                    ->clear('@detail.first.code')
                    ->clear('@detail.first.name')
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_an_existing_head_code_and_head_name_and_detail_code_and_detail_name_will_trigger_an_error()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(3000)
                    ->type('@head.code', $data[1]['info']->code)
                    ->type('@head.name', $data[1]['info']->name)
                    ->type('@detail.first.code', $data[1]['detail'][0]->code)
                    ->type('@detail.first.name', $data[1]['detail'][0]->name)
                    ->type('@detail.first.contact', $data[1]['detail'][0]->contact)
                    ->type('@detail.first.address', $data[1]['detail'][0]->address)
                    ->type('@detail.first.business.name', $data[1]['detail'][0]->business_name)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.unique'))
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function corresponding_row_will_be_updated_successfully_upon_updating_the_data_with_a_unique_code_and_name()
    {
        $data1 = $this->build();
        $data2 = $this->build();

        $this->browse(function ($browser) use($data1, $data2) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@create')
                    ->pause(1000)
                    ->type('@detail.first.code', $data1['detail']->code)
                    ->type('@detail.first.name', $data1['detail']->name)
                    ->type('@detail.first.contact', $data1['detail']->contact)
                    ->type('@detail.first.address', $data1['detail']->address)
                    ->type('@detail.first.business.name', $data1['detail']->business_name)
                    ->click('@detail.first.action')
                    ->type('@detail.first.code', $data2['detail']->code)
                    ->type('@detail.first.name', $data2['detail']->name)
                    ->click('@detail.first.action')
                    ->assertInputValue('@detail.first.code', $data2['detail']->code)
                    ->assertInputValue('@detail.first.name', $data2['detail']->name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_branch_detail()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->assertVisible('@detail.confirm')
                    ->assertSeeIn('@detail.confirm', Str::upper(Lang::get('core::label.delete.branch')))
                    ->assertSeeIn('@detail.confirm', Lang::get('core::confirm.delete.branch'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_branch_detail_will_hide_the_confirm_message()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.cancel')
                    ->assertMissing('@detail.confirm')
                    ->assertVisible('@detail.first.name')
                    ->assertVisible('@detail.second.name')
                    ->assertVisible('@detail.third.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_branch_detail_will_delete_the_data_in_table()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->assertMissing('@detail.confirm')
                    ->pause(1000)
                    ->assertMissing('@detail.third.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function branch_detail_data_should_still_be_retained_if_the_delete_process_is_confirmed_but_not_saved()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(1000)
                    ->click('@close.detail')
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->assertInputValue('@detail.first.code', $data['detail'][0]->code)
                    ->assertInputValue('@detail.first.name', $data['detail'][0]->name)
                    ->assertInputValue('@detail.first.contact', $data['detail'][0]->contact)
                    ->assertInputValue('@detail.first.address', $data['detail'][0]->address)
                    ->assertInputValue('@detail.first.business.name', $data['detail'][0]->business_name)
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function branch_detail_data_should_be_deleted_if_the_delete_process_is_confirmed_and_saved()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->click('@detail.first.action')
                    ->click('@detail.confirm.ok')
                    ->click('@save')
                    ->pause(5000)
                    ->click('@list.body.second.code')
                    ->pause(1000)
                    ->assertInputValueIsNot('@detail.first.code', $data['detail'][0]->code)
                    ->assertInputValueIsNot('@detail.first.name', $data['detail'][0]->name)
                    ->assertInputValueIsNot('@detail.first.contact', $data['detail'][0]->contact)
                    ->assertInputValueIsNot('@detail.first.address', $data['detail'][0]->address)
                    ->assertInputValueIsNot('@detail.first.business.name', $data['detail'][0]->business_name)
                    ->assertInputValue('@detail.first.code', $data['detail'][1]->code)
                    ->assertInputValue('@detail.first.name', $data['detail'][1]->name)
                    ->assertInputValue('@detail.first.contact', $data['detail'][1]->contact)
                    ->assertInputValue('@detail.first.address', $data['detail'][1]->address)
                    ->assertInputValue('@detail.first.business.name', $data['detail'][1]->business_name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_with_no_details_will_trigger_an_error()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.code')
                    ->pause(3000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(2000)
                    ->click('@detail.first.action')
                    ->pause(1000)
                    ->click('@detail.confirm.ok')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.details.min.one'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_table_list()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.first.action')
                    ->pause(1000)
                    ->assertVisible('@list.confirm')
                    ->assertSeeIn('@list.confirm', Str::upper(Lang::get('core::label.delete.branch')))
                    ->assertSeeIn('@list.confirm', Lang::get('core::confirm.delete.branch'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_and_clicking_ok_button_will_delete_the_data_and_return_a_success_message()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.action')
                    ->pause(3000)
                    ->click('@list.confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_and_clicking_cancel_button_will_not_delete_the_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new Branch)
                    ->pause(3000)
                    ->click('@list.body.second.action')
                    ->pause(3000)
                    ->click('@list.confirm.cancel')
                    ->pause(1000)
                    ->assertDontSee(Lang::get('core::success.deleted'))
                    ->logout();
        });
    }

    private function build($info = array(), $detail = array())
    {
        $branch = Factory::build('branch', $info);
        
        $detail = Factory::build('branch_detail', array_merge(array(
            'branch_id' => $branch->id
        ), $detail));

        return array(
            'info' => $branch,
            'detail' => $detail
        );
    }

    private function create($info = array(), $detail = array())
    {
        $branch = Factory::create('branch', $info);
        
        $detail = Factory::times(2)->create('branch_detail', array_merge(array(
            'branch_id' => $branch->id
        ), $detail));
        
        return array(
            'info' => $branch,
            'detail' => $detail
        );
    }

    private function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) { 
            $data[] = $this->create();
        }

        return $data;
    }
}
