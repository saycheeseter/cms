<?php

namespace Tests\Browser\Traits;

use Illuminate\Support\Str;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\Remote\LocalFileDetector;
use Facebook\WebDriver\Interactions\WebDriverActions;

trait Interaction
{
    /**
     * Returns the option inside the given select field
     * 
     * @param string $field
     * @return options
     */
    public function selectOption($field)
    {
        $element = $this->resolver->resolveForSelection($field);

        return $element->findElements(WebDriverBy::tagName('option'));
    }

    /**
     * Select the given value or random value of a drop-down chosen field.
     *
     * @param  string  $field
     * @param  string  $value
     * @return $this
     */
    public function selectChosen($field, $value = null)
    {
        $element = $this->resolver->find($field);

        $element->click();

        $options = $element->findElements(WebDriverBy::tagName('li'));

        if (is_null($value)) {
            $options[array_rand($options)]->click();
        } else {
            foreach ($options as $option) {
                if ($option->getAttribute('value') === $value) {
                    $option->click();

                    break;
                }
            }
        }

        return $this;
    }

    /**
     * Overrides default clear method for browser
     *
     * @return $this
     */
    public function clear($field)
    {
        $this->keys($field, ['{control}', 'a'], '{backspace}');

        return $this;
    }

    /**
     * Select the specified date of the given datepicker
     *  
     * @param  string $field
     * @param  string $date 
     * @return $this       
     */
    public function selectDatepicker($field, $date)
    {
        $element = $this->resolver->find($field);

        $element->click();

        $date = date_parse($date);

        $displayedDate = $element->findElement(WebDriverBy::cssSelector('div.calendar:nth-child(2) header span.up'));

        $current = date_parse($displayedDate->getText());

        if ($date['year'] !== $current['year']) {
            
            $displayedDate->click();
            $displayedMonth = $element->findElement(WebDriverBy::cssSelector('div.calendar:nth-child(3) header span.up'));
            $displayedMonth->click();

            $this->selectYear($element, $date['year']);
            $this->selectMonth($element, $date['month']);
            $this->selectDay($element, $date['day']);
        } else if ($current['month'] != $date['month']) {
            $displayedDate->click();

            $this->selectMonth($element, $date['month']);
            $this->selectDay($element, $date['day']);
        } else {
            $this->selectDay($element, $date['day']);
        }

        return $this;
    }

    private function selectYear($element, $selected)
    {
        for(;;) {
            $displayedYear = $element->findElement(WebDriverBy::cssSelector('div.calendar:nth-child(4) header span:nth-child(2)'));
            $nextYearRange = $element->findElement(WebDriverBy::cssSelector('div.calendar:nth-child(4) header span.next'));
            $prevYearRange = $element->findElement(WebDriverBy::cssSelector('div.calendar:nth-child(4) header span.prev'));
            $yearRange = str_replace("'s", "", $displayedYear->getText());

            if ($selected > ($yearRange + 9)) {
                $nextYearRange->click();
            } else if ($selected < $yearRange) {
                $prevYearRange->click();
            } else {
                $years = $element->findElements(WebDriverBy::cssSelector('div.calendar:nth-child(4) span.year'));

                foreach ($years as $year) {
                    if ((int) $year->getText() === (int) $selected) {
                        $year->click();
                        break;
                    }
                }

                break;
            }
        }
    }

    private function selectMonth($element, $selected)
    {
        $months = $element->findElements(WebDriverBy::cssSelector('div.calendar:nth-child(3) span.month'));

        foreach ($months as $key => $month) {
            if (((int) $key + 1 )=== (int) $selected) {
                $month->click();
                break;
            }
        }
    }

    private function selectDay($element, $selected)
    {
        $days = $element->findElements(WebDriverBy::cssSelector('div.calendar:nth-child(2) span.day'));

        foreach ($days as $day) {
            if ((int) $day->getText() === (int) $selected) {
                $day->click();
            }
        }
    }
}
