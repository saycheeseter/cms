<?php

namespace Tests\Browser\Traits;

trait Authentication
{
    /**
     * Log into the application as the default user.
     *
     * @return $this
     */
    public function authenticate()
    {
        return $this->authenticateAs($this->userId, $this->branchId, $this->guard);
    }

    /**
     * Log into the application using a given user ID or email.
     *
     * @param  object|string  $userId
     * @param  string         $branchId
     * @param  string         $guard
     * @return $this
     */
    public function authenticateAs($userId, $branchId, $guard = null)
    {
        return $this->visit(rtrim('/_dusk/login/'.$userId.'/'.$branchId.'/'.$guard, '/'));
    }
}