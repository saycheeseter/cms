<?php

namespace Tests\Browser\Traits;

trait TableSelector
{
    protected $selector = '';

    /**
     * Build selector for table element
     *
     * @param  string $selector
     * @return $this
     */
    public function table($selector)
    {
        $this->selector .= $selector;

        return $this;
    }

    /**
     * Build selector for the specified row number
     *
     * @param  int $number
     * @return $this
     */
    public function row($number)
    {
        $this->selector .= sprintf(' tr:nth-child(%s) ', $number);

        return $this;
    }

    /**
     * Build selector for the specified table header
     *
     * @param  int $number
     * @return $this
     */
    public function header($number)
    {
        $this->selector .= sprintf('th:nth-child(%s) ', $number);

        return $this;
    }

    /**
     * Build selector for the specified cell number
     *
     * @param  int $number
     * @return $this
     */
    public function cell($number)
    {
        $this->selector .= sprintf('td:nth-child(%s) ', $number);

        return $this;
    }

    /**
     * Build selector for the specified element
     *
     * @param  string $selector
     * @return $this
     */
    public function element($selector)
    {
        $this->selector .= sprintf('%s', $selector);

        return $this;
    }

    /**
     * Returns the actual selector string
     *
     * @return string
     */
    public function get()
    {
        $selector = $this->selector;

        $this->selector = '';

        return $selector;
    }
}
