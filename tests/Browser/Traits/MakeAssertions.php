<?php

namespace Tests\Browser\Traits;

use PHPUnit_Framework_Assert as PHPUnit;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Str;

trait MakeAssertions
{
    public function assertEquals($base, $value)
    {
        PHPUnit::assertEquals($base, $value);

        return $this;
    }

    public function assertTrue($value)
    {
        PHPUnit::assertTrue($value);

        return $this;
    }

    public function assertFalse($value)
    {
        PHPUnit::assertFalse($value);

        return $this;
    }

    /**
     * Assert that the given chosen field has the given value selected.
     *
     * @param  string  $field
     * @param  string  $value
     * @return $this
     */
    public function assertSelectedChosen($field, $value)
    {
        PHPUnit::assertTrue(
            $this->selectedChosen($field, $value),
            "Expected value [{$value}] to be selected for [{$field}], but it wasn't."
        );

        return $this;
    }

    /**
     * Assert that the given chosen field does not have the given value selected.
     *
     * @param  string  $field
     * @param  string  $value
     * @return $this
     */
    public function assertNotSelectedChosen($field, $value)
    {
        PHPUnit::assertFalse(
            $this->selectedChosen($field, $value),
            "Unexpected value [{$value}] selected for [{$field}]."
        );

        return $this;
    }

    /**
     * Determine if the given value is selected for the given select field.
     *
     * @param  string  $field
     * @param  string  $value
     * @return bool
     */
    public function selectedChosen($field, $value)
    {
        $element = $this->resolver->find($field);
        $options = $element->findElements(WebDriverBy::cssSelector('div.selected span'));

        $selected = [];
        $value = !is_array($value) ? [$value] : $value;

        foreach ($options as $option) {
            $selected[] = $option->getAttribute('value');
        }

        $diff = array_merge(
            array_diff($value, $selected), 
            array_diff($selected, $value)
        );

        return count($diff) === 0;
    }

    /**
     * Determine if the given date is the value of the datepicker
     *
     * @param  string  $field
     * @param  string  $value
     * @return bool
     */
    public function assertDatepicker($field, $value)
    {
        PHPUnit::assertTrue(
            $this->selectedDate($field, $value),
            "Expected value [{$value}] to be selected for [{$field}], but it wasn't."
        );

        return $this;
    }

    /**
     * Determine if the given date is the value of the datepicker
     *
     * @param  string  $field
     * @param  string  $value
     * @return bool
     */
    public function assertDatepickerIsNot($field, $value)
    {
        PHPUnit::assertFalse(
            $this->selectedDate($field, $value),
            "Unexpected value [{$value}] selected for [{$field}]."
        );

        return $this;
    }

    /**
     * Check if the given date is equal to the actual date
     * @param  string $field
     * @param  string $value
     * @return bool       
     */
    public function selectedDate($field, $value)
    {
        $element = $this->resolver->find($field);

        $input = $element->findElement(WebDriverBy::tagName('input'));

        return $value === $input->getAttribute('value');
    }

    /**
     * Get the value of the given input or text area field.
     *
     * @param  string  $field
     * @return string
     */
    public function inputValue($field)
    {
        $element = $this->resolver->resolveForTyping($field);

        return $element->getAttribute('value');
    }

    /**
     * Assert that the given array of text appears on the page.
     *
     * @param  string  $array
     * @return $this
     */
    public function assertSeeArray($array)
    {
        return $this->assertSeeInArray('', $array);
    }

    /**
     * Assert that the given array of text does not appear on the page.
     *
     * @param  string  $array
     * @return $this
     */
    public function assertDontSeeArray($array)
    {
        return $this->assertDontSeeInArray('', $array);
    }

    /**
     * Assert that the given array of text appears within the given selector.
     *
     * @param  string  $selector
     * @param  string  $array
     * @return $this
     */
    public function assertSeeInArray($selector, $array)
    {
        $fullSelector = $this->resolver->format($selector);

        $element = $this->resolver->findOrFail($selector);

        $content = $element->getText();

        foreach ($array as $key => $text) {
            PHPUnit::assertTrue(
                Str::contains($content, $text),
                "Did not see expected text [{$text}] within element [{$fullSelector}]."
            );
        }

        return $this;
    }

    /**
     * Assert that the given array of text does not appear within the given selector.
     *
     * @param  string  $selector
     * @param  string  $array
     * @return $this
     */
    public function assertDontSeeInArray($selector, $array)
    {
        $fullSelector = $this->resolver->format($selector);

        $element = $this->resolver->findOrFail($selector);

        $content = $element->getText();
        
        foreach ($array as $key => $text) {
            PHPUnit::assertFalse(
                Str::contains($content, $text),
                "Did not see expected text [{$text}] within element [{$fullSelector}]."
            );
        }

        return $this;
    }

    /**
     * Assert that the element at the given selector has the given text.
     *
     * @param  string  $selector
     * @param  string  $value
     * @return $this
     */
    public function assertText($selector, $value)
    {
        $actual = $this->resolver->findOrFail($selector)->getText();

        PHPUnit::assertEquals($value, $actual);

        return $this;
    }

    /**
     * Assert that the element is disabled
     * @param  string $selector 
     * @return $this           
     */
    public function assertDisabled($selector)
    {
        $element = $this->resolver->findOrFail($selector);

        PHPUnit::assertTrue(
            $element->getAttribute('disabled') === 'true'
        );

        return $this;
    }

    /**
     * Assert that the element is not disabled
     * @param  string $selector 
     * @return $this           
     */
    public function assertNotDisabled($selector)
    {
        $element = $this->resolver->findOrFail($selector);

        PHPUnit::assertTrue(
            $element->getAttribute('disabled') === 'false'
        );

        return $this;
    }

    /**
     * Assert that the element has specified class
     * @param  string $selector
     * @param  string|array $classes 
     * @return $this          
     */
    public function assertHasClass($selector, $classes) 
    {
        $element = $this->resolver->findOrFail($selector);

        $element_class = $element->getAttribute('class');

         PHPUnit::assertTrue(
            Str::contains($element_class, $classes)
        );

        return $this;
    }

    /**
     * Assert that the element does not have specified class
     * @param  string $selector
     * @param  string|array $classes 
     * @return $this          
     */
    public function assertHasNoClass($selector, $classes) 
    {
        $element = $this->resolver->findOrFail($selector);

        $element_class = $element->getAttribute('class');

         PHPUnit::assertTrue(
            Str::contains($element_class, $classes)
        );

        return $this;
    }
}