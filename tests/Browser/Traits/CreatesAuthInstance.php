<?php

namespace Tests\Browser\Traits;

use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Auth;
use App;

trait CreatesAuthInstance
{
    protected $user = 1;
    protected $branch = 1;
    protected $guard = null;
    
    /**
     * Set default user for authentication
     * 
     * @param  int $id
     * @return $this    
     */
    public function setUser($id)
    {
        $this->user = $id;

        return $this;
    }

    /**
     * Set default branch for authentication
     * 
     * @param  int $id
     * @return $this    
     */
    public function setBranch($id)
    {
        $this->branch = $id;

        return $this;
    }

    /**
     * Set default guard for authentication
     * 
     * @param  string $guard
     * @return $this    
     */
    public function setGuard($guard)
    {
        $this->guard = $guard;

        return $this;
    }
    
    /**
     * Log in the user inside the application (For Factory purposes)
     *
     * @return $this
     */
    public function auth()
    {
        $user = App::make(UserRepository::class)
            ->with(['branchPermissions' => $this->branch])
            ->find($this->user);

        $branch = App::make(BranchRepository::class)->find($this->branch);
        $permissions = $user->permissions;

        Auth::guard($this->guard)->login($user);
        Auth::setConfig($branch, $permissions);

        return $this;
    }
}