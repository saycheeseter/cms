<?php

namespace Tests\Browser\Traits;

use Illuminate\Contracts\Console\Kernel;

trait DatabaseMigrations
{
    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        $this->artisan('module:migrate', [
            'module' => 'Core'
        ]);

        $this->artisan('module:seed', [
            'module' => 'Core'
        ]);
        
        $this->app[Kernel::class]->setArtisan(null);

        $this->beforeApplicationDestroyed(function () {
            $this->artisan('module:migrate-rollback', [
                'module' => 'Core'
            ]);
        });
    }
}
