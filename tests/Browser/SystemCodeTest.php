<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Illuminate\Support\Str;
use Lang;

abstract class SystemCodeTest extends DuskTestCase
{
    protected $url = '';
    protected $page = '';
    protected $factory = '';

    /**
     * @test
     */
    public function accessing_system_code_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit($that->url)
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filters.code', 'Code')
                    ->assertSeeIn('@filters.name', 'Name')
                    ->assertSeeIn('@filters.remarks', 'Remarks')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;
        $data = $this->collection();

        $this->browse(function ($browser)  use ($that, $data) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '1')
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertInputValue('@first.row.code', $data[0]->code)
                    ->assertInputValue('@first.row.name', $data[0]->name)
                    ->assertInputValue('@first.row.remarks', $data[0]->remarks)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $that = $this;
        $data = $this->collection();

        $this->browse(function ($browser)  use ($that, $data) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', 'Foo')
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertInputValue('@first.row.code', $data[0]->code)
                    ->assertInputValue('@first.row.name', $data[0]->name)
                    ->assertInputValue('@first.row.remarks', $data[0]->remarks)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $that = $this;
        
        $this->collection();

        $this->browse(function ($browser)  use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '3')
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertInputValue('@first.row.code', '')
                    ->assertInputValue('@first.row.name', '')
                    ->assertInputValue('@first.row.remarks', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_code_and_name_will_trigger_an_error()
    {
        $that = $this;

        $this->browse(function ($browser)  use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error()
    {
        $that = $this;
        $data = $this->create();

        $this->browse(function ($browser) use ($data, $that){
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->type('@second.row.code', $data->code)
                    ->type('@second.row.name', $data->name)
                    ->type('@second.row.remarks', $data->remarks)
                    ->click('@second.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_unique_code_and_name_will_successfully_save_the_data_and_return_a_success_message()
    {
        $that = $this;
        $data = $this->data();

        $this->browse(function ($browser) use ($data, $that){
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->type('@first.row.code', $data['code'])
                    ->type('@first.row.name', $data['name'])
                    ->type('@first.row.remarks', $data['remarks'])
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->assertInputValue('@first.row.code', $data['code'])
                    ->assertInputValue('@first.row.name', $data['name'])
                    ->assertInputValue('@first.row.remarks', $data['remarks'])
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_and_name_will_trigger_an_error()
    {
        $that = $this;

        $this->create();

        $this->browse(function ($browser) use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->clear('@first.row.code')
                    ->clear('@first.row.name')
                    ->clear('@first.row.remarks')
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::validation.code.required'))
                    ->assertSee(Lang::get('core::validation.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_or_name_will_trigger_an_error()
    {
        $that = $this;
        $data = $this->create();

        $this->create([
            'code' => '2',
            'name' => 'Bar'
        ]);

        $this->browse(function ($browser) use ($data, $that){
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->type('@second.row.code', $data->code)
                    ->type('@second.row.name', $data->name)
                    ->type('@second.row.remarks', $data->remarks)
                    ->click('@second.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::validation.code.unique'))
                    ->assertSee(Lang::get('core::validation.name.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_unique_code_and_name_will_successfully_save_the_data_and_return_a_success_message()
    {
        $that = $this;

        $this->create();
        $this->create([
            'code' => '2',
            'name' => 'Bar'
        ]);

        $this->browse(function ($browser) use ($that){
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->type('@second.row.code', '3')
                    ->type('@second.row.name', 'Baz')
                    ->type('@second.row.remarks', 'Lorem')
                    ->click('@second.row.command')
                    ->pause(3000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_on_the_data_will_show_a_confirm_message()
    {   
        $that = $this;

        $this->create();

        $this->browse(function ($browser) use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.data')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.data'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_and_selecting_the_cancel_button_on_the_data_will_hide_the_confirmation_message()
    {   
        $that = $this;

        $this->create();

        $this->browse(function ($browser) use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@cancel')
                    ->pause(2000)
                    ->assertMissing('@confirm')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_delete_icon_and_selecting_the_ok_button_will_delete_the_data_and_return_a_success_message()
    {   
        $that = $this;

        $this->collection();

        $this->browse(function ($browser) use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_data_will_return_a_no_results_found()
    {   
        $that = $this;

        $this->create();

        $this->browse(function ($browser) use ($that) {
            $page = '\\Tests\\Browser\\Pages\\'.$that->page;
            $browser->authenticate()
                    ->visit(new $page)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(3000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->logout();
        });
    }

    private function data($data = [])
    {
        return array_merge(array(
            'code' => '1',
            'name' => 'Foo',
            'remarks' => 'Lorem'
        ), $data);
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, array_merge(array(
            'code' => '1',
            'name' => 'Foo',
            'remarks' => 'Lorem'
        ), $data));
    }

    private function collection()
    {
        return array(
            Factory::create($this->factory, array(
                'code' => '1',
                'name' => 'Foo',
                'remarks' => 'Lorem'
            )),
            Factory::create($this->factory, array(
                'code' => '2',
                'name' => 'Bar',
                'remarks' => 'Lorem'
            ))
        );
    }
}
