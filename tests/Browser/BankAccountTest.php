<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\BankAccount;
use Illuminate\Support\Str;
use Lang;

class BankAccountTest extends DuskTestCase
{
    private $factory = 'bank_account';
    private $banks;

    public function setUp()
    {
        parent::setUp();

        $this->banks();
    }

    /**
     * @test
     */
    public function accessing_bank_account_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/bank-account')
                    ->pause(3000)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_search_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->assertSeeIn('@filters.account.number', 'Account No')
                    ->assertSeeIn('@filters.account.name', 'Account Name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_account_number_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.account.number')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[2]->account_number)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSelectedChosen('@first.row.bank', (string) $data[2]->bank_id)
                    ->assertInputValue('@first.row.account.number', $data[2]->account_number)
                    ->assertInputValue('@first.row.account.name', $data[2]->account_name)
                    ->assertInputValue('@first.row.opening.balance', $data[2]->formatter()->opening_balance)
                    ->assertMissing('@third.row.bank')
                    ->assertMissing('@third.row.account.number')
                    ->assertMissing('@third.row.account.name')
                    ->assertMissing('@third.row.opening.balance')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_account_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser)  use ($data) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.account.name')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', $data[3]->account_name)
                    ->click('@filters.search')
                    ->pause(3000)
                    ->assertSelectedChosen('@first.row.bank', (string) $data[3]->bank_id)
                    ->assertInputValue('@first.row.account.number', $data[3]->account_number)
                    ->assertInputValue('@first.row.account.name', $data[3]->account_name)
                    ->assertInputValue('@first.row.opening.balance', $data[3]->formatter()->opening_balance)
                    ->assertMissing('@third.row.bank')
                    ->assertMissing('@third.row.account.number')
                    ->assertMissing('@third.row.account.name')
                    ->assertMissing('@third.row.opening.balance')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $that = $this;

        $this->browse(function ($browser) use($that){
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@filters')
                    ->pause(3000)
                    ->click('@filters.account.number')
                    ->click('@filters.put')
                    ->select('@filters.condition.first.operation', '=')
                    ->type('@filters.condition.first.value', '3')
                    ->click('@filters.search')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertSelectedChosen('@first.row.bank', $that->banks[0]->id)
                    ->assertInputValue('@first.row.account.number', '')
                    ->assertInputValue('@first.row.account.name', '')
                    ->assertInputValue('@first.row.opening.balance', '0')
                    ->assertMissing('@second.row.bank')
                    ->assertMissing('@second.row.account.number')
                    ->assertMissing('@second.row.account.name')
                    ->assertMissing('@second.row.opening.balance')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function typing_a_non_numeric_value_in_opening_balance_is_not_allowed()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->clear('@first.row.opening.balance')
                    ->type('@first.row.opening.balance', 'H')
                    ->assertInputValue('@first.row.opening.balance', '')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_empty_account_number_or_account_name_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.account.number.required'))
                    ->assertSee(Lang::get('core::validation.account.name.required'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_opening_balance_will_trigger_an_error()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->clear('@first.row.opening.balance')
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.opening.balance.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_an_existing_account_number_will_trigger_an_error()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->type('@second.row.account.number', $data->account_number)
                    ->type('@second.row.account.name', $data->account_name)
                    ->type('@second.row.opening.balance', $data->formatter()->opening_balance)
                    ->click('@second.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.account.number.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_proper_format_will_successfully_save_the_data_and_return_a_success_message()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->selectChosen('@first.row.bank', (string) $data->bank_id)
                    ->type('@first.row.account.number', $data->account_number)
                    ->type('@first.row.account.name', $data->account_name)
                    ->type('@first.row.opening.balance', $data->formatter()->opening_balance)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->assertSelectedChosen('@first.row.bank', (string) $data->bank_id)
                    ->assertInputValue('@first.row.account.number', $data->account_number)
                    ->assertInputValue('@first.row.account.name', $data->account_name)
                    ->assertInputValue('@first.row.opening.balance', $data->formatter()->opening_balance)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_account_number_account_name_or_opening_balance_will_trigger_an_error()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->clear('@first.row.account.number')
                    ->clear('@first.row.account.name')
                    ->clear('@first.row.opening.balance')
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.account.number.required'))
                    ->assertSee(Lang::get('core::validation.account.name.required'))
                    ->assertSee(Lang::get('core::validation.opening.balance.numeric'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_account_number_will_trigger_an_error()
    {
        $data = $this->create();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->selectChosen('@second.row.bank', (string) $data->bank_id)
                    ->type('@second.row.account.number', $data->account_number)
                    ->type('@second.row.account.name', $data->account_name)
                    ->type('@second.row.opening.balance', $data->formatter()->opening_balance)
                    ->click('@second.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.account.number.unique'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_proper_format_will_successfully_save_the_data_and_return_a_success_message()
    {
        $this->collection();

        $data = $this->build();

        $this->browse(function ($browser) use ($data){
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->selectChosen('@second.row.bank', (string) $data->bank_id)
                    ->type('@second.row.account.number', $data->account_number)
                    ->type('@second.row.account.name', $data->account_name)
                    ->type('@second.row.opening.balance', $data->formatter()->opening_balance)
                    ->click('@second.row.command')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_table_list()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->assertVisible('@confirm')
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.bank.account')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.bank.account'))
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_table_list_will_hide_the_confirm_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@cancel')
                    ->pause(1000)
                    ->assertMissing('@confirm')
                    ->assertSelectedChosen('@first.row.bank', (string) $data[0]->bank_id)
                    ->assertInputValue('@first.row.account.number', $data[0]->account_number)
                    ->assertInputValue('@first.row.account.name', $data[0]->account_name)
                    ->assertInputValue('@first.row.opening.balance', $data[0]->formatter()->opening_balance)
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_table_list_will_delete_the_data_in_table()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertMissing('@confirm')
                    ->assertInputValueIsNot('@first.row.account.number', $data[0]->account_number)
                    ->assertInputValueIsNot('@first.row.account.name', $data[0]->account_name)
                    ->assertInputValueIsNot('@first.row.opening.balance', $data[0]->formatter()->opening_balance)
                    ->assertInputValue('@first.row.account.number', $data[1]->account_number)
                    ->assertInputValue('@first.row.account.name', $data[1]->account_name)
                    ->assertInputValue('@first.row.opening.balance', $data[1]->formatter()->opening_balance)
                    ->logout();
        });
    }
    /**
     * @test
     */
    public function deleting_the_last_row_of_the_table_list_will_return_a_no_results_found()
    {
        $data = $this->create();

        $this->browse(function ($browser) use($data) {
            $browser->authenticate()
                    ->visit(new BankAccount)
                    ->pause(3000)
                    ->click('@first.row.command')
                    ->pause(1000)
                    ->click('@ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertInputValueIsNot('@first.row.account.number', $data->account_number)
                    ->assertInputValueIsNot('@first.row.account.name', $data->account_name)
                    ->assertInputValueIsNot('@first.row.opening.balance', $data->formatter()->opening_balance)
                    ->assertMissing('@second.row.bank')
                    ->assertMissing('@second.row.account.number')
                    ->assertMissing('@second.row.account.name')
                    ->assertMissing('@second.row.opening.balance')
                    ->logout();
        });
    }

    private function build($data = [])
    {
        return Factory::build($this->factory, array_merge(array(
            'bank_id' => $this->banks[rand(0, 1)]->id
        ), $data));
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, array_merge(array(
            'bank_id' => $this->banks[rand(0, 1)]->id
        ), $data));
    }

    private function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'bank_id' => $this->banks[rand(0, 1)]->id
        ));
    }

    private function banks()
    {
        $this->banks = Factory::times(2)->create('bank');
    }
}
