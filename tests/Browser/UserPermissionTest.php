<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laracasts\TestDummy\Factory;
use Tests\Browser\Pages\UserPermission;
use Illuminate\Support\Str;
use Lang;
use DB;

class UserPermissionTest extends DuskTestCase
{
    private $factory = 'user';

    public function setUp() 
    {
        parent::setUp();

        $this->branch();
    }

    /**
     * @test
     */
    public function accessing_user_list_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/user')
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function accessing_user_detail_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->visit('/user/create')
                    ->pause(1500)
                    ->assertPathIs('/');
        });
    }

    /**
     * @test
     */
    public function clicking_the_filters_button_will_display_the_modal_and_its_list_of_filters()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1500)
                    ->click('@filters')
                    ->pause(1500)
                    ->assertSeeIn('@filters.code', 'Code')
                    ->assertSeeIn('@filters.full.name', 'Full Name')
                    ->assertSeeIn('@filters.user.name', 'Username')
                    ->assertSeeIn('@filters.position', 'Position')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_code_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1500)
                    ->click('@filters')
                    ->pause(1500)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->type('@filters.condition.first.value', $data[0]['info']->code)
                    ->click('@filters.search')
                    ->pause(1500)
                    ->assertSeeIn('@first.row.code', $data[0]['info']->code)
                    ->assertSeeIn('@first.row.full.name', $data[0]['info']->full_name)
                    ->assertSeeIn('@first.row.user.name', $data[0]['info']->username)
                    ->assertMissing('@second.row.code')
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.user.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_full_name_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1500)
                    ->click('@filters')
                    ->click('@filters.full.name')
                    ->click('@filters.put')
                    ->type('@filters.condition.first.value', $data[1]['info']->full_name)
                    ->click('@filters.search')
                    ->pause(1500)
                    ->assertSeeIn('@first.row.code', $data[1]['info']->code)
                    ->assertSeeIn('@first.row.full.name', $data[1]['info']->full_name)
                    ->assertSeeIn('@first.row.user.name', $data[1]['info']->username)
                    ->assertMissing('@second.row.code')
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.user.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_by_username_and_clicking_the_search_will_return_a_collection_of_data_based_on_filters()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1500)
                    ->click('@filters')
                    ->click('@filters.user.name')
                    ->click('@filters.put')
                    ->type('@filters.condition.first.value', $data[2]['info']->username)
                    ->click('@filters.search')
                    ->pause(1500)
                    ->assertSeeIn('@first.row.code', $data[2]['info']->code)
                    ->assertSeeIn('@first.row.full.name', $data[2]['info']->full_name)
                    ->assertSeeIn('@first.row.user.name', $data[2]['info']->username)
                    ->assertMissing('@second.row.code')
                    ->assertMissing('@second.row.full.name')
                    ->assertMissing('@second.row.user.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_filter_that_doesnt_match_any_data_and_clicking_the_search_will_return_no_data()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1500)
                    ->click('@filters')
                    ->pause(1500)
                    ->click('@filters.code')
                    ->click('@filters.put')
                    ->type('@filters.condition.first.value', 'H')
                    ->click('@filters.search')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('@first.row.code')
                    ->assertMissing('@first.row.full.name')
                    ->assertMissing('@first.row.user.name')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function a_confirmation_message_should_appear_upon_clicking_the_delete_icon_in_table_list()
    {
        $this->collection();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->pause(1000)
                    ->assertVisible('@confirm')
                    ->assertSeeIn('@confirm', Str::upper(Lang::get('core::label.delete.user')))
                    ->assertSeeIn('@confirm', Lang::get('core::confirm.delete.user'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_cancel_button_upon_confirmation_in_table_list_will_hide_the_confirm_message()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->pause(1000)
                    ->click('@confirm.cancel')
                    ->pause(1000)
                    ->assertMissing('@confirm')
                    ->assertSeeIn('@first.row.full.name', $data[0]['info']->full_name)
                    ->assertSeeIn('@first.row.code', $data[0]['info']->code)
                    ->assertSeeIn('@second.row.full.name', $data[1]['info']->full_name)
                    ->assertSeeIn('@second.row.code', $data[1]['info']->code)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_the_ok_button_upon_confirmation_in_table_list_will_delete_the_data_in_table()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->pause(1000)
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.deleted'))
                    ->assertMissing('@confirm.list')
                    ->assertDontSeeIn('@first.row.full.name', $data[0]['info']->full_name)
                    ->assertDontSeeIn('@first.row.code', $data[0]['info']->code)
                    ->assertSeeIn('@first.row.full.name', $data[1]['info']->full_name)
                    ->assertSeeIn('@first.row.code', $data[1]['info']->code)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function deleting_the_last_row_of_the_table_list_will_return_a_no_results_found()
    {
        $this->create();

        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(3000)
                    ->click('@first.row.delete')
                    ->pause(1000)
                    ->click('@confirm.ok')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::info.no.results.found'))
                    ->assertMissing('first.row.full.name')
                    ->assertMissing('first.row.code')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_add_new_user_will_redirect_to_create_user()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->assertPathIs('/user/create')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function selecting_a_branch_will_show_the_list_of_permission_sections()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->assertMissing('@sections')
                    ->selectChosen('@branch', '1')
                    ->assertVisible('@sections')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_permission_section_will_toggle_its_corresponding_permissions()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->assertVisible('@section.general.checkbox')
                    ->click('@section.data')
                    ->assertMissing('@section.general.checkbox')
                    ->assertVisible('@section.data.checkbox')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function permissions_in_a_specific_section_should_be_checked_if_permissions_section_checkbox_is_checked()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.data')
                    ->check('@section.data.checkbox')
                    ->assertChecked('@view.pbc.permission')
                    ->assertChecked('@view.detail.pbc.permission')
                    ->assertChecked('@create.pbc.permission')
                    ->assertChecked('@edit.pbc.permission')
                    ->assertChecked('@delete.pbc.permission')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function permission_section_should_be_checked_if_all_of_the_permissions_in_a_section_is_checked()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@login.permission')
                    ->assertChecked('@section.general.checkbox')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function permissions_in_a_specific_section_should_be_unchecked_if_permission_section_is_unchecked()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@login.permission')
                    ->uncheck('@section.general.checkbox')
                    ->assertNotChecked('@login.permission')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function permissions_checked_should_be_unchecked_if_branch_is_changed()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@login.permission')
                    ->selectChosen('@branch', '2')
                    ->assertNotChecked('@login.permission')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function permissions_checked_should_still_be_checked_if_the_corresponding_branch_is_selected_again()
    {
        $this->browse(function ($browser) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@create')
                    ->pause(1000)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@login.permission')
                    ->selectChosen('@branch', '2')
                    ->selectChosen('@branch', '1')
                    ->assertChecked('@login.permission')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_without_details_will_display_error_message()
    {
        $that = $this;

        $this->browse(function ($browser) use ($that) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->click('@save')
                    ->pause(500)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.required'),
                        Lang::get('core::validation.username.required'),
                        Lang::get('core::validation.password.required'),
                        Lang::get('core::validation.full.name.required')
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_with_existing_code_username_or_full_name_will_display_error_message() 
    {
        $that = $this;

        $data = $this->collection();

        $this->browse(function ($browser) use ($data, $that) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->type('@code', $data[0]['info']->code)
                    ->type('@user.name', $data[0]['info']->username)
                    ->type('@full.name', $data[0]['info']->full_name)
                    ->type('@password', $that->faker->name)
                    ->click('@tab.permissions')
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@section.general.checkbox')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSeeArray(array(
                        Lang::get('core::validation.code.unique'),
                        Lang::get('core::validation.username.unique'),
                        Lang::get('core::validation.full.name.unique'),
                    ))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_with_short_password_will_display_error_message() 
    {
        $that = $this;

        $data = $this->build();

        $this->browse(function ($browser) use ($that, $data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->type('@code', $data['info']->code)
                    ->type('@user.name', $data['info']->username)
                    ->type('@full.name', $data['info']->full_name)
                    ->type('@password', 'short')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::validation.password.min.length'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_with_incorrect_format_of_email_will_display_error_message()
    {
        $that = $this;

        $data = $this->build();

        $this->browse(function ($browser) use ($that, $data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->type('@code', $data['info']->code)
                    ->type('@user.name', $data['info']->username)
                    ->type('@full.name', $data['info']->full_name)
                    ->type('@password', $data['info']->password)
                    ->type('@email', $that->faker->name)
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::validation.email.invalid'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_without_permissions_will_display_error_message()
    {
        $data = $this->build();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->type('@code', $data['info']->code)
                    ->type('@user.name', $data['info']->username)
                    ->type('@full.name', $data['info']->full_name)
                    ->type('@password', $data['info']->password)
                    ->type('@email', $data['info']->email)
                    ->click('@save')
                    ->pause(500)
                    ->assertSee(Lang::get('core::validation.permissions.min.length'))
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function saving_with_correct_details_will_display_message_redirect_to_list_and_add_the_data_in_list()
    {
        $that = $this;
        $data = $this->build();

        $this->browse(function ($browser) use ($data, $that) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(500)
                    ->click('@create')
                    ->pause(500)
                    ->type('@code', $data['info']->code)
                    ->type('@user.name', $data['info']->username)
                    ->type('@full.name', $data['info']->full_name)
                    ->type('@password', $that->faker->name)
                    ->type('@email', $data['info']->email)
                    ->click('@tab.permissions')
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@section.general.checkbox')
                    ->pause(1000)
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.created'))
                    ->pause(3000)
                    ->assertPathIs('/user')
                    ->assertSeeIn('@first.row.code', $data['info']->code)
                    ->assertSeeIn('@first.row.user.name', $data['info']->username)
                    ->assertSeeIn('@first.row.full.name', $data['info']->full_name)
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function clicking_a_user_in_the_list_will_display_detail()
    {
        $data = $this->collection();

        $this->browse(function ($browser) use ($data) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@first.row.code')
                    ->pause(3000)
                    ->assertInputValue('@code', $data[0]['info']->code)
                    ->assertInputValue('@user.name', $data[0]['info']->username)
                    ->assertInputValue('@full.name', $data[0]['info']->full_name)
                    ->assertInputValue('@telephone', $data[0]['info']->telephone)
                    ->assertInputValue('@mobile', $data[0]['info']->mobile)
                    ->assertInputValue('@email', $data[0]['info']->email)
                    ->assertInputValue('@address', $data[0]['info']->address)
                    ->assertInputValue('@memo', $data[0]['info']->memo)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->assertChecked('@section.general.checkbox')
                    ->assertChecked('@login.permission')
                    ->click('@section.data')
                    ->assertChecked('@section.data.checkbox')
                    ->assertChecked('@view.pbc.permission')
                    ->assertChecked('@view.detail.pbc.permission')
                    ->assertChecked('@create.pbc.permission')
                    ->assertChecked('@edit.pbc.permission')
                    ->assertChecked('@delete.pbc.permission')
                    ->logout();
        });
    }

    /**
     * @test
     */
    public function updating_a_user_with_correct_details_will_display_success_message()
    {
        $this->collection();

        $that = $this;
        $data = $this->build();

        $this->browse(function ($browser) use ($data, $that) {
            $browser->authenticate()
                    ->visit(new UserPermission)
                    ->pause(1000)
                    ->click('@first.row.code')
                    ->pause(1000)
                    ->assertPathIs('/user/2/edit')
                    ->type('@code', $data['info']->code)
                    ->type('@user.name', $data['info']->username)
                    ->type('@full.name', $data['info']->full_name)
                    ->type('@telephone', $data['info']->telephone)
                    ->type('@mobile', $data['info']->mobile)
                    ->type('@email', $data['info']->email)
                    ->type('@address', $data['info']->address)
                    ->type('@memo', $data['info']->memo)
                    ->type('@password', $that->faker->password)
                    ->click('@tab.permissions')
                    ->pause(1000)
                    ->selectChosen('@branch', '1')
                    ->click('@section.general')
                    ->check('@section.general.checkbox')
                    ->click('@section.data')
                    ->check('@section.data.checkbox')
                    ->click('@save')
                    ->pause(1000)
                    ->assertSee(Lang::get('core::success.updated'))
                    ->pause(4000)
                    ->assertPathIs('/user')
                    ->assertSeeIn('@first.row.code', $data['info']->code)
                    ->assertSeeIn('@first.row.user.name', $data['info']->username)
                    ->assertSeeIn('@first.row.full.name', $data['info']->full_name)
                    ->logout();
        });
    }

    private function create($range = 6, $data = [])
    {
        $data = [];
        
        $user = Factory::create($this->factory, $data);

        $permissions = array();

        for ($i=1; $i <= $range; $i++) { 
            $permissions[] = $i;

            DB::table('branch_permission_user')->insert([
                'branch_id' => 1,
                'permission_id' => $i,
                'user_id' => $user->id
            ]);
        }

        return array(
            'info' => $user,
            'permissions' => $permissions
        );
    }

    private function build($range = 6, $data = [])
    {
        $data = [];
        
        $user = Factory::build($this->factory, $data);

        $permissions = [];

        for ($i=1; $i <= $range; $i++) { 
            $permissions[] = $i;

            DB::table('branch_permission_user')->insert([
                'branch_id' => 1,
                'permission_id' => $i,
                'user_id' => $user->id
            ]);
        }

        return array(
            'info' => $user,
            'permissions' => $permissions
        );
    }

    private function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) { 
            $data[] = $this->create();
        }

        return $data;
    }

    private function branch()
    {
        Factory::create('branch');
    }
}
