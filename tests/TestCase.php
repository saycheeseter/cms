<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as LaravelTestCase;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutEvents;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\Traits\Authentication;
use Tests\Traits\Migrations;
use Tests\Traits\PHPSettingsOverride;
use Tests\Traits\ResetTableIndex;
use Tests\Traits\SmartSeeder;

abstract class TestCase extends LaravelTestCase
{
    use WithFaker;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Overrides setUpTraits on parent to prevent race condition between custom migration trait
     * command and database transaction trait
     *
     * @return array|null
     */
    protected function setUpTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[DatabaseTransactions::class])) {
            $this->beginDatabaseTransaction();
        }

        if (isset($uses[RefreshDatabase::class])) {
            $this->refreshDatabase();
        }

        if (isset($uses[Migrations::class])) {
            $this->runMigrations();
        }

        if (isset($uses[ResetTableIndex::class])) {
            $this->resetTableIndex();
        }

        if (isset($uses[WithoutMiddleware::class])) {
            $this->disableMiddlewareForAllTests();
        }

        if (isset($uses[WithoutEvents::class])) {
            $this->disableEventsForAllTests();
        }

        if (isset($uses[WithFaker::class])) {
            $this->setUpFaker();

            $this->faker->unique(true);
        }

        if (isset($uses[SmartSeeder::class])) {
            $this->runSmartSeed();
        }

        if (isset($uses[PHPSettingsOverride::class])) {
            $this->changePHPSettings();
        }

        if (isset($uses[Authentication::class])) {
            $this->login();
        }

        return $uses;
    }
}