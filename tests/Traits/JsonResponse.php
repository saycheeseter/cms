<?php

namespace Tests\Traits;

trait JsonResponse
{
    /**
     * Response format used by system API
     * 
     * @param  bool $isSucessful
     * @param  array $data       
     * @param  string $message    
     * @param  array $errors     
     * @return array             
     */
    protected function response(
        $isSucessful,
        $data,
        $message, 
        $errors
    ) {
        return array(
            'isSuccessful' => $isSucessful,
            'values' => $data,
            'message' => $message,
            'errors' => $errors
        );
    }

    protected function successfulResponse($data = array(), $message = 'Request success.')
    {
        return $this->response(true, $data, $message, array());
    }

    protected function noEntryFoundResponse($errors = array(), $message = 'No entry found.')
    {
        return $this->response(false, array(), $message, $errors);
    }

    protected function errorResponse($errors = array(), $message = 'Request failed.')
    {
        return $this->response(false, array(), $message, $errors);
    }

    protected function notAuthorizedResponse($message = 'You are not authorized to access this page.')
    {
        return $this->response(false, array(), $message, array());
    }
}