<?php

namespace Tests\Traits;

use Modules\Core\Repositories\Contracts\BranchSettingRepository;
use Modules\Core\Repositories\Contracts\GenericSettingRepository;
use App;
use DB;

trait UpdatesApplicationSettings 
{
    /**
     * Refresh existing instance of the settings
     */
    protected function refreshSettings()
    {
        $this->app->singleton('BranchSettings', function ($app) {
            return App::make(BranchSettingRepository::class)->all()->pluck('value', 'key')->toArray();
        });

        $this->app->singleton('GenericSettings', function ($app) {
            return App::make(GenericSettingRepository::class)->all()->pluck('value', 'key')->toArray();
        });
    }

    /**
     * Updates the generic settings values in the database
     *
     * @param $keys
     * @param $value
     */
    protected function genericSetting($keys, $value)
    {
        $this->setting($keys, $value, 'generic_settings');
    }

    /**
     * Updates the branch settings values in the database
     *
     * @param $keys
     * @param $value
     */
    protected function branchSetting($keys, $value)
    {
        $this->setting($keys, $value, 'branch_settings');
    }

    /**
     * Updates the settings value in the database based on the table
     * and refresh the current settings singleton instance
     * 
     * @param $keys
     * @param $value
     * @param $table
     */
    private function setting($keys, $value, $table)
    {
        $table = DB::table($table);

        if (!is_array($keys)) {
            $keys = [$keys];
        }

        foreach ($keys as $key) {
            $table = $table->orWhere('key', $key);
        }

        $table->update(['value' => $value]);

        $this->refreshSettings();
    }
}