<?php

namespace Tests\Traits;

use Illuminate\Support\Facades\Schema;

trait Assertions
{
    /**
     * Assert if the given column exists in the specified table
     * 
     * @param  string $table
     * @param  string $field
     * @return $this
     */
    protected function assertTableHasColumn($table, $field)
    {
        $this->assertTrue(Schema::hasColumn($table, $field), "Failed to assert that column $field exists in the specified table $table");

        return $this;
    }

    /**
     * Assert if the given column does not exists in the specified table
     * 
     * @param  string $table
     * @param  string $field
     * @return $this
     */
    protected function assertTableHasNoColumn($table, $field)
    {
        $this->assertFalse(Schema::hasColumn($table, $field), "Failed to assert that column $field doesn\'t exists in the specified table $table");

        return $this;
    }

    /**
     * Assert the the given column is of the specified data type
     * 
     * @param  string $table
     * @param  string $column
     * @param  string $type
     * @return $this
     */
    protected function assertTableColumnIsOfType($table, $column, $type)
    {
        $this->assertTrue(Schema::getColumnType($table, $column) === $type, "Failed to assert that column $column is of type $type in the specified table $table");

        return $this;
    }

    /**
     * Assert the the given column is not of the specified data type
     * 
     * @param  string $table
     * @param  string $column
     * @param  string $type
     * @return $this
     */
    protected function assertTableColumnIsNotOfType($table, $column, $type)
    {
        $this->assertFalse(Schema::getColumnType($table, $column) === $type, "Failed to assert that column $column is not of type $type in the specified table $table");

        return $this;
    }

    /**
     * Shortcut method for asserting if set of data exists in database
     *
     * @param $table
     * @param $data
     * @return $this
     */
    protected function assertDatabaseHave($table, $data)
    {
        foreach ($data as $datum) {
            $this->assertDatabaseHas($table, $datum);
        }

        return $this;
    }

    /**
     * Shortcut method for asserting if set of data doesn't exists in database
     *
     * @param $table
     * @param $data
     * @return $this
     */
    protected function assertDatabaseHaveMissing($table, $data)
    {
        foreach ($data as $datum) {
            $this->assertDatabaseMissing($table, $datum);
        }

        return $this;
    }
}