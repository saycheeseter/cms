<?php

namespace Tests\Traits;

use Auth;

trait Authentication
{
    /**
     * Automatically log-in dummy user
     *
     * @param int $id
     * @param int $branch
     * @return $this
     */
    protected function login($id = 1, $branch = 1)
    {
        Auth::loginUsingId($id);

        Auth::loginBranchUsingId($branch);

        return $this;
    }

    /**
     * Logout user
     * 
     * @return $this
     */
    protected function logout()
    {
        Auth::logout();
    }
}