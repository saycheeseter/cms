<?php

namespace Tests\Traits;

trait PHPSettingsOverride
{
    protected function changePHPSettings()
    {
        ini_set('memory_limit', '2G');
    }
}