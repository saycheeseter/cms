<?php

namespace Tests\Traits;

use Exception;
use Illuminate\Database\QueryException;
use DB;

trait ResetTableIndex
{
    protected function resetTableIndex()
    {
        $connection = env('DB_CONNECTION');

        $driver = config("database.connections.{$connection}.driver");

        if (is_null($driver)) {
            throw new Exception('No driver provided!');
        }

        foreach ($this->getTableList() as $key => $table) {
            try {
                switch ($driver) {
                    case 'mysql':
                        DB::statement(sprintf('ALTER TABLE %s AUTO_INCREMENT=1', $table));
                        break;

                    case 'sqlsrv':
                        DB::statement(sprintf('DBCC CHECKIDENT (%s, RESEED, 1)', $table));
                        break;

                    case 'sqlite':
                        DB::statement(sprintf('UPDATE SQLITE_SEQUENCE SET SEQ=1 WHERE NAME=\'%s\'', $table));
                        break;
                }
            } catch (QueryException $e) {
                continue;
            }
        }
    }

    protected function getTableList()
    {
        $connection = env('DB_CONNECTION');

        $driver = config("database.connections.{$connection}.driver");

        $tables = [];

        switch ($driver) {
            case 'mysql':
                $result = DB::select('SHOW TABLES');
                $property = 'Tables_in_test';
                break;

            case 'sqlite':
                $result = DB::select('SELECT name FROM sqlite_master WHERE type=\'table\'');
                $property = 'name';
                break;

            case 'sqlsrv':
                $result = DB::select('SELECT * FROM INFORMATION_SCHEMA.TABLES');
                $property = 'TABLE_NAME';
                break;
        }

        foreach ($result as $key => $table) {
            $tables[] = $table->{$property};
        }

        return $tables;
    }
}