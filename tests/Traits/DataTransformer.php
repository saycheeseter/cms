<?php

namespace Tests\Traits;

use Litipk\BigNumbers\Decimal;
use Carbon\Carbon;

trait DataTransformer 
{
    /**
     * API for transformer single item
     * 
     * @param  array $data       
     * @param  string $transformer
     * @return array             
     */
    protected function item($data, $transformer)
    {
        return $this->{$transformer}($data, $transformer);
    }

    /**
     * API for transformer collection
     * 
     * @param  mixed $data
     * @param  string $transformer
     * @return array             
     */
    protected function collection($data, $transformer)
    {
        $new = [];

        foreach ($data as $key => $value) {
            $new[] = $this->item($value, $transformer);
        }

        return $new;
    }

    protected function toArrayCast($model)
    {
        $data = [];

        foreach($model->getAttributes() as $key => $value) {
            if($model->$key instanceof Decimal) {
                $data[$key] = $model->$key->innerValue();
            } else if($model->$key instanceof Carbon && array_key_exists($key, $model->getCasts())) {
                $type = $model->getCasts()[$key];

                $data[$key] = $type === 'date'
                    ? $model->$key->toDateString()
                    : $model->$key->toDateTimeString();
            } else {
                $data[$key] = $model->$key;
            }
        }

        return $data;
    }
}