<?php

namespace Tests\Traits;

trait Migrations
{
    /**
     * Execute table migrations
     *
     */
    protected function runMigrations()
    {
        $this->artisan('module:migrate');
        $this->artisan('migrate');

        return $this;
    }
}