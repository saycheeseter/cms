<?php

namespace Tests\Traits;

use Modules\Core\Entities\Base;

trait SmartSeeder
{
    protected function runSmartSeed()
    {
        $this->artisan('seed');

        Base::clearBootedModels();
    }
}