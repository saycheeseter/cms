<?php

namespace Tests\Traits;

use Modules\Core\Entities\Permission;
use DB;

trait GivesUserPermissions
{
    public function givePermissions($userId, $branchId, $permissions)
    {
        $permissions = array_wrap($permissions);

        $ids = Permission::whereIn('alias', $permissions)
            ->pluck('id')
            ->toArray();

        $collection = [];

        foreach ($ids as $id) {
            $collection[] = [
                'user_id' => $userId,
                'branch_id' => $branchId,
                'permission_id' => $id
            ];
        }


        DB::table(config('auth.providers.permissions.table'))->insert($collection);
    }
}