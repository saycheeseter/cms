<?php

namespace Tests\Unit;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Session\Session;
use Mockery;
use Modules\Core\Entities\Branch;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;
use Modules\Core\Extensions\Guard\NSIGuard;

class NSIGuardTest extends TestCase
{
    /**
     * @test
     */
    public function setting_a_config_other_than_a_branch_instance_will_not_be_set_in_session()
    {
        $guard = $this->createGuard();

        $guard->getSession()
            ->shouldNotReceive('put')
            ->with($this->salt('branch', $guard), 'conf');

        $guard->getSession()
            ->shouldNotReceive('migrate')
            ->with(true);

        $guard->setConfig('foo', 'bar');
    }

    /**
     * @test
     */
    public function setting_a_branch_instance_as_a_config_will_be_set_in_session()
    {
        $guard = $this->createGuard();

        list($branch) = $this->getMockConfig();

        $branch->shouldReceive('getAttribute')
            ->with('id')
            ->andReturn(1);

        $guard->getSession()
            ->shouldReceive('put')
            ->with($this->salt('branch', $guard), 1);

        $guard->getSession()
            ->shouldReceive('migrate')
            ->with(true);

        $guard->setConfig($branch);
    }

    /**
     * @test
     */
    public function check_method_will_also_check_if_branch_session_id_is_set()
    {
        $session = Mockery::mock(Session::class);
        $userProvider = Mockery::mock(UserProvider::class);
        $request = Mockery::mock(Request::class);
        $request->cookies = Mockery::mock(ParameterBag::class);

        $guard = Mockery::mock(NSIGuard::class.'[user]', [
            'nsi',
            $userProvider,
            $session,
            $request
        ]);

        $guard->shouldReceive('user')->andReturn('bar');

        $guard->getSession()
            ->shouldReceive('get')
            ->with($this->salt('branch', $guard));

        $guard->check();
    }

    protected function getMockConfig()
    {
        return [
            Mockery::mock(Branch::class.'[getAttribute]')
        ];
    }

    protected function createGuard()
    {
        $session = Mockery::mock(Session::class);
        $userProvider = Mockery::mock(UserProvider::class);
        $request = Mockery::mock(Request::class);
        $request->cookies = Mockery::mock(ParameterBag::class);

        return new NSIGuard(
            'nsi',
            $userProvider,
            $session,
            $request
        );
    }

    protected function salt($name, $guard = null)
    {
        $guard = $guard ?? 'nsi';

        return $name.'_nsi_'.sha1(get_class($guard));
    }
}
