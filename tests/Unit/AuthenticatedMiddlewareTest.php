<?php

namespace Tests\Unit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery;
use Modules\Core\Http\Middleware\Authenticate;
use Tests\TestCase;

class AuthenticatedMiddlewareTest extends TestCase
{
    /**
     * @test
     */
    public function allow_request_if_current_url_is_excluded()
    {
        $urls = [
            'auth',
            '_dusk',
            'logout',
            'lang',
        ];

        foreach ($urls as $key => $value) {
            $request = Mockery::mock(Request::class);

            $this->setRequestSegment($request, $value);

            $this->assertEquals('next', $this->createMiddleware()
                ->handle($request, $this->createClosure())
            );
        }
    }

    /**
     * @test
     */
    public function redirect_to_dashboard_if_url_is_login_and_still_authenticated()
    {
        $request = Mockery::mock(Request::class);

        $this->setRequestSegment($request,null, 2);
        $this->setGuardCheck(true);

        $response = $this->createMiddleware()
            ->handle($request, $this->createClosure());

        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $response);

        $this->assertEquals($this->app['url']->to('/dashboard'), $response->headers->get('Location'));
    }

    /**
     * @test
     */
    public function redirect_to_login_if_not_excluded_url_and_not_authenticated()
    {
        $request = Mockery::mock(Request::class);

        $this->setRequestSegment($request,'brand', 2);
        $this->setGuardCheck(false);

        $response = $this->createMiddleware()
            ->handle($request, $this->createClosure());

        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $response);

        $this->assertEquals($this->app['url']->to('/'), $response->headers->get('Location'));
    }

    /**
     * Set mock path for request class
     *
     * @param $path
     * @param int $times
     * @return mixed
     */
    private function setRequestSegment($request, $path, $times = 1)
    {
        $request
            ->shouldReceive('segment')
            ->times($times)
            ->andReturn($path);
    }

    private function createMiddleware()
    {
        return new Authenticate;
    }

    private function createClosure($value = 'next')
    {
        return (function() use ($value) { return $value; });
    }

    /**
     * Set mock check to given boolean value
     *
     * @param boolean $value
     */
    private function setGuardCheck($value = true)
    {
        Auth::shouldReceive('guard')
            ->with(null)
            ->once()
            ->andReturnSelf()
            ->shouldReceive('check')
            ->andReturn($value);
    }
}
