#!/bin/bash
read -p 'Please indicate the directory path: ' directory_path
mkdir -p $directory_path
git clone https://mldpena@github.com/nelsoft/CIRMS.git $directory_path
cd $directory_path
npm config set python python2.7
npm config set msvs_version 2015
php -r "file_exists('.env') || copy('.env.example', '.env');"
composer install --no-dev
php artisan key:generate
php artisan passport:keys
php artisan project:set-env
php artisan storage:link
sh deploy.sh
npm install
gulp --production
php artisan project:set-admin-key
echo Done installing the application.
sleep 10