<?php

namespace Modules\Api\Transformers\PurchaseInboundDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\PurchaseInboundDetail;

class ApiTransformer extends BaseApiTransformer
{
    public function transform(PurchaseInboundDetail $model)
    {
        return[
            'id'                   => $model->id,
            'product'              => [
                'id'   => $model->product_id,
                'link' => $this->link('ProductController@show', ['id' => $model->product_id])
            ],
            'unit'                 => [
                'id'   => $model->unit_id,
                'link' => $this->link('UOMController@show', ['id' => $model->unit_id])
            ],
            'unit_qty'             => $model->number()->unit_qty,
            'qty'                  => $model->number()->qty,
            'oprice'               => $model->number()->oprice,
            'price'                => $model->number()->price,
            'discount1'            => $model->number()->discount1,
            'discount2'            => $model->number()->discount2,
            'discount3'            => $model->number()->discount3,
            'discount4'            => $model->number()->discount4,
            'remarks'              => $model->remarks,
            'deleted_at'           => $model->deleted_at,
            'created_at'           => $model->created_at,
            'updated_at'           => $model->updated_at,
            'transactionable_id'   => $model->transactionable_id,
            'transactionable_type' => $model->transactionable_type,
            'group_type'           => [
                'value' => $model->group_type,
                'label' => $model->presenter()->group_type
            ],
            'product_type'         => [
                'value' => $model->product_type,
                'label' => $model->presenter()->product_type
            ],
            'manageable'           => [
                'value' => $model->manageable,
                'label' => $model->presenter()->manageable
            ],
            'ma_cost'              => $model->number()->ma_cost,
        ];
    }
}