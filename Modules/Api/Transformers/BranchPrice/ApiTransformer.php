<?php

namespace Modules\Api\Transformers\BranchPrice;

use League\Fractal\TransformerAbstract;
use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\BranchPrice;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\BranchPrice;
 */
class ApiTransformer extends BaseApiTransformer
{   
    /**
     * Transform the BranchPrice entity
     * @param BranchPrice $model
     *
     * @return array
     */
    public function transform(BranchPrice $model)
    {
        return [
            'purchase_price'          => $model->number()->purchase_price,
            'selling_price'          => $model->number()->selling_price,
            'wholesale_price'          => $model->number()->wholesale_price,
            'price_a'          => $model->number()->price_a,
            'price_b'          => $model->number()->price_b,
            'price_c'          => $model->number()->price_c,
            'price_d'          => $model->number()->price_d,
            'price_e'          => $model->number()->price_e,
            'editable'          => $model->editable,
            'branch' => [
                'id' => $model->branch_id,
                'link' => $this->link('BranchController@show' ,['id' => $model->branch_id])
            ]
        ];
    }
}