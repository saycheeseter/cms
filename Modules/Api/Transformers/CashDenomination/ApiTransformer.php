<?php

namespace Modules\Api\Transformers\CashDenomination;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\CashDenomination;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\CashDenomination;
 */
class ApiTransformer extends BaseApiTransformer
{
    /**
     * Transform the CashDenomination entity
     * @param CashDenomination $model
     *
     * @return array
     */
    public function transform(CashDenomination $model)
    {
        return [
            'terminal_number' => $model->terminal_number,
            'create_date' => $model->create_date->toDateTimeString(),
            'b1000' => $model->b1000,
            'b500' => $model->b500,
            'b200' => $model->b200,
            'b100' => $model->b100,
            'b50' => $model->b50,
            'b20' => $model->b20,
            'c10' => $model->c10,
            'c5' => $model->c5,
            'c1' => $model->c1,
            'c25c' => $model->c25c,
            'c10c' => $model->c10c,
            'c5c' => $model->c5c,
            'type' => $model->type,
            'created_by'     => [
                'id' => $model->created_by,
                'link' => $this->link('UserController@show' ,['id' => $model->created_by])
            ],
            'created_from'     => [
                'id' => $model->created_from,
                'link' => $this->link('BranchController@show' ,['id' => $model->created_from])
            ]
        ];
    }
}
