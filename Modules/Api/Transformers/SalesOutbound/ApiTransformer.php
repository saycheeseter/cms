<?php

namespace Modules\Api\Transformers\SalesOutbound;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\SalesOutbound;
use Modules\Api\Transformers\SalesOutboundDetail\ApiTransformer as SalesOutboundDetailTransformer;

class ApiTransformer extends BaseApiTransformer
{
    protected $defaultIncludes = [
        'details'
    ];

    public function transform(SalesOutbound $model)
    {
        return[
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'created_from'          => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', $model->created_from)
            ],
            'requested_by'          => [
                'id'   => $model->requested_by,
                'link' => $this->link('UserController@show', $model->requested_by)
            ],
            'created_by'            => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', $model->created_by)
            ],
            'modified_by'           => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'audited_by'            => [
                'id'   => $model->audited_by,
                'link' => $this->link('UserController@show', ['id' => $model->audited_by])
            ],
            'for_location'          => [
                'id'   => $model->for_location,
                'link' => $this->link('BranchController@show', $model->for_location)
            ],
            'salesman'              => [
                'id'   => $model->salesman_id,
                'link' => $this->link('UserController@show', $model->salesman_id)
            ],
            'customer'              => [
                'id'   => $model->customer_id,
                'link' => $this->link('CustomerController@show', $model->customer_id)
            ],
            'customer_detail'       => [
                'id'   => $model->customer_detail,
                'link' => $this->link('CustomerController@show', $model->customer_id)
            ],
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'audited_date'          => $model->audited_date,
            'term'                  => $model->term,
            'transaction_date'      => $model->transaction_date,
            'transaction_type'      => [
                'value' => $model->transaction_type,
                'label' => $model->presenter()->transaction_type
            ],
            'approval_status'       => [
                'value' => $model->approval_status,
                'label' => $model->presenter()->approval
            ],
            'reference'             => [
                'value' => $model->reference_id,
            ],
            'total_amount'          => $model->number()->total_amount,
            'remarks'               => $model->remarks,
            'deleted_at'            => $model->deleted_at,
            'created_at'            => $model->created_at,
            'updated_at'            => $model->updated_at,
            'remaining_amount'      => $model->number()->remaining_amount,
            'transactionable_id'    => $model->transactionable_id,
            'transactionable_type'  => $model->transactionable_type,
        ];
    }

    public function includeDetails(SalesOutbound $model)
    {
        return $this->collection($model->details, new SalesOutboundDetailTransformer);
    }
}