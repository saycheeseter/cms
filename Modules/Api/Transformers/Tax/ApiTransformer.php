<?php

namespace Modules\Api\Transformers\Tax;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\Tax;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\Tax;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the Tax entity
     * @param Tax $model
     *
     * @return array
     */
    public function transform(Tax $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'percentage' => $model->percentage,
            'is_inclusive' => $model->is_inclusive
        ];
    }
}
