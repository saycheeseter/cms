<?php

namespace Modules\Api\Transformers\ProductDiscountTarget;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductDiscountTarget;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductDiscountTarget;
 */
class ApiTransformer extends BaseApiTransformer
{
    /**
     * Transform the ProductDiscountTarget entity
     * @param ProductDiscountTarget $model
     *
     * @return array
     */
    public function transform(ProductDiscountTarget $model)
    {
        return [
            'product_discount_id'    => $model->product_discount_id,
            'target_id'    => $model->target_id,
            'target_type'    => $model->target_type,
        ];
    }
}
