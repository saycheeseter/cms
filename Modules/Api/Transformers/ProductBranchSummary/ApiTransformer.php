<?php

namespace Modules\Api\Transformers\ProductBranchSummary;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductBranchSummary;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductBranchSummary;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the ProductBranchSummary entity
     * @param ProductBranchSummary $model
     *
     * @return array
     */
    public function transform(ProductBranchSummary $model)
    {
        return [
            'reserved_qty' => $model->number()->reserved_qty,
            'requested_qty' => $model->number()->requested_qty,
            'qty' => $model->number()->qty,
            'branch' => [
                'id' => $model->branch_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }
}
