<?php

namespace Modules\Api\Transformers\GiftCheck;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\GiftCheck;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\GiftCheck;
 */
class ApiTransformer extends BaseApiTransformer
{
    /**
     * Transform the GiftCheck entity
     * @param GiftCheck $model
     *
     * @return array
     */
    public function transform(GiftCheck $model)
    {
        return [
            'id'               => $model->id,
            'code'             => $model->code,
            'check_number'     => $model->check_number,
            'amount'           => $model->number()->amount,
            'date_from'        => $model->date_from->toDateString(),
            'date_to'          => $model->date_to->toDateString(),
            'redeemed_date'    => $model->redeemed_date ? $model->redeemed_date->toDateTimeString() : null,
            'status'           => $model->status,
            'redeemed_by'      => $model->redeemed_by,
            'generated_rand'   => $model->generated_rand,
            'reference_number' => $model->reference_number,
            'created_by'       => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'      => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'created_from'     => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ]
        ];
    }
}
