<?php

namespace Modules\Api\Transformers\PurchaseInbound;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Api\Transformers\PurchaseInboundDetail\ApiTransformer as PurchaseInboundDetailApiTransformer;

class ApiTransformer extends BaseApiTransformer
{
    protected $defaultIncludes = [
        'details'
    ];

    public function transform(PurchaseInbound $model)
    {
        return [
            'id'                   => $model->id,
            'sheet_number'         => $model->sheet_number,
            'created_from'         => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', $model->created_from)
            ],
            'created_by'           => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', $model->created_by)
            ],
            'modified_by'          => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'audited_by'           => [
                'id'   => $model->audited_by,
                'link' => $this->link('UserController@show', ['id' => $model->audited_by])
            ],
            'for_location'         => [
                'id'   => $model->for_location,
                'link' => $this->link('BranchController@show', $model->for_location)
            ],
            'supplier'             => [
                'id'   => $model->supplier_id,
                'link' => $this->link('SupplierController@show', ['id' => $model->supplier_id])
            ],
            'payment_method'       => [
                'value' => $model->payment_method,
                'label' => $model->presenter()->payment
            ],
            'term'                 => $model->term,
            'dr_no'                => $model->dr_no,
            'supplier_invoice_no'  => $model->supplier_invoice_no,
            'supplier_box_count'   => $model->supplier_box_count,
            'audited_date'         => $model->audited_date,
            'transaction_date'     => $model->transaction_date,
            'transaction_type'     => [
                'value' => $model->transaction_type,
                'label' => $model->presenter()->transaction_type
            ],
            'approval_status'      => [
                'value' => $model->approval_status,
                'label' => $model->presenter()->approval
            ],
            'total_amount'         => $model->number()->total_amount,
            'remarks'              => $model->remarks,
            'deleted_at'           => $model->deleted_at,
            'created_at'           => $model->created_at,
            'updated_at'           => $model->updated_at,
            'remaining_amount'     => $model->number()->remaining_amount,
            'transactionable_id'   => $model->transactionable_id,
            'transactionable_type' => $model->transactionable_type,
        ];
    }

    public function includeDetails(PurchaseInbound $model)
    {
        return $this->collection($model->details, new PurchaseInboundDetailApiTransformer);
    }
}