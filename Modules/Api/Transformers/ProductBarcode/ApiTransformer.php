<?php

namespace Modules\Api\Transformers\ProductBarcode;

use League\Fractal\TransformerAbstract;
use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductBarcode;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductBarcode;
 */
class ApiTransformer extends BaseApiTransformer
{   
    /**
     * Transform the ProductBarcode entity
     * @param ProductBarcode $model
     *
     * @return array
     */
    public function transform(ProductBarcode $model)
    {
        return [
            'code'          => $model->code,
            'memo'          => $model->memo,
            'uom' => [
                'id' => $model->uom_id,
                'link' => $this->link('UOMController@show' ,['id' => $model->uom_id])
            ]
        ];
    }
}