<?php

namespace Modules\Api\Transformers\Customer;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Api\Transformers\CustomerDetail\ApiTransformer as CustomerDetailApiTransformer;
use Modules\Core\Entities\Customer;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\Customer;
 */
class ApiTransformer extends BaseApiTransformer
{
    protected $defaultIncludes = [
        'details'
    ];

    /**
     * Transform the Customer entity
     * @param Customer $model
     *
     * @return array
     */
    public function transform(Customer $model)
    {
        return [
            'id'                       => $model->id,
            'created_from'             => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'               => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'              => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'name'                     => $model->name,
            'code'                     => $model->code,
            'memo'                     => $model->memo,
            'credit_limit'             => $model->number()->credit_limit,
            'pricing_type'             => $model->pricing_type,
            'historical_change_revert' => $model->historical_change_revert,
            'historical_default_price' => $model->historical_default_price,
            'pricing_rate'             => $model->number()->pricing_rate,
            'salesman'                 => [
                'id'   => $model->salesman_id,
                'link' => $this->link('UserController@show', ['id' => $model->salesman_id])
            ],
            'website'                  => $model->website,
            'term'                     => $model->term,
            'status'                   => $model->status,
            'deleted_at'               => $model->deleted_at,
            'created_at'               => $model->created_at->toDateTimeString(),
            'updated_at'               => $model->updated_at->toDateTimeString(),
        ];
    }

    public function includeDetails(Customer $model)
    {
        return $this->collection($model->details, new CustomerDetailApiTransformer);
    }
}
