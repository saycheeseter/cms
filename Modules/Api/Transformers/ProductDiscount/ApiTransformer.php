<?php

namespace Modules\Api\Transformers\ProductDiscount;

use Modules\Api\Transformers\ProductDiscountDetail\ApiTransformer as ProductDiscountDetailApiTransformer;
use Modules\Api\Transformers\ProductDiscountTarget\ApiTransformer as ProductDiscountTargetApiTransformer;
use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductDiscount;

use Carbon\Carbon;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductDiscount;
 */
class ApiTransformer extends BaseApiTransformer
{

    protected $defaultIncludes = [
        'details',
        'targets'
    ];

    /**
     * Transform the ProductDiscount entity
     * @param ProductDiscount $model
     *
     * @return array
     */
    public function transform(ProductDiscount $model)
    {
        return [
            'id'              => $model->id,
            'code'            => $model->code,
            'name'            => $model->name,
            'valid_from'      => $model->valid_from,
            'valid_to'        => $model->valid_to,
            'status'          => $model->status,
            'mon'             => $model->mon,
            'tue'             => $model->tue,
            'wed'             => $model->wed,
            'thur'            => $model->thur,
            'sat'             => $model->sat,
            'sun'             => $model->sun,
            'target_type'     => $model->target_type,
            'discount_scheme' => $model->discount_scheme,
            'select_type'     => $model->select_type,
            'created_for'     => [
                'id'   => $model->created_for,
                'link' => $this->link('ProductDiscountController@show', ['id' => $model->created_for])
            ],
            'created_from'    => [
                'id'   => $model->created_from,
                'link' => $this->link('ProductDiscountController@show', ['id' => $model->created_from])
            ],
            'created_by'      => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'     => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'      => $model->deleted_at,
            'created_at'      => $model->created_at->toDateTimeString(),
            'updated_at'      => $model->updated_at->toDateTimeString(),
        ];
    }

    public function includeDetails(ProductDiscount $model)
    {
        return $this->collection($model->details, new ProductDiscountDetailApiTransformer);
    }

    public function includeTargets(ProductDiscount $model)
    {
        return $this->collection($model->targets, new ProductDiscountTargetApiTransformer);
    }
}
