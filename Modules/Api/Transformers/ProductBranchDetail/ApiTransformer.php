<?php

namespace Modules\Api\Transformers\ProductBranchDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductBranchDetail;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductBranchDetail;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the ProductBranchDetail entity
     * @param ProductBranchDetail $model
     *
     * @return array
     */
    public function transform(ProductBranchDetail $model)
    {
        return [
            'min' => $model->number()->min,
            'max' => $model->number()->max,
            'branch' => [
                'id' => $model->branch_detail_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }
}
