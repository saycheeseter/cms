<?php

namespace Modules\Api\Transformers\Member;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\Member;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\Member;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the Member entity
     * @param Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'id'                => $model->id,
            'rate_head'         => [
                'id'   => $model->rate_head_id,
                'link' => $this->link('MemberRateController@show', ['id' => $model->rate_head_id])
            ],
            'created_from'      => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'        => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'       => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'card_id'           => $model->card_id,
            'barcode'           => $model->barcode,
            'full_name'         => $model->full_name,
            'mobile'            => $model->mobile,
            'telephone'         => $model->telephone,
            'address'           => $model->address,
            'email'             => $model->email,
            'gender'            => $model->gender,
            'birth_date'        => $model->birth_date->toDateString(),
            'registration_date' => $model->registration_date->toDateString(),
            'expiration_date'   => $model->expiration_date->toDateString(),
            'status'            => $model->status,
            'memo'              => $model->memo,
            'points'            => $model->number()->total_points,
            'deleted_at'        => $model->deleted_at,
            'created_at'        => $model->created_at->toDateTimeString(),
            'updated_at'        => $model->updated_at->toDateTimeString(),
        ];
    }
}
