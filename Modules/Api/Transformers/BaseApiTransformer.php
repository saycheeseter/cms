<?php

namespace Modules\Api\Transformers;

use League\Fractal\TransformerAbstract;

abstract class BaseApiTransformer extends TransformerAbstract
{
    protected function link($controller, $parameters = [])
    {
        return action(sprintf('Modules\Api\Http\Controllers\%s', $controller), $parameters);
    }
}