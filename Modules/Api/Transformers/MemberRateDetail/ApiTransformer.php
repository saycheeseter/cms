<?php

namespace Modules\Api\Transformers\MemberRateDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\MemberRateDetail;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\MemberRateDetail;
 */
class ApiTransformer extends BaseApiTransformer
{   
    /**
     * Transform the MemberRateDetail entity
     * @param MemberRateDetail $model
     *
     * @return array
     */
    public function transform(MemberRateDetail $model)
    {   
        return [
            'id'               => $model->id,
            'amount_from'      => $model->number()->amount_from,
            'amount_to'        => $model->number()->amount_to,
            'date_from'        => $model->date_from,
            'date_to'          => $model->date_to,
            'amount_per_point' => $model->number()->amount_per_point,
            'discount'         => $model->number()->discount,
            'deleted_at'       => $model->deleted_at,
            'created_at'       => $model->created_at->toDateTimeString(),
            'updated_at'       => $model->updated_at->toDateTimeString(),
        ];
    }
}
