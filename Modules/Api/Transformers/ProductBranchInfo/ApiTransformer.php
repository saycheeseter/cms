<?php

namespace Modules\Api\Transformers\ProductBranchInfo;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductBranchInfo;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductBranchInfo;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the ProductBranchInfo entity
     * @param ProductBranchInfo $model
     *
     * @return array
     */
    public function transform(ProductBranchInfo $model)
    {
        return [
            'status' => $model->status,
            'branch' => [
                'id' => $model->branch_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }
}
