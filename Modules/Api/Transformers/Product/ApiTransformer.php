<?php

namespace Modules\Api\Transformers\Product;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\Product;

use Modules\Api\Transformers\ProductUnit\ApiTransformer as ProductUnitApiTransformer;
use Modules\Api\Transformers\ProductBarcode\ApiTransformer as ProductBarcodeApiTransformer;
use Modules\Api\Transformers\BranchPrice\ApiTransformer as BranchPriceApiTransformer;
use Modules\Api\Transformers\ProductBranchInfo\ApiTransformer as ProductBranchInfoApiTransformer;
use Modules\Api\Transformers\ProductBranchDetail\ApiTransformer as ProductBranchDetailApiTransformer;
use Modules\Api\Transformers\ProductBranchSummary\ApiTransformer as ProductBranchSummaryApiTransformer;
use Modules\Api\Transformers\Tax\ApiTransformer as TaxApiTransformer;
use Modules\Api\Transformers\Supplier\ApiTransformer as SupplierApiTransformer;
use Modules\Api\Transformers\Category\ApiTransformer as CategoryApiTransformer;
use Modules\Api\Transformers\SystemCode\ApiTransformer as BrandApiTransformer;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\Product;
 */
class ApiTransformer extends BaseApiTransformer
{
    protected $defaultIncludes = [
        'units',
        'barcodes',
        'prices',
        'branches',
        'locations',
        'summaries',
        'taxes'
    ];

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {   
        return [
            'id'           => $model->id,
            'stock_no'     => $model->stock_no,
            'supplier_sku' => $model->supplier_sku,
            'name'         => $model->name,
            'description'  => $model->description,
            'chinese_name' => $model->chinese_name,
            'senior'       => $model->senior,
            'status'       => $model->status,
            'memo'         => $model->memo,
            'group_type'   => [
                'value' => $model->group_type,
                'label' => $model->presenter()->group_type_label
            ],
            'type'   => [
                'value' => $model->type,
                'label' => $model->presenter()->type_label
            ],
            'default_unit' => $model->default_unit,
            'created_from' => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'   => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'  => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'supplier' => $this->evaluateRelationship($model, 'supplier', SupplierApiTransformer::class, 'supplier_id', 'SupplierController@show'),
            'category' => $this->evaluateRelationship($model, 'category', SupplierApiTransformer::class, 'category_id', 'CategoryController@show'),
            'brand' => $this->evaluateRelationship($model, 'brand', BrandApiTransformer::class, 'brand_id', 'BrandController@show'),
            'deleted_at'   => $model->deleted_at,
            'created_at'   => $model->created_at->toDateTimeString(),
            'updated_at'   => $model->updated_at->toDateTimeString(),
        ];
    }

    private function evaluateRelationship($model, $relationship, $transformer, $idKey, $link)
    {
        if($model->relationLoaded($relationship)) {
            return $this->item($model->$relationship, new $transformer)->getData();
        } else {
            return [
                'id' => $model->$idKey,
                'link' => $this->link($link, ['id' => $model->$idKey])
            ];
        }
    }

    public function includeUnits(Product $model)
    {
        return $this->collection($model->units, new ProductUnitApiTransformer);
    }

    public function includeBarcodes(Product $model)
    {
        return $this->collection($model->barcodes, new ProductBarcodeApiTransformer);
    }

    public function includePrices(Product $model)
    {
        return $this->collection($model->prices, new BranchPriceApiTransformer);
    }

    public function includeBranches(Product $model)
    {
        return $this->collection($model->branches, new ProductBranchInfoApiTransformer);
    }

    public function includeLocations(Product $model)
    {
        return $this->collection($model->locations, new ProductBranchDetailApiTransformer);
    }

    public function includeSummaries(Product $model)
    {
        return $this->collection($model->summaries, new ProductBranchSummaryApiTransformer);
    }

    public function includeTaxes(Product $model)
    {
        return $this->collection($model->taxes, new TaxApiTransformer);
    }
}
