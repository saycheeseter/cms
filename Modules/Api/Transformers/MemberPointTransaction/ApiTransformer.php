<?php

namespace Modules\Api\Transformers\MemberPointTransaction;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\MemberPointTransaction;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\MemberPointTransaction;
 */
class ApiTransformer extends BaseApiTransformer
{
    /**
     * Transform the MemberPointTransaction entity
     * @param MemberPointTransaction $model
     *
     * @return array
     */
    public function transform(MemberPointTransaction $model)
    {
        return [
            'reference_type' => $model->reference_type,
            'reference_number' => $model->reference_number,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'amount' => $model->number()->amount,
            'created_by'     => [
                'id' => $model->created_by,
                'link' => $this->link('UserController@show' ,['id' => $model->created_by])
            ],
            'created_from'     => [
                'id' => $model->created_from,
                'link' => $this->link('BranchController@show' ,['id' => $model->created_from])
            ],
            'member'     => [
                'id' => $model->member_id,
                'link' => $this->link('MemberController@show' ,['id' => $model->member_id])
            ]
        ];
    }
}
