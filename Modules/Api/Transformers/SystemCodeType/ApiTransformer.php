<?php

namespace Modules\Api\Transformers\SystemCodeType;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\SystemCodeType;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\SystemCodeType;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the SystemCodeType entity
     * @param SystemCodeType $model
     *
     * @return array
     */
    public function transform(SystemCodeType $model)
    {
        return [
            'id'   => $model->id,
            'name' => $model->name
        ];
    }
}
