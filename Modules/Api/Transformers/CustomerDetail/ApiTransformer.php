<?php

namespace Modules\Api\Transformers\CustomerDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\CustomerDetail;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\CustomerDetail;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the CustomerDetail entity
     * @param CustomerDetail $model
     *
     * @return array
     */
    public function transform(CustomerDetail $model)
    {
        return [
            'id'             => $model->id,
            'name'           => $model->name,
            'address'        => $model->address,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'contact_person' => $model->contact_person,
            'tin'            => $model->tin,
            'deleted_at'     => $model->deleted_at,
            'created_at'     => !is_null($model->created_at) ? $model->created_at->toDateTimeString() : '',
            'updated_at'     => !is_null($model->updated_at) ? $model->updated_at->toDateTimeString() : '',
        ];
    }
}
