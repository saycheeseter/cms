<?php

namespace Modules\Api\Transformers\Branch;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Api\Transformers\BranchDetail\ApiTransformer as BranchDetailApiTransformer;
use Modules\Core\Entities\Branch;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\Branch;
 */
class ApiTransformer extends BaseApiTransformer
{

    protected $defaultIncludes = [
        'details'
    ];

    /**
     * Transform the Branch entity
     * @param Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address,
            'contact'       => $model->contact,
            'business_name' => $model->business_name,
            'created_by'    => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'   => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'    => $model->deleted_at,
            'created_at'    => $model->created_at->toDateTimeString(),
            'updated_at'    => $model->updated_at->toDateTimeString(),
        ];
    }

    public function includeDetails(Branch $model)
    {
        return $this->collection($model->details, new BranchDetailApiTransformer);
    }
}
