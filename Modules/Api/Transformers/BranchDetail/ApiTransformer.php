<?php

namespace Modules\Api\Transformers\BranchDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\BranchDetail;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\BranchDetail;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the BranchDetail entity
     * @param BranchDetail $model
     *
     * @return array
     */
    public function transform(BranchDetail $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address,
            'contact'       => $model->contact,
            'business_name' => $model->business_name,
            'created_by'    => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'   => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'    => $model->deleted_at,
            'created_at'    => $model->created_at->toDateTimeString(),
            'updated_at'    => $model->updated_at->toDateTimeString(),
        ];
    }
}
