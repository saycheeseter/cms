<?php

namespace Modules\Api\Transformers\ProductDiscountDetail;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductDiscountDetail;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductDiscountDetail;
 */
class ApiTransformer extends BaseApiTransformer
{
    /**
     * Transform the ProductDiscountDetail entity
     * @param ProductDiscountDetail $model
     *
     * @return array
     */
    public function transform(ProductDiscountDetail $model)
    {
        return [
            'product_discount_id'    => $model->product_discount_id,
            'entity_type'    => $model->entity_type,
            'entity_id'    => $model->entity_id,
            'discount_type'    => $model->discount_type,
            'discount_value'    => $model->number()->discount_value,
            'qty'    => $model->number()->qty,
        ];
    }
}
