<?php

namespace Modules\Api\Transformers\SystemCode;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\SystemCode;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\SystemCode;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the SystemCode entity
     * @param SystemCode $model
     *
     * @return array
     */
    public function transform(SystemCode $model)
    {
        return [
            'id'         => $model->id,
            'type'       => [
                'id'   => $model->type_id,
                'link' => $this->link('SystemCodeTypeController@show', ['id' => $model->type_id])
            ],
            'code'       => $model->code,
            'name'       => $model->name,
            'remarks'    => $model->remarks,
            'deleted_at' => $model->deleted_at,
            'created_at' => $model->created_at->toDateTimeString(),
            'updated_at' => $model->updated_at->toDateTimeString(),
        ];
    }
}
