<?php

namespace Modules\Api\Transformers\MemberRate;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\MemberRate;
use Modules\Api\Transformers\MemberRateDetail\ApiTransformer as MemberRateDetailApiTransformer;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\MemberRate;
 */
class ApiTransformer extends BaseApiTransformer
{   
    protected $defaultIncludes = [
        'details'
    ];

    /**
     * Transform the MemberRate entity
     * @param MemberRate $model
     *
     * @return array
     */
    public function transform(MemberRate $model)
    {   
        return [
            'id'           => $model->id,
            'name'         => $model->name,
            'memo'         => $model->memo,
            'status'       => $model->status,
            'created_from' => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'   => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'  => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'   => $model->deleted_at,
            'created_at'   => $model->created_at->toDateTimeString(),
            'updated_at'   => $model->updated_at->toDateTimeString(),
        ];
    }

    public function includeDetails(MemberRate $model)
    {
        return $this->collection($model->details, new MemberRateDetailApiTransformer);
    }
}
