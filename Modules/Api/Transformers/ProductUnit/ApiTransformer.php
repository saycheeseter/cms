<?php

namespace Modules\Api\Transformers\ProductUnit;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\ProductUnit;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\ProductUnit
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the ProductUnitentity
     * @param ProductUnit$model
     *
     * @return array
     */
    public function transform(ProductUnit $model)
    {
        return [
            'qty'    => $model->number()->qty,
            'length'    => $model->number()->length,
            'width'    => $model->number()->width,
            'height'    => $model->number()->height,
            'weight'    => $model->number()->weight,
            'default'    => $model->default,
            'uom' => [
                'id' => $model->uom_id,
                'link' => $this->link('UOMController@show' ,['id' => $model->uom_id])
            ]
        ];
    }
}
