<?php

namespace Modules\Api\Transformers\Supplier;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\Supplier;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\Supplier;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the Supplier entity
     * @param Supplier $model
     *
     * @return array
     */
    public function transform(Supplier $model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'telephone'      => $model->telephone,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'term'           => $model->term,
            'status'         => $model->status,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'created_from'   => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'     => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'    => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'     => $model->deleted_at,
            'created_at'     => $model->created_at->toDateTimeString(),
            'updated_at'     => $model->updated_at->toDateTimeString(),
        ];
    }
}
