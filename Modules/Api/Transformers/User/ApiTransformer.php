<?php

namespace Modules\Api\Transformers\User;

use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\User;

/**
 * Class BasicTransformer
 * @package namespace Modules\Api\Transformers\User;
 */
class ApiTransformer extends BaseApiTransformer
{

    /**
     * Transform the User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'username'     => $model->username,
            'password'     => $model->password,
            'full_name'    => $model->full_name,
            'position'     => $model->position,
            'address'      => $model->address,
            'telephone'    => $model->telephone,
            'mobile'       => $model->mobile,
            'email'        => $model->email,
            'memo'         => $model->memo,
            'type'         => $model->type,
            'status'       => $model->status,
            'created_from' => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by'   => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'  => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at'   => $model->deleted_at,
            'created_at'   => $model->created_at->toDateTimeString(),
            'updated_at'   => $model->updated_at->toDateTimeString(),
            'permissions'  => $this->getPivot($model->permissions),
        ];
    }

    private function getPivot($data)
    {
        $returnArray = [];
        
        foreach ($data as $key => $value) {
            $returnArray[] = array_merge($value->pivot->toArray(), ['alias' => $value->alias]);
        }

        return $returnArray;
    }
}
