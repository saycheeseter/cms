<?php

namespace Modules\Api\Transformers\Category;

use League\Fractal\TransformerAbstract;
use Modules\Api\Transformers\BaseApiTransformer;
use Modules\Core\Entities\Category;

/**
 * Class ApiTransformer
 * @package namespace Modules\Api\Transformers\Category;
 */
class ApiTransformer extends BaseApiTransformer
{   
    /**
     * Transform the Category entity
     * @param Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        return [
            'id'         => $model->id,
            'code'       => $model->code,
            'name'       => $model->name,
            'parent'     => [
                'id'   => (int)$model->parent_id,
                'link' => $this->link('CategoryController@show', ['id' => $model->parent_id])
            ],
            'creator'    => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modifier'   => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'deleted_at' => $model->deleted_at,
            'created_at' => $model->created_at->toDateTimeString(),
            'updated_at' => $model->updated_at->toDateTimeString(),
        ];
    }
}