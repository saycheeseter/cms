<?php

Route::group([
    'middleware' => 'client',
    'prefix' => 'api/v1',
    'namespace' => 'Modules\Api\Http\Controllers'
], function()
{
    Route::get('brands', 'BrandController@index');
    Route::get('brands/{id}', 'BrandController@show');

    Route::get('payment-methods', 'PaymentMethodController@index');
    Route::get('payment-methods/{id}', 'PaymentMethodController@show');

    Route::get('uoms', 'UOMController@index');
    Route::get('uoms/{id}', 'UOMController@show');

    Route::get('categories', 'CategoryController@index');
    Route::get('categories/{id}', 'CategoryController@show');

    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@show');

    Route::get('branches', 'BranchController@index');
    Route::get('branches/{id}', 'BranchController@show');

    Route::get('customers', 'CustomerController@index');
    Route::get('customers{id}', 'CustomerController@show');

    Route::get('members', 'MemberController@index');
    Route::get('members/{id}', 'MemberController@show');
    Route::get('members/{id}/points', 'MemberController@points');

    Route::get('member-rate', 'MemberRateController@index');
    Route::get('member-rate/{id}', 'MemberRateController@show');

    Route::get('products', 'ProductController@index');
    Route::get('products/{id}', 'ProductController@show');

    Route::get('suppliers', 'SupplierController@index');
    Route::get('suppliers/{id}', 'SupplierController@show');

    Route::post('pos-sales/sync', 'PosSalesController@sync');

    Route::get('product-discounts', 'ProductDiscountController@index');
    Route::get('product-discounts/{id}', 'ProductDiscountController@show');

    Route::post('xyz-readings/sync', 'XyzReadingController@sync');

    Route::post('cash-denominations/sync', 'CashDenominationController@sync');

    Route::get('system-code-types/{id}', 'SystemCodeTypeController@show');
    Route::get('system-code-types', 'SystemCodeTypeController@index');

    Route::get('gift-checks/{check}/valid', 'GiftCheckController@valid');

    Route::get('purchase-inbounds/sheet-number/{sheetNumber}', 'PurchaseInboundController@showBySheetNumber');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'api/v1',
    'namespace' => 'Modules\Api\Http\Controllers'
], function()
{
    Route::patch('gift-checks/{check}/redeem', 'GiftCheckController@redeem');
    Route::post('member/{id}/earn', 'MemberController@earnPoints');
    Route::post('member/{id}/use', 'MemberController@usePoints');
    Route::post('member/{id}/load', 'MemberController@loadPoints');
    Route::post('data-collector', 'DataCollectorController@store');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'oauth/user',
    'namespace' => 'Modules\Api\Http\Controllers'
], function()
{
    Route::get('', 'OauthController@user');
    Route::get('branches', 'OauthController@branches');
});