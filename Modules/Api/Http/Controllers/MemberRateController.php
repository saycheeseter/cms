<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Modules\Api\Transformers\MemberRate\ApiTransformer;

class MemberRateController extends DataController
{
    protected $transformer = ApiTransformer::class;
    protected $relationships = ['details'];

    public function __construct(MemberRateRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
