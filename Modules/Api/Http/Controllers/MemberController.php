<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Requests\MemberPointsRequest;
use Modules\Core\Repositories\Contracts\MemberPointTransactionRepository;
use Modules\Core\Repositories\Contracts\MemberRepository;
use Modules\Api\Transformers\Member\ApiTransformer;
use Modules\Core\Services\Contracts\MemberPointTransactionServiceInterface;
use Lang;
use App;

class MemberController extends DataController
{
    protected $transformer = ApiTransformer::class;

    public function __construct(MemberRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    /**
     * Retrieve the points of the specified member
     *
     * @param MemberPointTransactionRepository $repository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function points(MemberPointTransactionRepository $repository, $id)
    {
        $points = $repository->points($id);

        return $this->successfulResponse([
            'points' => $points
        ]);
    }

    public function earnPoints(MemberPointsRequest $request, $id)
    {
        $member = $this->repository->find($id);

        if (!$member) {
            return $this->errorResponse([
                'member.doesnt.exists' => Lang::get('core::validation.member.doesnt.exists')
            ]);
        }

        $data = App::make(MemberPointTransactionServiceInterface::class)->earn(
            $id,
            $request->get('amount'),
            $request->get('reference_number'),
            $request->get('remarks')
        );

        if (null === $data) {
            return $this->errorResponse([
                'no.points.earned' => Lang::get('core::label.no.points.earned')
            ]);
        }

        return $this->successfulResponse([
            'points' => $data
        ]);
    }

    public function usePoints(MemberPointsRequest $request, $id)
    {
        $member = $this->repository->find($id);

        if (!$member) {
            return $this->errorResponse([
                'member.doesnt.exists' => Lang::get('core::validation.member.doesnt.exists')
            ]);
        }

        $data = App::make(MemberPointTransactionServiceInterface::class)->use(
            $id,
            $request->get('amount'),
            $request->get('reference_number'),
            $request->get('remarks')
        );

        return $this->successfulResponse([
            'points' => $data
        ]);
    }

    public function loadPoints(MemberPointsRequest $request, $id)
    {
        $member = $this->repository->find($id);

        if (!$member) {
            return $this->errorResponse([
                'member.doesnt.exists' => Lang::get('core::validation.member.doesnt.exists')
            ]);
        }

        $data = App::make(MemberPointTransactionServiceInterface::class)->load(
            $id,
            $request->get('amount'),
            $request->get('reference_number'),
            $request->get('remarks')
        );

        return $this->successfulResponse([
            'points' => $data
        ]);
    }
}