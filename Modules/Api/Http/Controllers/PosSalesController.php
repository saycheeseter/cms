<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Requests\SyncPosSalesRequest;
use Modules\Core\Services\Contracts\PosSalesServiceInterface;
use Modules\Core\Rules\Contracts\PosSalesRuleInterface as PosSalesRule;
use Lang;
use App;

class PosSalesController extends ApiController
{
    private $service;

    public function __construct(PosSalesServiceInterface $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function sync(SyncPosSalesRequest $request)
    {
        $rule = $this->rule('sync', PosSalesRule::class, $request->all());

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $result = $this->service->sync($request->all());

        if (!$result) {
            return $this->errorResponse(array(
                'sync.failed' => Lang::get('core::error.sync.failed')
            ));
        }

        return $this->successfulResponse([
            'count' => $result->count()
        ], Lang::get('core::success.synced'));
    }
}