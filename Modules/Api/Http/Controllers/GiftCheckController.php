<?php

namespace Modules\Api\Http\Controllers;

use Exception;
use Modules\Core\Repositories\Contracts\GiftCheckRepository;
use Modules\Api\Transformers\GiftCheck\ApiTransformer;
use Modules\Core\Services\Contracts\GiftCheckServiceInterface;
use Modules\Core\Http\Requests\GiftCheckRedeemRequest;
use Lang;

class GiftCheckController extends ApiController
{   
    private $repository;
    private $transformer;
    private $service;

    public function __construct(GiftCheckRepository $repository, GiftCheckServiceInterface $service)
    {
        parent::__construct();

        $this->repository = $repository;
        $this->transformer = ApiTransformer::class;
        $this->service = $service;
    }

    /**
     * Check if the given check number is still valid
     *
     * @param $check
     * @return \Illuminate\Http\JsonResponse
     */
    public function valid($check)
    {
        $data = $this->repository->findValidCheckByNo($check);

        if(!$data) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.gift.check.not.found')
            ));
        }

        return $this->successfulResponse([
            $this->item($data, $this->transformer)
        ]);
    }

    /**
     * Redeem the given check number and check if the given check number is still valid
     *
     * @param GiftCheckRedeemRequest $request
     * @param $check
     * @return \Illuminate\Http\JsonResponse
     */
    public function redeem(GiftCheckRedeemRequest $request, $check)
    {
        $data = $this->repository->findValidCheckByNo($check);

        if(is_null($data)) {
            return $this->errorResponse(array(
                'gift.check.not.valid' => Lang::get('core::error.gift.check.not.valid')
            ));
        }

        try {
            $this->service->redeem($data->id, $request->get('reference_number'));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'redeem.failed' => Lang::get('core::error.gift.check.not.found')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.gift.check.successfully.redeemed'));
    }
}
