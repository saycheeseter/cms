<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Api\Transformers\Product\ApiTransformer;
use Lang;

class ProductController extends DataController
{   
    protected $transformer = ApiTransformer::class;

    public function __construct(ProductRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->repository->filter();

        $message = $data->count() === 0
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse(
            $this->collection($data, ApiTransformer::class),
            $message
        );
    }
}