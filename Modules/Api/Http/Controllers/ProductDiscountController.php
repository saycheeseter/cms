<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\ProductDiscountRepository;
use Modules\Api\Transformers\ProductDiscount\ApiTransformer;
use Lang;

class ProductDiscountController extends DataController
{   
    protected $transformer = ApiTransformer::class;
    protected $relationships =[
        'details',
        'targets'
    ];

    public function __construct(ProductDiscountRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
