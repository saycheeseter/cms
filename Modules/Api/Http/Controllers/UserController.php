<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Api\Transformers\User\ApiTransformer;
use App;
use Modules\Core\Repositories\Criteria\SuperadminExcludedCriteria;

class UserController extends DataController
{
    protected $transformer = ApiTransformer::class;
    protected $relationships = ['permissions'];

    public function __construct(UserRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
