<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Transformers\SystemCode\ApiTransformer;

abstract class SystemCodeController extends DataController
{
    protected $transformer = ApiTransformer::class;
}