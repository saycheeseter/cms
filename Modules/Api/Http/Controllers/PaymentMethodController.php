<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\PaymentMethodRepository;

class PaymentMethodController extends SystemCodeController
{
    public function __construct(PaymentMethodRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}