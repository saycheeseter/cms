<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Requests\SyncCashDenominationRequest;
use Modules\Core\Services\Contracts\CashDenominationServiceInterface;
use Lang;

class CashDenominationController extends ApiController
{   
    protected $service;

    public function __construct(CashDenominationServiceInterface $service)
    {   
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SyncCashDenominationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sync(SyncCashDenominationRequest $request)
    {
        $result = $this->service->bulkStore($request->all());

        if (!$result) {
            return $this->errorResponse(array(
                'sync.failed' => Lang::get('core::error.sync.failed')
            ));
        }

        return $this->successfulResponse([
            'count' => $result->count()
        ], Lang::get('core::success.synced'));
    }
}
