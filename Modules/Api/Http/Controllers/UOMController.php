<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\UOMRepository;

class UOMController extends SystemCodeController
{
    public function __construct(UOMRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
