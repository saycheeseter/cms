<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Transformers\Branch\ApiTransformer;
use App;

class OauthController extends ApiController
{
    public function user()
    {
        return request()->user();
    }

    public function branches()
    {
        $branches = request()->user()->assignedBranches();

        return $this->successfulResponse($this->collection($branches, ApiTransformer::class));
    }
}