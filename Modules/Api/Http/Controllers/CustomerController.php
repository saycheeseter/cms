<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Api\Transformers\Customer\ApiTransformer;

class CustomerController extends DataController
{
    protected $transformer = ApiTransformer::class;
    protected $relationships = ['details'];

    public function __construct(CustomerRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}