<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Core\Traits\ResponsesJson;
use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Traits\PaginatesCollection;
use Modules\Core\Traits\GeneratesRule;

abstract class ApiController extends Controller
{
    use ResponsesJson,
        FractalTransformer,
        PaginatesCollection,
        GeneratesRule;

    public function __construct()
    {
        ini_set('max_execution_time', 0);
    }
}
