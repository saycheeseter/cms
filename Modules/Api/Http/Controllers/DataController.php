<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lang;

abstract class DataController extends ApiController
{
    protected $relationships = null;
    protected $repository;
    protected $transformer;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $perPage = $request->get('per_page', 10);

        if(!is_null($this->relationships)) {
            $data = $this->repository
                ->with($this->relationships)
                ->paginate($perPage);
        } else {
            $data = $this->repository
                ->paginate($perPage);
        }

        $message = $data->count() === 0 
            ? Lang::get('core::info.no.results.found') 
            : '';

        return $this->successfulResponse(
            $this->collection($data, $this->transformer),
            $message
        );
    }

    public function show($id)
    {
        if(!is_null($this->relationships)) {
            $data = $this->repository->with($this->relationships)->find($id);
        } else {
            $data = $this->repository->find($id);
        }

        if(!$data) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse([
            $this->item($data, $this->transformer)
        ]);
    }
}
