<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Transformers\PurchaseInbound\ApiTransformer as PurchaseInboundApiTransformer;
use Modules\Core\Repositories\Contracts\PurchaseInboundRepository;
use Lang;

class PurchaseInboundController extends DataController
{
    protected $transformer = PurchaseInboundApiTransformer::class;

    public function __construct(PurchaseInboundRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    public function showBySheetNumber($sheetNumber)
    {
        $data = $this->repository->with(['details'])->findBySheetNumber($sheetNumber);

        if(!$data) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse($this->item($data, PurchaseInboundApiTransformer::class));
    }

}