<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\SystemCodeTypeRepository;
use Modules\Api\Transformers\SystemCodeType\ApiTransformer;

class SystemCodeTypeController extends DataController
{
    protected $transformer = ApiTransformer::class;

    public function __construct(SystemCodeTypeRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}