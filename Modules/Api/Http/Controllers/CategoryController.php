<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Api\Transformers\Category\ApiTransformer;

class CategoryController extends DataController
{   
    protected $transformer = ApiTransformer::class;

    public function __construct(CategoryRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
