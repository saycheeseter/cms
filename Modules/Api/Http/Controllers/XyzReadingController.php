<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Requests\SyncXyzReadingRequest;
use Modules\Core\Services\Contracts\XyzReadingServiceInterface;
use Lang;
use App;

class XyzReadingController extends ApiController
{
    protected $service;

    public function __construct(XyzReadingServiceInterface $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function sync(SyncXyzReadingRequest $request)
    {
        $result = $this->service->bulkUpdateOrCreate([
            'terminal_number',
            'read_type',
            'created_from',
            'read_count'
        ], $request->all());

        if (!$result) {
            return $this->errorResponse(array(
                'sync.failed' => Lang::get('core::error.sync.failed')
            ));
        }

        return $this->successfulResponse([
            'count' => $result->count()
        ], Lang::get('core::success.synced'));
    }
}