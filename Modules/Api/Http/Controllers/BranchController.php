<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Api\Transformers\Branch\ApiTransformer;

class BranchController extends DataController
{   
    protected $transformer = ApiTransformer::class;
    protected $relationships = ['details'];

    public function __construct(BranchRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
