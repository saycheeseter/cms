<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Api\Transformers\Supplier\ApiTransformer;

class SupplierController extends DataController
{   
    protected $transformer = ApiTransformer::class;

    public function __construct(SupplierRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}