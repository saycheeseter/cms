<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Requests\DataCollectorRequest;
use Modules\Core\Services\Contracts\DataCollectorServiceInterface as DataCollectorService;
use Modules\Core\Transformers\DataCollectorTransaction\BasicTransformer as DataCollectorTransformer;
use Lang;
use App;
use Exception;

class DataCollectorController extends ApiController
{
    protected $service;

    public function __construct(DataCollectorService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function store(DataCollectorRequest $request)
    {
        try{
            $result = App::make(DataCollectorService::class)->store($this->data($request));
        } catch(Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse(
            $this->item($result, DataCollectorTransformer::class),
            Lang::get('core::success.created')
        );
    }

    private function data(DataCollectorRequest $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }
}