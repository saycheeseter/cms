<?php

namespace Modules\Api\Http\Controllers;

use Modules\Core\Repositories\Contracts\BrandRepository;

class BrandController extends SystemCodeController
{
    public function __construct(BrandRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
}
