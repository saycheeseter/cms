<?php

namespace Modules\Api\Http\Requests;

use Lang;
use Modules\Core\Http\Requests\FormRequest;

class SyncCashDenominationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            '*.created_from' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            '*.created_by' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            '*.terminal_number' => [
                'required'
            ],
            '*.create_date' => [
                'required',
                'date'
            ],
            '*.b1000' => [
                'numeric'
            ],
            '*.b500' => [
                'numeric'
            ],
            '*.b200' => [
                'numeric'
            ],
            '*.b100' => [
                'numeric'
            ],
            '*.b50' => [
                'numeric'
            ],
            '*.b20' => [
                'numeric'
            ],
            '*.c10' => [
                'numeric'
            ],
            '*.c5' => [
                'numeric'
            ],
            '*.c1' => [
                'numeric'
            ],
            '*.c25c' => [
                'numeric'
            ],
            '*.c10c' => [
                'numeric'
            ],
            '*.c5c' => [
                'numeric'
            ],
            '*.type'  => [
                'required'
            ],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return $this->createArrayMessages('*', [
            'created_from.required'    => Lang::get('core::validation.created.from.required'),
            'created_by.required'      => Lang::get('core::validation.created.by.required'),
            'created_from.exists_in'   => Lang::get('core::validation.branch.doesnt.exists'),
            'created_by.exists_in'     => Lang::get('core::validation.user.doesnt.exists'),
            'terminal_number.required' => Lang::get('core::validation.terminal.number.required'),
            'create_date.required'     => Lang::get('core::validation.date.required'),
            'create_date.date'         => Lang::get('core::validation.date.format'),
            'b1000.numeric'            => Lang::get('core::validation.denomination.numeric', ['number' => '1000']),
            'b500.numeric'             => Lang::get('core::validation.denomination.numeric', ['number' => '500']),
            'b200.numeric'             => Lang::get('core::validation.denomination.numeric', ['number' => '200']),
            'b100.numeric'             => Lang::get('core::validation.denomination.numeric', ['number' => '100']),
            'b50.numeric'              => Lang::get('core::validation.denomination.numeric', ['number' => '50']),
            'b20.numeric'              => Lang::get('core::validation.denomination.numeric', ['number' => '20']),
            'c10.numeric'              => Lang::get('core::validation.denomination.numeric', ['number' => '10']),
            'c5.numeric'               => Lang::get('core::validation.denomination.numeric', ['number' => '5']),
            'c1.numeric'               => Lang::get('core::validation.denomination.numeric', ['number' => '1']),
            'c25c.numeric'             => Lang::get('core::validation.denomination.numeric', ['number' => '25c']),
            'c10c.numeric'             => Lang::get('core::validation.denomination.numeric', ['number' => '10c']),
            'c5c.numeric'              => Lang::get('core::validation.denomination.numeric', ['number' => '5c']),
            'type.required'            => Lang::get('core::validation.denomination_type.required')
        ]);
    }
}