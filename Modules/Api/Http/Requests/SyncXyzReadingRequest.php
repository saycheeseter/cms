<?php

namespace Modules\Api\Http\Requests;

use Modules\Core\Http\Requests\FormRequest;
use Lang;

class SyncXyzReadingRequest extends FormRequest
{
    public function rules()
    {
        return [
            '*.created_from' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            '*.read_count' => [
                'required',
                'numeric'
            ],
            '*.read_type' => [
                'required',
                'numeric'
            ],
            '*.start_or' => [
                'required'
            ],
            '*.end_or' => [
                'required'
            ],
            '*.date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            '*.terminal_number' => [
                'required',
                'numeric'
            ],
            '*.branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            '*.old_grand_total' => [
                'required',
                'numeric'
            ],
            '*.new_grand_total' => [
                'required',
                'numeric'
            ],
            '*.total_discount_amount' => [
                'required',
                'numeric'
            ],
            '*.vat_returns_amount' => [
                'required',
                'numeric'
            ],
            '*.nonvat_returns_amount' => [
                'required',
                'numeric'
            ],
            '*.senior_returns_amount' => [
                'required',
                'numeric'
            ],
            '*.cash_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.credit_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.debit_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.bank_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.gift_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.senior_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.vatable_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.nonvat_sales_amount' => [
                'required',
                'numeric'
            ],
            '*.total_qty_sold' => [
                'required',
                'numeric'
            ],
            '*.total_qty_void' => [
                'required',
                'numeric'
            ],
            '*.void_transaction_amount' => [
                'required',
                'numeric'
            ],
            '*.success_transaction_count' => [
                'required',
                'numeric'
            ],
            '*.return_qty_count' => [
                'required',
                'numeric'
            ],
            '*.member_points_amount' => [
                'required',
                'numeric'
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return $this->createArrayMessages('*', [
            'created_from.required'              => Lang::get('core::validation.created.from.required'),
            'created_from.exists_in'             => Lang::get('core::validation.created.from.doesnt.exists'),
            'read_count.required'                => Lang::get('core::validation.read.count.required'),
            'read_count.numeric'                 => Lang::get('core::validation.read.count.numeric'),
            'read_type.required'                 => Lang::get('core::validation.read.type.required'),
            'read_type.numeric'                  => Lang::get('core::validation.read.type.numeric'),
            'start_or.required'                  => Lang::get('core::validation.start.or.required'),
            'end_or.required'                    => Lang::get('core::validation.end.or.required'),
            'date.required'                      => Lang::get('core::validation.date.required'),
            'date.date_format'                   => Lang::get('core::validation.date.format'),
            'terminal_number.required'           => Lang::get('core::validation.terminal.number.required'),
            'terminal_number.numeric'            => Lang::get('core::validation.terminal.number.numeric'),
            'branch_id.required'                 => Lang::get('core::validation.branch.required'),
            'branch_id.exists_in'                => Lang::get('core::validation.branch.doesnt.exists'),
            'old_grand_total.required'           => Lang::get('core::validation.old.grand.total.required'),
            'old_grand_total.numeric'            => Lang::get('core::validation.old.grand.total.numeric'),
            'new_grand_total.required'           => Lang::get('core::validation.new.grand.total.required'),
            'new_grand_total.numeric'            => Lang::get('core::validation.new.grand.total.numeric'),
            'total_discount_amount.required'     => Lang::get('core::validation.total.discount.amount.required'),
            'total_discount_amount.numeric'      => Lang::get('core::validation.total.discount.amount.numeric'),
            'vat_returns_amount.required'        => Lang::get('core::validation.vat.returns.amount.required'),
            'vat_returns_amount.numeric'         => Lang::get('core::validation.vat.returns.amount.numeric'),
            'nonvat_returns_amount.required'     => Lang::get('core::validation.nonvat.returns.amount.required'),
            'nonvat_returns_amount.numeric'      => Lang::get('core::validation.nonvat.returns.amount.numeric'),
            'senior_returns_amount.required'     => Lang::get('core::validation.senior.returns.amount.required'),
            'senior_returns_amount.numeric'      => Lang::get('core::validation.senior.returns.amount.numeric'),
            'cash_sales_amount.required'         => Lang::get('core::validation.cash.sales.amount.required'),
            'cash_sales_amount.numeric'          => Lang::get('core::validation.cash.sales.amount.numeric'),
            'credit_sales_amount.required'       => Lang::get('core::validation.credit.sales.amount.required'),
            'credit_sales_amount.numeric'        => Lang::get('core::validation.credit.sales.amount.numeric'),
            'debit_sales_amount.required'        => Lang::get('core::validation.debit.sales.amount.required'),
            'debit_sales_amount.numeric'         => Lang::get('core::validation.debit.sales.amount.numeric'),
            'bank_sales_amount.required'         => Lang::get('core::validation.bank.sales.amount.required'),
            'bank_sales_amount.numeric'          => Lang::get('core::validation.bank.sales.amount.numeric'),
            'gift_sales_amount.required'         => Lang::get('core::validation.gift.sales.amount.required'),
            'gift_sales_amount.numeric'          => Lang::get('core::validation.gift.sales.amount.numeric'),
            'senior_sales_amount.required'       => Lang::get('core::validation.senior.sales.amount.required'),
            'senior_sales_amount.numeric'        => Lang::get('core::validation.senior.sales.amount.numeric'),
            'vatable_sales_amount.required'      => Lang::get('core::validation.vatable.sales.amount.required'),
            'vatable_sales_amount.numeric'       => Lang::get('core::validation.vatable.sales.amount.numeric'),
            'nonvat_sales_amount.required'       => Lang::get('core::validation.nonvat.sales.amount.required'),
            'nonvat_sales_amount.numeric'        => Lang::get('core::validation.nonvat.sales.amount.numeric'),
            'total_qty_sold.required'            => Lang::get('core::validation.total.qty.sold.required'),
            'total_qty_sold.numeric'             => Lang::get('core::validation.total.qty.sold.numeric'),
            'total_qty_void.required'            => Lang::get('core::validation.total.qty.void.required'),
            'total_qty_void.numeric'             => Lang::get('core::validation.total.qty.void.numeric'),
            'void_transaction_amount.required'   => Lang::get('core::validation.void.transaction.amount.required'),
            'void_transaction_amount.numeric'    => Lang::get('core::validation.void.transaction.amount.numeric'),
            'success_transaction_count.required' => Lang::get('core::validation.success.transaction.count.required'),
            'success_transaction_count.numeric'  => Lang::get('core::validation.success.transaction.count.numeric'),
            'return_qty_count.required'          => Lang::get('core::validation.return.qty.count.required'),
            'return_qty_count.numeric'           => Lang::get('core::validation.return.qty.count.numeric'),
            'member_points_amount.required'      => Lang::get('core::validation.member.points.amount.required'),
            'member_points_amount.numeric'       => Lang::get('core::validation.member.points.amount.numeric')
        ]);
    }
}