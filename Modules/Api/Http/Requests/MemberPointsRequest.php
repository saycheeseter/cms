<?php

namespace Modules\Api\Http\Requests;

use Modules\Core\Http\Requests\FormRequest;
use Lang;

class MemberPointsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'amount' => [
                'required',
                'numeric',
            ],
            'reference_number' => [
                'required'
            ]
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'amount.required'           => Lang::get('core::validation.amount.required'),
            'amount.numeric'            => Lang::get('core::validation.amount.numeric'),
            'reference_number.required' => Lang::get('core::validation.reference.number.required')
        ];
    }
}