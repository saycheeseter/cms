<?php

namespace Modules\Api\Http\Requests;

use Modules\Core\Http\Requests\FormRequest;
use Lang;

class DataCollectorRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->infoRules(),
            $this->detailRules(),
            $this->arrayRules()
        );
    }

    private function infoRules()
    {
        return [
            'info.branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.filesize' => [
                'required',
                'numeric'
            ],
            'info.client_id' => [
                'required',
                'numeric'
            ],
            'info.transaction_type' => [
                'required',
                'numeric'
            ],
            'info.source' => [
                'required',
                'numeric'
            ],
            'info.row_count' => [
                'required',
                'numeric'
            ],
            'info.import_date' => [
                'required',
                'date'
            ],
            'info.imported_by' => [
                'required',
                'numeric'
            ],
        ];
    }

    private function detailRules()
    {
        return [
            'details.*.product_id' => [
                'required',
                'numeric'
            ],
            'details.*.qty' => [
                'required',
                'numeric'
            ],
            'details.*.price' => [
                'required',
                'numeric'
            ],
            'details.*.unit_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'details.*.unit_qty' => [
                'required',
                'numeric'
            ],
        ];
    }

    private function arrayRules()
    {
        return [
            'info' => [
                'required',
                'array',
                'min:1'
            ],
            'details' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return array_merge(
            $this->infoMessages(),
            $this->detailMessages(),
            $this->arrayMessages()
        );
    }

    private function infoMessages()
    {
        return [
            'info.branch_id.required'        => Lang::get('core::validation.branch.id.required'),
            'info.branch_id.exists_in'       => Lang::get('core::validation.branch.id.doesnt.exists'),
            'info.filesize.required'         => Lang::get('core::validation.filesize.required'),
            'info.filesize.numeric'          => Lang::get('core::validation.filesize.numeric'),
            'info.client_id.required'        => Lang::get('core::validation.client.id.required'),
            'info.client_id.numeric'         => Lang::get('core::validation.client.id.numeric'),
            'info.transaction_type.required' => Lang::get('core::validation.transaction.type.required'),
            'info.transaction_type.numeric'  => Lang::get('core::validation.transaction.type.numeric'),
            'info.source.required'           => Lang::get('core::validation.source.required'),
            'info.source.numeric'            => Lang::get('core::validation.source.numeric'),
            'info.row_count.required'        => Lang::get('core::validation.row.count.required'),
            'info.row_count.numeric'         => Lang::get('core::validation.row.count.numeric'),
            'info.import_date.required'      => Lang::get('core::validation.import.date.required'),
            'info.import_date.date'          => Lang::get('core::validation.import.date.date'),
            'info.imported_by.required'      => Lang::get('core::validation.imported.by.required'),
            'info.imported_by.numeric'       => Lang::get('core::validation.imported.by.numeric')
        ];
    }

    private function detailMessages()
    {
        return $this->createArrayMessages('details.*', [
            'product_id.required' => Lang::get('core::validation.product.id.required'),
            'product_id.numeric'  => Lang::get('core::validation.product.id.numeric'),
            'qty.required'        => Lang::get('core::validation.qty.required'),
            'qty.numeric'         => Lang::get('core::validation.qty.numeric'),
            'price.required'      => Lang::get('core::validation.price.required'),
            'price.numeric'       => Lang::get('core::validation.price.numeric'),
            'unit_id.required'    => Lang::get('core::validation.unit.id.required'),
            'unit_id.exists_in'   => Lang::get('core::validation.unit.id.doesnt.exists'),
            'unit_qty.requred'    => Lang::get('core::validation.unit.qty.required'),
            'unit_qty.numeric'    => Lang::get('core::validation.unit.qty.numeric'),
        ]);
    }

    private function arrayMessages()
    {
        return [
            'info.required'    => Lang::get('core::validation.info.required'),
            'info.array'       => Lang::get('core::validation.invalid.format'),
            'info.min'         => Lang::get('core::validation.info.min.one'),
            'details.required' => Lang::get('core::validation.details.required'),
            'details.array'    => Lang::get('core::validation.invalid.format'),
            'details.min'      => Lang::get('core::validation.details.min.one')
        ];
    }
}