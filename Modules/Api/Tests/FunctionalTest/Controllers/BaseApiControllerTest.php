<?php

use Laravel\Passport\ClientRepository;
use Modules\Core\Entities\User;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;
use Modules\Core\Tests\Traits\Application;
use Modules\Core\Tests\Traits\ApiResponse;
use Modules\Core\Tests\Traits\Assertions;
use Modules\Core\Tests\Traits\HttpRequests;
use Modules\Core\Tests\Traits\Authentication;
use Modules\Core\Tests\Traits\Migrations;
use Modules\Core\Tests\Traits\DataTransformer;
use Modules\Core\Tests\Traits\UpdatesApplicationSettings;
use Faker\Factory as Faker;

abstract class BaseApiControllerTest extends BaseTestCase
{
    use DatabaseTransactions,
        Application,
        Assertions,
        HttpRequests,
        Authentication,
        Migrations,
        ApiResponse,
        DataTransformer,
        UpdatesApplicationSettings;

    protected $baseUrl;

    protected $faker;

    protected $version = '/api/v1';

    protected $headers = [];

    protected $uri;

    public function setUp()
    {
        parent::setUp();

        ini_set('memory_limit', '2G');

        $this->baseUrl = env('APP_URL', 'http://localhost/').$this->version;

        $this->artisan('migrate');
        $this->runMigrations();
        $this->resetTableIndex();

        $this->init();
        $this->setAPIToken();

        $this->faker = Faker::create()->unique(true);

        Factory::$factoriesPath = 'Modules/Core/Tests/Factories';
    }

    protected function link($controller, $parameters = [])
    {
        return action(sprintf('Modules\Api\Http\Controllers\%s', $controller), $parameters);
    }

    protected function setAPIToken()
    {
        $user = User::find(1);

        $clientRepository = new ClientRepository();

        $client = $clientRepository->createPersonalAccessClient(
            null,
            'Test Personal Access Client',
            $this->baseUrl
        );

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);

        $personalAccessToken = $user->createToken('TestToken');

        Auth::loginUsingId($user->id);

        Auth::user()->withAccessToken($personalAccessToken->token);

        $this->headers['Authorization'] = 'Bearer '.$personalAccessToken->accessToken;
        $this->headers['Branch'] = 1;
    }
}
