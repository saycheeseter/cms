<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;

class ApiProductDiscountControllerTest extends BaseApiControllerTest
{
    protected $uri = '/product-discount';

    private $factory = array(
        'info' => 'product_discount',
        'detail' => 'product_discount_detail',
        'target' => 'product_discount_target'
    );
 
    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '7777',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    protected function build($data = array())
    {
        $discount = Factory::build(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::build(
            $this->factory['detail'], 
            isset($data['detail']) 
                ? $data['detail']
                : []
        );

        $targets = Factory::build(
            $this->factory['target'], 
            isset($data['target']) 
                ? $data['target']
                : []
        );

        return array(
            'info' => $discount,
            'details' => $details,
            'targets' => $targets
        );
    }

    protected function create($data = array())
    {   
        $discount = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('product_discount_id' => $discount->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        $targets = Factory::times(2)->create(
            $this->factory['target'], 
            array_merge(
                array('product_discount_id' => $discount->id),
                isset($data['target']) 
                ? $data['target']
                : []
            )
        );

        return array(
            'info' => $discount,
            'details' => $details,
            'targets' => $targets
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function apiHeadTransformer($model)
    {   
        $info = $model['info'];
        $details = $model['details'];
        $targets = $model['targets'];

        return [ 
            'id'    => $info->id,
            'code'  => (string) $info->code,
            'name'  => $info->name,
            'valid_from'  => $info->valid_from->toDateTimeString(),
            'valid_to'  => $info->valid_to->toDateTimeString(),
            'status'  => $info->status,
            'mon'  => $info->mon,
            'tue'  => $info->tue,
            'wed'  => $info->wed,
            'thur'  => $info->thur,
            'sat'  => $info->sat,
            'sun'  => $info->sun,
            'target_type'  => $info->target_type,
            'discount_scheme'  => $info->discount_scheme,
            'select_type'  => $info->select_type,
            'created_for'     => [
                'id' => $info->created_for,
                'link' => $this->link('ProductDiscountController@show' ,['id' => $info->created_for])
            ],
            'created_from'     => [
                'id' => $info->created_from,
                'link' => $this->link('ProductDiscountController@show' ,['id' => $info->created_from])
            ],
            'created_by'     => [
                'id' => $info->created_by,
                'link' => $this->link('UserController@show' ,['id' => $info->created_by])
            ],
            'modified_by'     => [
                'id' => $info->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $info->modified_by])
            ],
            'details' => array(
                'data' => $this->collect($details, 'apiDetailTransformer')
            ),
            'targets' => array(
                'data' => $this->collect($targets, 'apiTargetTransformer')
            )
        ];
    }

    protected function apiDetailTransformer($model)
    {   
        return [ 
            'product_discount_id'    => (string) $model->product_discount_id,
            'entity_type'    => (string) $model->entity_type,
            'entity_id'    => $model->entity_id,
            'discount_type'    => $model->discount_type,
            'discount_value'    => (string) $model->number()->discount_value,
            'qty'    => $model->number()->qty
        ];
    }

    protected function apiTargetTransformer($model)
    {   
        return [ 
            'product_discount_id'    => (string) $model->product_discount_id,
            'target_id'    => (string) $model->target_id,
            'target_type'    => (string) $model->target_type
        ];
    }
}
