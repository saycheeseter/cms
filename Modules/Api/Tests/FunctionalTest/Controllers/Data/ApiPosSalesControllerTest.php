<?php

use Laracasts\TestDummy\Factory;

class ApiPosSalesControllerTest extends BaseApiControllerTest
{
    protected $uri = '/sales';
    protected $factory = [
        'head' => 'pos_sales',
        'detail' => 'pos_sales_detail',
        'collection' => 'pos_collection',
    ];
    protected $references = [
        'product' => [],
        'product_branch_summary' => []
    ];

    public function setUp()
    {
        parent::setUp();

        $this->reference();
    }

    /**
     * @test
     */
    public function saving_an_empty_array_will_trigger_an_error_response()
    {
        $data = [];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'info.cashier' => [Lang::get('core::validation.cashier.required')],
                'info.salesman_id' => [Lang::get('core::validation.salesman.required')],
                'info.created_for' => [Lang::get('core::validation.created.for.required')],
                'info.for_location' => [Lang::get('core::validation.for.location.required')],
                'info.transaction_date' => [Lang::get('core::validation.transaction.date.required')],
                'info.customer_type' => [Lang::get('core::validation.customer.type.required')],
                'info.total_amount' => [Lang::get('core::validation.total.amount.required')],
                'info.terminal_number' => [Lang::get('core::validation.terminal.number.required')],
                'info.is_non_vat' => [Lang::get('core::validation.is.non.vat.required')],
                'info.is_wholesale' => [Lang::get('core::validation.is.wholesale.required')],
                'details' => [Lang::get('core::validation.details.required')],
                'collections' => [Lang::get('core::validation.collections.required')]
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_details_or_collections_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['details'] = '123123';
        $data['collections'] = '123123';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details' => [Lang::get('core::validation.invalid.format')],
                'collections' => [Lang::get('core::validation.invalid.format')]
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_array_details_or_collections_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['details'] = [];
        $data['collections'] = [];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details' => [Lang::get('core::validation.details.required')],
                'collections' => [Lang::get('core::validation.collections.required')]
            ]));
    }

    /**
     * @test
     */
    public function saving_non_existent_user_or_branch_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['info']['cashier'] = 99;
        $data['info']['salesman_id'] = 99;
        $data['info']['created_for'] = 99;
        $data['info']['for_location'] = 99;

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'info.cashier' => [Lang::get('core::validation.cashier.doesnt.exists')],
                'info.salesman_id' => [Lang::get('core::validation.salesman.doesnt.exists')],
                'info.created_for' => [Lang::get('core::validation.created.for.doesnt.exists')],
                'info.for_location' => [Lang::get('core::validation.for.location.doesnt.exists')],
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_date_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['info']['transaction_date'] = 'abc';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'info.transaction_date' => [Lang::get('core::validation.transaction.date.invalid.format')],
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_for_numeric_values_in_info_array_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['info']['total_amount'] = 'abc';
        $data['info']['is_non_vat'] = 'abc';
        $data['info']['is_wholesale'] = 'abc';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'info.total_amount' => [Lang::get('core::validation.total.amount.numeric')],
                'info.is_non_vat' => [Lang::get('core::validation.is.not.vat.numeric')],
                'info.is_wholesale' => [Lang::get('core::validation.is.wholesale.numeric')],
            ]));
    }

    /**
     * @test
     */
    public function saving_details_with_missing_required_data_will_trigger_an_error_response()
    {
        $data = $this->build();

        unset($data['details'][0]['product_id']);
        unset($data['details'][0]['unit_id']);
        unset($data['details'][0]['unit_qty']);
        unset($data['details'][0]['qty']);
        unset($data['details'][0]['oprice']);
        unset($data['details'][0]['price']);
        unset($data['details'][0]['vat']);
        unset($data['details'][0]['is_senior']);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details.0.product_id' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.product.required')],
                'details.0.unit_id' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.unit.required')],
                'details.0.unit_qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.unit.qty.required')],
                'details.0.qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.qty.required')],
                'details.0.oprice' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.oprice.required')],
                'details.0.price' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.price.required')],
                'details.0.vat' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.vat.required')],
                'details.0.is_senior' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.is.senior.required')],
            ]));
    }

    /**
     * @test
     */
    public function saving_details_with_non_existent_data_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['details'][0]['product_id'] = 99;
        $data['details'][0]['unit_id'] = 99;

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details.0.product_id' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.product.doesnt.exists')],
                'details.0.unit_id' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.unit.doesnt.exists')]
            ]));
    }

    /**
     * @test
     */
    public function saving_details_with_incorrect_numeric_format_data_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['details'][0]['unit_qty'] = 'abc';
        $data['details'][0]['qty'] = 'abc';
        $data['details'][0]['oprice'] = 'abc';
        $data['details'][0]['price'] = 'abc';
        $data['details'][0]['discount1'] = 'abc';
        $data['details'][0]['discount2'] = 'abc';
        $data['details'][0]['discount3'] = 'abc';
        $data['details'][0]['discount4'] = 'abc';
        $data['details'][0]['vat'] = 'abc';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details.0.unit_qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.unit.qty.numeric')],
                'details.0.qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.qty.numeric')],
                'details.0.oprice' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.oprice.numeric')],
                'details.0.price' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.price.numeric')],
                'details.0.discount1' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.numeric', ['num' => '1'])],
                'details.0.discount2' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.numeric', ['num' => '2'])],
                'details.0.discount3' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.numeric', ['num' => '3'])],
                'details.0.discount4' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.numeric', ['num' => '4'])],
                'details.0.vat' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.vat.numeric')]
            ]));
    }

    /**
     * @test
     */
    public function saving_qty_and_unit_qty_below_minimum_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['details'][0]['unit_qty'] = 0;
        $data['details'][0]['qty'] = 0;

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'details.0.unit_qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.unit.qty.greater.than.zero')],
                'details.0.qty' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.qty.greater.than.zero')],
            ]));
    }

    /**
     * @test
     */
    public function saving_data_with_empty_payment_method_and_amount_in_collection_array_will_trigger_an_error_response()
    {
        $data = $this->build();

        unset($data['collections'][0]['payment_method']);
        unset($data['collections'][0]['amount']);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'collections.0.amount' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.amount.required')],
                'collections.0.payment_method' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.payment.method.required')]
            ]));
    }

    /**
     * @test
     */
    public function saving_data_with_non_existent_payment_method_in_collection_array_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['collections'][0]['payment_method'] = 99;

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'collections.0.payment_method' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.payment.method.doesnt.exists')]
            ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_format_of_amount_in_collection_array_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['collections'][0]['amount'] = 'abc';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'collections.0.amount' => [Lang::get('core::error.row').'[1] '.Lang::get('core::validation.amount.numeric')]
            ]));
    }

    /**
     * @test
     */
    public function saving_a_correct_format_of_data_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $data = $this->build();

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse([], 
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('pos_sales', $data['info']);
    }

    /**
     * @test
     */
    public function saving_a_correct_format_of_data_will_successfully_save_and_decrease_inventory()
    {
        $data = $this->build();

        $expected = [
            'branch_id' => 1,
            'branch_detail_id' => 1,
            'product_id' => $this->references['product']->id,
            'qty' => $this->references['product_branch_summary']->qty->innerValue() - $data['details'][0]['qty']
        ];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse([], 
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('product_branch_summary', $expected);
    }

    /**
     * @test
     */
    public function saving_a_correct_format_of_data_will_successfully_save_and_insert_in_product_transaction_summary_table()
    {
        $data = $this->build();

        $expected = [
            'product_id' => $this->references['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'pos_sales' => $data['details'][0]['qty']
        ];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse([], 
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('product_transaction_summary', $expected);
    }

    /**
     * @test
     */
    public function saving_a_correct_format_of_data_will_successfully_save_and_insert_in_product_stock_card_table()
    {
        $data = $this->build();

        $expected = [
            'product_id' => $this->references['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'reference_type' => 11,
            'qty' => $data['details'][0]['qty'] * -1,
            'price' => $data['details'][0]['price'],
            'amount' => $data['details'][0]['qty'] * $data['details'][0]['price']
        ];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse([], 
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('product_stock_card', $expected);
    }

    protected function reference()
    {
        $this->references['product'] = Factory::create('product');
        $this->references['product_branch_summary'] = Factory::create(
            'product_branch_summary',
            ['product_id' => $this->references['product']->id]
        );
    }

    protected function build($info = array(), $details = array(), $collections = array())
    {   
        return [
            'info' => $this->toArrayCast(Factory::build($this->factory['head'], $info)),
            'details' => [$this->toArrayCast(Factory::build(
                $this->factory['detail'], 
                array_merge(
                    $details, 
                    ['product_id' => $this->references['product']->id])
                )
            )],
            'collections' => [$this->toArrayCast(Factory::build($this->factory['collection'], $collections))]
        ];
    }
}