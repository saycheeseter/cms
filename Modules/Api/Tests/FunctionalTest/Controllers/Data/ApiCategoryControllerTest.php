<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;

class ApiCategoryControllerTest extends BaseApiControllerTest
{
    protected $uri = '/category';

    private $factory = 'category';

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'transform'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'transform'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 6,
                    'count' => 6,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'transform')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 'test',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'transform')
        )));
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 2, $deep = 2)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) { 

            $category = Factory::create($this->factory);

            $current = $category;

            for ($x=0; $x < $deep; $x++) { 
                $current = Factory::create($this->factory, array(
                    'parent_id' => $current->id
                ));
                $data[] = $current;
            }

            $data[] = $category;
        }

        return $data;
    }
    protected function transform($model)
    {   
        return [
            'id'            => $model->id,
            'code'          => (string) $model->code,
            'name'          => $model->name,
            'parent'     => [
                'id' => $model->parent_id ?? 0,
                'link' => $this->link('CategoryController@show' ,['id' => $model->parent_id])
            ],
            'creator'     => [
                'id' => $model->created_by,
                'link' => $this->link('UserController@show' ,['id' => $model->created_by])
            ],
            'modifier'     => [
                'id' => $model->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $model->modified_by])
            ]
        ];
    }
}
