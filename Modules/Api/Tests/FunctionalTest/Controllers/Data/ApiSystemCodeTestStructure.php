<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\SystemCode;

abstract class ApiSystemCodeTestStructure extends BaseApiControllerTest
{
    protected $uri;
    protected $factory;
    protected $type;

    public function setUp()
    {
        parent::setUp();
        $this->clean();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->remarks,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'remarks'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'transform')
            
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'test'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'transform')
            
        ], ''));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'transform')
        )));
    }


    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function transform($model)
    {
        return array(
            'id' => $model->id,
            'type' => [
                'id' => (string) $model->type_id,
                'link' => $this->link('SystemCodeTypeController@show', ['id' => $model->type_id])
            ],
            'code' => (string) $model->code,
            'name' => $model->name,
            'remarks' => $model->remarks,
        );
    }

    protected function clean()
    {
        SystemCode::getQuery()->delete();
    }
}
