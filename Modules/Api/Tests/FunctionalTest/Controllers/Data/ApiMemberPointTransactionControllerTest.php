<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\MemberPointType;

class ApiMemberPointTransactionControllerTest extends BaseApiControllerTest
{
    protected $uri = '/member-point-transaction';

    private $factory = 'member_point_transaction';
    private $table = 'member_point_transaction';

    private $members = [];

    public function setUp()
    {
        parent::setUp();

        $this->createMembers();
    }

    /**
     * @test
     */
    public function saving_an_empty_member_created_from_created_by_reference_type_reference_number_transaction_date_and_amount_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data = array_merge($data, [
            'member_id' => '',
            'created_from' => '',
            'created_by' => '',
            'reference_type' => '',
            'reference_number' => '',
            'transaction_date' => ''
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'member_id'      =>  [Lang::get('core::validation.member.required')],
                'created_from'   =>  [Lang::get('core::validation.created.from.required')],
                'created_by'     =>  [Lang::get('core::validation.created.by.required')],
                'reference_type' =>  [Lang::get('core::validation.reference_type.required')],
                'reference_number'      =>  [Lang::get('core::validation.reference_number.required')],
                'transaction_date'      =>  [Lang::get('core::validation.transaction.date.required')]
            ]));

    }

    /**
     * @test
     */
    public function saving_a_branch_member_and_user_that_doesnt_exist_will_trigger_an_error_response()
    {
        $data = $build = $this->build([
            'member_id' => 99,
            'created_from' => 99,
            'created_by' => 99,
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'member_id'      =>  [Lang::get('core::validation.member.doesnt.exists')],
                'created_from'      =>  [Lang::get('core::validation.branch.doesnt.exists')],
                'created_by'      =>  [Lang::get('core::validation.user.doesnt.exists')]
            ]));

    }

    /**
     * @test
     */
    public function saving_an_invalid_transaction_date_and_amount_will_trigger_an_error_response()
    {
        $data = $this->build([
            'member_id' => $this->members[rand(0, 2)]->id,
            'created_from' => 1,
        ]);

        $data = array_merge($data, [
            'transaction_date' => 'zzz',
            'amount' => 'zzz',
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'transaction_date'      =>  [Lang::get('core::validation.transaction.date.invalid.format')],
                'amount'      =>  [Lang::get('core::validation.amount.numeric')],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_correct_member_point_transaction_format_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $data = $this->build([
            'member_id' => $this->members[rand(0, 2)]->id,
            'created_from' => 1
        ]);
        
        $expected = [
            'response' => [
                'data' => $this->item($data, 'transform')
            ],
            'database' => $data
        ];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.created')
            ))->seeInDatabase($this->table, $expected['database']);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function build($data = array())
    {   
        return $this->toArrayCast(Factory::build($this->factory, $data));
    }

    protected function collection($times = 4)
    {   
        return Factory::times($times)->create($this->factory, ['member_id' => $this->members[rand(0, 2)]->id]);
    }

    private function createMembers()
    {
        $this->members = Factory::times(3)->create('member');
    }

    protected function transform($model)
    {   
        return [
            'reference_type' => $model['reference_type'],
            'reference_number' => $model['reference_number'],
            'transaction_date' => $model['transaction_date'],
            'amount' => $this->setSignedAmount($model['amount'], $model['reference_type']),
            'created_by'     => [
                'id' => $model['created_by'],
                'link' => $this->link('UserController@show' ,['id' => $model['created_by']])
            ],
            'created_from'     => [
                'id' => $model['created_from'],
                'link' => $this->link('BranchController@show' ,['id' => $model['created_from']])
            ],
            'member'     => [
                'id' => $model['member_id'],
                'link' => $this->link('MemberController@show' ,['id' => $model['member_id']])
            ]
        ];
    }

    /**
     * setter for transaction amount, set to negative depeding on the transaction type
     * @param int $amount
     * @param string $type
     * @return int amount
     */
    protected function setSignedAmount($amount, $type)
    {    
        switch ($type) {
            case MemberPointType::SALES_USE_POINTS :
                return $amount * -1;
                break;
            
            default:
                return $amount;
                break;
        }
    }
}
