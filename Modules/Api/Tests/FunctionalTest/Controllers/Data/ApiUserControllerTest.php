<?php

use Laracasts\TestDummy\Factory;

class ApiUserControllerTest extends BaseApiControllerTest
{
    protected $uri = '/user';

    private $factory = 'user';

    /**
     * @test
     */
    public function searching_for_a_data_based_on_full_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->full_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'full_name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_username_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->username,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'username'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_position_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->position,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'position'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'transform')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {   
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '7777',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'full_name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function transform($model)
    {
        return [
            'id' => $model->id,
            'code' => $model->code,
            'username' => $model->username,
            'password' => $model->password,
            'full_name' => $model->full_name,
            'position' => $model->position,
            'address' => $model->address,
            'telephone' => $model->telephone,
            'mobile' => $model->mobile,
            'email' => $model->email,
            'memo' => $model->memo,
            'type' => (string)$model->type,
            'status' => $model->status,
            'permissions' => $this->getPivot($model->permissions),
            'created_from' => [
                'id' => (string)$model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ],
            'created_by' => [
                'id' => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by' => [
                'id' => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
        ];
    }

    private function getPivot($data)
    {
        $returnArray = [];
        
        foreach ($data as $key => $value) {
            $returnArray[] = $value->pivot;
        }

        return $returnArray;
    }
}