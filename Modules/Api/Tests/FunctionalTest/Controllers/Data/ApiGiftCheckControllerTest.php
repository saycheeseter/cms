<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\GiftCheckStatus;
use Modules\Core\Entities\Terminal;

class ApiGiftCheckControllerTest extends BaseApiControllerTest
{
    protected $uri = '/gift-check';

    private $factory = 'gift_check';

    /**
     * @test
     */
    public function fetching_already_redeemed_gift_check_will_return_an_error_response()
    {
        $data = $this->create([
            'status' => GiftCheckStatus::REDEEMED
        ]);

        $this->getJson(
            sprintf('%s/%s/valid', $this->uri, $data->check_number), [], $this->headers
        )->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.gift.check.not.found')
        )));
    }

    /**
     * @test
     */
    public function fetching_overdue_gift_check_will_return_an_error_response()
    {
        $data = $this->create([
            'date_from' => Carbon::now()->subDays(2),
            'date_to' => Carbon::yesterday()
        ]);

        $this->getJson(
            sprintf('%s/%s/valid', $this->uri, $data->check_number), [], $this->headers
        )->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.gift.check.not.found')
        )));
    }

    /**
     * @test
     */
    public function fetching_a_valid_gift_check_will_return_an_gift_check_information()
    {
        $data = $this->collection();

        $this->getJson(
            sprintf('%s/%s/valid', $this->uri, $data[0]->check_number), [], $this->headers
        )->seeJsonEquals($this->successfulResponse([
            $this->item($data[0], 'transform')
        ]));
    }

    /**
     * @test
     */
    public function redeeming_a_valid_gift_check_without_reference_number_will_trigger_error_response()
    {
        $data = $this->collection();

        $this->patchJson(
            sprintf('%s/%s/redeem', $this->uri, $data[0]->check_number), [], $this->headers
        )->seeJsonEquals($this->errorResponse([
                'reference_number' => array(
                    Lang::get('core::validation.reference.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function redeeming_an_already_redeemed_gift_check_will_return_an_error_response()
    {
       $data = $this->create([
           'status' => GiftCheckStatus::REDEEMED
       ]);

        $this->patchJson(
            sprintf('%s/%s/redeem', $this->uri, $data->check_number) , ['reference_number' => '1234567'], $this->headers
        )->seeJsonEquals($this->errorResponse(array(
            'redeem.failed' => Lang::get('core::error.gift.check.not.found')
        )));
    }

    /**
     * @test
     */
    public function redeeming_a_valid_gift_check_will_return_a_successful_response()
    {
        $data = $this->collection();

        $data[0]['reference_number'] = '1234567';

        $this->patchJson(
            sprintf('%s/%s/redeem', $this->uri, $data[0]->check_number) , ['reference_number' => '1234567'], $this->headers
        )->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.gift.check.successfully.redeemed')));
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function transform($model)
    {
        return [
            'id'               => $model->id,
            'code'             => $model->code,
            'check_number'     => $model->check_number,
            'amount'           => $model->number()->amount,
            'date_from'        => $model->date_from->toDateString(),
            'date_to'          => $model->date_to->toDateString(),
            'redeemed_date'    => $model->redeemed_date ? $model->redeemed_date->toDateTimeString() : null,
            'status'           => $model->status,
            'redeemed_by'      => $model->redeemed_by ? $model->redeemed_by : null,
            'generated_rand'   => $model->generated_rand,
            'reference_number' => $model->reference_number,
            'created_by'       => [
                'id'   => $model->created_by,
                'link' => $this->link('UserController@show', ['id' => $model->created_by])
            ],
            'modified_by'      => [
                'id'   => $model->modified_by,
                'link' => $this->link('UserController@show', ['id' => $model->modified_by])
            ],
            'created_from'     => [
                'id'   => $model->created_from,
                'link' => $this->link('BranchController@show', ['id' => $model->created_from])
            ]
        ];
    }
}
