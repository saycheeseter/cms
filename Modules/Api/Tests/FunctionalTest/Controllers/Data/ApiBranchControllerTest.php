<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;
use Modules\Core\Entities\Branch;

class ApiBranchControllerTest extends BaseApiControllerTest
{
    protected $uri = '/branch';

    private $factory = array(
        'info' => 'branch',
        'detail' => 'branch_detail'
    );

    public function setUp()
    {
        parent::setUp();
        $this->clean();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_business_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->business_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'business_name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]['info']->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'apiHeadTransformer')
        )));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 'test',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    protected function build($data = array())
    {
        $branch = Factory::build(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::build(
            $this->factory['detail'], 
            isset($data['detail']) 
                ? $data['detail']
                : []
        );

        return array(
            'info' => $branch,
            'details' => $details
        );
    }

    protected function create($data = array())
    {
        $branch = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('branch_id' => $branch->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        return array(
            'info' => $branch,
            'details' => $details
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function apiHeadTransformer($model)
    {   
        $info = $model['info'];
        $details = $model['details'];

        return [ 
            'id'    => $info->id,
            'code'  => (string) $info->code,
            'name'  => $info->name,
            'address'  => $info->address,
            'contact'  => $info->contact,
            'business_name'  => $info->business_name,
            'created_by'     => [
                'id' => $info->created_by,
                'link' => $this->link('UserController@show' ,['id' => $info->created_by])
            ],
            'modified_by'     => [
                'id' => $info->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $info->modified_by])
            ],
            'details' => array(
                'data' => $this->collect($details, 'apiDetailTransformer')
            )
        ];
    }

    protected function apiDetailTransformer($model)
    {   
        return [ 
            'id'    => $model->id,
            'code'  => (string) $model->code,
            'name'  => $model->name,
            'address'  => $model->address,
            'contact'  => $model->contact,
            'business_name'  => $model->business_name,
            'created_by'     => [
                'id' => $model->created_by,
                'link' => $this->link('UserController@show' ,['id' => $model->created_by])
            ],
            'modified_by'     => [
                'id' => $model->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $model->modified_by])
            ]
        ];
    }

    protected function clean()
    {
        Branch::getQuery()->delete();
    }
}
