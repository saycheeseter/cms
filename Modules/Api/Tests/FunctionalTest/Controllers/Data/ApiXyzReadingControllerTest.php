<?php

use Laracasts\TestDummy\Factory;

class ApiXyzReadingControllerTest extends BaseApiControllerTest
{
    protected $uri = '/xyz-reading';
    protected $factory = 'xyz_reading';

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function saving_an_empty_array_will_trigger_an_error_response()
    {
        $data = [];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'created_from' => [Lang::get('core::validation.created.from.required')],
                'read_count' => [Lang::get('core::validation.read.count.required')],
                'read_type' => [Lang::get('core::validation.read.type.required')],
                'start_or' => [Lang::get('core::validation.start.or.required')],
                'end_or' => [Lang::get('core::validation.end.or.required')],
                'date' => [Lang::get('core::validation.date.required')],
                'terminal_number' => [Lang::get('core::validation.terminal.number.required')],
                'branch_id' => [Lang::get('core::validation.branch.required')],
                'old_grand_total' => [Lang::get('core::validation.old.grand.total.required')],
                'new_grand_total' => [Lang::get('core::validation.new.grand.total.required')],
                'total_discount_amount' => [Lang::get('core::validation.total.discount.amount.required')],
                'vat_returns_amount' => [Lang::get('core::validation.vat.returns.amount.required')],
                'nonvat_returns_amount' => [Lang::get('core::validation.nonvat.returns.amount.required')],
                'senior_returns_amount' => [Lang::get('core::validation.senior.returns.amount.required')],
                'cash_sales_amount' => [Lang::get('core::validation.cash.sales.amount.required')],
                'credit_sales_amount' => [Lang::get('core::validation.credit.sales.amount.required')],
                'debit_sales_amount' => [Lang::get('core::validation.debit.sales.amount.required')],
                'bank_sales_amount' => [Lang::get('core::validation.bank.sales.amount.required')],
                'gift_sales_amount' => [Lang::get('core::validation.gift.sales.amount.required')],
                'senior_sales_amount' => [Lang::get('core::validation.senior.sales.amount.required')],
                'vatable_sales_amount' => [Lang::get('core::validation.vatable.sales.amount.required')],
                'nonvat_sales_amount' => [Lang::get('core::validation.nonvat.sales.amount.required')],
                'total_qty_sold' => [Lang::get('core::validation.total.qty.sold.required')],
                'total_qty_void' => [Lang::get('core::validation.total.qty.void.required')],
                'void_transaction_amount' => [Lang::get('core::validation.void.transaction.amount.required')],
                'success_transaction_count' => [Lang::get('core::validation.success.transaction.count.required')],
                'return_qty_count' => [Lang::get('core::validation.return.qty.count.required')],
                'member_points_amount' => [Lang::get('core::validation.member.points.amount.required')]
            ]));
    }

    /**
     * @test
     */
    public function saving_an_with_non_existent_branch_will_trigger_an_error_response()
    {
        $data = $this->build([
            'created_from' => '99',
            'branch_id' => '99',
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'created_from' => [Lang::get('core::validation.created.from.doesnt.exists')],
                'branch_id' => [Lang::get('core::validation.branch.doesnt.exists')],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_date_with_incorrect_format_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data['date'] = 'zzz';

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'date' => [Lang::get('core::validation.date.format')],
            ]));
    }

    /**
     * @test
     */
    public function saving_data_that_is_not_numeric_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data = array_merge($data, [
            'read_count' => 'a',
            'read_type' => 'a',
            'terminal_number' => 'a',
            'old_grand_total' => 'a',
            'new_grand_total' => 'a',
            'total_discount_amount' => 'a',
            'vat_returns_amount' => 'a',
            'nonvat_returns_amount' => 'a',
            'senior_returns_amount' => 'a',
            'cash_sales_amount' => 'a',
            'credit_sales_amount' => 'a',
            'debit_sales_amount' => 'a',
            'bank_sales_amount' => 'a',
            'gift_sales_amount' => 'a',
            'senior_sales_amount' => 'a',
            'vatable_sales_amount' => 'a',
            'nonvat_sales_amount' => 'a',
            'total_qty_sold' => 'a',
            'total_qty_void' => 'a',
            'void_transaction_amount' => 'a',
            'success_transaction_count' => 'a',
            'return_qty_count' => 'a',
            'member_points_amount' => 'a'
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'read_count' => [Lang::get('core::validation.read.count.numeric')],
                'read_type' => [Lang::get('core::validation.read.type.numeric')],
                'terminal_number' => [Lang::get('core::validation.terminal.number.numeric')],
                'old_grand_total' => [Lang::get('core::validation.old.grand.total.numeric')],
                'new_grand_total' => [Lang::get('core::validation.new.grand.total.numeric')],
                'total_discount_amount' => [Lang::get('core::validation.total.discount.amount.numeric')],
                'vat_returns_amount' => [Lang::get('core::validation.vat.returns.amount.numeric')],
                'nonvat_returns_amount' => [Lang::get('core::validation.nonvat.returns.amount.numeric')],
                'senior_returns_amount' => [Lang::get('core::validation.senior.returns.amount.numeric')],
                'cash_sales_amount' => [Lang::get('core::validation.cash.sales.amount.numeric')],
                'credit_sales_amount' => [Lang::get('core::validation.credit.sales.amount.numeric')],
                'debit_sales_amount' => [Lang::get('core::validation.debit.sales.amount.numeric')],
                'bank_sales_amount' => [Lang::get('core::validation.bank.sales.amount.numeric')],
                'gift_sales_amount' => [Lang::get('core::validation.gift.sales.amount.numeric')],
                'senior_sales_amount' => [Lang::get('core::validation.senior.sales.amount.numeric')],
                'vatable_sales_amount' => [Lang::get('core::validation.vatable.sales.amount.numeric')],
                'nonvat_sales_amount' => [Lang::get('core::validation.nonvat.sales.amount.numeric')],
                'total_qty_sold' => [Lang::get('core::validation.total.qty.sold.numeric')],
                'total_qty_void' => [Lang::get('core::validation.total.qty.void.numeric')],
                'void_transaction_amount' => [Lang::get('core::validation.void.transaction.amount.numeric')],
                'success_transaction_count' => [Lang::get('core::validation.success.transaction.count.numeric')],
                'return_qty_count' => [Lang::get('core::validation.return.qty.count.numeric')],
                'member_points_amount' => [Lang::get('core::validation.member.points.amount.numeric')]
            ]));
    }

    /**
     * @test
     */
    public function saving_a_correct_format_of_xyz_reading_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $data = $this->build();

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse([], 
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('xyz_reading', $data);
    }

    protected function build($data = array())
    {   
        return $this->toArrayCast(Factory::build($this->factory, $data));
    }
}