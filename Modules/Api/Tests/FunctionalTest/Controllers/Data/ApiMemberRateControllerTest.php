<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;

class ApiMemberRateControllerTest extends BaseApiControllerTest
{
    protected $uri = '/member-rate';

    private $factory = array(
        'info' => 'member_rate',
        'detail' => 'member_rate_detail'
    );
 
    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_memo_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->memo,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'memo'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '7777',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]['info']->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'apiHeadTransformer')
        )));
    }

    protected function build($data = array())
    {
        $rate = Factory::build(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::build(
            $this->factory['detail'], 
            isset($data['detail']) 
                ? $data['detail']
                : []
        );

        return array(
            'info' => $rate,
            'details' => $details
        );
    }

    protected function create($data = array())
    {
        $rate = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('member_rate_head_id' => $rate->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        return array(
            'info' => $rate,
            'details' => $details
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function apiHeadTransformer($model)
    {   
        $info = $model['info'];
        $details = $model['details'];

        return [ 
            'id'    => $info->id,
            'name'    => $info->name,
            'memo'    => $info->memo,
            'status'    => (string) $info->status,
            'created_from'  => [
                'id' => (string) $info->created_from,
                'link' => $this->link('BranchController@show' ,['id' => $info->created_from])
            ],
            'created_by'     => [
                'id' => $info->created_by,
                'link' => $this->link('UserController@show' ,['id' => $info->created_by])
            ],
            'modified_by'     => [
                'id' => $info->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $info->modified_by])
            ],
            'details' => array(
                'data' => $this->collect($details, 'apiDetailTransformer')
            )
        ];
    }

    protected function apiDetailTransformer($model)
    {   
        return [ 
            'id'    => $model->id,
            'amount_from'    => $model->number()->amount_from,
            'amount_to'    => $model->number()->amount_to,
            'date_from'    => $model->date_from->toDateTimeString(),
            'date_to'    => $model->date_to->toDateTimeString(),
            'amount_per_point'    => $model->number()->amount_per_point,
            'discount'    => $model->number()->discount
        ];
    }
}
