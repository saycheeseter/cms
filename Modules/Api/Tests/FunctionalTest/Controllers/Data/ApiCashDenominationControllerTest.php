<?php

use Laracasts\TestDummy\Factory;

class ApiCashDenominationControllerTest extends BaseApiControllerTest
{
    protected $uri = '/cash-denomination';

    private $factory = 'cash_denomination';
    private $table = 'cash_denomination';

    private $terminals = [];

    /**
     * @test
     */
    public function saving_an_empty_terminal_number_type_created_from_created_by_and_create_date_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data = array_merge($data, [
            'terminal_number' => '',
            'created_from' => '',
            'created_by' => '',
            'type' => '',
            'create_date' => '',
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'terminal_number'      =>  [Lang::get('core::validation.terminal.number.required')],
                'created_from'   =>  [Lang::get('core::validation.created.from.required')],
                'created_by'     =>  [Lang::get('core::validation.created.by.required')],
                'type'     =>  [Lang::get('core::validation.denomination_type.required')],
                'create_date'     =>  [Lang::get('core::validation.date.required')]
            ]));

    }

    /**
     * @test
     */
    public function saving_an_invalid_create_date_and_denomination_will_trigger_an_error_response()
    {
        $data = $this->build();

        $data = array_merge($data, [
            'create_date' => '7777',
            'b1000' => 'zzz' ,
            'b500' => 'zzz' ,
            'b200' => 'zzz' ,
            'b100' => 'zzz' ,
            'b50' => 'zzz' ,
            'b20' => 'zzz' ,
            'c10' => 'zzz' ,
            'c5' => 'zzz' ,
            'c1' => 'zzz' ,
            'c25c' => 'zzz' ,
            'c10c' => 'zzz' ,
            'c5c' => 'zzz' ,
        ]);
        
        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'create_date' =>  [Lang::get('core::validation.date.format')],
                'b1000'       => [Lang::get('core::validation.denomination.numeric', ['number' => '1000'])],
                'b500'        => [Lang::get('core::validation.denomination.numeric', ['number' => '500'])],
                'b200'        => [Lang::get('core::validation.denomination.numeric', ['number' => '200'])],
                'b100'        => [Lang::get('core::validation.denomination.numeric', ['number' => '100'])],
                'b50'         => [Lang::get('core::validation.denomination.numeric', ['number' => '50'])],
                'b20'         => [Lang::get('core::validation.denomination.numeric', ['number' => '20'])],
                'c10'         => [Lang::get('core::validation.denomination.numeric', ['number' => '10'])],
                'c5'          => [Lang::get('core::validation.denomination.numeric', ['number' => '5'])],
                'c1'          => [Lang::get('core::validation.denomination.numeric', ['number' => '1'])],
                'c25c'        => [Lang::get('core::validation.denomination.numeric', ['number' => '25c'])],
                'c10c'        => [Lang::get('core::validation.denomination.numeric', ['number' => '10c'])],
                'c5c'         => [Lang::get('core::validation.denomination.numeric', ['number' => '5c'])],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existent_created_by_and_created_from_will_trigger_an_error_response()
    {
        $data = $this->build([
            'created_from' => '77',
            'created_by' => '77',
        ]);

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->errorResponse([
                'created_from'      =>  [Lang::get('core::validation.branch.doesnt.exists')],
                'created_by'      =>  [Lang::get('core::validation.user.doesnt.exists')]
            ]));

    }

    /**
     * @test
     */
    public function saving_a_correct_cash_denomination_format_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $data = $this->build();
        
        $expected = [
            'response' => [
                'data' => $this->item($data, 'transform')
            ],
            'database' => $data
        ];

        $this->postJson($this->uri, $data, $this->headers)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.created')
            ))->seeInDatabase($this->table, $expected['database']);
    }


    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function build($data = array())
    {   
        return $this->toArrayCast(Factory::build($this->factory, $data));
    }

    protected function collection($times = 4)
    {   
        return Factory::times($times)->create($this->factory, ['terminal_number' => 1]);
    }

    protected function transform($model)
    {
        return [
            'terminal_number' => $model['terminal_number'],
            'create_date' => $model['create_date'],
            'b1000' => $model['b1000'],
            'b500' => $model['b500'],
            'b200' => $model['b200'],
            'b100' => $model['b100'],
            'b50' => $model['b50'],
            'b20' => $model['b20'],
            'c10' => $model['c10'],
            'c5' => $model['c5'],
            'c1' => $model['c1'],
            'c25c' => $model['c25c'],
            'c10c' => $model['c10c'],
            'c5c' => $model['c5c'],
            'type' => $model['type'],
            'created_by'     => [
                'id' => $model['created_by'],
                'link' => $this->link('UserController@show' ,['id' => $model['created_by']])
            ],
            'created_from'     => [
                'id' => $model['created_from'],
                'link' => $this->link('BranchController@show' ,['id' => $model['created_from']])
            ]
        ];
    }
}
