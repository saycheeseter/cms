<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;

class ApiCustomerControllerTest extends BaseApiControllerTest
{
    protected $uri = '/customer';

    private $factory = array(
        'info' => 'customer',
        'detail' => 'customer_detail'
    );
 
    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array($this->item($data[0], 'apiHeadTransformer'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '7777',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]['info']->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'apiHeadTransformer')
        )));
    }

    protected function build($data = array())
    {
        $customer = Factory::build(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::build(
            $this->factory['detail'], 
            isset($data['detail']) 
                ? $data['detail']
                : []
        );

        return array(
            'info' => $customer,
            'details' => $details
        );
    }

    protected function create($data = array())
    {
        $customer = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('head_id' => $customer->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        return array(
            'info' => $customer,
            'details' => $details
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function apiHeadTransformer($model)
    {   
        $info = $model['info'];
        $details = $model['details'];

        return [ 
            'id' => $info->id,
            'created_from' => [
                'id' => (string) $info->created_from,
                'link' => $this->link('BranchController@show', ['id' => $info->created_from])
            ],
            'created_by' => [
                'id' => $info->created_by,
                'link' => $this->link('UserController@show', ['id' => $info->created_by])
            ],
            'modified_by' => [
                'id' => $info->modified_by,
                'link' => $this->link('UserController@show', ['id' => $info->modified_by])
            ],
            'name' => $info->name,
            'code' => (string) $info->code,
            'memo' => $info->memo,
            'credit_limit' => $info->number()->credit_limit,
            'pricing_type' => (string) $info->pricing_type,
            'historical_change_revert' => (string) $info->historical_change_revert,
            'historical_default_price' => (string) $info->historical_default_price,
            'pricing_rate' => $info->number()->pricing_rate,
            'salesman' => [
                'id' => (string) $info->salesman_id,
                'link' => $this->link('UserController@show', ['id' => $info->salesman_id])
            ],
            'website' => $info->website,
            'term' => (string) $info->term,
            'status' => (string) $info->status,
            'deleted_at' => $info->deleted_at ? $info->deleted_at->toDateString() : null,
            'created_at' => $info->created_at ? $info->created_at->toDateString() : null,
            'updated_at' => $info->updated_at ? $info->updated_at->toDateString() : null,
            'details' => array(
                'data' => $this->collect($details, 'apiDetailTransformer')
            )
        ];
    }

    protected function apiDetailTransformer($model)
    {   
        return [ 
            'id' => $model->id,
            'name' => $model->name,
            'address' => $model->address,
            'chinese_name' => $model->chinese_name,
            'owner_name' => $model->owner_name,
            'contact' => $model->contact,
            'landline' => $model->landline,
            'fax' => $model->fax,
            'mobile' => $model->mobile,
            'email' => $model->email,
            'contact_person' => $model->contact_person,
            'tin' => (string) $model->tin,
            'deleted_at' => $model->deleted_at ? $model->deleted_at->toDateString() : null,
            'created_at' => $model->created_at ? $model->created_at->toDateString() : null,
            'updated_at' => $model->updated_at ? $model->updated_at->toDateString() : null,
        ];
    }
}
