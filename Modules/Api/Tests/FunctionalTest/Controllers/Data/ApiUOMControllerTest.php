<?php

use Modules\Core\Enums\SystemCodeType;

class ApiUOMControllerTest extends ApiSystemCodeTestStructure
{
    protected $uri = '/uom';
    protected $factory = 'uom';
    protected $type = SystemCodeType::UOM;
}
