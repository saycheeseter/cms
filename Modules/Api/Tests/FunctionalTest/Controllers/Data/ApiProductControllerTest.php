<?php

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\InventoryStatus;

class ApiProductControllerTest extends BaseApiControllerTest
{
    protected $uri = '/product';

    private $factory = [
        'info' => 'product',
        'unit' => 'product_unit',
        'price' => 'branch_price',
        'barcode' => 'product_barcode',
        'branch' => 'product_branch_info',
        'location' => 'product_branch_detail',
        'inventory' => 'product_branch_summary',
        'tax' => 'tax'
    ];
    
    private $references = [
        'uoms' => [],
        'categories' => [],
        'suppliers' => [],
        'taxes' => [],
        'brands' => [],
        'customers' => []
    ];

    /**
     * @test
     */
    public function searching_for_a_data_based_on_stock_no_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]['info']->stock_no,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'stock_no'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[1], 'apiHeadTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_unit_barcode_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]['barcodes']->first()->code,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'barcode'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[2], 'apiHeadTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]['info']->name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[3], 'apiHeadTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_chinese_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]['info']->chinese_name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'chinese_name'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => array(
                $this->item($data[3], 'apiHeadTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_status_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => ProductStatus::ACTIVE,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'status'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_inventory_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => InventoryStatus::WITH_INVENTORY,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'inventory'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 1,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'id'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => $this->collect($data, 'apiHeadTransformer')
        ], ''));
    }

    /**
     * 
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '7777',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'stock_no'
                )
            )
        ), $this->headers)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function accessing_a_non_existent_data_will_return_an_error_response()
    {   
        $data = $this->collection();

        $this->getJson(sprintf('%s/77', $this->uri), [], $this->headers)
        ->seeJsonEquals($this->errorResponse(array(
            'show.failed' => Lang::get('core::error.show.failed')
        )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_will_return_data_full_information()
    {   
        $data = $this->collection();
        
        $this->getJson(sprintf('%s/%s', $this->uri, $data[0]['info']->id), [], $this->headers)
        ->seeJsonEquals($this->successfulResponse(array(
            $this->item($data[0], 'apiHeadTransformer')
        )));
    }

    private function create($data = array())
    {
        $product = Factory::create($this->factory['info'], array_merge(array(
            'supplier_id' => $this->references['suppliers'][rand(0, 2)]->id,
            'brand_id' => $this->references['brands'][rand(0, 2)]->id,
            'category_id' => $this->references['categories'][rand(0, 2)]->id,
        ), $data['info'] ?? []));

        $productTax = [
            'product_id' => $product->id,
            'tax_id' => $this->references['taxes'][rand(0, 2)]->id
        ];

        DB::table('product_tax')->insert($productTax);
        
        $tax = DB::table('tax')->where('id',  $productTax['tax_id'])->get()->first();

        $uom = $this->references['uoms'][rand(0, 2)];

        $unit = Factory::create($this->factory['unit'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['unit'] ?? []));

        $barcode = Factory::create($this->factory['barcode'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['barcode'] ?? []));

        $price = Factory::create($this->factory['price'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['price'] ?? []));

        $branch = Factory::create($this->factory['branch'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['branch'] ?? []));

        $location = Factory::create($this->factory['location'], array_merge(array(
            'product_id' => $product->id,
            'branch_detail_id' => 1,
        ), $data['location'] ?? []));

        $inventory = Factory::create($this->factory['inventory'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
            'branch_detail_id' => 1,
        ), $data['inventory'] ?? []));

        return [
            'info' => $product->fresh(),
            'taxes' => collect([$tax]),
            'units' => collect([$unit]),
            'barcodes' => collect([$barcode]),
            'prices' => collect([$price]),
            'branches' => collect([$branch]),
            'locations' => collect([$location]),
            'summaries' => collect([$inventory])
        ];
    }

    protected function collection($times = 4)
    {
        $data = [];

        $this->createReferences();

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    private function createReferences()
    {
        $this->references = [
            'uoms'       => Factory::times(3)->create('uom'),
            'categories' => Factory::times(3)->create('category'),
            'suppliers'  => Factory::times(3)->create('supplier'),
            'taxes'      => Factory::times(3)->create('tax'),
            'brands'     => Factory::times(3)->create('brand'),
            'customers'  => Factory::times(3)->create('customer'),
        ];
    }

    protected function apiHeadTransformer($model)
    {   
        $info = $model['info'];
        $units = $model['units'];
        $barcodes = $model['barcodes'];
        $prices = $model['prices'];
        $branches = $model['branches'];
        $locations = $model['locations'];
        $summaries = $model['summaries'];
        $taxes = $model['taxes'];

        return [ 
            'id'    => $info->id,
            'stock_no'    => $info->stock_no,
            'supplier_sku'    => $info->supplier_sku,
            'name'    => $info->name,
            'description'    => $info->description,
            'chinese_name'    => $info->chinese_name,
            'senior'    => $info->senior,
            'status'    => $info->status,
            'memo'    => $info->memo,
            'created_from'  => [
                'id' => $info->created_from,
                'link' => $this->link('BranchController@show' ,['id' => $info->created_from])
            ],
            'created_by'     => [
                'id' => $info->created_by,
                'link' => $this->link('UserController@show' ,['id' => $info->created_by])
            ],
            'modified_by'     => [
                'id' => $info->modified_by,
                'link' => $this->link('UserController@show' ,['id' => $info->modified_by])
            ],
            'supplier'     => [
                'id' => $info->supplier_id,
                'link' => $this->link('SupplierController@show' ,['id' => $info->supplier_id])
            ],
            'category'     => [
                'id' => $info->category_id,
                'link' => $this->link('CategoryController@show' ,['id' => $info->category_id])
            ],
            'brand'     => [
                'id' => $info->brand_id,
                'link' => $this->link('BrandController@show' ,['id' => $info->brand_id])
            ],
            'units' =>  array ('data' => $this->collect($units, 'apiUnitTransformer')),
            'barcodes' =>  array ('data' => $this->collect($barcodes, 'apiBarcodeTransformer')),
            'prices' =>  array ('data' => $this->collect($prices, 'apiPriceTransformer')),
            'branches' =>  array ('data' => $this->collect($branches, 'apiBranchTransformer')),
            'locations' =>  array ('data' => $this->collect($locations, 'apiLocationTransformer')),
            'summaries' =>  array ('data' => $this->collect($summaries, 'apiInventoryTransformer')),
            'taxes' =>  array ('data' => $this->collect($taxes, 'apiTaxTransformer'))
        ];
    }

    protected function apiUnitTransformer($model)
    {   
        return [ 
            'qty'    => $model->number()->qty,
            'length'    => $model->number()->length,
            'width'    => $model->number()->width,
            'height'    => $model->number()->height,
            'weight'    => $model->number()->weight,
            'default'    => (string) $model->default,
            'uom' => [
                'id' => (string) $model->uom_id,
                'link' => $this->link('UOMController@show' ,['id' => $model->uom_id])
            ]
        ];
    }

    protected function apiBarcodeTransformer($model)
    {   
        return [ 
            'code'          => (string) $model->code,
            'memo'          => $model->memo,
            'uom' => [
                'id' => (string) $model->uom_id,
                'link' => $this->link('UOMController@show' ,['id' => $model->uom_id])
            ]
        ];
    }

    protected function apiPriceTransformer($model)
    {   
        return [ 
            'purchase_price'          => $model->number()->purchase_price,
            'selling_price'          => $model->number()->selling_price,
            'wholesale_price'          => $model->number()->wholesale_price,
            'price_a'          => $model->number()->price_a,
            'price_b'          => $model->number()->price_b,
            'price_c'          => $model->number()->price_c,
            'price_d'          => $model->number()->price_d,
            'price_e'          => $model->number()->price_e,
            'editable'          => (string) $model->editable,
            'branch' => [
                'id' => (string) $model->branch_id,
                'link' => $this->link('BranchController@show' ,['id' => $model->branch_id])
            ]
        ];
    }

    protected function apiBranchTransformer($model)
    {   
        return [ 
            'status' => (string) $model->status,
            'branch' => [
                'id' => (string) $model->branch_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }

    protected function apiLocationTransformer($model)
    {   
        return [ 
            'min' => $model->number()->min,
            'max' => $model->number()->max,
            'branch' => [
                'id' => $model->branch_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }

    protected function apiInventoryTransformer($model)
    {   
        return [ 
            'reserved_qty' => $model->number()->reserved_qty,
            'requested_qty' => $model->number()->requested_qty,
            'qty' => $model->number()->qty,
            'branch' => [
                'id' => (string) $model->branch_id,
                'link' => $this->link('BranchController@show', ['id' => $model->branch_id])
            ]
        ];
    }

    protected function apiTaxTransformer($model)
    {   
        return [ 
            'id' => (int) $model->id,
            'name' => $model->name,
            'percentage' => $model->percentage,
            'is_inclusive' => $model->is_inclusive
        ];
    }
}
