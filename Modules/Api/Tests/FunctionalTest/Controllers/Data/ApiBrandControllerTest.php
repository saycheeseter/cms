<?php

use Modules\Core\Enums\SystemCodeType;

class ApiBrandControllerTest extends ApiSystemCodeTestStructure
{
    protected $uri = '/brand';
    protected $factory = 'brand';
    protected $type = SystemCodeType::BRAND;
}
