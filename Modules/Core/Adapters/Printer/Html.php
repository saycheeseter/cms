<?php

namespace Modules\Core\Adapters\Printer;

use Illuminate\Contracts\View\Factory as View;
use Modules\Core\Services\Contracts\Printer\Generator;
use Modules\Core\Entities\PrintoutTemplate;

class Html implements Generator
{
    private $view;
    private $data;
    private $format;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Set data to be used for printing
     * 
     * @param  object|string|array $data
     * @return $this
     */
    public function data($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set the format for custom templates
     * 
     * @param  object|string|array $data
     * @return $this
     */
    public function format(PrintoutTemplate $format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Use view instance to generate printout using blade template
     * 
     * @param  string $file
     * @return Response      
     */
    public function generate($file)
    {
        return $this->view->make($file, array(
            'data' => $this->data,
            'template' => $this->format
        ));
    }
}