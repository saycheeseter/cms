<?php

namespace Modules\Core\Enums;

final class DiscountType Extends Base
{
    const PERCENTAGE = 1;
    const DEDUCT_AMOUNT = 2;
    const DISCOUNTED_PRICE = 3;
}