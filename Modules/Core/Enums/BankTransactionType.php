<?php

namespace Modules\Core\Enums;

use Lang;

final class BankTransactionType extends Base 
{   
    const DEPOSIT = 0;
    const WITHDRAWAL = 1;
    const TRANSFER = 2;

    protected function __DEPOSIT()
    {
        $this->description = Lang::get('core::label.deposit');
    }

    protected function __WITHDRAWAL()
    {
        $this->description = Lang::get('core::label.withdrawal');
    }

    protected function __TRANSFER()
    {
        $this->description = Lang::get('core::label.transfer');
    }
}