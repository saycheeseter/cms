<?php

namespace Modules\Core\Enums;

final class ProductSeniorDiscount extends Base
{
    const NON_SENIOR = 0;
    const SENIOR_20 = 1;
    const SENIOR_5 = 2;
}