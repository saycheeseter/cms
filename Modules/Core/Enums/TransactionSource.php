<?php

namespace Modules\Core\Enums;

use Lang;

final class TransactionSource extends Base 
{
    const WEB = 1;
    const DATACOLLECTOR = 2;
    const CSV = 3;

    protected function __WEB()
    {
        $this->description = Lang::get('core::label.web');
    }

    protected function __DATACOLLECTOR()
    {
        $this->description = Lang::get('core::label.data.collector');
    }

    protected function __CSV()
    {
        $this->description = Lang::get('core::label.csv');
    }
}