<?php

namespace Modules\Core\Enums;

final class SerialStatus extends Base 
{
    const NOT_AVAILABLE = 0;
    const AVAILABLE = 1;
    const ON_TRANSIT = 2;
}