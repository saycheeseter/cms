<?php

namespace Modules\Core\Enums;

final class PurchaseBranchPriceBasis extends Base 
{
    const OWN_BRANCH = 1;
    const SELECTED_BRANCH = 2;
}