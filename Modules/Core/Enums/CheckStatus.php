<?php

namespace Modules\Core\Enums;

final class CheckStatus extends Base
{
    const PENDING = 1;
    const TRANSFERRED = 2;
}