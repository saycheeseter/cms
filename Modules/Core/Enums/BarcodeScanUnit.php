<?php

namespace Modules\Core\Enums;

final class BarcodeScanUnit extends Base
{
    const BARCODE_UNIT = 1;
    const DEFAULT_UNIT = 2;
}