<?php

namespace Modules\Core\Enums;

use Lang;

final class TransactionSummary extends Base 
{
    CONST PURCHASE_INBOUND = 1;
    CONST STOCK_DELIVERY_INBOUND = 2;
    CONST STOCK_RETURN_INBOUND = 3;
    CONST SALES_RETURN_INBOUND = 4;
    CONST PURCHASE_RETURN_OUTBOUND = 5;
    CONST STOCK_DELIVERY_OUTBOUND = 6;
    CONST STOCK_RETURN_OUTBOUND = 7;
    CONST SALES_OUTBOUND = 8;
    CONST DAMAGE_OUTBOUND = 9;
    const INVENTORY_ADJUST = 10;
    const POS_SALES = 11;
    const PRODUCT_CONVERSION = 12;
    const AUTO_PRODUCT_CONVERSION = 13;

    protected function __PURCHASE_INBOUND()
    {
        $this->description = Lang::get('core::label.purchase.inbound');
    }

    protected function __STOCK_DELIVERY_INBOUND()
    {
        $this->description = Lang::get('core::label.stock.delivery.inbound');
    }

    protected function __STOCK_RETURN_INBOUND()
    {
        $this->description = Lang::get('core::label.stock.return.inbound');
    }

    protected function __SALES_RETURN_INBOUND()
    {
        $this->description = Lang::get('core::label.sales.return.inbound');
    }

    protected function __PURCHASE_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.purchase.return.outbound');
    }

    protected function __STOCK_DELIVERY_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.delivery.outbound');
    }

    protected function __STOCK_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.return.outbound');
    }

    protected function __SALES_OUTBOUND()
    {
        $this->description = Lang::get('core::label.sales.outbound');
    }

    protected function __DAMAGE_OUTBOUND()
    {
        $this->description = Lang::get('core::label.damage.outbound');
    }

    protected function __INVENTORY_ADJUST()
    {
        $this->description = Lang::get('core::label.inventory.adjust');
    }

    protected function __POS_SALES()
    {
        $this->description = Lang::get('core::label.pos.sales');
    }

    protected function __PRODUCT_CONVERSION()
    {
        $this->description = Lang::get('core::label.product.conversion');
    }

    protected function __AUTO_PRODUCT_CONVERSION()
    {
        $this->description = Lang::get('core::label.automatic.product.conversion');
    }
}