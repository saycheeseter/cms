<?php

namespace Modules\Core\Enums;

final class TransactionSummaryComputation extends Base
{
    const AUTOMATIC = 1;
    const MANUAL = 2;
}