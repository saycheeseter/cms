<?php

namespace Modules\Core\Enums;

use Lang;

final class DataCollectorTransactionType extends Base 
{
    const PURCHASE_INBOUND = 1;
    const PURCHASE_RETURN_OUTBOUND = 2;
    const DAMAGE_OUTBOUND = 3;
    const SALES_OUTBOUND = 4;
    const SALES_RETURN_INBOUND = 5;
    const STOCK_DELIVERY_OUTBOUND = 6;
    const STOCK_RETURN_OUTBOUND = 7;
    const INVENTORY_ADJUST = 8;

    protected function __PURCHASE_INBOUND()
    {
        $this->description = Lang::get('core::label.purchase.inbound');
    }

    protected function __PURCHASE_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.purchase.return.outbound');
    }

    protected function __DAMAGE_OUTBOUND()
    {
        $this->description = Lang::get('core::label.damage.outbound');
    }

    protected function __SALES_OUTBOUND()
    {
        $this->description = Lang::get('core::label.sales.outbound');
    }

    protected function __SALES_RETURN_INBOUND()
    {
        $this->description = Lang::get('core::label.sales.return.inbound');
    }

    protected function __STOCK_DELIVERY_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.delivery.outbound');
    }

    protected function __STOCK_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.return.outbound');
    }

    protected function __INVENTORY_ADJUST()
    {
        $this->description = Lang::get('core::label.inventory.adjust');
    }
}