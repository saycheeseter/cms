<?php

namespace Modules\Core\Enums;

final class InventoryStatus extends Base
{
    const NEGATIVE = 0;
    const NO_INVENTORY = 1;
    const WITH_INVENTORY = 2;
}


