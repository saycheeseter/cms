<?php

namespace Modules\Core\Enums;

final class BatchStatus extends Base 
{
    const NOT_AVAILABLE = 0;
    const AVAILABLE = 1;
}