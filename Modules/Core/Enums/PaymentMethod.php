<?php

namespace Modules\Core\Enums;

use Lang;

final class PaymentMethod extends Base 
{   
    const CASH = 2;
    const CHECK = 3;
    const BANK_DEPOSIT = 4;
    const FROM_BALANCE = 5;
    const CREDIT_CARD = 6;
    const DEBIT_CARD = 7;

    static public function all()
    {
        return [
            PaymentMethod::CASH,
            PaymentMethod::CHECK,
            PaymentMethod::BANK_DEPOSIT,
            PaymentMethod::FROM_BALANCE,
            PaymentMethod::CREDIT_CARD,
            PaymentMethod::DEBIT_CARD,
        ];
    }

    static public function default()
    {
        return [
            PaymentMethod::CASH,
            PaymentMethod::CHECK,
            PaymentMethod::BANK_DEPOSIT,
            //PaymentMethod::FROM_BALANCE,
        ];
    }

    static public function walkIn()
    {
        return [
            PaymentMethod::CASH,
            PaymentMethod::CHECK,
            PaymentMethod::BANK_DEPOSIT,
        ];
    }

    protected function __CASH()
    {
        $this->description = Lang::get('core::label.cash');
    }

    protected function __CHECK()
    {
        $this->description = Lang::get('core::label.check');
    }

    protected function __BANK_DEPOSIT()
    {
        $this->description = Lang::get('core::label.bank.deposit');
    }

    protected function __FROM_BALANCE()
    {
        $this->description = Lang::get('core::label.from.balance');
    }

    protected function __CREDIT_CARD()
    {
        $this->description = Lang::get('core::label.credit.card');
    }

    protected function __DEBIT_CARD()
    {
        $this->description = Lang::get('core::label.debit.card');
    }
}