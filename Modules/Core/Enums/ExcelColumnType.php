<?php

namespace Modules\Core\Enums;

final class ExcelColumnType extends Base
{
    const TEXT = 1;
    const NUMERIC = 2;
}


