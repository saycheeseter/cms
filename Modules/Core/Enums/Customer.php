<?php

namespace Modules\Core\Enums;

use Lang;

final class Customer extends Base
{
    const WALK_IN = 0;
    const REGULAR = 1;

    protected function __WALK_IN()
    {
        $this->description = Lang::get('core::label.walk.in.customer');
    }

    protected function __REGULAR()
    {
        $this->description = Lang::get('core::label.regular');
    }
}


