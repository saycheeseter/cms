<?php

namespace Modules\Core\Enums;

use Lang;

final class CustomerPricingType extends Base
{
    const HISTORICAL = 1;
    const WHOLESALE = 2;
    const RETAIL = 3;
    const PERCENT_PURCHASE = 4;
    const PERCENT_WHOLESALE = 5;
    const PRICE_A = 6;
    const PRICE_B = 7;
    const PRICE_C = 8;
    const PRICE_D = 9;
    const PRICE_E = 10;

    protected function __HISTORICAL()
    {
        $this->description = Lang::get('core::label.historical.record');
    }

    protected function __WHOLESALE()
    {
        $this->description = Lang::get('core::label.wholesale.price');
    }

    protected function __RETAIL()
    {
        $this->description = Lang::get('core::label.retail.price');
    }

    protected function __PERCENT_PURCHASE()
    {
        $this->description = Lang::get('core::label.percent.purchase');
    }

    protected function __PERCENT_WHOLESALE()
    {
        $this->description = Lang::get('core::label.percent.wholesale');
    }

    protected function __PRICE_A()
    {
        $this->description = Lang::get('core::label.price.a');
    }

    protected function __PRICE_B()
    {
        $this->description = Lang::get('core::label.price.b');
    }

    protected function __PRICE_C()
    {
        $this->description = Lang::get('core::label.price.c');
    }

    protected function __PRICE_D()
    {
        $this->description = Lang::get('core::label.price.d');
    }

    protected function __PRICE_E()
    {
        $this->description = Lang::get('core::label.price.e');
    }
}


