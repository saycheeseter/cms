<?php

namespace Modules\Core\Enums;

final class SystemCodeType extends Base
{
    const BRAND = 1;
    const UOM = 2;
    const BANK = 3;
    const PAYMENT_METHOD = 4;
    const REASON = 5;
    const PAYMENT_TYPE = 6;
    const INCOME_TYPE = 7;
}


