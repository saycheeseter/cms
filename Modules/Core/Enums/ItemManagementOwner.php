<?php

namespace Modules\Core\Enums;

final class ItemManagementOwner extends Base 
{
    const SUPPLIER = 1;
    const CUSTOMER = 2;
    const BRANCH_DETAIL = 3;
}