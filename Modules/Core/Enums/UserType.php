<?php

namespace Modules\Core\Enums;

final class UserType extends Base 
{
    const SUPERADMIN = 0;
    const ADMIN = 1;
    const DEFAULT = 2;
}


