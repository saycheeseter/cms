<?php

namespace Modules\Core\Enums;

use Lang;

final class UserDefinedFieldType extends Base 
{   
    const STR = 1;
    const INTR = 2;
    const DECIMAL = 3;
    const DATE = 4;
    const DATETIME = 5;
}