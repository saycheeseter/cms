<?php

namespace Modules\Core\Enums;

use Lang;

final class AuditEvent extends Base 
{   
    const CREATED = 'created';
    const UPDATED = 'updated';
    const DELETED = 'deleted';

    protected function __CREATED()
    {
        $this->description = Lang::get('core::label.create');
    }

    protected function __UPDATED()
    {
        $this->description = Lang::get('core::label.update');
    }

    protected function __DELETED()
    {
        $this->description = Lang::get('core::label.delete');
    }
}