<?php

namespace Modules\Core\Enums;

final class ProductManageType extends Base 
{
    const NONE = 0;
    const SERIAL = 1;
    const BATCH = 2;
}