<?php

namespace Modules\Core\Enums;

final class InventoryWarning extends Base
{
    const MIN = 1;
    const MAX = 2;
}