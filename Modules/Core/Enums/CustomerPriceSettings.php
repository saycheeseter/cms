<?php

namespace Modules\Core\Enums;

use Lang;

final class CustomerPriceSettings extends Base 
{
    const CUSTOM = 0;
    const WHOLESALE = 1;
    const RETAIL = 2;
}