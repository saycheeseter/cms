<?php

namespace Modules\Core\Enums;

use Lang;

final class TransactionStatus extends Base 
{
    const COMPLETE = 1;
    const INCOMPLETE = 2;
    const EXCESS = 3;
    const NO_DELIVERY = 4;

    protected function __COMPLETE()
    {
        $this->description = Lang::get('core::label.complete');
    }

    protected function __INCOMPLETE()
    {
        $this->description = Lang::get('core::label.incomplete');
    }

    protected function __EXCESS()
    {
        $this->description = Lang::get('core::label.excess');
    }

    protected function __NO_DELIVERY()
    {
        $this->description = Lang::get('core::label.no.delivery');
    }
}