<?php

namespace Modules\Core\Enums;

use Lang;

final class ProductType extends Base 
{
    const REGULAR = 1;
    const GROUP = 2;
    const COMPONENT = 3;

    protected function __REGULAR()
    {
        $this->description = Lang::get('core::label.regular');
    }

    protected function __GROUP()
    {
        $this->description = Lang::get('core::label.group');
    }

    protected function __COMPONENT()
    {
        $this->description = Lang::get('core::label.component');
    }
}