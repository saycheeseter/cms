<?php

namespace Modules\Core\Enums;

use Lang;

final class ModuleDefinedFields extends Base 
{   
    const PURCHASE = 1;
    const PURCHASE_RETURN = 2;
    const DAMAGE = 3;
    const SALES = 4;
    const SALES_RETURN = 5;
    const STOCK_REQUEST = 6;
    const STOCK_DELIVERY = 7;
    const STOCK_RETURN = 8;
    const PURCHASE_INBOUND = 9;
    const PURCHASE_RETURN_OUTBOUND = 10;
    const DAMAGE_OUTBOUND = 11;
    const SALES_OUTBOUND = 12;
    const SALES_RETURN_INBOUND = 13;
    const STOCK_DELIVERY_OUTBOUND = 14;
    const STOCK_DELIVERY_INBOUND = 15;
    const STOCK_RETURN_OUTBOUND = 16;
    const STOCK_RETURN_INBOUND = 17;
    const INVENTORY_ADJUST = 18;
    const PRODUCT = 19;
    const COUNTER = 20;
    const PAYMENT = 21;
    const USER = 22;
    const COLLECTION = 23;

    protected $model = '';

    public function getModel()
    {
        return $this->model;
    }

    protected function __PURCHASE()
    {
        $this->model = \Modules\Core\Entities\Purchase::class;
        $this->description = Lang::get('core::label.purchase.order');
    }

    protected function __PURCHASE_RETURN()
    {
        $this->model = \Modules\Core\Entities\PurchaseReturn::class;
        $this->description = Lang::get('core::label.purchase.return');
    }

    protected function __DAMAGE()
    {
        $this->model = \Modules\Core\Entities\Damage::class;
        $this->description = Lang::get('core::label.damage');
    }

    protected function __SALES()
    {
        $this->model = \Modules\Core\Entities\Sales::class;
        $this->description = Lang::get('core::label.sales');
    }

    protected function __SALES_RETURN()
    {
        $this->model = \Modules\Core\Entities\SalesReturn::class;
        $this->description = Lang::get('core::label.sales.return');
    }

    protected function __STOCK_REQUEST()
    {
        $this->model = \Modules\Core\Entities\StockRequest::class;
        $this->description = Lang::get('core::label.stock.request');
    }

    protected function __STOCK_DELIVERY()
    {
        $this->model = \Modules\Core\Entities\StockDelivery::class;
        $this->description = Lang::get('core::label.stock.delivery');
    }

    protected function __STOCK_RETURN()
    {
        $this->model = \Modules\Core\Entities\StockReturn::class;
        $this->description = Lang::get('core::label.stock.return');
    }

    protected function __PURCHASE_INBOUND()
    {
        $this->model = \Modules\Core\Entities\PurchaseInbound::class;
        $this->description = Lang::get('core::label.purchase.inbound');
    }

    protected function __PURCHASE_RETURN_OUTBOUND()
    {
        $this->model = \Modules\Core\Entities\PurchaseReturnOutbound::class;
        $this->description = Lang::get('core::label.purchase.return.outbound');
    }

    protected function __DAMAGE_OUTBOUND()
    {
        $this->model = \Modules\Core\Entities\DamageOutbound::class;
        $this->description = Lang::get('core::label.damage.outbound');
    }

    protected function __SALES_OUTBOUND()
    {
        $this->model = \Modules\Core\Entities\SalesOutbound::class;
        $this->description = Lang::get('core::label.sales.outbound');
    }

    protected function __SALES_RETURN_INBOUND()
    {
        $this->model = \Modules\Core\Entities\SalesReturnInbound::class;
        $this->description = Lang::get('core::label.sales.return.inbound');
    }

    protected function __STOCK_DELIVERY_OUTBOUND()
    {
        $this->model = \Modules\Core\Entities\StockDeliveryOutbound::class;
        $this->description = Lang::get('core::label.stock.delivery.outbound');
    }

    protected function __STOCK_DELIVERY_INBOUND()
    {
        $this->model = \Modules\Core\Entities\StockDeliveryInbound::class;
        $this->description = Lang::get('core::label.stock.delivery.inbound');
    }

    protected function __STOCK_RETURN_OUTBOUND()
    {
        $this->model = \Modules\Core\Entities\StockReturnOutbound::class;
        $this->description = Lang::get('core::label.stock.return.outbound');
    }

    protected function __STOCK_RETURN_INBOUND()
    {
        $this->model = \Modules\Core\Entities\StockReturnInbound::class;
        $this->description = Lang::get('core::label.stock.return.inbound');
    }

    protected function __INVENTORY_ADJUST()
    {
        $this->model = \Modules\Core\Entities\InventoryAdjust::class;
        $this->description = Lang::get('core::label.inventory.adjust');
    }

    protected function __PRODUCT()
    {
        $this->model = \Modules\Core\Entities\Product::class;
        $this->description = Lang::get('core::label.product');
    }

    protected function __COUNTER()
    {
        $this->model = \Modules\Core\Entities\Counter::class;
        $this->description = Lang::get('core::label.counter');
    }
}