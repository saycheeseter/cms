<?php

namespace Modules\Core\Enums;

use Lang;

final class TargetType extends Base
{
    const ALL_CUSTOMERS = 1;
    const ALL_MEMBERS = 2;
    const SPECIFIC_MEMBER_TYPE = 3;
    const SPECIFIC_MEMBER = 4;
    const SPECIFIC_CUSTOMER = 5;

    protected function __ALL_CUSTOMERS()
    {
        $this->description = Lang::get('core::label.all.customers');
    }

    protected function __ALL_MEMBERS()
    {
        $this->description = Lang::get('core::label.all.members');
    }

    protected function __SPECIFIC_MEMBER_TYPE()
    {
        $this->description = Lang::get('core::label.specific.member.rate');
    }

    protected function __SPECIFIC_MEMBER()
    {
        $this->description = Lang::get('core::label.specific.member');
    }
    
    protected function __SPECIFIC_CUSTOMER()
    {
        $this->description = Lang::get('core::label.specific.customer');
    }

    public static function all() 
    {
        return [
            self::ALL_CUSTOMERS,
            self::ALL_MEMBERS
        ];
    }
}