<?php

namespace Modules\Core\Enums;

use Lang;

final class GiftCheckStatus extends Base
{
    const AVAILABLE = 1;
    const REDEEMED = 2;

    protected function __AVAILABLE()
    {
        $this->description = Lang::get('core::label.available');
    }

    protected function __REDEEMED()
    {
        $this->description = Lang::get('core::label.redeemed');
    }
}


