<?php

namespace Modules\Core\Enums;

final class BranchPrice extends Base 
{
    const PURCHASE = 1;
    const SELLING = 2;
    const WHOLESALE = 3;
    const A = 4;
    const B = 5;
    const C = 6;
    const D = 7;
    const E = 8;
}


