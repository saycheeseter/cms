<?php

namespace Modules\Core\Enums;

use Lang;

final class SelectType extends Base
{
    const ALL_PRODUCTS = 1;
    const SPECIFIC_PRODUCT = 2;
    const SPECIFIC_BRAND = 3;
    const SPECIFIC_CATEGORY = 4;
    const SPECIFIC_SUPPLIER = 5;

    protected function __ALL_PRODUCTS()
    {
        $this->description = Lang::get('core::label.all.products');
    }

    protected function __SPECIFIC_PRODUCT()
    {
        $this->description = Lang::get('core::label.specific.product');
    }

    protected function __SPECIFIC_BRAND()
    {
        $this->description = Lang::get('core::label.specific.brand');
    }

    protected function __SPECIFIC_CATEGORY()
    {
        $this->description = Lang::get('core::label.specific.category');
    }
    
    protected function __SPECIFIC_SUPPLIER()
    {
        $this->description = Lang::get('core::label.specific.supplier');
    }
}