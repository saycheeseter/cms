<?php

namespace Modules\Core\Enums;

final class ProductStatus extends Base
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const DISCONTINUED = 2;
}


