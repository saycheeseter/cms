<?php

namespace Modules\Core\Enums;

use Lang;

final class PriceScheme extends Base
{
    const DEFAULT = 0;
    const CUSTOMER = 1;
    const SUPPLIER = 2;
    const INVENTORY = 3;
}


