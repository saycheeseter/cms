<?php

namespace Modules\Core\Enums;

use Lang;

final class ModuleGroup extends Base 
{   
    const PURCHASE = 1;
    const PURCHASE_RETURN = 2;
    const DAMAGE = 3;
    const SALES = 4;
    const SALES_RETURN = 5;
    const STOCK_REQUEST = 6;
    const STOCK_DELIVERY = 7;
    const STOCK_RETURN = 8;
    const PURCHASE_INBOUND = 9;
    const PURCHASE_RETURN_OUTBOUND = 10;
    const DAMAGE_OUTBOUND = 11;
    const SALES_OUTBOUND = 12;
    const SALES_RETURN_INBOUND = 13;
    const STOCK_DELIVERY_OUTBOUND = 14;
    const STOCK_DELIVERY_INBOUND = 15;
    const STOCK_RETURN_OUTBOUND = 16;
    const STOCK_RETURN_INBOUND = 17;
    const INVENTORY_ADJUST = 18;
    const PRODUCT = 19;
    const COUNTER = 20;
    const PAYMENT = 21;
    const USER = 22;
    const COLLECTION = 23;
    const PRODUCT_CONVERSION = 24;
    const STOCK_DELIVERY_OUTBOUND_FROM = 25;
    const STOCK_REQUEST_FROM = 26;
    const BRANCH = 27;
    const SUPPLIER = 28;
    const CUSTOMER = 29;
    const MEMBER = 30;
    const PRODUCT_DISCOUNT = 31;
    const RECEIVED_CHECK = 32;
    const ISSUED_CHECK = 33;
    const STOCK_CARD = 34;
    const POS = 35;

    protected function __PURCHASE()
    {
        $this->description = Lang::get('core::label.purchase.order');
    }

    protected function __PURCHASE_RETURN()
    {
        $this->description = Lang::get('core::label.purchase.return');
    }

    protected function __DAMAGE()
    {
        $this->description = Lang::get('core::label.damage');
    }

    protected function __SALES()
    {
        $this->description = Lang::get('core::label.sales');
    }

    protected function __SALES_RETURN()
    {
        $this->description = Lang::get('core::label.sales.return');
    }

    protected function __STOCK_REQUEST()
    {
        $this->description = Lang::get('core::label.stock.request');
    }

    protected function __STOCK_DELIVERY()
    {
        $this->description = Lang::get('core::label.stock.delivery');
    }

    protected function __STOCK_RETURN()
    {
        $this->description = Lang::get('core::label.stock.return');
    }

    protected function __PURCHASE_INBOUND()
    {
        $this->description = Lang::get('core::label.purchase.inbound');
    }

    protected function __PURCHASE_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.purchase.return.outbound');
    }

    protected function __DAMAGE_OUTBOUND()
    {
        $this->description = Lang::get('core::label.damage.outbound');
    }

    protected function __SALES_OUTBOUND()
    {
        $this->description = Lang::get('core::label.sales.outbound');
    }

    protected function __SALES_RETURN_INBOUND()
    {
        $this->description = Lang::get('core::label.sales.return.inbound');
    }

    protected function __STOCK_DELIVERY_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.delivery.outbound');
    }

    protected function __STOCK_DELIVERY_INBOUND()
    {
        $this->description = Lang::get('core::label.stock.delivery.inbound');
    }

    protected function __STOCK_RETURN_OUTBOUND()
    {
        $this->description = Lang::get('core::label.stock.return.outbound');
    }

    protected function __STOCK_RETURN_INBOUND()
    {
        $this->description = Lang::get('core::label.stock.return.inbound');
    }

    protected function __INVENTORY_ADJUST()
    {
        $this->description = Lang::get('core::label.inventory.adjust');
    }

    protected function __PRODUCT()
    {
        $this->description = Lang::get('core::label.product');
    }

    protected function __COUNTER()
    {
        $this->description = Lang::get('core::label.counter');
    }

    protected function __PAYMENT()
    {
        $this->description = Lang::get('core::label.payment');
    }

    protected function __USER()
    {
        $this->description = Lang::get('core::label.user');
    }

    protected function __COLLECTION()
    {
        $this->description = Lang::get('core::label.collection');
    }

    protected function __PRODUCT_CONVERSION()
    {
        $this->description = Lang::get('core::label.product.conversion');
    }

    protected function __STOCK_DELIVERY_OUTBOUND_FROM()
    {
        $this->description = Lang::get('core::label.stock.delivery.outbound.from');
    }

    protected function __STOCK_REQUEST_FROM()
    {
        $this->description = Lang::get('core::label.stock.request.from');
    }

    protected function __BRANCH()
    {
        $this->description = Lang::get('core::label.branch');
    }

    protected function __SUPPLIER()
    {
        $this->description = Lang::get('core::label.supplier');
    }

    protected function __CUSTOMER()
    {
        $this->description = Lang::get('core::label.customer');
    }

    protected function __MEMBER()
    {
        $this->description = Lang::get('core::label.member');
    }

    protected function __PRODUCT_DISCOUNT()
    {
        $this->description = Lang::get('core::label.product.discount');
    }

    protected function __RECEIVED_CHECK()
    {
        $this->description = Lang::get('core::label.received.check');
    }

    protected function __ISSUED_CHECK()
    {
        $this->description = Lang::get('core::label.issued.check');
    }

    protected function __STOCK_CARD()
    {
        $this->description = Lang::get('core::label.stock.card');
    }

    protected function __POS()
    {
        $this->description = Lang::get('core::label.pos.sales');
    }
}