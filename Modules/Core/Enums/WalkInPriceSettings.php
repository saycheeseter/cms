<?php

namespace Modules\Core\Enums;

final class WalkInPriceSettings extends Base 
{
    const WHOLESALE = 0;
    const RETAIL = 1;
    const PRICE_A = 2;
    const PRICE_B = 3;
    const PRICE_C = 4;
    const PRICE_D = 5;
    const PRICE_E = 6;
}