<?php

namespace Modules\Core\Enums;

use Lang;

final class TransactionType extends Base
{
    const DIRECT = 0;
    const FROM_INVOICE = 1;

    protected function __DIRECT()
    {
        $this->description = Lang::get('core::label.direct');
    }

    protected function __FROM_INVOICE()
    {
        $this->description = Lang::get('core::label.from.invoice');
    }
}
