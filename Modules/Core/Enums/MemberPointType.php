<?php

namespace Modules\Core\Enums;

use Lang;

final class MemberPointType extends Base 
{
    const SALES_USE_POINTS = 1;
    const SALES_EARN_POINTS = 2;
    const LOAD_POINTS = 3;
}