<?php

namespace Modules\Core\Enums;

use Lang;

final class ApprovalStatus extends Base 
{   
    const DRAFT = 1;
    const FOR_APPROVAL = 2;
    const APPROVED = 3;
    const DECLINED = 4;

    protected function __DRAFT()
    {
        $this->description = Lang::get('core::label.draft');
    }

    protected function __FOR_APPROVAL()
    {
        $this->description = Lang::get('core::label.for.approval');
    }

    protected function __APPROVED()
    {
        $this->description = Lang::get('core::label.approved');
    }

    protected function __DECLINED()
    {
        $this->description = Lang::get('core::label.declined');
    }
}