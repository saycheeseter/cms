<?php

namespace Modules\Core\Enums;

use Lang;

final class DiscountScheme extends Base
{
    const SIMPLE = 1;
    const BUY_X_FOR_PRICE_Y = 2;
    const BUY_X_FOR_FIXED_PRICE_Y = 3;

    protected function __SIMPLE()
    {
        $this->description = Lang::get('core::label.simple');
    }

    protected function __BUY_X_FOR_PRICE_Y()
    {
        $this->description = Lang::get('core::info.buy.x.for.price.y');
    }

    protected function __BUY_X_FOR_FIXED_PRICE_Y()
    {
        $this->description = Lang::get('core::info.buy.x.for.fixed.price.y');
    }
}