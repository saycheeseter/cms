<?php

namespace Modules\Core\Enums;

use Artisaninweb\Enum\Enum;

abstract class Base extends Enum 
{
    protected $description;

    public function getValue()
    {
        return $this->getConstValue();
    }

    public function getDescription()
    {
        return $this->description;
    }
}


