<?php

namespace Modules\Core\Enums;

use Lang;

final class SerialTransaction extends Base 
{
    const PURCHASE_INBOUND = 1;
    const PURCHASE_RETURN_OUTBOUND = 2;
    const DAMAGE_OUTBOUND = 3;
    const SALES_OUTBOUND = 4;
    const SALES_RETURN_INBOUND = 5;
    const STOCK_DELIVERY_OUTBOUND = 6;
    const STOCK_DELIVERY_INBOUND = 7;
    const STOCK_RETURN_OUTBOUND = 8;
    const STOCK_RETURN_INBOUND = 9;
    const INVENTORY_ADJUST_INCREASE = 10;
    const INVENTORY_ADJUST_DECREASE = 11;
}