<?php

namespace Modules\Core\Enums;

use Lang;

final class GroupType extends Base 
{
    const NONE = 1;
    const MANUAL_GROUP = 2;
    const MANUAL_UNGROUP = 3;
    const AUTOMATIC_GROUP = 4;
    const AUTOMATIC_UNGROUP = 5;

    protected function __NONE()
    {
        $this->description = Lang::get('core::label.none');
    }

    protected function __MANUAL_GROUP()
    {
        $this->description = Lang::get('core::label.manual.group');
    }

    protected function __MANUAL_UNGROUP()
    {
        $this->description = Lang::get('core::label.manual.ungroup');
    }

    protected function __AUTOMATIC_GROUP()
    {
        $this->description = Lang::get('core::label.automatic.group');
    }

    protected function __AUTOMATIC_UNGROUP()
    {
        $this->description = Lang::get('core::label.automatic.ungroup');
    }
}