<?php

namespace Modules\Core\Enums;

use Lang;

final class Manageable extends Base 
{
    const NO = 0;
    const YES = 1;

    public function __NO()
    {
        $this->description = Lang::get('core::label.no');
    }

    public function __YES()
    {
        $this->description = Lang::get('core::label.yes');
    }
}


