<?php

namespace Modules\Core\Enums;

final class PosSummaryPostingStatus extends Base
{
    const IDLE = 0;
    const GENERATING = 1;
}