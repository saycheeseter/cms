<?php

namespace Modules\Core\Enums;

use Lang;

final class PurchaseInboundAutoUpdateCostSettings extends Base
{   
    const NO = 0;
    const OWN_BRANCH = 1;
    const ALL_BRANCHES = 2;
    const COPIED_TO_BRANCH = 3;
}