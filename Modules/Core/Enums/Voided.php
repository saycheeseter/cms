<?php

namespace Modules\Core\Enums;

use Lang;

final class Voided extends Base 
{
    const YES = 1;
    const NO = 0;

    public function __YES()
    {
        $this->description = Lang::get('core::label.yes');
    }

    public function __NO()
    {
        $this->description = Lang::get('core::label.no');
    }
}


