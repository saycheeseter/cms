<?php

namespace Modules\Core\Validations;

use Litipk\BigNumbers\Decimal;
use Illuminate\Validation\Rule;
use Modules\Core\Entities\ProductSerialNumber;
use Validator;

class SerialNumberValidator
{
    /**
     * Count validation of serial number encoded versus product total quantity per detail
     * Rule format (serial_count)
     * 
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function count($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();
        
        list($details, $row, $serials) = explode('.',  $attribute);

        $detail = $data[$details][$row];
        
        $precision = setting('monetary.precision');

        $total = Decimal::create(1, $precision);

        for ($i=0; $i < count($parameters); $i++) {
            $breakdown = explode('|', $parameters[$i]);

            $method = $breakdown[1] ?? 'mul';

            $total = $total->$method(Decimal::create($detail[$breakdown[0]], $precision));
        }

        $total = $total->abs();

        return !(count($value) > 0 && !$total->ceil()->equals(Decimal::create(count($value), $precision)));
    }

    /**
     * Unique validation of encoded serial numbers
     * eg. serial_unique
     * 
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function unique($attribute, $value, $parameters, $validator)
    {   
        $data = $validator->getData();

        list($details, $row, $serials, $index) = explode('.',  $attribute);

        $id = $data[$details][$row][$serials][$index]['id'] ?? 0;

        $count = ProductSerialNumber::where('serial_number', $value)
            ->where('status', '>', 0)
            ->where('id', '<>', $id)
            ->count();

        return !($count > 0);
    }

    /**
     * Unique validation of passed set of serials per transaction detail
     * Rule format (serial_unique_in_array)
     *
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function uniqueInArray($attribute, $value, $parameters, $validator)
    {   
        $numbers = [];

        $data = $validator->getData();

        $plucked = array_pluck($data['details'], 'serials.*.serial_number');

        $serials = array_flatten($plucked);

        return !(in_array($value, $serials) && array_count_values($serials)[$value] > 1);
    }
}
