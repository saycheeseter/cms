<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;
use Validator;

class ArrayUniqueValidator
{
    /**
     * Unique validation for array attributes eg.details.created.0.code
     * Rule format (array_unique:table,table_column,soft_delete_column,id_column)
     *
     * eg. array_unique:branch,code,deleted_at,id
     * 
     * @param  string $attribute  
     * @param  string $value      
     * @param  array $parameters 
     * @param  Validator $validator  
     * @return bool             
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $data = array_dot($validator->getData());
        $path = $this->getPath($attribute);

        $table = $parameters[0];
        $column = $parameters[1];
        $softDeletedColumn = !isset($parameters[2]) || empty($parameters[2])
            ? null
            : $parameters[2];

        $id = $parameters[3] ?? null;

        $rule = Rule::unique($table, $column);

        if (!is_null($softDeletedColumn)) {
            $rule->where(function ($query) use ($softDeletedColumn){
                $query->whereNull($softDeletedColumn);
            });
        }

        if (!is_null($id)) {
            $rule->ignore($data[$path.'.'.$id], $id);
        }

        $validation = Validator::make(array($column => $value), [
            $column => $rule
        ]);

        return $validation->passes();
    }

    /**
     * Extract the path needed to traverse the current index of an array
     * 
     * @param  string $attribute
     * @return string           
     */
    private function getPath($attribute)
    {
        // eg. details.created.0.code
        $attribute = explode('.', $attribute);
        $path = '';

        // Do not include the last index which is the column
        for ($i=0; $i < (count($attribute) - 1); $i++) { 
            $path .= '.'.$attribute[$i];
        }

        return substr($path, 1);
    }
}