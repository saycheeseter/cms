<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;

use Modules\Core\Entities\Product;

use Validator;
use App;

class UomValidator
{
    /**
     * Check wether if the given uom already exists in the array of data
     * 
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();

        list($table, $row, $column) = explode('.',  $attribute);

        $uoms = array_pluck($data[$table], 'uom_id');

        $counts = array_count_values($uoms);

        return !($counts[$value] >= 2);
    }
}
