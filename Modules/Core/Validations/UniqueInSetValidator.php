<?php

namespace Modules\Core\Validations;

class UniqueInSetValidator
{
    /**
     * Validate current attribute's value if it's already existing 
     * inside the data passed in the request
     *
     * eg. unique_in_set:detals.created,details.updated
     * 
     * @param  string $attribute 
     * @param  string $value     
     * @param  array $parameters
     * @param  Validator $validator 
     * @return bool            
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();
        $attribute = explode('.', $attribute);
        $field = $attribute[(count($attribute) - 1)];
        $detach = '';

        // Build string path for the data to be detached in array
        for ($i=0; $i < (count($attribute) - 1); $i++) { 
            $detach.= '.'.$attribute[$i];
        }

        array_forget($data, substr($detach, 1));

        // Check each parameter path if value exist in each set
        foreach ($parameters as $key => $path) {
            if (is_null(array_get($data, $path))) {
                continue;
            }

            if ($this->valueExist(
                $value, 
                $field, 
                array_get($data, $path)
            )) {
                return false;
            }
        }

        return true;
    }
    
    /**
     * Extract current field in a separate array and check if the value
     * exist in the data set
     * 
     * @param  string $value
     * @param  string $field
     * @param  array $data 
     * @return bool       
     */
    private function valueExist($value, $field, $data)
    {
        $data = array_pluck($data, $field);

        if (in_array($value, $data)) {
            return true;
        }
        
        return false;
    }
}