<?php

namespace Modules\Core\Validations;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class DateAfterValidator
{
    /**
     * Checks whether the date value is after the specified date
     * Rule format (date_after:value or field in set of data)
     * 
     * eg. date_after:2016-02-123
     * eg. date_afte:registration_date
     * 
     * @param  string $attribute  
     * @param  string $value      
     * @param  array $parameters 
     * @param  Validator $validator  
     * @return bool  
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $rules = $validator->getRules();
        $data = [$attribute => $value];
        $parameter = $parameters[0];

        // If not a valid date, should not validate further the attribute
        if (!$this->assertValidDate($data, $attribute, $rules)) {
            return true;
        }

        $parameter = $this->parse($parameter, $rules);

        $validation = Validator::make($data, [
            $attribute => 'after:'.$parameter
        ]);

        return $validation->passes();
    }

    /**
     * Checks whether the given attribute is a proper date, if date or date_format
     * validation exist in rules, attribute will be validated based on that rule
     * 
     * @param  array $data     
     * @param  string $attribute
     * @param  array  $rules    
     * @return bool           
     */
    private function assertValidDate($data, $attribute, $rules = [])
    {
        $rule = null;

        // Check each rule of the attribute
        foreach ($rules[$attribute] as $key => $temp) {
            // If attribute has a date or date_format rule, validate the data 
            // using that rule
            if (Str::contains('date', $temp)) {
                $rule = $temp;
                break;
            }
        }

        $validation = Validator::make($data, [
            $attribute => $rule = $rule ?? 'date'
        ]);

        return $validation->passes();
    }

    /**
     * Parse the given parameter, if given parameter is a string/column of a given data,
     * get the value on the request bag.
     * 
     * @param  string $parameter
     * @param  array $rules    
     * @return string           
     */
    private function parse($parameter, $rules)
    {
        // If parameter was a column and not a date
        if (array_key_exists($parameter, $rules)) {
            $parameter = Request::get($parameter);
        }

        return $parameter;
    }
}