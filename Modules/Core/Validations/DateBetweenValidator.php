<?php

namespace Modules\Core\Validations;
use Carbon\Carbon;

class DateBetweenValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $dateFrom = new Carbon($parameters[0]);
        $dateTo = new Carbon($parameters[1]);
        $value = new Carbon($value);

        return !$value->between($dateFrom, $dateTo);
    }
}