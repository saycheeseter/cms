<?php

namespace Modules\Core\Validations;

use Litipk\BigNumbers\Decimal;

class DecimalValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $precision = setting('monetary.precision');

        $zero = Decimal::create(0, $precision);

        if (!is_numeric($value)) {
            return true;
        }

        return Decimal::create($value, $precision)->isGreaterThan($zero);
    }
}