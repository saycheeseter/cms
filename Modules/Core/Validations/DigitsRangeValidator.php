<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;
use Validator;

class DigitsRangeValidator
{
    /**
     * Checks whether the value is within the given range of number
     * 
     * eg. digits_range:1,10
     * 
     * @param  string $attribute  
     * @param  string $value      
     * @param  array $parameters 
     * @param  Validator $validator  
     * @return bool             
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $min = (int) $parameters[0];
        $max = (int) $parameters[1];
        $value = (int) $value;

        return $value >= $min && $value <= $max;
    }
}