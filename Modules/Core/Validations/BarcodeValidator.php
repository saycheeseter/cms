<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;

use Modules\Core\Entities\Product;
use Modules\Core\Entities\ProductBarcode;

use Validator;
use App;

class BarcodeValidator
{
    /**
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();

        list($table, $row, $column) = explode('.',  $attribute);

        $productId = $parameters[0] ?? 0;

        //validation of duplicate entry of barcode on the sent array
        $codes = array_pluck($data[$table], 'code');

        $counts = array_count_values($codes);

        if ($counts[$value] >= 2) {
            return false;
        }

        //database level validation of unique barcodes
        $products = Product::whereDoesntHave('barcodes', function($query) use ($productId, $value){
            $query->where('code', '=', $value)->where('product_id', '=', $productId);
        })->whereHas('barcodes', function($query) use ($productId, $value){
            $query->where('code', '=', $value);
        })->get();

        if($products->count() > 0){
            return false;
        }

        return true;
    }
}
