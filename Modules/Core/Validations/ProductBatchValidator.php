<?php

namespace Modules\Core\Validations;

use Litipk\BigNumbers\Decimal;
use Illuminate\Validation\Rule;
use Modules\Core\Entities\ProductBatch;
use Validator;

class ProductBatchValidator
{
    /**
     * Count validation of batch encoded versus product total quantity per detail
     * Rule format (batch_count)
     * 
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function count($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();

        list($details, $row, $name) = explode('.',  $attribute);

        $detail = $data[$details][$row];
        
        $precision = setting('monetary.precision');

        $total = Decimal::create(1, $precision);

        for ($i=0; $i < count($parameters); $i++) { 
            $total = $total->mul(Decimal::create($detail[$parameters[$i]], $precision));
        }

        $batches = Decimal::create(0, $precision);

        for ($i=0; $i < count($value); $i++) { 
            $batches = $batches->add(Decimal::create($value[$i]['qty']));
        }

        return !(count($value) > 0 && !$total->equals($batches));
    }

    /**
     * Unique validation of encoded batch
     * eg. batch_unique
     * 
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function unique($attribute, $value, $parameters, $validator)
    {   
        $data = $validator->getData();

        list($details, $row, $batches, $index) = explode('.',  $attribute);

        $id = $data[$details][$row][$batches][$index]['id'] ?? 0;

        $count = ProductBatch::where('name', $value)
            ->where('status', '>', 0)
            ->where('id', '<>', $id)
            ->count();

        return !($count > 0);
    }

    /**
     * Unique validation of passed set of batches per transaction detail
     * Rule format (batch_unique_in_array)
     *
     * @param  string $attribute
     * @param  string $value
     * @param  array $parameters
     * @param  Validator $validator
     * @return bool
     */
    public function uniqueInArray($attribute, $value, $parameters, $validator)
    {   
        $numbers = [];

        $data = $validator->getData();

        $plucked = array_pluck($data['details'], 'batches.*.name');

        $batches = array_flatten($plucked);

        return !(in_array($value, $batches) && array_count_values($batches)[$value] > 1);
    }
}
