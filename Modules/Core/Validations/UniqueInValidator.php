<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;
use Validator;

class UniqueInValidator
{
    /**
     * Unique validation extension to easily hookup condition for soft deleted 
     * tables and ignoring id
     * Rule format (unique_in:table,table_column,soft_delete_column,id_value|id_column)
     * 
     * eg. unique_in:branch,code,deleted_at,1|sales_id
     * 
     * @param  string $attribute  
     * @param  string $value      
     * @param  array $parameters 
     * @param  Validator $validator  
     * @return bool             
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $table = $parameters[0];
        $column = $parameters[1];
        $softDeletedColumn = !isset($parameters[2]) || empty($parameters[2])
            ? null
            : $parameters[2];

        $id = isset($parameters[3]) ? explode('|', $parameters[3]) : null;

        $rule = Rule::unique($table, $column);

        if (!is_null($softDeletedColumn)) {
            $rule->where(function ($query) use ($softDeletedColumn){
                $query->whereNull($softDeletedColumn);
            });
        }

        if (!is_null($id)) {
            $rule->ignore($id[0], $id[1] ?? 'id');
        }

        $validation = Validator::make([$column => $value], [
            $column => $rule
        ]);
        
        return $validation->passes();
    }
}