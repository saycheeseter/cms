<?php

namespace Modules\Core\Validations;

use Illuminate\Validation\Rule;
use Validator;

class ExistsInValidator
{
    /**
     * Extended exists validation, with additional hookup for soft deleted columns
     * 
     * eg. exists_in:branch,code,deleted_at
     * 
     * @param  string $attribute  
     * @param  string $value      
     * @param  array $parameters 
     * @param  Validator $validator  
     * @return bool             
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $table = $parameters[0];
        $column = $parameters[1];
        $softDeletedColumn = !isset($parameters[2]) || empty($parameters[2])
            ? null
            : $parameters[2];

        $rule = Rule::exists($table, $column);

        if (!is_null($softDeletedColumn)) {
            $rule->where(function ($query) use ($softDeletedColumn){
                $query->whereNull($softDeletedColumn);
            });
        }

        $validation = Validator::make([$column => $value], [
            $column => $rule
        ]);

        return $validation->passes();
    }
}