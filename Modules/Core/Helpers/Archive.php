<?php

if (!function_exists('archive')) {
    /**
     * Returns all the archive json content
     * Save the passed array to the json file
     * Get the value of the indicated key in the json file
     *
     * @param  string  $key
     * @return string
     */
    function archive($key = null)
    {
        if (! Storage::disk('root')->exists('archive.json')) {
            return null;
        }

        $archive = json_decode(Storage::disk('root')->get('archive.json'), true);

        if (is_null($key)) {
            return $archive;
        }

        if (is_array($key)) {
            Storage::disk('root')->put('archive.json', json_encode($key));

            return true;
        }

        $key = str_replace('.', '_', $key);

        return array_get($archive, $key);
    }
}