<?php

if (!function_exists('setting')) {
    /**
     * Returns the current value if the setting
     *
     * @param  string  $key
     * @return string
     */
    function setting($key)
    {
        return array_get(settings(), $key);
    }
}

if (!function_exists('settings')) {
    /**
     * Returns the current value if the setting
     *
     * @param string $notation
     * @return array
     */
    function settings($notation = '.')
    {
        return array_merge(generic_settings($notation), branch_settings($notation));
    }
}

if (!function_exists('generic_settings')) {
    /**
     * Returns all generic setting
     * parse settings using specified notation
     *
     * @param string $notation
     * @return array
     */
    function generic_settings($notation = '.')
    {
        $generic = [];
        $settings = app('GenericSettings');

        foreach ($settings as $key => $setting) {
            $generic[str_replace('.', $notation, $setting->key)] = $setting->value;
        }

        return $generic;
    }
}

if (!function_exists('branch_settings')) {
    /**
     * Returns all branch setting
     * parse settings using specified notation
     *
     * @param string $notation
     * @return array
     */
    function branch_settings($notation = '.')
    {
        $branch = [];
        $settings = app('BranchSettings');

        foreach ($settings as $key => $setting) {
            if (branch() !== $setting->branch_id) {
                continue;
            }

            $branch[str_replace('.', $notation, $setting->key)] = $setting->value;
        }

        return $branch;
    }
}