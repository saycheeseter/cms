<?php

if (!function_exists('array_to_object')) {
    /**
     * Returns the current value if the setting
     *
     * @param  string  $key
     * @return string
     */
    function array_to_object($object)
    {
        return json_decode(json_encode($object));
    }

    function is_array_assoc($array)
    {
        foreach ($array as $key => $value) {
            if (is_string($key)) {
                return true;
            }

            return false;

            break;
        }
    }

    function mergeArrayofArrays($array)
    {
        $merged = [];

        foreach ($array as $arr) {
            $merged = array_merge($merged, $arr);
        }

        return $merged;
    }

    function unique_multidim_array($array, $key) {
        $temp = [];
        $i = 0;
        $keyArray = [];

        foreach($array as $val) {
            if (!in_array($val[$key], $keyArray)) {
                $keyArray[$i] = $val[$key];
                $temp[$i] = $val;
            }

            $i++;
        }

        return array_values($temp);
    }
}