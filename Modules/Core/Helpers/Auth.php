<?php

if (!function_exists('branch')) {
    /**
     * Returns the current value if the setting
     *
     * @param  string  $key
     * @return string
     */
    function branch()
    {
        if (Auth::guard('nsi')->check()) {
            return Auth::branch()->id;
        } else if(request()->header('Branch')) {
            return request()->header('Branch');
        } else {
            null;
        }
    }
}