<?php

if (!function_exists('enum_chosen')) {
    /**
     * Returns the current value if the setting
     *
     * @param  string  $key
     * @return string
     */
    function enum_chosen($class)
    {
        return collect($class::getEnumValues())
            ->map(function($item) {
                return array(
                    'id'    => $item->getValue(),
                    'label' => $item->getDescription()
                );
            })->values();
    }
}

if (!function_exists('get_enum_by_value')) {
    /**
     * Returns current instance of given value
     * @param  [class] $class
     * @param  [int] $value
     * @return [instance]        [class instance]
     */
    function get_enum_by_value($class, $value)
    {
        return collect($class::getEnumValues())
            ->filter(function($item) use ($value) {
                if($item->getValue() == $value) {
                    return $item;
                }
            })->first();
    }
}