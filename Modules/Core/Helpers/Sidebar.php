<?php

if (!function_exists('set_active_path')) {
    /**
     * Returns the active sidebar link to be highlighted.
     *
     * @param  string  $path
     * @return string  CSS style class
     */
    function set_active_path($path)
    {
        $paths = explode("/", $path);

        for ($i = 0; $i < count($paths); $i++) {
            if ($paths[$i] !== Request::segment($i + 1)) {
                return false;
            }
        }

        return true;
    }
}

if (!function_exists('set_url')) {
    /**
     * Returns the url for the sidebar links.
     *
     * @param  string  $url
     * @return string  Full URL
     */
    function set_url($url)
    {
        return Request::root() . '/' . $url;
    }
}
