<?php

if (!function_exists('object_to_array')) {
    /**
     * Returns the current value if the setting
     *
     * @param  string  $key
     * @return string
     */
    function object_to_array($object)
    {
        return json_decode(json_encode($object), true);
    }
}