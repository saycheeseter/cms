<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class StockReturnRequest extends InvoiceRequest
{
    public function extendedRules()
    {
        return [
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.deliver_to' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.deliver_to_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.deliver_to.required' => Lang::get('core::validation.deliver.to.required'),
            'info.deliver_to.exists_in' => Lang::get('core::validation.deliver.to.doesnt.exists'),
            'info.deliver_to_location.required' => Lang::get('core::validation.deliver.to.location.required'),
            'info.deliver_to_location.exists_in' => Lang::get('core::validation.deliver.to.location.doesnt.exists'),
        ]; 
    }
}