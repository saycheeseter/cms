<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class UDFRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'label' => [
                'required',
                Rule::unique('user_defined_fields', 'label')
                    ->where(function ($query) {
                        $query->where('module_id', $this->get('module_id'));
                    })
            ],
            'module_id' => [
                'required',
                'digits_range:1,18'
            ],
            'type' => [
                'required',
                'digits_range:1,5'
            ],
            'visible' => [
                'required',
                'boolean'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['label'][1] = $rules['label'][1]->ignore($this->id());
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'label.required' => Lang::get('core::validation.label.required'),
            'label.unique' => Lang::get('core::validation.label.exists'),
            'module_id.required' => Lang::get('core::validation.module.required'),
            'module_id.digits_range' => Lang::get('core::validation.module.doesnt.exists'),
            'type.required' => Lang::get('core::validation.type.required'),
            'type.digits_range' => Lang::get('core::validation.type.doesnt.exists'),
            'visible.required' => Lang::get('core::validation.visible.required'),
            'visible.boolean' => Lang::get('core::validation.visible.invalid.value'),
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}
