<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Support\Str;
use Lang;

class FormRequest extends \Illuminate\Foundation\Http\FormRequest
{
    protected function createArrayMessages($path, $validations, $prefix = '')
    {
        $data = $this->input($path);

        if (!is_array($data)) {
            return [];
        }

        return $this->createMessageThroughIteration($data, $path, [], $validations, $prefix);
    }

    /**
     * Iterate through the inner children to write the message for the specified validations
     *
     * @param $data
     * @param $path
     * @param $keys
     * @param $validations
     * @param string $prefix
     * @return array
     */
    private function createMessageThroughIteration($data, $path, $keys, $validations, $prefix = '')
    {
        $messages = [];

        foreach ($data as $key => $value) {
            // Determine if the current array element is an assoc or not
            // If its an assoc, then it means the iteration is at the end of the data
            // If its still a numbered index, do a recursive loop
            if (count($value) > 0 && !is_array_assoc($value)) {
                // Register the index of the current loop
                // If its a 2d array the display will be like [0][0]
                $keys[] = $key;

                $messages = array_merge($messages, $this->createMessageThroughIteration($value, $path, $keys, $validations, $prefix));

                $keys = [];
            } else {
                $indexes = array_merge($keys, [$key]);

                $readableIndex = [];

                // We will increment each recorded indexes so that the message index
                // is humanly readable, which starts the counting by 1 instead of zero
                foreach ($indexes as $index) {
                    $readableIndex[] = ++$index;
                }

                $temp = $path;

                if (count($indexes) !== substr_count($path, '*')) {
                    $temp = $path.'.*';
                }

                $assoc = Str::replaceArray('*', $indexes, $temp);

                // Create the string with the provided validation
                foreach ($validations as $field => $msg) {
                    $messages[sprintf("%s.%s", $assoc, $field)] = sprintf('%s%s[%s]. %s',
                        $prefix,
                        Lang::get('core::error.row'),
                        implode('][', $readableIndex),
                        $msg
                    );
                }
            }
        }

        return $messages;
    }
}