<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class PurchaseReturnApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [
            'payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'term' => [
                'numeric',
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'payment_method.exists_in' => Lang::get('core::validation.payment.method.doesnt.exists'),
            'term.numeric' => Lang::get('core::validation.term.numeric'),
        ]; 
    }
}