<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class MemberPointTransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'member_id' => [
                'required',
                'exists_in:member,id,deleted_at'
            ],
            'created_from' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'created_by' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'reference_type' => [
                'required'
            ],
            'reference_number' => [
                'required'
            ],
            'transaction_date' => [
                'required',
                'date'
            ],
            'amount' => [
                'numeric'
            ]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'member_id.required' => Lang::get('core::validation.member.required'),
            'member_id.exists_in' => Lang::get('core::validation.member.doesnt.exists'),
            'created_from.required' => Lang::get('core::validation.created.from.required'),
            'created_by.required' => Lang::get('core::validation.created.by.required'),
            'created_from.exists_in' => Lang::get('core::validation.branch.doesnt.exists'),
            'created_by.exists_in' => Lang::get('core::validation.user.doesnt.exists'),
            'reference_type.required' => Lang::get('core::validation.reference_type.required'),
            'reference_number.required' => Lang::get('core::validation.reference_number.required'),
            'transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'transaction_date.date' => Lang::get('core::validation.transaction.date.invalid.format'),
            'amount.numeric' => Lang::get('core::validation.amount.numeric')

        ];
    }
}