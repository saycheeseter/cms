<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class StockDeliveryRequest extends InvoiceRequest
{
    public function extendedRules()
    {
        return [
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.deliver_to' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.deliver_to_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.reference_id' => [
                'nullable',
                'exists_in:stock_request,id,deleted_at'
            ],
            'details.*.reference_id' => [
                'nullable',
                'exists_in:stock_request_detail,id,deleted_at'
            ]
        ];
    }

    protected function extendedMessages()
    {
        return array_merge([
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.deliver_to.required' => Lang::get('core::validation.deliver.to.required'),
            'info.deliver_to.exists_in' => Lang::get('core::validation.deliver.to.doesnt.exists'),
            'info.deliver_to_location.required' => Lang::get('core::validation.deliver.to.location.required'),
            'info.deliver_to_location.exists_in' => Lang::get('core::validation.deliver.to.location.doesnt.exists'),
            'info.reference_id.exists_in' => Lang::get('core::validation.stock.request.doesnt.exists'),
        ], $this->extendedDetailMessages());
    }

    protected function extendedDetailMessages()
    {
        $messages = [];

        $details = $this->detailMessages();

        if (count($details) === 0) {
            return $messages;
        }

        $path = 'details';

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.reference_id.exists_in', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.reference.product.doesnt.exists')
            );
        }

        return $messages;
    }
}