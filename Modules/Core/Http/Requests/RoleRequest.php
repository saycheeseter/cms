<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class RoleRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'info.code' => [
                'unique_in:role,code,deleted_at',
            ],
            'info.name' => [
                'required',
                'unique_in:role,name,deleted_at',
            ],
            'permissions' => [
                'required',
                'array',
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['info.code'][0] .= ','.$this->id();
                $rules['info.code'][1] = 'required';
                $rules['info.name'][1] .= ','.$this->id();

                $rules['apply'] = [
                    'required',
                    'boolean'
                ];

                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }   

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'info.code.required'   => Lang::get('core::validation.code.required'),
            'info.code.unique_in'  => Lang::get('core::validation.code.unique'),
            'info.name.required'   => Lang::get('core::validation.name.required'),
            'info.name.unique_in'  => Lang::get('core::validation.name.unique'),
            'permissions.array'    => Lang::get('core::validation.permissions.invalid.format'),
            'permissions.required' => Lang::get('core::validation.permissions.required'),
            'apply.required'       => Lang::get('core::validation.apply.required'),
            'apply.boolean'        => Lang::get('core::validation.apply.invalid'),
        ];
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}