<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'info.code' => [
                'unique_in:user,code,deleted_at',
            ],
            'info.username' => [
                'required',
                'unique_in:user,username,deleted_at',
            ],
            'info.password' => [
                'required',
                'min:6'
            ],
            'info.full_name' => [
                'required',
                'unique_in:user,full_name,deleted_at',
            ],
            'info.status' => [
                'required',
                'boolean'
            ],
            'info.email' => [
                'email'
            ],
            'info.role_id' => [
                'nullable',
                'exists_in:role,id,deleted_at'
            ],
            'permissions' => [
                'required',
                'array'
            ],
            // Will still evaluate if this validations will be triggered. Just in case
            'permissions.*.branch' => [
                'required',
                'exists_in:branch,id,deleted_at',
            ],
            'permissions.*.code' => [
                'required',
                'array',
            ],
            'permissions.*.code.*' => [
                'exists:permission,id'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                unset($rules['info.password'][0]);
                $rules['info.code'][1] = $rules['info.code'][0].','.$this->id();
                $rules['info.code'][0] = 'required';
                $rules['info.username'][1] .= ','.$this->id();
                $rules['info.full_name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'info.code.required'       => Lang::get('core::validation.code.required'),
            'info.code.unique_in'      => Lang::get('core::validation.code.unique'),
            'info.username.required'   => Lang::get('core::validation.username.required'),
            'info.username.unique_in'  => Lang::get('core::validation.username.unique'),
            'info.password.required'   => Lang::get('core::validation.password.required'),
            'info.password.min'        => Lang::get('core::validation.password.min.length'),
            'info.full_name.required'  => Lang::get('core::validation.full.name.required'),
            'info.full_name.unique_in' => Lang::get('core::validation.full.name.unique'),
            'info.status.required'     => Lang::get('core::validation.status.required'),
            'info.status.boolean'      => Lang::get('core::validation.status.numeric'),
            'info.email.email'         => Lang::get('core::validation.email.invalid'),
            'info.role_id.exists_in'   => Lang::get('core::validation.role.doesnt.exists'),
            'permissions.array'        => Lang::get('core::validation.permissions.invalid.format'),
            'permissions.required'     => Lang::get('core::validation.permissions.required'),
        ];
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}
