<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class PurchaseRequest extends InvoiceRequest
{
    public function extendedRules()
    {
        return [
            'info.supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
            'info.payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'info.deliver_until' => [
                'required',
                'date_format:Y-m-d',
            ],
            'info.term' => [
                'numeric',
            ],
            'info.deliver_to' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.supplier_id.required' => Lang::get('core::validation.supplier.required'),
            'info.supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists'),
            'info.payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'info.payment_method.exists_in' => Lang::get('core::validation.payment.method.doesnt.exists'),
            'info.deliver_until.required' => Lang::get('core::validation.deliver.until.required'),
            'info.deliver_until.date_format' => Lang::get('core::validation.deliver.until.invalid.format'),
            'info.term.numeric' => Lang::get('core::validation.term.numeric'),
            'info.deliver_to.required' => Lang::get('core::validation.deliver.to.required'),
            'info.deliver_to.exists_in' => Lang::get('core::validation.deliver.to.doesnt.exists'),
        ]; 
    }
}