<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class InventoryAdjustRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->transactionRules(),
            $this->detailRules()
        );
    }

    protected function transactionRules()
    {
        return [
            'info.requested_by' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.transaction_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ]
        ];
    }

    protected function detailRules()
    {
        return [
            'details.*.product_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'details.*.unit_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'details.*.unit_qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details.*.qty' => [
                'required',
                'numeric',
            ],
            'details.*.pc_qty' => [
                'required',
                'numeric',
            ],
            'details.*.price' => [
                'required',
                'numeric'
            ],
            'details.*.inventory' => [
                'required',
                'numeric'
            ],
            'details.*.serials' => [
                'serial_count:qty,unit_qty,pc_qty|add,inventory|sub'
            ],
            'details.*.serials.*.serial_number' => [
                'required',
                'serial_unique',
                'serial_unique_in_array'
            ],
            'details' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->transactionMessages(),
            $this->detailMessages(),
            $this->arrayMessages(),
            $this->serialMessages()
        );
    }

    protected function transactionMessages()
    {
        return [
            'info.requested_by.required' => Lang::get('core::validation.request_by.required'),
            'info.requested_by.exists_in' => Lang::get('core::validation.user.doesnt.exists'),
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists'),
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date_format' => Lang::get('core::validation.transaction.date.invalid.format'),
        ];
    }

    protected function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.product_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.required')
            );

            $messages[sprintf('%s.%s.product_id.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.required')
            );

            $messages[sprintf('%s.%s.unit_id.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_qty.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.required')
            );

            $messages[sprintf('%s.%s.unit_qty.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.numeric')
            );

            $messages[sprintf('%s.%s.unit_qty.decimal_min', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.greater.than.zero')
            );

            $messages[sprintf('%s.%s.qty.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.required')
            );

            $messages[sprintf('%s.%s.qty.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.numeric')
            );

            $messages[sprintf('%s.%s.pc_qty.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.pc.qty.required')
            );

            $messages[sprintf('%s.%s.pc_qty.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.pc.qty.numeric')
            );

            $messages[sprintf('%s.%s.price.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.purchase.price.required')
            );

            $messages[sprintf('%s.%s.price.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.purchase.price.numeric')
            );

            $messages[sprintf('%s.%s.inventory.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.inventory.required')
            );

            $messages[sprintf('%s.%s.inventory.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.inventory.numeric')
            );

            $messages[sprintf('%s.%s.serials.serial_count', $path, $key)] = sprintf('%s[%s]: %s',
                Lang::get('core::error.serial.detail.row'),
                $number,
                Lang::get('core::validation.serial.count.invalid')
            );
        }

        return $messages;
    }

    protected function arrayMessages()
    {
        return array(
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one'),
            'details.required' => Lang::get('core::validation.details.min.one')
        );
    }

    protected function serialMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $row => $detail) {
            $product = $row + 1;

            if (isset($detail['serials'])) {
                foreach ($detail['serials'] as $index => $serial) {
                    $number = $index + 1;

                    $messages[sprintf('%s.%s.serials.%s.serial_number.required', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.required')
                    );

                    $messages[sprintf('%s.%s.serials.%s.serial_number.serial_unique', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.unique')
                    );

                    $messages[sprintf('%s.%s.serials.%s.serial_number.serial_unique_in_array', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.unique')
                    );
                }
            }

            if (isset($detail['batches'])) {
                foreach ($detail['batches'] as $index => $serial) {
                    $number = $index + 1;

                    $messages[sprintf('%s.%s.batches.%s.name.required', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.required')
                    );

                    $messages[sprintf('%s.%s.batches.%s.name.batch_unique', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.unique')
                    );

                    $messages[sprintf('%s.%s.batches.%s.name.batch_unique_in_array', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.unique')
                    );
                }
            }
        }

        return $messages;
    }
}
