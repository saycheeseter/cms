<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;
use Modules\Core\Enums\PaymentMethod;

abstract class FinancialFlowRequest extends FormRequest
{
    protected $reference = '';

    public function rules()
    {
        return array_merge(
            $this->infoRules(),
            $this->detailRules(),
            $this->referenceRules(),
            $this->extendedRules()
        );
    }

    protected function infoRules()
    {
        return [
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ]
        ];
    }

    protected function detailRules()
    {
        return [
            'details.*.amount' => [
                'required',
                'numeric'
            ],
            'details.*.payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'details.*.bank_account_id' => [
                sprintf('required_if:details.*.payment_method,%s,%s', PaymentMethod::CHECK, PaymentMethod::BANK_DEPOSIT)
            ],
            'details.*.check_no' => [
                sprintf('required_if:details.*.payment_method,%s', PaymentMethod::CHECK)
            ],
            'details.*.check_date' => [
                sprintf('required_if:details.*.payment_method,%s', PaymentMethod::CHECK)
            ],
            'details' => [
                'array',
                'min:1'
            ]
        ];
    }

    protected function referenceRules()
    {
        return [
            'references.*.'.$this->reference => [
                'required',
                'numeric'
            ],
            'references' => [
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return array_merge(
            $this->infoMessages(),
            $this->detailMessages(),
            $this->referenceMessages(),
            $this->extendedMessages(),
            $this->arrayMessages()
        );
    }

    public function infoMessages() 
    {
        return [
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists')
        ];
    }

    public function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.amount.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.amount.required')
            );

            $messages[sprintf('%s.%s.amount.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.amount.numeric')
            );

            $messages[sprintf('%s.%s.payment_method.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.payment.method.required')
            );

            $messages[sprintf('%s.%s.payment_method.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.payment.method.doesnt.exists')
            );

            $messages[sprintf('%s.%s.bank_account_id.required_if', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.bank.account.required')
            );

            $messages[sprintf('%s.%s.check_no.required_if', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.check.no.required')
            );

            $messages[sprintf('%s.%s.check_date.required_if', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.check.date.required')
            );
        }

        return $messages;
    }

    public function referenceMessages()
    {
        $messages = [];

        $path = 'references';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.reference_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.reference.required')
            );

            $messages[sprintf('%s.%s.%s.required', $path, $key, $this->reference)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.amount.required')
            );

            $messages[sprintf('%s.%s.%s.numeric', $path, $key, $this->reference)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.amount.numeric')
            );
        }

        return $messages;
    }

    protected function arrayMessages()
    {
        return array(
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one'),
            'references.array' => Lang::get('core::validation.invalid.format'),
            'references.min' => Lang::get('core::validation.references.min.one')
        );
    }

    abstract protected function extendedRules();
    abstract protected function extendedMessages();
}