<?php

namespace Modules\Core\Http\Requests;

use Modules\Core\Enums\Customer;
use Lang;

class CollectionRequest extends FinancialFlowRequest
{
    protected $reference = 'collectible';

    protected function extendedRules()
    {
        return [
            'info.customer' => [
                sprintf('required_if:info.customer_type,%s', Customer::REGULAR),
                sprintf('required_if:info.customer_type,%s', Customer::WALK_IN)
            ],
            'info.customer_type' => [
                'required',
                'numeric'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.customer.required_if'              => Lang::get('core::validation.customer.required'),
            'info.customer_type.required'            => Lang::get('core::validation.customer.type.required'),
            'info.customer_type.numeric'             => Lang::get('core::validation.customer.type.numeric'),
        ];
    }
}