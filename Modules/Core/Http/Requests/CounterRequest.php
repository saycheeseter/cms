<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class CounterRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->infoRules(),
            $this->detailRules()
        );
    }

    public function infoRules()
    {
        return [
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.customer_id' => [
                'required',
                'exists_in:customer,id,deleted_at'
            ],
            'info.transaction_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            'info' => [
                'array',
                'required'
            ]
        ];
    }

    public function detailRules()
    {
        return [
            'details.*.reference_id' => [
                'required',
                'exists_in:sales_outbound,id,deleted_at'
            ],
            'details.*.remaining_amount' => [
                'required',
                'numeric'
            ],
            'details' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return array_merge(
            $this->infoMessages(),
            $this->detailMessages(),
            $this->arrayMessages()
        );
    }

    public function infoMessages()
    {
        return [
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists'),
            'info.customer_id.required' => Lang::get('core::validation.customer.required'),
            'info.customer_id.exists_in' => Lang::get('core::validation.customer.doesnt.exists'),
            'info.transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date_format' => Lang::get('core::validation.transaction.date.invalid.format')
        ];
    }

    public function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.reference_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.reference.required')
            );

            $messages[sprintf('%s.%s.reference_id.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.reference.doesnt.exists')
            );

            $messages[sprintf('%s.%s.remaining_amount.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.remaining.amount.required')
            );

            $messages[sprintf('%s.%s.remaining_amount.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.remaining.amount.numeric')
            );
        }

        return $messages;
    }

    public function arrayMessages()
    {
        return [
            'info.array' => Lang::get('core::validation.info.invalid.format'),
            'info.required' => Lang::get('core::validation.info.required'),
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one'),
            'details.required' => Lang::get('core::validation.details.min.one'),
        ];
    }
}