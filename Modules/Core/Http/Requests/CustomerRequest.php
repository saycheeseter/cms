<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Enums\CustomerPricingType;
use Lang;

class CustomerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'info.name' => [
                'required',
                'unique_in:customer,name,deleted_at',
            ],
            'info.code' => [
                'unique_in:customer,code,deleted_at',
            ],
            'info.credit_limit' => [
                'numeric'
            ],
            'info.pricing_type' => [
                'required',
                Rule::in(range(CustomerPricingType::HISTORICAL, CustomerPricingType::PRICE_E))
            ],
            'info.pricing_rate' => [
                'numeric'
            ],
            'info.term' => [
                'numeric'
            ],
            'info.salesman_id' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'details.*.name' => [
                'required',
                'unique_in_set:details'
            ],
            'details.*.email' => [
                'email'
            ],
            'details' => [
                'required',
                'array',
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['info.code'][1] = $rules['info.code'][0].','.$this->id();
                $rules['info.code'][0] = 'required';
                $rules['info.name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        $messages = array_merge(
            $this->arrayMessages(),
            $this->infoMessages(), 
            $this->detailMessages()
        );

        return $messages;
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

    /**
     * List of validation messages for the info array
     *
     * @return array
     */
    private function infoMessages()
    {
        return [
            'info.code.required'         => Lang::get('core::label.customer.detail'). ' : ' .Lang::get('core::validation.code.required'),
            'info.code.unique_in'        => Lang::get('core::label.customer.detail'). ' : ' .Lang::get('core::validation.code.unique'),
            'info.name.required'         => Lang::get('core::label.customer.detail'). ' : ' .Lang::get('core::validation.name.required'),
            'info.name.unique_in'        => Lang::get('core::label.customer.detail'). ' : ' .Lang::get('core::validation.name.unique'),
            'info.credit_limit.numeric'  => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.credit.limit.numeric'),
            'info.pricing_rate.numeric'  => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.pricing.rate.numeric'),
            'info.term.numeric'          => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.term.numeric'),
            'info.pricing_type.required' => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.pricing.type.required'),
            'info.pricing_type.in'       => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.pricing.type.invalid'),
            'info.salesman_id.required'  => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.salesman.required'),
            'info.salesman_id.exists_in' => Lang::get('core::label.advance.info'). ' : ' .Lang::get('core::validation.salesman.doesnt.exists'),
        ];
    }

    /**
     * List of validation messages for the detail array
     *
     * @return array
     */
    private function detailMessages()
    {
        return $this->createArrayMessages('details', [
            'name.required'      => Lang::get('core::validation.name.required'),
            'name.unique_in_set' => Lang::get('core::validation.name.array.unique'),
            'email.email'        => Lang::get('core::validation.email.invalid'),
        ], Lang::get('core::label.customer.branches').' : ');
    }

    /**
     * List of validation messages for detail arrays
     *
     * @return array
     */
    private function arrayMessages()
    {
        return array(
            'details.array'    => Lang::get('core::label.customer.branches') . ' : ' . Lang::get('core::validation.invalid.format'),
            'details.required' => Lang::get('core::label.customer.branches') . ' : ' . Lang::get('core::validation.details.min.one')
        );
    }
}
