<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class CheckManagementRequest extends FormRequest
{
    public function rules()
    {
        return [
            'transfer_date' => [
                'required',
                'date_format:Y-m-d',
            ],
            'ids' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'transfer_date.required' => Lang::get('core::validation.transfer.date.required'),
            'transfer_date.date_format' => Lang::get('core::validation.transfer.date.invalid.format'),
            'ids.required' => Lang::get('core::validation.check.min.one'),
            'ids.array' => Lang::get('core::validation.invalid.format'),
            'ids.min' => Lang::get('core::validation.check.min.one')
        ];
    }
}