<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class MemberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'rate_head_id' => [
                'required',
                'exists_in:member_rate,id,deleted_at'
            ],
            'full_name' => [
                'required',
                'unique_in:member,full_name,deleted_at'
            ],
            'card_id' => [
                'unique_in:member,card_id,deleted_at'
            ],
            'barcode' => [
                'required',
                'numeric',
                'unique_in:member,barcode,deleted_at'
            ],
            'status' => [
                'required',
                'boolean'
            ],
            'birth_date' => [
                'required',
                'date_format:Y-m-d'
            ],
            'registration_date' => [
                'required',
                'date_format:Y-m-d'
            ],
            'expiration_date' => [
                'required',
                'date_format:Y-m-d',
                'date_after:registration_date'
            ],
            'email' => [
                'email',
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['full_name'][1] .= ','.$this->id();
                $rules['card_id'][0] .= ','.$this->id();
                $rules['card_id'][1] = 'required';
                $rules['barcode'][2] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'rate_head_id.required'         => Lang::get('core::validation.rate.required'),
            'rate_head_id.exists_in'        => Lang::get('core::validation.member.rate.doesnt.exists'),
            'full_name.required'            => Lang::get('core::validation.full.name.required'),
            'full_name.unique_in'           => Lang::get('core::validation.full.name.unique'),
            'card_id.required'              => Lang::get('core::validation.card.id.required'),
            'card_id.unique_in'             => Lang::get('core::validation.card.id.unique'),
            'barcode.required'              => Lang::get('core::validation.barcode.required'),
            'barcode.unique_in'             => Lang::get('core::validation.barcode.unique'),
            'barcode.numeric'               => Lang::get('core::validation.barcode.numeric'),
            'status.required'               => Lang::get('core::validation.status.required'),
            'status.boolean'                => Lang::get('core::validation.status.numeric'),
            'registration_date.required'    => Lang::get('core::validation.registration.date.required'),
            'registration_date.date_format' => Lang::get('core::validation.registration.date.format'),
            'expiration_date.date_format'   => Lang::get('core::validation.expiration.date.format'),
            'expiration_date.required'      => Lang::get('core::validation.expiration.date.required'),
            'expiration_date.date_after'    => Lang::get('core::validation.expiration.date.invalid.range'),
            'email.email'                   => Lang::get('core::validation.email.invalid'),
            'birth_date.required'           => Lang::get('core::validation.birth.date.required'),
            'birth_date.date_format'        => Lang::get('core::validation.birth.date.invalid.format'),
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}
