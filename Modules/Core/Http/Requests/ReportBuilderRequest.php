<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class ReportBuilderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
                'unique:report_builder,name',
                'max:120'
            ],
            'group_id' => [
                'required'
            ],
            'format' => [
                'array'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => Lang::get('core::validation.name.required'),
            'name.unique' => Lang::get('core::validation.name.unique'),
            'name.max' => Lang::get('core::validation.name.max'),
            'group_id.required' => Lang::get('core::validation.module.required'),
            'format.array' => Lang::get('core::validation.invalid.template.data.format'),
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(3);
    }
}
