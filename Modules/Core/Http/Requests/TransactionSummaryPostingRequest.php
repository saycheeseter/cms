<?php

namespace Modules\Core\Http\Requests;

use Lang;

class TransactionSummaryPostingRequest extends FormRequest
{
    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'date.required'    => Lang::get('core::validation.date.required'),
            'date.date_format' => Lang::get('core::validation.date.format')
        ];
    }
}