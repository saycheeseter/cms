<?php

namespace Modules\Core\Http\Requests;

use Lang;

class PaymentRequest extends FinancialFlowRequest
{
    protected $reference = 'payable';

    protected function extendedRules()
    {
        return [
            'info.supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.supplier_id.required' => Lang::get('core::validation.supplier.required'),
            'info.supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists')
        ];
    }
}