<?php

namespace Modules\Core\Http\Requests;

use Lang;

class BranchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'info.code' => [
                'unique_in:branch,code,deleted_at',
            ],
            'info.name' => [
                'required',
                'unique_in:branch,name,deleted_at',
            ],
            'details.*.code' => [
                'unique_in_set:details'
            ],
            'details.*.name' => [
                'required',
                'unique_in_set:details'
            ],
            'details' => [
                'required',
                'array',
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['info.code'][1] = $rules['info.code'][0].','.$this->id();
                $rules['info.code'][0] = 'required';
                $rules['info.name'][1] .= ','.$this->id();
                $rules['details.*.code'][1] = $rules['details.*.code'][0];
                $rules['details.*.code'][0] = 'required';
                $rules['details.*.name'][1] .= ',id';
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        $messages = array_merge(
            $this->arrayMessages(),
            $this->infoMessages(), 
            $this->detailMessages()
        );

        return $messages;
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

    /**
     * List of validation messages for the info array
     * 
     * @return array
     */
    private function infoMessages()
    {
        return [
            'info.code.required'  => Lang::get('core::validation.code.required'),
            'info.code.unique_in' => Lang::get('core::validation.code.unique'),
            'info.name.required'  => Lang::get('core::validation.name.required'),
            'info.name.unique_in' => Lang::get('core::validation.name.unique')
        ];
    }

    private function detailMessages()
    {
        return $this->createArrayMessages('details', [
            'code.required'      => Lang::get('core::validation.code.required'),
            'code.array_unique'  => Lang::get('core::validation.code.unique'),
            'code.unique_in_set' => Lang::get('core::validation.code.array.unique'),
            'name.required'      => Lang::get('core::validation.name.required'),
            'name.unique_in_set' => Lang::get('core::validation.name.array.unique'),
        ]);
    }

    /**
     * List of validation messages for detail arrays
     * 
     * @return array
     */
    private function arrayMessages()
    {
        return [
            'details.array'    => Lang::get('core::validation.invalid.format'),
            'details.required' => Lang::get('core::validation.details.min.one')
        ];
    }
}