<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => [
                'unique_in:category,code,deleted_at'
            ],
            'name' => [
                'required',
                'unique_in:category,name,deleted_at'
            ],
            'parent_id' => [
                'nullable',
                'exists_in:category,id,deleted_at'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['code'][0] .= ','.$this->id();
                $rules['code'][1] = 'required';
                $rules['name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => Lang::get('core::validation.code.required'),
            'code.unique_in' => Lang::get('core::validation.code.unique'),
            'name.required' => Lang::get('core::validation.name.required'),
            'name.unique_in' => Lang::get('core::validation.name.unique'),
            'parent_id.exists_in' => Lang::get('core::validation.parent.doesnt.exists')
        ];
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}