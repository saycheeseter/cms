<?php

namespace Modules\Core\Http\Requests;

use Lang;

class InventoryAdjustApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [];
    }

    protected function extendedMessages()
    {
        return [];
    }
}
