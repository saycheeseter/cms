<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class StockReturnApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [];
    }

    protected function extendedMessages()
    {
        return []; 
    }
}