<?php

namespace Modules\Core\Http\Requests;

use Lang;

class PurchaseInboundRequest extends InventoryChainRequest
{
    protected $reference = [
        'info' => 'purchase',
        'detail' => 'purchase_detail'
    ];

    public function extendedRules()
    {
        return [
            'info.supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
            'info.payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'info.term' => [
                'numeric',
            ],
            'details.*.serials.*.serial_number' => [
                'required',
                'serial_unique',
                'serial_unique_in_array'
            ],
            'details.*.batches.*.name' => [
                'required',
                'batch_unique',
                'batch_unique_in_array'
            ],
            'details.*.batches.*.qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return array_merge([
            'info.supplier_id.required' => Lang::get('core::validation.supplier.required'),
            'info.supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists'),
            'info.payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'info.payment_method.exists_in' => Lang::get('core::validation.payment.method.doesnt.exists'),
            'info.term.numeric' => Lang::get('core::validation.term.numeric'),
        ], $this->serialMessages());
    }

    protected function serialMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $row => $detail) {
            $product = $row + 1;
            
            if (isset($detail['serials'])) {
                foreach ($detail['serials'] as $index => $serial) {
                    $number = $index + 1;
                    
                    $messages[sprintf('%s.%s.serials.%s.serial_number.required', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.required')
                    );

                    $messages[sprintf('%s.%s.serials.%s.serial_number.serial_unique', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.unique')
                    );

                    $messages[sprintf('%s.%s.serials.%s.serial_number.serial_unique_in_array', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.serial.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.serial.no.unique')
                    );
                }
            }
            
            if (isset($detail['batches'])) {
                foreach ($detail['batches'] as $index => $serial) {
                    $number = $index + 1;
                    
                    $messages[sprintf('%s.%s.batches.%s.name.required', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.required')
                    );

                    $messages[sprintf('%s.%s.batches.%s.name.batch_unique', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.unique')
                    );

                    $messages[sprintf('%s.%s.batches.%s.name.batch_unique_in_array', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.name.unique')
                    );

                    $messages[sprintf('%s.%s.batches.%s.qty.required', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.qty.required')
                    );

                    $messages[sprintf('%s.%s.batches.%s.qty.numeric', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.qty.numeric')
                    );

                    $messages[sprintf('%s.%s.batches.%s.qty.decimal_min', $path, $row, $index)] = sprintf('%s[%s]: %s[%s] %s',
                        Lang::get('core::error.batch.detail.row'),
                        $product,
                        Lang::get('core::error.row'),
                        $number,
                        Lang::get('core::validation.qty.min.one')
                    );
                }
            }
        }

        return $messages;
    }
}
