<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class AttachmentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'attachments.*' => [
                'max:2000000',
            ],
        ];

        if($this->type() == 'image') {
            $rules['attachments.*'][] = 'mimes:jpeg,bmp,png';
        } else {
            $rules['attachments.*'][] = 'mimes:jpeg,bmp,png,doc,xls,pdf,docx,xlsx,';
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [];

        foreach ($this->file('attachments') as $key => $value) {
            $originalName = $value->getClientOriginalName();

            $messages[sprintf('attachments.%s.max', $key)] = sprintf('%s [%s]. %s ', 
                Lang::get('core::validation.error.on.file'),
                $originalName,
                Lang::get('core::validation.file.over.allowed.size')
            );

            $messages[sprintf('attachments.%s.mimes', $key)] = sprintf('%s [%s]. %s ', 
                Lang::get('core::validation.error.on.file'),
                $originalName,
                Lang::get('core::validation.invalid.file.type')
            );

            $messages[sprintf('attachments.%s.uploaded', $key)] = sprintf('%s [%s]. %s ', 
                Lang::get('core::validation.error.on.file'),
                $originalName,
                Lang::get('core::validation.cannot.upload.file')
            );
        }

        return $messages;
    }

    private function type()
    {
        return $this->segment(3);
    }
}
