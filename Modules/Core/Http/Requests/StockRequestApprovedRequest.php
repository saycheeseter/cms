<?php

namespace Modules\Core\Http\Requests;

use Lang;

class StockRequestApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [];
    }

    protected function extendedMessages()
    {
        return [];
    }
}
