<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\Customer;
use Lang;

class PosSalesRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->transactionRules(),
            $this->detailRules(),
            $this->collectionRules()
        );
    }

    protected function transactionRules()
    {
        return [
            'info.cashier_id' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'info.customer_id' => [
                sprintf('required_if:info.customer_type,%s', Customer::REGULAR),
            ],
            'info.customer_detail_id' => [
                sprintf('required_if:info.customer_type,%s', Customer::REGULAR),
            ],
            'info.salesman_id' => [
                'nullable',
                'exists_in:user,id,deleted_at'
            ],
            'info.member_id' => [
                'nullable',
                'exists_in:member,id,deleted_at'
            ],
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.transaction_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            'info.customer_type' => [
                'required'
            ],
            'info.customer_walk_in_name' => [
                sprintf('required_if:info.customer_type,%s', Customer::WALK_IN)
            ],
            'info.total_amount' => [
                'required',
                'numeric'
            ],
            'info.terminal_number' => [
                'required'
            ],
            'info.is_non_vat' => [
                'required',
                'numeric'
            ],
            'info.is_wholesale' => [
                'required',
                'numeric'
            ]
        ];
    }

    protected function detailRules()
    {
        return [
            'details.*.product_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'details.*.unit_id' => [
                'required',
                'exists:system_code,id'
            ],
            'details.*.unit_qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details.*.qty' => [
                'required',
                'numeric',
            ],
            'details.*.oprice' => [
                'required',
                'numeric'
            ],
            'details.*.price' => [
                'required',
                'numeric'
            ],
            'details.*.discount1' => [
                'numeric'
            ],
            'details.*.discount2' => [
                'numeric'
            ],
            'details.*.discount3' => [
                'numeric'
            ],
            'details.*.discount4' => [
                'numeric'
            ],
            'details.*.vat' => [
                'required',
                'numeric'
            ],
            'details.*.is_senior' => [
                'required'
            ],
            'details.*.group_type' => [
                'required'
            ],
            'details.*.product_type' => [
                'required'
            ],
            'details.*.manageable' => [
                'required'
            ],
            'details' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    protected function collectionRules()
    {
        return [
            'collections.*.payment_method' => [
                'required',
                'exists:system_code,id'
            ],
            'collections.*.amount' => [
                'required',
                'numeric'
            ],
            'collections' => [
                'required',
                'array'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->transactionMessages(),
            $this->detailMessages(),
            $this->collectionMessages(),
            $this->arrayMessages()
        );
    }

    protected function transactionMessages()
    {
        return [
            'info.cashier.required'                  => Lang::get('core::validation.cashier.required'),
            'info.cashier.exists_in'                 => Lang::get('core::validation.cashier.doesnt.exists'),
            'info.customer_id.required_if'           => Lang::get('core::validation.customer.required'),
            'info.customer_detail_id.required_if'    => Lang::get('core::validation.customer.detail.required'),
            'info.salesman_id.exists_in'             => Lang::get('core::validation.salesman.doesnt.exists'),
            'info.member_id.exists_in'               => Lang::get('core::validation.member.doesnt.exists'),
            'info.created_for.required'              => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in'             => Lang::get('core::validation.created.for.doesnt.exists'),
            'info.for_location.required'             => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in'            => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.transaction_date.required'         => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date_format'      => Lang::get('core::validation.transaction.date.invalid.format'),
            'info.customer_type.required'            => Lang::get('core::validation.customer.type.required'),
            'info.customer_walk_in_name.required_if' => Lang::get('core::validation.customer.walk.in.name.required'),
            'info.total_amount.required'             => Lang::get('core::validation.total.amount.required'),
            'info.total_amount.numeric'              => Lang::get('core::validation.total.amount.numeric'),
            'info.terminal_number.required'          => Lang::get('core::validation.terminal.number.required'),
            'info.is_non_vat.required'               => Lang::get('core::validation.is.non.vat.required'),
            'info.is_non_vat.numeric'                => Lang::get('core::validation.is.not.vat.numeric'),
            'info.is_wholesale.required'             => Lang::get('core::validation.is.wholesale.required'),
            'info.is_wholesale.numeric'              => Lang::get('core::validation.is.wholesale.numeric'),
        ];
    }

    protected function detailMessages()
    {
        return $this->createArrayMessages('details', [
            'product_id.required'   => Lang::get('core::validation.product.required'),
            'product_id.exists_in'  => Lang::get('core::validation.product.doesnt.exists'),
            'unit_id.required'      => Lang::get('core::validation.unit.required'),
            'unit_id.exists'        => Lang::get('core::validation.unit.doesnt.exists'),
            'unit_qty.required'     => Lang::get('core::validation.unit.qty.required'),
            'unit_qty.numeric'      => Lang::get('core::validation.unit.qty.numeric'),
            'unit_qty.decimal_min'  => Lang::get('core::validation.unit.qty.greater.than.zero'),
            'qty.required'          => Lang::get('core::validation.qty.required'),
            'qty.numeric'           => Lang::get('core::validation.qty.numeric'),
            'oprice.required'       => Lang::get('core::validation.oprice.required'),
            'oprice.numeric'        => Lang::get('core::validation.oprice.numeric'),
            'price.required'        => Lang::get('core::validation.price.required'),
            'price.numeric'         => Lang::get('core::validation.price.numeric'),
            'discount1.numeric'     => Lang::get('core::validation.discount.numeric', ['num' => '1']),
            'discount2.numeric'     => Lang::get('core::validation.discount.numeric', ['num' => '2']),
            'discount3.numeric'     => Lang::get('core::validation.discount.numeric', ['num' => '3']),
            'discount4.numeric'     => Lang::get('core::validation.discount.numeric', ['num' => '4']),
            'vat.required'          => Lang::get('core::validation.vat.required'),
            'vat.numeric'           => Lang::get('core::validation.vat.numeric'),
            'is_senior.required'    => Lang::get('core::validation.is.senior.required'),
            'group_type.required'   => Lang::get('core::validation.group.type.required'),
            'product_type.required' => Lang::get('core::validation.product.type.required'),
            'manageable.required'   => Lang::get('core::validation.manageable.required'),
        ]);
    }

    public function collectionMessages()
    {
        return $this->createArrayMessages('collections', [
            'amount.required'         => Lang::get('core::validation.amount.required'),
            'amount.numeric'          => Lang::get('core::validation.amount.numeric'),
            'payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'payment_method.exists'   => Lang::get('core::validation.payment.method.doesnt.exists')
        ]);
    }

    protected function arrayMessages()
    {
        return array(
            'details.required'     => Lang::get('core::validation.details.required'),
            'details.array'        => Lang::get('core::validation.invalid.format'),
            'details.min'          => Lang::get('core::validation.details.min.one'),
            'collections.required' => Lang::get('core::validation.collections.required'),
            'collections.array'    => Lang::get('core::validation.invalid.format'),
            'collections.min'      => Lang::get('core::validation.collection.min.one')
        );
    }
}