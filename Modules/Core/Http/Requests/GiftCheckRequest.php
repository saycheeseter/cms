<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class GiftCheckRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => [
                'required',
                'unique_in:gift_check,code,deleted_at'
            ],
            'check_number' => [
                'required',
                'unique_in:gift_check,check_number,deleted_at'
            ],
            'amount' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'date_from' => [
                'required',
                'date_format:"Y-m-d"',
                'before_or_equal:date_to'
            ],
            'date_to' => [
                'required',
                'date_format:"Y-m-d"',
                'after_or_equal:date_from'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['code'][1] .= ','.$this->id();
                $rules['check_number'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => Lang::get('core::validation.code.required'),
            'code.unique_in' => Lang::get('core::validation.code.unique'),
            'check_number.required' => Lang::get('core::validation.check.number.required'),
            'check_number.unique_in' => Lang::get('core::validation.check.number.exists'),
            'amount.required' => Lang::get('core::validation.amount.required'),
            'amount.numeric' => Lang::get('core::validation.amount.numeric'),
            'amount.decimal_min' => Lang::get('core::validation.amount.greater.than.zero'),
            'date_from.required' => Lang::get('core::validation.date.from.required'),
            'date_from.date_format' => Lang::get('core::validation.date.from.invalid.format'),
            'date_from.before_or_equal' => Lang::get('core::validation.date.from.invalid.range'),
            'date_to.required' => Lang::get('core::validation.date.to.required'),
            'date_to.date_format' => Lang::get('core::validation.date.to.invalid.format'),
            'date_to.after_or_equal' => Lang::get('core::validation.date.to.invalid.range'),
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}