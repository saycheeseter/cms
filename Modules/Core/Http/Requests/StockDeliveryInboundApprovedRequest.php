<?php

namespace Modules\Core\Http\Requests;

use Modules\Core\Enums\TransactionType;
use Lang;

class StockDeliveryInboundApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [];
    }

    protected function extendedMessages()
    {
        return [];
    }
}
