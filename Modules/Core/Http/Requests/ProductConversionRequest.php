<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Lang;

class ProductConversionRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->infoRules(),
            $this->detailRules()
        );
    }

    public function infoRules()
    {
        return [
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.transaction_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            'info.product_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'info.qty' => [
                'required',
                'numeric'
            ],
            'info' => [
                'array',
                'required'
            ]
        ];
    }

    public function detailRules()
    {
        return [
            'details.*.component_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'details.*.required_qty' => [
                'required',
                'numeric'
            ],
            'details' => [
                'required',
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return array_merge(
            $this->infoMessages(),
            $this->detailMessages(),
            $this->arrayMessages()
        );
    }

    public function infoMessages()
    {
        return [
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists'),
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.requested_by.required' => Lang::get('core::validation.request_by.required'),
            'info.requested_by.exists_in' => Lang::get('core::validation.user.doesnt.exists'),
            'info.transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date_format' => Lang::get('core::validation.transaction.date.invalid.format'),
            'info.product_id.required' => Lang::get('core::validation.product.required'),
            'info.product_id.exists_in' => Lang::get('core::validation.product.doesnt.exists'),
            'info.qty.required' => Lang::get('core::validation.qty.required'),
            'info.qty.numeric' => Lang::get('core::validation.qty.numeric'),
        ];
    }

    public function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.component_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.component.id.required')
            );

            $messages[sprintf('%s.%s.component_id.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.component.doesnt.exists')
            );

            $messages[sprintf('%s.%s.required_qty.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.required.qty.required')
            );

            $messages[sprintf('%s.%s.required_qty.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.required.qty.numeric')
            );
        }

        return $messages;
    }

    public function arrayMessages()
    {
        return [
            'info.array' => Lang::get('core::validation.info.invalid.format'),
            'info.required' => Lang::get('core::validation.info.required'),
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one'),
            'details.required' => Lang::get('core::validation.details.min.one'),
        ];
    }
}