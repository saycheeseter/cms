<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class BankTransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                'numeric'
            ], 
            'transaction_date' => [
                'required',
                'date_format:Y-m-d'
            ],
            'amount' => [
                'required',
                'numeric'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => Lang::get('core::validation.type.required'),
            'type.numeric' => Lang::get('core::validation.type.numeric'),
            'transaction_date.required' => Lang::get('core::validation.date.required'),
            'transaction_date.date_format' => Lang::get('core::validation.date.format'),
            'amount.required' => Lang::get('core::validation.amount.required'),
            'amount.numeric' => Lang::get('core::validation.amount.numeric')
        ];
    }
}