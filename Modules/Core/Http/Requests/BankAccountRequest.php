<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class BankAccountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'bank_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'account_name' => [
                'required'
            ],
            'account_number' => [
                'required',
                'unique_in:bank_account,account_number,deleted_at'
            ],
            'opening_balance' => [
                'numeric'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['account_number'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'bank_id.required'         => Lang::get('core::validation.bank.required'),
            'bank_id.exists_in'        => Lang::get('core::validation.bank.must.exists'),
            'account_name.required'    => Lang::get('core::validation.account.name.required'),
            'account_number.required'  => Lang::get('core::validation.account.number.required'),
            'account_number.unique_in' => Lang::get('core::validation.account.number.exists'),
            'opening_balance.numeric'  => Lang::get('core::validation.invalid.opening.balance')
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}
