<?php

namespace Modules\Core\Http\Requests;

use Lang;

class DamageOutboundRequest extends InventoryChainRequest
{
    protected $reference = [
        'info' => 'damage',
        'detail' => 'damage_detail',
    ];

    public function extendedRules()
    {
        return [
            'info.reason_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.reason_id.required' => Lang::get('core::validation.reason.required'),
            'info.reason_id.exists_in' => Lang::get('core::validation.reason.doesnt.exists'),
        ];
    }
}
