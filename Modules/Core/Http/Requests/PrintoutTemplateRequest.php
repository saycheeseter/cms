<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class PrintoutTemplateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [ 
            'name' => [
                'required',
                'unique_in:printout_template,name,deleted_at'
            ],
            'module_id' => [
                'required',
            ],
            'format' => [
                'required'
            ],
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['name'][1] .= ','.$this->segment(2);
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => Lang::get('core::validation.name.required'),
            'name.unique_in' => Lang::get('core::validation.name.unique'),
            'module_id.required' => Lang::get('core::validation.module.required'),
            'format.required'  => Lang::get('core::validation.format.required'),
        ];
    }
}
