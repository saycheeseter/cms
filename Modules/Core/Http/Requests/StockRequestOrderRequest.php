<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class StockRequestOrderRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->transactionRules(),
            $this->detailRules()
        );
    }

    protected function transactionRules()
    {
        return [
            'info.transactionable_id' => [
                'required',
                'exists_in:stock_request,id,deleted_at'
            ],
            'info.deliver_to' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
        ];
    }

    protected function detailRules()
    {
        return [
            'details.*.product_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'details.*.unit_id' => [
                'required',
                'exists:system_code,id'
            ],
            'details.*.unit_qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details.*.qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details' => [
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->transactionMessages(),
            $this->detailMessages(),
            $this->arrayMessages()
        );
    }

    protected function transactionMessages()
    {
        return [
            'info.transactionable_id.required' => Lang::get('core::validation.reference.transaction.required'),
            'info.transactionable_id.exists_in' => Lang::get('core::validation.reference.transaction.doesnt.exists'),
            'info.deliver_to.required' => Lang::get('core::validation.deliver.to.required'),
            'info.deliver_to.exists_in' => Lang::get('core::validation.deliver.to.doesnt.exists'),
        ];
    }

    protected function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.product_id.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.required')
            );

            $messages[sprintf('%s.%s.product_id.exists_in', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.required')
            );

            $messages[sprintf('%s.%s.unit_id.exists', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_qty.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.required')
            );

            $messages[sprintf('%s.%s.unit_qty.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.numeric')
            );

            $messages[sprintf('%s.%s.unit_qty.decimal_min', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.greater.than.zero')
            );

            $messages[sprintf('%s.%s.qty.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.required')
            );

            $messages[sprintf('%s.%s.qty.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.numeric')
            );

            $messages[sprintf('%s.%s.qty.decimal_min', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.greater.than.zero')
            );
        }

        return $messages;
    }

    protected function arrayMessages()
    {
        return array(
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one')
        );
    }
}