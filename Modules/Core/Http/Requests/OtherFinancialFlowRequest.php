<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Core\Enums\PaymentMethod;
use Lang;

class OtherFinancialFlowRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'info.reference_name' => [
                'required'
            ],
            'info.transaction_date' => [
                'required',
                'date'
            ],
            'info.payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'info.bank_account_id' => [
                sprintf('required_if:info.payment_method,%s,%s', PaymentMethod::CHECK, PaymentMethod::BANK_DEPOSIT)
            ],
            'info.check_number' => [
                sprintf('required_if:info.payment_method,%s', PaymentMethod::CHECK)
            ],
            'info.check_date' => [
                sprintf('required_if:info.payment_method,%s', PaymentMethod::CHECK)
            ],
            'details.*.type' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'details.*.amount' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details' => [
                'array',
                'min:1'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                break;
            
            case 'PUT': 
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        $messages = array_merge(
            $this->infoMessages(), 
            $this->detailMessages(),
            $this->arrayMessages()
        );

        return $messages;
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

    /**
     * List of validation messages for the info array
     * 
     * @return array
     */
    private function infoMessages()
    {
        return [
            'info.reference_name.required' => Lang::get('core::validation.name.required'),
            'info.transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date' => Lang::get('core::validation.transaction.date.invalid.format'),
            'info.payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'info.payment_method.exists_in' => Lang::get('core::validation.payment.method.doesnt.exists'),
            'info.bank_account_id.required_if' => Lang::get('core::validation.bank.account.required'),
            'info.check_number.required_if' => Lang::get('core::validation.check.no.required'),
            'info.check_date.required_if' => Lang::get('core::validation.check.date.required')
        ];
    }

    private function detailMessages()
    {
        $messages = [];
        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);
            
            $messages[$path.'.'.$key.'.amount.required'] = Lang::get('core::error.row').'['.$number.']. '.Lang::get('core::validation.amount.required');
            $messages[$path.'.'.$key.'.type.required'] = Lang::get('core::error.row').'['.$number.']. '.Lang::get('core::validation.type.required');
            $messages[$path.'.'.$key.'.type.exists_in'] = Lang::get('core::error.row').'['.$number.']. '.Lang::get('core::validation.type.doesnt.exists');
            $messages[$path.'.'.$key.'.amount.numeric'] = Lang::get('core::error.row').'['.$number.']. '.Lang::get('core::validation.amount.numeric');
            $messages[$path.'.'.$key.'.amount.decimal_min'] = Lang::get('core::error.row').'['.$number.']. '.Lang::get('core::validation.amount.cannot.be.zero');
        }

        return $messages;
    }

     /**
     * List of validation messages for detail arrays
     * 
     * @return array
     */
    private function arrayMessages()
    {
        return array(
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one')
        );
    }
}