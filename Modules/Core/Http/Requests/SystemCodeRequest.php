<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\SystemCodeType;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Lang;

class SystemCodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => [
                Rule::unique('system_code', 'code')
                    ->where(function ($query) {
                        $query->where('type_id', $this->type())
                            ->where('deleted_at', null);
                    })
            ],
            'name' => [
                'required',
                Rule::unique('system_code', 'name')
                    ->where(function ($query) {
                        $query->where('type_id', $this->type())
                            ->where('deleted_at', null);
                    })
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['code'][0] = $rules['code'][0]->ignore($this->id());
                $rules['code'][1] = 'required';
                $rules['name'][1] = $rules['name'][1]->ignore($this->id());
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => Lang::get('core::validation.code.required'),
            'code.unique' => Lang::get('core::validation.code.unique'),
            'name.required'  => Lang::get('core::validation.name.required'),
            'name.unique' => Lang::get('core::validation.name.unique')
        ];
    }

    /**
     * Get enum type based on url. Acceptable approach because creation 
     * of these data can only be done here and will not be reused in other 
     * parts of the system.
     * 
     * @return int
     */
    private function type()
    {
        $uri = Str::upper(str_replace('-', '_', $this->segment(1)));

        return SystemCodeType::byName($uri)->getConstValue();
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}