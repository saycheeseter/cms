<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Lang;

class ProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'product.name' => [
                'required',
                'unique_in:product,name,deleted_at'
            ],
            'product.stock_no' => [
                'unique_in:product,stock_no,deleted_at'
            ],
            'product.supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
            'product.brand_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'product.category_id' => [
                'required',
                'exists_in:category,id,deleted_at'
            ],
            'product.group_type' => [
                'required'
            ],
            'product.manageable' => [
                'required'
            ],
            'units.*.uom_id' => [
                'required',
                'exists_in:system_code,id,deleted_at',
                'uom'
            ],
            'units.*.qty' => [
                'required',
                'numeric'
            ],
            'units' => [
                'array',
                'min:1'
            ],
            'prices.*.branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'prices.*.copy_from' => [
                'nullable',
                'exists_in:branch,id,deleted_at'
            ],
            'prices.*.purchase_price' => [
                'required',
                'numeric'
            ],
            'prices.*.selling_price' => [
                'required',
                'numeric'
            ],
            'prices.*.wholesale_price' => [
                'required',
                'numeric'
            ],
            'prices.*.price_a' => [
                'numeric'
            ],
            'prices.*.price_b' => [
                'numeric'
            ],
            'prices.*.price_c' => [
                'numeric'
            ],
            'prices.*.price_d' => [
                'numeric'
            ],
            'prices.*.price_e' => [
                'numeric'
            ],
            'prices' => [
                'array',
                'min:1'
            ],
            'barcodes.*.uom_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'barcodes.*.code' => [
                'barcode'
            ],
            'locations.*.min' => [
                'numeric',
            ],
            'locations.*.max' => [
                'numeric',
            ],
            'barcodes' => [
                'array',
                'min:1'
            ],
            'taxes' => [
                'array',
                'min:1'
            ],
            'branches' => [
                'array',
                'min:1'
            ],
            'locations' => [
                'array',
                'min:1'
            ],
            'alternatives' => [
                'array',
            ],
            'components' => [
                'array'
            ],
            'components.*.component_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'components.*.qty' => [
                'required',
                'numeric'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['product.name'][1] .= ','.$this->id();
                $rules['product.stock_no'][0] .= ','.$this->id();
                $rules['product.stock_no'][1] = 'required';
                $rules['barcodes.*.code'][0] .= ':'.$this->id();
                $rules['units.*.uom_id'][2] .= ':'.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the product is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = array_merge(
            $this->productMessages(),
            $this->priceMessages('prices'),
            $this->unitMessages('units'),
            $this->barcodeMessages('barcodes'),
            $this->locationMessages('locations'),
            $this->componentMessages('components'),
            $this->arrayMessages('prices', Lang::get('core::label.branch.price')),
            $this->arrayMessages('units', Lang::get('core::label.product.unit')),
            $this->arrayMessages('barcodes', Lang::get('core::label.product.unit.barcode')),
            $this->arrayMessages('taxes', Lang::get('core::label.vatable')),
            $this->arrayMessages('branches', Lang::get('core::label.branch')),
            $this->arrayMessages('locations', Lang::get('core::label.location')),
            $this->arrayMessages('alternatives', Lang::get('core::label.alternative.items')),
            $this->arrayMessages('components', Lang::get('core::label.components'))
        );

        return $messages;
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

     /**
     * List of validation messages for the product array
     *
     * @return array
     */
    private function productMessages()
    {
        return [
            'product.name.required'         => Lang::get('core::validation.name.required'),
            'product.name.unique_in'        => Lang::get('core::validation.name.unique'),
            'product.stock_no.required'     => Lang::get('core::validation.stock.no.required'),
            'product.stock_no.unique_in'    => Lang::get('core::validation.stock.no.unique'),
            'product.supplier_id.required'  => Lang::get('core::validation.supplier.required'),
            'product.category_id.required'  => Lang::get('core::validation.category.required'),
            'product.brand_id.required'     => Lang::get('core::validation.brand.required'),
            'product.supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists'),
            'product.category_id.exists_in' => Lang::get('core::validation.category.doesnt.exists'),
            'product.brand_id.exists_in'    => Lang::get('core::validation.brand.doesnt.exists'),
            'product.group_type.required'   => Lang::get('core::validation.group.type.required'),
            'product.manageable.required'   => Lang::get('core::validation.manageable.required')
        ];
    }

    /**
     * Build error messages based on the passed holder of the data
     *
     * @param  string $path
     * @return array
     */
    private function unitMessages($path)
    {
        $messages = [];

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $messages[$path.'.'.$key.'.uom_id.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.uom.required')
            );

            $messages[$path.'.'.$key.'.uom_id.exists_in'] = sprintf("%s: %s[%s]. %s",
                Lang::get('core::label.product.unit'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.uom.doesnt.exists')
            );

            $messages[$path.'.'.$key.'.uom_id.uom'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.uom.already.exists')
            );

            $messages[$path.'.'.$key.'.qty.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.qty.required')
            );

            $messages[$path.'.'.$key.'.qty.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.qty.numeric')
            );
        }

        return $messages;
    }

    /**
     * Build error messages based on the passed holder of the data
     *
     * @param  string $path
     * @return array
     */
    private function priceMessages($path)
    {
        $messages = [];

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $messages[$path.'.'.$key.'.branch_id.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.branch.required')
            );

            $messages[$path.'.'.$key.'.branch_id.exists_in'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.branch.doesnt.exists')
            );

            $messages[$path.'.'.$key.'.copy_from.exists_in'] = sprintf("%s: %s[%s]. %s",
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.copy.from.doesnt.exists')
            );

            $messages[$path.'.'.$key.'.purchase_price.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.purchase.price.required')
            );

            $messages[$path.'.'.$key.'.selling_price.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.selling.price.required')
            );
            
            $messages[$path.'.'.$key.'.wholesale_price.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.wholesale.price.required')
            );

            $messages[$path.'.'.$key.'.purchase_price.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.purchase.price.numeric')
            );

            $messages[$path.'.'.$key.'.selling_price.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.selling.price.numeric')
            );
            
            $messages[$path.'.'.$key.'.wholesale_price.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.wholesale.price.numeric')
            );

            $messages[$path.'.'.$key.'.price_a.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.price.letter.numeric', ['letter' => 'A'])
            );

            $messages[$path.'.'.$key.'.price_b.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.price.letter.numeric', ['letter' => 'B'])
            );

            $messages[$path.'.'.$key.'.price_c.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.price.letter.numeric', ['letter' => 'C'])
            );

            $messages[$path.'.'.$key.'.price_d.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.price.letter.numeric', ['letter' => 'D'])
            );

            $messages[$path.'.'.$key.'.price_e.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.branch.price'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.price.letter.numeric', ['letter' => 'E'])
            );
        }

        return $messages;
    }

    /**
     * Build error messages based on the passed holder of the data
     *
     * @param  string $path
     * @return array
     */
    private function barcodeMessages($path)
    {
        $messages = [];

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $messages[$path.'.'.$key.'.uom_id.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit.barcode'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.uom.required')
            );

            $messages[$path.'.'.$key.'.uom_id.exists_in'] = sprintf("%s: %s[%s]. %s",
                Lang::get('core::label.product.unit.barcode'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.uom.doesnt.exists')
            );

            $messages[$path.'.'.$key.'.code.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit.barcode'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.barcode.required')
            );

            $messages[$path.'.'.$key.'.code.barcode'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.unit.barcode'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.barcode.exists')
            );

        }

        return $messages;
    }

    /**
     * Build error messages based on the passed holder of the data
     *
     * @param  string $path
     * @return array
     */
    private function locationMessages($path)
    {
        $messages = [];

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $messages[$path.'.'.$key.'.min.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.location'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.min.numeric')
            );

            $messages[$path.'.'.$key.'.max.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.location'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.max.numeric')
            );
        }

        return $messages;
    }

    /**
     * Build error messages based on the passed holder of the data
     *
     * @param  string $path
     * @return array
     */
    private function componentMessages($path)
    {
        $messages = [];

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $messages[$path.'.'.$key.'.component_id.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.component'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.component.id.required')
            );

            $messages[$path.'.'.$key.'.component_id.exists_in'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.component'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.component.doesnt.exists')
            );

            $messages[$path.'.'.$key.'.qty.required'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.component'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.qty.required')
            );

            $messages[$path.'.'.$key.'.qty.numeric'] = sprintf("%s: %s[%s]. %s", 
                Lang::get('core::label.product.component'),
                Lang::get('core::error.row'),
                ($key + 1),
                Lang::get('core::validation.qty.numeric')
            );
        }

        return $messages;
    }

    /**
     * List of validation messages for detail arrays
     *
     * @param string $table
     * @param string $section
     * @return array
     */
    private function arrayMessages($table, $section)
    {
        return [
            $table.'.array' => sprintf("%s: %s",
                $section,
                Lang::get('core::validation.invalid.format')
            ),
            $table.'.min' => sprintf("%s: %s",
                $section,
                Lang::get('core::validation.details.min.one')
            ),
        ];
    }

}
