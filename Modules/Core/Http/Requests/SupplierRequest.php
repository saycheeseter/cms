<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;

class SupplierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => [
                'unique_in:supplier,code,deleted_at'
            ],
            'full_name' => [
                'required',
                'unique_in:supplier,full_name,deleted_at'
            ],
            'status' => [
                'required',
                'numeric'
            ],
            'term' => [
                'required',
                'numeric'
            ],
            'email' => [
                'email'
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['code'][0] .= ','.$this->id();
                $rules['code'][1] = 'required';
                $rules['full_name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => Lang::get('core::validation.code.required'),
            'code.unique_in' => Lang::get('core::validation.code.unique'),
            'full_name.required'  => Lang::get('core::validation.full.name.required'),
            'full_name.unique_in' => Lang::get('core::validation.full.name.unique'),
            'status.required' => Lang::get('core::validation.status.required'),
            'status.numeric' => Lang::get('core::validation.status.numeric'),
            'term.required' => Lang::get('core::validation.term.required'),
            'term.numeric' => Lang::get('core::validation.term.numeric'),
            'email.email' => Lang::get('core::validation.email.invalid')
        ];
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }
}
