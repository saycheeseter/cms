<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class MemberRateRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'info.name' => [
                'required',
                'unique_in:member_rate,name,deleted_at'
            ],
            'details.*.amount_from' => [
                'required',
                'numeric'
            ],
            'details.*.amount_to' => [
                'required',
                'numeric'
            ],
            'details.*.date_from' => [
                'required',
                'date_format:"Y-m-d"'
            ],
            'details.*.date_to' => [
                'required',
                'date_format:"Y-m-d"'
            ],
            'details.*.amount_per_point' => [
                'required',
                'numeric'
            ],
            'details.*.discount' => [
                'required',
                'numeric'
            ],
            'details' => [
                'required',
                'array',
            ]
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['info.name'][1] .= ','.$this->id();
                break;
            
            case 'PUT': 
                break;
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        $messages = array_merge(
            $this->infoMessages(), 
            $this->detailMessages(),
            $this->arrayMessages()
        );

        return $messages;
    }

    /**
     * Get id of data
     * 
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

    /**
     * List of validation messages for the info array
     * 
     * @return array
     */
    private function infoMessages()
    {
        return [
            'info.name.required'  => Lang::get('core::validation.name.required'),
            'info.name.unique_in' => Lang::get('core::validation.name.unique')
        ];
    }

    private function detailMessages()
    {
        return $this->createArrayMessages('details', [
            'amount_from.required'      => Lang::get('core::validation.details.amount.from.required'),
            'amount_from.numeric'       => Lang::get('core::validation.details.amount.from.invalid.format'),
            'amount_to.required'        => Lang::get('core::validation.details.amount.to.required'),
            'amount_to.numeric'         => Lang::get('core::validation.details.amount.to.invalid.format'),
            'date_from.required'        => Lang::get('core::validation.details.date.from.required'),
            'date_from.date_format'     => Lang::get('core::validation.details.date.from.invalid.format'),
            'date_to.required'          => Lang::get('core::validation.details.date.to.required'),
            'date_to.date_format'       => Lang::get('core::validation.details.date.to.invalid.format'),
            'amount_per_point.required' => Lang::get('core::validation.details.amount.per.point.required'),
            'amount_per_point.numeric'  => Lang::get('core::validation.details.amount.per.point.invalid.format'),
            'discount.required'         => Lang::get('core::validation.details.discount.required'),
            'discount.numeric'          => Lang::get('core::validation.details.discount.invalid.format'),
        ]);
    }

     /**
     * List of validation messages for detail arrays
     * 
     * @return array
     */
    private function arrayMessages()
    {
        return [
            'details.array'    => Lang::get('core::validation.invalid.format'),
            'details.required' => Lang::get('core::validation.details.min.one')
        ];
    }
}