<?php

namespace Modules\Core\Http\Requests;

use Lang;

class StockDeliveryOutboundApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [];
    }

    protected function extendedMessages()
    {
        return [];
    }
}
