<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\SystemCodeType;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Lang;

class BarcodeScanRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'barcode' => 'required',
            'scheme' => 'required|digits_range:0,3'
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'barcode.required' => Lang::get('core::validation.barcode.required'),
            'scheme.required' => Lang::get('core::validation.price.scheme.required'),
            'scheme.digits_range'  => Lang::get('core::validation.price.scheme.invalid'),
        ];
    }
}