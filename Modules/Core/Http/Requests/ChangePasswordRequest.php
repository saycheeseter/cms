<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'old_password' => [
                'required'
            ],
            'new_password' => [
                'required',
                'confirmed',
                'min:6'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'old_password.required'  => Lang::get('core::validation.old.password.required'),
            'new_password.required'  => Lang::get('core::validation.new.password.required'),
            'new_password.confirmed' => Lang::get('core::validation.confirm.password.incorrect'),
            'new_password.min'       => Lang::get('core::validation.new.password.min.length'),
        ];
    }
}