<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ImportExcelRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'attachment' => [
                'max:2000000',
                'mimes:xlt,xlsx,xls,xltx'
            ],
        ];


        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'attachment.max' => Lang::get('core::validation.file.over.allowed.size'),
            'attachment.mimes' => Lang::get('core::validation.invalid.file.type')
        ];
    }
}