<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Modules\Core\Enums\TargetType;
use Modules\Core\Enums\SelectType;

class ProductDiscountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            $this->infoRules(),
            $this->detailRules(),
            $this->targetRules(),
            $this->arrayRules()
        );
    }

    protected function infoRules()
    {
        $rules = [
            'info.code' => [
                'required',
                'unique_in:product_discount,code,deleted_at',
                'string'
            ],
            'info.name' => [
                'required',
                'unique_in:product_discount,name,deleted_at',
                'string'
            ],
            'info.valid_from' => [
                'required',
                'date'
            ],
            'info.valid_to' => [
                'required',
                'date'
            ],
            'info.status' => [
                'required'
            ],
            'info.mon' => [
                'required'
            ],
            'info.tue' => [
                'required'
            ],
            'info.wed' => [
                'required'
            ],
            'info.thur' => [
                'required'
            ],
            'info.fri' => [
                'required'
            ],
            'info.sat' => [
                'required'
            ],
            'info.sun' => [
                'required'
            ],
            'info.status' => [
                'required'
            ],
            'info.target_type' => [
                'required',
                'numeric'
            ],
            'info.discount_scheme' => [
                'required',
                'numeric'
            ],
            'info.select_type' => [
                'required',
                'numeric'
            ],
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
        ];

        switch ($this->method()) {
            case 'PATCH':
                $rules['info.code'][1] .= ','.$this->id();
                $rules['info.name'][1] .= ','.$this->id();
                break;

            case 'PUT':
                # code...
                break;
        }

        return $rules;
    }

    /**
     * Get id of data
     *
     * @return int
     */
    private function id()
    {
        return $this->segment(2);
    }

    protected function detailRules()
    {
        return [
            'details.*.entity_type' => [
                'required'
            ],
            'details.*.discount_type' => [
                'required',
                'numeric'
            ],
            'details.*.discount_value' => [
                'required',
                'numeric'
            ],
            'details.*.qty' => [
                'required',
                'numeric'
            ],
        ];
    }

    protected function targetRules()
    {
        return [
            'targets.*.target_id' => [
                'required',
                'numeric'
            ],
            'targets.*.target_type' => [
                'required',
                'numeric'
            ]
        ];
    }

    protected function arrayRules()
    {
        return [
            'details' => [
                'required',
                'array',
                'min:1'
            ],
            'targets' => [
                'required_if:info.target_type,'.TargetType::SPECIFIC_MEMBER_TYPE.'|'.
                'required_if:info.target_type,'.TargetType::SPECIFIC_MEMBER.'|'.
                'required_if:info.target_type,'.TargetType::SPECIFIC_CUSTOMER,
                'array'//,
                // 'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->infoMessages(),
            $this->detailMessages(),
            $this->targetMessages(),
            $this->arrayMessages()
        );
    }

    protected function infoMessages()
    {
        return [
            'info.code.required' => Lang::get('core::validation.code.required'),
            'info.code.string' => Lang::get('core::validation.code.string'),
            'info.code.unique_in' => Lang::get('core::validation.code.unique'),
            'info.name.required' => Lang::get('core::validation.name.required'),
            'info.name.string' => Lang::get('core::validation.name.string'),
            'info.name.unique_in' => Lang::get('core::validation.name.unique'),
            'info.valid_from.required' => Lang::get('core::validation.valid_from.required'),
            'info.valid_from.date' => Lang::get('core::validation.valid_from.date'),
            'info.valid_to.required' => Lang::get('core::validation.valid_to.required'),
            'info.valid_to.date' => Lang::get('core::validation.valid_to.date'),
            'info.status.required' => Lang::get('core::validation.status.required'),
            'info.mon.required' => Lang::get('core::validation.mon.required'),
            'info.tue.required' => Lang::get('core::validation.tue.required'),
            'info.wed.required' => Lang::get('core::validation.wed.required'),
            'info.thur.required' => Lang::get('core::validation.thur.required'),
            'info.fri.required' => Lang::get('core::validation.fri.required'),
            'info.sat.required' => Lang::get('core::validation.sat.required'),
            'info.sun.required' => Lang::get('core::validation.sun.required'),
            'info.status.required' => Lang::get('core::validation.status.required'),
            'info.target_type.required' => Lang::get('core::validation.target.type.required'),
            'info.target_type.numeric' => Lang::get('core::validation.target.type.numeric'),
            'info.discount_scheme.required' => Lang::get('core::validation.discount.scheme.required'),
            'info.discount_scheme.numeric' => Lang::get('core::validation.discount.scheme.numeric'),
            'info.select_type.required' => Lang::get('core::validation.select.type.required'),
            'info.select_type.numeric' => Lang::get('core::validation.select.type.numeric'),
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists')
        ];
    }

    protected function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.entity_type.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.entity.type.required')
            );

            $messages[sprintf('%s.%s.discount_type.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.type.required')
            );

            $messages[sprintf('%s.%s.discount_type.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.type.numeric')
            );

            $messages[sprintf('%s.%s.discount_value.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.value.required')
            );

            $messages[sprintf('%s.%s.discount_value.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.value.numeric')
            );

            $messages[sprintf('%s.%s.qty.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.required')
            );

            $messages[sprintf('%s.%s.qty.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.numeric')
            );
        }

        return $messages;
    }

    protected function targetMessages()
    {
        $messages = [];

        $path = 'targets';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.target_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.target.id.required')
            );

            $messages[sprintf('%s.%s.target_id.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.target.id.numeric')
            );

            $messages[sprintf('%s.%s.target_type.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.target.type.required')
            );

            $messages[sprintf('%s.%s.target_type.numeric', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.target.type.numeric')
            );
        }

        return $messages;
    }

    protected function arrayMessages()
    {
        return [
            'details.required' => Lang::get('core::validation.details.required'),
            'details.array' => Lang::get('core::validation.details.array'),
            'details.min' => Lang::get('core::validation.details.min.one'),
            'targets.required_if' => Lang::get('core::validation.targets.required'),
            'targets.array' => Lang::get('core::validation.targets.array'),
            'targets.min' => Lang::get('core::validation.targets.min'),
        ];
    }
}