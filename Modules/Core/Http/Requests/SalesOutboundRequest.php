<?php

namespace Modules\Core\Http\Requests;

use Modules\Core\Enums\Customer;
use Lang;

class SalesOutboundRequest extends InventoryChainRequest
{
    protected $reference = [
        'info' => 'sales',
        'detail' => 'sales_detail',
    ];

    public function extendedRules()
    {
        return [
            'info.customer_id' => [
                sprintf('required_if:info.customer_type,%s', Customer::REGULAR),
            ],
            'info.customer_detail_id' => [
                sprintf('required_if:info.customer_type,%s', Customer::REGULAR),
            ],
            'info.customer_walk_in_name' => [
                sprintf('required_if:info.customer_type,%s', Customer::WALK_IN)
            ],
            'info.customer_type' => [
                'required',
                'numeric'
            ],
            'info.salesman_id' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'info.term' => [
                'numeric',
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.customer_id.required_if' => Lang::get('core::validation.customer.required'),
            'info.customer_detail_id.required_if' => Lang::get('core::validation.customer.detail.required'),
            'info.customer_walk_in_name.required_if' => Lang::get('core::validation.customer.walk.in.name.required'),
            'info.customer_type.required' => Lang::get('core::validation.customer.type.required'),
            'info.customer_type.numeric' => Lang::get('core::validation.customer.type.numeric'),
            'info.term.numeric' => Lang::get('core::validation.term.numeric'),
            'info.salesman_id.required' => Lang::get('core::validation.salesman.required'),
            'info.salesman_id.exists_in' => Lang::get('core::validation.salesman.doesnt.exists'),
        ];
    }
}
