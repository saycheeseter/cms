<?php 

namespace Modules\Core\Http\Requests;

use Modules\Core\Enums\Customer;
use Lang;

class SalesReturnApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [
            'salesman_id' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'term' => [
                'numeric',
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'term.numeric' => Lang::get('core::validation.term.numeric'),
            'salesman_id.required' => Lang::get('core::validation.salesman.required'),
            'salesman_id.exists_in' => Lang::get('core::validation.salesman.doesnt.exists'),
        ]; 
    }
}