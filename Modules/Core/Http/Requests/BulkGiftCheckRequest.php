<?php

namespace Modules\Core\Http\Requests;

use Lang;

class BulkGiftCheckRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code_series' => [
                'required',
            ],
            'check_number_series' => [
                'required',
            ],
            'amount' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'date_from' => [
                'required',
                'date_format:"Y-m-d"',
                'before_or_equal:date_to'
            ],
            'date_to' => [
                'required',
                'date_format:"Y-m-d"',
                'after_or_equal:date_from'
            ],
            'qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code_series.required'         => Lang::get('core::validation.code.required'),
            'check_number_series.required' => Lang::get('core::validation.check.number.required'),
            'amount.required'              => Lang::get('core::validation.amount.required'),
            'amount.numeric'               => Lang::get('core::validation.amount.numeric'),
            'amount.decimal_min'           => Lang::get('core::validation.amount.greater.than.zero'),
            'qty.required'                 => Lang::get('core::validation.qty.required'),
            'qty.numeric'                  => Lang::get('core::validation.qty.numeric'),
            'qty.decimal_min'              => Lang::get('core::validation.qty.greater.than.zero'),
            'date_from.required'           => Lang::get('core::validation.date.from.required'),
            'date_from.date_format'        => Lang::get('core::validation.date.from.invalid.format'),
            'date_from.before_or_equal'    => Lang::get('core::validation.date.from.invalid.range'),
            'date_to.required'             => Lang::get('core::validation.date.to.required'),
            'date_to.date_format'          => Lang::get('core::validation.date.to.invalid.format'),
            'date_to.after_or_equal'       => Lang::get('core::validation.date.to.invalid.range'),
        ];
    }
}