<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

abstract class UpdateApprovedTransactionRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->transactionRules(),
            $this->extendedRules()
        );
    }

    protected function transactionRules()
    {
        return [
            'requested_by' => [
                'required',
                'exists_in:user,id,deleted_at'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function transactionMessages()
    {
        return [
            'requested_by.required' => Lang::get('core::validation.request_by.required'),
            'requested_by.exists_in' => Lang::get('core::validation.user.doesnt.exists')
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->transactionMessages(),
            $this->extendedMessages()
        );
    }

    abstract protected function extendedRules();
    abstract protected function extendedMessages();
}