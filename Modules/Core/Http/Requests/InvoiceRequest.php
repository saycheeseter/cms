<?php 

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Lang;

abstract class InvoiceRequest extends FormRequest
{
    public function rules()
    {
        return array_merge(
            $this->transactionRules(),
            $this->detailRules(),
            $this->extendedRules()
        );
    }

    protected function transactionRules()
    {
        return [
            'info.requested_by' => [
                'required',
                'exists_in:user,id,deleted_at'
            ],
            'info.created_for' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.transaction_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ]
        ];
    }

    protected function detailRules()
    {
        return [
            'details.*.product_id' => [
                'required',
                'exists_in:product,id,deleted_at'
            ],
            'details.*.unit_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'details.*.unit_qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details.*.qty' => [
                'required',
                'numeric',
                'decimal_min'
            ],
            'details.*.oprice' => [
                'required',
                'numeric'
            ],
            'details.*.price' => [
                'required',
                'numeric'
            ],
            'details.*.discount1' => [
                'numeric'
            ],
            'details.*.discount2' => [
                'numeric'
            ],
            'details.*.discount3' => [
                'numeric'
            ],
            'details.*.discount4' => [
                'numeric'
            ],
            'details.*.group_type' => [
                'required'
            ],
            'details.*.product_type' => [
                'required'
            ],
            'details.*.manageable' => [
                'required'
            ],
            'details' => [
                'array',
                'min:1'
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->transactionMessages(),
            $this->detailMessages(),
            $this->extendedMessages(),
            $this->arrayMessages()
        );
    }

    protected function transactionMessages()
    {
        return [
            'info.requested_by.required' => Lang::get('core::validation.request_by.required'),
            'info.requested_by.exists_in' => Lang::get('core::validation.user.doesnt.exists'),
            'info.created_for.required' => Lang::get('core::validation.created.for.required'),
            'info.created_for.exists_in' => Lang::get('core::validation.created.for.doesnt.exists'),
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.transaction_date.required' => Lang::get('core::validation.transaction.date.required'),
            'info.transaction_date.date_format' => Lang::get('core::validation.transaction.date.invalid.format'),
        ];
    }

    protected function detailMessages()
    {
        $messages = [];

        $path = 'details';

        if (!$this->input($path) || !is_array($this->input($path))) {
            return $messages;
        }

        foreach ($this->input($path) as $key => $value) {
            $number = ($key + 1);

            $messages[sprintf('%s.%s.product_id.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.required')
            );

            $messages[sprintf('%s.%s.product_id.exists_in', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_id.required', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.required')
            );

            $messages[sprintf('%s.%s.unit_id.exists_in', $path, $key)] = sprintf('%s[%u] %s',
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.doesnt.exists')
            );

            $messages[sprintf('%s.%s.unit_qty.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.required')
            );

            $messages[sprintf('%s.%s.unit_qty.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.numeric')
            );

            $messages[sprintf('%s.%s.unit_qty.decimal_min', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.unit.qty.greater.than.zero')
            );

            $messages[sprintf('%s.%s.qty.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.required')
            );

            $messages[sprintf('%s.%s.qty.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.numeric')
            );

            $messages[sprintf('%s.%s.qty.decimal_min', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.qty.greater.than.zero')
            );

            $messages[sprintf('%s.%s.oprice.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.oprice.required')
            );

            $messages[sprintf('%s.%s.oprice.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.oprice.numeric')
            );

            $messages[sprintf('%s.%s.price.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.price.required')
            );

            $messages[sprintf('%s.%s.price.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.price.numeric')
            );

            $messages[sprintf('%s.%s.discount1.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.numeric', ['num' => '1'])
            );

            $messages[sprintf('%s.%s.discount2.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.numeric', ['num' => '2'])
            );

            $messages[sprintf('%s.%s.discount3.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.numeric', ['num' => '3'])
            );

            $messages[sprintf('%s.%s.discount4.numeric', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.discount.numeric', ['num' => '4'])
            );

            $messages[sprintf('%s.%s.group_type.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.group.type.required')
            );

            $messages[sprintf('%s.%s.product_type.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.product.type.required')
            );

            $messages[sprintf('%s.%s.manageable.required', $path, $key)] = sprintf('%s[%u] %s', 
                Lang::get('core::error.row'),
                $number,
                Lang::get('core::validation.manageable.required')
            );
        }

        return $messages;
    }

    protected function arrayMessages()
    {
        return array(
            'details.array' => Lang::get('core::validation.invalid.format'),
            'details.min' => Lang::get('core::validation.details.min.one')
        );
    }

    abstract protected function extendedRules();
    abstract protected function extendedMessages();
}