<?php

namespace Modules\Core\Http\Requests;

use Lang;

class DamageApprovedRequest extends UpdateApprovedTransactionRequest
{
    public function extendedRules()
    {
        return [
            'reason_id' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'reason_id.required' => Lang::get('core::validation.reason.required'),
            'reason_id.exists_in' => Lang::get('core::validation.reason.doesnt.exists'),
        ];
    }
}
