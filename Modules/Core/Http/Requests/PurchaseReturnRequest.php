<?php 

namespace Modules\Core\Http\Requests;

use Lang;

class PurchaseReturnRequest extends InvoiceRequest
{
    public function extendedRules()
    {
        return [
            'info.supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
            'info.payment_method' => [
                'required',
                'exists_in:system_code,id,deleted_at'
            ],
            'info.term' => [
                'numeric',
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.supplier_id.required' => Lang::get('core::validation.supplier.required'),
            'info.supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists'),
            'info.payment_method.required' => Lang::get('core::validation.payment.method.required'),
            'info.payment_method.exists_in' => Lang::get('core::validation.payment.method.doesnt.exists'),
            'info.term.numeric' => Lang::get('core::validation.term.numeric'),
        ]; 
    }
}