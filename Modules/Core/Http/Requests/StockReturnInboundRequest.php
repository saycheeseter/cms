<?php

namespace Modules\Core\Http\Requests;

use Modules\Core\Enums\TransactionType;
use Lang;

class StockReturnInboundRequest extends InventoryChainRequest
{
    protected $reference = [
        'info' => 'stock_return_outbound',
        'detail' => 'stock_return_outbound_detail',
    ];

    public function extendedRules()
    {
        return [
            'info.delivery_from' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'info.delivery_from_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.delivery_from.required' => Lang::get('core::validation.delivery.from.required'),
            'info.delivery_from_location.required' => Lang::get('core::validation.delivery.from.location.required'),
            'info.delivery_from.exists_in' => Lang::get('core::validation.delivery.from.doesnt.exists'),
            'info.delivery_from_location.exists_in' => Lang::get('core::validation.delivery.from.location.doesnt.exists'),
        ];
    }
}
