<?php

namespace Modules\Core\Http\Requests;

use Lang;

class StockRequestRequest extends InvoiceRequest
{
    public function extendedRules()
    {
        return [
            'info.for_location' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'info.request_to' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
        ];
    }

    protected function extendedMessages()
    {
        return [
            'info.for_location.required' => Lang::get('core::validation.for.location.required'),
            'info.for_location.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'info.request_to.required' => Lang::get('core::validation.request.to.required'),
            'info.request_to.exists_in' => Lang::get('core::validation.request.to.doesnt.exists'),
        ];
    }
}
