<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Services\Contracts\BrandServiceInterface;
use Modules\Core\Entities\Brand;
use Lang;
use Modules\Core\Services\Contracts\Import\BrandImportExcelServiceInterface;


class BrandController extends SystemCodeController
{
    public function __construct(BrandRepository $repository, BrandServiceInterface $service, BrandImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = Brand::class;
        $this->title = Lang::get('core::label.brand');
    }
}
