<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Services\Contracts\BranchServiceInterface;
use Modules\Core\Http\Requests\BranchRequest;
use Modules\Core\Transformers\Branch\BasicTransformer;
use Modules\Core\Transformers\Branch\FullInfoTransformer;
use Modules\Core\Transformers\BranchDetail\BasicTransformer as DetailTransformer;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as DetailChosenTransformer;
use Modules\Core\Entities\Branch;
use Fractal;
use Lang;
use Exception;
use Auth;

class BranchController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(BranchRepository $repository, BranchServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Branch::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'branches'),
                $message
            );
        }

        $this->authorize('view', $this->entity);
        
        $permissions = $this->permissions();

        return view('core::branch.list', compact('permissions'));
    }

    /**
     * Find the data by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   
        $this->authorize('viewDetail', $this->entity);

        $branch = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$branch) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $details = $this->paginate($branch->details, 10);

        return $this->successfulResponse(array(
            'branch'  => $this->item($branch, FullInfoTransformer::class),
            'details' => $this->collection($details, DetailTransformer::class)['data']
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  BranchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BranchRequest $request)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $this->authorize('store', $this->entity);
        
        $result = $this->service->store($this->data($request));

        if (!$result) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'branch' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BranchRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BranchRequest $request, $id)
    {  
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        } 
        
        return $this->successfulResponse([
            'branch' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Get the branch detail of the specified branch
     * and transform the result into a chosen selection 
     * structure
     * 
     * @param  int $id 
     * @return \Illuminate\Http\JsonResponse
     */
    public function details($id)
    {
        $branch = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$branch) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse(array(
            'details' => $this->collection($branch->details, DetailChosenTransformer::class)['data']
        ));
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    protected function permissions()
    {
        return [
            'view'        => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update'      => Auth::user()->can('update', $this->entity),
            'delete'      => Auth::user()->can('delete', $this->entity),
            'create'      => Auth::user()->can('store', $this->entity),
        ];
    }
}
