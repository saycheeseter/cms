<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\PurchaseReturnOutboundRepository;
use Modules\Core\Services\Contracts\PurchaseReturnOutboundServiceInterface;
use Modules\Core\Transformers\PurchaseReturnOutbound\BasicTransformer;
use Modules\Core\Transformers\PurchaseReturnOutbound\FullInfoTransformer;
use Modules\Core\Transformers\PurchaseReturnOutbound\ExportListTransformer;
use Modules\Core\Transformers\PurchaseReturnOutbound\PaymentTransformer;
use Modules\Core\Transformers\PurchaseReturnOutbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Http\Requests\PurchaseReturnOutboundRequest;
use Modules\Core\Http\Requests\PurchaseReturnOutboundApprovedRequest;
use Modules\Core\Rules\Contracts\PurchaseReturnOutboundRuleInterface as PurchaseReturnOutboundRule;
use Modules\Core\Services\Contracts\Export\PurchaseReturnOutboundExportServiceInterface as PurchaseReturnOutboundExport;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Lang;
use App;

class PurchaseReturnOutboundController extends InventoryChainController
{
    public function __construct(PurchaseReturnOutboundRepository $repository, PurchaseReturnOutboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = PurchaseReturnOutboundExport::class;
        $this->rule = PurchaseReturnOutboundRule::class;
        $this->request['transaction'] = PurchaseReturnOutboundRequest::class;
        $this->request['info'] = PurchaseReturnOutboundApprovedRequest::class;
        $this->entity = PurchaseReturnOutbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::PURCHASE_RETURN_OUTBOUND;
        $this->eagerLoad = [
            'supplier',
            'payment',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    public function transactionsWithBalance(Request $request)
    {
        $result = $this->repository->transactionsWithBalance(
            $request->get('branch_id'),
            $request->get('supplier_id'),
            $request->get('per_page', 10)
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, BasicTransformer::class)
        ]);
    }

    public function getPayableTransactions(Request $request)
    {
        $result = $this->repository->findMany(
            $request->get('ids')
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, PaymentTransformer::class)
        ]);
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(PaymentMethodRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($tsuppliers, $paymentMethods, $udfs, $permissions, $templates) = $this->references();

        return view('core::purchase-return-outbound.detail',
            compact('tsuppliers', 'paymentMethods', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::purchase-return-outbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
