<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\StockReturnInboundRepository;
use Modules\Core\Services\Contracts\StockReturnInboundServiceInterface;
use Modules\Core\Transformers\InventoryChain\ChosenTransformer;
use Modules\Core\Transformers\StockReturnInbound\BasicTransformer;
use Modules\Core\Transformers\StockReturnInbound\FullInfoTransformer;
use Modules\Core\Transformers\StockReturnInbound\ExportListTransformer;
use Modules\Core\Transformers\StockReturnInbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\StockReturnInboundRequest;
use Modules\Core\Http\Requests\StockReturnInboundApprovedRequest;
use Modules\Core\Rules\Contracts\StockReturnInboundRuleInterface as StockReturnInboundRule;
use Modules\Core\Services\Contracts\Export\StockReturnInboundExportServiceInterface as StockReturnInboundExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\StockReturnInbound;
use Lang;
use App;

class StockReturnInboundController extends InventoryChainController
{
     public function __construct(StockReturnInboundRepository $repository, StockReturnInboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockReturnInboundExport::class;
        $this->rule = StockReturnInboundRule::class;
        $this->request['transaction'] = StockReturnInboundRequest::class;
        $this->request['info'] = StockReturnInboundApprovedRequest::class;
        $this->entity = StockReturnInbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->transformers['reference'] = ChosenTransformer::class;
        $this->module = ModuleDefinedFields::STOCK_RETURN_INBOUND;
        $this->eagerLoad = [
            'deliveryFrom',
            'deliveryFromLocation',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::stock-return-inbound.detail', 
            compact('udfs','permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::stock-return-inbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
