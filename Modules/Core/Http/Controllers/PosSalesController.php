<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Eloquent\PosSalesRepositoryEloquent;
use Modules\Core\Transformers\Branch\ChosenTransformer;
use Modules\Core\Transformers\PosSalesDetail\TransactionTransformer;
use Modules\Core\Transformers\PosSales\FullTransformer as PosSalesTransformer;
use Modules\Core\Transformers\PosSalesDetail\FullTransformer as PosSalesDetailTransformer;
use Lang;
use App;

class PosSalesController extends Controller
{
    private $repository;

    public function __construct(PosSalesRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->withTrashed()
                ->with([
                    'for',
                    'location',
                    'cashier',
                    'salesman',
                    'customer',
                    'member'
                ])
                ->orderBy('or_number', 'ASC')
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, PosSalesTransformer::class),
                $message
            );
        }

        $this->checkPermission('view_pos_sales_report');

        return view('core::pos.list', [
            'branches' => $this->collection(App::make(BranchRepository::class)->all(), ChosenTransformer::class)['data']
        ]);
    }

    public function show(Request $request, $id)
    {
        $this->checkPermission('view_pos_sales_report');

        if ($request->wantsJson()) {
            $transaction = $this->repository->find($id);

            $transaction->load([
                'details.product',
                'details.barcodes',
                'details.unit',
            ]);

            return $this->successfulResponse([
                'info'    => $this->item($transaction, PosSalesTransformer::class),
                'details' => $this->collection($transaction->details, PosSalesDetailTransformer::class)
            ]);
        }
    }

    public function view($id)
    {
        $this->checkPermission('view_pos_sales_report');

        return view('core::pos.detail');
    }

    /**
     * Get all the detail transaction of the product provided
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productTransactions(Request $request)
    {
        $transactions = $this->repository->findTransactionsByProduct(
            $request->get('product_id'),
            $request->get('per_page', 10)
        );

        if($transactions->isEmpty()) {
            return $this->errorResponse(array(
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ));
        }

        return $this->successfulResponse($this->collection($transactions, TransactionTransformer::class));
    }
}