<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\PurchaseReturnRepository;
use Modules\Core\Services\Contracts\PurchaseReturnServiceInterface;
use Modules\Core\Transformers\PurchaseReturn\BasicTransformer;
use Modules\Core\Transformers\PurchaseReturn\FullInfoTransformer;
use Modules\Core\Transformers\PurchaseReturn\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\PurchaseReturn\ExportListTransformer;
use Modules\Core\Transformers\PurchaseReturn\RetrieveTransformer;
use Modules\Core\Http\Requests\PurchaseReturnRequest;
use Modules\Core\Http\Requests\PurchaseReturnApprovedRequest;
use Modules\Core\Rules\Contracts\PurchaseReturnRuleInterface as PurchaseReturnRule;
use Modules\Core\Services\Contracts\Export\PurchaseReturnExportServiceInterface as PurchaseReturnExport;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\PurchaseReturn;
use Lang;
use App;

class PurchaseReturnController extends InvoiceController
{
    public function __construct(PurchaseReturnRepository $repository, PurchaseReturnServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = PurchaseReturnExport::class;
        $this->rule = PurchaseReturnRule::class;
        $this->request['transaction'] = PurchaseReturnRequest::class;
        $this->request['info'] = PurchaseReturnApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = PurchaseReturn::class;
        $this->module = ModuleDefinedFields::PURCHASE_RETURN;
        $this->eagerLoad = [
            'supplier',
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'requested',
            'payment'
        ];
    }

    /**
     * List of module references
     * 
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(PaymentMethodRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {
        list(
            $tsuppliers, 
            $paymentMethods, 
            $udfs, 
            $permissions,
            $templates) 
        = $this->references();

        return view('core::purchase-return.detail', 
            compact(
                'tsuppliers', 
                'paymentMethods', 
                'udfs', 
                'permissions',
                'templates'
            )
        );
    }

    protected function list()
    {
        return view('core::purchase-return.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
