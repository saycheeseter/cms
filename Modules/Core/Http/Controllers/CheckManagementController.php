<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Requests\CheckManagementRequest;
use Modules\Core\Transformers\CheckManagement\BasicTransformer;

use Illuminate\Http\Request;

use Lang;
use App;
use Auth;

abstract class CheckManagementController Extends Controller
{
    protected $repository;
    protected $service;
    protected $request;
    protected $form;

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'checks'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view($this->form, compact('permissions'));
    }

    public function transfer(CheckManagementRequest $request)
    {
        try {
            $result = $this->service->transfer($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'transfer.failed' => Lang::get('core::error.transfer.failed')
            ));
        }

        $this->authorize('transfer', $this->entity);

        return $this->successfulResponse([], Lang::get('core::success.transferred'));
    }

    private function data($request)
    {
        return $request->only(
            'ids',
            'transfer_date'
        );
    }

    protected function permissions()
    {
        return [
            'view'            => Auth::user()->can('view', $this->entity),
            'transfer'        => Auth::user()->can('transfer', $this->entity),
            'column_settings' => Auth::user()->can('useColumnSettings', $this->entity)
        ];
    }
}