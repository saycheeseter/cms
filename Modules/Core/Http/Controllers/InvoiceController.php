<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Transformers\Invoice\BasicTransformer;
use Modules\Core\Transformers\InvoiceDetail\ImportTransformer;
use Modules\Core\Transformers\InvoiceDetail\RetrieveTransformer;
use Modules\Core\Transformers\InvoiceDetail\PrintTransformer;
use Modules\Core\Transformers\InventoryFlow\FooterPrintTransformer as FooterTransformer;
use Modules\Core\Transformers\InvoiceDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\Invoice\ChosenTransformer as InvoiceChosenTransformer;
use App;
use Lang;
use Auth;

abstract class InvoiceController extends InventoryFlowController
{
    protected $key = [
        'list' => 'invoices',
        'transaction' => 'invoice'
    ];

    protected $transformers = [
        'retrieve' => [
            'info'   => BasicTransformer::class,
            'detail' => RetrieveTransformer::class,
        ],
        'print'    => [
            'info'   => '',
            'detail' => PrintTransformer::class,
            'footer' => FooterTransformer::class
        ],
        'import'   => ImportTransformer::class,
        'full'     => BasicTransformer::class,
        'basic'    => BasicTransformer::class,
        'detail'   => DetailFullInfoTransformer::class,
    ];

    /**
     * Import/Retrieve transaction details by reference number
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remaining(Request $request)
    {
        $sheetNumber = $request->get('sheet_number');
        $branch = $request->get('branch');

        $invoices  = $this->repository->findRemaining($branch, $sheetNumber);

        if($invoices->count() === 0) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        return $this->successfulResponse([
            $this->key['list'] => $this->collection($invoices, InvoiceChosenTransformer::class)['data']
        ]);
    }

    /**
     * Retrieve invoice details
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieve(Request $request)
    {
        ini_set('max_execution_time', 240);

        $invoice = $this->repository->retrieve($request->get('id'));

        if(!$invoice) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        if($invoice->details->count() === 0) {
            return $this->errorResponse(array(
                'no.remaining.products.found' => Lang::get('core::error.no.remaining.products.found')
            ));
        }

        $details = $this->paginate($invoice->details, 10);

        return $this->successfulResponse([
            $this->key['transaction'] => $this->item($invoice, $this->transformers['retrieve']['info']),
            'details'                 => $this->collection($details, $this->transformers['retrieve']['detail']),
        ], Lang::get('core::success.transaction.successfully.imported'));
    }
}
