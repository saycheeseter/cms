<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\StockReturnRepository;
use Modules\Core\Services\Contracts\StockReturnServiceInterface;
use Modules\Core\Transformers\StockReturn\BasicTransformer;
use Modules\Core\Transformers\StockReturn\FullInfoTransformer;
use Modules\Core\Transformers\StockReturn\ExportListTransformer;
use Modules\Core\Transformers\StockReturn\RetrieveTransformer;
use Modules\Core\Transformers\StockReturn\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\StockReturnRequest;
use Modules\Core\Http\Requests\StockReturnApprovedRequest;
use Modules\Core\Rules\Contracts\StockReturnRuleInterface as StockReturnRule;
use Modules\Core\Services\Contracts\Export\StockReturnExportServiceInterface as StockReturnExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\StockReturn;
use Lang;
use App;
use Auth;

class StockReturnController extends InvoiceController
{
    public function __construct(StockReturnRepository $repository, StockReturnServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockReturnExport::class;
        $this->rule = StockReturnRule::class;
        $this->request['transaction'] = StockReturnRequest::class;
        $this->request['info'] = StockReturnApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = StockReturn::class;
        $this->module = ModuleDefinedFields::STOCK_RETURN;
        $this->eagerLoad = [
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'deliverTo',
            'deliverToLocation',
            'requested',
        ];
    }
    
    /**
     * List of module references
     * 
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::stock-return.detail', 
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::stock-return.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
