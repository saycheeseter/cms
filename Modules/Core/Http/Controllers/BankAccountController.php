<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Services\Contracts\BankAccountServiceInterface;
use Modules\Core\Http\Requests\BankAccountRequest;
use Modules\Core\Transformers\BankAccount\BasicTransformer;
use Modules\Core\Repositories\Contracts\BankRepository;
use Modules\Core\Transformers\Bank\ChosenTransformer as BankChosenTransformer;
use Modules\Core\Entities\BankAccount;
use App;
use Fractal;
use Lang;
use Exception;
use Auth;

class BankAccountController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(BankAccountRepository $repository, BankAccountServiceInterface $service)
    {   
        $this->entity = BankAccount::class;
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'accounts'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        $banks = $this->collection(App::make(BankRepository::class)->all(), BankChosenTransformer::class)['data'];

        return view('core::bank-account.list', compact('banks', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BankAccountRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BankAccountRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch(Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'account' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BankAccountRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BankAccountRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'account' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'bank_id',
            'account_name',
            'account_number',
            'opening_balance'
        );
    }

    protected function permissions()
    {
        return [
            'view'   => Auth::user()->can('view', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }
}
