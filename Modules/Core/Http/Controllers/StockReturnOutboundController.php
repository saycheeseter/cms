<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Repositories\Contracts\StockReturnOutboundRepository;
use Modules\Core\Repositories\Contracts\StockReturnOutboundDetailRepository;
use Modules\Core\Services\Contracts\StockReturnOutboundServiceInterface;
use Modules\Core\Transformers\StockReturnOutbound\BasicTransformer;
use Modules\Core\Transformers\StockReturnOutbound\FullInfoTransformer;
use Modules\Core\Transformers\StockReturnOutbound\ExportListTransformer;
use Modules\Core\Transformers\StockReturnOutbound\RetrieveTransformer;
use Modules\Core\Transformers\StockReturnOutbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\StockReturnOutboundDetail\ProductBatchTransformer as BatchBasicTransformer;
use Modules\Core\Transformers\InventoryChainDetail\RetrieveTransformer as RetrieveDetailTransformer;
use Modules\Core\Transformers\InventoryChain\ChosenTransformer as StockReturnOutboundChosenTransformer;
use Modules\Core\Http\Requests\StockReturnOutboundRequest;
use Modules\Core\Http\Requests\StockReturnOutboundApprovedRequest;
use Modules\Core\Rules\Contracts\StockReturnOutboundRuleInterface as StockReturnOutboundRule;
use Modules\Core\Services\Contracts\Export\StockReturnOutboundExportServiceInterface as StockReturnOutboundExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialBasicTransformer;
use Modules\Core\Entities\StockReturnOutbound;
use Lang;
use App;

class StockReturnOutboundController extends InventoryChainController
{
     public function __construct(StockReturnOutboundRepository $repository, StockReturnOutboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockReturnOutboundExport::class;
        $this->rule = StockReturnOutboundRule::class;
        $this->request['transaction'] = StockReturnOutboundRequest::class;
        $this->request['info'] = StockReturnOutboundApprovedRequest::class;
        $this->entity = StockReturnOutbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::STOCK_RETURN_OUTBOUND;
        $this->eagerLoad = [
            'deliverTo',
            'deliverToLocation',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * Import/Retrieve transaction details by reference number
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remaining(Request $request)
    {
        $sheetNumber = $request->get('sheet_number');
        $branch = $request->get('branch');

        $returns  = $this->repository->findRemaining($branch, $sheetNumber);

        if($returns->count() === 0) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        return $this->successfulResponse([
            'stock_return_outbounds' => $this->collection($returns, StockReturnOutboundChosenTransformer::class)['data']
        ]);
    }

    /**
     * Retrieve return details
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieve(Request $request)
    {
        ini_set('max_execution_time', 240);

        $return = $this->repository->retrieve($request->get('id'));

        if(!$return) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        if($return->details->count() === 0) {
            return $this->errorResponse(array(
                'no.remaining.products.found' => Lang::get('core::error.no.remaining.products.found')
            ));
        }

        $details = $this->paginate($return->details, 10);

        return $this->successfulResponse([
            'stock_return_outbound' => $this->item($return, RetrieveTransformer::class),
            'details' => $this->collection($details, RetrieveDetailTransformer::class),
        ], Lang::get('core::success.transaction.successfully.imported'));
    }

    /**
     * Get the list of serials of the indicated product
     * 
     * @param  StockReturnOutboundDetailRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function serials(StockReturnOutboundDetailRepository $repository, $id)
    {
        $serials = $repository->serials($id);

        if ($serials->count() === 0) {
            return $this->errorResponse(array(
                'no.serials.found' => Lang::get('core::error.no.serials.found')
            ));
        }

        return $this->successfulResponse([
            'serials' => $this->collection($serials, SerialBasicTransformer::class)['data']
        ]);
    }

    /**
     * Get the list of batches of the indicated product
     *
     * @param  StockReturnOutboundDetailRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function batches(StockReturnOutboundDetailRepository $repository, $id)
    {
        $batches = $repository->batches($id);

        if ($batches->count() === 0) {
            return $this->errorResponse(array(
                'no.batches.found' => Lang::get('core::error.no.batches.found')
            ));
        }

        return $this->successfulResponse([
            'batches' => $this->collection($batches, BatchBasicTransformer::class)['data']
        ]);
    }
    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::stock-return-outbound.detail', 
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::stock-return-outbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
