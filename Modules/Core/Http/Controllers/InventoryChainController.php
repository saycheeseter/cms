<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Transformers\InventoryChain\BasicTransformer;
use Modules\Core\Transformers\InventoryFlowDetail\TransactionTransformer;
use Modules\Core\Transformers\InventoryChainDetail\ImportTransformer;
use Modules\Core\Transformers\InventoryFlow\FooterPrintTransformer as FooterTransformer;
use Modules\Core\Transformers\InventoryChainDetail\PrintTransformer;
use Modules\Core\Transformers\InventoryChainDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\Invoice\ChosenTransformer as ReferenceChosenTransformer;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Lang;

abstract class InventoryChainController extends InventoryFlowController
{
    protected $key = [
        'list' => 'records',
        'transaction' => 'record'
    ];

    protected $transformers = [
        'import'  => ImportTransformer::class,
        'full' => BasicTransformer::class,
        'basic' => BasicTransformer::class,
        'detail' => DetailFullInfoTransformer::class,
        'reference' => ReferenceChosenTransformer::class,
        'print' => [
            'info' => '',
            'detail' => PrintTransformer::class,
            'footer' => FooterTransformer::class
        ],
    ];

    /**
     * Get all the detail transaction of the product provided
     * 
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productTransactions(Request $request)
    {   
        $transactions = $this->repository->findTransactionsByProduct(
            $request->get('product_id'), 
            $request->get('per_page', 10)
        );

        if($transactions->isEmpty()) {
            return $this->errorResponse(array(
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ));
        }

        return $this->successfulResponse($this->collection($transactions, TransactionTransformer::class));
    }

    /**
     * Show the data of the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        ini_set('max_execution_time', 240);

        $permission = $request->has('enabled') && $request->get('enabled') == "false"
            ? 'viewDetailFrom'
            : 'viewDetail';

        $this->authorize($permission, $this->entity);

        if ($request->wantsJson()) {

            $record = $this->repository->find($id);

            if(!$record) {
                return $this->trashed($request, $id);
            }

            $record->load([
                'attachments',
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.components',
                'details.serials',
                'details.batches',
                'details.reference'
            ]);

            $details = $this->paginate($record->details, 10);

            $reference = $record->reference;

            return $this->successfulResponse([
                $this->key['transaction'] => $this->item($record, $this->transformers['full']),
                'reference'               => is_null($record->reference)
                    ? []
                    : [$this->item($reference, $this->transformers['reference'])],
                'details'                 => $this->collection($details, $this->transformers['detail']),
                'attachments'             => $this->collection($record->attachments, AttachmentBasicTransformer::class)['data'],
            ]);
        }
    }

    protected function trashed(Request $request, $id)
    {
        $record = $this->repository
            ->withTrashed()
            ->find($id);

        if(!$record) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $record->load([
            'details' => function ($query) use ($record) {
                $query->withTrashed()
                    ->where('deleted_at', '>=', $record->deleted_at);
            },
            'attachments',
            'details.product',
            'details.barcodes',
            'details.units.uom',
            'details.components',
            'details.serials',
            'details.batches',
            'details.reference'
        ]);

        $details = $this->paginate($record->details, 10);

        $reference = $record->reference;

        return $this->successfulResponse([
            $this->key['transaction'] => $this->item($record, $this->transformers['full']),
            'reference'               => is_null($record->reference)
                ? []
                : [$this->item($reference, ReferenceChosenTransformer::class)],
            'details'                 => $this->collection($details, $this->transformers['detail']),
            'attachments'             => $this->collection($record->attachments, AttachmentBasicTransformer::class)['data'],
        ]);
    }
}
