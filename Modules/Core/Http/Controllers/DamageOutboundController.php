<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\DamageOutboundRepository;
use Modules\Core\Services\Contracts\DamageOutboundServiceInterface;
use Modules\Core\Transformers\DamageOutbound\BasicTransformer;
use Modules\Core\Transformers\DamageOutbound\FullInfoTransformer;
use Modules\Core\Transformers\DamageOutbound\ExportListTransformer;
use Modules\Core\Transformers\DamageOutbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\DamageOutboundRequest;
use Modules\Core\Http\Requests\DamageOutboundApprovedRequest;
use Modules\Core\Rules\Contracts\DamageOutboundRuleInterface as DamageOutboundRule;
use Modules\Core\Services\Contracts\Export\DamageOutboundExportServiceInterface as DamageOutboundExport;
use Modules\Core\Repositories\Contracts\ReasonRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\DamageOutbound;
use Lang;
use App;

class DamageOutboundController extends InventoryChainController
{
     public function __construct(DamageOutboundRepository $repository, DamageOutboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = DamageOutboundExport::class;
        $this->rule = DamageOutboundRule::class;
        $this->request['transaction'] = DamageOutboundRequest::class;
        $this->request['info'] = DamageOutboundApprovedRequest::class;
        $this->entity = DamageOutbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::DAMAGE_OUTBOUND;
        $this->eagerLoad = [
            'reason',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(ReasonRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($reasons, $udfs, $permissions, $templates) = $this->references();

        return view('core::damage-outbound.detail',
            compact('reasons', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::damage-outbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'reasons'     => $this->collection(App::make(ReasonRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
