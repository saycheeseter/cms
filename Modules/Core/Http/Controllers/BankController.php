<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\BankRepository;
use Modules\Core\Services\Contracts\BankServiceInterface;
use Modules\Core\Entities\Bank;
use Lang;
use Modules\Core\Services\Contracts\Import\BankImportExcelServiceInterface;

class BankController extends SystemCodeController
{
    public function __construct(BankRepository $repository, BankServiceInterface $service, BankImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = Bank::class;
        $this->title = Lang::get('core::label.bank');
    }
}
