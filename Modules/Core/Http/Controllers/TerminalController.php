<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\TerminalRepository;
use Lang;
use App;
use Modules\Core\Transformers\Terminal\FullInfoTransformer;

class TerminalController extends Controller
{
    private $repository;

    public function __construct(TerminalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getByBranch($branch)
    {
        $terminals = $this->repository->getByBranch($branch);

        if ($terminals->count() === 0) {
            return $this->errorResponse(array(
                'no.terminals.found' => Lang::get('core::error.no.terminals.found')
            ));
        }

        return $this->successfulResponse([
            'terminals' => $this->collection($terminals, FullInfoTransformer::class)['data']
        ]);
    }
}
