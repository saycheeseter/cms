<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\UOMRepository;
use Modules\Core\Services\Contracts\Import\UOMImportExcelServiceInterface;
use Modules\Core\Services\Contracts\UOMServiceInterface;
use Modules\Core\Entities\UOM;
use Lang;

class UOMController extends SystemCodeController
{
    public function __construct(UOMRepository $repository, UOMServiceInterface $service, UOMImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = UOM::class;
        $this->title = Lang::get('core::label.uom');
    }
}
