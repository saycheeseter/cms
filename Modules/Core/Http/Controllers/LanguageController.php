<?php

namespace Modules\Core\Http\Controllers;

use Session;

class LanguageController
{
    public function set($lang)
    {
        Session::put('nsi_lang', $lang);
    }
}