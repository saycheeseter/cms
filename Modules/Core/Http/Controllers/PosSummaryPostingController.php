<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Requests\PosSummaryPostingRequest;
use Modules\Core\Repositories\Contracts\PosSummaryPostingRepository;
use Modules\Core\Rules\Contracts\PosSummaryPostingRuleInterface;
use Modules\Core\Services\Contracts\PosSummaryPostingServiceInterface;
use Modules\Core\Transformers\PosSummaryPosting\FullInfoTransformer;
use Lang;
use App;
use Log;

class PosSummaryPostingController extends Controller
{
    private $repository;

    public function __construct(PosSummaryPostingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $result = $this->repository
                ->with(['branch'])
                ->paginate($request->get('per_page', 10));

            $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'terminals' => $this->collection($result, FullInfoTransformer::class)
            ], $message);
        }

        return view('core::pos-summary-posting.list');
    }

    public function generateSummary(PosSummaryPostingRequest $request, $id)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $rule = $rule = $this->rule('generateSummary', PosSummaryPostingRuleInterface::class, $id, $request->get('date'));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $service = App::make(PosSummaryPostingServiceInterface::class);

        try {
            $terminal = $service->generateSummary($id, $request->get('date'));

            return $this->successfulResponse([
                'terminal' => $this->item($terminal, FullInfoTransformer::class)
            ], Lang::get('core::success.summary.generated'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return $this->errorResponse([
                'generate.summary.failed' => Lang::get('core::error.unable.to.generate.summary')
            ]);
        }
    }
}