<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\CollectionRepository;
use Modules\Core\Repositories\Contracts\SalesReturnInboundRepository;
use Modules\Core\Services\Contracts\CollectionServiceInterface;
use Modules\Core\Transformers\SalesOutbound\WalkInChosenTransformer as SalesOutboundWalkInChosenTransformer;
use Modules\Core\Transformers\SalesReturnInbound\WalkInChosenTransformer as SalesReturnInboundWalkInChosenTransformer;
use Modules\Core\Http\Requests\CollectionRequest;
use Modules\Core\Rules\Contracts\CollectionRuleInterface as CollectionRule;
use Modules\Core\Transformers\Collection\BasicTransformer;
use Modules\Core\Transformers\Collection\FullInfoTransformer;
use Modules\Core\Transformers\CollectionDetail\FullInfoTransformer as FullInfoDetailTransformer;
use Modules\Core\Transformers\CollectionReference\FullInfoTransformer as FullInfoReferenceTransformer;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Entities\Collection;
use Modules\Core\Repositories\Contracts\SalesOutboundRepository;
use App;
use Lang;

class CollectionController extends FinancialFlowController
{
    protected $transformers = [
        'list'      => BasicTransformer::class,
        'full'      => FullInfoTransformer::class,
        'detail'    => FullInfoDetailTransformer::class,
        'reference' => FullInfoReferenceTransformer::class
    ];

    public function __construct(CollectionRepository $repository, CollectionServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->rule = CollectionRule::class;
        $this->request = CollectionRequest::class;
        $this->entity = Collection::class;
    }

    protected function references()
    {
        $walkInCustomers = unique_multidim_array(
            array_merge(
                $this->collection(App::make(SalesOutboundRepository::class)->getWalkInCustomers(), SalesOutboundWalkInChosenTransformer::class)['data'],
                $this->collection(App::make(SalesReturnInboundRepository::class)->getWalkInCustomers(), SalesReturnInboundWalkInChosenTransformer::class)['data']
            ),
            'id'
        );

        return [
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $walkInCustomers,
            $this->permissions()
        ];
    }

    protected function form()
    {   
        list($customers, $walkInCustomers, $permissions) = $this->references();

        return view('core::collection.detail', 
            compact('customers', 'walkInCustomers', 'permissions')
        ); 
    }

    protected function list()
    {
        return view('core::collection.list', array(
            'branches'    => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            'customers'   => App::make(CustomerRepository::class)->all(),
            'permissions' => $this->permissions()
        ));
    }
}
