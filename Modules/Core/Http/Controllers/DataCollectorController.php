<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\DataCollectorRepository;
use Modules\Core\Services\Contracts\DataCollectorServiceInterface as DataCollectorService;
use Modules\Core\Transformers\DataCollectorTransaction\BasicTransformer as DataCollectorTransformer;
use Modules\Core\Rules\Contracts\DataCollectorRuleInterface as DataCollectorRule;
use Lang;
use Exception;

class DataCollectorController extends Controller
{
    protected $repository;
    protected $service;
    protected $eagerLoad;

    public function __construct(DataCollectorRepository $repository, DataCollectorService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->eagerLoad = [
            'importer',
            'details',
        ];
    }

    public function index(Request $request)
    {
        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->with($this->eagerLoad)
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, DataCollectorTransformer::class),
                $message
            );
        }

        return view('core::data-collector.transaction-list');
    }

    public function findByReferenceNumber(Request $request)
    {
        $reference = $request->get('reference_number');
        $transactionType = $request->get('transaction_type');

        $record = $this->repository
            ->with($this->eagerLoad)
            ->findByReference($reference, $transactionType);

        if(!$record) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        return $this->successfulResponse(
            $this->item($record, DataCollectorTransformer::class),
            Lang::get('core::success.transaction.successfully.imported')
        );
    }

    public function destroyByReferenceNumber(Request $request, $reference)
    {
        $rule = $this->rule('destroyByReferenceNumber', DataCollectorRule::class, $reference);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->deleteByReference($reference);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    public function destroy(Request $request, $id)
    {
        $rule = $this->rule('destroy', DataCollectorRule::class, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }
}