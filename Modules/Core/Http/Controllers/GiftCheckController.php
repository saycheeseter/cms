<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Requests\BulkGiftCheckRequest;
use Modules\Core\Repositories\Contracts\GiftCheckRepository;
use Modules\Core\Rules\Contracts\GiftCheckRuleInterface;
use Modules\Core\Services\Contracts\GiftCheckServiceInterface;
use Modules\Core\Entities\GiftCheck;
use Modules\Core\Http\Requests\GiftCheckRequest;
use Modules\Core\Transformers\GiftCheck\BasicTransformer;
use Modules\Core\Transformers\GiftCheck\FullInfoTransformer;
use Illuminate\Http\Request;
use Lang;
use Exception;
use Auth;

class GiftCheckController extends Controller
{
    private $repository;
    private $service;
    private $entity;

    public function __construct(GiftCheckRepository $repository, GiftCheckServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = GiftCheck::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'gift_checks'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::gift-check.list', compact('permissions'));
    }

    /**
     * Find the data by id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $this->authorize('viewDetail', $this->entity);

        $giftCheck = $this->repository->find($id);

        if (!$giftCheck) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse(array(
            'gift_check' => $this->item($giftCheck, FullInfoTransformer::class)
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GiftCheckRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GiftCheckRequest $request)
    {
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'gift_check' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  GiftCheckRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GiftCheckRequest $request, $id)
    {
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', GiftCheckRuleInterface::class, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'gift_check' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Bulk / Series entry
     *
     * @param  BulkGiftCheckRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkEntry(BulkGiftCheckRequest $request)
    {
        $this->authorize('store', $this->entity);

        $data = $request->only(
            'code_series',
            'check_number_series',
            'amount',
            'date_from',
            'date_to',
            'qty'
        );

        $rule = $this->rule('bulkEntry', GiftCheckRuleInterface::class, $data);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $result = $this->service->bulkEntry($data);

        if (!$result) {
            return $this->errorResponse(array(
                'bulk.entry.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.created'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'code',
            'check_number',
            'amount',
            'date_from',
            'date_to'
        );
    }

    protected function permissions()
    {
        return [
            'view'        => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update'      => Auth::user()->can('update', $this->entity),
            'delete'      => Auth::user()->can('delete', $this->entity),
            'create'      => Auth::user()->can('store', $this->entity),
        ];
    }
}