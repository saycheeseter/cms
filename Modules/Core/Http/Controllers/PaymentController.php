<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\PaymentRepository;
use Modules\Core\Services\Contracts\PaymentServiceInterface;
use Modules\Core\Http\Requests\PaymentRequest;
use Modules\Core\Rules\Contracts\PaymentRuleInterface as PaymentRule;
use Modules\Core\Transformers\Payment\BasicTransformer;
use Modules\Core\Transformers\Payment\FullInfoTransformer;
use Modules\Core\Transformers\PaymentDetail\FullInfoTransformer as FullInfoDetailTransformer;
use Modules\Core\Transformers\PaymentReference\FullInfoTransformer as FullInfoReferenceTransformer;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Entities\Payment;
use App;
use Lang;

class PaymentController extends FinancialFlowController
{
    protected $transformers = [
        'list'      => BasicTransformer::class,
        'full'      => FullInfoTransformer::class,
        'detail'    => FullInfoDetailTransformer::class,
        'reference' => FullInfoReferenceTransformer::class
    ];

    public function __construct(PaymentRepository $repository, PaymentServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->rule = PaymentRule::class;
        $this->request = PaymentRequest::class;
        $this->entity = Payment::class;
    }

    protected function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->permissions()
        ];
    }

    protected function form()
    {   
        list($suppliers, $permissions) = $this->references();

        return view('core::payment.detail', 
            compact('suppliers', 'permissions')
        ); 
    }

    protected function list()
    {
        return view('core::payment.list', array(
            'branches'    => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            'suppliers'   => App::make(SupplierRepository::class)->all(),
            'permissions' => $this->permissions()
        ));
    }
}
