<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\OtherPaymentRepository;
use Modules\Core\Repositories\Contracts\PaymentTypeRepository;
use Modules\Core\Rules\Contracts\OtherPaymentRuleInterface;
use Modules\Core\Services\Contracts\OtherPaymentServiceInterface;
use Modules\Core\Transformers\OtherPayment\FullInfoTransformer;
use Modules\Core\Transformers\OtherPayment\BasicTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\OtherPaymentDetail\BasicTransformer as OtherPaymentDetailBasicTransformer;
use Modules\Core\Entities\OtherPayment;
use App;
use Auth;
use Lang;

class OtherPaymentController extends OtherFinancialFlowController
{   
    protected $transformers = [
        'full' => FullInfoTransformer::class,
        'basic' => BasicTransformer::class,
        'detail' => OtherPaymentDetailBasicTransformer::class
    ];

    protected $blade = 'core::other-payment.list';

    public function __construct(OtherPaymentRepository $repository, OtherPaymentServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = OtherPayment::class;
        $this->rule = OtherPaymentRuleInterface::class;
    }

    /**
     * list of payment types
     * 
     * @return array
     */
    protected function type()
    {
        return $this->collection(App::make(PaymentTypeRepository::class)->all(), SystemCodeChosenTransformer::class)['data'];
    }
}
