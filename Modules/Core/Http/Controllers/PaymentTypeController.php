<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\PaymentTypeRepository;
use Modules\Core\Services\Contracts\Import\PaymentTypeImportExcelServiceInterface;
use Modules\Core\Services\Contracts\PaymentTypeServiceInterface;
use Modules\Core\Entities\PaymentType;
use Lang;

class PaymentTypeController extends SystemCodeController
{
    public function __construct(PaymentTypeRepository $repository, PaymentTypeServiceInterface $service, PaymentTypeImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = PaymentType::class;
        $this->title = Lang::get('core::label.payment.type.label');
    }
}
    