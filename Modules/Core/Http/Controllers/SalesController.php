<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\SalesRepository;
use Modules\Core\Services\Contracts\SalesServiceInterface;
use Modules\Core\Transformers\Sales\BasicTransformer;
use Modules\Core\Transformers\Sales\FullInfoTransformer;
use Modules\Core\Transformers\Sales\ExportListTransformer;
use Modules\Core\Transformers\Sales\RetrieveTransformer;
use Modules\Core\Transformers\Sales\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\SalesRequest;
use Modules\Core\Http\Requests\SalesApprovedRequest;
use Modules\Core\Rules\Contracts\SalesRuleInterface as SalesRule;
use Modules\Core\Services\Contracts\Export\SalesExportServiceInterface as SalesExport;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\Sales;
use Lang;
use App;
use DB;

class SalesController extends InvoiceController
{
    public function __construct(SalesRepository $repository, SalesServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = SalesExport::class;
        $this->rule = SalesRule::class;
        $this->request['transaction'] = SalesRequest::class;
        $this->request['info'] = SalesApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = Sales::class;
        $this->module = ModuleDefinedFields::SALES;
        $this->eagerLoad = [
            'customer',
            'customerDetail',
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'salesman',
            'requested'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($customers, $udfs, $permissions, $templates) = $this->references();

        return view('core::sales.detail', 
            compact('customers', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::sales.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
