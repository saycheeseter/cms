<?php 

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Illuminate\Http\Request;
use Modules\Core\Enums\ApprovalStatus;
use Exception;
use App;
use Lang;
use Auth;

abstract class FinancialFlowController extends Controller
{
    protected $entity;
    protected $repository;
    protected $service;
    protected $rule;
    protected $request;
    protected $transformers = [
        'list'      => '',
        'full'      => '',
        'detail'    => '',
        'reference' => ''
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository->search($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, $this->transformers['list'], 'records'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        return $this->list();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show the edit form and return the list of references
     *
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {   
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {
            $record = $this->repository->find($id);

            if(!$record) {
                return $this->trashed($request, $id);
            }

            $record->load([
                'details',
                'references.reference',
                'attachments'
            ]);

            return $this->successfulResponse([
                'record'      => $this->item($record, $this->transformers['full']),
                'details'     => $this->collection($record->details, $this->transformers['detail']),
                'references'  => $this->collection($record->references, $this->transformers['reference']),
                'attachments' => $this->collection($record->attachments, AttachmentBasicTransformer::class)['data'],
            ]);
        }
    }

    public function store()
    {   
        $this->authorize('store', $this->entity);

        $request = App::make($this->request);

        $rule = $this->rule('store', $this->rule, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'record' => $this->item($result, $this->transformers['full']),
        ], Lang::get('core::success.created'));
    }

    public function update($id)
    {   
        $this->authorize('update', $this->entity);

        $request = App::make($this->request);

        $rule = $this->rule('update', $this->rule, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', $this->rule, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    public function approval(Request $request)
    {
        $transactions = $request->get('transactions');
        $status = $request->get('status');

        if($status == ApprovalStatus::APPROVED) {
            $this->authorize('approve', $this->entity);
        } else if($status == ApprovalStatus::DECLINED) {
            $this->authorize('decline', $this->entity);
        }

        $rule = $this->rule('approval', $this->rule, $transactions, $status);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }
        $this->service->approval($transactions, $status);

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    protected function trashed(Request $request, $id)
    {
        $record = $this->repository
            ->withTrashed()
            ->find($id);

        if(!$record) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $record->load([
            'details' => function ($query) use ($record) {
                $query->withTrashed()
                    ->where('deleted_at', '>=', $record->deleted_at);
            },
            'references' => function ($query) use ($record) {
                $query->withTrashed()
                    ->where('deleted_at', '>=', $record->deleted_at);
            },
            'references.reference',
            'attachments'
        ]);

        return $this->successfulResponse([
            'record'      => $this->item($record, $this->transformers['full']),
            'details'     => $this->collection($record->details, $this->transformers['detail']),
            'references'  => $this->collection($record->references, $this->transformers['reference']),
            'attachments' => $this->collection($record->attachments, AttachmentBasicTransformer::class)['data'],
        ]);
    }

    protected function data(Request $request)
    {
        return $request->only(
            'info',
            'references',
            'details'
        );
    }

    protected function permissions()
    {
        return [
            'view'              => Auth::user()->can('view', $this->entity),
            'view_detail'       => Auth::user()->can('viewDetail', $this->entity),
            'update'            => Auth::user()->can('update', $this->entity),
            'delete'            => Auth::user()->can('delete', $this->entity),
            'create'            => Auth::user()->can('store', $this->entity),
            'approve'           => Auth::user()->can('approve', $this->entity),
            'decline'           => Auth::user()->can('decline', $this->entity),
            'column_settings'   => Auth::user()->can('useColumnSettings', $this->entity)
        ];
    }

    protected abstract function references();
    protected abstract function form();
    protected abstract function list();
}