<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Api\Transformers\SalesOutbound\ApiTransformer;
use Modules\Core\Repositories\Contracts\SalesOutboundRepository;
use Modules\Core\Services\Contracts\SalesOutboundServiceInterface;
use Modules\Core\Transformers\SalesOutbound\BasicTransformer;
use Modules\Core\Transformers\SalesOutbound\FullInfoTransformer;
use Modules\Core\Transformers\SalesOutbound\ExportListTransformer;
use Modules\Core\Transformers\SalesOutbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\SalesOutbound\CollectionTransformer;
use Modules\Core\Http\Requests\SalesOutboundRequest;
use Modules\Core\Http\Requests\SalesOutboundApprovedRequest;
use Modules\Core\Rules\Contracts\SalesOutboundRuleInterface as SalesOutboundRule;
use Modules\Core\Services\Contracts\Export\SalesOutboundExportServiceInterface as SalesOutboundExport;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\SalesOutbound;
use Lang;
use App;
use Auth;

class SalesOutboundController extends InventoryChainController
{
    public function __construct(SalesOutboundRepository $repository, SalesOutboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = SalesOutboundExport::class;
        $this->rule = SalesOutboundRule::class;
        $this->request['transaction'] = SalesOutboundRequest::class;
        $this->request['info'] = SalesOutboundApprovedRequest::class;
        $this->entity = SalesOutbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::SALES_OUTBOUND;
        $this->eagerLoad = [
            'customer',
            'customerDetail',
            'salesman',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    public function transactionsWithBalance(Request $request)
    {
        $result = $this->repository->transactionsWithBalance(
            $request->get('branch_id'), 
            $request->get('customer'),
            $request->get('customer_type'),
            $request->get('per_page', 10)
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, BasicTransformer::class)
        ]);
    }

    public function getCollectibleTransactions(Request $request)
    {
        $result = $this->repository->findMany(
            $request->get('ids')
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, CollectionTransformer::class)
        ]);
    }

    public function getByDateAndStatus(Request $request)
    {
        $result = $this->repository->findByTransactionDateRangeAndStatuses(
            $request->get('date_from'),
            $request->get('date_to'),
            $request->get('statuses'),
            $request->get('per_page')
        );

        $message = $result->count() === 0
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse(
            $this->collection($result, ApiTransformer::class),
            $message
        );
    }

    public function getBySheetNumberAndStatus(Request $request)
    {
        $result = $this->repository->findBySheetNumberRangeAndStatuses(
            $request->get('sheets'),
            $request->get('statuses'),
            $request->get('per_page')
        );

        $message = $result->count() === 0
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse(
            $this->collection($result, ApiTransformer::class),
            $message
        );
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($customers, $udfs, $permissions, $templates) = $this->references();

        return view('core::sales-outbound.detail',
            compact('customers', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::sales-outbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }

    protected function permissions()
    {
        $permissions = parent::permissions();

        return array_merge($permissions, [
            'allow_customer_exceed_credit_limit' => Auth::user()->can('allowExceedCreditLimit', $this->entity),
        ]);
    }
}
