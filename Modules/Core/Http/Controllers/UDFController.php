<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\UserDefinedFieldRepository;
use Modules\Core\Services\Contracts\UserDefinedFieldServiceInterface;
use Modules\Core\Transformers\UserDefinedField\BasicTransformer;
use Modules\Core\Rules\Contracts\UDFRuleInterface as UDFRule;
use Modules\Core\Http\Requests\UDFRequest;
use Lang;
use Exception;

class UDFController extends Controller
{
    protected $service;
    protected $repository;

    public function __construct(UserDefinedFieldServiceInterface $service, UserDefinedFieldRepository $repository)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {   
        if ($request->wantsJson()) {
            $udfs = $this->repository->search(
                $request->get('module'),
                $request->get('per_page', 10)
            );

            $message = $udfs->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($udfs, BasicTransformer::class, 'udfs'),
                $message
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UDFRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UDFRequest $request)
    {
        try {
            $udf = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'udf' => $this->item($udf, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Updates resource in storage.
     *
     * @param  UDFRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UDFRequest $request, $id)
    {
        $data = $this->data($request);

        $rule = $this->rule('update', UDFRule::class, $data, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $udf = $this->service->update($data, $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'udf' => $this->item($udf, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $rule = $this->rule('destroy', UDFRule::class, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }
        
        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * fetch udf per module
     * @param  string $module selected module id
     * @return object udfs
     */
    public function byModule($module)
    {
        return $this->successfulResponse([
            'udfs' => $this->collection(
                $this->repository->findByModule($module), 
                BasicTransformer::class
            )['data']
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'label',
            'module_id',
            'type',
            'visible'
        );
    }
}
