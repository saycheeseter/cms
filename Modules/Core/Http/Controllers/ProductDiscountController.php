<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Entities\ProductDiscount;
use Modules\Core\Http\Requests\ProductDiscountRequest;
use Modules\Core\Repositories\Contracts\ProductDiscountRepository;
use Modules\Core\Services\Contracts\ProductDiscountServiceInterface;
use Modules\Core\Rules\Contracts\ProductDiscountRuleInterface as ProductDiscountRule;
use Modules\Core\Transformers\ProductDiscount\BasicTransformer;
use Modules\Core\Transformers\ProductDiscount\FullInfoTransformer;
use Modules\Core\Transformers\ProductDiscountDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\ProductDiscountTarget\FullInfoTransformer as TargetFullInfoTransformer;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Transformers\Category\ChosenTransformer as CategoryChosenTransformer;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Auth;
use App;
use Lang;

class ProductDiscountController extends Controller
{
    protected $repository;
    protected $service;
    protected $entity = ProductDiscount::class;

    public function __construct(ProductDiscountRepository $repository, ProductDiscountServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'records'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        list($branches, $permissions) = $this->references();

        return view('core::product-discount.list',
            compact('branches', 'permissions')
        );
    }

    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {
            $record = $this->repository->find($id);

            if(!$record) {
                return $this->errorResponse(array(
                    'show.failed' => Lang::get('core::error.show.failed')
                ));
            }

            $record->load(['details.entity', 'targets.target']);

            $details = $this->paginate($record->details, 10);
            $targets = $this->paginate($record->targets, 10);

            return $this->successfulResponse([
                'record' => $this->item($record, FullInfoTransformer::class),
                'details' => $this->collection($details, DetailFullInfoTransformer::class),
                'targets' => $this->collection($targets, TargetFullInfoTransformer::class)
            ]);
        }
    }

    public function store(ProductDiscountRequest $request)
    {
        $this->authorize('store', $this->entity);

        $rule = $this->rule('store', ProductDiscountRule::class, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'record' => $this->item($result, FullInfoTransformer::class),
        ], Lang::get('core::success.created'));
    }

    public function update($id, ProductDiscountRequest $request)
    {
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', ProductDiscountRule::class, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);
        
        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    protected function data(Request $request)
    {
        return $request->only(
            'info',
            'details',
            'targets'
        );
    }

    protected function form()
    {
        list($branches, $permissions, $categories, $suppliers, $brands) = $this->references();

        return view('core::product-discount.detail',
            compact('branches', 'permissions', 'categories', 'suppliers', 'brands')
        );
    }

    protected function references()
    {
        return [
            $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            $this->permissions(),
            $this->collection(App::make(CategoryRepository::class)->all(), CategoryChosenTransformer::class)['data'],
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(BrandRepository::class)->all(), SystemCodeChosenTransformer::class)['data']
        ];
    }

    protected function permissions()
    {   
        return [
            'view'            => Auth::user()->can('view', $this->entity),
            'view_detail'     => Auth::user()->can('viewDetail', $this->entity),
            'create'          => Auth::user()->can('store', $this->entity),
            'update'          => Auth::user()->can('update', $this->entity),
            'delete'          => Auth::user()->can('delete', $this->entity),
            'column_settings' => Auth::user()->can('useColumnSettings', $this->entity),
        ];
    }
}