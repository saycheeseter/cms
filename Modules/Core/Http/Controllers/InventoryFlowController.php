<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\UserDefinedFieldRepository;
use Modules\Core\Transformers\InventoryFlow\BasicTransformer;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\PrinterServiceInterface as PrinterService;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface as ExcelTemplateExport;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Entities\Product as ProductEntity;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Transformers\UserDefinedField\BasicTransformer as UserDefinedFieldTransformer;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Auth;
use App;
use Lang;
use Exception;
use Log;

abstract class InventoryFlowController extends Controller
{
    protected $module = null;
    protected $eagerLoad = [];
    protected $service;
    protected $repository;
    protected $rule;
    protected $request = [
        'transaction' => '',
        'info' => ''
    ];

    protected $entity;
    protected $export;

    protected $key = [
        'list' => '',
        'transaction' => ''
    ];

    protected $transformers = [
        'basic' => BasicTransformer::class,
        'print' => [
            'info' => '',
            'detail' => '',
            'footer' => ''
        ],
        'export' => '',
        'import' => ''
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->withTrashed()
                ->with($this->eagerLoad)
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, $this->transformers['basic'], $this->key['list']),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        return $this->list();
    }

    /**
     * Show create form and return list of references
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show the edit form and return the list of references
     *
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        ini_set('max_execution_time', 240);

        $permission = $request->has('enabled') && $request->get('enabled') == "false"
            ? 'viewDetailFrom'
            : 'viewDetail';

        $this->authorize($permission, $this->entity);

        if ($request->wantsJson()) {
            $invoice = $this->repository->find($id);

            if(!$invoice) {
                return $this->trashed($request, $id);
            }

            $invoice->load([
                'attachments',
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.components'
            ]);

            $details = $this->paginate($invoice->details, 10);

            return $this->successfulResponse([
                $this->key['transaction']    => $this->item($invoice, $this->transformers['full']),
                'details'     => $this->collection($details, $this->transformers['detail']),
                'attachments' => $this->collection($invoice->attachments, AttachmentBasicTransformer::class)['data'],
            ]);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        ini_set('max_execution_time', 240);

        $this->authorize('store', $this->entity); 

        $request = App::make($this->request['transaction']);

        $rule = $this->rule('store', $this->rule, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            $this->key['transaction'] => $this->item($result, $this->transformers['basic']),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        ini_set('max_execution_time', 240);

        $this->authorize('update', $this->entity);

        $request = App::make($this->request['transaction']);

        $rule = $this->rule('update', $this->rule, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    public function updateApproved($id)
    {
        $this->authorize('update', $this->entity);

        $request = App::make($this->request['info']);

        $rule = $this->rule('updateApproved', $this->rule, $request->all(), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->updateApproved($request, $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', $this->rule, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Updates the specified transaction to the
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approval(Request $request)
    {
        ini_set('max_execution_time', 240);

        $transactions = $request->get('transactions');
        $status = $request->get('status');

        switch ($status) {
            case ApprovalStatus::APPROVED:
                $this->authorize('approve', $this->entity);
                break;

            case ApprovalStatus::DECLINED:
                $this->authorize('decline', $this->entity);
                break;
        }

        $rule = $this->rule('approval', $this->rule, $transactions, $status);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $this->service->approval($transactions, $status);

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    /**
     * Import/Retrieve transaction details by reference number
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(Request $request)
    {
        $reference = $request->get('reference_number');
        $id = $request->get('id');

        $record = $this->repository->findByReference($reference, $id);

        if(!$record) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        $record->with([
            'details.product',
            'details.barcodes',
            'details.units.uom',
            'details.components'
        ]);

        $details = $this->paginate($record->details, 10);

        return $this->successfulResponse([
            'details' => $this->collection($details, $this->transformers['import'])
        ], Lang::get('core::success.transaction.successfully.imported'));
    }

    /**
     * Generate excel data
     *
     * @param Request $request
     */
    public function export(Request $request)
    {
        ini_set('max_execution_time', 240);

        $this->authorize('export', $this->entity);

        $result = $this->repository
            ->with($this->eagerLoad)
            ->withTrashed()
            ->all();

        if ($result->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($result, $this->transformers['export'])['data'];

        if($request->has('template_id')) {
            $templateData =  App::make(ExcelTemplateRepository::class)->find($request->get('template_id'));

            App::make(ExcelTemplateExport::class)
                ->settings($request->get('settings'))
                ->udfs($this->udfs())
                ->setFileName($templateData->name)
                ->setTemplateFormat($templateData->format)
                ->generate($data);
        } else {
            App::make($this->export)
                ->settings($request->get('settings'))
                ->udfs($this->udfs())
                ->list($data);
        }
    }

    protected function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    /**
     * Generate printout for the transaction
     *
     * @param PrintoutTemplateRepository $printout
     * @param  int $id
     * @param  int $template
     * @return \Illuminate\Http\JsonResponse
     */
    public function print(PrintoutTemplateRepository $printout, $id, $template)
    {
        ini_set('max_execution_time', 240);

        $this->authorize('print', $this->entity);

        $data = $this->repository
            ->withTrashed()
            ->find($id);

        $template = $printout->find($template);

        if (!$data || !$template) {
            throw new ModelNotFoundException;
        }

        $data->load([
            'details' => function ($query) use ($data) {
                if (!is_null($data->deleted_at)) {
                    $query->withTrashed()
                        ->where('deleted_at', '>=', $data->deleted_at);
                }
            },
            'details.product',
            'details.barcodes',
            'details.units.uom',
        ]);

        $data = [
            'info'     => $this->item($data, $this->transformers['print']['info']),
            'details'  => $this->collection($data->details, $this->transformers['print']['detail'])['data'],
            'footer'   => $this->item($data, $this->transformers['print']['footer']),
            'settings' => $this->printoutSettings()
        ];

        try {
            $print = App::make(PrinterService::class);

            return $print->template($data, $template);

        } catch (PrintException $e) {
            return $this->errorResponse(array(
                'show.failed' => $e->getMessage()
            ));
        }
    }

    protected function permissions()
    {   
        return [
            'view'                      => Auth::user()->can('view', $this->entity),
            'view_detail'               => Auth::user()->can('viewDetail', $this->entity),
            'create'                    => Auth::user()->can('store', $this->entity),
            'update'                    => Auth::user()->can('update', $this->entity),
            'delete'                    => Auth::user()->can('delete', $this->entity),
            'approve'                   => Auth::user()->can('approve', $this->entity),
            'decline'                   => Auth::user()->can('decline', $this->entity),
            'export'                    => Auth::user()->can('export', $this->entity),
            'print'                     => Auth::user()->can('print', $this->entity),
            'column_settings'           => Auth::user()->can('useColumnSettings', $this->entity),
            'show_price_amount'         => Auth::user()->can('showPriceAmount', $this->entity),
            'show_cost_product'         => Auth::user()->can('showCost', App::make(ProductEntity::class)),
        ];
    }

    protected function printoutSettings()
    {   
        $showPriceAmount = $this->permissions()['show_price_amount'];

        return [
            'amount'    => $showPriceAmount,
            'price'     => $showPriceAmount,
            'oprice'    => $showPriceAmount,
            'net_total' => $showPriceAmount
        ];
    }

    protected function udfs()
    {
        return $this->collection(
            App::make(UserDefinedFieldRepository::class)->findByModule($this->module),
            UserDefinedFieldTransformer::class
        )['data'];
    }

    protected function trashed(Request $request, $id)
    {
        $transaction = $this->repository
            ->withTrashed()
            ->find($id);

        if(!$transaction) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $transaction->load([
            'details' => function ($query) use ($transaction) {
                $query->withTrashed()
                    ->where('deleted_at', '>=', $transaction->deleted_at);
            },
            'attachments',
            'details.product',
            'details.barcodes',
            'details.units.uom',
            'details.components'
        ]);

        $details = $this->paginate($transaction->details, 10);

        return $this->successfulResponse([
            $this->key['transaction'] => $this->item($transaction, $this->transformers['full']),
            'details'                 => $this->collection($details, $this->transformers['detail']),
            'attachments'             => $this->collection($transaction->attachments, AttachmentBasicTransformer::class)['data'],
        ]);
    }

    protected abstract function form();
}
