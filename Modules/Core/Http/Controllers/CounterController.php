<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Repositories\Contracts\CounterRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Services\Contracts\CounterServiceInterface;
use Modules\Core\Services\Contracts\Export\CounterExportServiceInterface as CounterExport;
use Modules\Core\Transformers\Counter\BasicTransformer;
use Modules\Core\Transformers\Counter\FooterPrintTransformer;
use Modules\Core\Transformers\Counter\FullInfoTransformer;
use Modules\Core\Transformers\Counter\ExportListTransformer;
use Modules\Core\Transformers\Counter\HeaderPrintTransformer;
use Modules\Core\Transformers\CounterDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Http\Requests\CounterRequest;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\CounterDetail\PrintTransformer;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface as ExcelTemplateExport;
use Modules\Core\Entities\Counter;
use Modules\Core\Services\Contracts\PrinterServiceInterface as PrinterService;
use Lang;
use App;
use Auth;
use Exception;

class CounterController extends Controller
{
    protected $repository;
    protected $service;
    protected $entity;
    protected $eagerLoad;
    protected $module;

    public function __construct(CounterRepository $repository, CounterServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Counter::class;
        $this->module = ModuleDefinedFields::COUNTER;
        $this->eagerLoad = [
            'for',
            'from',
            'customer',
            'creator'
        ];
    }

    public function index(Request $request)
    {
        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->with($this->eagerLoad)
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'records'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        return $this->list();
    }

    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {
            $record = $this->repository->find($id);

            if(!$record) {
                return $this->errorResponse(array(
                    'show.failed' => Lang::get('core::error.show.failed')
                ));
            }

            $record->load(['details.reference']);

            $details = $this->paginate($record->details, 10);

            return $this->successfulResponse([
                'record' => $this->item($record, FullInfoTransformer::class),
                'details' => $this->collection($details, DetailFullInfoTransformer::class),
            ]);
        }
    }

    public function store(Request $request)
    {
        $this->authorize('store', $this->entity);

        $request = App::make(CounterRequest::class);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'record' => $this->item($result, FullInfoTransformer::class),
        ], Lang::get('core::success.created'));
    }

    public function update($id)
    {
        $this->authorize('update', $this->entity);

        $request = App::make(CounterRequest::class);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    public function export(Request $request)
    {
        $this->authorize('export', $this->entity);

        $result = $this->repository
            ->with($this->eagerLoad)
            ->withTrashed()
            ->all();

        if ($result->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($result, ExportListTransformer::class)['data'];

        if($request->has('template_id')) {
            $templateData =  App::make(ExcelTemplateRepository::class)->find($request->get('template_id'));

            App::make(ExcelTemplateExport::class)
                ->setFileName($templateData->name)
                ->setTemplateFormat($templateData->format)
                ->generate($data);
        } else {
            App::make(CounterExport::class)
                ->settings($request->get('settings'))
                ->list($data);
        }
    }

    /**
     * Generate printout for the transaction
     *
     * @param  int $id
     * @param  int $template
     * @return \Illuminate\Http\JsonResponse
     */
    public function print(PrintoutTemplateRepository $printout, $id, $template)
    {
        $this->authorize('print', $this->entity);

        $data = $this->repository->find($id);

        $template = $printout->find($template);

        if (!$data || !$template) {
            throw new ModelNotFoundException;
        }

        $data->load([
            'details.reference',
        ]);

        $data = [
            'info'     => $this->item($data, HeaderPrintTransformer::class),
            'details'  => $this->collection($data->details, PrintTransformer::class)['data'],
            'footer'   => $this->item($data, FooterPrintTransformer::class),
            'settings' => []
        ];

        try {
            $print = App::make(PrinterService::class);

            return $print->template($data, $template);

        } catch (PrintException $e) {
            return $this->errorResponse(array(
                'show.failed' => $e->getMessage()
            ));
        }
    }

    protected function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    protected function form()
    {   
        list($branches, $customers, $permissions) = $this->references();

        $templates = App::make(PrintoutTemplateRepository::class)->findByModule($this->module);

        return view('core::counter.detail',
            compact('branches', 'customers', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        list($branches, $customers, $permissions) = $this->references();

        $templates = App::make(ExcelTemplateRepository::class)->findByModule($this->module);

        return view('core::counter.list',
            compact('branches', 'customers', 'permissions', 'templates')
        );
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $this->permissions(),
        ];
    }

    protected function permissions()
    {   
        return [
            'view'            => Auth::user()->can('view', $this->entity),
            'view_detail'     => Auth::user()->can('viewDetail', $this->entity),
            'create'          => Auth::user()->can('store', $this->entity),
            'update'          => Auth::user()->can('update', $this->entity),
            'delete'          => Auth::user()->can('delete', $this->entity),
            'column_settings' => Auth::user()->can('useColumnSettings', $this->entity),
            'export'          => Auth::user()->can('export', $this->entity),
            'print'           => Auth::user()->can('print', $this->entity),
        ];
    }
}
