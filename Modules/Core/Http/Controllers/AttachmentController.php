<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Http\Requests\AttachmentRequest;
use Modules\Core\Repositories\Contracts\AttachmentRepository;
use Modules\Core\Services\Contracts\AttachmentServiceInterface;
use Modules\Core\Transformers\Attachment\BasicTransformer;
use Lang;
use App;
use Exception;

class AttachmentController extends Controller
{
    private $service;
    private $repository;

    public function __construct(AttachmentServiceInterface $service, AttachmentRepository $repository)
    {   
        $this->service = $service;
        $this->repository = $repository;
    }

    public function temporary(AttachmentRequest $request, $type)
    {   
        if (!$request->hasFile('attachments')) {
            return $this->errorResponse(array(
                'no.files.found' => Lang::get('core::error.files.not.found')
            ));
        }

        try {
            $attachment = $this->service->temporary($request->file('attachments'));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'attachment' => $attachment,
            'message' => Lang::get('core::success.attachment.temporary.uploaded')
        ]);
    }

    public function attach(AttachmentRequest $request, $module, $type, $id)
    {
        if (!$request->hasFile('attachments')) {
            return;
        }

        try {
            $attachments = $this->service->attach($request->file('attachments'), $module, $id);
        } catch (ModelNotFoundException $m) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.record.not.found')
            ));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'attachments' => $this->collection($attachments, BasicTransformer::class)['data'],
            'message' => Lang::get('core::success.attachment.uploaded')
        ]);

    }

    public function update(AttachmentRequest $request, $module, $type, $id)
    {   
        if (!$request->hasFile('attachments')) {
            return;
        }

        try {
            $attachments = $this->service->update($request->file('attachments'), $module, $id);
        } catch (ModelNotFoundException $m) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.record.not.found')
            ));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'attachments' => $this->collection($attachments, BasicTransformer::class)['data'],
            'message' => Lang::get('core::success.attachment.uploaded')
        ]);

    }

    public function detach(Request $request, $key)
    {   
        if($request->get('status') == 'temporary') {
            $this->service->removeTemporary($key);
        } else {
            try {
                $this->service->detach($key, $request->get('module'), $request->get('entity_id'));
            } catch (ModelNotFoundException $m) {
                return $this->errorResponse(array(
                    'delete.failed' => Lang::get('core::error.record.not.found')
                ));
            } catch (Exception $e) {
                return $this->errorResponse(array(
                    'delete.failed' => Lang::get('core::error.delete.failed')
                ));
            }
        }

        return $this->successfulResponse([
            'message' => Lang::get('core::success.deleted')
        ]);
    }

    public function getByType($type, $id)
    {
        $attachments = $this->repository->getByType($type, $id);

        $message = $attachments->count() === 0
            ? Lang::get('core::info.no.attachments.found')
            : '';

        return $this->successfulResponse($this->collection($attachments, BasicTransformer::class), $message);
    }
}