<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\ReasonRepository;
use Modules\Core\Services\Contracts\Import\ReasonImportExcelServiceInterface;
use Modules\Core\Services\Contracts\ReasonServiceInterface;
use Modules\Core\Entities\Reason;
use Lang;

class ReasonController extends SystemCodeController
{
    public function __construct(ReasonRepository $repository, ReasonServiceInterface $service, ReasonImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = Reason::class;
        $this->title = Lang::get('core::label.reason');
    }
}
