<?php

namespace Modules\Core\Http\Controllers;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Repositories\Contracts\ReportBuilderRepository;
use Modules\Core\Services\Contracts\ReportBuilderServiceInterface;
use Modules\Core\Http\Requests\ReportBuilderRequest;
use Modules\Core\Transformers\ReportBuilder\BasicTransformer;
use Modules\Core\Transformers\ReportBuilder\FullInfoTransformer;
use Modules\Core\Entities\ReportBuilder;
use Lang;
use App;
use Auth;

class ReportBuilderController extends Controller
{
    protected $service;
    protected $repository;
    protected $entity;

    public function __construct(ReportBuilderRepository $repository, ReportBuilderServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = ReportBuilder::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'templates'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::reports.builder.list', compact('permissions'));
    }

    /**
     * Show create form and return list of available templates
     * 
     * @param  ReportBuilderRepository $repository
     * @return Response
     */
    public function create()
    {   
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    public function edit($id)
    {
        $model = $this->repository
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Find the data by id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   
        $this->authorize('viewDetail', $this->entity);

        $template = $this->repository
            ->find($id);

        if (!$template) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::common.show.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($template, FullInfoTransformer::class)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ReportBuilderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReportBuilderRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, FullInfoTransformer::class),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ReportBuilderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReportBuilderRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, FullInfoTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'group_id',
            'name',
            'format'
        );
    }

    protected function form()
    {   
        $permissions = $this->permissions();

        return view('core::reports.builder.detail', compact('permissions'));
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }
}