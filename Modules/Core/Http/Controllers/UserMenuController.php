<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Repositories\Contracts\UserMenuRepository;
use Modules\Core\Services\Contracts\UserMenuServiceInterface;
use Modules\Core\Transformers\UserMenu\FullInfoTransformer;
use Modules\Core\Transformers\ReportBuilder\SidebarTransformer as ReportBuilderSidebarTransformer;
use Modules\Core\Repositories\Contracts\ReportBuilderRepository;
use Auth;
use Lang;
use Session;
use App;

class UserMenuController extends Controller
{   
    protected $service;
    protected $repository;

    public function __construct(UserMenuRepository $repository, UserMenuServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Find the user menu by user id
     *
     */
    public function index(Request $request)
    {   
        $menu = $this->item($this->repository->findByUser(Auth::user()->id), FullInfoTransformer::class)['format'];
        $reports = $this->collection(App::make(ReportBuilderRepository::class)->all()->groupBy('group_id'), ReportBuilderSidebarTransformer::class)['data'];

        return view('core::user-menu.user-menu', compact('menu', 'reports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        try {
            $result = $this->service->updateOrCreate(['user_id' => Auth::user()->id],
                ['format' => $request->get('format')]
            );
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'menu' => $this->item($result, FullInfoTransformer::class),
        ], Lang::get('core::success.custom.menu.saved'));
    }

    /**
     * sets active menu type on session
     * @param int $type menu type
    */
    public function set($type)
    {
        Session::put('active_menu_type', $type);
    }

    /**
     * gets active menu type on session
     * @param int $type menu type
    */
    public function get($type)
    {
        Session::get('active_menu_type');
    }
}
