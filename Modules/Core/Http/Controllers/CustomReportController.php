<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface as ExcelTemplateExport;
use Modules\Core\Repositories\Contracts\ReportBuilderRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Services\Concrete\ReportBuilder\Modules\Factory;
use Modules\Core\Entities\ReportBuilder;
use Modules\Core\Entities\User;
use Lang;
use App;

class CustomReportController extends Controller
{   
    protected $factory;

    public function __construct(Factory $factory)
    {   
        $this->factory = $factory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $slug)
    {    
        $template = App::make(ReportBuilderRepository::class)->findBySlug($slug);

        list($service, $repository, $transformer) = $this->factory->getInstance($template->group_id);

        if ($request->wantsJson()) {
            $data = $repository->getCustomReportData($template->format, $request->get('filters'));

            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, $transformer, 'records'),
                $message
            );
        }
        
        $this->authorize('viewCustomReport', $template);

        return view('core::reports.custom.template', array(
            'filters' => $service->getFilters(),
            'template' => $template
        ));
    }

    public function export(Request $request)
    {    
        $template = App::make(ReportBuilderRepository::class)->find($request->get('template_id'));

        list($service, $repository, $transformer) = $this->factory->getInstance($template->group_id);
        
        $result = $repository->getCustomReportData($template->format, $request->get('filters'));

        $data = $this->collection($result, $transformer)['data'];

        App::make(ExcelTemplateExport::class)
            ->settings($request->get('settings'))
            ->setFileName($template->name)
            ->setTemplateFormat($template->excel_format)
            ->generate($data);
    }
}