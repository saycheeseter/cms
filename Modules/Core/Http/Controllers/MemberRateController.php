<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Modules\Core\Services\Contracts\MemberRateServiceInterface;
use Modules\Core\Http\Requests\MemberRateRequest;
use Modules\Core\Transformers\MemberRate\FullInfoTransformer;
use Modules\Core\Transformers\MemberRate\BasicTransformer;
use Modules\Core\Transformers\MemberRateDetail\BasicTransformer as MemberRateDetailBasicTransformer;
use Modules\Core\Rules\Contracts\MemberRateRuleInterface as MemberRateRule;
use Modules\Core\Entities\MemberRate;
use Fractal;
use Lang;
use Exception;
use Auth;

class MemberRateController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(MemberRateRepository $repository, MemberRateServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = MemberRate::class;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'rates'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::member-rate.list', compact('permissions'));
    }

    /**
     * Show details of the specified resource
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   
        $this->authorize('viewDetail', $this->entity);
        
        $rate = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$rate) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $details = $this->paginate($rate->details, 10);

        return $this->successfulResponse(array(
            'rate'    => $this->item($rate, FullInfoTransformer::class),
            'details' => $this->collection($details, MemberRateDetailBasicTransformer::class)['data']
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  MemberRateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MemberRateRequest $request)
    {
        $this->authorize('store', $this->entity);

        $rule = $this->rule('store', MemberRateRule::class, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => $e->getMessage()
            ));
        }

        return $this->successfulResponse([
            'rate' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  MemberRateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MemberRateRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', MemberRateRule::class, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (\Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'rate' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', MemberRateRule::class, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (\Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated member rates and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $memberRates = $this->repository->findMany($request->get('ids'));

        if ($memberRates->count() === 0) {
            return $this->errorResponse(array(
                'no.member.rates.found' => Lang::get('core::error.no.member.rates.found')
            ));
        }

        return $this->successfulResponse([
            'memberRates' => $this->collection($memberRates, BasicTransformer::class)['data']
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    protected function permissions()
    {
        return [
            'view'        => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update'      => Auth::user()->can('update', $this->entity),
            'delete'      => Auth::user()->can('delete', $this->entity),
            'create'      => Auth::user()->can('store', $this->entity),
        ];
    }
}
