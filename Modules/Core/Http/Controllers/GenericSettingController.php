<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Rules\Contracts\GenericSettingRuleInterface;
use Modules\Core\Services\Contracts\GenericSettingServiceInterface;
use Lang;
use Exception;

class GenericSettingController extends Controller
{
    protected $service;

    public function __construct(GenericSettingServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {   
        if ($request->wantsJson()) {
            return $this->successfulResponse([
                'settings' => generic_settings("_")
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->service->store($request->all());
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.created'));
    }

    /**
     * Updates resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $rule = $this->rule('update', GenericSettingRuleInterface::class, $data);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $this->service->update($data);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }
}
