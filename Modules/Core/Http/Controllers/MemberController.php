<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Requests\ImportExcelRequest;
use Modules\Core\Repositories\Contracts\MemberRepository;
use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Modules\Core\Services\Contracts\Import\MemberImportExcelServiceInterface;
use Modules\Core\Services\Contracts\MemberServiceInterface;
use Modules\Core\Http\Requests\MemberRequest;
use Modules\Core\Transformers\Member\BasicTransformer;
use Modules\Core\Transformers\Member\FullInfoTransformer;
use Modules\Core\Transformers\MemberRate\ChosenTransformer as MemberRateChosenTransformer;
use Modules\Core\Entities\Member;
use Fractal;
use Lang;
use App;
use Exception;
use Auth;

class MemberController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(MemberRepository $repository, MemberServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Member::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository
                ->with(['rate'])
                ->paginate($perPage);

            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'members'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        $rates = $this->collection(App::make(MemberRateRepository::class)->all(), MemberRateChosenTransformer::class)['data'];

        return view('core::member.list', compact('rates', 'permissions'));
    }

    /**
     * Find the data by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   
        $this->authorize('viewDetail', $this->entity);
        
        $member = $this->repository->find($id);

        if (!$member) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse(array(
            'member' => $this->item($member, FullInfoTransformer::class)
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  MemberRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MemberRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'member' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MemberRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MemberRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'member' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated members and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $members = $this->repository->findMany($request->get('ids'));

        if ($members->count() === 0) {
            return $this->errorResponse(array(
                'no.members.found' => Lang::get('core::error.no.members.found')
            ));
        }

        return $this->successfulResponse([
            'members' => $this->collection($members, BasicTransformer::class)['data']
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'rate_head_id',
            'card_id',
            'barcode',
            'full_name',
            'mobile',
            'telephone',
            'address',
            'email',
            'gender',
            'birth_date',
            'registration_date',
            'expiration_date',
            'status',
            'memo'
        );
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
            'import_excel' => Auth::user()->can('importExcel', $this->entity),
        ];
    }

    public function importExcel(ImportExcelRequest $request)
    {
        $this->authorize('importExcel', $this->entity);

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        if (!$request->hasFile('attachment')) {
            return $this->errorResponse(array(
                'no.files.found' => Lang::get('core::error.files.not.found')
            ));
        }

        try {
            $logs = App::make(MemberImportExcelServiceInterface::class)
                ->setFile($request->file('attachment'))
                ->execute()
                ->getLogs();
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'unable.to.import.excel' => $e->getMessage()
            ));
        }

        return $this->successfulResponse([
            'logs' => $logs
        ]);
    }

    public function downloadImportExcelTemplate()
    {
        $this->authorize('importExcel', $this->entity);

        App::make(MemberImportExcelServiceInterface::class)->exportDefaultTemplate();
    }
}
