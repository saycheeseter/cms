<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Http\Requests\ProductConversionRequest;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Repositories\Contracts\ProductConversionRepository;
use Modules\Core\Services\Contracts\ProductConversionServiceInterface;
use Modules\Core\Rules\Contracts\ProductConversionRuleInterface as ProductConversionRule;
use Modules\Core\Transformers\ProductConversion\BasicTransformer;
use Modules\Core\Transformers\ProductConversion\FullInfoTransformer;
use Modules\Core\Transformers\ProductConversion\ExportListTransformer;
use Modules\Core\Transformers\ProductConversionDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Transformers\ProductConversion\TransactionTransformer;
use Modules\Core\Transformers\ProductConversionDetail\TransactionTransformer as TransactionDetailTransformer;
use Modules\Core\Services\Contracts\Export\ProductConversionExportServiceInterface as ProductConversionExportService;
use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface as ExcelTemplateExport;
use Modules\Core\Enums\GroupType;
use Lang;
use Auth;
use App;
use Exception;
use Log;

class ProductConversionController extends Controller
{
    private $service;
    private $repository;
    private $rule = ProductConversionRule::class;
    private $entity = ProductConversion::class;
    private $export = ProductConversionExportService::class;
    private $eagerLoad = [
        'product',
        'for',
        'creator',
        'auditor',
        'from',
        'location',
    ];

    private $transformers = [
        'basic' => BasicTransformer::class,
        'export' => ExportListTransformer::class,
    ];

    public function __construct(ProductConversionRepository $repository, ProductConversionServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->withTrashed()
                ->with($this->eagerLoad)
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, $this->transformers['basic'], 'records'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        return $this->list();
    }

    /**
     * Show create form and return list of references
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show the edit form and return the list of references
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);
        
        if ($request->wantsJson()) {
            $record = $this->repository->find($id);

            if(!$record) {
                return $this->trashed($request, $id);
            }

            $record->load(['details.component.barcodes']);

            $details = $record->details;

            return $this->successfulResponse([
                'record'  => $this->item($record, FullInfoTransformer::class),
                'details' => $this->collection($details, DetailFullInfoTransformer::class),
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductConversionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductConversionRequest $request)
    {
        $this->authorize('store', $this->entity);

        $rule = $this->rule('store', $this->rule, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'record' => $this->item($result, $this->transformers['basic']),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductConversionRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductConversionRequest $request, $id)
    {
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', $this->rule, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', $this->rule, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            Log::error($e);

            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Updates the specified transaction to the
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approval(Request $request)
    {
        $transactions = $request->get('transactions');
        $status = $request->get('status');

        if($status == ApprovalStatus::APPROVED) {
            $this->authorize('approve', $this->entity);
        } else if($status == ApprovalStatus::DECLINED) {
            $this->authorize('decline', $this->entity);
        }

        $rule = $this->rule('approval', $this->rule, $transactions, $status);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $this->service->approval($transactions, $status);

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    /**
     * Get all the detail transaction of the product provided
     *
     * @param Request $request
     * @param ProductRepository $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function productTransactions(Request $request, ProductRepository $repository)
    {   
        $product = $repository->find($request->get('product_id'));

        $transactions = null;
        $transformer = null;

        if($request->get('type') == 1) {
            if($product->group_type == GroupType::MANUAL_GROUP) {
                //get head inc
                $transactions = $this->repository->getGroupIncrement($request->get('product_id'), $request->get('per_page', 10));
                $transformer = TransactionTransformer::class;
            } else {
                //get detail inc
                $transactions = $this->repository->getComponentIncrement($request->get('product_id'), $request->get('per_page', 10));
                $transformer = TransactionDetailTransformer::class;
            }
        } else if($request->get('type') == 2) {
            if($product->group_type == GroupType::MANUAL_UNGROUP) {
                //get head dec
                $transactions = $this->repository->getGroupDecrement($request->get('product_id'), $request->get('per_page', 10));
                $transformer = TransactionTransformer::class;
            } else {
                //get detail dec
                $transactions = $this->repository->getComponentDecrement($request->get('product_id'), $request->get('per_page', 10));
                $transformer = TransactionDetailTransformer::class;
            }
        }

        if($transactions->count() === 0) {
            return $this->errorResponse(array(
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ));
        }

        return $this->successfulResponse($this->collection($transactions, $transformer));
    }

    /**
     * Generate excel data
     *
     * @param Request $request
     */
    public function export(Request $request)
    {
        $result = $this->repository
            ->with($this->eagerLoad)
            ->withTrashed()
            ->all();

        if ($result->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($result, $this->transformers['export'])['data'];

        App::make($this->export)
            ->settings($request->get('settings'))
            ->list($data);
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    private function form()
    {
        list($permissions) = $this->references();

        return view('core::product-conversion.detail',
            compact('permissions')
        );
    }

    private function list()
    {
        list($permissions) = $this->references();

        return view('core::product-conversion.list',
            compact('permissions')
        );
    }

    private function references()
    {
        return [
            $this->permissions(),
        ];
    }

    private function trashed($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if(!$model) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $model->load(['details.component.barcodes']);

        $details = $model->details;

        return $this->successfulResponse([
            'records' => $this->item($model, FullInfoTransformer::class),
            'details' => $this->collection($details, DetailFullInfoTransformer::class),
        ]);
    }

    private function permissions()
    {
        return [
            'view'              => Auth::user()->can('view', $this->entity),
            'view_detail'       => Auth::user()->can('viewDetail', $this->entity),
            'create'            => Auth::user()->can('store', $this->entity),
            'update'            => Auth::user()->can('update', $this->entity),
            'delete'            => Auth::user()->can('delete', $this->entity),
            'approve'           => Auth::user()->can('approve', $this->entity),
            'decline'           => Auth::user()->can('decline', $this->entity),
        ];
    }
}
