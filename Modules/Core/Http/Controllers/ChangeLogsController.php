<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\Controller;

class ChangeLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('core::home.changelogs');
    }
}
