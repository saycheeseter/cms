<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Http\Requests\ImportExcelRequest;
use Modules\Core\Repositories\Contracts\ProductBatchRepository;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Repositories\Contracts\ProductTransactionSummaryRepository;
use Modules\Core\Repositories\Contracts\ProductStockCardRepository;
use Modules\Core\Repositories\Contracts\ProductSerialNumberRepository;
use Modules\Core\Services\Contracts\Import\ProductImportExcelServiceInterface;
use Modules\Core\Services\Contracts\ProductServiceInterface;
use Modules\Core\Services\Contracts\Export\ProductAvailableSerialsExportServiceInterface;
use Modules\Core\Http\Requests\ProductRequest;
use Modules\Core\Http\Requests\BarcodeScanRequest;
use Modules\Core\Http\Requests\StockNoRequest;
use Modules\Core\Transformers\Product\AlternativeTransformer;
use Modules\Core\Transformers\Product\SummaryTransformer;
use Modules\Core\Transformers\Product\ListTransformer;
use Modules\Core\Transformers\Product\ExportListTransformer;
use Modules\Core\Transformers\Product\BasicTransformer;
use Modules\Core\Transformers\Product\FullInfoTransformer;
use Modules\Core\Transformers\BranchPrice\BasicTransformer as BranchPriceTransformer;
use Modules\Core\Transformers\ProductBranchInfo\BasicTransformer as ProductBranchInfoTransformer;
use Modules\Core\Transformers\ProductBranchDetail\BasicTransformer as ProductBranchDetailTransformer;
use Modules\Core\Transformers\ProductSerialNumber\AvailableSerialTransformer;
use Modules\Core\Transformers\ProductUnit\BasicTransformer as UnitTransformer;
use Modules\Core\Transformers\ProductBarcode\BasicTransformer as BarcodeTransformer;
use Modules\Core\Transformers\Product\SupplierPriceTransformer;
use Modules\Core\Transformers\Product\CustomerPriceTransformer;
use Modules\Core\Transformers\Product\SummaryAndPriceTransformer;
use Modules\Core\Transformers\Product\ComponentTransformer;
use Modules\Core\Transformers\Product\ConversionTransformer;
use Modules\Core\Transformers\Product\ChosenTransformer as ProductChosenTransformer;;
use Modules\Core\Transformers\ProductTransactionSummary\TransactionSummaryTransformer;
use Modules\Core\Transformers\ProductTransactionSummary\BeginningTransformer;
use Modules\Core\Transformers\ProductStockCard\BasicTransformer as ProductStockCardTransformer;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\Category\ChosenTransformer as CategoryChosenTransformer;
use Modules\Core\Transformers\Tax\ChosenTransformer as TaxChosenTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\BranchDetail\ProductLocationTransformer;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialTransformer;
use Modules\Core\Transformers\ProductBatch\BasicTransformer as BatchBasicTransformer;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\UOMRepository;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\BranchDetailRepository;
use Modules\Core\Repositories\Contracts\TaxRepository;
use Modules\Core\Rules\Contracts\ProductRuleInterface as ProductRule;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\WalkInPriceSettings;
use Modules\Core\Enums\BranchPrice;
use Modules\Core\Enums\PriceScheme;
use Modules\Core\Entities\Product;
use Modules\Core\Services\Contracts\Export\ProductExportServiceInterface as ProductExport;
use Exception;
use Lang;
use App;
use Auth;

class ProductController extends Controller
{
    protected $service;
    protected $repository;
    protected $entity;

    public function __construct(ProductRepository $repository, ProductServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Product::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $data = $this->repository->search($request->input('filters'));

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, ListTransformer::class, 'products'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::product.list', compact('permissions'));
    }

    /**
     * Show the product create form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show the details of the specified resource id
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {   
        if (!$request->wantsJson()) {
            return false;
        }

        $this->authorize('viewDetail', $this->entity);
        
        $product = $this->repository
            ->with([
                'prices.branch',
                'units',
                'barcodes',
                'taxes',
                'branches.branch',
                'locations.location',
                'alternatives',
                'attachments',
                'components'
            ])
            ->find($id);

        if (!$product) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::common.show.failed')
            ));
        }

        $prices = $product->prices;
        $units = $product->units;
        $barcodes = $product->barcodes;
        $branches = $product->branches;
        $location = $product->locations;
        $taxes = $product->taxes;
        $alternatives = $product->alternatives;
        $components = $product->components;
        
        return $this->successfulResponse(array(
            'product'      => $this->item($product, FullInfoTransformer::class),
            'prices'       => $this->collection($prices, BranchPriceTransformer::class)['data'],
            'units'        => $this->collection($units, UnitTransformer::class)['data'],
            'barcodes'     => $this->collection($barcodes, BarcodeTransformer::class)['data'],
            'branches'     => $this->collection($branches, ProductBranchInfoTransformer::class)['data'],
            'locations'    => $this->collection($location, ProductBranchDetailTransformer::class)['data'],
            'taxes'        => $taxes->pluck('id')->toArray(),
            'attachments'  => $this->collection($product->attachments, AttachmentBasicTransformer::class)['data'],
            'alternatives' => $this->collection($alternatives, BasicTransformer::class)['data'],
            'components'   => $this->collection($components, ComponentTransformer::class)['data'],
        ));
    }

    /**
     * Show the product edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->repository
            ->whereActiveInBranch()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductRequest $request)
    {   
        $this->authorize('store', $this->entity);

        $rule = $this->rule('store', ProductRule::class, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'product' => $this->item($result, BasicTransformer::class),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', ProductRule::class, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'product' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Returns the list of specified products with their corresponding
     * supplier prices
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectWithSupplierPrice(Request $request)
    {
        $rule = $this->rule('selectWithSupplierPrice', ProductRule::class, $request->all());

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $result = $this->repository->getSupplierPrices(
            $request->get('product_ids'),
            $request->get('supplier_id'),
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if ($result->count() === 0) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->successfulResponse([
            'products' => $this->collection($result, SupplierPriceTransformer::class)['data']
        ]);
    }

    /**
     * Returns the list of specified products with their corresponding
     * customer prices
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectWithCustomerPrice(Request $request)
    {
        if ((int) $request->get('customer_type') === Customer::WALK_IN) {
            $request = $this->rebind($request, 'price', $this->getWalkInPricing());

            return $this->selectWithSummaryAndPrice($request);
        }

        $rule = $this->rule('selectWithCustomerPrice', ProductRule::class, $request->all());

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $result = $this->repository->getCustomerPrices(
            $request->get('product_ids'),
            $request->get('customer_id'),
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if ($result->count() === 0) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->successfulResponse([
            'products' => $this->collection($result, CustomerPriceTransformer::class)['data']
        ]);
    }

    /**
     * Returns the list of specified products with their corresponding
     * summary and requested prices
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectWithSummaryAndPrice(Request $request)
    {
        $rule = $this->rule('selectWithSummaryAndPrice', ProductRule::class, $request->all());

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        $result = $this->repository->getSummariesAndPrices(
            $request->get('product_ids'),
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if ($result->count() === 0) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->successfulResponse([
            'products' => $this->collection($result, SummaryAndPriceTransformer::class)['data']
        ]);
    }

    /**
     * Scan mode function, search the product by the given barcode and return its corresponding pricing
     * based on the scheme type
     *
     * @param Request|BarcodeScanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findByBarcode(BarcodeScanRequest $request)
    {
        $product = $this->repository->findByBarcode($request->get('barcode'), $request->get('excluded'));

        if (!$product) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->getProductPriceByScheme($request, $product->id);
    }

    /**
     * Scan mode function, search the product by the given stock number and return its corresponding pricing
     * based on the scheme type
     *
     * @param StockNoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findByStockNo(StockNoRequest $request)
    {
        $product = $this->repository->findByStockNo($request->get('stock_no'), $request->get('excluded'));

        if (!$product) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->getProductPriceByScheme($request, $product->id);
    }

    public function searchByName(Request $request)
    {
        $results = $this->repository->searchByName($request->get('filters'));

        return $this->successfulResponse($this->collection($results, ProductChosenTransformer::class)['data']);
    }

    /**
     * Condition on where to redirect the price scheme
     *
     * @param $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    private function getProductPriceByScheme($request, $id)
    {
        $request = $this->rebind($request, 'product_ids', $id);

        switch ($request->get('scheme')) {
            case PriceScheme::CUSTOMER:
                $result = $this->selectWithCustomerPrice($request);
                break;

            case PriceScheme::SUPPLIER:
                $result = $this->selectWithSupplierPrice($request);
                break;

            default:
                $result = $this->selectWithSummaryAndPrice($request);
                break;
        }

        $data = $result->getData();

        if(!$data->isSuccessful) {
            return $result;
        }

        $product = $data->values->products[0];

        return $this->successfulResponse([
            'product' => $product
        ]);
    }

    /**
     * Display the product summary page
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report($id)
    {
        $product = $this->repository
            ->whereActiveInBranch()
            ->with(['barcodes'])
            ->find($id);

        if (!$product) {
            throw new ModelNotFoundException;
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $suppliers = $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'];

        return view('core::product.summary', compact('branches', 'suppliers', 'product'));
    }

    /**
     * Get the transaction summary of the requested product
     * 
     * @param  Request $request
     * @param  ProductTransactionSummaryRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function transactionSummary(Request $request, ProductTransactionSummaryRepository $repository, $id)
    {
        $beginning = null;
        
        $result = $repository->search($id);
        
        $earliestDate = $this->getEarliestDate($request->get('filters'));

        if(!is_null($earliestDate)) {
            $beginning = $repository->getBeginning($id, $earliestDate);
        }

        return $this->successfulResponse([
            'summary' => array_merge(
                $this->item($result, TransactionSummaryTransformer::class), 
                $this->item($beginning, BeginningTransformer::class)
            )
        ]);
    }

    /**
     * Get the stock card summary of the requested product
     * 
     * @param  ProductStockCardRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function stockCard(Request $request, ProductStockCardRepository $repository, $id)
    {
        $results = $repository->search($id, $request->get('per_page'));

        return $this->successfulResponse([
            'stock_card' => $this->collection($results, ProductStockCardTransformer::class)
        ]);
    }

    /**
     * Get the list of serials of the indicated product
     *
     * @param  ProductSerialNumberRepository $repository
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function serials(ProductSerialNumberRepository $repository, Request $request, $id)
    {
        $serials = $repository->search(
            $id,
            $request->get('constraint'),
            $request->get('entity'),
            $request->get('status'),
            $request->get('transaction'),
            $request->get('supplier_id')
        );

        if ($serials->count() === 0) {
            return $this->errorResponse(array(
                'no.serials.found' => Lang::get('core::error.no.serials.found')
            ));
        }

        return $this->successfulResponse([
            'serials' => $this->collection($serials, SerialTransformer::class)['data']
        ]);
    }

    /**
     * Get the list of batches of the indicated product
     *
     * @param  ProductBatchRepository $repository
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function batches(ProductBatchRepository $repository, Request $request, $id)
    {
        $batches = $repository->search(
            $id,
            $request->get('constraint'),
            $request->get('entity'),
            $request->get('supplier')
        );

        if ($batches->count() === 0) {
            return $this->errorResponse(array(
                'no.batches.found' => Lang::get('core::error.no.batches.found')
            ));
        }

        return $this->successfulResponse([
            'batches' => $this->collection($batches, BatchBasicTransformer::class)['data']
        ]);
    }

    /**
     * Find the indicated products and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request, $type)
    {
        $products = $this->repository->findMany($request->get('ids'));

        if ($products->count() === 0) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        switch ($type) {
            case 'basic':
                $transformer = BasicTransformer::class;
                break;

            default:
                $transformer = BasicTransformer::class;
                break;
        }

        return $this->successfulResponse([
            'products' => $this->collection($products, $transformer)['data']
        ]);
    }


    /**
     * Get the summary of the indicated product within the specified branch
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function summary(Request $request, $id)
    {
        $product = $this->repository->getSummaries(
            $id,
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if (!$product) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->successfulResponse([
            'product' => $this->item($product, SummaryTransformer::class)
        ]);
    }

    /**
     * Get the summaries of the given products within the specified branch
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function summaries(Request $request)
    {
        $products = $this->repository->getSummaries(
            $request->get('product_ids'),
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if ($products->count() === 0) {
            return $this->errorResponse(array(
                'no.products.found' => Lang::get('core::error.no.products.found')
            ));
        }

        return $this->successfulResponse([
            'products' => $this->collection($products, SummaryTransformer::class)['data']
        ]);
    }

    /**
     * Get the list alternatives of the indicated product
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function alternatives(Request $request, $id)
    {
        $alternatives = $this->repository->alternatives(
            $id,
            $request->get('branch_id'),
            $request->get('location_id')
        );

        if ($alternatives->count() === 0) {
            return $this->errorResponse(array(
                'insufficient.inv.no.alternatives.found' => Lang::get('core::error.insufficient.inv.no.alternatives.found')
            ));
        }

        return $this->successfulResponse([
            'products' => $this->collection($alternatives, AlternativeTransformer::class)['data']
        ]);
    }

    /**
     * Get the components of the specified resource id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @internal param Request $request
     */
    public function components($id)
    {
        $components = $this->repository->components($id);

        if ($components->count() === 0) {
            return $this->errorResponse(array(
                'no.components.found' => Lang::get('core::error.no.components.found')
            ));
        }

        return $this->successfulResponse([
            'components' => $this->collection($components, ConversionTransformer::class)['data']
        ]);
    }

    /**
     * Get the list of available serials for the indicated product and filters
     *
     * @param ProductSerialNumberRepository $repository
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function availableSerials(ProductSerialNumberRepository $repository, Request $request, $id)
    {
        $serials = $repository->getAvailableSerials($id, $request->get('filters'));

        $message = $serials->count() === 0
            ? Lang::get('core::error.no.serials.found')
            : '';

        return $this->successfulResponse([
            'serials' => $this->collection($serials, AvailableSerialTransformer::class)
        ], $message);
    }

    public function exportAvailableSerials(ProductSerialNumberRepository $repository, Request $request, $id)
    {
        $serials = $repository->getAvailableSerials($id, $request->get('filters'), false);

        if($serials->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($serials, AvailableSerialTransformer::class)['data'];

        App::make(ProductAvailableSerialsExportServiceInterface::class)->list($data);
    }

    public function findBySerialNumber(ProductSerialNumberRepository $repository, Request $request, $id)
    {
        $serials = $repository->getAvailableSerials($id, $request->get('filters'));

        $message = $serials->count() === 0
            ? Lang::get('core::error.no.serials.found')
            : '';

        return $this->successfulResponse([
            'serials' => $this->collection($serials, AvailableSerialTransformer::class)
        ], $message);
    }

    public function importExcel(ImportExcelRequest $request)
    {
        $this->authorize('importExcel', $this->entity);

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        if (!$request->hasFile('attachment')) {
            return $this->errorResponse(array(
                'no.files.found' => Lang::get('core::error.files.not.found')
            ));
        }

        try {
            $logs = App::make(ProductImportExcelServiceInterface::class)
                ->setFile($request->file('attachment'))
                ->execute()
                ->getLogs();
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'unable.to.import.excel' => $e->getMessage()
            ));
        }

        return $this->successfulResponse([
            'logs' => $logs
        ]);
    }

    public function downloadImportExcelTemplate()
    {
        $this->authorize('importExcel', $this->entity);

        App::make(ProductImportExcelServiceInterface::class)->exportDefaultTemplate();
    }

    public function export(Request $request)
    {
        $this->checkPermission('export_excel_product');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $products = App::make(ProductRepository::class)->search($request->get('filters'), false);

        if($products->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($products, ExportListTransformer::class)['data'];

        App::make(ProductExport::class)->settings($request->get('settings'))->list($data);
    }

    public function updateTransactionsPurchasePrice(Request $request, $id)
    {
        ini_set('max_execution_time', 120);

        try {
            $result = $this->service->updateTransactionsPurchasePrice(
                $id,
                $request->get('branch_id'),
                $request->get('to'),
                $request->get('from'),
                $request->get('cost')
            );
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'apply.failed' => Lang::get('core::error.apply.purchase.price.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.apply.purchase.price'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'product',
            'units',
            'prices',
            'barcodes',
            'taxes',
            'branches',
            'locations',
            'alternatives',
            'components'
        );
    }

    private function form()
    {
        list($suppliers, $brands, $uoms, $categories, $taxes, $branches, $locations, $permissions) = $this->references();

        return view('core::product.detail', compact(
            'suppliers', 
            'brands', 
            'uoms', 
            'branches', 
            'categories', 
            'taxes', 
            'locations', 
            'permissions'
        ));
    }

    private function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(BrandRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->collection(App::make(UOMRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->collection(App::make(CategoryRepository::class)->all(), CategoryChosenTransformer::class)['data'],
            $this->collection(App::make(TaxRepository::class)->all(), TaxChosenTransformer::class)['data'],
            $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            $this->collection(App::make(BranchDetailRepository::class)->with(['branch'])->all(), ProductLocationTransformer::class)['data'],
            $this->permissions()
        ];
    }

    /**
     * Get the lowest possible date in the set of fitlter
     * 
     * @param  array $filters
     * @return string
     */
    private function getEarliestDate($filters)
    {
        return collect($filters)
            ->filter(function ($value, $key) {
                return $value['column'] == 'transaction_date';
            })
            ->pluck('value')
            ->sort()
            ->first();
    }
    
    /**
     * Get current branch price enum value based on walk in customer settings
     * 
     * @return int
     */
    private function getWalkInPricing()
    {
        $walkinPriceBasis = setting('soi.walkin.customers.price.basis');
            
        switch ($walkinPriceBasis) {
            case  WalkInPriceSettings::WHOLESALE:
                return BranchPrice::WHOLESALE;
                break;

            case  WalkInPriceSettings::RETAIL:
                return BranchPrice::SELLING;
                break;

            case  WalkInPriceSettings::PRICE_A:
                return BranchPrice::A;
                break;

            case  WalkInPriceSettings::PRICE_B:
                return BranchPrice::B;
                break;

            case  WalkInPriceSettings::PRICE_C:
                return BranchPrice::C;
                break;

            case  WalkInPriceSettings::PRICE_D:
                return BranchPrice::D;
                break;
                
            case  WalkInPriceSettings::PRICE_E:
                return BranchPrice::E;
                break;
        }
    }

    /**
     * List of product permissions
     * 
     * @return array
     */
    protected function permissions()
    {
        return [
            'view'            => Auth::user()->can('view', $this->entity),
            'view_detail'     => Auth::user()->can('viewDetail', $this->entity),
            'update'          => Auth::user()->can('update', $this->entity),
            'delete'          => Auth::user()->can('delete', $this->entity),
            'create'          => Auth::user()->can('store', $this->entity),
            'column_settings' => Auth::user()->can('useColumnSettings', $this->entity),
            'show_cost'       => Auth::user()->can('showCost', $this->entity),
            'import_excel'    => Auth::user()->can('importExcel', $this->entity),
            'export_excel'    => Auth::user()->can('exportExcel', $this->entity),
        ];
    }

    protected function rebind($request, $key, $value)
    {
        $request->request->add([$key => $value]);

        $request[$key] = $value;

        App::instance(Request::class, $request);

        return $request;
    }
}
