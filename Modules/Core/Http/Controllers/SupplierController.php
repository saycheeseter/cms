<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\SupplierBalanceFlowRepository;
use Modules\Core\Services\Contracts\SupplierServiceInterface;
use Modules\Core\Http\Requests\SupplierRequest;
use Modules\Core\Transformers\Supplier\BasicTransformer;
use Modules\Core\Transformers\Supplier\FullInfoTransformer;
use Modules\Core\Transformers\BalanceFlow\BalanceTransformer;
use Modules\Core\Entities\Supplier;
use App;
use Lang;
use Exception;
use Auth;

class SupplierController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(SupplierRepository $repository, SupplierServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Supplier::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'suppliers'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::supplier.list', compact('permissions'));
    }

    /**
     * Find the data by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $supplier = $this->repository->find($id);

        if (!$supplier) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        return $this->successfulResponse(array(
            'supplier' => $this->item($supplier, FullInfoTransformer::class)
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  SupplierRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SupplierRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'supplier' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SupplierRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SupplierRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'supplier' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated suppliers and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $suppliers = $this->repository->findMany($request->get('ids'));

        if ($suppliers->count() === 0) {
            return $this->errorResponse(array(
                'no.suppliers.found' => Lang::get('core::error.no.suppliers.found')
            ));
        }

        return $this->successfulResponse([
            'suppliers' => $this->collection($suppliers, BasicTransformer::class)['data']
        ]);
    }

    /**
     * Get the current balance of the specified supplier
     * 
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function balance($id)
    {
        $supplier = $this->repository->find($id);

        if (!$supplier) {
            return $this->errorResponse(array(
                'supplier.not.found' => Lang::get('core::error.supplier.not.found')
            ));
        }

        $result = App::make(SupplierBalanceFlowRepository::class)->getBalance($id);

        return $this->successfulResponse([
            'balance' => $this->item($result, BalanceTransformer::class)
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'code',
            'full_name',
            'chinese_name',
            'owner_name',
            'contact',
            'address',
            'landline',
            'telephone',
            'fax',
            'mobile',
            'email',
            'website',
            'term',
            'status',
            'contact_person',
            'memo'
        );
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }
}
