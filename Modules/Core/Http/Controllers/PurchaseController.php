<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\PurchaseRepository;
use Modules\Core\Services\Contracts\PurchaseServiceInterface;
use Modules\Core\Transformers\Purchase\BasicTransformer;
use Modules\Core\Transformers\Purchase\FullInfoTransformer;
use Modules\Core\Transformers\Purchase\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\Purchase\ExportListTransformer;
use Modules\Core\Transformers\Purchase\RetrieveTransformer;
use Modules\Core\Transformers\PurchaseDetail\RetrieveTransformer as DetailRetrieveTransformer;
use Modules\Core\Transformers\PurchaseDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Http\Requests\PurchaseRequest;
use Modules\Core\Http\Requests\PurchaseApprovedRequest;
use Modules\Core\Http\Requests\StockRequestOrderRequest;
use Modules\Core\Rules\Contracts\PurchaseRuleInterface as PurchaseRule;
use Modules\Core\Services\Contracts\Export\PurchaseExportServiceInterface as PurchaseExport;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\BranchDetailRepository;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as BranchDetailChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\Purchase;
use Lang;
use App;

class PurchaseController extends InvoiceController
{
    public function __construct(PurchaseRepository $repository, PurchaseServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = PurchaseExport::class;
        $this->rule = PurchaseRule::class;
        $this->request['transaction'] = PurchaseRequest::class;
        $this->request['info'] = PurchaseApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['retrieve']['detail'] = DetailRetrieveTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['detail'] = DetailFullInfoTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = Purchase::class;
        $this->module = ModuleDefinedFields::PURCHASE;
        $this->eagerLoad = [
            'deliverTo',
            'payment',
            'supplier',
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'requested'
        ];
    }
    
    /**
     * Create a new transaction based from the product's supplier and stock request data
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFromStockRequestBySupplier(StockRequestOrderRequest $request)
    {
        $result = $this->service->createFromStockRequestBySupplier($this->data($request));

        if (!$result) {
            return $this->errorResponse(array(
                'create.from.stock.request.failed' => Lang::get('core::error.create.from.stock.request.failed')
            ));
        }

        return $this->successfulResponse([
            'invoices' => $this->collection($result, $this->transformers['basic'])['data'],
        ], Lang::get('core::success.created'));
    }

    /**
     * List of module references
     * 
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(PaymentMethodRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list(
            $tsuppliers, 
            $paymentMethods, 
            $branchDetails, 
            $udfs, 
            $permissions,
            $templates
        ) = $this->references();

        return view('core::purchase-order.detail', 
            compact(
                'tsuppliers', 
                'paymentMethods', 
                'branchDetails', 
                'udfs', 
                'permissions',
                'templates'
            )
        ); 
    }

    protected function list()
    {
        return view('core::purchase-order.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
