<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Services\Contracts\PrintoutTemplateServiceInterface;
use Modules\Core\Http\Requests\PrintoutTemplateRequest;
use Modules\Core\Transformers\PrintoutTemplate\BasicTransformer;
use Modules\Core\Transformers\PrintoutTemplate\FullInfoTransformer;
use Modules\Core\Entities\PrintoutTemplate;

use App;
use Lang;
use Exception;
use Auth;

class PrintoutTemplateController extends Controller
{
    protected $service;
    protected $repository;

    public function __construct(PrintoutTemplateRepository $repository, PrintoutTemplateServiceInterface $service)
    {   
        $this->entity = PrintoutTemplate::class;
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'templates'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::printout.list', compact('permissions'));
    }

    /**
     * Show create form and return list of references
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show the edit form and return the list of references
     *
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);
        
        if ($request->wantsJson()) {

            $template = $this->repository->find($id);

            if(!$template) {
                return $this->errorResponse(array(
                    'show.failed' => Lang::get('core::error.show.failed')
                ));
            }

            return $this->successfulResponse([
                'template' => $this->item($template, FullInfoTransformer::class),
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PrintoutTemplateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PrintoutTemplateRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PrintoutTemplateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PrintoutTemplateRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    protected function references()
    {
        return [
            $this->permissions()
        ];
    }

    protected function form()
    {
        list($permissions) = $this->references();

        return view('core::printout.detail',
            compact('permissions')
        );
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }

    protected function data($request)
    {
        return $request->only(
            'name',
            'module_id',
            'format'
        );
    }
}
