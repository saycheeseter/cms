<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\DamageRepository;
use Modules\Core\Services\Contracts\DamageServiceInterface;
use Modules\Core\Transformers\Damage\BasicTransformer;
use Modules\Core\Transformers\Damage\FullInfoTransformer;
use Modules\Core\Transformers\Damage\ExportListTransformer;
use Modules\Core\Transformers\Damage\RetrieveTransformer;
use Modules\Core\Transformers\Damage\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\DamageRequest;
use Modules\Core\Http\Requests\DamageApprovedRequest;
use Modules\Core\Rules\Contracts\DamageRuleInterface as DamageRule;
use Modules\Core\Services\Contracts\Export\DamageExportServiceInterface as DamageExport;
use Modules\Core\Repositories\Contracts\ReasonRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\Damage;
use Lang;
use App;

class DamageController extends InvoiceController
{
    public function __construct(DamageRepository $repository, DamageServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->export = DamageExport::class;
        $this->rule = DamageRule::class;
        $this->request['transaction'] = DamageRequest::class;
        $this->request['info'] = DamageApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = Damage::class;
        $this->module = ModuleDefinedFields::DAMAGE;
        $this->eagerLoad = [
            'reason',
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'requested'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(ReasonRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($reasons, $udfs, $permissions, $templates) = $this->references();

        return view('core::damage.detail',
            compact('reasons', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::damage.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'reasons'     => $this->collection(App::make(ReasonRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
