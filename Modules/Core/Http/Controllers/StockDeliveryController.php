<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\StockDeliveryRepository;
use Modules\Core\Services\Contracts\StockDeliveryServiceInterface;
use Modules\Core\Transformers\StockDelivery\BasicTransformer;
use Modules\Core\Transformers\StockDelivery\FullInfoTransformer;
use Modules\Core\Transformers\StockDelivery\ExportListTransformer;
use Modules\Core\Transformers\StockDelivery\RetrieveTransformer;
use Modules\Core\Transformers\StockDeliveryDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\StockDelivery\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\StockDeliveryRequest;
use Modules\Core\Http\Requests\StockDeliveryApprovedRequest;
use Modules\Core\Rules\Contracts\StockDeliveryRuleInterface as StockDeliveryRule;
use Modules\Core\Services\Contracts\Export\StockDeliveryExportServiceInterface as StockDeliveryExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as BranchDetailChosenTransformer;
use Modules\Core\Transformers\Invoice\ChosenTransformer as InvoiceChosenTransformer;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\StockDelivery;
use Lang;
use App;
use Auth;

class StockDeliveryController extends InvoiceController
{
    public function __construct(StockDeliveryRepository $repository, StockDeliveryServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockDeliveryExport::class;
        $this->rule = StockDeliveryRule::class;
        $this->request['transaction'] = StockDeliveryRequest::class;
        $this->request['info'] = StockDeliveryApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = StockDelivery::class;
        $this->module = ModuleDefinedFields::STOCK_DELIVERY;
        $this->eagerLoad = [
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'deliverTo',
            'deliverToLocation',
            'requested',
            'reference'
        ];
    }

    /**
     * Show the specified resource data
     * 
     * @param  Request $request
     * @param  int  $id     
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {   
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {

            $invoice = $this->repository->find($id);

            if(!$invoice) {
                return $this->trashed($request, $id);
            }

            $invoice->load([
                'attachments',
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.components'
            ]);

            $details = $this->paginate($invoice->details, 10);

            return $this->successfulResponse([
                $this->key['transaction'] => $this->item($invoice, FullInfoTransformer::class),
                'details'                 => $this->collection($details, DetailFullInfoTransformer::class),
                'requests'                => is_null($invoice->reference)
                    ? []
                    : [$this->item($invoice->reference, InvoiceChosenTransformer::class)],
                'attachments'             => $this->collection($invoice->attachments, AttachmentBasicTransformer::class)['data'],
            ]);
        }
    }

    protected function trashed(Request $request, $id)
    {
        $invoice = $this->repository
            ->withTrashed()
            ->find($id);

        if(!$invoice) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $invoice->load([
            'details' => function ($query) use ($invoice) {
                $query->withTrashed()
                    ->where('deleted_at', '>=', $invoice->deleted_at);
            },
            'attachments',
            'details.product',
            'details.barcodes',
            'details.units.uom',
            'details.components'
        ]);

        $details = $this->paginate($invoice->details, 10);

        return $this->successfulResponse([
            $this->key['transaction'] => $this->item($invoice, FullInfoTransformer::class),
            'details'                 => $this->collection($details, DetailFullInfoTransformer::class),
            'requests'                => is_null($invoice->reference)
                ? []
                : [$this->item($invoice->reference, InvoiceChosenTransformer::class)],
            'attachments'             => $this->collection($invoice->attachments, AttachmentBasicTransformer::class)['data'],
        ]);
    }

    /**
     * List of module references
     * 
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::stock-delivery.detail', 
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::stock-delivery.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
