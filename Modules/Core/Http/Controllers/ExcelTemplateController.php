<?php

namespace Modules\Core\Http\Controllers;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Repositories\Contracts\UserDefinedFieldRepository;
use Modules\Core\Services\Contracts\ExcelTemplateServiceInterface;
use Modules\Core\Http\Requests\ExcelTemplateRequest;
use Modules\Core\Transformers\ExcelTemplate\BasicTransformer;
use Modules\Core\Transformers\ExcelTemplate\FullInfoTransformer;
use Modules\Core\Transformers\ExcelTemplate\ListTransformer;
use Modules\Core\Transformers\UserDefinedField\BasicTransformer as UserDefinedFieldTransformer;
use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface as ExcelTemplateExport;
use Modules\Core\Entities\ExcelTemplate;

use Lang;
use App;
use Auth;

class ExcelTemplateController extends Controller
{
    protected $service;
    protected $repository;
    protected $entity;

    public function __construct(ExcelTemplateRepository $repository, ExcelTemplateServiceInterface $service)
    {   
        $this->entity = ExcelTemplate::class;
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {      
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, ListTransformer::class, 'templates'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::excel-template.list', compact('permissions'));
    }

    /**
     * Show create form and return list of available templates
     * 
     * @param  ExcelTemplateRepository $repository
     * @return Response
     */
    public function create()
    {   
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    public function edit($id)
    {
        $model = $this->repository
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Find the data by id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   
        $this->authorize('viewDetail', $this->entity);

        $template = $this->repository
            ->find($id);

        if (!$template) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::common.show.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($template, FullInfoTransformer::class),
            'udfs' => $this->udf($template->module)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ExcelTemplateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExcelTemplateRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, BasicTransformer::class),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ExcelTemplateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExcelTemplateRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([
            'template' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Find the module udfs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUDF($module)
    {   
        return $this->successfulResponse([
            'udfs' => $this->udf($module)
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }

    public function preview(Request $request)
    {
        $data = $this->repository->find($request->get('template_id'));

        App::make(ExcelTemplateExport::class)
            ->setFileName($data->name)
            ->setTemplateFormat($data->format)
            ->previewOnly()
            ->generate();
    }

    private function data(Request $request)
    {
        return $request->only(
            'module',
            'name',
            'format'
        );
    }

    protected function form()
    {   
        $permissions = $this->permissions();

        return view('core::excel-template.detail', compact('permissions'));
    }

    protected function udf($module)
    {
        return $this->collection(
            App::make(UserDefinedFieldRepository::class)
                ->findByModule($module), 
            UserDefinedFieldTransformer::class
        )['data'];
    }
}
