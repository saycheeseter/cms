<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Modules\Core\Traits\ChecksUserPermission;
use Modules\Core\Traits\ResponsesJson;
use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Traits\PaginatesCollection;
use Modules\Core\Traits\GeneratesRule;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\Authorizable;

abstract class Controller extends BaseController
{
    use ResponsesJson,
        FractalTransformer,
        PaginatesCollection,
        GeneratesRule,
        AuthorizesRequests,
        Authorizable,
        ChecksUserPermission;
}
