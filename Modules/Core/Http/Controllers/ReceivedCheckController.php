<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Services\Contracts\ReceivedCheckServiceInterface as ReceivedCheckService;
use Modules\Core\Repositories\Contracts\ReceivedCheckRepository;
use Modules\Core\Entities\ReceivedCheck;

class ReceivedCheckController extends CheckManagementController
{
    protected $form = 'core::received-check.list';

    public function __construct(ReceivedCheckRepository $repository, ReceivedCheckService $service)
    {
        $this->repository = $repository;
        $this->service = $service;  
        $this->entity = ReceivedCheck::class;
    }
}