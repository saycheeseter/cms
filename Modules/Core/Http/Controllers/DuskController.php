<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Auth;
use App;

class DuskController
{
    /**
     * Retrieve the authenticated user identifier and class name.
     *
     * @param  string|null  $guard
     * @return array
     */
    public function user($guard = null)
    {
        $user = Auth::guard($guard)->user();

        if (! $user) {
            return [];
        }

        return [
            'id' => $user->getAuthIdentifier(),
            'className' => get_class($user),
        ];
    }

    /**
     * Login using the given user ID / email.
     *
     * @param  string  $userId
     * @param  string  $guard
     * @return Response
     */
    public function login($userId, $branchId, $guard = null)
    {
        $guard = $guard ?: config('auth.defaults.guard');

        $user = App::make(UserRepository::class)
            ->with(['branchPermissions' => $branchId])
            ->find($userId);

        $branch = App::make(BranchRepository::class)->find($branchId);
        $permissions = $user->permissions;

        Auth::guard($guard)->login($user);
        Auth::setConfig($branch, $permissions);
    }

    /**
     * Log the user out of the application.
     *
     * @param  string  $guard
     * @return Response
     */
    public function logout($guard = null)
    {
        Auth::guard($guard ?: config('auth.defaults.guard'))->logout();
    }

    /**
     * Get the model for the given guard.
     *
     * @param  string  $guard
     * @return string
     */
    protected function modelForGuard($guard)
    {
        $provider = config("auth.guards.{$guard}.provider");

        return config("auth.providers.{$provider}.model");
    }
}
