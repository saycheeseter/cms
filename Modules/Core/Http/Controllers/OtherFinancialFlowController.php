<?php 

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Requests\OtherFinancialFlowRequest;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Repositories\Eloquent\BranchRepositoryEloquent;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\BankAccount\ChosenTransformer as BankAccountChosenTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Lang;
use Exception;
use Auth;
use App;

abstract class OtherFinancialFlowController extends Controller
{
    protected $repository;
    protected $service;
    protected $transformers;
    protected $blade;
    protected $rule;
    protected $entity;

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
      */
    public function index(Request $request)
    {   
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository
                ->with(['method', 'creator', 'from'])
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';
                
            return $this->successfulResponse(
                $this->collection($data, $this->transformers['basic']),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        return $this->form();
    }

    public function show($id)
    {
        $this->authorize('viewDetail', $this->entity);
        
        $data = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$data) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $details = $this->paginate($data->details, 10);

        return $this->successfulResponse(array(
            'data' => $this->item($data, $this->transformers['full']),
            'details' => $this->collection($details,  $this->transformers['detail'])
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OtherFinancialFlowRequest $request)
    {   
        $this->authorize('store', $this->entity);

        $rule = $this->rule('store', $this->rule, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'data' => $this->item($result,  $this->transformers['basic'])
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(OtherFinancialFlowRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        $rule = $this->rule('update', $this->rule, $this->data($request), $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'data' => $this->item($result,  $this->transformers['basic'])
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', $this->rule, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    /**
     * List of module default references
     * 
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(PaymentMethodRepository::class)->getPaymentMethodWalkIn(), SystemCodeChosenTransformer::class)['data'],
            $this->collection(App::make(BankAccountRepository::class)->all(), BankAccountChosenTransformer::class)['data'],
            $this->permissions(),
            $this->collection(App::make(BranchRepositoryEloquent::class)->all(), BranchChosenTransformer::class)['data'],
        ];
    }

    protected function permissions()
    {
        return [
            'view'              => Auth::user()->can('view', $this->entity),
            'view_detail'       => Auth::user()->can('viewDetail', $this->entity),
            'update'            => Auth::user()->can('update', $this->entity),
            'delete'            => Auth::user()->can('delete', $this->entity),
            'create'            => Auth::user()->can('store', $this->entity)
        ];
    }

    protected function form()
    {
        list($paymentMethods, $bankAccounts, $permissions, $branches) = $this->references();

        $types = $this->type();

        return view($this->blade, compact(
            'types',
            'paymentMethods',
            'bankAccounts',
            'permissions',
            'branches'
        ));
    }

    protected abstract function type();
}