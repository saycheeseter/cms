<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Repositories\Contracts\BankTransactionReportRepository;
use Modules\Core\Repositories\Contracts\BranchDetailRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Contracts\CashTransactionRepository;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Repositories\Contracts\OtherIncomeRepository;
use Modules\Core\Repositories\Contracts\PurchaseInboundRepository;
use Modules\Core\Transformers\SalesOutbound\CollectionDueWarningTransformer;
use Modules\Core\Transformers\Pos\DiscountedSummaryReportTransformer;
use Modules\Core\Transformers\SalesOutbound\SalesmanTransactionsProductsTransformer;
use Modules\Core\Transformers\SalesOutbound\SalesmanTransactionsTransformer;
use Modules\Core\Repositories\Contracts\PosSalesRepository;
use Modules\Core\Repositories\Contracts\ProductCustomerRepository;
use Modules\Core\Repositories\Contracts\ProductMonthlyTransactionRepository;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Repositories\Contracts\ProductSupplierRepository;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\SalesOutboundRepository;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\SupplierBalanceFlowRepository;
use Modules\Core\Repositories\Contracts\CustomerBalanceFlowRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Repositories\Contracts\AuditTrailRepository;
use Modules\Core\Repositories\Contracts\OtherPaymentRepository;
use Modules\Core\Transformers\Customer\BasicTransformer as CustomerTransformer;
use Modules\Core\Transformers\MonthlySalesRanking\SupplierTransformer as SupplierMonthlySalesRankingTransformer;
use Modules\Core\Transformers\Supplier\BasicTransformer as SupplierTransformer;
use Modules\Core\Transformers\ProductSupplier\BasicTransformer as ProductSupplierBasicTransformer;
use Modules\Core\Transformers\ProductCustomer\BasicTransformer as ProductCustomerBasicTransformer;
use Modules\Core\Transformers\BalanceFlow\BasicTransformer as BalanceFlowBasicTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as LocationChosenTransformer;
use Modules\Core\Transformers\User\ChosenTransformer as UserChosenTransformer;
use Modules\Core\Transformers\AuditTrail\BasicTransformer as AuditTrailBasicTransformer;
use Modules\Core\Transformers\BalanceFlow\BeginningBalanceTransformer;
use Modules\Core\Transformers\Product\InventoryValueTransformer as ProductInventoryValueTransformer;
use Modules\Core\Transformers\Product\InventoryValueSummaryTransformer as ProductInventoryValueSummaryTransformer;
use Modules\Core\Transformers\Product\PeriodicSalesTransformer as ProductPeriodicSalesTransformer;
use Modules\Core\Transformers\Product\BranchInventoryTransformer as ProductBranchInventoryTransformer;
use Modules\Core\Transformers\Product\ExportPeriodicSalesTransformer as ExportProductPeriodicSalesTransformer;
use Modules\Core\Transformers\Product\ExportBranchInventoryTransformer as ExportProductBranchInventoryTransformer;
use Modules\Core\Transformers\Product\TransactionListTransformer as ProductTransactionListTransformer;
use Modules\Core\Transformers\Product\ExportTransactionListTransformer as ExportProductTransactionListTransformer;
use Modules\Core\Transformers\ProductInventoryWarning\FullInfoTransformer as ProductInventoryWarningTransformer;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as BranchDetailChosenTransformer;
use Modules\Core\Transformers\MonthlySalesRanking\CategoryTransformer as CategoryMonthlySalesRankingTransformer;
use Modules\Core\Transformers\IncomeStatement\BasicTransformer as IncomeStatementTransformer;
use Modules\Core\Entities\Hydration\IncomeStatement;
use Modules\Core\Transformers\MonthlySalesRanking\ProductTransformer as ProductMonthlySalesBasicTransformer;
use Modules\Core\Transformers\MonthlySalesRanking\BrandTransformer as BrandMonthlySaleBasicTransformer;
use Modules\Core\Services\Contracts\Export\ProductPeriodicSalesExportServiceInterface as ProductPeriodicSalesExport;
use Modules\Core\Services\Contracts\Export\ProductTransactionListExportServiceInterface as ProductTransactionListExport;
use Modules\Core\Transformers\BankAccount\ChosenTransformer as BankAccountChosenTransformer;
use Modules\Core\Transformers\BankTransactionReport\BeginningBalanceTransformer as BankTransactionBeginningBalanceTransformer;
use Modules\Core\Transformers\BankTransactionReport\BasicTransformer as BankTransactionBasicTransformer;
use Modules\Core\Transformers\Pos\TerminalReportTransformer;
use Modules\Core\Transformers\Pos\SeniorTransactionReportTransformer;
use Modules\Core\Transformers\CashTransaction\BeginningBalanceTransformer as CashTransactionBeginningBalanceTransformer;
use Modules\Core\Transformers\CashTransaction\BasicTransformer as CashTransactionBasicTransformer;
use Modules\Core\Services\Contracts\Export\ProductBranchInventoryExportServiceInterface as ProductBranchInventoryExport;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\Category\ChosenTransformer as CategoryChosenTransformer;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SalesOutboundNegativeProfitProduct\NegativeProfitProductsTransformer;
use Modules\Core\Transformers\SalesOutboundNegativeProfitProduct\ExportNegativeProfitProductTransformer;
use Modules\Core\Services\Contracts\Export\NegativeProfitProductExportServiceInterface as NegativeProfitProductExport;
use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Modules\Core\Transformers\MemberRate\ChosenTransformer as MemberRateChosenTransformer;
use Modules\Core\Repositories\Contracts\MemberRepository;
use Modules\Core\Transformers\Member\ReportTransformer as MemberReportTransformer;
use Modules\Core\Transformers\Member\ExportReportTransformer as ExportMemberReportTransformer;
use Modules\Core\Services\Contracts\Export\MemberReportExportServiceInterface as MemberReportExport;
use Modules\Core\Transformers\PurchaseInbound\PaymentDueWarningTransformer;
use Modules\Core\Transformers\PurchaseInbound\PaymentDueWarningSummaryTransformer;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Lang;
use App;
use Auth;
use Modules\Core\Transformers\SalesOutbound\CollectionDueWarningSummaryTransformer;
use Modules\Core\Transformers\SalesOutbound\DetailSalesTransformer;
use Modules\Core\Transformers\SalesOutbound\DetailSalesSummaryTransformer;
use Modules\Core\Transformers\SalesOutbound\DetailSalesExportTransformer;
use Modules\Core\Services\Contracts\Export\DetailSalesReportExportServiceInterface as DetailSalesReportExportService;


class ReportsController extends Controller
{
    /**
     * Displays the Customer Product Price Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function customerProductPrice(Request $request)
    {
        $this->checkPermission('view_customer_product_price');

        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = App::make(ProductCustomerRepository::class)->search($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, ProductCustomerBasicTransformer::class, 'history'),
                $message
            );
        }

        $customers = $this->collection(
            App::make(CustomerRepository::class)->all(),
            CustomerTransformer::class
        )['data'];

        return view('core::reports.customer.product-price', compact('customers'));
    }

    /**
     * Displays the Supplier Product Price Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function supplierProductPrice(Request $request)
    {
        $this->checkPermission('view_supplier_product_price');

        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = App::make(ProductSupplierRepository::class)->search($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, ProductSupplierBasicTransformer::class, 'history'),
                $message
            );
        }

        $suppliers = $this->collection(
            App::make(SupplierRepository::class)->all(),
            SupplierTransformer::class
        )['data'];

        return view('core::reports.supplier.product-price', compact('suppliers'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function brandMonthlySalesRanking(Request $request)
    {
        $this->checkPermission('view_brand_monthly_sales_ranking');

        if ($request->wantsJson()) {
            $data = App::make(BrandRepository::class)->getBrandMonthlySales($request->input('filters'));

            $message = $data->count() === 0 ?
                Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'brands' => $this->collection($data, BrandMonthlySaleBasicTransformer::class)],
                $message
            );
        }

        return view('core::reports.brand.monthly-sales-ranking');
    }

    /**
     * Displays the all or the specified category's monthly sales ranking depending on the
     * filters given
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function categoryMonthlySalesRanking(Request $request)
    {
        $this->checkPermission('view_category_monthly_sales_ranking');

        if ($request->wantsJson()) {
            $data = App::make(CategoryRepository::class)->getMonthlySalesRanking($request->get('filters'));

            $message = $data->count() > 0 ? '' : Lang::get('core::info.no.results.found');

            return $this->successfulResponse([
                'categories' => $this->collection($data, CategoryMonthlySalesRankingTransformer::class)
            ], $message);
        }

        return view('core::reports.category.monthly-sales-ranking');
    }


    /**
     * Displays the Salesman Report view.
     *
     * @param Request $request
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function salesmanReport(Request $request, $type = '')
    {
        $this->checkPermission('view_salesman_report');

        if ($request->wantsJson()) {
            switch ($type) {
                case "transactions":
                    $result = App::make(SalesOutboundRepository::class)->getTransactionsBySalesmanId($request->get('filters'));

                    $message = $result->count() === 0
                        ? Lang::get('core::info.no.results.found')
                        : '';

                    return $this->successfulResponse([
                        'transactions' => $this->collection($result, SalesmanTransactionsTransformer::class)
                    ], $message);
                    break;

                case "products":
                    $result = App::make(SalesOutboundRepository::class)->getTransactionProductsBySalesmanId($request->get('filters'));

                    $message = $result->count() === 0
                        ? Lang::get('core::info.no.results.found')
                        : '';

                    return $this->successfulResponse([
                        'products' => $this->collection($result, SalesmanTransactionsProductsTransformer::class)
                    ], $message);
                    break;
            }
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'];
        $salesmen = $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'];

        return view('core::reports.salesman.report',compact('branches', 'locations', 'salesmen'));
    }

    /**
     * Displays the Negative Profit Product Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function negativeProfitProduct(Request $request)
    {
        $this->checkPermission('view_negative_profit_product');

        if ($request->wantsJson()) {
            $transactions = App::make(SalesOutboundRepository::class)->getNegativeProfitProduct($request->get('filters'));

            $message = $transactions->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'transactions' => $this->collection($transactions, NegativeProfitProductsTransformer::class)
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $brands = $this->collection(App::make(BrandRepository::class)->all(), SystemCodeChosenTransformer::class)['data'];
        $categories = $this->collection(App::make(CategoryRepository::class)->all(), CategoryChosenTransformer::class)['data'];
        $suppliers = $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'];

        return view('core::reports.product.negative-profit', compact('branches', 'brands', 'categories', 'suppliers'));
    }

    public function exportNegativeProfitProduct(Request $request)
    {
        $this->checkPermission('export_negative_profit_product');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $transactions = App::make(SalesOutboundRepository::class)->getNegativeProfitProduct($request->get('filters'), false);

        if($transactions->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($transactions, ExportNegativeProfitProductTransformer::class)['data'];

        App::make(NegativeProfitProductExport::class)->list($data);
    }

    /**
     * Displays the Member Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function memberReport(Request $request)
    {
        $this->checkPermission('view_member_report');

        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = App::make(MemberRepository::class)
                ->with(['rate', 'points'])
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'members' => $this->collection($data, MemberReportTransformer::class)
            ], $message);
        }

        $rates = $this->collection(App::make(MemberRateRepository::class)->all(), MemberRateChosenTransformer::class)['data'];

        return view('core::reports.member.report', compact('rates'));
    }

    public function exportMemberReport(Request $request)
    {
        $this->checkPermission('export_member_report');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $data = App::make(MemberRepository::class)->with(['rate', 'points'])->get();

        if($data->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($data, ExportMemberReportTransformer::class)['data'];

        App::make(MemberReportExport::class)->list($data);
    }

    /**
     * Displays the Product Periodic Sales Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productPeriodicSales(Request $request)
    {
        $this->checkPermission('view_product_periodic_sales');

        if($request->wantsJson()) {
            $products = App::make(ProductRepository::class)->getPeriodicSales($request->get('filters'));

            $message = $products->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($products, ProductPeriodicSalesTransformer::class),
                $message
            );
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];

        return view('core::reports.product.periodic-sales', compact('branches'));
    }

    /**
     * Displays the POS Discounted Summary Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function posDiscountedSummary(Request $request)
    {
        $this->checkPermission('view_pos_discounted_summary');

        if ($request->wantsJson()) {
            $data = App::make(PosSalesRepository::class)->getDiscountedSummaryReport($request->input('filters'));

            $message = $data->count() === 0 ?
                Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse($this->collection($data, DiscountedSummaryReportTransformer::class), $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];

        return view('core::reports.pos.discounted-summary', compact('branches'));
    }

    /**
     * Displays the POS Sales Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function posSales(Request $request)
    {
        $this->checkPermission('view_pos_sales_report');

        if ($request->wantsJson()) {
            $data = App::make(PosSalesRepository::class)->getTerminalReport($request->input('filters'), $request->input('terminals'));

            $message = $data->count() === 0 ?
                Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'summary' => $this->collection($data, TerminalReportTransformer::class),
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];

        return view('core::reports.pos-sales', compact('branches'));
    }

    /**
     * Displays the Product Transaction List Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productTransactionList(Request $request)
    {
        $this->checkPermission('view_product_transaction_list');

        if($request->wantsJson()) {
            $products = App::make(ProductRepository::class)->getTransactionList(
                $request->get('basicFilters'),
                $request->get('advancedFilters')
            );

            $message = $products->count() === 0
                ? Lang::get('core::error.no.records.found')
                : '';

            return $this->successfulResponse([
                'products' => $this->collection($products, ProductTransactionListTransformer::class)
            ], $message);
        }

        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'];

        return view('core::reports.product.transaction-list', compact('locations'));
    }

    public function exportProductTransactionList(Request $request)
    {
        $this->checkPermission('export_product_transaction_list');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $products = App::make(ProductRepository::class)->getTransactionList(
            $request->get('basicFilters'),
            $request->get('advancedFilters'),
            false
        );

        if($products->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($products, ExportProductTransactionListTransformer::class)['data'];

        App::make(ProductTransactionListExport::class)->list($data);
    }

    /**
     * Displays the Product Serial Transaction Report view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function serialTransaction()
    {
        $this->checkPermission('view_product_serial_transaction_report');

        return view('core::reports.product.serial-transaction');
    }

    /**
     * Displays the Senior Transaction Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function seniorTransaction(Request $request)
    {
        $this->checkPermission('view_senior_transaction');

        if($request->wantsJson()) {
            $data = App::make(PosSalesRepository::class)->getSeniorReport($request->get('filters'));

            $message = $data->count() > 0 ? '' : Lang::get('core::info.no.results.found');

            return $this->successfulResponse($this->collection($data, SeniorTransactionReportTransformer::class), $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];

        return view('core::reports.senior.transaction-report', compact('branches'));
    }

    public function bankTransaction(Request $request)
    {
        $this->checkPermission('view_bank_transaction_report');

        if ($request->wantsJson()) {
            $message = Lang::get('core::info.no.results.found');

            $repository = App::make(BankTransactionReportRepository::class);

            $beginning = $repository->getBeginning($request->get('filters'));

            $results = $repository->search($request->get('filters'));

            $message = $results->count() > 0 ? '' : $message;

            return $this->successfulResponse([
                'beginning' => $this->item($beginning, BankTransactionBeginningBalanceTransformer::class),
                'records' => $this->collection($results, BankTransactionBasicTransformer::class)['data']
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];
        $bankAccounts = $this->collection(App::make(BankAccountRepository::class)->all(),BankAccountChosenTransformer::class)['data'];

        return view('core::reports.bank-transaction', compact('branches','bankAccounts'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function cashTransaction(Request $request)
    {
        $this->checkPermission('view_cash_transaction');

        if ($request->wantsJson()) {
            $beginning = null;

            $message = Lang::get('core::info.no.results.found');

            $repository = App::make(CashTransactionRepository::class);

            $beginning = $repository->getBeginning($request->get('filters'));

            $results = $repository->search($request->get('filters'));

            $message = $results->count() > 0 ? '' : $message;

            return $this->successfulResponse([
                'beginning' => $this->item($beginning, CashTransactionBeginningBalanceTransformer::class),
                'records' => $this->collection($results, CashTransactionBasicTransformer::class)['data']
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];

        return view('core::reports.cash-transaction', compact('branches'));
    }

    /**
     * Displays the Product Branch Inventory Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productBranchInventory(Request $request)
    {
        $this->checkPermission('view_product_branch_inventory');

        if($request->wantsJson()) {
            $inventories = App::make(ProductRepository::class)->getBranchInventories($request->get('filters'));

            $message = $inventories->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($inventories, ProductBranchInventoryTransformer::class),
                $message
            );
        }

        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(), LocationChosenTransformer::class)['data'];

        return view('core::reports.product.branch-inventory', compact('locations'));
    }

    public function exportProductBranchInventory(Request $request)
    {
        $this->checkPermission('export_product_branch_inventory');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $inventories = App::make(ProductRepository::class)->getBranchInventories($request->get('filters'), false);

        if($inventories->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($inventories, ExportProductBranchInventoryTransformer::class)['data'];

        App::make(ProductBranchInventoryExport::class)->list($data);
    }

    /**
     * Exports the Product Periodic Sales Report.
     *
     * @param Request $request
     */
    public function exportProductPeriodicSales(Request $request)
    {
        $this->checkPermission('export_product_periodic_sales');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $products = App::make(ProductRepository::class)->getPeriodicSales($request->get('filters'), false);

        if($products->count() === 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $data = $this->collection($products, ExportProductPeriodicSalesTransformer::class)['data'];

        App::make(ProductPeriodicSalesExport::class)->list($data);
    }

    /**
     * Displays the all or the specified supplier's monthly sales ranking depending on the
     * filters given
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function supplierMonthlySalesRanking(Request $request)
    {
        $this->checkPermission('view_supplier_monthly_sales_ranking');

        if ($request->wantsJson()) {
            $data = App::make(SupplierRepository::class)->getMonthlySalesRanking($request->get('filters'));

            $message = $data->count() > 0 ? '' : Lang::get('core::info.no.results.found');

            return $this->successfulResponse([
                'suppliers' => $this->collection($data, SupplierMonthlySalesRankingTransformer::class)
            ], $message);
        }

        return view('core::reports.supplier.monthly-sales-ranking');
    }

    /**
     * Displays the Supplier Balance Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function supplierBalanceFlow(Request $request)
    {
        $this->checkPermission('view_supplier_balance');

        if ($request->wantsJson()) {
            $beginning = null;
            $results = [];
            $message = Lang::get('core::info.no.results.found');

            if ($request->has('filters')) {
                $repository = App::make(SupplierBalanceFlowRepository::class);

                $earliestDate = $this->getEarliestDate($request->get('filters'));

                if(!is_null($earliestDate)) {
                    $beginning = $repository->getBeginning($earliestDate);
                }

                $results = $repository->search();

                $message = $results->count() > 0 ? '' : $message;
            }

            return $this->successfulResponse([
                'beginning' => $this->item($beginning, BeginningBalanceTransformer::class),
                'records' => $this->collection($results, BalanceFlowBasicTransformer::class)['data']
            ], $message);
        }

        $suppliers = $this->collection(App::make(SupplierRepository::class)->all(), SupplierTransformer::class)['data'];

        return view('core::reports.supplier.balance', compact('suppliers'));
    }

    /**
     * Displays the Customer Balance Report view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function customerBalanceFlow(Request $request)
    {
        $this->checkPermission('view_customer_balance');

        if ($request->wantsJson()) {
            $beginning = null;
            $results = [];
            $message = Lang::get('core::info.no.results.found');

            if ($request->has('filters')) {
                $repository = App::make(CustomerBalanceFlowRepository::class);

                $earliestDate = $this->getEarliestDate($request->get('filters'));

                if(!is_null($earliestDate)) {
                    $beginning = $repository->getBeginning($earliestDate);
                }

                $results = $repository->search();

                $message = $results->count() > 0 ? '' : $message;
            }

            return $this->successfulResponse([
                'beginning' => $this->item($beginning, BeginningBalanceTransformer::class),
                'records' => $this->collection($results, BalanceFlowBasicTransformer::class)['data']
            ], $message);
        }

        $customers = $this->collection(App::make(CustomerRepository::class)->all(), CustomerTransformer::class)['data'];

        return view('core::reports.customer.balance', compact('customers'));
    }

    /**
     * Displays the audit trail view
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function auditTrail(Request $request)
    {
        $this->checkPermission('view_audit_trail');

        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = App::make(AuditTrailRepository::class)
                ->with(['audited'])
                ->search($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, AuditTrailBasicTransformer::class, 'audits'),
                $message
            );
        }

        $users = $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'];

        return view('core::reports.audit-trail', compact('users'));
    }

    /**
     * Displays the product inventory value report view
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productInventoryValue(Request $request)
    {
        $this->checkPermission('view_product_inventory_value');

        if ($request->wantsJson()) {
            $data = App::make(ProductRepository::class)->getInventoryValue($request->input('filters'));

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, ProductInventoryValueTransformer::class, 'products'),
                $message
            );
        }

        return view('core::reports.product.inventory-value');
    }

    /**
     * Displays the product inventory summary data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productInventoryValueSummary(Request $request)
    {
        $this->checkPermission('view_product_inventory_value');

        $summaries = App::make(ProductRepository::class)->getInventoryValueSummary($request->input('filters'));

        if (!$summaries) {
            return $this->errorResponse(array(
                'no.records.found' => Lang::get('core::error.no.records.found')
            ));
        }

        return $this->successfulResponse([
            'summaries' => $this->item($summaries, ProductInventoryValueSummaryTransformer::class)
        ]);
    }

    /**
     * Displays the product monthly sales ranking summary data
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productMonthlySalesRanking(Request $request)
    {
        $this->checkPermission('view_product_monthly_sales_ranking');

        if ($request->wantsJson()) {
            $data = App::make(ProductMonthlyTransactionRepository::class)->getProductMonthlySales($request->input('filters'));

            $message = $data->count() === 0 ?
                Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse([
                'products' => $this->collection($data, ProductMonthlySalesBasicTransformer::class),
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];
        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(),LocationChosenTransformer::class)['data'];

        return view('core::reports.product.monthly-sales-ranking', compact('branches', 'locations'));
    }

    /**
     * Displays the product inventory warning report view
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function productInventoryWarning(Request $request)
    {
        $this->checkPermission('view_product_inventory_warning');

        if ($request->wantsJson()) {
            $products = App::make(ProductRepository::class)->getInventoryWarning($request->input('filters'));

            $message = $products->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            if (!$products) {
                return $this->errorResponse(array(
                    'no.products.found' => Lang::get('core::error.no.products.found')
                ));
            }

            return $this->successfulResponse([
                'products' => $this->collection($products, ProductInventoryWarningTransformer::class)
            ], $message);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'];

        return view('core::reports.product.inventory-warning', compact('branches', 'locations'));
    }

    public function incomeStatement(Request $request)
    {
        $this->checkPermission('view_income_statement');

        if ($request->wantsJson()) {
            $otherPayment = App::make(OtherPaymentRepository::class)->getSummary($request->get('filters'));
            $otherIncome = App::make(OtherIncomeRepository::class)->getSummary($request->get('filters'));
            $monthlyTransactionSummary = App::make(ProductMonthlyTransactionRepository::class)->getIncomeStatementSummary($request->get('filters'));

            $model = IncomeStatement::newModelInstance()->newFromBuilder([
                'gross_sales'            => $monthlyTransactionSummary->gross_sales,
                'return_amount'          => $monthlyTransactionSummary->return_amount,
                'cost_of_goods_sold'     => $monthlyTransactionSummary->cost_of_goods_sold,
                'ma_cost_of_goods_sold'  => $monthlyTransactionSummary->ma_cost_of_goods_sold,
                'damage'                 => $monthlyTransactionSummary->damage,
                'adjustment_discrepancy' => $monthlyTransactionSummary->adjustment_discrepancy,
                'other_payment'          => $otherPayment->total_amount,
                'other_income'           => $otherIncome->total_amount
            ]);

            return $this->successfulResponse([
                'summary' => $this->item($model, IncomeStatementTransformer::class)
            ]);
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];

        return view('core::reports.income-statement', compact('branches'));
    }

    public function paymentDueWarning(Request $request)
    {
        $this->checkPermission('view_payment_due_warning');

        if($request->wantsJson()) {
            $result = App::make(PurchaseInboundRepository::class)
                ->getDueWarning($request->get('filters'), $request->get('all'));

            $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($result, PaymentDueWarningTransformer::class),
                $message
            );
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(),BranchChosenTransformer::class)['data'];
        $suppliers = $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'];

        return view("core::reports.payment-due-warning", compact('branches','suppliers'));
    }

    public function paymentDueWarningSummary(Request $request) 
    {
        $result = App::make(PurchaseInboundRepository::class)
            ->getDueWarningSummary($request->get('filters'), $request->get('all'));

        $message = !$result
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse(
            $this->item($result, PaymentDueWarningSummaryTransformer::class),
            $message
        );
    }

    public function collectionDueWarning(Request $request)
    {
        $this->checkPermission('view_collection_due_warning');

        if ($request->wantsJson()) {
            $result = App::make(SalesOutboundRepository::class)
                ->getDueWarning($request->get('filters'), $request->get('all'));

            $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($result, CollectionDueWarningTransformer::class),
                $message
            );
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $customers = $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'];

        return view("core::reports.collection-due-warning", compact('branches', 'customers'));
    }

    public function collectionDueWarningSummary(Request $request) 
    {
        $result = App::make(SalesOutboundRepository::class)
            ->getDueWarningSummary($request->get('filters'), $request->get('all'));

        $message = !$result
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse(
            $this->item($result, CollectionDueWarningSummaryTransformer::class),
            $message
        );
    }

    public function detailSalesReport(Request $request)
    {
        $this->checkPermission('view_detail_sales_report');

        if($request->wantsJson()) {
            $result = App::make(SalesOutboundRepository::class)->detailSalesReport($request->get('filters'));

            $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($result, DetailSalesTransformer::class),
                $message
            );
        }

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];
        $locations = $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'];
        $customers = $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'];
        $users = $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'];
        $brands = $this->collection(App::make(BrandRepository::class)->all(), SystemCodeChosenTransformer::class)['data'];
        $categories = $this->collection(App::make(CategoryRepository::class)->all(), CategoryChosenTransformer::class)['data'];
        $suppliers = $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'];
        $exportPermission = Auth::user()->hasAccessTo('export_detail_sales_report');

        return view('core::reports.sales.detail-report', compact(
            'branches',
            'locations',
            'customers',
            'users',
            'brands',
            'categories',
            'suppliers',
            'exportPermission'
        ));
    }

    public function detailSalesReportSummary(Request $request)
    {
        $this->checkPermission('view_detail_sales_report');

        $result = App::make(SalesOutboundRepository::class)->detailSalesReportSummary($request->get('filters'));

        $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

        return $this->successfulResponse(
            $this->item($result[0], DetailSalesSummaryTransformer::class),
            $message
        );
    }

    public function exportDetailSalesReport(Request $request)
    {
        $this->checkPermission('export_detail_sales_report');

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $results = App::make(SalesOutboundRepository::class)->detailSalesReport($request->get('filters'), false);

        if($results->count() == 0) {
            echo Lang::get('core::info.no.results.found');
            exit;
        }

        $results = $this->collection($results, DetailSalesExportTransformer::class)['data'];
        
        App::make(DetailSalesReportExportService::class)->settings($request->get('columns'))->list($results);
    }

    /**
     * Get the lowest possible date in the set of filter
     *
     * @param  array $filters
     * @return string
     */
    private function getEarliestDate($filters)
    {
        return collect($filters)
            ->filter(function ($value, $key) {
                return $value['column'] == 'transaction_date';
            })
            ->pluck('value')
            ->sort()
            ->first();
    }
}
