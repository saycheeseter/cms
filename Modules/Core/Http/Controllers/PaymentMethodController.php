<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Services\Contracts\PaymentMethodServiceInterface;
use Modules\Core\Entities\PaymentMethod;
use Lang;

class PaymentMethodController extends SystemCodeController
{
    public function __construct(PaymentMethodRepository $repository, PaymentMethodServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->title = Lang::get('core::label.payment.method');
        $this->entity = PaymentMethod::class;
    }
}
