<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\PurchaseInboundRepository;
use Modules\Core\Services\Contracts\PurchaseInboundServiceInterface;
use Modules\Core\Transformers\PurchaseInbound\BasicTransformer;
use Modules\Core\Transformers\PurchaseInbound\FullInfoTransformer;
use Modules\Core\Transformers\PurchaseInbound\ExportListTransformer;
use Modules\Core\Transformers\PurchaseInbound\ImportTransformer;
use Modules\Core\Transformers\PurchaseInboundDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\PurchaseInboundDetail\ImportTransformer as DetailImportTransformer;
use Modules\Core\Transformers\PurchaseInbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\PurchaseInbound\PaymentTransformer;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Http\Requests\PurchaseInboundRequest;
use Modules\Core\Http\Requests\PurchaseInboundApprovedRequest;
use Modules\Core\Rules\Contracts\PurchaseInboundRuleInterface as PurchaseInboundRule;
use Modules\Core\Services\Contracts\Export\PurchaseInboundExportServiceInterface as PurchaseInboundExport;
use Modules\Core\Transformers\Supplier\ChosenTransformer as SupplierChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\PurchaseInbound;
use Lang;
use App;

class PurchaseInboundController extends InventoryChainController
{
    public function __construct(PurchaseInboundRepository $repository, PurchaseInboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = PurchaseInboundExport::class;
        $this->rule = PurchaseInboundRule::class;
        $this->request['transaction'] = PurchaseInboundRequest::class;
        $this->request['info'] = PurchaseInboundApprovedRequest::class;
        $this->entity = PurchaseInbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['detail'] = DetailFullInfoTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::PURCHASE_INBOUND;
        $this->eagerLoad = [
            'supplier',
            'payment',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * Retrieve list of transaction with remaining amount greater than zero
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function transactionsWithBalance(Request $request)
    {
        $result = $this->repository->transactionsWithBalance(
            $request->get('branch_id'), 
            $request->get('supplier_id'),
            $request->get('per_page', 10)
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, BasicTransformer::class)
        ]);
    }

    /**
     * Get the payable details of the indicated transactions
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPayableTransactions(Request $request)
    {
        $result = $this->repository->findMany(
            $request->get('ids')
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, PaymentTransformer::class)
        ]);
    }

    /**
     * Import/Retrieve transaction details by reference number
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findApprovedTransactionByReference($reference)
    {
        $transaction = $this->repository->findApprovedTransactionByReference($reference);

        if(!$transaction) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        $details = $this->paginate($transaction->details, 10);

        return $this->successfulResponse([
            'transaction' => $this->item($transaction, ImportTransformer::class),
            'details' => $this->collection($details, DetailImportTransformer::class)
        ], Lang::get('core::success.transaction.successfully.imported'));
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(SupplierRepository::class)->all(), SupplierChosenTransformer::class)['data'],
            $this->collection(App::make(PaymentMethodRepository::class)->all(), SystemCodeChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    /**
     * Display the form with its corresponding references
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function form()
    {   
        list($tsuppliers, $paymentMethods, $udfs, $permissions, $templates) = $this->references();

        return view('core::purchase-inbound.detail',
            compact('tsuppliers', 'paymentMethods', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::purchase-inbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
