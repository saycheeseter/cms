<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\SalesReturnRepository;
use Modules\Core\Services\Contracts\SalesReturnServiceInterface;
use Modules\Core\Transformers\SalesReturn\BasicTransformer;
use Modules\Core\Transformers\SalesReturn\FullInfoTransformer;
use Modules\Core\Transformers\SalesReturn\ExportListTransformer;
use Modules\Core\Transformers\SalesReturn\RetrieveTransformer;
use Modules\Core\Transformers\SalesReturn\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\SalesReturnRequest;
use Modules\Core\Http\Requests\SalesReturnApprovedRequest;
use Modules\Core\Rules\Contracts\SalesReturnRuleInterface as SalesReturnRule;
use Modules\Core\Services\Contracts\Export\SalesReturnExportServiceInterface as SalesReturnExport;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\SalesReturn;
use Lang;
use App;

class SalesReturnController extends InvoiceController
{
    public function __construct(SalesReturnRepository $repository, SalesReturnServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = SalesReturnExport::class;
        $this->rule = SalesReturnRule::class;
        $this->request['transaction'] = SalesReturnRequest::class;
        $this->request['info'] = SalesReturnApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->entity = SalesReturn::class;
        $this->module = ModuleDefinedFields::SALES_RETURN;
        $this->eagerLoad = [
            'customer',
            'customerDetail',
            'for',
            'location',
            'creator',
            'auditor',
            'from',
            'salesman',
            'requested'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($customers, $udfs, $permissions, $templates) = $this->references();

        return view('core::sales-return.detail', 
            compact('customers', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::sales-return.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
