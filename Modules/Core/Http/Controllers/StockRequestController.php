<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Repositories\Contracts\StockRequestRepository;
use Modules\Core\Services\Contracts\StockRequestServiceInterface;
use Modules\Core\Transformers\StockRequest\BasicTransformer;
use Modules\Core\Transformers\StockRequest\FullInfoTransformer;
use Modules\Core\Transformers\StockRequest\ExportListTransformer;
use Modules\Core\Transformers\StockRequest\RetrieveTransformer;
use Modules\Core\Transformers\StockRequest\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\StockRequestDetail\RetrieveTransformer as RetrieveDetailTransformer;
use Modules\Core\Transformers\StockRequestDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Http\Requests\StockRequestRequest;
use Modules\Core\Http\Requests\StockRequestApprovedRequest;
use Modules\Core\Rules\Contracts\StockRequestRuleInterface as StockRequestRule;
use Modules\Core\Services\Contracts\Export\StockRequestExportServiceInterface as StockRequestExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as BranchDetailChosenTransformer;
use Modules\Core\Entities\StockRequest;
use Lang;
use App;
use Auth;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;

class StockRequestController extends InvoiceController
{   
    private $page = 'to';

    public function __construct(StockRequestRepository $repository, StockRequestServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockRequestExport::class;
        $this->rule = StockRequestRule::class;
        $this->request['transaction'] = StockRequestRequest::class;
        $this->request['info'] = StockRequestApprovedRequest::class;
        $this->transformers['retrieve']['info'] = RetrieveTransformer::class;
        $this->transformers['retrieve']['detail'] = RetrieveDetailTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['detail'] = DetailFullInfoTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->entity = StockRequest::class;
        $this->module = ModuleDefinedFields::STOCK_REQUEST;
        $this->eagerLoad = [
            'for',
            'from',
            'location',
            'to',
            'creator',
            'auditor',
            'requested',
        ];
    }

    /**
     * Display a listing of the resource for stock request to module
     * @return Response
     */
    public function to(Request $request)
    {   
        $this->authorize('view', $this->entity);

        return $this->list('stock-request-to.list');
    }

    /**
     * Display a listing of the resource for stock request to module
     * @return Response
     */
    public function from(Request $request)
    {   
        $this->authorize('viewFrom', $this->entity);

        $this->page = 'from';
        
        return $this->list('stock-request-from.list');
    }

    /**
     * Show viewing form and return list of references
     *
     * @return Response
     */
    public function view($id)
    {   
        $this->authorize('viewDetailFrom', $this->entity);

        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        list($udfs, $permissions, $templates) = $this->references();

        return view(sprintf('core::%s', 'stock-request-from.detail'),
            compact('udfs', 'permissions', 'templates')
        );
    }

    /**
     * Check if each encoded product is already requested, or already had a delivery
     * 
     * @param  StockRequestRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function warning(StockRequestRequest $request)
    {
        if(setting('request.warning.already.requested.not.delivered.toggle')
            || setting('request.warning.already.requested.delivered.toggle')
        ) {
            $rule = $this->rule('warning', $this->rule, $this->data($request));

            if ($rule->fails()) {
                return $this->errorResponse($rule->errors());
            }
        }

        return $this->successfulResponse();
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function list($blade)
    {
        return view(sprintf('core::%s', $blade), array(
            'udfs' => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates' => App::make(ExcelTemplateRepository::class)->findByModule($this->module),
        ));
    }

    protected function form()
    {
        list($udfs, $permissions, $templates) = $this->references();

        return view(sprintf('core::%s', 'stock-request-to.detail'),
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function permissions()
    {
        return array_merge(parent::permissions(), [
            'from_view'         => Auth::user()->can('viewFrom', $this->entity),
            'from_view_detail'  => Auth::user()->can('viewDetailFrom', $this->entity),
            'export'            => ($this->page == 'from')
                ? Auth::user()->can('exportFrom', $this->entity)
                : Auth::user()->can('export', $this->entity),
            'print'             => ($this->page == 'from')
                ? Auth::user()->can('printFrom', $this->entity)
                : Auth::user()->can('print', $this->entity),
            'column_settings'   => ($this->page == 'from')
                ? Auth::user()->can('useColumnSettingsFrom', $this->entity)
                : Auth::user()->can('useColumnSettings', $this->entity),
            'show_price_amount' => ($this->page == 'from')
                ? Auth::user()->can('showPriceAmountFrom', $this->entity)
                : Auth::user()->can('showPriceAmount', $this->entity)
        ]);
    }
}
