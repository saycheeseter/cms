<?php

namespace Modules\Core\Http\Controllers;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('core::home.dashboard');
    }
}
