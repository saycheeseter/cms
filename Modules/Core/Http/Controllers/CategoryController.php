<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Services\Contracts\CategoryServiceInterface;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Core\Transformers\Category\BasicTransformer;
use Modules\Core\Transformers\Category\TreeTransformer;
use Modules\Core\Repositories\Criteria\ParentOnlyCriteria;
use Modules\Core\Rules\Contracts\CategoryRuleInterface as CategoryRule;
use Modules\Core\Entities\Category;
use Fractal;
use Lang;
use App;
use Exception;
use Auth;

class CategoryController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(CategoryRepository $repository, CategoryServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Category::class;

        // Traversing from root parent is only needed for this module
        $this->repository->pushCriteria(app(ParentOnlyCriteria::class));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            if ($request->has('parent_id')) {
                $this->repository->popCriteria(ParentOnlyCriteria::class);
            }

            $data = $this->repository->all();

            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, TreeTransformer::class, 'categories'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::category.list', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'category' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'category' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', CategoryRule::class, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated categories and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $categories = $this->repository->findMany($request->get('ids'));

        if ($categories->count() === 0) {
            return $this->errorResponse(array(
                'no.categories.found' => Lang::get('core::error.no.categories.found')
            ));
        }

        return $this->successfulResponse([
            'categories' => $this->collection($categories, BasicTransformer::class)['data']
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'parent_id',
            'code',
            'name'
        );
    }

    protected function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
        ];
    }
}
