<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\UserColumnSettingsRepository;
use Modules\Core\Services\Contracts\UserColumnSettingsServiceInterface as UserColumnSettingsService;
use Lang;

class UserColumnSettingsController extends Controller
{
    protected $service;
    protected $repository;

    public function __construct(UserColumnSettingsRepository $repository, UserColumnSettingsService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function getByModule(Request $request, $module)
    {
        $settings = $this->repository->getByModule($module);

        return $this->successfulResponse([
            'settings' => $settings
        ]);
    }

    public function save(Request $request, $module)
    {
        try {
            $this->service->save($module, $request->get('settings'));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.saved'));
    }
}