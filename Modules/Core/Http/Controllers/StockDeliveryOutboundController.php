<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository;
use Modules\Core\Repositories\Contracts\StockDeliveryOutboundDetailRepository;
use Modules\Core\Services\Contracts\StockDeliveryOutboundServiceInterface;
use Modules\Core\Transformers\StockDeliveryOutbound\BasicTransformer;
use Modules\Core\Transformers\StockDeliveryOutbound\FullInfoTransformer;
use Modules\Core\Transformers\StockDeliveryOutbound\ExportListTransformer;
use Modules\Core\Transformers\StockDeliveryOutbound\RetrieveTransformer;
use Modules\Core\Transformers\StockDeliveryOutbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Transformers\StockDeliveryOutboundDetail\ProductBatchTransformer as BatchBasicTransformer;
use Modules\Core\Transformers\InventoryChainDetail\RetrieveTransformer as RetrieveDetailTransformer;
use Modules\Core\Transformers\InventoryChain\ChosenTransformer as StockDeliveryOutboundChosenTransformer;
use Modules\Core\Http\Requests\StockDeliveryOutboundRequest;
use Modules\Core\Http\Requests\StockDeliveryOutboundApprovedRequest;
use Modules\Core\Rules\Contracts\StockDeliveryOutboundRuleInterface as StockDeliveryOutboundRule;
use Modules\Core\Services\Contracts\Export\StockDeliveryOutboundExportServiceInterface as StockDeliveryOutboundExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialBasicTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\StockDeliveryOutbound;
use Lang;
use App;
use Auth;

class StockDeliveryOutboundController extends InventoryChainController
{   
    private $page = 'to';

    public function __construct(StockDeliveryOutboundRepository $repository, StockDeliveryOutboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockDeliveryOutboundExport::class;
        $this->rule = StockDeliveryOutboundRule::class;
        $this->request['transaction'] = StockDeliveryOutboundRequest::class;
        $this->request['info'] = StockDeliveryOutboundApprovedRequest::class;
        $this->entity = StockDeliveryOutbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::STOCK_DELIVERY_OUTBOUND;
        $this->eagerLoad = [
            'deliverTo',
            'deliverToLocation',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * Display a listing of the resource for stock request to module
     *
     * @return Response
     */
    public function to()
    {   
        $this->authorize('view', $this->entity);

        return $this->list('stock-delivery-outbound-to.list');
    }

    /**
     * Display a listing of the resource for stock request to module
     *
     * @return Response
     */
    public function from()
    {   
        $this->authorize('viewFrom', $this->entity);

        $this->page = 'from';

        return $this->list('stock-delivery-outbound-from.list');
    }

    /**
     * Show viewing form and return list of references
     *
     * @return Response
     */
    public function view($id)
    {   
        $this->authorize('viewDetailFrom', $this->entity);

        $model = $this->repository
            ->withTrashed()
            ->find($id);

        if (!$model) {
            throw new ModelNotFoundException;
        }

        list($udfs, $permissions, $templates) = $this->references();

        return view(sprintf('core::%s', 'stock-delivery-outbound-from.detail'),
            compact('udfs', 'permissions', 'templates')
        );
    }

    /**
     * Import/Retrieve transaction details by reference number
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remaining(Request $request)
    {
        $sheetNumber = $request->get('sheet_number');
        $branch = $request->get('branch');

        $returns  = $this->repository->findRemaining($branch, $sheetNumber);

        if($returns->count() === 0) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        return $this->successfulResponse([
            'stock_delivery_outbounds' => $this->collection($returns, StockDeliveryOutboundChosenTransformer::class)['data']
        ]);
    }

    /**
     * Retrieve return details
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieve(Request $request)
    {
        ini_set('max_execution_time', 240);

        $transaction = $this->repository->retrieve($request->get('id'));

        if(!$transaction) {
            return $this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ));
        }

        if($transaction->details->count() === 0) {
            return $this->errorResponse(array(
                'no.remaining.products.found' => Lang::get('core::error.no.remaining.products.found')
            ));
        }

        $details = $this->paginate($transaction->details, 10);

        return $this->successfulResponse([
            'stock_delivery_outbound' => $this->item($transaction, RetrieveTransformer::class),
            'details' => $this->collection($details, RetrieveDetailTransformer::class),
        ], Lang::get('core::success.transaction.successfully.imported'));
    }

    /**
     * Check if each product encoded in the transaction reaches the minimum set in the product info
     * 
     * @param  StockDeliveryOutboundRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function warning(StockDeliveryOutboundRequest $request) 
    {
        if(setting('delivery.outbound.warn.if.inventory.reaches.minimum')) {
            $rule = $this->rule('warning', $this->rule, $this->data($request));

            if ($rule->fails()) {
                return $this->errorResponse($rule->errors());
            }
        }

        return $this->successfulResponse();
    }
    
    /**
     * Get the list of serials of the indicated product
     * 
     * @param  StockDeliveryOutboundDetailRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function serials(StockDeliveryOutboundDetailRepository $repository, $id)
    {
        $serials = $repository->serials($id);

        if ($serials->count() === 0) {
            return $this->errorResponse(array(
                'no.serials.found' => Lang::get('core::error.no.serials.found')
            ));
        }

        return $this->successfulResponse([
            'serials' => $this->collection($serials, SerialBasicTransformer::class)['data']
        ]);
    }

    /**
     * Get the list of batches of the indicated product
     *
     * @param  StockDeliveryOutboundDetailRepository $repository
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function batches(StockDeliveryOutboundDetailRepository $repository, $id)
    {
        $batches = $repository->batches($id);

        if ($batches->count() === 0) {
            return $this->errorResponse(array(
                'no.batches.found' => Lang::get('core::error.no.batches.found')
            ));
        }

        return $this->successfulResponse([
            'batches' => $this->collection($batches, BatchBasicTransformer::class)['data']
        ]);
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view(sprintf('core::%s', 'stock-delivery-outbound-to.detail'),
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function permissions()
    {
        return array_merge(parent::permissions(), [
            'from_view'         => Auth::user()->can('viewFrom', $this->entity),
            'from_view_detail'  => Auth::user()->can('viewDetailFrom', $this->entity),
            'export'            => ($this->page == 'from')
                ? Auth::user()->can('exportFrom', $this->entity)
                : Auth::user()->can('export', $this->entity),
            'print'             => ($this->page == 'from')
                ? Auth::user()->can('printFrom', $this->entity)
                : Auth::user()->can('print', $this->entity),
            'column_settings'   => ($this->page == 'from')
                ? Auth::user()->can('useColumnSettingsFrom', $this->entity)
                : Auth::user()->can('useColumnSettings', $this->entity),
            'show_price_amount' => ($this->page == 'from')
                ? Auth::user()->can('showPriceAmountFrom', $this->entity)
                : Auth::user()->can('showPriceAmount', $this->entity)
        ]);
    }

    protected function list($blade)
    {
        return view(sprintf('core::%s', $blade), array(
            'udfs' => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates' => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
