<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Http\Requests\TransactionSummaryPostingRequest;
use Modules\Core\Repositories\Contracts\TransactionSummaryPostingRepository;
use Modules\Core\Rules\Contracts\TransactionSummaryPostingRuleInterface;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;
use Modules\Core\Transformers\TransactionSummaryPosting\FullTransformer;
use Lang;
use Exception;
use Log;

class TransactionSummaryPostingController extends Controller
{
    public function index(Request $request, TransactionSummaryPostingRepository $repository)
    {
        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
            if ($request->wantsJson()) {
                return $this->errorResponse();
            } else {
                return redirect('/dashboard');
            }
        }

        if ($request->wantsJson()) {
            $result = $repository->with(['transaction'])
                ->orderBy('transaction_date', 'ASC')
                ->orderBy('id', 'ASC')
                ->paginate($request->get('per_page'));

            $message = $result->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse($this->collection($result, FullTransformer::class), $message);
        }

        return view('core::transaction-summary-posting.list');
    }

    public function generateSummary(TransactionSummaryPostingRequest $request, TransactionSummaryPostingServiceInterface $service)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $date = $request->get('date');

        $rule = $rule = $this->rule('generateSummary', TransactionSummaryPostingRuleInterface::class, $date);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $service->process($date);

            return $this->successfulResponse([], Lang::get('core::success.summary.generated'));
        } catch (Exception $e) {
            Log::error($e->getMessage());

            return $this->errorResponse([
                'generate.summary.failed' => Lang::get('core::error.unable.to.generate.summary')
            ]);
        }
    }
}