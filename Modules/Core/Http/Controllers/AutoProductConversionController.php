<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\AutoProductConversionRepository;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Enums\GroupType;
use Modules\Core\Transformers\AutoProductConversion\TransactionTransformer;
use Modules\Core\Transformers\AutoProductConversionDetail\TransactionTransformer as TransactionDetailTransformer;
use Lang;

class  AutoProductConversionController extends Controller
{
    public function __construct(AutoProductConversionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function productTransactions(Request $request, ProductRepository $repository)
    {
        $product = $repository->find($request->get('product_id'));

        if(in_array($product->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP])) {
            $transactions = $this->repository->findProductInInfoTransactions(
                $request->get('product_id'), 
                $request->get('type'), 
                $request->get('per_page', 10)
            );
            $transformer = TransactionTransformer::class;
        } else {
            $transactions = $this->repository->findProductInDetailTransactions(
                $request->get('product_id'), 
                $request->get('type'), 
                $request->get('per_page', 10)
            );
            $transformer = TransactionDetailTransformer::class;
        }

        if($transactions->isEmpty()) {
            return $this->errorResponse(array(
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ));
        }

        return $this->successfulResponse($this->collection($transactions, $transformer));
    }
}