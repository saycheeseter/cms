<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Support\Facades\App;
use Modules\Core\Http\Requests\BankTransactionRequest;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Repositories\Eloquent\BranchRepositoryEloquent;
use Modules\Core\Services\Contracts\BankTransactionServiceInterface;
use Modules\Core\Repositories\Contracts\BankTransactionRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\BankAccount\ChosenTransformer as BankAccountChosenTransformer;
use Modules\Core\Transformers\BankTransaction\BasicTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Entities\BankTransaction;

class BankTransactionController extends Controller
{
    protected $service;
    protected $repository;
    protected $entity = BankTransaction::class;

    public function __construct(BankTransactionServiceInterface $service, BankTransactionRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository->paginate($perPage);

            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'bank_transactions'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        list($branches, $bankAccounts, $permissions) = $this->references();

        return view('core::bank.transaction', compact(
            'branches',
            'bankAccounts',
            'permissions'
        ));
    }

    public function store(BankTransactionRequest $request)
    {
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'bank_transaction' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    public function update(BankTransactionRequest $request, $id)
    {
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'bank_transaction' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    protected function references()
    {
        return [
            $this->collection(App::make(BranchRepositoryEloquent::class)->all(), BranchChosenTransformer::class)['data'],
            $this->collection(App::make(BankAccountRepository::class)->all(), BankAccountChosenTransformer::class)['data'],
            $this->permissions()
        ];
    }

    private function data(BankTransactionRequest $request)
    {
        return $request->only(
            'id',
            'transaction_date',
            'type',
            'amount',
            'from_bank',
            'to_bank',
            'remarks'
        );
    }

    private function permissions()
    {
        return [
            'view' => Auth::user()->can('view', $this->entity),
            'create' => Auth::user()->can('store', $this->entity),
            'update' => Auth::user()->can('update', $this->entity),
            'delete' => Auth::user()->can('delete', $this->entity)
        ];
    }
}
