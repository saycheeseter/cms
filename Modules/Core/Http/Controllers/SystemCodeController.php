<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Requests\ImportExcelRequest;
use Modules\Core\Http\Requests\SystemCodeRequest;
use Modules\Core\Transformers\SystemCode\BasicTransformer;
use Fractal;
use Lang;
use Exception;
use PDOException;
use Auth;

abstract class SystemCodeController extends Controller
{
    protected $service;
    protected $repository;
    protected $title;
    protected $entity;
    protected $import;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $title = $this->title;

        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);
            $data = $this->repository->paginate($perPage);
            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'system_codes'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();
        
        return view('core::system-code.list', compact('title', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  SystemCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SystemCodeRequest $request)
    {
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'save.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'system_code' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SystemCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SystemCodeRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'system_code' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch(PDOException $e) {
            return $this->errorResponse(array(
                'delete.failed' =>  Lang::get('core::error.used.data')
            ));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated system code and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $systemCodes = $this->repository->findMany($request->get('ids'));

        if ($systemCodes->count() === 0) {
            return $this->errorResponse(array(
                'no.system.code.found' => Lang::get('core::error.no.system.code.found')
            ));
        }

        return $this->successfulResponse([
            'system_codes' => $this->collection($systemCodes, BasicTransformer::class)['data']
        ]);
    }

    public function importExcel(ImportExcelRequest $request)
    {
        $this->authorize('importExcel', $this->entity);

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        if (!$request->hasFile('attachment')) {
            return $this->errorResponse(array(
                'no.files.found' => Lang::get('core::error.files.not.found')
            ));
        }

        try {
            $logs = $this->import
                ->setFile($request->file('attachment'))
                ->execute()
                ->getLogs();
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'unable.to.import.excel' => $e->getMessage()
            ));
        }

        return $this->successfulResponse([
            'logs' => $logs
        ]);
    }

    public function downloadImportExcelTemplate()
    {
        $this->authorize('importExcel', $this->entity);

        $this->import->exportDefaultTemplate();
    }

    private function data(Request $request)
    {
        return $request->only('code', 'name', 'remarks');
    }

    protected function permissions()
    {
        return [
            'update'       => Auth::user()->can('update', $this->entity),
            'delete'       => Auth::user()->can('delete', $this->entity),
            'create'       => Auth::user()->can('store', $this->entity),
            'import_excel' => Auth::user()->can('importExcel', $this->entity),
        ];
    }
}
