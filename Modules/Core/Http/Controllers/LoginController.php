<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Modules\Core\Events\UserLoggedIn;
use Modules\Core\Http\Requests\LoginRequest;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Transformers\Branch\ChosenTransformer;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface as LicenseApiService;
use App;

class LoginController extends Controller
{
    use ThrottlesLogins;

    private $userRepository;
    private $branchRepository;

    public function __construct(BranchRepository $branchRepository, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->branchRepository = $branchRepository;
    }

    /**
     * Display login page
     * 
     * @return Response
     */
    public function index()
    {
        return view('core::auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->lockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if(!$this->guard()->user()->isSuperadmin()) {
                if (!$this->guard()->user()->isWithinLoginSchedule()) {
                    return $this->noLoginScheduleResponse($request);
                }

                if (!(config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false)) {
                    if(empty($this->guard()->user()->license_key)) {
                        return $this->noLicenceKeyResponse($request);
                    }

                    $verification = $this->verifyLicenseKey();

                    if (!$verification['isSuccessful']) {
                        return $this->invalidLicenseResponse($request, $verification['errors']);
                    }
                }

                if (!$this->guard()->user()->hasAssignedBranch()) {
                    return $this->noBranchAssignedResponse($request);
                }
            }

            return $this->loginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->noEntryFoundResponse([
            'auth' => Lang::get('core::validation.auth.failed')
        ]);
    }

    /**
     * Store selected branch to session
     * 
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticated(Request $request)
    {
        $branchId = $request->get('id');

        if ($branchId == 0) {
            return $this->errorResponse([
                'auth' => Lang::get('core::validation.no.branch.selected') 
            ]);
        }

        $branch = $this->branchRepository->find($branchId);

        if (!$branch) {
            return $this->errorResponse([
                'auth' => Lang::get('core::validation.branch.not.found') 
            ]);
        }

        $this->guard()->setConfig($branch);

        event(new UserLoggedIn($this->guard()->user()));

        return $this->successfulResponse([], Lang::get('core::success.connected'));
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $hasRevoked = $request->has('revoked') && $request->get('revoked');

        $audit = $hasRevoked
            ? $this->guard()->user()->last_login_audit
            : null;
        
        $this->guard()->logout();

        $request->session()->invalidate();

        if ($hasRevoked) {
            $request->session()->regenerate();
            $request->session()->flash('revoked', [
                'ip_address' => $audit->ip_address ?? '',
                'user_agent' => $audit->user_agent ?? ''
            ]);
        }

        return redirect('/');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    protected function username()
    {
        return 'username';
    }

    /**
     * Attempt to login in the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    private function attemptLogin(Request $request)
    {
        return $this->guard()->attempt($request->only('username', 'password'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    private function guard()
    {
        return Auth::guard();
    }

    /**
     * Redirect the user after determining they are locked out.
     * Overrides default lockout response to return json response
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function lockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return $this->errorResponse([
            'throttle' => Lang::get('core::validation.auth.throttle', [
                'seconds' => $seconds
            ])
        ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function loginResponse(Request $request)
    {
        $branches = $this->guard()->user()->assignedBranches();

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->successfulResponse($this->collection($branches, ChosenTransformer::class, 'branches'));
    }

    private function verifyLicenseKey()
    {
        $user = $this->guard()->user();

        return App::make(LicenseApiService::class)->verify($user->license_key);
    }
    
    /**
     * Send no branch found response if user doesn't have a branch assigned.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function noBranchAssignedResponse(Request $request)
    {
        $this->logoutAndIncrementAttempts($request);

        return $this->noEntryFoundResponse([
            'no.branch.found' => Lang::get('core::validation.no.branch.found')
        ]);
    }

    private function noLoginScheduleResponse(Request $request)
    {
        $this->logoutAndIncrementAttempts($request);

        return $this->errorResponse([
            'invalid.login.schedule' => Lang::get('core::validation.invalid.login.schedule')
        ]);
    }

    private function invalidLicenseResponse(Request $request, $message = [])
    {
        $this->logoutAndIncrementAttempts($request);

        return $this->errorResponse($message);
    }

    private function noLicenceKeyResponse(Request $request)
    {
        $this->logoutAndIncrementAttempts($request);

        return $this->errorResponse([
            'no.license.key' => Lang::get('core::error.no.license.key')
        ]);
    }

    private function logoutAndIncrementAttempts(Request $request)
    {
        $this->guard()->logout();

        $this->incrementLoginAttempts($request);

        return $this;
    }
}
