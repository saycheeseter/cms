<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\OtherIncomeRepository;
use Modules\Core\Repositories\Contracts\IncomeTypeRepository;
use Modules\Core\Rules\Contracts\OtherIncomeRuleInterface;
use Modules\Core\Services\Contracts\OtherIncomeServiceInterface;
use Modules\Core\Transformers\OtherIncome\FullInfoTransformer;
use Modules\Core\Transformers\OtherIncome\BasicTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as SystemCodeChosenTransformer;
use Modules\Core\Transformers\OtherIncomeDetail\BasicTransformer as OtherIncomeDetailBasicTransformer;
use Modules\Core\Entities\OtherIncome;
use App;
use Auth;
use Lang;

class OtherIncomeController extends OtherFinancialFlowController
{
    protected $transformers = [
        'full' => FullInfoTransformer::class,
        'basic' => BasicTransformer::class,
        'detail' => OtherIncomeDetailBasicTransformer::class
    ];

    protected $blade = 'core::other-income.list';

    public function __construct(OtherIncomeRepository $repository, OtherIncomeServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = OtherIncome::class;
        $this->rule = OtherIncomeRuleInterface::class;
    }

    /**
     * list of income types
     * 
     * @return array
     */
    protected function type()
    {
        return $this->collection(App::make(IncomeTypeRepository::class)->all(), SystemCodeChosenTransformer::class)['data'];
    }
}
