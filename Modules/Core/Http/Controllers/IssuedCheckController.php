<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Services\Contracts\IssuedCheckServiceInterface as IssuedCheckService;
use Modules\Core\Repositories\Contracts\IssuedCheckRepository;
use Modules\Core\Entities\IssuedCheck;

class IssuedCheckController extends CheckManagementController
{
    protected $form = 'core::issued-check.list';

    public function __construct(IssuedCheckRepository $repository, IssuedCheckService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = IssuedCheck::class;
    }
}
