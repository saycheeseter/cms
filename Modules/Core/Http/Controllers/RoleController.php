<?php

namespace Modules\Core\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\RoleRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Services\Contracts\RoleServiceInterface;
use Modules\Core\Rules\Contracts\RoleRuleInterface as RoleRule;
use Modules\Core\Transformers\Role\BasicTransformer;
use Modules\Core\Transformers\Role\FullInfoTransformer;
use Modules\Core\Http\Requests\RoleRequest;
use Modules\Core\Repositories\Contracts\PermissionSectionRepository;
use Modules\Core\Transformers\PermissionSection\SectionTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Entities\Role;
use Lang;
use Auth;
use App;

class RoleController extends Controller
{
    private $repository;
    private $service;
    private $entity = Role::class;

    public function __construct(RoleRepository $repository, RoleServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'roles'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::role.list', compact('permissions'));
    }

    /**
     * Displays the form for creation
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Shows the form for updating
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Retrieve the details of the specified resource
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {
            $record = $this->repository->find($id);

            if(!$record) {
                return $this->errorResponse(array(
                    'show.failed' => Lang::get('core::error.show.failed')
                ));
            }

            return $this->successfulResponse([
                'role' => $this->item($record, FullInfoTransformer::class)
            ]);
        }
    }

    /**
     * Create a new resource by the given input
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleRequest $request)
    {
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'role' => $this->item($result, FullInfoTransformer::class),
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the given resource with the specified input
     *
     * @param RoleRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleRequest $request, $id)
    {
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.updated'));
    }

    /**
     * Delete the specified resource in the database
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('delete', $this->entity);

        $rule = $this->rule('destroy', RoleRule::class, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }
        
        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Retrieve the set of permissions within the given role
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchPermissionByRoleId($id)
    {
        $record = $this->repository->find($id);

        if(!$record) {
            return $this->errorResponse(array(
                'record.not.found' => Lang::get('core::error.record.not.found')
            ));
        }

        $data = $this->item($record, FullInfoTransformer::class);

        return $this->successfulResponse([
            'permissions' => $data['permissions']
        ]);
    }

    protected function data(Request $request)
    {
        return $request->only(
            'apply',
            'info',
            'permissions'
        );
    }

    protected function form()
    {   
        list($sections, $permissions, $branches) = $this->references();

        return view('core::role.detail',
            compact('sections', 'permissions', 'branches')
        );
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        $sections = App::make(PermissionSectionRepository::class)
            ->with(['modules', 'modules.permissions'])
            ->all();

        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];

        $sections = $this->collection($sections, SectionTransformer::class)['data'];

        return [
            $sections,
            $this->permissions(),
            $branches
        ];
    }

    protected function permissions()
    {   
        return [
            'view'          => Auth::user()->can('view', $this->entity),
            'view_detail'   => Auth::user()->can('viewDetail', $this->entity),
            'create'        => Auth::user()->can('store', $this->entity),
            'update'        => Auth::user()->can('update', $this->entity),
            'delete'        => Auth::user()->can('delete', $this->entity)
        ];
    }
}
