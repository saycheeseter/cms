<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\RoleRepository;
use Modules\Core\Repositories\Contracts\PermissionSectionRepository;
use Modules\Core\Repositories\Criteria\SuperadminCurrentUserExcludedCriteria;
use Modules\Core\Repositories\Criteria\SuperadminExcludedCriteria;
use Modules\Core\Services\Contracts\UserServiceInterface;
use Modules\Core\Http\Requests\UserRequest;
use Modules\Core\Http\Requests\ChangePasswordRequest;
use Modules\Core\Http\Requests\ChangeAdminPasswordRequest;
use Modules\Core\Transformers\User\BasicTransformer;
use Modules\Core\Transformers\User\FullInfoTransformer;
use Modules\Core\Transformers\User\PermissionTransformer;
use Modules\Core\Transformers\PermissionSection\SectionTransformer;
use Modules\Core\Transformers\Role\ChosenTransformer as RoleChosenTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\Attachment\BasicTransformer as AttachmentBasicTransformer;
use Modules\Core\Transformers\UserLoginSchedule\FullInfoTransformer as UserLoginScheduleTransformer;
use Modules\Core\Entities\User;
use Modules\Core\Rules\Contracts\UserRuleInterface as UserRule;
use Fractal;
use Lang;
use App;
use Exception;
use Auth;

class UserController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(UserRepository $repository, UserServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = User::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->pushCriteria(app(SuperadminCurrentUserExcludedCriteria::class))
                ->paginate($perPage);

            $message = $data->count() === 0 
                ? Lang::get('core::info.no.results.found') 
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'users'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $permissions = $this->permissions();

        return view('core::user.list', compact('permissions'));
    }

    /**
     * Show create form and return list of available branches
     *
     * @return Response
     */
    public function create()
    {   
        $this->authorize('store', $this->entity);

        return $this->form();
    }

    /**
     * Show edit form
     * 
     * @param  integer $id
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('viewDetail', $this->entity);

        return $this->form();
    }

    /**
     * Show data for the specified user
     * 
     * @param  Request $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $this->authorize('viewDetail', $this->entity);

        if ($request->wantsJson()) {
            $user = $this->repository
                ->pushCriteria(app(SuperadminExcludedCriteria::class))
                ->with(['branches', 'permissions', 'attachments'])
                ->find($id);

            if(!$user) {
                return $this->errorResponse(array(
                    'show.failed' => Lang::get('core::error.show.failed')
                ));
            }

            return $this->successfulResponse([
                'user'        => $this->item($user, FullInfoTransformer::class),
                'attachment'  => $this->collection($user->attachments, AttachmentBasicTransformer::class)['data'],
                'permissions' => $this->item($user, PermissionTransformer::class),
                'schedule'    => $this->item($user->schedule, UserLoginScheduleTransformer::class)
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {   
        $this->authorize('store', $this->entity);

        $data = $this->data($request);

        $rule = $this->rule('store', UserRule::class, $data);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->store($data);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'user' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        $data = $this->data($request);

        $rule = $this->rule('update', UserRule::class, $data, $id);

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->update($data, $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'user' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Change the password of the currently authenticated user
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeCurrentUserPassword(ChangePasswordRequest $request)
    {
        $rule = $this->rule('changeCurrentUserPassword', UserRule::class, $request->only([
            'old_password',
            'new_password',
            'new_password_confirmation'
        ]));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        try {
            $result = $this->service->changeCurrentUserPassword($request->get('new_password'));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'change.password.failed' => Lang::get('core::error.change.password.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.password.changed'));
    }

    public function changeAdminPassword(ChangeAdminPasswordRequest $request, $id)
    {
        try {
            $result = $this->service->changeAdminPassword($id, $request->get('password'));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'change.password.failed' => Lang::get('core::error.change.password.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.password.changed'));
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'permissions',
            'schedule'
        );
    }

    protected function form()
    {   
        list($branches, $sections, $roles, $permissions) = $this->references();

        return view('core::user.detail', 
            compact('branches', 'sections', 'roles', 'permissions')
        );
    }


    private function references()
    {
        $branches = $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'];

        $roles = $this->collection(App::make(RoleRepository::class)->all(), RoleChosenTransformer::class)['data'];

        $sections = App::make(PermissionSectionRepository::class)
            ->with(['modules', 'modules.permissions'])
            ->all()
            ->sortBy('sequence_number');

        $sections = $this->collection($sections, SectionTransformer::class)['data'];

        return array($branches, $sections, $roles, $this->permissions());
    }

    protected function permissions()
    {
        return [
            'view'        => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update'      => Auth::user()->can('update', $this->entity),
            'delete'      => Auth::user()->can('delete', $this->entity),
            'create'      => Auth::user()->can('store', $this->entity),
        ];
    }
}
