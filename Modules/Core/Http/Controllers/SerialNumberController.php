<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\ProductSerialNumberRepository;
use Modules\Core\Transformers\InventoryChain\SerialTransactionTransformer;
use Modules\Core\Transformers\ProductSerialNumber\FullInfoTransformer;
use Lang;

class SerialNumberController extends Controller
{
    private $repository;

    public function __construct(ProductSerialNumberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findByNumber(Request $request, $number)
    {
        if (!$request->wantsJson()) {
            redirect('dashboard');
        }

        $serial = $this->repository->findByNumber($number);

        if (!$serial) {
            return $this->errorResponse([
                'serial.number.not.found' => Lang::get('core::error.serial.number.not.found')
            ]);
        }

        return $this->successfulResponse([
            'serial' => $this->item($serial, FullInfoTransformer::class)
        ]);
    }

    public function findTransactions(Request $request, $id)
    {
        if (!$request->wantsJson()) {
            redirect('dashboard');
        }

        $transactions = $this->repository->getTransactionReport($id);

        $message = $transactions->count() === 0
            ? Lang::get('core::info.no.results.found')
            : '';

        return $this->successfulResponse([
            'transactions' => $this->collection($transactions, SerialTransactionTransformer::class)['data']
        ], $message);
    }
}