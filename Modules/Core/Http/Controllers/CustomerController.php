<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Repositories\Contracts\CustomerBalanceFlowRepository;
use Modules\Core\Services\Contracts\CustomerServiceInterface;
use Modules\Core\Http\Requests\CustomerRequest;
use Modules\Core\Transformers\Customer\BasicTransformer;
use Modules\Core\Transformers\Customer\FullInfoTransformer;
use Modules\Core\Transformers\CustomerDetail\FullInfoTransformer as DetailTransformer;
use Modules\Core\Transformers\CustomerDetail\ChosenTransformer as DetailChosenTransformer;
use Modules\Core\Transformers\User\ChosenTransformer as UserChosenTransformer;
use Modules\Core\Transformers\BalanceFlow\BalanceTransformer;
use Modules\Core\Entities\Customer;
use Fractal;
use Lang;
use App;
use Exception;
use Auth;

class CustomerController extends Controller
{
    private $service;
    private $repository;
    private $entity;

    public function __construct(CustomerRepository $repository, CustomerServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->entity = Customer::class;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $perPage = $request->get('per_page', 10);

            $data = $this->repository
                ->with(['salesman'])
                ->paginate($perPage);

            $message = $data->count() === 0
                ? Lang::get('core::info.no.results.found')
                : '';

            return $this->successfulResponse(
                $this->collection($data, BasicTransformer::class, 'customers'),
                $message
            );
        }

        $this->authorize('view', $this->entity);

        $salesmen = $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'];

        $permissions = $this->permissions();

        return view('core::customer.list', compact('salesmen', 'permissions'));
    }

    /**
     * Find the data by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $customer = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$customer) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }

        $details = $this->paginate($customer->details, 10);

        return $this->successfulResponse(array(
            'customer' => $this->item($customer, FullInfoTransformer::class),
            'details'  => $this->collection($details, DetailTransformer::class)['data']
        ));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request)
    {   
        $this->authorize('store', $this->entity);

        try {
            $result = $this->service->store($this->data($request));
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'store.failed' => Lang::get('core::error.save.failed')
            ));
        }

        return $this->successfulResponse([
            'customer' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.created'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, $id)
    {   
        $this->authorize('update', $this->entity);

        try {
            $result = $this->service->update($this->data($request), $id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'update.failed' => Lang::get('core::error.update.failed')
            ));
        }
        
        return $this->successfulResponse([
            'customer' => $this->item($result, BasicTransformer::class)
        ], Lang::get('core::success.updated'));
    }

    /**
     * Get the customer detail of the specified customer
     * and transform the result into a chosen selection 
     * structure
     * 
     * @param  int $id 
     * @return \Illuminate\Http\JsonResponse
     */
    public function details($id)
    {   
        $customer = $this->repository
            ->with(['details'])
            ->find($id);

        if (!$customer) {
            return $this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            ));
        }
        
        return $this->successfulResponse(array(
            'details' => $this->collection($customer->details, DetailChosenTransformer::class)['data']
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {   
        $this->authorize('delete', $this->entity);

        try {
            $result = $this->service->delete($id);
        } catch (Exception $e) {
            return $this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            ));
        }

        return $this->successfulResponse([], Lang::get('core::success.deleted'));
    }

    /**
     * Find the indicated customers and return it with their corresponding data
     * structure indicated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findMany(Request $request)
    {
        $customers = $this->repository->findMany($request->get('ids'));

        if ($customers->count() === 0) {
            return $this->errorResponse(array(
                'no.customers.found' => Lang::get('core::error.no.customers.found')
            ));
        }

        return $this->successfulResponse([
            'customers' => $this->collection($customers, BasicTransformer::class)['data']
        ]);
    }

    /**
     * Get the current balance of the specified customer
     * 
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function balance($id)
    {
        $customer = $this->repository->find($id);

        if (!$customer) {
            return $this->errorResponse(array(
                'customer.not.found' => Lang::get('core::error.customer.not.found')
            ));
        }

        $result = App::make(CustomerBalanceFlowRepository::class)->getBalance($id);

        return $this->successfulResponse([
            'balance' => $this->item($result, BalanceTransformer::class)
        ]);
    }

    private function data(Request $request)
    {
        return $request->only(
            'info',
            'details'
        );
    }

    protected function permissions()
    {
        return [
            'view'        => Auth::user()->can('view', $this->entity),
            'view_detail' => Auth::user()->can('viewDetail', $this->entity),
            'update'      => Auth::user()->can('update', $this->entity),
            'delete'      => Auth::user()->can('delete', $this->entity),
            'create'      => Auth::user()->can('store', $this->entity),
        ];
    }
}
