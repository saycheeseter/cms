<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Repositories\Contracts\SalesReturnInboundRepository;
use Modules\Core\Services\Contracts\SalesReturnInboundServiceInterface;
use Modules\Core\Transformers\SalesReturnInbound\BasicTransformer;
use Modules\Core\Transformers\SalesReturnInbound\CollectionTransformer;
use Modules\Core\Transformers\SalesReturnInbound\FullInfoTransformer;
use Modules\Core\Transformers\SalesReturnInbound\ExportListTransformer;
use Modules\Core\Transformers\SalesReturnInbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\SalesReturnInboundRequest;
use Modules\Core\Http\Requests\SalesReturnInboundApprovedRequest;
use Modules\Core\Rules\Contracts\SalesReturnInboundRuleInterface as SalesReturnInboundRule;
use Modules\Core\Services\Contracts\Export\SalesReturnInboundExportServiceInterface as SalesReturnInboundExport;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\SalesReturnInbound;
use Lang;
use App;

class SalesReturnInboundController extends InventoryChainController
{
     public function __construct(SalesReturnInboundRepository $repository, SalesReturnInboundServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->export = SalesReturnInboundExport::class;
        $this->rule = SalesReturnInboundRule::class;
        $this->request['transaction'] = SalesReturnInboundRequest::class;
        $this->request['info'] = SalesReturnInboundApprovedRequest::class;
        $this->entity = SalesReturnInbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->module = ModuleDefinedFields::SALES_RETURN_INBOUND;
        $this->eagerLoad = [
            'customer',
            'customerDetail',
            'salesman',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    public function transactionsWithBalance(Request $request)
    {
        $result = $this->repository->transactionsWithBalance(
            $request->get('branch_id'),
            $request->get('customer'),
            $request->get('customer_type'),
            $request->get('per_page', 10)
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, BasicTransformer::class)
        ]);
    }

    public function getCollectibleTransactions(Request $request)
    {
        $result = $this->repository->findMany(
            $request->get('ids')
        );

        return $this->successfulResponse([
            'transactions' => $this->collection($result, CollectionTransformer::class)
        ]);
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data'],
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($customers, $udfs, $permissions, $templates) = $this->references();

        return view('core::sales-return-inbound.detail',
            compact('customers', 'udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::sales-return-inbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
