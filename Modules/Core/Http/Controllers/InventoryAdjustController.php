<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Services\Contracts\InventoryAdjustServiceInterface;
use Modules\Core\Services\Contracts\Export\InventoryAdjustExportServiceInterface as InventoryAdjustExport;
use Modules\Core\Repositories\Contracts\InventoryAdjustRepository;
use Modules\Core\Http\Requests\InventoryAdjustRequest;
use Modules\Core\Http\Requests\InventoryAdjustApprovedRequest;
use Modules\Core\Rules\Contracts\InventoryAdjustRuleInterface as InventoryAdjustRule;
use Modules\Core\Transformers\InventoryAdjust\BasicTransformer;
use Modules\Core\Transformers\InventoryAdjust\FullInfoTransformer;
use Modules\Core\Transformers\InventoryAdjust\ExportListTransformer;
use Modules\Core\Transformers\InventoryAdjustDetail\FullInfoTransformer as DetailFullInfoTransformer;
use Modules\Core\Transformers\InventoryAdjust\InfoPrintTransformer;
use Modules\Core\Transformers\InventoryAdjustDetail\PrintTransformer as DetailPrintTransformer;
use Modules\Core\Transformers\InventoryAdjust\FooterPrintTransformer;
use Modules\Core\Transformers\InventoryFlowDetail\TransactionTransformer;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\InventoryAdjust;
use Lang;
use App;
use Auth;

class InventoryAdjustController extends InventoryFlowController
{
    public function __construct(InventoryAdjustRepository $repository, InventoryAdjustServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->rule = InventoryAdjustRule::class;
        $this->request['transaction'] = InventoryAdjustRequest::class;
        $this->request['info'] = InventoryAdjustApprovedRequest::class;
        $this->entity = InventoryAdjust::class;
        $this->transformers['print'] = [
            'info' => InfoPrintTransformer::class,
            'detail' => DetailPrintTransformer::class,
            'footer' => FooterPrintTransformer::class
        ];

        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['detail'] = DetailFullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->export = InventoryAdjustExport::class;
        $this->module = ModuleDefinedFields::INVENTORY_ADJUST;

        $this->key = [
            'list' => 'records',
            'transaction' => 'record'
        ];

        $this->eagerLoad = [
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
        ];
    }

    /**
     * Checks if current recorded inventory in the transaction still matches
     * with the actual inventory of the products
     * 
     * @param  InventoryAdjustRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInventoryDiscrepancy(InventoryAdjustRequest $request)
    {
        ini_set('max_execution_time', 240);

        $rule = $this->rule('checkInventoryDiscrepancy', $this->rule, $this->data($request));

        if ($rule->fails()) {
            return $this->errorResponse($rule->errors());
        }

        return $this->successfulResponse();
    }

    /**
     * Get all the detail transaction of the product provided
     * 
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productTransactions(Request $request)
    {   
        $transactions = $this->repository->findTransactionsByProduct(
            $request->get('product_id'), 
            $request->get('type'),
            $request->get('per_page', 10)
        );

        if($transactions->isEmpty()) {
            return $this->errorResponse(array(
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ));
        }

        return $this->successfulResponse($this->collection($transactions, TransactionTransformer::class));
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::inventory-adjust.detail',
            compact('udfs','permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::inventory-adjust.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}
