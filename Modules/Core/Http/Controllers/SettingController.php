<?php

namespace Modules\Core\Http\Controllers;

class SettingController
{
    public function index()
    {
        return view('core::settings.settings');
    }
}