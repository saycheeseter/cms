<?php

namespace Modules\Core\Http\Controllers;

use Modules\Core\Http\Controllers\SystemCodeController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Repositories\Contracts\IncomeTypeRepository;
use Modules\Core\Services\Contracts\Import\IncomeTypeImportExcelServiceInterface;
use Modules\Core\Services\Contracts\IncomeTypeServiceInterface;
use Modules\Core\Entities\IncomeType;
use Lang;

class IncomeTypeController extends SystemCodeController
{
    public function __construct(IncomeTypeRepository $repository, IncomeTypeServiceInterface $service, IncomeTypeImportExcelServiceInterface $import)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->import = $import;
        $this->entity = IncomeType::class;
        $this->title = Lang::get('core::label.income.type');
    }
}
