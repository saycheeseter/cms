<?php 

namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository;
use Modules\Core\Services\Contracts\StockDeliveryInboundServiceInterface;
use Modules\Core\Transformers\InventoryChain\ChosenTransformer;
use Modules\Core\Transformers\StockDeliveryInbound\BasicTransformer;
use Modules\Core\Transformers\StockDeliveryInbound\FullInfoTransformer;
use Modules\Core\Transformers\StockDeliveryInbound\ExportListTransformer;
use Modules\Core\Transformers\StockDeliveryInbound\PrintTransformer as InfoPrintTransformer;
use Modules\Core\Http\Requests\StockDeliveryInboundRequest;
use Modules\Core\Http\Requests\StockDeliveryInboundApprovedRequest;
use Modules\Core\Rules\Contracts\StockDeliveryInboundRuleInterface as StockDeliveryInboundRule;
use Modules\Core\Services\Contracts\Export\StockDeliveryInboundExportServiceInterface as StockDeliveryInboundExport;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Entities\StockDeliveryInbound;
use Lang;
use App;

class StockDeliveryInboundController extends InventoryChainController
{
    public function __construct(StockDeliveryInboundRepository $repository, StockDeliveryInboundServiceInterface $service)
    {   
        $this->repository = $repository;
        $this->service = $service;
        $this->export = StockDeliveryInboundExport::class;
        $this->rule = StockDeliveryInboundRule::class;
        $this->request['transaction'] = StockDeliveryInboundRequest::class;
        $this->request['info'] = StockDeliveryInboundApprovedRequest::class;
        $this->entity = StockDeliveryInbound::class;
        $this->transformers['print']['info'] = InfoPrintTransformer::class;
        $this->transformers['basic'] = BasicTransformer::class;
        $this->transformers['full'] = FullInfoTransformer::class;
        $this->transformers['export'] = ExportListTransformer::class;
        $this->transformers['reference'] = ChosenTransformer::class;
        $this->module = ModuleDefinedFields::STOCK_DELIVERY_INBOUND;
        $this->eagerLoad = [
            'deliveryFrom',
            'deliveryFromLocation',
            'for',
            'creator',
            'auditor',
            'from',
            'requested',
            'location',
            'reference'
        ];
    }

    /**
     * List of module references
     *
     * @return array
     */
    protected function references()
    {
        return [
            $this->udfs(),
            $this->permissions(),
            App::make(PrintoutTemplateRepository::class)->findByModule($this->module),
        ];
    }

    protected function form()
    {   
        list($udfs, $permissions, $templates) = $this->references();

        return view('core::stock-delivery-inbound.detail', 
            compact('udfs', 'permissions', 'templates')
        );
    }

    protected function list()
    {
        return view('core::stock-delivery-inbound.list', array(
            'udfs'        => $this->udfs(),
            'permissions' => $this->permissions(),
            'templates'   => App::make(ExcelTemplateRepository::class)->findByModule($this->module)
        ));
    }
}