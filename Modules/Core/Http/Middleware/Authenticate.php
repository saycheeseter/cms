<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Exceptions\MultipleLoginException;

class Authenticate
{
    private $excludedUrls = [
        'auth',
        '_dusk',
        'logout',
        'lang',
    ];

    public function handle($request, Closure $next, $guard = null)
    {
        // If exluded url, allow access to controller
        if ($this->excludedUrl($request)) {
            return $next($request);
        }

        if (Auth::guard($guard)->check()) {
            if (!Auth::guard($guard)->inSession()) {
                throw new MultipleLoginException;
            } else if (null === $request->segment(1)) {
                return redirect('/dashboard');
            } else {
                return $next($request);
            }
        } else {
            if (null === $request->segment(1)) {
                return $next($request);
            }

            return redirect('/');
        }
    }

    private function excludedUrl($request)
    {
        return in_array($request->segment(1), $this->excludedUrls);
    }
}
