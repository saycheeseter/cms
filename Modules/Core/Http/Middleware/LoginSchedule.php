<?php

namespace Modules\Core\Http\Middleware;

use Modules\Core\Enums\UserType;
use Modules\Core\Exceptions\LoginScheduleException;
use Closure;
use Auth;

class LoginSchedule
{
    private $excludedUrls = [
        'auth',
        '_dusk',
        'logout',
        'lang',
        null
    ];

    public function handle($request, Closure $next)
    {
        // If exluded url, allow access to controller
        if ($this->excludedUrl($request)
            || Auth::guard()->user()->isEitherAdminOrSuperadmin()
            || Auth::guard()->user()->isWithinLoginSchedule()
        ) {
            return $next($request);
        }

        throw new LoginScheduleException();
    }

    private function excludedUrl($request)
    {
        return in_array($request->segment(1), $this->excludedUrls);
    }
}