<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Session;
use App;

class SwitchLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $locale = Session::get('nsi_lang', config('app.fallback_locale'));

        App::setLocale($locale);

        return $next($request);
    }
}
