<?php

Route::group([
    'middleware' => 'web',
    'namespace' => 'Modules\Core\Http\Controllers'
], function() {
    Route::get('/lang/{lang}', 'LanguageController@set');
    Route::get('/', 'LoginController@index');
    Route::post('/', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');
    Route::post('/auth', 'LoginController@authenticated');

    Route::get('dashboard', 'DashboardController@index');
    Route::get('changelogs', 'ChangeLogsController@index');
    Route::get('translation-manager', 'TranslationManagerController@index');

    Route::get('/settings', 'SettingController@index');

    Route::get('/settings/branch', 'BranchSettingController@index');
    Route::post('/settings/branch', 'BranchSettingController@store');
    Route::patch('/settings/branch', 'BranchSettingController@update');

    Route::get('/settings/generic', 'GenericSettingController@index');
    Route::post('/settings/generic', 'GenericSettingController@store');
    Route::patch('/settings/generic', 'GenericSettingController@update');

    Route::get('/udf/module/{module}', 'UDFController@byModule');
    Route::resource('udf', 'UDFController', [
        'parameters' => [
            'brand' => 'id'
        ]
    ]);

    Route::get('brand/m', 'BrandController@findMany');
    Route::post('brand/excel/import', 'BrandController@importExcel');
    Route::get('brand/excel/import/template', 'BrandController@downloadImportExcelTemplate');
    Route::resource('brand', 'BrandController', [
        'parameters' => [
            'brand' => 'id'
        ]
    ]);
    Route::resource('testing', 'TestingController');



    Route::resource('testing', 'TestingController');

    Route::get('uom/m', 'UOMController@findMany');
    Route::post('uom/excel/import', 'UOMController@importExcel');
    Route::get('uom/excel/import/template', 'UOMController@downloadImportExcelTemplate');
    Route::resource('uom', 'UOMController', [
        'parameters' => [
            'uom' => 'id'
        ]
    ]);

    Route::resource('bank-account', 'BankAccountController', [
        'parameters' => [
            'uom' => 'id'
        ]
    ]);

    Route::get('supplier/{id}/balance', 'SupplierController@balance');
    Route::get('supplier/m', 'SupplierController@findMany');
    Route::resource('supplier', 'SupplierController', [
        'parameters' => [
            'supplier' => 'id'
        ]
    ]);

    Route::get('member/m', 'MemberController@findMany');
    Route::post('member/excel/import', 'MemberController@importExcel');
    Route::get('member/excel/import/template', 'MemberController@downloadImportExcelTemplate');
    Route::resource('member', 'MemberController', [
        'parameters' => [
            'member' => 'id'
        ]
    ]);

    Route::get('user/my-menu', 'UserMenuController@index');
    Route::post('user/my-menu/save', 'UserMenuController@save');
    Route::get('user/my-menu/set/{type}', 'UserMenuController@set');
    Route::patch('user/change-password', 'UserController@changeCurrentUserPassword');
    Route::patch('user/{id}/change-admin-password', 'UserController@changeAdminPassword');
    Route::resource('user', 'UserController', [
        'parameters' => [
            'user' => 'id'
        ]
    ]);

    Route::post('product/excel/import', 'ProductController@importExcel');
    Route::get('product/excel/import/template', 'ProductController@downloadImportExcelTemplate');
    Route::get('product/export', 'ProductController@export');
    Route::get('product/select/supplier/price', 'ProductController@selectWithSupplierPrice');
    Route::get('product/select/customer/price', 'ProductController@selectWithCustomerPrice');
    Route::get('product/select/summary/price', 'ProductController@selectWithSummaryAndPrice');
    Route::get('product/{id}/report/transaction-summary', 'ProductController@transactionSummary');
    Route::get('product/{id}/report/stock-card', 'ProductController@stockCard');
    Route::get('product/{id}/report', 'ProductController@report');
    Route::get('product/{id}/serials/export', 'ProductController@exportAvailableSerials');
    Route::get('product/barcode', 'ProductController@findByBarcode');
    Route::get('product/stock-no', 'ProductController@findByStockNo');
    Route::get('product/name', 'ProductController@searchByName');
    Route::get('product/{id}/serials', 'ProductController@serials');
    Route::get('product/{id}/available-serials', 'ProductController@availableSerials');
    Route::get('product/{id}/batches', 'ProductController@batches');
    Route::get('product/m/summaries', 'ProductController@summaries');
    Route::get('product/m/{type}', 'ProductController@findMany');
    Route::get('product/{id}/summary', 'ProductController@summary');
    Route::get('product/{id}/alternatives', 'ProductController@alternatives');
    Route::get('product/{id}/components', 'ProductController@components');
    Route::patch('product/{id}/transactions/cost', 'ProductController@updateTransactionsPurchasePrice');

    Route::resource('product-discount', 'ProductDiscountController', [
        'parameters' => [
            'product-discount' => 'id'
        ]
    ]);

    Route::resource('product', 'ProductController', [
        'parameters' => [
            'product' => 'id'
        ]
    ]);

    Route::get('category/m', 'CategoryController@findMany');
    Route::resource('category', 'CategoryController', [
        'parameters' => [
            'category' => 'id'
        ]
    ]);

    Route::get('branch/{id}/details', 'BranchController@details');
    Route::resource('branch', 'BranchController', [
        'parameters' => [
            'branch' => 'id'
        ]
    ]);

    Route::get('customer/{id}/balance', 'CustomerController@balance');
    Route::get('customer/{id}/details', 'CustomerController@details');
    Route::get('customer/m', 'CustomerController@findMany');
    Route::resource('customer', 'CustomerController', [
        'parameters' => [
            'customer' => 'id'
        ]
    ]);

    Route::get('member-rate/m', 'MemberRateController@findMany');
    Route::resource('member-rate', 'MemberRateController', [
        'parameters' => [
            'memberrate' => 'id'
        ]
    ]);

    Route::get('bank/m', 'BankController@findMany');
    Route::post('bank/excel/import', 'BankController@importExcel');
    Route::get('bank/excel/import/template', 'BankController@downloadImportExcelTemplate');
    Route::resource('bank', 'BankController', [
        'parameters' => [
            'bank' => 'id'
        ]
    ]);

    Route::post('payment-method/excel/import', 'PaymentMethodController@importExcel');
    Route::get('payment-method/excel/import/template', 'PaymentMethodController@downloadImportExcelTemplate');
    Route::resource('payment-method', 'PaymentMethodController', [
        'parameters' => [
            'payment-method' => 'id'
        ]
    ]);

    Route::post('purchase-order/approval', 'PurchaseController@approval');
    Route::get('purchase-order/{id}/print/{template}/template', 'PurchaseController@print');
    Route::get('purchase-order/export', 'PurchaseController@export');
    Route::get('purchase-order/import', 'PurchaseController@import');
    Route::get('purchase-order/retrieve', 'PurchaseController@retrieve');
    Route::get('purchase-order/remaining', 'PurchaseController@remaining');
    Route::post('purchase-order/from-stock-request/create', 'PurchaseController@createFromStockRequestBySupplier');
    Route::patch('purchase-order/{id}/approved', 'PurchaseController@updateApproved');
    Route::resource('purchase-order', 'PurchaseController', [
        'parameters' => [
            'purchase-order' => 'id'
        ]
    ]);

    Route::post('purchase-return/approval', 'PurchaseReturnController@approval');
    Route::get('purchase-return/{id}/print/{template}/template', 'PurchaseReturnController@print');
    Route::get('purchase-return/export', 'PurchaseReturnController@export');
    Route::get('purchase-return/import', 'PurchaseReturnController@import');
    Route::patch('purchase-return/{id}/approved', 'PurchaseReturnController@updateApproved');
    Route::get('purchase-return/remaining', 'PurchaseReturnController@remaining');
    Route::get('purchase-return/retrieve', 'PurchaseReturnController@retrieve');
    Route::resource('purchase-return', 'PurchaseReturnController', [
        'parameters' => [
            'purchase-return' => 'id'
        ]
    ]);

    Route::post('damage/approval', 'DamageController@approval');
    Route::get('damage/{id}/print/{template}/template', 'DamageController@print');
    Route::get('damage/export', 'DamageController@export');
    Route::get('damage/import', 'DamageController@import');
    Route::get('damage/retrieve', 'DamageController@retrieve');
    Route::get('damage/remaining', 'DamageController@remaining');
    Route::patch('damage/{id}/approved', 'DamageController@updateApproved');
    Route::resource('damage', 'DamageController', [
        'parameters' => [
            'damage' => 'id'
        ]
    ]);

    Route::get('reason/m', 'ReasonController@findMany');
    Route::post('reason/excel/import', 'ReasonController@importExcel');
    Route::get('reason/excel/import/template', 'ReasonController@downloadImportExcelTemplate');
    Route::resource('reason', 'ReasonController', [
        'parameters' => [
            'reason' => 'id'
        ]
    ]);

    Route::post('sales/approval', 'SalesController@approval');
    Route::get('sales/{id}/print/{template}/template', 'SalesController@print');
    Route::get('sales/export', 'SalesController@export');
    Route::get('sales/import', 'SalesController@import');
    Route::get('sales/retrieve', 'SalesController@retrieve');
    Route::get('sales/remaining', 'SalesController@remaining');
    Route::patch('sales/{id}/approved', 'SalesController@updateApproved');
    Route::resource('sales', 'SalesController', [
        'parameters' => [
            'sales' => 'id'
        ]
    ]);

    Route::get('stock-request-to', 'StockRequestController@to');
    Route::get('stock-request-to/create', 'StockRequestController@create');
    Route::get('stock-request-to/{id}/edit', 'StockRequestController@edit');

    Route::get('stock-request-from', 'StockRequestController@from');
    Route::get('stock-request-from/{id}/view', 'StockRequestController@view');

    Route::get('stock-request/{id}/print/{template}/template', 'StockRequestController@print');
    Route::get('stock-request/export', 'StockRequestController@export');
    Route::post('stock-request/warning', 'StockRequestController@warning');
    Route::get('stock-request/remaining', 'StockRequestController@remaining');
    Route::get('stock-request/retrieve', 'StockRequestController@retrieve');
    Route::post('stock-request/approval', 'StockRequestController@approval');
    Route::get('stock-request/import', 'StockRequestController@import');
    //Route::get('stock-request/import-by-range-and-statuses', 'SalesOutboundController@importByRangeAndStatuses');
    Route::post('stock-request', 'StockRequestController@store');
    Route::get('stock-request/{id}', 'StockRequestController@show');
    Route::delete('stock-request/{id}', 'StockRequestController@destroy');
    Route::patch('stock-request/{id}', 'StockRequestController@update');
    Route::get('stock-request', 'StockRequestController@index');
    Route::patch('stock-request/{id}/approved', 'StockRequestController@updateApproved');

    Route::post('sales-return/approval', 'SalesReturnController@approval');
    Route::get('sales-return/{id}/print/{template}/template', 'SalesReturnController@print');
    Route::get('sales-return/export', 'SalesReturnController@export');
    Route::get('sales-return/import', 'SalesReturnController@import');
    Route::get('sales-return/remaining', 'SalesReturnController@remaining');
    Route::get('sales-return/retrieve', 'SalesReturnController@retrieve');
    Route::patch('sales-return/{id}/approved', 'SalesReturnController@updateApproved');
    Route::resource('sales-return', 'SalesReturnController', [
        'parameters' => [
            'sales-return' => 'id'
        ]
    ]);

    Route::get('stock-delivery/retrieve', 'StockDeliveryController@retrieve');
    Route::post('stock-delivery/approval', 'StockDeliveryController@approval');
    Route::get('stock-delivery/export', 'StockDeliveryController@export');
    Route::get('stock-delivery/import', 'StockDeliveryController@import');
    Route::get('stock-delivery/remaining', 'StockDeliveryController@remaining');
    Route::patch('stock-delivery/{id}/approved', 'StockDeliveryController@updateApproved');
    Route::get('stock-delivery/{id}/print/{template}/template', 'StockDeliveryController@print');

    Route::resource('stock-delivery', 'StockDeliveryController', [
        'parameters' => [
            'stock-delivery' => 'id'
        ]
    ]);

    Route::post('damage-outbound/approval', 'DamageOutboundController@approval');
    Route::get('damage-outbound/{id}/print/{template}/template', 'DamageOutboundController@print');
    Route::get('damage-outbound/export', 'DamageOutboundController@export');
    Route::get('damage-outbound/import', 'DamageOutboundController@import');
    Route::get('damage-outbound/product/transactions', 'DamageOutboundController@productTransactions');
    Route::patch('damage-outbound/{id}/approved', 'DamageOutboundController@updateApproved');
    Route::resource('damage-outbound', 'DamageOutboundController', [
        'parameters' => [
            'damage-outbound' => 'id'
        ]
    ]);

    Route::get('stock-return/retrieve', 'StockReturnController@retrieve');
    Route::post('stock-return/approval', 'StockReturnController@approval');
    Route::get('stock-return/export', 'StockReturnController@export');
    Route::get('stock-return/import', 'StockReturnController@import');
    Route::get('stock-return/remaining', 'StockReturnController@remaining');
    Route::patch('stock-return/{id}/approved', 'StockReturnController@updateApproved');
    Route::get('stock-return/{id}/print/{template}/template', 'StockReturnController@print');

    Route::resource('stock-return', 'StockReturnController', [
        'parameters' => [
            'stock-return' => 'id'
        ]
    ]);

    Route::get('sales-outbound/transaction-dates', 'SalesOutboundController@getByDateAndStatus');
    Route::get('sales-outbound/sheet-numbers', 'SalesOutboundController@getBySheetNumberAndStatus');
    Route::post('sales-outbound/approval', 'SalesOutboundController@approval');
    Route::get('sales-outbound/{id}/print/{template}/template', 'SalesOutboundController@print');
    Route::get('sales-outbound/export', 'SalesOutboundController@export');
    Route::get('sales-outbound/import', 'SalesOutboundController@import');
    Route::get('sales-outbound/transactions/balance', 'SalesOutboundController@transactionsWithBalance');
    Route::get('sales-outbound/transactions/collectible', 'SalesOutboundController@getCollectibleTransactions');
    Route::get('sales-outbound/product/transactions', 'SalesOutboundController@productTransactions');
    Route::patch('sales-outbound/{id}/approved', 'SalesOutboundController@updateApproved');
    Route::resource('sales-outbound', 'SalesOutboundController', [
        'parameters' => [
            'sales-outbound' => 'id'
        ]
    ]);

    Route::get('stock-delivery-outbound-to', 'StockDeliveryOutboundController@to');
    Route::get('stock-delivery-outbound-to/create', 'StockDeliveryOutboundController@create');
    Route::get('stock-delivery-outbound-to/{id}/edit', 'StockDeliveryOutboundController@edit');

    Route::get('stock-delivery-outbound-from', 'StockDeliveryOutboundController@from');
    Route::get('stock-delivery-outbound-from/{id}/view', 'StockDeliveryOutboundController@view');
    
    Route::get('stock-delivery-outbound/{id}/print/{template}/template', 'StockDeliveryOutboundController@print');
    Route::get('stock-delivery-outbound/export', 'StockDeliveryOutboundController@export');
    Route::get('stock-delivery-outbound/detail/{id}/serials', 'StockDeliveryOutboundController@serials');
    Route::get('stock-delivery-outbound/detail/{id}/batches', 'StockDeliveryOutboundController@batches');
    Route::get('stock-delivery-outbound/remaining', 'StockDeliveryOutboundController@remaining');
    Route::get('stock-delivery-outbound/retrieve', 'StockDeliveryOutboundController@retrieve');
    Route::post('stock-delivery-outbound/approval', 'StockDeliveryOutboundController@approval');
    Route::get('stock-delivery-outbound/import', 'StockDeliveryOutboundController@import');
    Route::post('stock-delivery-outbound', 'StockDeliveryOutboundController@store');
    Route::get('stock-delivery-outbound/{id}', 'StockDeliveryOutboundController@show');
    Route::delete('stock-delivery-outbound/{id}', 'StockDeliveryOutboundController@destroy');
    Route::patch('stock-delivery-outbound/{id}', 'StockDeliveryOutboundController@update');
    Route::get('stock-delivery-outbound', 'StockDeliveryOutboundController@index');
    Route::get('stock-delivery-outbound/product/transactions', 'StockDeliveryOutboundController@productTransactions');
    Route::get('stock-delivery-outbound/{id}/edit', 'StockDeliveryOutboundController@edit');
    Route::post('stock-delivery-outbound/warning', 'StockDeliveryOutboundController@warning');
    Route::get('stock-delivery-outbound', 'StockDeliveryOutboundController@index');
    Route::patch('stock-delivery-outbound/{id}/approved', 'StockDeliveryOutboundController@updateApproved');

    Route::post('stock-return-outbound/approval', 'StockReturnOutboundController@approval');
    Route::get('stock-return-outbound/detail/{id}/serials', 'StockReturnOutboundController@serials');
    Route::get('stock-return-outbound/detail/{id}/batches', 'StockReturnOutboundController@batches');
    Route::get('stock-return-outbound/{id}/print/{template}/template', 'StockReturnOutboundController@print');
    Route::get('stock-return-outbound/export', 'StockReturnOutboundController@export');
    Route::get('stock-return-outbound/import', 'StockReturnOutboundController@import');
    Route::get('stock-return-outbound/remaining', 'StockReturnOutboundController@remaining');
    Route::get('stock-return-outbound/retrieve', 'StockReturnOutboundController@retrieve');
    Route::get('stock-return-outbound/product/transactions', 'StockReturnOutboundController@productTransactions');
    Route::patch('stock-return-outbound/{id}/approved', 'StockReturnOutboundController@updateApproved');
    Route::resource('stock-return-outbound', 'StockReturnOutboundController', [
        'parameters' => [
            'stock-return-outbound' => 'id'
          ]
    ]);

    Route::post('purchase-return-outbound/approval', 'PurchaseReturnOutboundController@approval');
    Route::get('purchase-return-outbound/{id}/print/{template}/template', 'PurchaseReturnOutboundController@print');
    Route::get('purchase-return-outbound/export', 'PurchaseReturnOutboundController@export');
    Route::get('purchase-return-outbound/import', 'PurchaseReturnOutboundController@import');
    Route::get('purchase-return-outbound/transactions/balance', 'PurchaseReturnOutboundController@transactionsWithBalance');

    Route::get('purchase-return-outbound/transactions/payable', 'PurchaseReturnOutboundController@getPayableTransactions');

    Route::get('purchase-return-outbound/product/transactions', 'PurchaseReturnOutboundController@productTransactions');
    Route::patch('purchase-return-outbound/{id}/approved', 'PurchaseReturnOutboundController@updateApproved');
    Route::resource('purchase-return-outbound', 'PurchaseReturnOutboundController', [
        'parameters' => [
            'purchase-return-outbound' => 'id'
        ]
    ]);

    Route::post('sales-return-inbound/approval', 'SalesReturnInboundController@approval');
    Route::get('sales-return-inbound/{id}/print/{template}/template', 'SalesReturnInboundController@print');
    Route::get('sales-return-inbound/export', 'SalesReturnInboundController@export');
    Route::get('sales-return-inbound/import', 'SalesReturnInboundController@import');
    Route::get('sales-return-inbound/transactions/balance', 'SalesReturnInboundController@transactionsWithBalance');
    Route::get('sales-return-inbound/transactions/collectible', 'SalesReturnInboundController@getCollectibleTransactions');
    Route::get('sales-return-inbound/product/transactions', 'SalesReturnInboundController@productTransactions');
    Route::patch('sales-return-inbound/{id}/approved', 'SalesReturnInboundController@updateApproved');
    Route::resource('sales-return-inbound', 'SalesReturnInboundController', [
        'parameters' => [
            'sales-return-inbound' => 'id'
        ]
    ]);

    Route::post('stock-return-inbound/approval', 'StockReturnInboundController@approval');
    Route::get('stock-return-inbound/{id}/print/{template}/template', 'StockReturnInboundController@print');
    Route::get('stock-return-inbound/export', 'StockReturnInboundController@export');
    Route::get('stock-return-inbound/import', 'StockReturnInboundController@import');
    Route::get('stock-return-inbound/product/transactions', 'StockReturnInboundController@productTransactions');
    Route::patch('stock-return-inbound/{id}/approved', 'StockReturnInboundController@updateApproved');
    Route::resource('stock-return-inbound', 'StockReturnInboundController', [
        'parameters' => [
            'stock-return-inbound' => 'id'
          ]
    ]);

    Route::post('stock-delivery-inbound/approval', 'StockDeliveryInboundController@approval');
    Route::get('stock-delivery-inbound/export', 'StockDeliveryInboundController@export');
    Route::get('stock-delivery-inbound/import', 'StockDeliveryInboundController@import');
    Route::get('stock-delivery-inbound/{id}/print/{template}/template', 'StockDeliveryInboundController@print');
    Route::get('stock-delivery-inbound/product/transactions', 'StockDeliveryInboundController@productTransactions');
    Route::patch('stock-delivery-inbound/{id}/approved', 'StockDeliveryInboundController@updateApproved');
    Route::resource('stock-delivery-inbound', 'StockDeliveryInboundController', [
        'parameters' => [
            'stock-delivery-inbound' => 'id'
        ]
    ]);

    Route::post('purchase-inbound/approval', 'PurchaseInboundController@approval');
    Route::get('purchase-inbound/{id}/print/{template}/template', 'PurchaseInboundController@print');
    Route::get('purchase-inbound/export', 'PurchaseInboundController@export');
    Route::get('purchase-inbound/import', 'PurchaseInboundController@import');
    Route::get('purchase-inbound/transactions/balance', 'PurchaseInboundController@transactionsWithBalance');
    Route::get('purchase-inbound/transactions/payable', 'PurchaseInboundController@getPayableTransactions');
    Route::get('purchase-inbound/product/transactions', 'PurchaseInboundController@productTransactions');
    Route::patch('purchase-inbound/{id}/approved', 'PurchaseInboundController@updateApproved');
    Route::get('purchase-inbound/transaction/find/{reference}', 'PurchaseInboundController@findApprovedTransactionByReference');

    Route::resource('purchase-inbound', 'PurchaseInboundController', [
        'parameters' => [
            'purchase-inbound' => 'id'
        ]
    ]);

    Route::get('reports/customer/product-price', 'ReportsController@customerProductPrice');
    Route::get('reports/brand/monthly-sales-ranking', 'ReportsController@brandMonthlySalesRanking');
    Route::get('reports/supplier/product-price', 'ReportsController@supplierProductPrice');
    Route::get('reports/product/monthly-sales-ranking', 'ReportsController@productMonthlySalesRanking');
    Route::get('reports/category/monthly-sales-ranking', 'ReportsController@categoryMonthlySalesRanking');
    Route::get('reports/supplier/monthly-sales-ranking', 'ReportsController@supplierMonthlySalesRanking');
    Route::get('reports/product/inventory-warning', 'ReportsController@productInventoryWarning');
    Route::get('reports/product/negative-profit', 'ReportsController@negativeProfitProduct');
    Route::get('reports/product/periodic-sales', 'ReportsController@productPeriodicSales');
    Route::get('reports/product/serial-transaction', 'ReportsController@serialTransaction');
    Route::get('reports/product/transaction-list', 'ReportsController@productTransactionList');
    Route::get('reports/salesman', 'ReportsController@salesmanReport');
    Route::get('reports/salesman/{type}', 'ReportsController@salesmanReport');
    Route::get('reports/product/periodic-sales/export', 'ReportsController@exportProductPeriodicSales');
    Route::get('reports/product/transaction-list/export', 'ReportsController@exportProductTransactionList');
    Route::get('reports/product/negative-profit/export', 'ReportsController@exportNegativeProfitProduct');
    Route::get('reports/supplier/balance', 'ReportsController@supplierBalanceFlow');
    Route::get('reports/customer/balance', 'ReportsController@customerBalanceFlow');
    Route::get('reports/audit-trail', 'ReportsController@auditTrail');
    Route::get('reports/income-statement', 'ReportsController@incomeStatement');
    Route::get('reports/sales/detail-report', 'ReportsController@detailSalesReport');
    Route::get('reports/sales/detail-report-summary', 'ReportsController@detailSalesReportSummary');
    Route::get('reports/sales/detail-report/export', 'ReportsController@exportDetailSalesReport');

    Route::get('reports/pos-sales', 'ReportsController@posSales');
    Route::get('reports/pos/discounted-summary', 'ReportsController@posDiscountedSummary');
    Route::get('reports/member/report', 'ReportsController@memberReport');
    Route::get('reports/member/report/export', 'ReportsController@exportMemberReport');
    Route::get('terminals/branch/{branch}', 'TerminalController@getByBranch');
    Route::get('reports/bank-transaction', 'ReportsController@bankTransaction');
    Route::get('reports/cash-transaction', 'ReportsController@cashTransaction');
    Route::get('reports/product/branch-inventory', 'ReportsController@productBranchInventory');
    Route::get('reports/product/branch-inventory/export', 'ReportsController@exportProductBranchInventory');
    Route::get('reports/senior/transaction-report', 'ReportsController@seniorTransaction');
    Route::get('reports/payment/due-warning', 'ReportsController@paymentDueWarning');
    Route::get('reports/payment/due-warning-summary', 'ReportsController@paymentDueWarningSummary');
    Route::get('reports/collection/due-warning', 'ReportsController@collectionDueWarning');
    Route::get('reports/collection/due-warning-summary', 'ReportsController@collectionDueWarningSummary');
    Route::get('reports/product/inventory-value', 'ReportsController@productInventoryValue');
    Route::get('reports/product/inventory-value-summary', 'ReportsController@productInventoryValueSummary');
    Route::post('inventory-adjust/approval', 'InventoryAdjustController@approval');
    Route::get('inventory-adjust/export', 'InventoryAdjustController@export');
    Route::get('inventory-adjust/import', 'InventoryAdjustController@import');
    Route::post('inventory-adjust/check-inventory', 'InventoryAdjustController@checkInventoryDiscrepancy');
    Route::get('inventory-adjust/{id}/print/{template}/template', 'InventoryAdjustController@print');
    Route::get('inventory-adjust/product/transactions', 'InventoryAdjustController@productTransactions');
    Route::patch('inventory-adjust/{id}/approved', 'InventoryAdjustController@updateApproved');
    
    Route::resource('bank-transaction', 'BankTransactionController', [
        'parameters' => [
            'bank-transaction' => 'id'
        ]
    ]);

    Route::resource('inventory-adjust', 'InventoryAdjustController', [
        'parameters' => [
            'inventory-adjust' => 'id'
        ]
    ]);

    Route::post('payment/approval', 'PaymentController@approval');
    Route::resource('payment', 'PaymentController', [
        'parameters' => [
            'payment' => 'id'
        ]
    ]);

    Route::post('collection/approval', 'CollectionController@approval');
    Route::resource('collection', 'CollectionController', [
        'parameters' => [
            'collection' => 'id'
        ]
    ]);

    Route::resource('printout-template', 'PrintoutTemplateController', [
        'parameters' => [
            'printout-template' => 'id'
        ]
    ]);

    Route::get('excel-template/preview', 'ExcelTemplateController@preview');
    Route::get('excel-template/udf/{module}', 'ExcelTemplateController@getUDF');
    Route::resource('excel-template', 'ExcelTemplateController', [
        'parameters' => [
            'excel-template' => 'id'
        ]
    ]);

    Route::resource('reports/builder', 'ReportBuilderController', [
        'parameters' => [
            'reports/builder' => 'id'
        ]
    ]);

    Route::get('/reports/custom/export', 'CustomReportController@export');
    Route::get('/reports/custom/{name}', 'CustomReportController@index');

    Route::get('payment-type/m', 'PaymentTypeController@findMany');
    Route::post('payment-type/excel/import', 'PaymentTypeController@importExcel');
    Route::get('payment-type/excel/import/template', 'PaymentTypeController@downloadImportExcelTemplate');
    Route::resource('payment-type', 'PaymentTypeController', [
        'parameters' => [
            'payment_type' => 'id'
        ]
    ]);
    
    Route::get('issued-check/transfer', 'IssuedCheckController@transfer');
    Route::get('issued-check', 'IssuedCheckController@index');
    
    Route::get('received-check/transfer', 'ReceivedCheckController@transfer');
    Route::get('received-check', 'ReceivedCheckController@index');
  
    Route::get('counter/export', 'CounterController@export');
    Route::get('counter/{id}/print/{template}/template', 'CounterController@print');
    Route::resource('counter', 'CounterController', [
        'parameters' => [
            'counter' => 'id'
        ]
    ]);
  
    Route::get('other-payment/{id}/view', 'OtherPaymentController@index');
    Route::resource('other-payment', 'OtherPaymentController', [
        'parameters' => [
            'other-payment' => 'id'
        ]
    ]);

    Route::get('income-type/m', 'IncomeTypeController@findMany');
    Route::post('income-type/excel/import', 'IncomeTypeController@importExcel');
    Route::get('income-type/excel/import/template', 'IncomeTypeController@downloadImportExcelTemplate');
    Route::resource('income-type', 'IncomeTypeController', [
        'parameters' => [
            'income_type' => 'id'
        ]
    ]);

    Route::get('other-income/{id}/view', 'OtherIncomeController@index');
    Route::resource('other-income', 'OtherIncomeController', [
        'parameters' => [
            'other_income' => 'id'
        ]
    ]);

    Route::post('gift-check/bulk', 'GiftCheckController@bulkEntry');
    Route::resource('gift-check', 'GiftCheckController', [
        'parameters' => [
            'gift-check' => 'id'
        ]
    ]);

    Route::post('/attachment/temporary/{type}', 'AttachmentController@temporary');
    Route::post('/attachment/{module}/{type}/{id}/attach/', 'AttachmentController@attach');
    Route::post('/attachment/{module}/{type}/{id}/update/', 'AttachmentController@update');
    Route::post('/attachment/{key}/detach/', 'AttachmentController@detach');
    Route::get('/attachment/{type}/{id}', 'AttachmentController@getByType');

    Route::post('product-conversion/approval', 'ProductConversionController@approval');
    Route::get('product-conversion/export', 'ProductConversionController@export');
    Route::get('product-conversion/product/transactions', 'ProductConversionController@productTransactions');
    Route::resource('product-conversion', 'ProductConversionController', [
        'parameters' => [
            'product-conversion' => 'id'
        ]
    ]);

    Route::get('auto-product-conversion/product/transactions', 'AutoProductConversionController@productTransactions');

    Route::get('role/{id}/permissions', 'RoleController@fetchPermissionByRoleId');
    Route::resource('role', 'RoleController', [
        'parameters' => [
            'role' => 'id'
        ]
    ]);

    Route::get('user-column-settings/{module}', 'UserColumnSettingsController@getByModule');
    Route::post('user-column-settings/{module}', 'UserColumnSettingsController@save');

    Route::get('serial-number/{id}/transactions', 'SerialNumberController@findTransactions');
    Route::get('serial-number/number/{number}', 'SerialNumberController@findByNumber');

    Route::get('pos-summary-posting', 'PosSummaryPostingController@index');
    Route::patch('pos-summary-posting/{id}/generate', 'PosSummaryPostingController@generateSummary');

    Route::get('data-collector/reference-number', 'DataCollectorController@findByReferenceNumber');
    Route::delete('data-collector/reference-number/{reference}', 'DataCollectorController@destroyByReferenceNumber');
    Route::delete('data-collector/{id}', 'DataCollectorController@destroy');
    Route::get('data-collector', 'DataCollectorController@index');
    Route::get('pos-sales/product/transactions', 'PosSalesController@productTransactions');
    Route::get('pos-sales', 'PosSalesController@index');
    Route::get('pos-sales/{id}/view', 'PosSalesController@view');
    Route::get('pos-sales/{id}', 'PosSalesController@show');
    Route::get('transaction-summary-posting', 'TransactionSummaryPostingController@index');
    Route::post('transaction-summary-posting/generate', 'TransactionSummaryPostingController@generateSummary');
});