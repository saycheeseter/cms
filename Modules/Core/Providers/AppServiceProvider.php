<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Services\Contracts\BrandServiceInterface::class, \Modules\Core\Services\Concrete\BrandService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UOMServiceInterface::class, \Modules\Core\Services\Concrete\UOMService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BankAccountServiceInterface::class, \Modules\Core\Services\Concrete\BankAccountService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SupplierServiceInterface::class, \Modules\Core\Services\Concrete\SupplierService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\MemberServiceInterface::class, \Modules\Core\Services\Concrete\MemberService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BranchServiceInterface::class, \Modules\Core\Services\Concrete\BranchService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CategoryServiceInterface::class, \Modules\Core\Services\Concrete\CategoryService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductServiceInterface::class, \Modules\Core\Services\Concrete\ProductService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\MemberRateServiceInterface::class, \Modules\Core\Services\Concrete\MemberRateService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UserServiceInterface::class, \Modules\Core\Services\Concrete\UserService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CustomerServiceInterface::class, \Modules\Core\Services\Concrete\CustomerService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BankServiceInterface::class, \Modules\Core\Services\Concrete\BankService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PaymentMethodServiceInterface::class, \Modules\Core\Services\Concrete\PaymentMethodService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BranchSettingServiceInterface::class, \Modules\Core\Services\Concrete\BranchSettingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PurchaseServiceInterface::class, \Modules\Core\Services\Concrete\PurchaseService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\Generator::class, \Modules\Core\Adapters\Printer\Html::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ReasonServiceInterface::class, \Modules\Core\Services\Concrete\ReasonService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockReturnServiceInterface::class, \Modules\Core\Services\Concrete\StockReturnService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PurchaseReturnServiceInterface::class, \Modules\Core\Services\Concrete\PurchaseReturnService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SalesServiceInterface::class, \Modules\Core\Services\Concrete\SalesService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SalesReturnServiceInterface::class, \Modules\Core\Services\Concrete\SalesReturnService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockDeliveryServiceInterface::class, \Modules\Core\Services\Concrete\StockDeliveryService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockRequestServiceInterface::class, \Modules\Core\Services\Concrete\StockRequestService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\DamageServiceInterface::class, \Modules\Core\Services\Concrete\DamageService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\DamageOutboundServiceInterface::class, \Modules\Core\Services\Concrete\DamageOutboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BranchPriceServiceInterface::class, \Modules\Core\Services\Concrete\BranchPriceService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface::class, \Modules\Core\Services\Concrete\ProductBranchSummaryService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\RemainingQuantityServiceInterface::class, \Modules\Core\Services\Concrete\RemainingQuantityService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SalesReturnInboundServiceInterface::class, \Modules\Core\Services\Concrete\SalesReturnInboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockDeliveryOutboundServiceInterface::class, \Modules\Core\Services\Concrete\StockDeliveryOutboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockReturnOutboundServiceInterface::class, \Modules\Core\Services\Concrete\StockReturnOutboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockDeliveryInboundServiceInterface::class, \Modules\Core\Services\Concrete\StockDeliveryInboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SalesOutboundServiceInterface::class, \Modules\Core\Services\Concrete\SalesOutboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\StockReturnInboundServiceInterface::class, \Modules\Core\Services\Concrete\StockReturnInboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PurchaseReturnOutboundServiceInterface::class, \Modules\Core\Services\Concrete\PurchaseReturnOutboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PurchaseInboundServiceInterface::class, \Modules\Core\Services\Concrete\PurchaseInboundService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CustomerProductPricingServiceInterface::class, \Modules\Core\Services\Concrete\CustomerProductPricingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SupplierProductPricingServiceInterface::class, \Modules\Core\Services\Concrete\SupplierProductPricingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\GenericSettingServiceInterface::class, \Modules\Core\Services\Concrete\GenericSettingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ExcelTemplateServiceInterface::class, \Modules\Core\Services\Concrete\ExcelTemplateService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface::class, \Modules\Core\Services\Concrete\ProductTransactionSummaryService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductStockCardServiceInterface::class, \Modules\Core\Services\Concrete\ProductStockCardService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\InventoryAdjustServiceInterface::class, \Modules\Core\Services\Concrete\InventoryAdjustService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PrintoutTemplateServiceInterface::class, \Modules\Core\Services\Concrete\PrintoutTemplateService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PrinterServiceInterface::class, \Modules\Core\Services\Concrete\PrinterService::class);
        $this->app->singleton(\Modules\Core\Services\Contracts\PaperServiceInterface::class, \Modules\Core\Services\Concrete\PaperService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PaymentServiceInterface::class, \Modules\Core\Services\Concrete\PaymentService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\IssuedCheckServiceInterface::class, \Modules\Core\Services\Concrete\IssuedCheckService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CollectionServiceInterface::class, \Modules\Core\Services\Concrete\CollectionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ReceivedCheckServiceInterface::class, \Modules\Core\Services\Concrete\ReceivedCheckService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ReportBuilderServiceInterface::class, \Modules\Core\Services\Concrete\ReportBuilderService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\SupplierBalanceFlowServiceInterface::class, \Modules\Core\Services\Concrete\SupplierBalanceFlowService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CustomerBalanceFlowServiceInterface::class, \Modules\Core\Services\Concrete\CustomerBalanceFlowService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface::class, \Modules\Core\Services\Concrete\ProductSerialNumberService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UserDefinedFieldServiceInterface::class, \Modules\Core\Services\Concrete\UserDefinedFieldService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UserDefinedFieldMigrationServiceInterface::class, \Modules\Core\Services\Concrete\UserDefinedFieldMigrationService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\MemberPointTransactionServiceInterface::class, \Modules\Core\Services\Concrete\MemberPointTransactionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PosSalesServiceInterface::class, \Modules\Core\Services\Concrete\Fluent\PosSalesService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\GiftCheckServiceInterface::class, \Modules\Core\Services\Concrete\GiftCheckService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\XyzReadingServiceInterface::class, \Modules\Core\Services\Concrete\XyzReadingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CashDenominationServiceInterface::class, \Modules\Core\Services\Concrete\CashDenominationService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductBatchServiceInterface::class, \Modules\Core\Services\Concrete\ProductBatchService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BatchDeliveryServiceInterface::class, \Modules\Core\Services\Concrete\BatchDeliveryService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\AttachmentServiceInterface::class, \Modules\Core\Services\Concrete\AttachmentService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CounterServiceInterface::class, \Modules\Core\Services\Concrete\CounterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\OtherPaymentServiceInterface::class, \Modules\Core\Services\Concrete\OtherPaymentService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PaymentTypeServiceInterface::class, \Modules\Core\Services\Concrete\PaymentTypeService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\OtherIncomeServiceInterface::class, \Modules\Core\Services\Concrete\OtherIncomeService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\IncomeTypeServiceInterface::class, \Modules\Core\Services\Concrete\IncomeTypeService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UserMenuServiceInterface::class, \Modules\Core\Services\Concrete\UserMenuService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductDiscountServiceInterface::class, \Modules\Core\Services\Concrete\ProductDiscountService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductConversionServiceInterface::class, \Modules\Core\Services\Concrete\ProductConversionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\RoleServiceInterface::class, \Modules\Core\Services\Concrete\RoleService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PermissionSectionServiceInterface::class, \Modules\Core\Services\Concrete\PermissionSectionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PermissionModuleServiceInterface::class, \Modules\Core\Services\Concrete\PermissionModuleService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PermissionServiceInterface::class, \Modules\Core\Services\Concrete\PermissionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\AutoProductConversionServiceInterface::class, \Modules\Core\Services\Concrete\AutoProductConversionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface::class, \Modules\Core\Services\Concrete\TransactionMovingAverageCostService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ModuleSeriesServiceInterface::class, \Modules\Core\Services\Concrete\ModuleSeriesService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductBranchServiceInterface::class, \Modules\Core\Services\Concrete\ProductBranchService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductBranchDetailServiceInterface::class, \Modules\Core\Services\Concrete\ProductBranchDetailService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BranchModuleSeriesServiceInterface::class, \Modules\Core\Services\Concrete\BranchModuleSeriesService::class);
        $this->app->singleton('NumberFormatter', function($app) {
           return new \NumberFormatter('en_EN', \NumberFormatter::DECIMAL);
        });
        $this->app->singleton(\Modules\Core\Services\Contracts\UserDefinedFieldStructureInterface::class, \Modules\Core\Services\Concrete\UserDefinedFieldStructure::class);
        $this->app->bind(\Modules\Core\Services\Contracts\UserColumnSettingsServiceInterface::class, \Modules\Core\Services\Concrete\UserColumnSettingsService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface::class, \Modules\Core\Services\Concrete\ProductMonthlyTransactionService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\PosSummaryPostingServiceInterface::class, \Modules\Core\Services\Concrete\PosSummaryPostingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\LicenseApiServiceInterface::class, \Modules\Core\Services\Concrete\LicenseApiService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\ApiManagementServiceInterface::class, \Modules\Core\Services\Concrete\ApiManagementService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\TransactionSerialNumberLogsServiceInterface::class, \Modules\Core\Services\Concrete\TransactionSerialNumberLogsService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\DataCollectorServiceInterface::class, \Modules\Core\Services\Concrete\DataCollectorService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface::class, \Modules\Core\Services\Concrete\TransactionSummaryPostingService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\TransactionBatchLogServiceInterface::class, \Modules\Core\Services\Concrete\TransactionBatchLogService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\CustomerCreditServiceInterface::class, \Modules\Core\Services\Concrete\CustomerCreditService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\BankTransactionServiceInterface::class, \Modules\Core\Services\Concrete\BankTransactionService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
