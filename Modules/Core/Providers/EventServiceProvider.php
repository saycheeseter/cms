<?php

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Modules\Core\Events\PosSalesEvent' => [
            'Modules\Core\Listeners\PosSalesListener',
        ],
        'Modules\Core\Events\OtherPaymentCreated' => [
            'Modules\Core\Listeners\CreateCheckFromOtherPayment'
        ],
        'Modules\Core\Events\OtherPaymentUpdated' => [
            'Modules\Core\Listeners\ProcessCheckFromOtherPayment'
        ],
        'Modules\Core\Events\OtherPaymentDeleted' => [
            'Modules\Core\Listeners\DeleteCheckFromOtherPayment'
        ],
        'Modules\Core\Events\AutoProductConversionCreated' => [
            'Modules\Core\Listeners\AutoProductConversionListener'
        ],
        'Modules\Core\Events\UserCreated' => [
            'Modules\Core\Listeners\ActivateLicenseFromCreateUser'
        ],
        'Modules\Core\Events\UserUpdating' => [
            'Modules\Core\Listeners\ActivateLicenseFromUpdateUser'
        ],
        'Modules\Core\Events\UserDeleted' => [
            'Modules\Core\Listeners\DeactivateLicenseFromUser'
        ],
        'Modules\Core\Events\AutoProductConversionDeleted' => [
            'Modules\Core\Listeners\RevertAutoConversionSummaryData'
        ],
        'Modules\Core\Events\UserLoggedIn' => [
            'Modules\Core\Listeners\SetAuthUserSessionId',
            'Modules\Core\Listeners\AuditUserLogin',
        ],
        'Modules\Core\Events\SalesOutboundApproved' => [
            'Modules\Core\Listeners\IncrementCustomerDueFromSalesOutbound',
        ],
        'Modules\Core\Events\SalesOutboundReverted' => [
            'Modules\Core\Listeners\DecrementCustomerDueFromSalesOutbound',
        ],
        'Modules\Core\Events\SalesReturnInboundApproved' => [
            'Modules\Core\Listeners\DecrementCustomerDueFromSalesReturnInbound',
        ],
        'Modules\Core\Events\SalesReturnInboundReverted' => [
            'Modules\Core\Listeners\IncrementCustomerDueFromSalesReturnInbound',
        ],
        'Modules\Core\Events\CollectionApproved' => [
            'Modules\Core\Listeners\DecrementCustomerDueFromCollection',
        ],
        'Modules\Core\Events\CollectionReverted' => [
            'Modules\Core\Listeners\IncrementCustomerDueFromCollection',
        ],
        'Modules\Core\Events\ReceivedCheckTransferred' => [
            'Modules\Core\Listeners\DecrementCustomerDueFromReceivedCheck',
        ],
        'Modules\Core\Events\PosTransactionSynced' => [
            'Modules\Core\Listeners\DispatchInsertPosTransactionComponents',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
