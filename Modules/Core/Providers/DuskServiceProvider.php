<?php

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DuskServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        Route::get('/_dusk/login/{userId}/{branchId}/{guard?}', [
            'middleware' => 'web',
            'uses' => 'Modules\Core\Http\Controllers\DuskController@login',
        ]);

        Route::get('/_dusk/logout/{guard?}', [
            'middleware' => 'web',
            'uses' => 'Modules\Core\Http\Controllers\DuskController@logout',
        ]);

        Route::get('/_dusk/user/{guard?}', [
            'middleware' => 'web',
            'uses' => 'Modules\Core\Http\Controllers\DuskController@user',
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Laravel\Dusk\Console\InstallCommand::class,
                \Laravel\Dusk\Console\DuskCommand::class,
                \Laravel\Dusk\Console\MakeCommand::class,
                \Laravel\Dusk\Console\PageCommand::class,
            ]);
        }
    }
}
