<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class PrinterServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\PurchasePrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\PurchasePrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockReturnPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockReturnPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\PurchaseReturnPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\PurchaseReturnPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\SalesPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\SalesPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\SalesReturnPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\SalesReturnPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockDeliveryPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockDeliveryPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockRequestPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockRequestPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\DamagePrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\DamagePrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\DamageOutboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\DamageOutboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\SalesReturnInboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\SalesReturnInboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockDeliveryOutboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockDeliveryOutboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockReturnOutboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockReturnOutboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockDeliveryInboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockDeliveryInboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\SalesOutboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\SalesOutboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\StockReturnInboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\StockReturnInboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\PurchaseReturnOutboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\PurchaseReturnOutboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\PurchaseInboundPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\PurchaseInboundPrinterService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Printer\InventoryAdjustPrinterServiceInterface::class, \Modules\Core\Services\Concrete\Printer\InventoryAdjustPrinterService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
