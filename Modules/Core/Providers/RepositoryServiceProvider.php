<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Repositories\Contracts\UserRepository::class, \Modules\Core\Repositories\Eloquent\UserRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BranchRepository::class, \Modules\Core\Repositories\Eloquent\BranchRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BrandRepository::class, \Modules\Core\Repositories\Eloquent\BrandRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\UOMRepository::class, \Modules\Core\Repositories\Eloquent\UOMRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BankAccountRepository::class, \Modules\Core\Repositories\Eloquent\BankAccountRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SupplierRepository::class, \Modules\Core\Repositories\Eloquent\SupplierRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\MemberRepository::class, \Modules\Core\Repositories\Eloquent\MemberRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\MemberRateRepository::class, \Modules\Core\Repositories\Eloquent\MemberRateRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CategoryRepository::class, \Modules\Core\Repositories\Eloquent\CategoryRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductRepository::class, \Modules\Core\Repositories\Eloquent\ProductRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CustomerRepository::class, \Modules\Core\Repositories\Eloquent\CustomerRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\TaxRepository::class, \Modules\Core\Repositories\Eloquent\TaxRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PermissionSectionRepository::class, \Modules\Core\Repositories\Eloquent\PermissionSectionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BankRepository::class, \Modules\Core\Repositories\Eloquent\BankRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PaymentMethodRepository::class, \Modules\Core\Repositories\Eloquent\PaymentMethodRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BranchSettingRepository::class, \Modules\Core\Repositories\Eloquent\BranchSettingRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PurchaseRepository::class, \Modules\Core\Repositories\Eloquent\PurchaseRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BranchDetailRepository::class, \Modules\Core\Repositories\Eloquent\BranchDetailRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ReasonRepository::class, \Modules\Core\Repositories\Eloquent\ReasonRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockReturnRepository::class, \Modules\Core\Repositories\Eloquent\StockReturnRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PurchaseReturnRepository::class, \Modules\Core\Repositories\Eloquent\PurchaseReturnRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SalesRepository::class, \Modules\Core\Repositories\Eloquent\SalesRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SalesReturnRepository::class, \Modules\Core\Repositories\Eloquent\SalesReturnRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockDeliveryRepository::class, \Modules\Core\Repositories\Eloquent\StockDeliveryRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockRequestRepository::class, \Modules\Core\Repositories\Eloquent\StockRequestRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\DamageRepository::class, \Modules\Core\Repositories\Eloquent\DamageRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\DamageOutboundRepository::class, \Modules\Core\Repositories\Eloquent\DamageOutboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SalesReturnInboundRepository::class, \Modules\Core\Repositories\Eloquent\SalesReturnInboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository::class, \Modules\Core\Repositories\Eloquent\StockDeliveryOutboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockReturnOutboundRepository::class, \Modules\Core\Repositories\Eloquent\StockReturnOutboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository::class, \Modules\Core\Repositories\Eloquent\StockDeliveryInboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SalesOutboundRepository::class, \Modules\Core\Repositories\Eloquent\SalesOutboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockReturnInboundRepository::class, \Modules\Core\Repositories\Eloquent\StockReturnInboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PurchaseReturnOutboundRepository::class, \Modules\Core\Repositories\Eloquent\PurchaseReturnOutboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PurchaseInboundRepository::class, \Modules\Core\Repositories\Eloquent\PurchaseInboundRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductCustomerRepository::class, \Modules\Core\Repositories\Eloquent\ProductCustomerRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductSupplierRepository::class, \Modules\Core\Repositories\Eloquent\ProductSupplierRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductTransactionSummaryRepository::class, \Modules\Core\Repositories\Eloquent\ProductTransactionSummaryRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductStockCardRepository::class, \Modules\Core\Repositories\Eloquent\ProductStockCardRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\UserDefinedFieldRepository::class, \Modules\Core\Repositories\Eloquent\UserDefinedFieldRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\GenericSettingRepository::class, \Modules\Core\Repositories\Eloquent\GenericSettingRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ExcelTemplateRepository::class, \Modules\Core\Repositories\Eloquent\ExcelTemplateRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\InventoryAdjustRepository::class, \Modules\Core\Repositories\Eloquent\InventoryAdjustRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductBranchSummaryRepository::class, \Modules\Core\Repositories\Eloquent\ProductBranchSummaryRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PrintoutTemplateRepository::class, \Modules\Core\Repositories\Eloquent\PrintoutTemplateRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PaymentRepository::class, \Modules\Core\Repositories\Eloquent\PaymentRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CollectionRepository::class, \Modules\Core\Repositories\Eloquent\CollectionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ReportBuilderRepository::class, \Modules\Core\Repositories\Eloquent\ReportBuilderRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SupplierBalanceFlowRepository::class, \Modules\Core\Repositories\Eloquent\SupplierBalanceFlowRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CustomerBalanceFlowRepository::class, \Modules\Core\Repositories\Eloquent\CustomerBalanceFlowRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductSerialNumberRepository::class, \Modules\Core\Repositories\Eloquent\ProductSerialNumberRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockDeliveryOutboundDetailRepository::class, \Modules\Core\Repositories\Eloquent\StockDeliveryOutboundDetailRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\StockReturnOutboundDetailRepository::class, \Modules\Core\Repositories\Eloquent\StockReturnOutboundDetailRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\MemberPointTransactionRepository::class, \Modules\Core\Repositories\Eloquent\MemberPointTransactionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\GiftCheckRepository::class, \Modules\Core\Repositories\Eloquent\GiftCheckRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductDiscountRepository::class, \Modules\Core\Repositories\Eloquent\ProductDiscountRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CashDenominationRepository::class, \Modules\Core\Repositories\Eloquent\CashDenominationRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\SystemCodeTypeRepository::class, \Modules\Core\Repositories\Eloquent\SystemCodeTypeRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Builder\Contracts\CustomReportBuilderInterface::class, \Modules\Core\Repositories\Builder\Concrete\CustomReportBuilder::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductBatchRepository::class, \Modules\Core\Repositories\Eloquent\ProductBatchRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CounterRepository::class, \Modules\Core\Repositories\Eloquent\CounterRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\OtherPaymentRepository::class, \Modules\Core\Repositories\Eloquent\OtherPaymentRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PaymentTypeRepository::class, \Modules\Core\Repositories\Eloquent\PaymentTypeRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\OtherIncomeRepository::class, \Modules\Core\Repositories\Eloquent\OtherIncomeRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\IncomeTypeRepository::class, \Modules\Core\Repositories\Eloquent\IncomeTypeRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\IssuedCheckRepository::class, \Modules\Core\Repositories\Eloquent\IssuedCheckRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ReceivedCheckRepository::class, \Modules\Core\Repositories\Eloquent\ReceivedCheckRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductConversionRepository::class, \Modules\Core\Repositories\Eloquent\ProductConversionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\UserMenuRepository::class, \Modules\Core\Repositories\Eloquent\UserMenuRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\AuditTrailRepository::class, \Modules\Core\Repositories\Eloquent\AuditTrailRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\RoleRepository::class, \Modules\Core\Repositories\Eloquent\RoleRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\AutoProductConversionRepository::class, \Modules\Core\Repositories\Eloquent\AutoProductConversionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\UserColumnSettingsRepository::class, \Modules\Core\Repositories\Eloquent\UserColumnSettingsRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\ProductMonthlyTransactionRepository::class, \Modules\Core\Repositories\Eloquent\ProductMonthlyTransactionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BankTransactionReportRepository::class, \Modules\Core\Repositories\Eloquent\BankTransactionReportRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PosSalesRepository::class, \Modules\Core\Repositories\Eloquent\PosSalesRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\TerminalRepository::class, \Modules\Core\Repositories\Eloquent\TerminalRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\PosSummaryPostingRepository::class, \Modules\Core\Repositories\Eloquent\PosSummaryPostingRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\CashTransactionRepository::class, \Modules\Core\Repositories\Eloquent\CashTransactionRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\AttachmentRepository::class, \Modules\Core\Repositories\Eloquent\AttachmentRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\DataCollectorRepository::class, \Modules\Core\Repositories\Eloquent\DataCollectorRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\TransactionSummaryPostingRepository::class, \Modules\Core\Repositories\Eloquent\TransactionSummaryPostingRepositoryEloquent::class);
        $this->app->bind(\Modules\Core\Repositories\Contracts\BankTransactionRepository::class, \Modules\Core\Repositories\Eloquent\BankTransactionRepositoryEloquent::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
