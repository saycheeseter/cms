<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class ExportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Services\Contracts\Export\PurchaseExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\PurchaseExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockReturnExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockReturnExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\PurchaseReturnExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\PurchaseReturnExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\SalesExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\SalesExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\SalesReturnExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\SalesReturnExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockDeliveryExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockDeliveryExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockRequestExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockRequestExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\DamageExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\DamageExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\DamageOutboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\DamageOutboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\SalesReturnInboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\SalesReturnInboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockDeliveryOutboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockDeliveryOutboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockReturnOutboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockReturnOutboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockDeliveryInboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockDeliveryInboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\SalesOutboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\SalesOutboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\StockReturnInboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\StockReturnInboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\PurchaseReturnOutboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\PurchaseReturnOutboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\PurchaseInboundExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\PurchaseInboundExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ExcelTemplateExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\InventoryAdjustExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\InventoryAdjustExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\CounterExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\CounterExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductConversionExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductConversionExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductPeriodicSalesExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductPeriodicSalesExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductBranchInventoryExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductBranchInventoryExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductTransactionListExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductTransactionListExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\NegativeProfitProductExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\NegativeProfitProductExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\MemberReportExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\MemberReportExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductAvailableSerialsExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductAvailableSerialsExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\ProductExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\ProductExportService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Export\DetailSalesReportExportServiceInterface::class, \Modules\Core\Services\Concrete\Export\DetailSalesReportExportService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
