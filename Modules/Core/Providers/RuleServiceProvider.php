<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class RuleServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Rules\Contracts\CategoryRuleInterface::class, \Modules\Core\Rules\Concrete\CategoryRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\MemberRateRuleInterface::class, \Modules\Core\Rules\Concrete\MemberRateRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\ProductRuleInterface::class, \Modules\Core\Rules\Concrete\ProductRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PurchaseRuleInterface::class, \Modules\Core\Rules\Concrete\PurchaseRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockReturnRuleInterface::class, \Modules\Core\Rules\Concrete\StockReturnRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PurchaseReturnRuleInterface::class, \Modules\Core\Rules\Concrete\PurchaseReturnRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\SalesRuleInterface::class, \Modules\Core\Rules\Concrete\SalesRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\SalesReturnRuleInterface::class, \Modules\Core\Rules\Concrete\SalesReturnRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockDeliveryRuleInterface::class, \Modules\Core\Rules\Concrete\StockDeliveryRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockRequestRuleInterface::class, \Modules\Core\Rules\Concrete\StockRequestRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\DamageRuleInterface::class, \Modules\Core\Rules\Concrete\DamageRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\DamageOutboundRuleInterface::class, \Modules\Core\Rules\Concrete\DamageOutboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\SalesReturnInboundRuleInterface::class, \Modules\Core\Rules\Concrete\SalesReturnInboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockDeliveryOutboundRuleInterface::class, \Modules\Core\Rules\Concrete\StockDeliveryOutboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockReturnOutboundRuleInterface::class, \Modules\Core\Rules\Concrete\StockReturnOutboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockDeliveryInboundRuleInterface::class, \Modules\Core\Rules\Concrete\StockDeliveryInboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\SalesOutboundRuleInterface::class, \Modules\Core\Rules\Concrete\SalesOutboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\StockReturnInboundRuleInterface::class, \Modules\Core\Rules\Concrete\StockReturnInboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PurchaseReturnOutboundRuleInterface::class, \Modules\Core\Rules\Concrete\PurchaseReturnOutboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PurchaseInboundRuleInterface::class, \Modules\Core\Rules\Concrete\PurchaseInboundRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\InventoryAdjustRuleInterface::class, \Modules\Core\Rules\Concrete\InventoryAdjustRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PaymentRuleInterface::class, \Modules\Core\Rules\Concrete\PaymentRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\CollectionRuleInterface::class, \Modules\Core\Rules\Concrete\CollectionRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\UDFRuleInterface::class, \Modules\Core\Rules\Concrete\UDFRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\GiftCheckRuleInterface::class, \Modules\Core\Rules\Concrete\GiftCheckRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\ProductDiscountRuleInterface::class, \Modules\Core\Rules\Concrete\ProductDiscountRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\ProductConversionRuleInterface::class, \Modules\Core\Rules\Concrete\ProductConversionRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\RoleRuleInterface::class, \Modules\Core\Rules\Concrete\RoleRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\UserRuleInterface::class, \Modules\Core\Rules\Concrete\UserRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\OtherIncomeRuleInterface::class, \Modules\Core\Rules\Concrete\OtherIncomeRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\OtherPaymentRuleInterface::class, \Modules\Core\Rules\Concrete\OtherPaymentRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PosSalesRuleInterface::class, \Modules\Core\Rules\Concrete\PosSalesRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\PosSummaryPostingRuleInterface::class, \Modules\Core\Rules\Concrete\PosSummaryPostingRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\DataCollectorRuleInterface::class, \Modules\Core\Rules\Concrete\DataCollectorRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\TransactionSummaryPostingRuleInterface::class, \Modules\Core\Rules\Concrete\TransactionSummaryPostingRule::class);
        $this->app->bind(\Modules\Core\Rules\Contracts\GenericSettingRuleInterface::class, \Modules\Core\Rules\Concrete\GenericSettingRule::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
