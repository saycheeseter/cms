<?php

namespace Modules\Core\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Repositories\Contracts\BranchDetailRepository;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\MemberRepository;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Contracts\ReportBuilderRepository;
use Modules\Core\Repositories\Contracts\UserMenuRepository;
use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Traits\HasMorphMap;
use Modules\Core\Transformers\Supplier\ChosenTransformer;
use Modules\Core\Transformers\Customer\ChosenTransformer as CustomerChosenTransformer;
use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as BrandChosenTransformer;
use Modules\Core\Transformers\Category\ChosenTransformer as CategoryChosenTransformer;
use Modules\Core\Transformers\BranchDetail\ChosenTransformer as BranchDetailChosenTransformer;
use Modules\Core\Transformers\User\ChosenTransformer as UserChosenTransformer;
use Modules\Core\Transformers\SystemCode\ChosenTransformer as PaymentMethodChosenTransformer;
use Modules\Core\Transformers\BankAccount\ChosenTransformer as BankAccountChosenTransformer;
use Modules\Core\Transformers\ReportBuilder\SidebarTransformer as ReportBuilderSidebarTransformer;
use Modules\Core\Transformers\UserMenu\FullInfoTransformer as UserMenuTransformer;
use Auth;
use App;
use Session;

class ComposerServiceProvider extends ServiceProvider
{
    use FractalTransformer, HasMorphMap;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $that = $this;
        
        View::composer('core::layouts.master', function ($view) {
            $view->with([
                'loggedIn'   => Auth::check(),
                'reports'    => $this->collection(App::make(ReportBuilderRepository::class)->skipCriteria()->all()->groupBy('group_id'), ReportBuilderSidebarTransformer::class)['data'],
                'customMenu' => [
                    'active' => Session::get('active_menu_type'),
                    'list'   => Auth::check()
                        ? $this->item(
                            App::make(UserMenuRepository::class)->findByUser(Auth::user()->id ?? 0),
                            UserMenuTransformer::class
                        )['viewable_format']
                        : []
                ]
            ]);
        });

        View::composer('core::modal.product-filter', function ($view) {
            $view->with([
                'references' => [
                    'filters' => [
                        'product' => [
                            'suppliers'  => App::make(SupplierRepository::class)->all(),
                            'brands'     => App::make(BrandRepository::class)->all(),
                            'categories' => App::make(CategoryRepository::class)->all(),
                            'branches'   => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data']
                        ]
                    ]
                ]
            ]);
        });

        View::composer('core::modal.monthly-sales-ranking-filter', function($view) {
            $view->with([
                'branches' => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
                'locations' => $this->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'],
                'brands' => $this->collection(App::make(BrandRepository::class)->all(), BrandChosenTransformer::class)['data'],
                'categories' => $this->collection(App::make(CategoryRepository::class)->all(), CategoryChosenTransformer::class)['data'],
                'suppliers' => $this->collection(App::make(SupplierRepository::class)->all(), ChosenTransformer::class)['data']
            ]);
        });

        View::composer(['core::modal.invoice-filter'], function ($view) use ($that) {
            $view->with([
                'branches' => $that->collection(App::make(BranchRepository::class)->skipCriteria()->all(), BranchChosenTransformer::class)['data'],
                'locations' => $that->collection(App::make(BranchDetailRepository::class)->all(), BranchDetailChosenTransformer::class)['data'],
                'users'     => $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'],
            ]);
        });

        View::composer(['core::inventory-chain.detail', 'core::invoice.detail', 'core::product-conversion.detail', 'core::inventory-adjust.detail'], function ($view) use ($that) {
            $view->with([
                'branches'  => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
                'locations' => $this->collection(App::make(BranchRepository::class)->find(Auth::branch()->id)->details, BranchDetailChosenTransformer::class)['data'],
                'users'     => $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'],
            ]);
        });

        View::composer(['core::payment.detail', 'core::collection.detail'], function ($view) use ($that) {
            $view->with([
                'branches'       => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
                'paymentMethods' => $this->collection(App::make(PaymentMethodRepository::class)->getDefaultMethods(), PaymentMethodChosenTransformer::class)['data'],
                'bankAccounts'   => $this->collection(App::make(BankAccountRepository::class)->all(), BankAccountChosenTransformer::class)['data'],
                'types'          => [
                    'purchase_inbound'         => $this->getMorphMapKey(PurchaseInbound::class),
                    'purchase_return_outbound' => $this->getMorphMapKey(PurchaseReturnOutbound::class),
                    'sales_outbound'           => $this->getMorphMapKey(SalesOutbound::class),
                    'sales_return_inbound'     => $this->getMorphMapKey(SalesReturnInbound::class)
                ]
            ]);
        });

        View::composer(['core::issued-check.list', 'core::received-check.list'], function ($view) use ($that) {
            $view->with([
                'bankAccounts' => $this->collection(App::make(BankAccountRepository::class)->all(), BankAccountChosenTransformer::class)['data'],
                'branches' => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            ]);
        });

        View::composer(['core::purchase-order.list', 'core::purchase-inbound.list', 'core::purchase-return.list', 'core::purchase-return-outbound.list'], function ($view) use ($that) {
            $view->with([
                'suppliers' => $this->collection(App::make(SupplierRepository::class)->all(), ChosenTransformer::class)['data']
            ]);
        });

        View::composer(['core::sales.list', 'core::sales-outbound.list', 'core::sales-return.list', 'core::sales-return-inbound.list'], function ($view) use ($that) {
            $view->with([
                'customers' => $this->collection(App::make(CustomerRepository::class)->all(), CustomerChosenTransformer::class)['data']
            ]);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
