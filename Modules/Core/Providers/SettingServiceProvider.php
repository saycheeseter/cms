<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Repositories\Contracts\BranchSettingRepository;
use Modules\Core\Repositories\Contracts\GenericSettingRepository;
use App;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('BranchSettings', function ($app) {
            return App::make(BranchSettingRepository::class)->all();
        });

        $this->app->singleton('GenericSettings', function ($app) {
            return App::make(GenericSettingRepository::class)->all();
        });
    }
}
