<?php


namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class ImportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Modules\Core\Services\Contracts\Import\BrandImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\BrandImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\BankImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\BankImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\IncomeTypeImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\IncomeTypeImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\PaymentTypeImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\PaymentTypeImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\ReasonImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\ReasonImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\UOMImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\UOMImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\ProductImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\ProductImportExcelService::class);
        $this->app->bind(\Modules\Core\Services\Contracts\Import\MemberImportExcelServiceInterface::class, \Modules\Core\Services\Concrete\Import\MemberImportExcelService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}