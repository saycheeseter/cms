<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('date_after', '\Modules\Core\Validations\DateAfterValidator@validate');
        Validator::extend('unique_in_set', '\Modules\Core\Validations\UniqueInSetValidator@validate');
        Validator::extend('array_unique', '\Modules\Core\Validations\ArrayUniqueValidator@validate');
        Validator::extend('unique_in', '\Modules\Core\Validations\UniqueInValidator@validate');
        Validator::extend('exists_in', '\Modules\Core\Validations\ExistsInValidator@validate');
        Validator::extend('barcode', '\Modules\Core\Validations\BarcodeValidator@validate');
        Validator::extend('uom', '\Modules\Core\Validations\UomValidator@validate');
        Validator::extend('date_between', '\Modules\Core\Validations\DateBetweenValidator@validate');
        Validator::extend('decimal_min', '\Modules\Core\Validations\DecimalValidator@validate');
        Validator::extend('serial_count', '\Modules\Core\Validations\SerialNumberValidator@count');
        Validator::extend('serial_unique', '\Modules\Core\Validations\SerialNumberValidator@unique');
        Validator::extend('serial_unique_in_array', '\Modules\Core\Validations\SerialNumberValidator@uniqueInArray');
        Validator::extend('digits_range', '\Modules\Core\Validations\DigitsRangeValidator@validate');
        Validator::extend('batch_count', '\Modules\Core\Validations\ProductBatchValidator@count');
        Validator::extend('batch_unique', '\Modules\Core\Validations\ProductBatchValidator@unique');
        Validator::extend('batch_unique_in_array', '\Modules\Core\Validations\ProductBatchValidator@uniqueInArray');
    }
}
