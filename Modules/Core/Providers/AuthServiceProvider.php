<?php

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Modules\Core\Entities\Branch::class                 => \Modules\Core\Policies\BranchPolicy::class,
        \Modules\Core\Entities\Category::class               => \Modules\Core\Policies\CategoryPolicy::class,
        \Modules\Core\Entities\Supplier::class               => \Modules\Core\Policies\SupplierPolicy::class,
        \Modules\Core\Entities\Customer::class               => \Modules\Core\Policies\CustomerPolicy::class,
        \Modules\Core\Entities\Member::class                 => \Modules\Core\Policies\MemberPolicy::class,
        \Modules\Core\Entities\MemberRate::class             => \Modules\Core\Policies\MemberRatePolicy::class,
        \Modules\Core\Entities\BankAccount::class            => \Modules\Core\Policies\BankAccountPolicy::class,
        \Modules\Core\Entities\Brand::class                  => \Modules\Core\Policies\BrandPolicy::class,
        \Modules\Core\Entities\Bank::class                   => \Modules\Core\Policies\BankPolicy::class,
        \Modules\Core\Entities\Reason::class                 => \Modules\Core\Policies\ReasonPolicy::class,
        \Modules\Core\Entities\UOM::class                    => \Modules\Core\Policies\UomPolicy::class,
        \Modules\Core\Entities\Product::class                => \Modules\Core\Policies\ProductPolicy::class,
        \Modules\Core\Entities\User::class                   => \Modules\Core\Policies\UserPolicy::class,
        \Modules\Core\Entities\Purchase::class               => \Modules\Core\Policies\PurchasePolicy::class,
        \Modules\Core\Entities\PurchaseInbound::class        => \Modules\Core\Policies\PurchaseInboundPolicy::class,
        \Modules\Core\Entities\PurchaseReturn::class         => \Modules\Core\Policies\PurchaseReturnPolicy::class,
        \Modules\Core\Entities\PurchaseReturnOutbound::class => \Modules\Core\Policies\PurchaseReturnOutboundPolicy::class,
        \Modules\Core\Entities\Damage::class                 => \Modules\Core\Policies\DamagePolicy::class,
        \Modules\Core\Entities\DamageOutbound::class         => \Modules\Core\Policies\DamageOutboundPolicy::class,
        \Modules\Core\Entities\Sales::class                  => \Modules\Core\Policies\SalesPolicy::class,
        \Modules\Core\Entities\SalesOutbound::class          => \Modules\Core\Policies\SalesOutboundPolicy::class,
        \Modules\Core\Entities\SalesReturn::class            => \Modules\Core\Policies\SalesReturnPolicy::class,
        \Modules\Core\Entities\SalesReturnInbound::class     => \Modules\Core\Policies\SalesReturnInboundPolicy::class,
        \Modules\Core\Entities\StockRequest::class           => \Modules\Core\Policies\StockRequestPolicy::class,
        \Modules\Core\Entities\StockDelivery::class          => \Modules\Core\Policies\StockDeliveryPolicy::class,
        \Modules\Core\Entities\StockDeliveryInbound::class   => \Modules\Core\Policies\StockDeliveryInboundPolicy::class,
        \Modules\Core\Entities\StockDeliveryOutbound::class  => \Modules\Core\Policies\StockDeliveryOutboundPolicy::class,
        \Modules\Core\Entities\StockReturn::class            => \Modules\Core\Policies\StockReturnPolicy::class,
        \Modules\Core\Entities\StockReturnInbound::class     => \Modules\Core\Policies\StockReturnInboundPolicy::class,
        \Modules\Core\Entities\StockReturnOutbound::class    => \Modules\Core\Policies\StockReturnOutboundPolicy::class,
        \Modules\Core\Entities\InventoryAdjust::class        => \Modules\Core\Policies\InventoryAdjustPolicy::class,
        \Modules\Core\Entities\PaymentMethod::class          => \Modules\Core\Policies\PaymentMethodPolicy::class,
        \Modules\Core\Entities\PrintoutTemplate::class       => \Modules\Core\Policies\PrintoutTemplatePolicy::class,
        \Modules\Core\Entities\ExcelTemplate::class          => \Modules\Core\Policies\ExcelTemplatePolicy::class,
        \Modules\Core\Entities\OtherPayment::class           => \Modules\Core\Policies\OtherPaymentPolicy::class,
        \Modules\Core\Entities\PaymentType::class            => \Modules\Core\Policies\PaymentTypePolicy::class,
        \Modules\Core\Entities\OtherIncome::class            => \Modules\Core\Policies\OtherIncomePolicy::class,
        \Modules\Core\Entities\IncomeType::class             => \Modules\Core\Policies\IncomeTypePolicy::class,
        \Modules\Core\Entities\InventoryAdjust::class        => \Modules\Core\Policies\InventoryAdjustPolicy::class,
        \Modules\Core\Entities\PaymentMethod::class          => \Modules\Core\Policies\PaymentMethodPolicy::class,
        \Modules\Core\Entities\PrintoutTemplate::class       => \Modules\Core\Policies\PrintoutTemplatePolicy::class,
        \Modules\Core\Entities\ExcelTemplate::class          => \Modules\Core\Policies\ExcelTemplatePolicy::class,
        \Modules\Core\Entities\GiftCheck::class              => \Modules\Core\Policies\GiftCheckPolicy::class,
        \Modules\Core\Entities\Counter::class                => \Modules\Core\Policies\CounterPolicy::class,
        \Modules\Core\Entities\ProductDiscount::class        => \Modules\Core\Policies\ProductDiscountPolicy::class,
        \Modules\Core\Entities\ProductConversion::class      => \Modules\Core\Policies\ProductConversionPolicy::class,
        \Modules\Core\Entities\Payment::class                => \Modules\Core\Policies\PaymentPolicy::class,
        \Modules\Core\Entities\Collection::class             => \Modules\Core\Policies\CollectionPolicy::class,
        \Modules\Core\Entities\IssuedCheck::class            => \Modules\Core\Policies\IssuedCheckPolicy::class,
        \Modules\Core\Entities\ReceivedCheck::class          => \Modules\Core\Policies\ReceivedCheckPolicy::class,
        \Modules\Core\Entities\ReportBuilder::class          => \Modules\Core\Policies\ReportBuilderPolicy::class,
        \Modules\Core\Entities\Role::class                   => \Modules\Core\Policies\RolePolicy::class,
        \Modules\Core\Entities\BankTransaction::class        => \Modules\Core\Policies\BankTransactionPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
