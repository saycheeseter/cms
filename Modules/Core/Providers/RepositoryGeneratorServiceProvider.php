<?php
namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * Custom service provider for Prettus Repository package 
 * to override some conflict commands
 */
class RepositoryGeneratorServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            base_path() . '/vendor/prettus/l5-repository/src/resources/config/repository.php' => config_path('repository.php')
        ]);

        $this->mergeConfigFrom(base_path() . '/vendor/prettus/l5-repository/src/resources/config/repository.php', 'repository');

        $this->loadTranslationsFrom(base_path() . '/vendor/prettus/l5-repository/src/resources/lang', 'repository');
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Prettus\Repository\Generators\Commands\RepositoryCommand');
        $this->commands('Prettus\Repository\Generators\Commands\TransformerCommand');
        $this->commands('Prettus\Repository\Generators\Commands\PresenterCommand');
        $this->commands('Prettus\Repository\Generators\Commands\EntityCommand');
        $this->commands('Prettus\Repository\Generators\Commands\ValidatorCommand');
        //$this->commands('Prettus\Repository\Generators\Commands\ControllerCommand');
        // Custom ControllerCommand
        $this->commands('Modules\Core\Console\ControllerCommand');
        $this->commands('Prettus\Repository\Generators\Commands\BindingsCommand');
        $this->commands('Prettus\Repository\Generators\Commands\CriteriaCommand');
        $this->app->register('Prettus\Repository\Providers\EventServiceProvider');
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
