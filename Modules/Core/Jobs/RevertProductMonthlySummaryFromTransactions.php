<?php

namespace Modules\Core\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;

class RevertProductMonthlySummaryFromTransactions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transaction;

    public $tries = 5;

    public $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @param GenerateTransactionProductMonthlySummary $model
     */
    public function __construct(GenerateTransactionProductMonthlySummary $model)
    {
        $this->transaction = $model;
    }

    /**
     * Execute the job.
     *
     * @param ProductMonthlyTransactionServiceInterface $service
     * @return void
     */
    public function handle(ProductMonthlyTransactionServiceInterface $service)
    {
        $this->transaction->load('details');

        $service->revertFromTransaction($this->transaction);
    }
}