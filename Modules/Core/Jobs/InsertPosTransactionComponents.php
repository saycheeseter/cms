<?php

namespace Modules\Core\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\Product;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;

class InsertPosTransactionComponents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transaction;

    public $tries = 5;

    public $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @param PosSales $model
     */
    public function __construct(PosSales $model)
    {
        $this->transaction = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->transaction->load('details');

        foreach ($this->transaction->details as $detail) {
            if ($detail->product_type !== ProductType::GROUP) {
                continue;
            }

            $product = Product::with('components')->find($detail->product_id);
            $components = [];

            foreach ($product->components as $component) {
                $components[$component->id] = [
                    'qty'        => $component->pivot->qty->innerValue(),
                    'group_type' => $detail->group_type
                ];
            }

            $detail->components()->sync($components);
        }
    }
}