<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockDeliveryServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\StockDeliveryDetail;
use Carbon\Carbon;

class StockDeliveryService extends InvoiceService implements StockDeliveryServiceInterface
{
    protected function models()
    {
        $this->info = StockDelivery::class;
        $this->details = StockDeliveryDetail::class;
    }

    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data),[
            'deliver_to' => $data['deliver_to'],
            'deliver_to_location' => $data['deliver_to_location'],
            'transaction_type' => $data['transaction_type'],
            'reference_id' => $data['reference_id']
        ]);
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformDetail(array $data)
    {
        return array_merge(parent::transformDetail($data), [
            'reference_id' => isset($data['reference_id']) 
                ? $data['reference_id'] 
                : null,
            'transactionable_id' => $data['transactionable_id'] ?? null,
            'transactionable_type' => $data['transactionable_type'] ?? null
        ]);
    }
}