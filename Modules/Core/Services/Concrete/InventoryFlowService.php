<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Traits\DatabaseTransaction;
use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Model;
use Exception;

abstract class InventoryFlowService
{
    use DatabaseTransaction;

    protected $info;
    protected $details;
    protected $app;
    protected $events = [];

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->models();
        $this->checkModels();
    }

    /**
     * Store data to parent and child models
     *
     * @param  array  $attributes
     * @return Model
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $transaction = $that->createOrUpdate($attributes);
            $that->syncComponents($attributes, $transaction->details);
            return $transaction->info;
        });
    }

    /**
     * Create multiple transactions
     * 
     * @param  array $transactions
     * @return Collection
     */
    public function bulkStore($transactions)
    {
        $that = $this;

        return $this->transaction(function() use ($transactions, $that) {

            $results = [];

            foreach ($transactions as $key => $transaction) {
                $results[] = $this->createOrUpdate($transaction);
            }

            return collect($results);
        });
    }

    /**
     * Update or create a new transaction record (Not wrapped inside a database transaction instance)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Object
     */
    public function createOrUpdate($attributes, $id = 0)
    {
        $attributes[$this->parentRootKey()] = $this->transformInfo($attributes[$this->parentRootKey()]);

        $info = $this->app->make($this->info)->firstOrNew(['id' => $id], $attributes[$this->parentRootKey()]);

        $info->fill($attributes[$this->parentRootKey()])->save();

        $collection = [];
        // Loop through each data, pull only the needed data and create
        // a new instance of the child model

        foreach($attributes[$this->childRootKey()] as $data) {
            $collection[] = $this->transformDetail($data);
        }

        $changes = call_user_func(array($info, $this->relationship()))->sync($collection);

        return (object) [
            'info' => $info->fresh(),
            'details' => $changes
        ];
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Model
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $transaction = $that->createOrUpdate($attributes, $id);
            $that->syncComponents($attributes, $transaction->details);
            return $transaction->info;
        });
    }

    public function updateApproved($attributes, $id)
    {
        $attributes = $this->transformUpdateApprovedInfo($attributes);

        $info = $this->app->make($this->info)->find($id);

        $info->fill($attributes)->save();

        return $info;
    }

    /**
     * Delete parent model (Also deletes relationship model)
     * If there are other models to be deleted aside from the relationship,
     * leverage the deleting process using an observer.
     *
     * @param  integer $id
     * @return bool
     */
    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = $that->app->make($that->info)->findOrFail($id);

            $info->delete();

            call_user_func(array($info, $that->relationship()))->delete();

            return $info;
        });
    }

    /**
     * Updates the multiple|single transaction's current status
     *
     * @param  array|int $transactions
     * @param  int $status
     * @return int
     */
    public function approval($transactions, $status)
    {
        if (!is_array($transactions)) {
            $transactions = [$transactions];
        }

        $transactions = $this->app->make($this->info)->findMany($transactions);
        $that = $this;

        $this->transaction(function() use ($transactions, $status, $that) {
            foreach ($transactions as $key => $transaction) {
                $currentStatus = $transaction->approval_status;

                $transaction->approval_status = $status;
                $transaction->save();

                switch ($status) {
                    case ApprovalStatus::APPROVED:
                        if (array_key_exists('approved', $that->events)) {
                            event(new $that->events['approved']($transaction));
                        }
                        break;

                    case ApprovalStatus::DRAFT:
                        if ($currentStatus === ApprovalStatus::APPROVED
                            && array_key_exists('revert', $that->events)
                        ) {
                            event(new $that->events['revert']($transaction));
                        }
                        break;
                }
            }
        });

        return count($transactions);
    }

    protected function checkModels()
    {
        $model = $this->app->make($this->info);

        if (!$model instanceof Model) {
            throw new Exception(sprint("
                Class %s must be an instance of Illuminate\\Database\\Eloquent\\Model
            ", get_class($model)));
        }

        $model = $this->app->make($this->details);

        if (!$model instanceof Model) {
            throw new Exception(sprint("
                Class %s must be an instance of Illuminate\\Database\\Eloquent\\Model
            ", get_class($model)));
        }
    }

    protected function syncComponents($attributes, $details)
    {   
        $attached = clone $details['attached'];

        if ($attached->count() === 0) {
           return;
        }

        $filtered = array_where($attributes[$this->childRootKey()], function ($value, $key) {
            return $value['id'] == 0;
        });

        foreach ($filtered as $key => $detail) {
            $model = $attached->first();

            $attached->pull(0);
            $attached = $attached->values();

            if(!isset($detail['components']) || count($detail['components']) == 0) {
                continue;
            }

            $collection = $this->transformComponents($detail);

            if(!is_null($model)) {
                $model->components()->sync($collection);
            }
        }
    }

    protected function transformComponents($attributes)
    {
        $collection = [];

        foreach ($attributes['components'] as $key => $component) {
            $collection[$component['product_id']] = [
                'qty' => $component['qty'],
                'group_type' => $component['group_type']
            ];
        }

        return $collection;
    }

    /**
     * Relationship to be accessed by the parent table to save collection
     *
     * @return string
     */
    protected function relationship()
    {
        return 'details';
    }

    /**
     * Relationship id to accessed inside the array
     *
     * @return string
     */
    protected function relationshipKey()
    {
        return 'id';
    }

    /**
     * Key to be used upon accessing the parent data in the array
     *
     * @return string
     */
    protected function parentRootKey()
    {
        return 'info';
    }

    /**
     * Key to be used upon accessing the child data in the array
     *
     * @return string
     */
    protected function childRootKey()
    {
        return 'details';
    }

    abstract protected function transformInfo(array $data);
    abstract protected function transformDetail(array $data);
    abstract protected function transformUpdateApprovedInfo($data);
    abstract protected function models();
}
