<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\PaperServiceInterface;
use Illuminate\Support\Str;

class PaperService implements PaperServiceInterface
{
    private $size;
    private $isHalf;
    private $orientation;

    public function set($size, $orientation)
    {
        $this->size = $size;
        $this->orientation = $orientation;

        return $this;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;

        return $this;
    }

    public function getDimension()
    {
        $this->parseSize();

        $paper = new $this->size;

        $dimension = $this->getDimensionsByPaperAndOrientation($paper);

        return sprintf("%scm %scm", $dimension->width, $dimension->height);
    }

    private function getDimensionsByPaperAndOrientation($paper)
    {
        $width = $paper->getWidth();
        $height = $paper->getHeight();

        switch ($this->orientation) {
            case 'portrait':
                if ($this->isHalf) {
                    $height = $paper->getHeight() / 2;
                }
                break;

            case 'landscape':
                $width = $paper->getHeight();
                $height = $paper->getWidth();

                if ($this->isHalf) {
                    $width = $width / 2;
                }
                break;
        }

        return (object) [
            'width' => $width,
            'height' => $height
        ];
    }

    private function parseSize()
    {
        if (Str::contains($this->size, 'half')) {
            $this->size = Str::replaceFirst('-half', '', $this->size);
            $this->isHalf = true;
        }

        $this->size = sprintf("\Modules\Core\Services\Concrete\Printer\Paper\%s", Str::ucfirst($this->size));

        return $this;
    }
}