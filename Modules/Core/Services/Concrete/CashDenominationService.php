<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\CashDenominationServiceInterface;
use Modules\Core\Entities\CashDenomination;

class CashDenominationService extends BaseService implements CashDenominationServiceInterface
{
    protected function model()
    {
        return CashDenomination::class;
    }

    protected function transform($data)
    {
        $returned =  array_only($data, [
            'created_from',
            'created_by',
            'terminal_number',
            'create_date',
            'b1000' ,
            'b500' ,
            'b200' ,
            'b100' ,
            'b50' ,
            'b20' ,
            'c10' ,
            'c5' ,
            'c1' ,
            'c25c' ,
            'c10c' ,
            'c5c' ,
            'type'
        ]);

        $returned['create_date'] = Carbon::parse($returned['create_date']);

        return $returned;
    }
}