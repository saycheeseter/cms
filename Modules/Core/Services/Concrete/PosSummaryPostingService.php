<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\PosSummaryPosting;
use Modules\Core\Enums\Voided;
use Modules\Core\Jobs\GenerateProductMonthlySummaryFromTransactions;
use Modules\Core\Services\Contracts\AutoProductConversionServiceInterface;
use Modules\Core\Services\Contracts\PosSummaryPostingServiceInterface;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use App;

class PosSummaryPostingService implements PosSummaryPostingServiceInterface
{
    use DatabaseTransaction;

    public function generateSummary($id, $date)
    {
        return $this->transaction(function() use ($id, $date) {
            $transactionMovingAverageCostService = App::make(TransactionMovingAverageCostServiceInterface::class);
            $branchSummaryService = App::make(ProductBranchSummaryServiceInterface::class);
            $productTransactionSummaryService = App::make(ProductTransactionSummaryServiceInterface::class);
            $productStockCardService = App::make(ProductStockCardServiceInterface::class);
            $autoProductConversion = App::make(AutoProductConversionServiceInterface::class);

            $posting = PosSummaryPosting::find($id);

            $date = Carbon::parse($date);

            $builder = PosSales::with(['details'])
                ->where('terminal_number', $posting->terminal_number)
                ->where('created_for', $posting->branch_id)
                ->where('voided', Voided::NO)
                ->where(function($query) use ($date, $posting) {
                    if(!is_null($posting->last_post_date)) {
                        $query->where('transaction_date', '>=', $posting->last_post_date);
                    }

                    $query->where('transaction_date', '<=', $date);
                });

            //Cursor implementation
            foreach ($builder->cursor() as $posSale) {
                $transactionMovingAverageCostService->updateTransactionDetails($posSale);
                $branchSummaryService->computeInventory($posSale);
                $productTransactionSummaryService->summarizeTransaction($posSale);
                $productStockCardService->create($posSale);
                $autoProductConversion->processFromTransaction($posSale);
                dispatch((new GenerateProductMonthlySummaryFromTransactions($posSale))->onQueue('high'));
            }

            //Chuck implementation
//            PosSales::with(['details'])
//                ->where('terminal_number', $posting->terminal_number)
//                ->where('created_for', $posting->branch_id)
//                ->whereBetween('transaction_date', [
//                    $posting->last_post_date,
//                    $date
//                ])
//                ->chunk(50, function($posSales) use (
//                    $transactionMovingAverageCostService,
//                    $branchSummaryService,
//                    $productTransactionSummaryService,
//                    $productStockCardService
//                ) {
//                    foreach ($posSales as $posSale) {
//                        $transactionMovingAverageCostService->updateTransactionDetails($posSale);
//                        $branchSummaryService->computeInventory($posSale);
//                        $productTransactionSummaryService->summarizeTransaction($posSale);
//                        $productStockCardService->create($posSale);
//                    }
//                });

            $posting->last_post_date = $date;
            $posting->save();

            return $posting;
        });
    }
}