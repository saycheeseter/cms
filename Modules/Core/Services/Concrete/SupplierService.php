<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\SupplierServiceInterface;
use Modules\Core\Entities\Supplier;

class SupplierService extends BaseService implements SupplierServiceInterface
{
    protected function model()
    {
        return Supplier::class;
    }
}