<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ProductStockCardServiceInterface;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Entities\ProductStockCard;
use Modules\Core\Entities\InventoryChain;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\SalesReturnInboundDetail;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\ProductBranchSummary;
use Modules\Core\Entities\PosSales;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Enums\TransactionSummary;
use InvalidArgumentException;
use Modules\Core\Enums\GroupType;
use Modules\Core\Traits\HasMorphMap;
use App;

class ProductStockCardService implements ProductStockCardServiceInterface
{   
    use HasMorphMap;

    protected $model;

    /**
     * Create a new record for the product stock card table
     * 
     * @param  InventoryChain $model
     * @return $this
     */
    public function create($model)
    {   
        $this->setModel($model);

        if (!is_subclass_of($this->model, InventoryChain::class)
            && ! $this->model instanceof InventoryAdjust
            && ! $this->model instanceof PosSales
        ) {
            throw new InvalidArgumentException('CreateOrUpdate accepts only a subclass of InventoryChain');
        }

        $attributes = [
            'created_for' => $this->model->created_for,
            'for_location' => $this->model->for_location,
            'reference_type' => $this->getTransactionTypeModel(),
            'reference_id' => $this->model->id,
            'transaction_date' => $this->model->transaction_date,
        ];

        $details = $this->model->details;
        $precision = setting('monetary.precision');

        foreach ($details as $detail) {
            //if(!in_array($detail->group_type,[GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP])) {
                $previous = $this->getPreviousCard($detail->product_id, $attributes);

                $attributes = array_merge(
                    $attributes, [
                        'product_id' => $detail->product_id,
                        'qty' => $detail->added_inventory_value->mul($this->model->multiplier, $precision),
                        'price' => $detail->price,
                        'cost' => $detail->ma_cost
                    ],
                    $previous
                );

                $attributes['amount'] = $attributes['cost']->mul($attributes['qty'], $precision);

                if(! is_null($previous['previous_id'])) {
                    $attributes = array_merge($attributes, $this->postFieldComputations($attributes, $previous));
                } else {
                    $attributes = array_merge($attributes, [
                        'post_inventory' => $attributes['qty'],
                        'post_amount' => $attributes['amount']
                    ]);

                    $attributes['post_avg_cost'] = $this->postAvgComputation($attributes['qty'], $previous, $attributes);
                }

                ProductStockCard::create($attributes);
            //}
        }

        return $this;
    }

    /**
     * Create a new record for the product stock card table from Product Conversion table
     * 
     * @param  Product Conversion $model
     * @return $this
     */
    public function createFromProductConversion($model)
    {   
        $this->setModel($model);

        $precision = setting('monetary.precision');
        
        $attributes = [
            'created_for' => $this->model->created_for,
            'for_location' => $this->model->for_location,
            'reference_type' => $this->getTransactionTypeModel(),
            'reference_id' => $this->model->id,
            'transaction_date' => $this->model->transaction_date,
        ];

        $positive = Decimal::create(1, $precision);
        $negative = Decimal::create(-1, $precision);

        switch ($model->group_type) {
            case GroupType::MANUAL_GROUP:
                $this->createFromConversionHead($attributes, $positive);
                $this->createFromConversionDetail($attributes, $negative);
                break;

            case GroupType::MANUAL_UNGROUP:
                $this->createFromConversionHead($attributes, $negative);
                $this->createFromConversionDetail($attributes, $positive);
                break;

            case GroupType::AUTOMATIC_GROUP:
            case GroupType::AUTOMATIC_UNGROUP:
                $multiplier = $model->process_flow === 'auto_product_conversion_increase'
                    ? $positive
                    : $negative;

                $this->createFromConversionHead($attributes, $multiplier);

                $multiplier = $model->process_flow === 'auto_product_conversion_increase'
                    ? $negative
                    : $positive;

                $this->createFromConversionDetail($attributes, $multiplier);
                break;
        }

        return $this;
    }

    /**
     * set model 
     * @param $model entity
     */
    private function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * create stock card record from head of product conversion
     * @param  array $attributes
     * @param  Decimal $multiplier
     */
    private function createFromConversionHead($attributes, $multiplier) 
    {
        $precision = setting('monetary.precision');

        $previous = $this->getPreviousCard($this->model->product_id, $attributes);

        $attributes = array_merge(
            $attributes, [
                'product_id' => $this->model->product_id,
                'qty' => $this->model->qty->mul($multiplier, $precision),
                'price' => $this->model->ma_cost,
                'cost'  => $this->model->ma_cost,
            ],
            $previous
        );

        $attributes['amount'] = $attributes['cost']->mul($attributes['qty'], $precision);

        if(! is_null($previous['previous_id'])) {
            $attributes = array_merge($attributes, $this->postFieldComputations($attributes, $previous));
        } else {
            $attributes = array_merge($attributes, [
                'post_inventory' => $attributes['qty'],
                'post_amount' => $attributes['amount']
            ]);

            $attributes['post_avg_cost'] = $this->postAvgComputation($attributes['qty'], $previous, $attributes, true);
        }

        $card = ProductStockCard::create($attributes);

        if($card) {
            App::make(ProductBranchSummaryService::class)->updateCurrentCostFromStockCard($card);
        }
    }

    /**
     * create stock card record from head of product conversion
     * @param  array $attributes
     * @param  Decimal $multiplier
     */
    private function createFromConversionDetail($attributes, $multiplier) 
    {   
        $precision = setting('monetary.precision');

        foreach ($this->model->details as $detail) {
            
            $previous = $this->getPreviousCard($detail->component_id ?? $detail->product_id, $attributes);

            $attributes = array_merge(
                $attributes, [
                    'product_id' => $detail->component_id ?? $detail->product_id,
                    'qty' => $detail->required_qty->mul($this->model->qty, $precision)->mul($multiplier, $precision)
                ],
                $previous
            );

            $attributes['price'] = $this->getCurrentCostPBS($attributes['product_id']);
            $attributes['cost'] = $this->getCurrentCostPBS($attributes['product_id']);
            $attributes['amount'] = $attributes['cost']->mul($attributes['qty'], $precision);

            if(! is_null($previous['previous_id'])) {
                $attributes = array_merge($attributes, $this->postFieldComputations($attributes, $previous));
            } else {
                $attributes = array_merge($attributes, [
                    'post_inventory' => $attributes['qty'],
                    'post_amount' => $attributes['amount']
                ]);

                $attributes['post_avg_cost'] = $this->postAvgComputation($attributes['qty'], $previous, $attributes);
            }

            ProductStockCard::create($attributes);
        }
    }

    /**
     * Fetch the previous stock card of the corresponding product in the certain branch and location
     * 
     * @param  int $productId
     * @param  array $attributes
     * @return array
     */
    private function getPreviousCard($productId, $attributes)
    {
        $previous = ProductStockCard::where('product_id', $productId)
            ->where('created_for', '=', $attributes['created_for'])
            ->where('for_location', '=', $attributes['for_location'])
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();

        if(!$previous) {
            $zero = Decimal::create(0, setting('monetary.precision'));
            
            return [
                'previous_id' => null,
                'sequence_no' => 1,
                'pre_inventory' => $zero,
                'pre_avg_cost' => $zero,
                'pre_amount' => $zero
            ];
        }

        return [
            'previous_id' => $previous->id,
            'sequence_no' => $previous->sequence_no + 1,
            'pre_inventory' => $previous->post_inventory,
            'pre_avg_cost' => $previous->post_avg_cost,
            'pre_amount' => $previous->post_amount
        ];
    }

    /**
     * Execute conditions for post field values
     * 
     * @param  array $attributes        Contains current data
     * @return array                    Containing post data
     */
    private function postFieldComputations($attributes, $previous)
    {   
        $precision = setting('monetary.precision');
        $zero = Decimal::create(0, $precision);

        $data['post_inventory'] = $attributes['pre_inventory']->add($attributes['qty'], $precision);
        $data['post_avg_cost'] = $this->postAvgComputation($data['post_inventory'], $previous, $attributes);
        $data['post_amount'] = $data['post_inventory']->mul($data['post_avg_cost'], $precision);

        return $data;
    }

    /**
     * Get new cost value based on current value and previous stock card value
     * 
     * @param  array $attributes
     * @param  array $previous
     * @return Decimal
     */
    private function costComputation($attributes, $previous)
    {   
        $precision = setting('monetary.precision');

        $zero = Decimal::create(0, $precision);

        // If qty is positive, it means that the models are part
        // of an inbound process
        if($attributes['qty']->isGreaterThan($zero)) {
            return $attributes['price']->mul($attributes['qty'], $precision);
        }

        return Decimal::create($previous['pre_avg_cost'], $precision)->mul($attributes['qty'], $precision);
    }

    /**
     * post average computation process
     * @param  decimal $postInventory
     * @param  array $previous
     * @param  array $attributes
     * @return int $postAvgCost
     */
    private function postAvgComputation($postInventory, $previous, $attributes, $isConversionHead = false)
    {   
        $postAvgCost = 0;
        $precision = setting('monetary.precision');
        $zero = Decimal::create(0, $precision);

        if($this->isEitherInstanceOf([
            PurchaseInbound::class,
            StockDeliveryInbound::class,
            StockReturnInbound::class,
            InventoryAdjust::class
        ]) || $isConversionHead
        ) {
            if($postInventory->equals($zero)) {
                $postAvgCost = $zero;
            } else if($previous['pre_inventory']->isLessOrEqualTo($zero)) {
                $postAvgCost = $attributes['price'];
            } else {
                $postAvgCost = $attributes['amount']->add($previous['pre_amount'], $precision)->div($postInventory, $precision);
            }
        } else {
            $postAvgCost = $previous['pre_avg_cost'];
        }

        return $postAvgCost;
    }

    /**
     * get current cost on product branch summary
     * @param  int $productId
     * @return int current cost
     */
    private function getCurrentCostPBS($productId)
    {
        $pbs = ProductBranchSummary::where('product_id', $productId)
            ->where('branch_id', '=', $this->model->created_for)
            ->where('branch_detail_id', '=', $this->model->for_location)
            ->limit(1)
            ->first();

        return Decimal::create($pbs->current_cost ?? 0, setting('monetary.precision'));
    }

    /**
     * Get the corresponding transaction type value based on model
     * 
     * @return int
     */
    private function getTransactionTypeModel()
    {
        if($this->model instanceof PurchaseInbound) {
            return TransactionSummary::PURCHASE_INBOUND;
        } else if($this->model instanceof StockDeliveryInbound) {
            return TransactionSummary::STOCK_DELIVERY_INBOUND;
        } else if($this->model instanceof StockReturnInbound) {
            return TransactionSummary::STOCK_RETURN_INBOUND;
        } else if($this->model instanceof SalesReturnInbound) {
            return TransactionSummary::SALES_RETURN_INBOUND;
        } else if($this->model instanceof PurchaseReturnOutbound) {
            return TransactionSummary::PURCHASE_RETURN_OUTBOUND;
        } else if($this->model instanceof StockDeliveryOutbound) {
            return TransactionSummary::STOCK_DELIVERY_OUTBOUND;
        } else if($this->model instanceof StockReturnOutbound) {
            return TransactionSummary::STOCK_RETURN_OUTBOUND;
        } else if($this->model instanceof SalesOutbound) {
            return TransactionSummary::SALES_OUTBOUND;
        } else if($this->model instanceof DamageOutbound) {
            return TransactionSummary::DAMAGE_OUTBOUND;
        } else if($this->model instanceof InventoryAdjust) {
            return TransactionSummary::INVENTORY_ADJUST;
        } else if($this->model instanceof PosSales) {
            return TransactionSummary::POS_SALES;
        } else if($this->model instanceof ProductConversion) {
            return TransactionSummary::PRODUCT_CONVERSION;
        } else if($this->model instanceof AutoProductConversion) {
            return TransactionSummary::AUTO_PRODUCT_CONVERSION;
        }
    }

    /**
     * checker if instance belongs to set of instances
     * @param  array  $instances
     * @return boolean
     */
    private function isEitherInstanceOf($instances)
    {   
        foreach ($instances as $instance) {
            if($this->model instanceof $instance) {
                return true;
            }
        }

        return false;
    }
}