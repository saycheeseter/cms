<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ModuleSeriesServiceInterface;
use Modules\Core\Entities\Contracts\ModuleSeries as ModuleSeriesInterface;
use Modules\Core\Entities\ModuleSeries;

class ModuleSeriesService implements ModuleSeriesServiceInterface
{

    public function execute(ModuleSeriesInterface $model) 
    {
        $record = ModuleSeries::where('type', $model->series_type)
            ->where('column', $model->series_column)
            ->lockForUpdate()
            ->first();

        $column = $model->series_column;
        $value = trim($model->$column);

        if(($value == '' || is_null($value)) && !$model->exists) {
            $model->$column = $record->series;
            $this->incrementSeries($record);
        } else if(
            is_numeric($value)
            && $record->series <= $value
            && bccomp($record->limit, $value) == 1
        ) {
            $record->series = bcadd($value, 1);
            $record->save();
        }
    }

    private function incrementSeries(ModuleSeries $record)
    {
        $record->series = bcadd($record->series, 1);
        $record->save();
    }
}