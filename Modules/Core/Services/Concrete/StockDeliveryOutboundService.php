<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockDeliveryOutboundServiceInterface;
use Modules\Core\Services\Concrete\InventoryChainService;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockDeliveryOutboundDetail;
use Modules\Core\Entities\StockDeliveryOutboundSerialNumber;

class StockDeliveryOutboundService extends InventoryChainService implements StockDeliveryOutboundServiceInterface
{
    protected function models()
    {
        $this->info = StockDeliveryOutbound::class;
        $this->details = StockDeliveryOutboundDetail::class;
        $this->serial = StockDeliveryOutboundSerialNumber::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'deliver_to' => $data['deliver_to'],
            'deliver_to_location' => $data['deliver_to_location'],
        ]);
    }
}
