<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\LicenseApiServiceInterface;
use App;

class LicenseApiService extends ApiService implements LicenseApiServiceInterface
{
    public function isValidKey($key)
    {
        $route = sprintf('clients/%s/license-key/validate', config('management.client_id'));
        
        $responseData = $this->api
            ->setMethod('GET')
            ->setRoute($route)
            ->setParameters(['key' => $key])
            ->request()
            ->getJsonDecodeResponse();

        return $responseData['isSuccessful'];
    }

    public function activate($model)
    {
        $data = [
            'key' => $model->license_key,
            'user' => [
                'user_id' => $model->id,
                'username' => $model->username,
                'full_name' => $model->full_name
            ]
        ];

        $route = sprintf('clients/%s/license-key/activate', config('management.client_id'));
        
        return $this->api
            ->setMethod('PATCH')
            ->setRoute($route)
            ->setParameters($data)
            ->request()
            ->getJsonDecodeResponse();
    }

    public function verify($key)
    {
        $route = sprintf('clients/%s/license-key/verify', config('management.client_id'));

        return $this->api
            ->setMethod('GET')
            ->setRoute($route)
            ->setParameters(['key' => $key])
            ->request()
            ->getJsonDecodeResponse();
    }

    public function deactivate($key)
    {
        $route = sprintf('clients/%s/license-key/deactivate', config('management.client_id'));

        return $this->api
            ->setMethod('PUT')
            ->setRoute($route)
            ->setParameters(['key' => $key])
            ->request()
            ->getJsonDecodeResponse();
    }
}