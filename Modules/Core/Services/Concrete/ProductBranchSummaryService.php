<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface;
use Modules\Core\Entities\ProductBranchSummary;
use Modules\Core\Entities\BranchDetail;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\ProductStockCard;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Enums\GroupType;
use App;

class ProductBranchSummaryService implements ProductBranchSummaryServiceInterface
{
    /**
     * Create initial data in product branch summary table
     *
     * @param  BranchDetail  $detail
     */
    public function createInitialData(BranchDetail $detail)
    {
        $limit = 299;

        for ($i = 0;;$i++) {
            $products = Product::skip(($i * $limit))
                ->take($limit)
                ->get();

            if ($products->isEmpty()) {
                break;
            }

            $collection = [];

            foreach ($products as $product) {
                $collection[] = [
                    'branch_id'         => $detail->branch_id,
                    'branch_detail_id'  => $detail->id,
                    'product_id'        => $product->id,
                    'qty'               => 0,
                    'reserved_qty'      => 0,
                    'requested_qty'     => 0,
                    'current_cost'      => 0,
                ];
            }

            ProductBranchSummary::insert($collection);
        }
    }

    /**
     * Computes the inventory of the product by iterating to the
     * entire products inside the transaction
     * 
     * @param  InventoryChain $model
     */
    public function computeInventory($model)
    {
        foreach ($model->details as $key => $detail) {
            if(in_array($detail->group_type, [
                GroupType::AUTOMATIC_GROUP,
                GroupType::AUTOMATIC_UNGROUP])
            ) {
                continue;
            } else {
                $this->computeRegularProduct($model, $detail);
            }
        }
    }

    private function computeRegularProduct($model, $detail)
    {
        $qty = $detail->added_inventory_value
            ->mul($model->multiplier,  setting('setting.monetary'))
            ->innerValue();

        $this->increment($model, $detail->product_id, $qty);
    }

    private function revertRegularProduct($model, $detail)
    {
        $qty = $detail->added_inventory_value
            ->mul($model->multiplier,  setting('setting.monetary'))
            ->innerValue();

        $this->decrement($model, $detail->product_id, $qty);
    }

    private function increment($model, $productId, $qty)
    {
        $inventory = ProductBranchSummary::take(1)
            ->where('product_id', $productId)
            ->where('branch_id', $model->created_for)
            ->where('branch_detail_id', $model->for_location);

        $inventory->increment('qty', $qty);
    }

    private function decrement($model, $productId, $qty)
    {
        $inventory = ProductBranchSummary::take(1)
            ->where('product_id', $productId)
            ->where('branch_id', $model->created_for)
            ->where('branch_detail_id', $model->for_location);

        $inventory->decrement('qty', $qty);
    }

    /**
     * Compute the reserved/requested column depending on the model
     * class
     * 
     * @param  Invoice $model
     */
    public function computeTransactionPendingQuantity($model)
    {
        foreach ($model->details as $key => $detail) {
            $this->incrementPendingQuantity($detail, $detail->total_qty);
        }
    }

    /**
     * Add|subtract qty to reserved/requested column depending on the model
     * class
     *
     * @param  InvoiceDetail $model
     * @param $qty
     * @param string $method
     */
    public function computePendingQuantity($model, $qty, $method = 'increment')
    {
        $invoice = $model->transaction;

        $column = $this->getPendingQuantityColumnByModel($invoice);

        $inventory = ProductBranchSummary::take(1)
            ->where('product_id', $model->product_id)
            ->where('branch_id', $invoice->created_for)
            ->where('branch_detail_id', $invoice->for_location);

        call_user_func_array(array($inventory, $method), array(
            $column, 
            $qty->innerValue()
        ));
    }

    /**
     * Shortcut api for adding pending quantity to product branch inventory
     * 
     * @param InvoiceDetail $model
     */
    public function incrementPendingQuantity($model, $qty)
    {
        return $this->computePendingQuantity($model, $qty);
    }

    /**
     * Shortcut api for subtracting pending quantity to product branch inventory
     * 
     * @param InvoiceDetail $model
     */
    public function decrementPendingQuantity($model, $qty)
    {
        return $this->computePendingQuantity($model, $qty, 'decrement');
    }

    /**
     * Update current cost of branch summary
     *
     * @param ProductStockCard $card
     */
    public function updateCurrentCostFromStockCard(ProductStockCard $card)
    {   
        $model = App::make(ProductBranchSummary::class)
            ->where('product_id', $card->product_id)
            ->where('branch_id', $card->created_for)
            ->where('branch_detail_id', $card->for_location)
            ->update(['current_cost' => $card->post_avg_cost]);
    }

    /**
     * Determine the column to be used upon updating the product's
     * reserved or requested qty
     * 
     * @param  Invoice $model
     * @return int
     */
    private function getPendingQuantityColumnByModel($model)
    {
        return in_array(get_class($model), array(
            SalesReturn::class,
            Purchase::class,
            StockRequest::class,
        )) ? 'requested_qty' : 'reserved_qty';
    }

    public function manualConversion(ProductConversion $model)
    {
        switch ($model->group_type) {
            case GroupType::MANUAL_GROUP:
                $this->manualGroupConversion($model);
                break;

            case GroupType::MANUAL_UNGROUP:
                $this->manualUngroupConversion($model);
                break;
        }
    }

    public function manualGroupConversion(ProductConversion $model)
    {
        return $this->processConversion($model, 'increment', 'decrement');
    }

    public function manualUngroupConversion(ProductConversion $model)
    {
        return $this->processConversion($model, 'decrement', 'increment');
    }

    private function processConversion(ProductConversion $model, $groupMethod = 'increment', $componentMethod = 'decrement')
    {
        $group = ProductBranchSummary::take(1)
                ->where('product_id', $model->product_id)
                ->where('branch_id', $model->created_for)
                ->where('branch_detail_id', $model->for_location);

        $group->$groupMethod('qty', $model->qty->innerValue());

        foreach ($model->details as $key => $detail) {
            $component = ProductBranchSummary::take(1)
                ->where('product_id', $detail->component_id)
                ->where('branch_id', $model->created_for)
                ->where('branch_detail_id', $model->for_location);

            $component->$componentMethod('qty', 
                $detail->required_qty->mul(
                    $model->qty, 
                    setting('setting.monetary')
                )->innerValue());
        }
    }

    public function autoConversion(AutoProductConversion $model)
    {
        $this->processAutoConversion($model, 'increment');
    }

    public function deleteFromBranchDetail(BranchDetail $detail) 
    {
        ProductBranchSummary::where('branch_id', $detail->branch->id)
            ->where('branch_detail_id', $detail->id)
            ->delete();
    }

    public function revertTransactionPendingQuantity($model)
    {
        foreach ($model->details as $key => $detail) {
            $this->decrementPendingQuantity($detail, $detail->total_qty);
        }
    }

    public function revertInventory($model)
    {
        foreach ($model->details as $key => $detail) {
            if(in_array($detail->group_type, [
                GroupType::AUTOMATIC_GROUP,
                GroupType::AUTOMATIC_UNGROUP])
            ) {
                continue;
            } else {
                $this->revertRegularProduct($model, $detail);
            }
        }
    }

    public function revertAutoConversion(AutoProductConversion $model)
    {
        $this->processAutoConversion($model, 'decrement');
    }

    private function processAutoConversion(AutoProductConversion $model, $method)
    {
        $multiplier = $model->transaction->transaction->multiplier;

        foreach ($model->details as $key => $detail) {
            $component = ProductBranchSummary::take(1)
                ->where('product_id', $detail->product_id)
                ->where('branch_id', $model->created_for)
                ->where('branch_detail_id', $model->for_location);

            $component->$method('qty',
                $detail->required_qty->mul(
                    $model->qty,
                    setting('setting.monetary')
                )->mul(
                    $multiplier,
                    setting('setting.monetary')
                )->innerValue()
            );
        }
    }

    public function revertManualConversion(ProductConversion $model)
    {
        switch ($model->group_type) {
            case GroupType::MANUAL_GROUP:
                $this->revertManualGroupConversion($model);
                break;

            case GroupType::MANUAL_UNGROUP:
                $this->revertManualUngroupConversion($model);
                break;
        }
    }

    private function revertManualGroupConversion($model)
    {
        return $this->processConversion($model, 'decrement', 'increment');
    }

    private function revertManualUngroupConversion($model)
    {
        return $this->processConversion($model, 'increment', 'decrement');
    }
}
