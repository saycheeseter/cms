<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\BrandServiceInterface;
use Modules\Core\Entities\Brand;

class BrandService extends BaseService implements BrandServiceInterface
{
    protected function model()
    {
        return Brand::class;
    }
}