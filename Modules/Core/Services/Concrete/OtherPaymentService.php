<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\OtherPaymentServiceInterface;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Entities\OtherPaymentDetail;

class OtherPaymentService extends OtherFinancialFlowService implements OtherPaymentServiceInterface
{
    protected function models()
    {
        return [
            'info'    => OtherPayment::class,
            'details' => OtherPaymentDetail::class,
        ];
    }
}