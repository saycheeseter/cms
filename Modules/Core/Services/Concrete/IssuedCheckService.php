<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\IssuedCheckServiceInterface;
use Modules\Core\Entities\IssuedCheck;

class IssuedCheckService extends CheckManagementService implements IssuedCheckServiceInterface
{
    protected function model()
    {
        return IssuedCheck::class;
    }
}