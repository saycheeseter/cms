<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\TransactionSerialNumberLogs;
use Modules\Core\Services\Contracts\TransactionSerialNumberLogsServiceInterface;
use Modules\Core\Traits\HasMorphMap;

class TransactionSerialNumberLogsService implements TransactionSerialNumberLogsServiceInterface
{
    use HasMorphMap;

    public function log($model)
    {
        $serials = [];

        foreach ($model->details as $key => $detail) {
            $serials = array_merge($serials,  $detail->serials->pluck('id')->toArray());
        }

        if (count($serials) === 0) {
            return false;
        }

        $collection = [];

        // We will use query builder for more performance
        foreach ($serials as $serial) {
            $collection[] = [
                'serial_id' => $serial,
                'transaction_id' => $model->id,
                'transaction_type' => $this->getMorphMapKey(get_class($model)),
                'approval_date' => $model->audited_date
            ];
        }

        TransactionSerialNumberLogs::insert($collection);

        return $this;
    }

    public function remove($model)
    {
        TransactionSerialNumberLogs::where('transaction_id', $model->id)
            ->where('transaction_type', $this->getMorphMapKey(get_class($model)))
            ->delete();

        return $this;
    }
}