<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\BankServiceInterface;
use Modules\Core\Entities\Bank;

class BankService extends BaseService implements BankServiceInterface
{
    protected function model()
    {
        return Bank::class;
    }
}
