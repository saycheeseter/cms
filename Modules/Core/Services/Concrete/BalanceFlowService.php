<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\PaymentMethod;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;

abstract class BalanceFlowService 
{
    protected $model;

    public function create($attributes) 
    {
        $model = $this->model->fill($attributes)->save();

        return $model;
    }

    public function delete($id, $type)
    {
        $result = $this->model->where([
            'transactionable_id' => $id,
            'transactionable_type' => $type
        ])->delete();
        
        if ($result) {
            return $result;
        }
        
        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $id
        );
    }
}