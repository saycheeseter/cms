<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\TransactionBatchLog;
use Modules\Core\Services\Contracts\TransactionBatchLogServiceInterface;
use Modules\Core\Traits\HasMorphMap;

class TransactionBatchLogService implements TransactionBatchLogServiceInterface
{
    use HasMorphMap;

    public function log($model)
    {
        $batches = [];

        foreach ($model->details as $key => $detail) {
            $batches = array_merge($batches,  $detail->batches->pluck('id')->toArray());
        }

        if (count($batches) === 0) {
            return false;
        }

        $collection = [];

        // We will use query builder for more performance
        foreach ($batches as $batch) {
            $collection[] = [
                'batch_id' => $batch,
                'transaction_id' => $model->id,
                'transaction_type' => $this->getMorphMapKey(get_class($model)),
                'approval_date' => $model->audited_date
            ];
        }

        TransactionBatchLog::insert($collection);

        return $this;
    }

    public function remove($model)
    {
        TransactionBatchLog::where('transaction_id', $model->id)
            ->where('transaction_type', $this->getMorphMapKey(get_class($model)))
            ->delete();

        return $this;
    }
}