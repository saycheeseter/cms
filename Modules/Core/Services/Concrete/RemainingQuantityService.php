<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Contracts\TransactionDetailRemainingReference;
use Modules\Core\Entities\Contracts\TransactionRemainingReference;
use Modules\Core\Services\Contracts\RemainingQuantityServiceInterface;
use Modules\Core\Enums\TransactionStatus;
use Litipk\BigNumbers\Decimal;
use OwenIt\Auditing\Facades\Auditor;

class RemainingQuantityService implements RemainingQuantityServiceInterface
{
    public function deductRemaining(TransactionRemainingReference $model)
    {
        $model->load('details.reference');

        $details = $model->details;

        foreach ($details as $detail) {
            $this->deductDetailRemaining($detail);
        }

        return $this;
    }

    public function deductDetailRemaining(TransactionDetailRemainingReference $model)
    {
        $this->addOrSubDetailRemaining($model, 'sub');
    }

    public function updateTransactionStatus(TransactionRemainingReference $model)
    {
        $model->load('reference.details');

        $reference = $model->reference;

        //order of control structures matter, makes condition correct
        if ($reference->total_qty->equals($reference->total_unsigned_remaining_qty)) {
            $status = TransactionStatus::NO_DELIVERY;
        } else if($reference->total_unsigned_remaining_qty->isGreaterThan(Decimal::create(0, 6))) {
            $status = TransactionStatus::INCOMPLETE;
        } else if($reference->total_signed_remaining_qty->equals($reference->total_unsigned_remaining_qty)) {
            $status = TransactionStatus::COMPLETE;
        } else {
            $status = TransactionStatus::EXCESS;
        }

        $reference->transaction_status = $status;

        $reference->save();

        return $this;
    }

    public function addRemaining(TransactionRemainingReference $model)
    {
        $model->load('details.reference');

        $details = $model->details;

        foreach ($details as $detail) {
            $this->addDetailRemaining($detail);
        }

        return $this;
    }

    public function addDetailRemaining(TransactionDetailRemainingReference $model)
    {
        $this->addOrSubDetailRemaining($model, 'add');
    }

    private function addOrSubDetailRemaining(TransactionDetailRemainingReference $model, $method)
    {
        $reference = $model->reference;
        $reference->remaining_qty = $reference->remaining_qty->$method($model->total_qty);

        if ($reference->remaining_qty->isGreaterThan($reference->total_qty)) {
            $reference->remaining_qty = $reference->total_qty;
        }

        //$reference->save();

        $reference->getQuery()->where('id', $reference->id)
            ->update([
                'remaining_qty' => $reference->remaining_qty
            ]);
    }
}
