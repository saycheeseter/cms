<?php

namespace Modules\Core\Services\Concrete;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\ProductBatchOwner;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\BatchStatus;
use Modules\Core\Services\Contracts\ProductBatchServiceInterface;
use Modules\Core\Entities\ProductBatch;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\Contracts\BatchTransaction;
use DB;

class ProductBatchService implements ProductBatchServiceInterface
{
    /**
     * Create or Update batches based on the Purchase Inbound data
     * 
     * @param  PurchaseInbound $transaction
     * @param  int $product
     * @param  array $batches
     * @return array
     */
    public function createOrUpdateFromTransaction(PurchaseInbound $transaction, $product, $batches)
    {
        $models = [];

        foreach ($batches as $batch) {
            $model = ProductBatch::updateOrCreate(
                ['id' => $batch['id'] ?? 0],
                 $this->transformBatch($batch)
                + ['product_id' => $product, 'supplier_id' => $transaction->supplier_id]
            );

            $models[] = $model;
        }

        return collect($models);
    }

    /**
     * Create or Update the qty of the specified batch owner based on the model info
     *
     * @param  BatchTransaction $transaction
     * @return $this|bool
     */
    public function createOrUpdateBatchOwnerFromTransaction(BatchTransaction $transaction)
    {
        $this->processBatchOwnerFromTransaction($transaction, 'update');

        return $this;
    }

    /**
     * Update all of the batches from the Purchase Inbound to available
     *
     * @param PurchaseInbound $transaction
     * @return bool
     */
    public function setAvailableStatusFromPurchaseInbound(PurchaseInbound $transaction)
    {
        $this->updateStatusFromPurchaseInbound($transaction, BatchStatus::AVAILABLE);
    }

    /**
     * Transform batch number data
     * 
     * @param  array  $data
     * @return array
     */
    private function transformBatch(array $data)
    {
        return [
            'name' => $data['name'],
            'qty'  => $data['qty'],
        ];
    }

    /**
     * Decrement the qty of the indicated base batch owner of the transaction
     *
     * @param BatchTransaction $transaction
     * @param $batches
     */
    private function decrementBaseBatchOwner(BatchTransaction $transaction, $batches)
    {
        $this->processBaseBatchOwner($transaction, $batches, 'decrement');
    }

    private function incrementBaseBatchOwner(BatchTransaction $transaction, $batches)
    {
        $this->processBaseBatchOwner($transaction, $batches, 'increment');
    }

    private function processBaseBatchOwner(BatchTransaction $transaction, $batches, $method)
    {
        $keys = [
            'owner_type' => $transaction->batch_base_owner_type_assignment,
            'owner_id' => $transaction->batch_base_owner_id_assignment,
            'batch_id' => null,
        ];

        $column = $transaction->batch_base_owner_qty;

        $this->incrementOrDecrementBatchOwner($batches, $keys, $column, $method);
    }

    /**
     * Increment the qty of the indicated base batch owner of the transaction
     *
     * @param BatchTransaction $transaction
     * @param $batches
     */
    private function incrementBatchOwner(BatchTransaction $transaction, $batches)
    {
        $this->processBatchOwner($transaction, $batches, 'increment');
    }

    private function decrementBatchOwner(BatchTransaction $transaction, $batches)
    {
        $this->processBatchOwner($transaction, $batches, 'decrement');
    }

    private function processBatchOwner(BatchTransaction $transaction, $batches, $method)
    {
        $keys = [
            'owner_type' => $transaction->batch_owner_type_assignment,
            'owner_id' => $transaction->batch_owner_id_assignment,
            'batch_id' => null,
        ];

        $column = $transaction->batch_owner_qty;

        $this->incrementOrDecrementBatchOwner($batches, $keys, $column, $method);
    }

    /**
     * Increment or decrement the indicated columns based on the provided keys
     *
     * @param $batches
     * @param $keys
     * @param $column
     * @param $method
     */
    private function incrementOrDecrementBatchOwner($batches, $keys, $column, $method = 'increment')
    {
        foreach ($batches as $key => $qty) {
            $keys['batch_id'] = $key;

            $batch = ProductBatchOwner::where($keys)->first();

            // If no instance of the batch exists, we'll create a new one
            if (!$batch) {
                $qtys = [
                    'current_qty' => 0,
                    'pending_qty' => 0
                ];

                foreach ($qtys as $k => $v) {
                    if ($k !== $column) {
                        continue;
                    }

                    $qtys[$k] = $qty->innerValue();
                }

                ProductBatchOwner::create(array_merge($keys, $qtys));
            } else {
                // If there's an existing data, let's just decrement the current_qty of the base owner
                ProductBatchOwner::where($keys)->$method($column, $qty->innerValue());
            }
        }
    }

    public function setNotAvailableStatusFromPurchaseInbound(PurchaseInbound $transaction)
    {
        $this->updateStatusFromPurchaseInbound($transaction, BatchStatus::NOT_AVAILABLE);
    }

    private function updateStatusFromPurchaseInbound(PurchaseInbound $transaction, $status)
    {
        $batches = [];

        foreach ($transaction->details as $key => $detail) {
            $batches = array_merge($batches,  $detail->batches->pluck('id')->toArray());
        }

        if (count($batches) === 0) {
            return false;
        }

        ProductBatch::whereIn('id', $batches)
            ->update([
                'status' => $status
            ]);
    }

    public function revertBatchOwnerFromTransaction(BatchTransaction $transaction)
    {
        $this->processBatchOwnerFromTransaction($transaction, 'revert');

        return $this;
    }

    private function processBatchOwnerFromTransaction(BatchTransaction $transaction, $process)
    {
        $batches = [];

        // Populate each detail batch as an associative array
        foreach ($transaction->details as $key => $detail) {
            if ($detail->batches->count() === 0) {
                continue;
            }

            foreach ($detail->batches as $batch) {
                if (!array_key_exists($batch->id, $batches)) {
                    $batches[$batch->id] = Decimal::create(0, setting('monetary.precision'));
                }

                $batches[$batch->id] = $batches[$batch->id]->add($batch->pivot->qty);
            }
        }

        if (count($batches) === 0) {
            return false;
        }

        // Batch updating involves two process, decrementing the base owner and adding the qty
        // to the new owner

        // Evaluate first if there is an existing base owner coded in the current Model
        if (!is_null($transaction->batch_base_owner_type_assignment)) {
            switch ($process) {
                case 'update':
                    $this->decrementBaseBatchOwner($transaction, $batches);
                    break;

                case 'revert':
                    $this->incrementBaseBatchOwner($transaction, $batches);
                    break;
            }
        }

        if (!is_null($transaction->batch_owner_type_assignment)) {
            switch ($process) {
                case 'update':
                    $this->incrementBatchOwner($transaction, $batches);
                    break;

                case 'revert':
                    $this->decrementBatchOwner($transaction, $batches);
                    break;
            }
        }

        return $this;
    }
}
