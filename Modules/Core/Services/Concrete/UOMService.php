<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\UOMServiceInterface;
use Modules\Core\Entities\UOM;

class UOMService extends BaseService implements UOMServiceInterface
{
    protected function model()
    {
        return UOM::class;
    }
}