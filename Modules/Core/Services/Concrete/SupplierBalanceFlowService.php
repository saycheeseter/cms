<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\SupplierBalanceFlowServiceInterface;
use Modules\Core\Entities\SupplierBalanceFlow;
use Modules\Core\Entities\Payment;
use Modules\Core\Enums\FlowType;
use App;

class SupplierBalanceFlowService extends BalanceFlowService implements SupplierBalanceFlowServiceInterface
{
    public function __construct(SupplierBalanceFlow $model)
    {
        $this->model = $model;
    }

    /**
     * Formats Payment instance model to a Supplier Balance Flow data by using the from balance data
     * 
     * @param  Payment $model
     * @return SupplierBalanceFlow
     */
    public function fromBalance(Payment $model)
    {
        $attributes = [
            'supplier_id'          => $model->supplier_id,
            'transactionable_id'   => $model->id,
            'transactionable_type' => 'payment',
            'flow_type'            => FlowType::OUT,
            'transaction_date'     => $model->transaction_date,
            'amount'               => $model->total_from_balance,
        ];

        return $this->create($attributes);
    }

    /**
     * Formats Payment instance model to a Supplier Balance Flow data by using the excess amount data
     * 
     * @param  Payment $model
     * @return SupplierBalanceFlow
     */
    public function fromExcess(Payment $model)
    {
        $attributes = [
            'supplier_id'          => $model->supplier_id,
            'transactionable_id'   => $model->id,
            'transactionable_type' => 'payment',
            'flow_type'            => FlowType::IN,
            'transaction_date'     => $model->transaction_date,
            'amount'               => $model->total_excess_amount,
        ];

        return $this->create($attributes);
    }

    /**
     * Shortcut method for deleting a Supplier Balance Flow record using the Payment as the
     * transaction type
     * 
     * @param  int $id
     * @return SupplierBalanceFlow
     */
    public function deleteByPayment($id)
    {
        return $this->delete($id, 'payment');
    }
}