<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Contracts\TransactionReference;

class ReferenceNumberService
{
    /**
     * Generate reference number based on the model
     *
     * @param  TransactionReference $model
     * @return string
     */
    public static function generate(TransactionReference $model)
    {
        return sprintf('%s%s%s',
            $model->sheet_number_prefix,
            self::pad($model->created_from, 3),
            self::pad($model->id, 7)
        );
    }

    /**
     * Pad string to the desired length (Always to the left for reference number)
     * @param  string $string
     * @param  int $length
     * @return string
     */
    public static function pad($string, $length)
    {
        return str_pad($string, $length, '0', STR_PAD_LEFT);
    }
}
