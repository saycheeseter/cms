<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\BranchPrice;
use Modules\Core\Services\Contracts\ProductConversionServiceInterface;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Traits\DatabaseTransaction;
use Carbon\Carbon;
use DB;

class ProductConversionService implements ProductConversionServiceInterface
{
    use DatabaseTransaction;

    /**
     * Store data to parent and child models
     *
     * @param array $attributes
     * @return mixed
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $attributes = $this->attachItemCosts($attributes);
            $transaction = $that->createOrUpdate($attributes);

            return $transaction->info;
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param array $attributes
     * @param $id
     * @return mixed
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $transaction = $that->createOrUpdate($attributes, $id);

            return $transaction->info;
        });
    }

    /**
     * Updates the multiple|single transaction's current status
     *
     * @param $transactions
     * @param $status
     * @return int
     */
    public function approval($transactions, $status)
    {
        if (!is_array($transactions)) {
            $transactions = [$transactions];
        }

        $transactions = ProductConversion::findMany($transactions);

        return $this->transaction(function() use ($transactions, $status) {
            foreach ($transactions as $key => $transaction) {
                $transaction->approval_status = $status;
                $transaction->save();
            }
        });
    }

    /**
     * Update or create a new transaction record (Not wrapped inside a database transaction instance)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Object
     */
    public function createOrUpdate($attributes, $id = 0)
    {
        $attributes['info'] = $this->transformInfo($attributes['info']);

        $info = ProductConversion::firstOrNew(['id' => $id], $attributes['info']);

        $info->fill($attributes['info'])->save();

        $collection = [];
        // Loop through each data, pull only the needed data and create
        // a new instance of the child model

        foreach($attributes['details'] as $data) {
            $collection[] = $this->transformDetail($data);
        }

        $changes = $info->details()->sync($collection);

        return (object) [
            'info' => $info->fresh(),
            'details' => $changes
        ];
    }

    /**
     * Override delete process since conversion detail doesn't have soft delete process
     *
     * @param  integer $id
     * @return bool
     */
    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = ProductConversion::findOrFail($id);

            $info->delete();

            return $info;
        });
    }

    private function attachItemCosts($attributes)
    {
        $costs = $this->getPurchasePriceByBranch(
            $attributes['info']['created_for'] ?? 0,
            array_pluck($attributes['details'], 'component_id')
        );

        foreach($attributes['details'] as $index => $data) {
            $current = $attributes['details'][$index];

            $attributes['details'][$index]['cost'] = $costs->get($current['component_id']);
        }

        return $attributes;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return [
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'created_for'      => $data['created_for'],
            'for_location'     => $data['for_location'],
            'product_id'       => $data['product_id'],
            'qty'              => $data['qty'],
            'group_type'       => $data['group_type'],
            'cost'             => $data['cost']
        ];
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return [
            'id'           => $data['id'] ?? 0,
            'component_id' => $data['component_id'],
            'required_qty' => $data['required_qty'],
            'cost'         => $data['cost']
        ];
    }

    private function getPurchasePriceByBranch($branch, $products)
    {
        $products = array_wrap($products);

        $prices = BranchPrice::whereIn('product_id', $products)
            ->where('branch_id', $branch)
            ->get();

        return $prices->pluck('purchase_price', 'product_id');
    }

}