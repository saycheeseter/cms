<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Admin;
use Modules\Core\Services\Contracts\PermissionServiceInterface;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\ReportBuilder;
use Modules\Core\Entities\Branch;
use App;
use DB;

class PermissionService extends BaseService implements PermissionServiceInterface
{
    protected function model()
    {
        return Permission::class;
    }

    /**
     * Create a new Permission based on the ReportBuilder instance and the passed
     * module id
     *
     * @param ReportBuilder $report
     * @param $moduleId
     */
    public function storeFromReportBuilder(ReportBuilder $report, $moduleId)
    {   
        $permission = $this->store([
            'alias'       => $this->parseSlugAsAlias($report->slug),
            'module_id'   => $moduleId,
            'name'        => $this->parseAsName($report->name),
            'translation' => 'label.view',
            'sequence'    => '1'
        ]);

        $this->insertPermissionsForAdmins($permission->id);
    }

    /**
     * Update the name and alias of permission based on the ReportBuilder instance
     *
     * @param ReportBuilder $report
     */
    public function updateFromReportBuilder(ReportBuilder $report)
    {
        $permission = App::make($this->model())
            ->where('alias', $this->parseSlugAsAlias($report->getOriginal('slug')))
            ->first();

        $this->update([
            'name'  => $this->parseAsName($report->name),
            'alias' => $this->parseSlugAsAlias($report->slug)
        ], $permission->id);
    }

    /**
     * Delete an existing permission based on the ReportBuilder instance
     * Based from slug
     *
     * @param ReportBuilder $report
     */
    public function deleteFromReportBuilder(ReportBuilder $report)
    {
        $permission = App::make($this->model())
            ->where('alias', $this->parseSlugAsAlias($report->getOriginal('slug')))
            ->first();

        DB::table('branch_permission_user')->where('permission_id', $permission->id)->delete();

        $this->delete($permission->id);
    }

    /**
     * Parse string to formatted alias for permission
     *
     * @param  string $subject
     * @param  string $search
     * @param  string $permission
     * @return string
     */
    private function parseSlugAsAlias($subject, $search = '-', $permission = 'view')
    {
        return sprintf('%s_%s', $permission, str_replace($search, '_', $subject));
    }

    private function parseAsName($name)
    {
        return sprintf('View %s', ucwords(strtolower($name)));
    }

    private function insertPermissionsForAdmins($id)
    {
        $branches = Branch::all();
        $admins = Admin::all();

        foreach ($admins as $admin) {
            foreach ($branches as $key => $branch) {
                DB::table('branch_permission_user')->insert([
                    'branch_id' => $branch->id,
                    'permission_id' => $id,
                    'user_id' => $admin->id
                ]);
            }
        }
    }
}