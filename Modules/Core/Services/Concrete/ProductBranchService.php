<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ProductBranchServiceInterface;
use Modules\Core\Entities\ProductBranchInfo;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\Branch;

class ProductBranchService implements ProductBranchServiceInterface
{
    public function createInitialData(Branch $info)
    {
        $limit = 695;

        for ($i = 0;;$i++) {
            $products = Product::skip(($i * $limit))
                ->take($limit)
                ->get();

            if ($products->isEmpty()) {
                break;
            }

            $collection = [];

            foreach ($products as $product) {
                $collection[] = [
                    'branch_id'  => $info->id,
                    'product_id' => $product->id,
                    'status'     => 1
                ];
            }

            ProductBranchInfo::insert($collection);
        }
    }

    public function deleteFromBranch(Branch $branch) 
    {
        ProductBranchInfo::where('branch_id', $branch->id)->delete();
    }
}