<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\PrintoutTemplateServiceInterface;
use Modules\Core\Entities\PrintoutTemplate;

class PrintoutTemplateService extends BaseService implements PrintoutTemplateServiceInterface
{
    protected function model()
    {
        return PrintoutTemplate::class;
    }
}