<?php

namespace Modules\Core\Services\Concrete;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\CustomerCredit;
use Modules\Core\Entities\ReceivedCheck;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Enums\CheckStatus;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;

class CustomerCreditService implements CustomerCreditServiceInterface
{
    public function deductDueFromSalesOutbound(SalesOutbound $model)
    {
        $this->processDueFromSales($model, 'decrement');
    }

    public function addDueFromCollection(Collection $model)
    {
        $this->processDueFromCollection($model, 'increment');
    }

    public function addDueFromSalesOutbound(SalesOutbound $model)
    {
        $this->processDueFromSales($model, 'increment');
    }

    public function deductDueFromCollection(Collection $model)
    {
        $this->processDueFromCollection($model, 'decrement');
    }

    private function processDueFromSales($model, $method)
    {
        $credit = $model->customer->credit;

        if (!$credit) {
            $credit = new CustomerCredit;
            $credit->due = $model->total_amount;
            $model->customer->credit()->save($credit);
        } else {
            $credit->getQuery()
                ->where('customer_id', $model->customer_id)
                ->$method('due', $model->total_amount->innerValue());
        }
    }

    private function processDueFromCollection(Collection $model, $method)
    {
        $amount = Decimal::create(0, setting('monetary.precision'));

        foreach ($model->details as $detail) {
            if ($detail->payment_method === PaymentMethod::CHECK
                && $detail->check->status !== CheckStatus::TRANSFERRED
            ) {
                continue;
            }

            $amount = $amount->add($detail->amount);
        }

        $model->customer->credit->getQuery()
            ->where('customer_id', $model->customer_id)
            ->$method('due', $amount->innerValue());
    }

    public function deductDueFromReceivedCheck(ReceivedCheck $model)
    {
        $customer = $model->transactionable->transaction->customer;

        $customer->credit->getQuery()
            ->where('customer_id', $customer->id)
            ->decrement('due', $model->amount->innerValue());
    }

    public function addDueFromSalesReturnInbound(SalesReturnInbound $model)
    {
        $this->processDueFromSales($model, 'increment');
    }

    public function deductDueFromSalesReturnInbound(SalesReturnInbound $model)
    {
        $this->processDueFromSales($model, 'decrement');
    }
}