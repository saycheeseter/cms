<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\MemberRateServiceInterface;
use Modules\Core\Entities\MemberRate;
use Modules\Core\Entities\MemberRateDetail;

class MemberRateService extends TransactionService implements MemberRateServiceInterface
{
    protected function models()
    {
        return [
            'info'    => MemberRate::class,
            'details' => MemberRateDetail::class
        ];
    }

    /**
     * Transforms parent array
     *
     * @param  array  $data
     * @return array
     */
    protected function parentTransformer(array $data)
    {
        return [
            'name'   => $data['name'],
            'memo'   => $data['memo'],
            'status' => $data['status'],
        ];
    }

    /**
     * Transforms child array
     * 
     * @param  array  $data
     * @return array      
     */
    protected function childTransformer(array $data)
    {
        return [
            'id'               => $data['id'] ?? 0,
            'amount_from'      => $data['amount_from'],
            'amount_to'        => $data['amount_to'],
            'date_from'        => $data['date_from'],
            'date_to'          => $data['date_to'],
            'amount_per_point' => $data['amount_per_point'],
            'discount'         => $data['discount']
        ];
    }
}