<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Services\Contracts\CounterServiceInterface;
use Modules\Core\Entities\Counter;
use Modules\Core\Entities\CounterDetail;

class CounterService extends TransactionService implements CounterServiceInterface
{
    protected function models()
    {
        return [
            'info'    => Counter::class,
            'details' => CounterDetail::class,
        ];
    }

    protected function parentTransformer(array $data)
    {
        return [
            'created_for'      => $data['created_for'],
            'customer_id'      => $data['customer_id'],
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks']
        ];
    }

    protected function childTransformer(array $data)
    {
        return [
            'id'               => $data['id'] ?? 0,
            'reference_id'     => $data['reference_id'],
            'remaining_amount' => $data['remaining_amount']
        ];
    }
}