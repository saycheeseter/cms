<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\CustomerServiceInterface;
use Modules\Core\Entities\Customer;
use Modules\Core\Entities\CustomerDetail;

class CustomerService extends TransactionService implements CustomerServiceInterface
{
    protected function models()
    {
        return [
            'info'    => Customer::class,
            'details' => CustomerDetail::class,
        ];
    }

    /**
     * Transforms parent array
     *
     * @param  array  $data
     * @return array
     */
    protected function parentTransformer(array $data)
    {
        return [
            'code'                     => $data['code'],
            'name'                     => $data['name'],
            'memo'                     => $data['memo'],
            'credit_limit'             => $data['credit_limit'],
            'pricing_type'             => $data['pricing_type'],
            'pricing_rate'             => $data['pricing_rate'],
            'historical_change_revert' => $data['historical_change_revert'],
            'historical_default_price' => $data['historical_default_price'],
            'salesman_id'              => $data['salesman_id'],
            'website'                  => $data['website'],
            'term'                     => $data['term'],
            'status'                   => $data['status'],
        ];
    }

    /**
     * Transforms child array
     *
     * @param  array  $data
     * @return array
     */
    protected function childTransformer(array $data)
    {
        return [
            'id'             => $data['id'] ?? 0,
            'name'           => $data['name'],
            'address'        => $data['address'],
            'chinese_name'   => $data['chinese_name'],
            'owner_name'     => $data['owner_name'],
            'contact'        => $data['contact'],
            'landline'       => $data['landline'],
            'fax'            => $data['fax'],
            'mobile'         => $data['mobile'],
            'email'          => $data['email'],
            'contact_person' => $data['contact_person'],
            'tin'            => $data['tin']
        ];
    }
}
