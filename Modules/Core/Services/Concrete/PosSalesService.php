<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\PosSalesDetail;
use Modules\Core\Entities\PosCollection;
use Modules\Core\Entities\PosCardPayment;
use Modules\Core\Events\PosSalesEvent;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\PaymentMethod;
use App;
use Carbon\Carbon;

use Modules\Core\Services\Contracts\PosSalesServiceInterface;

class PosSalesService implements PosSalesServiceInterface
{
    use DatabaseTransaction;

    public function sync(array $transactions)
    {
        $that = $this;

        return $this->transaction(function() use ($transactions, $that) {

            $results = [];

            foreach ($transactions as $key => $transaction) {
                $results[] = $this->save($transaction);
            }

            return collect($results);
        });
    }

    /**
     * Store data to parent and child models (non-transaction processing)
     *
     * @param  array  $attributes
     * @return Model
     */
    private function save(array $attributes)
    {
        $attributes['info'] = $this->transformInfo($attributes['info']);

        $info = App::make(PosSales::class)->firstOrNew([
            'created_for'     => $attributes['info']['created_for'],
            'or_number'       => $attributes['info']['or_number'],
            'terminal_number' => $attributes['info']['terminal_number']
        ]);

        $infoExists = $info->exists;

        $info->fill($attributes['info'])->save();

        $collection = [];

        // Loop through each data, pull only the needed data and create
        // a new instance of the child model
        foreach($attributes['details'] as $data) {

            $data = $this->transformDetail($data);

            array_pull($data, 'id');

            $collection[] = App::make(PosSalesDetail::class)
                ->newInstance($data);
        }

        if($infoExists) {
            $info->details()->delete();
        }

        $info->details()->saveMany($collection);

        if($infoExists) {
            foreach ($info->collections as $key => $value) {
                if(!is_null($value->cardPayment)) {
                    $value->cardPayment()->delete();
                }
            }

            $info->collections()->delete();
        }

        foreach($attributes['collections'] as $collectionData) {

            $data = $this->transformCollection($collectionData);

            array_pull($data, 'id');

            $collection = App::make(PosCollection::class)->fill($data);

            $collection = $info->collections()->save($collection);

            if(in_array($data['payment_method'], [PaymentMethod::CREDIT_CARD, PaymentMethod::DEBIT_CARD])) {
                $cardPaymentData = $collectionData['card_payment'];
                $cardPayment = [];

                foreach ($cardPaymentData as $key => $value) {
                    $payment = $this->transformCardPayment($value);

                    array_pull($payment, 'id');

                    $cardPayment[] = App::make(PosCardPayment::class)->newInstance($payment);
                }
                
                $collection->cardPayment()->saveMany($cardPayment);
            }
        }

        event(new PosSalesEvent($info->fresh()));

        return $info->fresh();
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformInfo(array $data)
    {
        return [
            'cashier'               => $data['cashier'],
            'customer_id'           => $data['customer_id'] ?? null,
            'customer_detail_id'    => $data['customer_detail_id'] ?? null,
            'salesman_id'           => $data['salesman_id'],
            'created_for'           => $data['created_for'],
            'for_location'          => $data['for_location'],
            'remarks'               => $data['remarks'] ?? null,
            'transaction_date'      => Carbon::parse($data['transaction_date']),
            'customer_type'         => $data['customer_type'],
            'customer_walk_in_name' => $data['customer_walk_in_name'] ?? null,
            'total_amount'          => $data['total_amount'],
            'member_id'             => $data['member_id'] ?? null,
            'transaction_number'    => $data['transaction_number'],
            'or_number'             => $data['or_number'],
            'terminal_number'       => $data['terminal_number'],
            'senior_number'         => $data['senior_number'] ?? null,
            'senior_name'           => $data['senior_name'] ?? null,
            'is_non_vat'            => $data['is_non_vat'],
            'is_wholesale'          => $data['is_wholesale']
        ];
    }

    protected function transformDetail(array $data)
    {
        return [
            'product_id'   => $data['product_id'],
            'unit_id'      => $data['unit_id'],
            'unit_qty'     => $data['unit_qty'],
            'qty'          => $data['qty'],
            'oprice'       => $data['oprice'],
            'price'        => $data['price'],
            'discount1'    => $data['discount1'],
            'discount2'    => $data['discount2'],
            'discount3'    => $data['discount3'],
            'discount4'    => $data['discount4'],
            'remarks'      => $data['remarks'] ?? null,
            'vat'          => $data['vat'],
            'is_senior'    => $data['is_senior'],
            'group_type'   => $data['group_type'] ?? GroupType::NONE,
            'manageable'   => $data['manageable'],
            'product_type' => $data['product_type']
        ];
    }

    protected function transformCollection(array $data)
    {
        return [
            'payment_method'  => $data['payment_method'],
            'amount'          => $data['amount'],
            'bank_account_id' => $data['bank_account_id'] ?? null,
            'check_no'        => $data['check_no'] ?? null,
            'check_date'      => $data['check_date'] ?? null,
            'gift_check_id'   => $data['gift_check_id'] ?? null
        ];
    }

    protected function transformCardPayment(array $data)
    {
        return [
            'card_number'     => $data['card_number'],
            'full_name'       => $data['full_name'],
            'type'            => $data['type'],
            'amount'          => $data['amount'],
            'expiration_date' => $data['expiration_date'],
            'approval_code'   => $data['approval_code']
        ];
    }
}