<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\DamageServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\Damage;
use Modules\Core\Entities\DamageDetail;

class DamageService extends InvoiceService implements DamageServiceInterface
{
    protected function models()
    {
        $this->info = Damage::class;
        $this->details = DamageDetail::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'reason_id' => $data['reason_id'],
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'reason_id' => $data['reason_id'],
        ]);
    }
}
