<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\ReportBuilderServiceInterface;
use Modules\Core\Entities\ReportBuilder;

class ReportBuilderService extends BaseService implements ReportBuilderServiceInterface
{
    protected function model()
    {
        return ReportBuilder::class;
    }
}