<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\RoleServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Entities\Role;
use Modules\Core\Entities\BranchRole;
use DB;
use App;

class RoleService implements RoleServiceInterface
{
    use DatabaseTransaction;

    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $info = new Role($this->infoTransformer($attributes['info']));

            $info->save();

            foreach($attributes['permissions'] as $index => $value){
                if(count($value['code']) == 0) {
                    continue;
                }

                $branchRole = $info->branches()->create([
                    'branch_id' => $value['branch']
                ]);

                $branchRole->permissions()->attach($value['code']);
            }

            return $info->fresh();
        });
    }

    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $role = Role::findOrFail($id);

            $role->fill($this->infoTransformer($attributes['info']))->save();

            $branchRoles = $role->branches;
            $selectedBranches = array_pluck($attributes['permissions'], 'branch');

            foreach ($branchRoles as $branchRole) {
                if (!in_array($branchRole->branch_id, $selectedBranches)) {
                    if($attributes['apply']) {
                        $this->clearUserPermissionsByBranchRole($role, $branchRole);
                    }

                    $branchRole->permissions()->detach();
                    $branchRole->delete();
                }
            }

            foreach($attributes['permissions'] as $index => $value) {
                $current = $value['branch'];

                $branchRole = $branchRoles->first(function($branchRole, $key) use ($current){
                    return $branchRole->branch_id === $current;
                });

                if(count($value['code']) === 0) {
                    continue;
                }

                if (!$branchRole) {
                    $branchRole = $role->branches()->create([
                        'branch_id' => $value['branch']
                    ]);
                }

                $changes = $branchRole->permissions()->sync($value['code']);

                if($attributes['apply']) {
                    $this->applyRolePermissionChangesToUsers($role, $branchRole, $changes);
                }
            }

            return $role->fresh();
        });
    }

    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = Role::findOrFail($id);

            $info->delete();

            return $info;
        });
    }

    private function infoTransformer(array $data)
    {
        return [
            'code' => $data['code'],
            'name' => $data['name']
        ];
    }

    private function applyRolePermissionChangesToUsers(Role $role, BranchRole $branchRole, $permissionsChanges)
    {
        $attached = $permissionsChanges['attached'];
        $detached = $permissionsChanges['detached'];

        $users = $role->users->pluck('id')->toArray();

        if(count($attached) > 0) {
            $this->attachRolePermissionsToUsers($users, $branchRole, $attached);
        }

        if(count($detached) > 0) {
            $this->detachRolePermissionsToUsers($users, $branchRole, $detached);
        }
    }

    private function attachRolePermissionsToUsers($users, BranchRole $branchRole, $permissions)
    {
        foreach($users as $user) {
            $existing = DB::table('branch_permission_user')
                ->where('branch_id', $branchRole->branch_id)
                ->whereIn('permission_id', $permissions)
                ->where('user_id', $user)
                ->get()
                ->pluck('permission_id')
                ->toArray();

            $diff = array_diff($permissions, $existing);

            if (count($diff) === 0) {
                continue;
            }

            $collection = [];

            foreach ($diff as $permission) {
                $collection[] = [
                    'user_id' => $user,
                    'branch_id' => $branchRole->branch_id,
                    'permission_id' => $permission
                ];
            }

            DB::table('branch_permission_user')->insert($collection);
        }
    }

    private function detachRolePermissionsToUsers($users, BranchRole $branchRole, $permissions)
    {
        DB::table('branch_permission_user')
            ->whereIn('user_id', $users)
            ->where('branch_id', $branchRole->branch_id)
            ->whereIn('permission_id', $permissions)
            ->delete();
    }

    private function clearUserPermissionsByBranchRole(Role $role, BranchRole $branchRole)
    {
        $users = $role->users->pluck('id')->toArray();
        $permissions = $branchRole->permissions->pluck('id')->toArray();

        DB::table('branch_permission_user')
            ->whereIn('user_id', $users)
            ->where('branch_id', $branchRole->branch_id)
            ->whereIn('permission_id', $permissions)
            ->delete();
    }
}