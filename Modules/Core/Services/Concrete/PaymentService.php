<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Services\Contracts\PaymentServiceInterface;
use Modules\Core\Entities\Payment;
use Modules\Core\Entities\PaymentDetail;
use Modules\Core\Entities\PaymentReference;

class PaymentService extends FinancialFlowService implements PaymentServiceInterface
{
    public function __construct()
    {
        $this->info = Payment::class;
        $this->details = PaymentDetail::class;
        $this->reference = PaymentReference::class;
    }

    /**
     * Update the each transaction's remaining amount based on the Payment Reference data
     *
     * @param  Payment $payment
     * @return $this
     */
    public function updateTransactionsRemainingAmount(Payment $payment)
    {
        return $this->changeTransactionsRemainingAmount($payment, 'sub');
    }

    /**
     * Revert each transaction's remaining amount based on the Payment Reference data
     *
     * @param  Payment $payment
     * @return $this
     */
    public function revertTransactionsRemainingAmount(Payment $payment)
    {
        return $this->changeTransactionsRemainingAmount($payment);
    }

    /**
     * Trigger add or subtract remaining amount of Purchase Inbound
     *
     * @param  Payment $payment
     * @param  string  $method
     * @return $this
     */
    private function changeTransactionsRemainingAmount(Payment $payment, $method = 'add')
    {
        $references = $payment->references;

        foreach ($references as $reference) {
            $transaction = $reference->reference;

            $transaction->remaining_amount = $transaction->remaining_amount->$method($reference->payable->abs());

            $transaction->save();
        }

        return $this;
    }

    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return [
            'id'               => $data['id'] ?? 0,
            'created_for'      => $data['created_for'],
            'supplier_id'      => $data['supplier_id'],
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks']
        ];
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformReference(array $data)
    {
        return [
            'id'             => $data['id'] ?? 0,
            'reference_id'   => $data['reference_id'],
            'reference_type' => $data['reference_type'],
            'payable'        => $data['payable']
        ];
    }
}