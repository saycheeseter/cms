<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\Counter;
use Modules\Core\Entities\Damage;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\DataCollectorTransaction;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\Payment;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Services\Contracts\BranchModuleSeriesServiceInterface;
use Modules\Core\Entities\Contracts\BranchModuleSeries as BranchModuleSeriesInterface;
use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Traits\HasMorphMap;

class BranchModuleSeriesService implements BranchModuleSeriesServiceInterface
{
    use HasMorphMap;

    public function execute(BranchModuleSeriesInterface $model) 
    {
        $record = BranchModuleSeries::where('type', $model->series_type)
            ->where('column', $model->series_column)
            ->where('branch_id', $model->series_branch)
            ->lockForUpdate()
            ->first();

        $column = $model->series_column;
        $value = trim($model->$column);

        if(($value == '' || is_null($value)) && !$model->exists) {
            $model->$column = $record->series;
            $this->incrementSeries($record);
        } else if(
            is_numeric($value)
            && $record->series <= $value
            && bccomp($record->limit, $value) == 1
        ) {
            $record->series = bcadd($value, 1);
            $record->save();
        }
    }


    public function createInitialData(Branch $model)
    {
        $transactions = [
            $this->getMorphMapKey(Purchase::class),
            $this->getMorphMapKey(PurchaseInbound::class),
            $this->getMorphMapKey(PurchaseReturn::class),
            $this->getMorphMapKey(PurchaseReturnOutbound::class),
            $this->getMorphMapKey(Damage::class),
            $this->getMorphMapKey(DamageOutbound::class),
            $this->getMorphMapKey(Sales::class),
            $this->getMorphMapKey(SalesOutbound::class),
            $this->getMorphMapKey(SalesReturn::class),
            $this->getMorphMapKey(SalesReturnInbound::class),
            $this->getMorphMapKey(StockRequest::class),
            $this->getMorphMapKey(StockDelivery::class),
            $this->getMorphMapKey(StockDeliveryOutbound::class),
            $this->getMorphMapKey(StockDeliveryInbound::class),
            $this->getMorphMapKey(StockReturn::class),
            $this->getMorphMapKey(StockReturnOutbound::class),
            $this->getMorphMapKey(StockReturnInbound::class),
            $this->getMorphMapKey(InventoryAdjust::class),
            $this->getMorphMapKey(ProductConversion::class),
            $this->getMorphMapKey(Payment::class),
            $this->getMorphMapKey(Collection::class),
            $this->getMorphMapKey(Counter::class),
            $this->getMorphMapKey(DataCollectorTransaction::class),
        ];

        $collection = [];

        foreach ($transactions as $module) {
            $collection[] = [
                'branch_id' => $model->id,
                'type'      => $module,
                'column'    => 'sheet_number',
                'series'    => sprintf("1%s0000001", str_pad($model->id,3, '0', STR_PAD_LEFT)),
                'limit'     => sprintf("1%s9999999", str_pad($model->id,3, '0', STR_PAD_LEFT)),
            ];
        }

        BranchModuleSeries::insert($collection);
    }

    private function incrementSeries(BranchModuleSeries $record)
    {
        $record->series = bcadd($record->series, 1);
        $record->save();
    }

    public function deleteFromBranch(Branch $branch) 
    {
        BranchModuleSeries::where('branch_id', $branch->id)->delete();
    }
}