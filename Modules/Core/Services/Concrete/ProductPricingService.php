<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\ApprovalStatus;

abstract class ProductPricingService
{
    protected $productEntityLog;

    protected $inventoryChain;

    protected $primaryKey;

    /**
     * Updates historical data of the model by checking the fields of the current transaction
     * 
     * @param  InventoryChain $transaction
     */
    public function updateHistory($transaction)
    {
        foreach($transaction->details as $detail) {
            $id = $transaction->{$this->primaryKey};
            $productId = $detail->product_id;

            $history = $this->getHistory($id, $productId);

            // If product doesnt have a historical record, create a new one
            if (!$history) {
                $fields = $this->convertToFields($detail);

                $this->productEntityLog->newInstance($fields)->save();
            }  else {
                $fields = $this->updateFields($history, $detail);

                if(count($fields) > 0) {
                    $history->where('product_id', $productId)
                        ->where($this->primaryKey, $id)
                        ->update($fields);
                }
            }
        }
    }

    /**
     * Populate the historical record of the given entity id
     * 
     * @param  int $id (customer_id, supplier_id, etc.)
     */
    public function populate($id)
    {
        $transactions = $this->inventoryChain
            ->where($this->primaryKey, $id)
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->get();

        if ($transactions->count() === 0) {
            return false;
        }

        foreach ($transactions as $key => $transaction) {
            $this->updateHistory($transaction);
        }
    }

    /**
     * Get the updated fields of each property by executing the specific checkers per property
     * 
     * @param  ProductEntityLog $history
     * @param  InventoryChainDetail $detail
     * @return array
     */
    protected function updateFields($history, $detail)
    {
        $currentPrice = $detail->price;

        $fields = [];

        if ($currentPrice > $history->highest_price) {
            $fields['highest_price'] = $currentPrice;
        }

        if ($currentPrice < $history->lowest_price) {
            $fields['lowest_price'] = $currentPrice;
        }

        if ($detail->transaction->transaction_date->greaterThanOrEqualTo($history->latest_fields_updated_date)) {
            $fields['latest_fields_updated_date'] = $detail->transaction->transaction_date->toDateTimeString();
            $fields['latest_unit_id'] = $detail->unit_id;
            $fields['latest_unit_qty'] = $detail->unit_qty;
            $fields['latest_price'] = $detail->price;
        }

        return array_merge(
            $this->updateAdditionalFields($history, $detail),
            $fields
        );
    }

    /**
     * Convert the InventoryChainDetail entity into an array fields for ProductEntityLog
     * 
     * @param  InventoryChainDetail
     * @return array
     */
    protected function convertToFields($detail)
    {
        return array_merge(
            $this->getUniqueFields($detail),
            $this->getCommonFields($detail)
        );
    }

    /**
     * List of common fields for ProductEntityLog
     * Converts InventoryChainDetail to an array field
     * 
     * @param  InventoryChainDetail
     * @return array
     */
    protected function getCommonFields($detail)
    {
        return array(
            'product_id'       => $detail->product_id,
            'latest_unit_id'    => $detail->unit_id,
            'latest_unit_qty'   => $detail->unit_qty,
            'latest_price'      => $detail->price,
            'latest_fields_updated_date' => $detail->transaction->transaction_date->toDateTimeString(),
            'highest_price'     => $detail->price,
            'lowest_price'      => $detail->price,
        );
    }

    /**
     * List of unique fields fields for ProductEntityLog
     * Converts InventoryChainDetail to an array field
     * 
     * @param  InventoryChainDetail
     * @return array
     */
    protected function getUniqueFields($detail)
    {
       return array();
    }

    /**
     * Hook callback after executing the comparison of fields in updateFields method
     * 
     * @param  ProductInventoryLog $history
     * @param  InventoryChainDetail $detail
     * @return array
     */
    protected function updateAdditionalFields($history, $detail)
    {
        return array();
    }

    /**
     * Fetch the historical record of the given product id in the ProductInventoryLog
     * 
     * @param  int $id Entity Id (customer_id, supplier_id, etc.)
     * @param  int $productId
     * @return ProductInventoryLog
     */
    protected function getHistory($id, $productId)
    {
        return $this->productEntityLog->where('product_id', $productId)
            ->where($this->primaryKey, $id)
            ->first();
    }
}
