<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Events\ReceivedCheckTransferred;
use Modules\Core\Services\Contracts\ReceivedCheckServiceInterface;
use Modules\Core\Entities\ReceivedCheck;

class ReceivedCheckService extends CheckManagementService implements ReceivedCheckServiceInterface
{
    protected $events = [
        'transferred' => ReceivedCheckTransferred::class
    ];

    protected function model()
    {
        return ReceivedCheck::class;
    }
}