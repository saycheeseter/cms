<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\SalesOutboundPrinterServiceInterface as SalesOutboundPrinter;
use Lang;

class SalesOutboundPrinterService extends PrinterService implements SalesOutboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof SalesOutbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => SalesOutbound::class]));
        }

        return $this->print->data($object)->generate('core::sales-outbound.pdf.transaction');
    }
}
