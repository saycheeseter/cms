<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockDeliveryInboundPrinterServiceInterface as StockDeliveryInboundPrinter;
use Lang;

class StockDeliveryInboundPrinterService extends PrinterService implements StockDeliveryInboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof StockDeliveryInbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockDeliveryInbound::class]));
        }

        return $this->print->data($object)->generate('core::stock-delivery-inbound.pdf.transaction');
    }
}
