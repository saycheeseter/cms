<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Services\Contracts\PrinterServiceInterface as Printer;

abstract class PrinterService
{
    protected $print;

    public function __construct(Printer $print)
    {
        $this->print = $print;
    }
}