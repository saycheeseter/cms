<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\PurchaseReturnOutboundPrinterServiceInterface as PurchaseReturnOutboundPrinter;
use Lang;

class PurchaseReturnOutboundPrinterService extends PrinterService implements PurchaseReturnOutboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof PurchaseReturnOutbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => PurchaseReturnOutbound::class]));
        }

        return $this->print->data($object)->generate('core::purchase-return-outbound.pdf.transaction');
    }
}
