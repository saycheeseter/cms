<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\PurchaseReturnPrinterServiceInterface as PurchaseReturnPrinter;
use Lang;

class PurchaseReturnPrinterService extends PrinterService implements PurchaseReturnPrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof PurchaseReturn) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => PurchaseReturn::class]));
        }

        return $this->print->data($object)->generate('core::purchase-return.pdf.transaction');
    }
}