<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\InventoryAdjustPrinterServiceInterface as InventoryAdjustPrinter;
use Lang;

class InventoryAdjustPrinterService extends PrinterService implements InventoryAdjustPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof InventoryAdjust) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => InventoryAdjust::class]));
        }

        return $this->print->data($object)->generate('core::inventory-adjust.pdf.transaction');
    }
}
