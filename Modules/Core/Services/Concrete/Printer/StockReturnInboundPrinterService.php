<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockReturnInboundPrinterServiceInterface as StockReturnInboundPrinter;
use Lang;

class StockReturnInboundPrinterService extends PrinterService implements StockReturnInboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof StockReturnInbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockReturnInbound::class]));
        }

        return $this->print->data($object)->generate('core::stock-return-inbound.pdf.transaction');
    }
}
