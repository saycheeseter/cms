<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockRequest;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockRequestPrinterServiceInterface as StockRequestPrinter;
use Lang;

class StockRequestPrinterService extends PrinterService implements StockRequestPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof StockRequest) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockRequest::class]));
        }

        return $this->print->data($object)->generate('core::stock-request-to.pdf.transaction');
    }
}
