<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\Sales;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\SalesPrinterServiceInterface as SalesPrinter;
use Lang;

class SalesPrinterService extends PrinterService implements SalesPrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof Sales) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => Sales::class]));
        }

        return $this->print->data($object)->generate('core::sales.pdf.transaction');
    }
}