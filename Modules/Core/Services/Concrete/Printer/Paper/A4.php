<?php

namespace Modules\Core\Services\Concrete\Printer\Paper;

class A4 extends General
{
    protected $width = 21;
    protected $height = 29.7;
}