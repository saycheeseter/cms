<?php

namespace Modules\Core\Services\Concrete\Printer\Paper;

use Modules\Core\Services\Contracts\Printer\Paper;

class General implements Paper
{
    protected $width = 0;
    protected $height = 0;

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }
}