<?php

namespace Modules\Core\Services\Concrete\Printer\Paper;

class Legal extends General
{
    protected $width = 21.6;
    protected $height = 35.6;
}