<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\DamageOutboundPrinterServiceInterface as DamageOutboundPrinter;
use Lang;

class DamageOutboundPrinterService extends PrinterService implements DamageOutboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof DamageOutbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => DamageOutbound::class]));
        }

        return $this->print->data($object)->generate('core::damage-outbound.pdf.transaction');
    }
}
