<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockReturn;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockReturnPrinterServiceInterface as StockReturnPrinter;
use Lang;

class StockReturnPrinterService extends PrinterService implements StockReturnPrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof StockReturn) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockReturn::class]));
        }

        return $this->print->data($object)->generate('core::stock-return.pdf.transaction');
    }
}