<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\Damage;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\DamagePrinterServiceInterface as DamagePrinter;
use Lang;

class DamagePrinterService extends PrinterService implements DamagePrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof Damage) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => Damage::class]));
        }

        return $this->print->data($object)->generate('core::damage.pdf.transaction');
    }
}
