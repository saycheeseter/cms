<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\Purchase;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\PurchasePrinterServiceInterface as PurchasePrinter;
use Lang;

class PurchasePrinterService extends PrinterService implements PurchasePrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof Purchase) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => Purchase::class]));
        }

        return $this->print->data($object)->generate('core::purchase-order.pdf.transaction');
    }
}