<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\PurchaseInboundPrinterServiceInterface as PurchaseInboundPrinter;
use Lang;

class PurchaseInboundPrinterService extends PrinterService implements PurchaseInboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof PurchaseInbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => PurchaseInbound::class]));
        }

        return $this->print->data($object)->generate('core::purchase-inbound.pdf.transaction');
    }
}
