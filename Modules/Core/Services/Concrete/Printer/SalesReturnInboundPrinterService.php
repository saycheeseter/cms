<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\SalesReturnInboundPrinterServiceInterface as SalesReturnInboundPrinter;
use Lang;

class SalesReturnInboundPrinterService extends PrinterService implements SalesReturnInboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof SalesReturnInbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => SalesReturnInbound::class]));
        }

        return $this->print->data($object)->generate('core::sales-return-inbound.pdf.transaction');
    }
}
