<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\SalesReturn;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\SalesReturnPrinterServiceInterface as SalesReturnPrinter;
use Lang;

class SalesReturnPrinterService extends PrinterService implements SalesReturnPrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof SalesReturn) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => SalesReturn::class]));
        }

        return $this->print->data($object)->generate('core::sales-return.pdf.transaction');
    }
}