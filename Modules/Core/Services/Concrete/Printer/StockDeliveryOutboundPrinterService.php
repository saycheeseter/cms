<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockDeliveryOutboundPrinterServiceInterface as StockDeliveryOutboundPrinter;
use Lang;

class StockDeliveryOutboundPrinterService extends PrinterService implements StockDeliveryOutboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof StockDeliveryOutbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockDeliveryOutbound::class]));
        }

        return $this->print->data($object)->generate('core::stock-delivery-outbound.pdf.transaction');
    }
}
