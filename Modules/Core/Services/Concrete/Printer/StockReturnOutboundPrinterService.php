<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockReturnOutboundPrinterServiceInterface as StockReturnOutboundPrinter;
use Lang;

class StockReturnOutboundPrinterService extends PrinterService implements StockReturnOutboundPrinter
{
    /**
     * Generates transaction printout
     *
     * @param   $object
     * @return Response
     */
    public function transaction($object)
    {
        if (!$object instanceof StockReturnOutbound) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockReturnOutbound::class]));
        }

        return $this->print->data($object)->generate('core::stock-return-outbound.pdf.transaction');
    }
}
