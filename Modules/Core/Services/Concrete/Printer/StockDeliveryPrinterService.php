<?php

namespace Modules\Core\Services\Concrete\Printer;

use Modules\Core\Entities\StockDelivery;
use Modules\Core\Exceptions\PrintException;
use Modules\Core\Services\Contracts\Printer\StockDeliveryPrinterServiceInterface as StockDeliveryPrinter;
use Lang;

class StockDeliveryPrinterService extends PrinterService implements StockDeliveryPrinter
{
    /**
     * Generates transaction printout
     * 
     * @param   $object
     * @return Response        
     */
    public function transaction($object)
    {
        if (!$object instanceof StockDelivery) {
            throw new PrintException(Lang::get('core::error.parameter.error', ['class' => StockDelivery::class]));
        }

        return $this->print->data($object)->generate('core::stock-delivery.pdf.transaction');
    }
}