<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\InventoryAdjustServiceInterface;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\InventoryAdjustDetail;
use Carbon\Carbon;
use Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface;

class InventoryAdjustService extends InventoryFlowService implements InventoryAdjustServiceInterface
{
    protected function models()
    {
        $this->info = InventoryAdjust::class;
        $this->details = InventoryAdjustDetail::class;
    }

    /**
     * Store data to parent and child model, and create new serials
     *
     * @param  array  $attributes
     * @return Model
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $transaction = $that->createOrUpdate($attributes);

            $that->createOrUpdateItemManagementAndSync($attributes, $transaction);

            return $transaction->info->fresh();
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Model
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $transaction = $that->createOrUpdate($attributes, $id);

            $that->createOrUpdateItemManagementAndSync($attributes, $transaction);

            return $transaction->info;
        });
    }

    /**
     * Create new serials|batches and synced the existing/created data to the transaction details
     *
     * @param  array $attributes
     * @param  array $transaction
     */
    private function createOrUpdateItemManagementAndSync($attributes, $transaction)
    {
        $attributes = (object) $attributes;

        $productSerialNumberService = $this->app->make(ProductSerialNumberServiceInterface::class);
        //$productBatchService = $this->app->make(ProductBatchService::class);

        $attached = clone $transaction->details['attached'];

        $query = $this->app->make($this->details);

        foreach ($attributes->{$this->childRootKey()} as $key => $attribute) {
            $attribute = (object) $attribute;

            if (!is_null($attribute->id) && $attribute->id !== 0) {
                $detail = $query->find($attribute->id);
            } else {
                $detail = $attached->first();
                $attached->pull(0);
                $attached = $attached->values();
            }

            if (!$this->isSerialsOrBatchesSet($attribute)) {
                continue;
            }

            // If serial object exists, create or update the serials and sync it to the corresponding detail
            if (isset($attribute->serials) && count($attribute->serials) > 0) {
                $serials = [];

                switch ($detail->process_flow) {
                    case 'adjustment_increase':
                        $serials = $productSerialNumberService
                            ->createOrUpdate(
                                $attribute->product_id,
                                $attribute->serials
                            )
                            ->toArray();
                        break;

                    case 'adjustment_decrease':
                        $serials = $attribute->serials;
                        break;
                }

                $detail->serials()->sync($this->transformSerials($serials));
            }


            // If batch object exists, create or update the batches and sync it to the corresponding detail
//            else if (isset($attribute->batches) && count($attribute->batches) > 0) {
//                $batches = $productBatchService->createOrUpdateFromTransaction(
//                    $transaction->info,
//                    $attribute->product_id,
//                    $attribute->batches
//                );
//
//                $detail->batches()->sync($this->transformBatches($batches->toArray(), $transaction->info));
//            }
        }
    }

    /**
     * Check whether serials or batches data existing in the object
     *
     * @param $attribute
     * @return bool
     */
    protected function isSerialsOrBatchesSet($attribute)
    {
        return (isset($attribute->serials) && count($attribute->serials) > 0)
            || (isset($attribute->batches) && count($attribute->batches) > 0);
    }

    protected function transformSerials($data)
    {
        return array_pluck($data, 'id');
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $fields = [
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks' => $data['remarks'],
            'for_location' => $data['for_location'],
            'created_for' => $data['created_for'],
            'requested_by' => $data['requested_by'],
        ];

        foreach ($this->app->make($this->info)->getUDFColumns() as $key => $column) {
            $fields[$key] = $data[$key] ?? null;
        }

        return $fields;
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return [
            'id'         => $data['id'] ?? 0,
            'product_id' => $data['product_id'],
            'unit_id'    => $data['unit_id'],
            'unit_qty'   => $data['unit_qty'],
            'qty'        => $data['qty'],
            'pc_qty'     => $data['pc_qty'],
            'price'      => $data['price'],
            'cost'       => $data['cost'],
            'inventory'  => $data['inventory'],
            'remarks'    => $data['remarks'],
        ];
    }

    protected function transformUpdateApprovedInfo($data)
    {
        $fields = [
            'requested_by' => $data['requested_by'],
            'remarks'      => $data['remarks'] ?? null
        ];

        foreach ($model = $this->app->make($this->info)->getUDFColumns() as $key => $type) {
            $fields[$key] = $data[$key] ?? null;
        }

        return $fields;
    }
}
