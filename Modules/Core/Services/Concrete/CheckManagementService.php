<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Enums\CheckStatus;
use Modules\Core\Entities\Contracts\PaymentCheckManagement;
use Modules\Core\Entities\Contracts\OtherPaymentCheckManagement;
use Modules\Core\Entities\Contracts\PaymentDetailCheckManagement;

abstract class CheckManagementService extends BaseService
{
    protected $events = [];

    public function createFromPayment(PaymentCheckManagement $model)
    {
        $attributes = [
            'transactionable_type' => $model->check_transactionable_type_assignment,
            'remarks' => $model->remarks,
            'transaction_date' => $model->transaction_date,
            'status' => CheckStatus::PENDING
        ];

        foreach($model->details as $key => $value) {
            if($value->payment_method == PaymentMethod::CHECK) {
                $this->store(
                    array_merge(
                        $attributes,
                        [
                            'transactionable_id' => $value->id,
                            'bank_account_id' => $value->bank_account_id,
                            'check_no' => $value->check_no,
                            'check_date' => $value->check_date,
                            'amount' => $value->amount
                        ]
                    )
                );
            }
        }
    }

    public function deleteFromPaymentDetail(PaymentDetailCheckManagement $model)
    {
        $model->check->delete();
    }

    public function deleteFromPayment(PaymentCheckManagement $model)
    {
        foreach ($model->details as $detail) {
            if($detail->check) {
                $detail->check->delete();
            }
        }
    }

    public function createFromOtherPayment(OtherPaymentCheckManagement $model)
    {
        $this->store([
            'transactionable_id' => $model->id,
            'transactionable_type' => $model->check_transactionable_type_assignment,
            'remarks' => $model->remarks,
            'transaction_date' => $model->transaction_date,
            'status' => CheckStatus::PENDING,
            'bank_account_id' => $model->bank_account_id,
            'check_no' => $model->check_number,
            'check_date' => $model->check_date,
            'amount' => $model->total_amount
        ]);
    }

    public function updateFromOtherPayment(OtherPaymentCheckManagement $model)
    {
        $model->check->remarks = $model->remarks;
        $model->check->check_no = $model->check_number;
        $model->check->amount = $model->total_amount;
        $model->check->save();
    }

    public function deleteFromOtherPayment(OtherPaymentCheckManagement $model)
    {
        $model->check->delete();
    }

    public function transfer(Array $data)
    {
        $transferDate = $data['transfer_date'];
        $ids = $data['ids'];

        foreach($ids as $key => $value)
        {
            $check = $this->update([
                'transfer_date' => $transferDate,
                'status' => CheckStatus::TRANSFERRED
            ], $value);

            if (array_key_exists('transferred', $this->events)) {
                event(new $this->events['transferred']($check));
            }
        }
    }
}