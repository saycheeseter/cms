<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockRequestServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\StockRequestDetail;

class StockRequestService extends InvoiceService implements StockRequestServiceInterface
{
    protected function models()
    {
        $this->info = StockRequest::class;
        $this->details = StockRequestDetail::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'request_to' => $data['request_to'],
        ]);
    }
}
