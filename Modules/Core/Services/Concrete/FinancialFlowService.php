<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Traits\DatabaseTransaction;
use App;

abstract class FinancialFlowService
{
    use DatabaseTransaction;

    protected $info;
    protected $details;
    protected $reference;
    protected $events = [];

    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {

            $attributes['info'] = $this->transformInfo($attributes['info']);

            // Make a new instance of the parent model
            $info = App::make($that->info)->newInstance($attributes['info']);
            $info->save();

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes['details'] as $data) {

                $data = $this->transformDetail($data);

                array_pull($data, 'id');

                $collection[] = App::make($that->details)
                    ->newInstance($data);
            }

            call_user_func(array($info, 'details'))->saveMany($collection);

            $collection = [];

            foreach($attributes['references'] as $data) {

                $data = $this->transformReference($data);

                array_pull($data, 'id');

                $collection[] = App::make($that->reference)
                    ->newInstance($data);
            }

            call_user_func(array($info, 'references'))->saveMany($collection);

            return $info->fresh();
        });
    }

    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $attributes['info'] = $this->transformInfo($attributes['info']);
            $info = App::make($that->info)->findOrFail($id);
            $info->fill($attributes['info'])->save();

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes['details'] as $data) {
                $collection[] = $this->transformDetail($data);
            }

            call_user_func(array($info, 'details'))->sync($collection);

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes['references'] as $data) {
                $collection[] = $this->transformReference($data);
            }

            call_user_func(array($info, 'references'))->sync($collection);

            return $info;
        });
    }

    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = App::make($that->info)->findOrFail($id);

            $info->delete();

            call_user_func(array($info, 'details'))->delete();
            call_user_func(array($info, 'references'))->delete();

            return $info;
        });
    }

    public function approval($transactions, $status)
    {
        if (!is_array($transactions)) {
            $transactions = [$transactions];
        }

        $transactions = App::make($this->info)->findMany($transactions);
        $that = $this;

        $this->transaction(function() use ($transactions, $status, $that) {
            foreach ($transactions as $key => $transaction) {
                $currentStatus = $transaction->approval_status;

                $transaction->approval_status = $status;
                $transaction->save();

                switch ($status) {
                    case ApprovalStatus::APPROVED:
                        if (array_key_exists('approved', $that->events)) {
                            event(new $that->events['approved']($transaction));
                        }
                        break;

                    case ApprovalStatus::DRAFT:
                        if ($currentStatus === ApprovalStatus::APPROVED
                            && array_key_exists('revert', $that->events)
                        ) {
                            event(new $that->events['revert']($transaction));
                        }
                        break;
                }
            }
        });

        return count($transactions);
    }

    protected function checkModels()
    {
        $model = App::make($this->info);

        if (!$model instanceof Model) {
            throw new Exception(sprint("
                Class %s must be an instance of Illuminate\\Database\\Eloquent\\Model
            ", get_class($model)));
        }

        $model = App::make($this->details);

        if (!$model instanceof Model) {
            throw new Exception(sprint("
                Class %s must be an instance of Illuminate\\Database\\Eloquent\\Model
            ", get_class($model)));
        }
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return [
            'id'              => $data['id'] ?? 0,
            'payment_method'  => $data['payment_method'],
            'amount'          => $data['amount'],
            'bank_account_id' => $data['bank_account_id'] ?? null,
            'check_no'        => $data['check_no'] ?? null,
            'check_date'      => $data['check_date'] ?? null,
        ];
    }

    abstract protected function transformInfo(array $data);
    abstract protected function transformReference(array $data);
}