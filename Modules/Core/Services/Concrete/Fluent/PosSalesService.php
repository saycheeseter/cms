<?php

namespace Modules\Core\Services\Concrete\Fluent;

use Carbon\Carbon;
use Modules\Core\Entities\PosSales;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Events\PosTransactionSynced;
use Modules\Core\Services\Contracts\PosSalesServiceInterface;
use DB;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Enums\GroupType;

class PosSalesService implements PosSalesServiceInterface
{
    use DatabaseTransaction;

    private $tables = [
        'info' => 'pos_sales',
        'details' => 'pos_sales_detail',
        'collections' => 'pos_collection',
        'card_payments' => 'pos_card_payment',
        'components' => 'pos_detail_components'
    ];

    public function sync(array $transactions)
    {
        $that = $this;

        return $this->transaction(function() use ($transactions, $that) {

            $results = [];

            foreach ($transactions as $key => $transaction) {
                $results[] = $this->save($transaction);
            }

            return collect($results);
        });
    }

    private function save(array $attributes)
    {
        $now = Carbon::now();

        $attributes['info'] = $this->transformInfo($attributes['info']);

        // Check first if there are any existing record for the data to be synced
        $existing = DB::table($this->tables['info'])
            ->where([
                'created_for'     => $attributes['info']['created_for'],
                'or_number'       => $attributes['info']['or_number'],
                'terminal_number' => $attributes['info']['terminal_number']
            ])
            ->take(1)
            ->first();

        $transactionId = null;

        // If there are no existing record, insert the data
        if (null === $existing) {
            $transactionId = DB::table($this->tables['info'])->insertGetId(
                array_merge(
                    $attributes['info'],
                    [
                        'created_at' => $now,
                        'updated_at' => $now
                    ]
                )
            );
        } else {
            // If there are existing records, update the existing data
            $transactionId = $existing->id;

            DB::table($this->tables['info'])
                ->where('id', $transactionId)
                ->update(
                    array_merge(
                        $attributes['info'],
                        ['updated_at' => $now]
                    )
                );

            // If there are existing record, delete the underlying details,
            // collections and card payments
            $this->clearTransactionRelationships($transactionId);
        }

        $details = [];

        // Loop through each data, pull only the needed data
        foreach($attributes['details'] as $detail) {
            $detail = $this->transformDetail($detail);

            array_pull($detail, 'id');

            $detail = array_merge($detail, [
                'transaction_id' => $transactionId,
                'created_at' => $now,
                'updated_at' => $now,
                'ma_cost' => 0
            ]);

            $details[] = $detail;
        }

        if (!empty($details)) {
            DB::table($this->tables['details'])->insert($details);
        }

        $collections = [];

        foreach($attributes['collections'] as $collection) {
            $cards = $collection['card_payments'] ?? [];

            $collection = $this->transformCollection($collection);

            $collection = array_merge($collection, [
                'transaction_id' => $transactionId,
                'created_at' => $now,
                'updated_at' => $now,
            ]);

            array_pull($collection, 'id');

            switch ($collection['payment_method']) {
                case PaymentMethod::CREDIT_CARD:
                case PaymentMethod::DEBIT_CARD:

                    $cardPayments = [];

                    $collectionId = DB::table($this->tables['collections'])->insertGetId($collection);

                    foreach ($cards as $cardPayment) {
                        $cardPayment = $this->transformCardPayment($cardPayment);

                        array_pull($cardPayment, 'id');

                        $cardPayment = array_merge($cardPayment, [
                            'collection_id' => $collectionId,
                            'created_at' => $now,
                            'updated_at' => $now,
                        ]);

                        $cardPayments[] = $cardPayment;
                    }

                    if (!empty($cardPayments)) {
                        DB::table($this->tables['card_payments'])->insert($cardPayments);
                    }
                    break;

                default:
                    $collections[] = $collection;
                    break;
            }
        }

        if (!empty($collections)) {
            DB::table($this->tables['collections'])->insert($collections);
        }

        event(new PosTransactionSynced(PosSales::find($transactionId)));

        return $transactionId;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    private function transformInfo(array $data)
    {
        return [
            'cashier_id'            => $data['cashier_id'],
            'customer_id'           => $data['customer_id'] ?? null,
            'customer_detail_id'    => $data['customer_detail_id'] ?? null,
            'salesman_id'           => $data['salesman_id'],
            'created_for'           => $data['created_for'],
            'for_location'          => $data['for_location'],
            'remarks'               => $data['remarks'] ?? null,
            'transaction_date'      => Carbon::parse($data['transaction_date']),
            'customer_type'         => $data['customer_type'],
            'customer_walk_in_name' => $data['customer_walk_in_name'] ?? null,
            'total_amount'          => $data['total_amount'],
            'member_id'             => $data['member_id'] ?? null,
            'transaction_number'    => $data['transaction_number'],
            'or_number'             => $data['or_number'],
            'terminal_number'       => $data['terminal_number'],
            'senior_number'         => $data['senior_number'] ?? null,
            'senior_name'           => $data['senior_name'] ?? null,
            'is_non_vat'            => $data['is_non_vat'],
            'is_wholesale'          => $data['is_wholesale'],
            'voided'                => $data['voided'] ?? 0
        ];
    }

    private function transformDetail(array $data)
    {
        return [
            'product_id'   => $data['product_id'],
            'unit_id'      => $data['unit_id'],
            'unit_qty'     => $data['unit_qty'],
            'qty'          => $data['qty'],
            'oprice'       => $data['oprice'],
            'price'        => $data['price'],
            'cost'         => $data['cost'],
            'discount1'    => $data['discount1'],
            'discount2'    => $data['discount2'],
            'discount3'    => $data['discount3'],
            'discount4'    => $data['discount4'],
            'remarks'      => $data['remarks'] ?? null,
            'vat'          => $data['vat'],
            'is_senior'    => $data['is_senior'],
            'group_type'   => $data['group_type'] ?? GroupType::NONE,
            'manageable'   => $data['manageable'],
            'product_type' => $data['product_type']
        ];
    }

    private function transformCollection(array $data)
    {
        return [
            'payment_method'  => $data['payment_method'],
            'amount'          => $data['amount'],
            'bank_account_id' => $data['bank_account_id'] ?? null,
            'check_no'        => $data['check_no'] ?? null,
            'check_date'      => $data['check_date'] ?? null,
            'gift_check_id'   => $data['gift_check_id'] ?? null
        ];
    }

    private function transformCardPayment(array $data)
    {
        return [
            'card_number'     => $data['card_number'],
            'full_name'       => $data['full_name'],
            'type'            => $data['type'],
            'amount'          => $data['amount'],
            'expiration_date' => $data['expiration_date'],
            'approval_code'   => $data['approval_code']
        ];
    }

    private function clearTransactionRelationships($id)
    {
        $details = DB::table($this->tables['details'])
            ->where('transaction_id', $id)
            ->get();

        $collections = DB::table($this->tables['collections'])
            ->where('transaction_id', $id)
            ->get();

        DB::table($this->tables['card_payments'])
            ->whereIn('collection_id', $collections->pluck('id')->toArray())
            ->delete();

        DB::table($this->tables['components'])
            ->whereIn('pos_sales_detail_id', $details->pluck('id')->toArray())
            ->delete();

        DB::table($this->tables['details'])
            ->where('transaction_id', $id)
            ->delete();

        DB::table($this->tables['collections'])
            ->where('transaction_id', $id)
            ->delete();
    }
}