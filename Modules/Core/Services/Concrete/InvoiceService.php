<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Enums\GroupType;

abstract class InvoiceService extends InventoryFlowService
{
    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $fields = [
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks'],
            'created_for'      => $data['created_for'], 
            'for_location'     => $data['for_location'], 
            'requested_by'     => $data['requested_by'],
        ];
        
        foreach ($model = $this->app->make($this->info)->getUDFColumns() as $key => $type) {
            $fields[$key] = $data[$key] ?? null;
        }

        return $fields;
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return [
            'id'           => $data['id'] ?? 0,
            'product_id'   => $data['product_id'],
            'unit_id'      => $data['unit_id'],
            'unit_qty'     => $data['unit_qty'],
            'qty'          => $data['qty'],
            'oprice'       => $data['oprice'],
            'price'        => $data['price'],
            'cost'         => $data['cost'],
            'discount1'    => $data['discount1'] ?? 0,
            'discount2'    => $data['discount2'] ?? 0,
            'discount3'    => $data['discount3'] ?? 0,
            'discount4'    => $data['discount4'] ?? 0,
            'remarks'      => $data['remarks'] ?? null,
            'group_type'   => $data['group_type'] ?? GroupType::NONE,
            'manageable'   => $data['manageable'],
            'product_type' => $data['product_type']
        ];
    }

    protected function transformUpdateApprovedInfo($data)
    {
        $fields = [
            'requested_by' => $data['requested_by'],
            'remarks'      => $data['remarks'] ?? null
        ];

        foreach ($model = $this->app->make($this->info)->getUDFColumns() as $key => $type) {
            $fields[$key] = $data[$key] ?? null;
        }

        return $fields;
    }
}