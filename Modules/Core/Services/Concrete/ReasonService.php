<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\ReasonServiceInterface;
use Modules\Core\Entities\Reason;

class ReasonService extends BaseService implements ReasonServiceInterface
{
    protected function model()
    {
        return Reason::class;
    }
}
