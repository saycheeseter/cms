<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\PermissionModuleServiceInterface;
use Modules\Core\Entities\ReportBuilder;
use Modules\Core\Entities\PermissionModule;
use App;

class PermissionModuleService extends BaseService implements PermissionModuleServiceInterface
{
    protected function model()
    {
        return PermissionModule::class;
    }

    public function storeFromReportBuilder(ReportBuilder $sourceModel, $sectionId)
    {   
        $count = App::make($this->model())->where('section_id', $sectionId)->get()->count();

        $count++;

        $model = $this->store([
            'section_id'      => $sectionId,
            'name'            => $this->parseAsName($sourceModel->name),
            'alias'           => $this->parseAsAlias($sourceModel->slug),
            'sequence_number' => $count
        ]);

        return $model;
    }

    public function updateFromReportBuilder(ReportBuilder $sourceModel)
    {
        $section = App::make($this->model())
            ->where('alias', $this->parseAsAlias($sourceModel->getOriginal('slug')))
            ->first();

        $model = $this->update([
            'name'  =>  $this->parseAsName($sourceModel->name),
            'alias' => $this->parseAsAlias($sourceModel->slug)
        ], $section->id);
    }

    public function deleteFromReportBuilder(ReportBuilder $sourceModel)
    {
        $module = App::make($this->model())
            ->where('alias', $this->parseAsAlias($sourceModel->getOriginal('slug')))
            ->get()
            ->first();

        $model = $this->delete($module->id);
    }

    /**
     * parse string to formatted alias for permission
     * @param  strubg $subject
     * @param  string $search
     * @param  string $permission
     * @return string             parsed string alias
     */
    private function parseAsAlias($subject, $search = '-')
    {
        return str_replace($search, '_', $subject);
    }

    private function parseAsName($name)
    {
        return ucwords(strtolower($name));
    }
}