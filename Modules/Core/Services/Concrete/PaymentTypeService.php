<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\PaymentTypeServiceInterface;
use Modules\Core\Entities\PaymentType;

class PaymentTypeService extends BaseService implements PaymentTypeServiceInterface
{
    protected function model()
    {
        return PaymentType::class;
    }
}