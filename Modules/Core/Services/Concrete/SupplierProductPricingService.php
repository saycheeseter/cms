<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\SupplierProductPricingServiceInterface;
use Modules\Core\Entities\ProductSupplier;
use Modules\Core\Entities\PurchaseInbound;

class SupplierProductPricingService extends ProductPricingService implements SupplierProductPricingServiceInterface
{
    protected $primaryKey = 'supplier_id';

    public function __construct(ProductSupplier $productEntityLog, PurchaseInbound $inventoryChain)
    {
        $this->productEntityLog = $productEntityLog;
        $this->inventoryChain = $inventoryChain;
    }

    protected function getUniqueFields($detail)
    {
        return array(
            'supplier_id' => $detail->transaction->supplier_id,
            'min_qty' => $detail->total_qty
        );
    }

    protected function updateAdditionalFields($history, $detail)
    {
        $fields = [];

        if ($detail->total_qty < $history->min_qty) {
            $fields['min_qty'] = $detail->total_qty;
        }

        return $fields;
    }
}
