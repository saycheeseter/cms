<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\PurchaseInboundServiceInterface;
use Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface as ProductSerialNumberService;
use Modules\Core\Services\Contracts\ProductBatchServiceInterface as ProductBatchService;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseInboundDetail;

class PurchaseInboundService extends InventoryChainService implements PurchaseInboundServiceInterface
{
    protected function models()
    {
        $this->info = PurchaseInbound::class;
        $this->details = PurchaseInboundDetail::class;
    }

    /**
     * Store data to parent and child model, and create new serials
     *
     * @param  array  $attributes
     * @return Model
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $transaction = $that->createOrUpdate($attributes);

            $that->createOrUpdateItemManagementAndAttach($attributes, $transaction);
            $that->syncComponents($attributes, $transaction->details);

            return $transaction->info->fresh();
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Model
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $transaction = $that->createOrUpdate($attributes, $id);

            $that->createOrUpdateItemManagementAndAttach($attributes, $transaction);
            $that->syncComponents($attributes, $transaction->details);

            return $transaction->info;
        });
    }

    /**
     * Create new serials|batches and attached the created data to the transaction details
     * 
     * @param  array $attributes
     * @param  array $transaction
     */
    private function createOrUpdateItemManagementAndAttach($attributes, $transaction)
    {
        $attributes = (object) $attributes;

        $productSerialNumberService = $this->app->make(ProductSerialNumberService::class);
        $productBatchService = $this->app->make(ProductBatchService::class);

        $attached = clone $transaction->details['attached'];

        $query = $this->app->make($this->details);

        foreach ($attributes->{$this->childRootKey()} as $key => $attribute) {
            $attribute = (object) $attribute;

            if (!is_null($attribute->id) && $attribute->id !== 0) {
                $detail = $query->find($attribute->id);
            } else {
                $detail = $attached->first();
                $attached->pull(0);
                $attached = $attached->values();
            }

            if (!$this->isSerialsOrBatchesSet($attribute)) {
                continue;
            }

            // If serial object exists, create or update the serials and sync it to the corresponding detail
            if (isset($attribute->serials) && count($attribute->serials) > 0) {
                $serials = $productSerialNumberService->createOrUpdateFromTransaction(
                    $transaction->info,
                    $attribute->product_id,
                    $attribute->serials
                );

                $detail->serials()->sync($this->transformSerials($serials->toArray()));
            }

            // If batch object exists, create or update the batches and sync it to the corresponding detail
            else if (isset($attribute->batches) && count($attribute->batches) > 0) {
                $batches = $productBatchService->createOrUpdateFromTransaction(
                    $transaction->info,
                    $attribute->product_id,
                    $attribute->batches
                );

                $detail->batches()->sync($this->transformBatches($batches->toArray(), $transaction->info));
            }
        }
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'supplier_id'          => $data['supplier_id'],
            'payment_method'       => $data['payment_method'],
            'term'                 => $data['term'],
            'dr_no'                => $data['dr_no'],
            'supplier_invoice_no'  => $data['supplier_invoice_no'],
            'supplier_box_count'   => $data['supplier_box_count'],
            'transactionable_type' => $data['transactionable_type'] ?? null,
            'transactionable_id'   => $data['transactionable_id'] ?? null,
        ]);
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return array_merge(parent::transformDetail($data), [
            'transactionable_type' => $data['transactionable_type'] ?? null,
            'transactionable_id'   => $data['transactionable_id'] ?? null,
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'payment_method' => $data['payment_method'],
            'term' => $data['term'],
            'dr_no' => $data['dr_no'],
            'supplier_invoice_no' => $data['supplier_invoice_no'],
            'supplier_box_count' => $data['supplier_box_count']
        ]);
    }
}
