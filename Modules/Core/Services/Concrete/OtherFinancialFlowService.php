<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Events\OtherPaymentCreated;
use Modules\Core\Events\OtherPaymentDeleted;
use Modules\Core\Events\OtherPaymentUpdated;

abstract class OtherFinancialFlowService extends TransactionService
{
    protected $events = [
        'created' => OtherPaymentCreated::class,
        'updated' => OtherPaymentUpdated::class,
        'deleted' => OtherPaymentDeleted::class
    ];

    /**
     * Transforms info array
     * 
     * @param  array  $data
     * @return array      
     */
    protected function parentTransformer(array $data)
    {
        return [
            'reference_name'   => $data['reference_name'],
            'payment_method'   => $data['payment_method'],
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks'],
            'bank_account_id'  => $data['bank_account_id'] ?? null,
            'check_number'     => $data['check_number'] ?? null,
            'check_date'       => isset($data['check_date']) ? Carbon::parse($data['check_date']) : null,
        ];
    }

    /**
     * Transforms detail array
     * 
     * @param  array  $data
     * @return array      
     */
    protected function childTransformer(array $data)
    {
        return [
            'id' => $data['id'] ?? 0,
            'type' => $data['type'],
            'amount' => $data['amount']
        ];
    }
}