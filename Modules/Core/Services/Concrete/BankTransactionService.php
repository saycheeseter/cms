<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\BankTransactionServiceInterface;
use Modules\Core\Entities\BankTransaction;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Enums\BankTransactionType;

class BankTransactionService extends BaseService implements BankTransactionServiceInterface
{
    protected function model()
    {
        return BankTransaction::class;
    }

    protected function transform($data)
    {
        return [
            'branch_id'        => Auth::branch()->id,
            'transaction_date' => $data['transaction_date'],
            'type'             => $data['type'],
            'amount'           => $data['amount'],
            'from_bank'        => $data['type'] == BankTransactionType::DEPOSIT ? null : $data['from_bank'],
            'to_bank'          => $data['type'] == BankTransactionType::WITHDRAWAL ? null : $data['to_bank'],
            'remarks'          => $data['remarks']
        ];
    }
}
