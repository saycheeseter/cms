<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\UserMenuServiceInterface;
use Modules\Core\Entities\UserMenu;

class UserMenuService extends BaseService implements UserMenuServiceInterface
{
    protected function model()
    {
        return UserMenu::class;
    }
}