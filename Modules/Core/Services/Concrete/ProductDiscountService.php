<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ProductDiscountServiceInterface;

use Modules\Core\Traits\DatabaseTransaction;

use Modules\Core\Entities\ProductDiscount;
use Modules\Core\Entities\ProductDiscountDetail;
use Modules\Core\Entities\ProductDiscountTarget;

use Modules\Core\Entities\Customer;
use Modules\Core\Entities\Member;
use Modules\Core\Entities\MemberRate;

use Modules\Core\Entities\Product;
use Modules\Core\Entities\Brand;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Supplier;

use Modules\Core\Enums\TargetType;
use Modules\Core\Enums\SelectType;

use Modules\Core\Traits\HasMorphMap;

use App;

class ProductDiscountService implements ProductDiscountServiceInterface
{
    use DatabaseTransaction,
        HasMorphMap;

    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $info = App::make(ProductDiscount::class)->newInstance(
                $this->infoTransformer($attributes['info'])
            );

            $info->save();

            $collection = [];

            foreach($attributes['details'] as $data) {
                $transformed = $that->detailTransformer($data);

                array_pull($transformed, 'id');

                $collection[] = App::make(ProductDiscountDetail::class)
                    ->newInstance($transformed);
            }

            $info->details()->saveMany($collection);

            if($info->target_type != TargetType::ALL_CUSTOMERS 
                && $info->target_type != TargetType::ALL_MEMBERS) {
                
                $collection = [];

                foreach($attributes['targets'] as $data) {
                    $transformed = $that->targetTransformer($data);

                    array_pull($transformed, 'id');

                    $collection[] = App::make(ProductDiscountTarget::class)
                        ->newInstance($transformed);
                }

                $info->targets()->saveMany($collection);
            }

            return $info->fresh();
        });
    }

    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $info = App::make(ProductDiscount::class)->findOrFail($id);

            $info->fill($this->infoTransformer($attributes['info']))->save();

            $collection = [];

            if($attributes['details']) {
                foreach($attributes['details'] as $data) {
                    $collection[] = $that->detailTransformer($data);
                }
            }

            $info->details()->sync($collection);

            $collection = [];

            if($attributes['targets']) {
                foreach($attributes['targets'] as $data) {
                    $collection[] = $that->targetTransformer($data);
                }
            }

            $info->targets()->sync($collection);

            return $info;
        });
    }

    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = App::make(ProductDiscount::class)->findOrFail($id);

            $info->details()->delete();
            $info->targets()->delete();
            $info->delete();

            return $info;
        });
    }

    protected function infoTransformer(array $data)
    {
        return [
            'name' => $data['name'],
            'code' => $data['code'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to'],
            'status' => $data['status'],
            'mon' => $data['mon'],
            'tue' => $data['tue'],
            'wed' => $data['wed'],
            'thur' => $data['thur'],
            'fri' => $data['fri'],
            'sat' => $data['sat'],
            'sun' => $data['sun'],
            'target_type' => $data['target_type'],
            'discount_scheme' => $data['discount_scheme'],
            'select_type' => $data['select_type'],
            'created_for' => $data['created_for']
        ];
    }

    protected function detailTransformer(array $data)
    {
        return [
            'id' => $data['id'] ?? '',
            'entity_type' => $this->getEntityTypeMorphKey($data['entity_type']),
            'entity_id' => $data['entity_id'],
            'discount_type' => $data['discount_type'],
            'discount_value' => $data['discount_value'],
            'qty' => $data['qty']
        ];
    }

    protected function targetTransformer(array $data)
    {
        return [
            'id' => $data['id'] ?? '',
            'target_id' => $data['target_id'],
            'target_type' => $this->getTargetTypeMorphKey($data['target_type'])
        ];
    }

    protected function getTargetTypeMorphKey($value) 
    {
        $class = '';
        switch($value) {
            case TargetType::ALL_CUSTOMERS: 
            case TargetType::SPECIFIC_CUSTOMER: 
                $class = Customer::class;
                break;
            case TargetType::ALL_MEMBERS: 
            case TargetType::SPECIFIC_MEMBER: 
                $class = Member::class;
                break;
            case TargetType::SPECIFIC_MEMBER_TYPE: 
                $class = MemberRate::class;
                break;
        }

        return $this->getMorphMapKey($class);
    }

    protected function getEntityTypeMorphKey($value) 
    {
        $class = '';
        switch($value) {
            case SelectType::ALL_PRODUCTS: 
            case SelectType::SPECIFIC_PRODUCT: 
                $class = Product::class;
                break;
            case SelectType::SPECIFIC_BRAND: 
                $class = Brand::class;
                break;
            case SelectType::SPECIFIC_CATEGORY: 
                $class = Category::class;
                break;
            case SelectType::SPECIFIC_SUPPLIER: 
                $class = Supplier::class;
                break;
        }

        return $this->getMorphMapKey($class);
    }
}
