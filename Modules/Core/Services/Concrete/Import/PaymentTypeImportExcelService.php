<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\PaymentTypeImportExcelServiceInterface;

class PaymentTypeImportExcelService extends SystemCodeImportExcelService implements PaymentTypeImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::PAYMENT_TYPE
    ];

    protected $autogenerate = [
        'code' => 'payment_type'
    ];
}