<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\BankImportExcelServiceInterface;

class BankImportExcelService extends SystemCodeImportExcelService implements BankImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::BANK
    ];

    protected $autogenerate = [
        'code' => 'bank'
    ];
}