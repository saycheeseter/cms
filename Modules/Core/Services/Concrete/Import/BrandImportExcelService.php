<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\BrandImportExcelServiceInterface;

class BrandImportExcelService extends SystemCodeImportExcelService implements BrandImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::BRAND
    ];

    protected $autogenerate = [
        'code' => 'brand'
    ];
}