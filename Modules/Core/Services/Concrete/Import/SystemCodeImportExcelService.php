<?php

namespace Modules\Core\Services\Concrete\Import;

use DB;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat as Format;

abstract class SystemCodeImportExcelService extends ImportExcelService
{
    protected $timestamps = true;
    protected $table = 'system_code';

    protected function mappings($data)
    {
        return [
            'type_id' => $this->attributes['type_id'],
            'code'    => $data->code,
            'name'    => $data->name,
            'remarks' => $data->remarks
        ];
    }

    protected function areFieldsValid($data)
    {
        $rules = [
            'code' => 'code',
            'name' => 'name'
        ];

        foreach ($rules as $column => $property) {
            $count = DB::table('system_code')
                ->whereNull('deleted_at')
                ->where('type_id', $this->attributes['type_id'])
                ->where($column, $data->$property)
                ->count();

            if ($count > 0) {
                $this->logExistingData($column, $data->$property);
                return false;
            }
        }

        return true;
    }

    protected function getHeadersForSampleTemplate()
    {
        return [
            'Code'    => Format::FORMAT_GENERAL,
            'Name'    => Format::FORMAT_GENERAL,
            'Remarks' => Format::FORMAT_GENERAL,
        ];
    }

    protected function getRequiredFields()
    {
        return [
            'name'
        ];
    }
}