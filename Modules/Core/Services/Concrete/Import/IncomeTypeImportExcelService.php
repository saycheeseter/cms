<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\IncomeTypeImportExcelServiceInterface;

class IncomeTypeImportExcelService extends SystemCodeImportExcelService implements IncomeTypeImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::INCOME_TYPE
    ];

    protected $autogenerate = [
        'code' => 'income_type'
    ];
}