<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\ReasonImportExcelServiceInterface;

class ReasonImportExcelService extends SystemCodeImportExcelService implements ReasonImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::REASON
    ];

    protected $autogenerate = [
        'code' => 'reason'
    ];
}