<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductManageType;
use Modules\Core\Enums\ProductSeniorDiscount;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\ProductImportExcelServiceInterface;
use Illuminate\Support\Str;
use DB;
use PHPExcel_Style_NumberFormat as Format;

class ProductImportExcelService extends ImportExcelService implements ProductImportExcelServiceInterface
{
    protected $timestamps = true;
    protected $userAudit = true;
    protected $branchAudit = true;
    protected $table = 'product';
    protected $autogenerate = [
        'stock_no' => 'product',
    ];

    protected function mappings($data)
    {
        $supplier = $this->findSupplierByCode($data->supplier_code);
        $brand = $this->findBrandByCode($data->brand_code);
        $category = $this->findCategoryByCode($data->category_code);

        switch ($data->senior) {
            case 5:
                $senior = ProductSeniorDiscount::SENIOR_5;
                break;

            case 20:
                $senior = ProductSeniorDiscount::SENIOR_20;
                break;

            default:
                $senior = ProductSeniorDiscount::NON_SENIOR;
                break;
        }

        return [
            'stock_no'     => $data->stock_no,
            'supplier_sku' => $data->supplier_sku,
            'name'         => $data->name,
            'description'  => $data->description,
            'supplier_id'  => $supplier->id,
            'brand_id'     => $brand->id,
            'category_id'  => $category->id,
            'chinese_name' => $data->chinese_name,
            'senior'       => $senior,
            'memo'         => $data->memo,
            'status'       => ProductStatus::ACTIVE,
            'manage_type'  => ProductManageType::NONE,
            'group_type'   => GroupType::NONE,
            'type'         => ProductType::REGULAR,
            'manageable'   => 1,
            'default_unit' => setting('default.unit')
        ];
    }

    protected function callback($id, $data)
    {
        $this->createBranchPriceData($id, $data);
        $this->createUnitData($id);
        $this->createBarcodesData($id, $data);
        $this->createBranchInfoData($id);
        $this->createBranchLocationData($id);
        $this->createTaxData($id);
        $this->createBranchSummaryData($id);
    }

    protected function getHeadersForSampleTemplate()
    {
        return [
            'Stock No'        => Format::FORMAT_TEXT,
            'Name'            => Format::FORMAT_GENERAL,
            'Chinese Name'    => Format::FORMAT_GENERAL,
            'Description'     => Format::FORMAT_GENERAL,
            'Barcode'         => Format::FORMAT_TEXT,
            'Senior'          => Format::FORMAT_GENERAL,
            'Cost'            => Format::FORMAT_NUMBER_00,
            'Wholesale Price' => Format::FORMAT_NUMBER_00,
            'Retail Price'    => Format::FORMAT_NUMBER_00,
            'Supplier SKU'    => Format::FORMAT_GENERAL,
            'Brand Code'      => Format::FORMAT_GENERAL,
            'Supplier Code'   => Format::FORMAT_GENERAL,
            'Category Code'   => Format::FORMAT_GENERAL,
        ];
    }

    protected function areFieldsValid($data)
    {
        if (!$this->isValidProductData($data)) {
            return false;
        }

        if (!empty($data->barcode)) {
            if (!$this->isValidBarcodeData($data)) {
                return false;
            }
        }

        if (!$this->isValidBrandCode($data)) {
            return false;
        }

        if (!$this->isValidSupplierCode($data)) {
            return false;
        }

        if (!$this->isValidCategoryCode($data)) {
            return false;
        }

        return true;
    }

    protected function getRequiredFields()
    {
        return [
            'name',
            'supplier_code',
            'brand_code',
            'category_code',
            'senior',
            'cost',
            'wholesale_price',
            'retail_price',
        ];
    }

    private function isValidProductData($data)
    {
        $fields = [
            'name' => 'name',
            'stock_no' => 'stock_no'
        ];

        foreach ($fields as $column => $property) {
            $count = DB::table('product')
                ->whereNull('deleted_at')
                ->where($column, $data->$property)
                ->count();

            if ($count > 0) {
                $this->logExistingData($column, $data->$property);
                return false;
            }
        }

        return true;
    }

    private function isValidBarcodeData($data)
    {
        $barcodes = explode(',', $data->barcode);

        $count = DB::table('product_barcode')
            ->whereIn('code', $barcodes)
            ->count();

        if ($count > 0) {
            $this->logExistingData('barcode', $data->barcode);
            return false;
        }

        return true;
    }

    private function isValidBrandCode($data)
    {
        $count = DB::table('system_code')
            ->where('type_id', SystemCodeType::BRAND)
            ->whereNull('deleted_at')
            ->where('code', $data->brand_code)
            ->count();

        if ($count === 0) {
            $this->logDataNotExisting('brand_code', $data->brand_code);
            return false;
        }

        return true;
    }

    private function isValidSupplierCode($data)
    {
        $count = DB::table('supplier')
            ->whereNull('deleted_at')
            ->where('code', $data->supplier_code)
            ->count();

        if ($count === 0) {
            $this->logDataNotExisting('supplier_code', $data->supplier_code);
            return false;
        }

        return true;
    }

    private function isValidCategoryCode($data)
    {
        $count = DB::table('category')
            ->whereNull('deleted_at')
            ->where('code', $data->category_code)
            ->count();

        if ($count === 0) {
            $this->logDataNotExisting('category_code', $data->category_code);
            return false;
        }

        return true;
    }

    private function findSupplierByCode($code)
    {
        return DB::table('supplier')
            ->whereNull('deleted_at')
            ->where('code', $code)
            ->first();
    }

    private function findBrandByCode($code)
    {
        return DB::table('system_code')
            ->where('type_id', SystemCodeType::BRAND)
            ->whereNull('deleted_at')
            ->where('code', $code)
            ->first();
    }

    private function findCategoryByCode($code)
    {
        return DB::table('category')
            ->whereNull('deleted_at')
            ->where('code', $code)
            ->first();
    }

    private function createBranchPriceData($id, $data)
    {
        $branches = DB::table('branch')
            ->whereNull('deleted_at')
            ->get();

        $collection = [];

        foreach ($branches as $branch) {
            $collection[] = [
                'branch_id'       => $branch->id,
                'product_id'      => $id,
                'purchase_price'  => $data->cost,
                'selling_price'   => $data->retail_price,
                'wholesale_price' => $data->wholesale_price,
                'price_a'         => 0,
                'price_b'         => 0,
                'price_c'         => 0,
                'price_d'         => 0,
                'price_e'         => 0,
                'editable'        => 0,
            ];
        }

        DB::table('branch_price')->insert($collection);
    }

    private function createUnitData($id)
    {
        DB::table('product_unit')->insert([
            'product_id' => $id,
            'uom_id'     => setting('default.unit'),
            'qty'        => 1,
            'length'     => 0,
            'width'      => 0,
            'height'     => 0,
            'weight'     => 0,
        ]);
    }

    private function createBarcodesData($id, $data)
    {
        $barcodes = !empty($data->barcodes)
            ? explode(',', $data->barcodes)
            : [];

        $collection = [];

        if (empty($barcodes)) {
             $collection[] = $this->appendSeriesColumns([
                 'code' => '',
                 'uom_id' => setting('default.unit'),
                 'product_id' => $id,
                 'memo' => ''
            ], ['code' => 'product_barcode']);
        } else {
            foreach ($barcodes as $barcode) {
                $collection[] = $this->appendSeriesColumns([
                    'code' => $barcode,
                    'uom_id' => setting('default.unit'),
                    'product_id' => $id,
                    'memo' => ''
                ], ['code' => 'product_barcode']);
            }
        }

        DB::table('product_barcode')->insert($collection);
    }

    private function createBranchInfoData($id)
    {
        $branches = DB::table('branch')
            ->whereNull('deleted_at')
            ->get();

        $collection = [];

        foreach ($branches as $branch) {
            $collection[] = [
                'branch_id'  => $branch->id,
                'product_id' => $id,
                'status'     => 1,
            ];
        }

        DB::table('product_branch_info')->insert($collection);
    }

    private function createBranchLocationData($id)
    {
        $locations = DB::table('branch_detail')
            ->whereNull('deleted_at')
            ->get();

        $collection = [];

        foreach ($locations as $location) {
            $collection[] = [
                'branch_detail_id' => $location->id,
                'product_id'       => $id,
                'min'              => 0,
                'max'              => 0
            ];
        }

        DB::table('product_branch_detail')->insert($collection);
    }

    private function createTaxData($id)
    {
        $vatable = DB::table('tax')
            ->where('name', 'VAT')
            ->first();

        DB::table('product_tax')->insert([
            'product_id' => $id,
            'tax_id' => $vatable->id
        ]);
    }

    private function createBranchSummaryData($id)
    {
        $locations = DB::table('branch_detail')
            ->whereNull('deleted_at')
            ->get();

        $collection = [];

        foreach ($locations as $location) {
            $collection[] = [
                'branch_detail_id' => $location->id,
                'branch_id'        => $location->branch_id,
                'product_id'       => $id,
                'qty'              => 0,
                'reserved_qty'     => 0,
                'requested_qty'    => 0,
                'current_cost'     => 0,
            ];
        }

        DB::table('product_branch_summary')->insert($collection);
    }
}