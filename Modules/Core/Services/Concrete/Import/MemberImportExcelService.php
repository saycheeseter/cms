<?php


namespace Modules\Core\Services\Concrete\Import;

use Carbon\Carbon;
use DB;
use Excel;
use Lang;
use Modules\Core\Services\Contracts\Import\MemberImportExcelServiceInterface;
use PHPExcel_Style_NumberFormat as Format;
use Illuminate\Support\Str;

class MemberImportExcelService extends ImportExcelService implements MemberImportExcelServiceInterface
{
    protected $timestamps = true;
    protected $userAudit = true;
    protected $branchAudit = true;
    protected $table = 'member';
    protected $autogenerate = [
        'card_id' => 'member',
    ];

    protected $dates = [
        'birth_date' => 'date',
        'expiration_date' => 'datetime',
        'registration_date' => 'datetime'
    ];

    public function mappings($data)
    {
        $memberRate = $this->findByMemberRateName($data->member_rate_name);
        $data->status = Str::lower($data->status) == 'active' ? 1 : 0;
        $data->gender = Str::lower($data->gender) == 'female' ? 1 : 0;

        return [
            'card_id'           => $data->card_id,
            'barcode'           => $data->barcode,
            'full_name'         => $data->full_name,
            'mobile'            => $data->mobile,
            'telephone'         => $data->telephone,
            'address'           => $data->address,
            'email'             => $data->email,
            'gender'            => $data->gender,
            'birth_date'        => $data->birth_date,
            'registration_date' => $data->registration_date,
            'expiration_date'   => $data->expiration_date,
            'status'            => $data->status,
            'memo'              => $data->memo,
            'rate_head_id'      => $memberRate->id
        ];
    }

    protected function areFieldsValid($data)
    {
        if (!$this->isValidMemberData($data)) {
            return false;
        }

        if (!$this->isValidMemberRate($data)) {
            return false;
        }

        if (!$this->isValidDateFields($data)) {
            return false;
        }

        return true;
    }

    protected function getRequiredFields()
    {
        return [
            'full_name',
            'barcode',
            'birth_date',
            'registration_date',
            'expiration_date',
            'member_rate_name',
        ];
    }

    private function isValidMemberData($data)
    {
        $fields = [
            'full_name' => 'full_name',
            'barcode' => 'barcode',
            'card_id' => 'card_id'
        ];

        foreach ($fields as $column => $property) {
            $count = DB::table($this->table)
                ->whereNull('deleted_at')
                ->where($column, $data->$property)
                ->count();

            if ($count > 0) {
                $this->logExistingData($column, $data->$property);
                return false;
            }
        }

        return true;
    }

    private function isValidMemberRate($data)
    {
        $count = DB::table('member_rate')
            ->whereNull('deleted_at')
            ->where('name', $data->member_rate_name)
            ->count();

        if ($count === 0) {
            $this->logDataNotExisting('member_rate_name', $data->member_rate_name);
            return false;
        }

        return true;
    }

    private function isValidDateFields($data)
    {
        if ($data->birth_date->greaterThanOrEqualTo(Carbon::now())) {
            $this->logMessage(Lang::get('core::validation.birth.date.invalid.value'));
            return false;
        }

        if ($data->registration_date->greaterThanOrEqualTo($data->expiration_date)) {
            $this->logMessage(Lang::get('core::validation.expiration.date.invalid.range'));
            return false;
        }

        return true;
    }

    private function findByMemberRateName($memberRateName)
    {
        return DB::table('member_rate')
            ->whereNull('deleted_at')
            ->where('name', $memberRateName)
            ->first();
    }

    protected function getHeadersForSampleTemplate()
    {
        return [
            'Card Id'           => Format::FORMAT_TEXT,
            'Barcode'           => Format::FORMAT_TEXT,
            'Full Name'         => Format::FORMAT_GENERAL,
            'Mobile'            => Format::FORMAT_TEXT,
            'Telephone'         => Format::FORMAT_TEXT,
            'Address'           => Format::FORMAT_GENERAL,
            'Email'             => Format::FORMAT_GENERAL,
            'Gender'            => Format::FORMAT_GENERAL,
            'Birth Date'        => Format::FORMAT_DATE_YYYYMMDD2,
            'Registration Date' => Format::FORMAT_DATE_YYYYMMDD2,
            'Expiration Date'   => Format::FORMAT_DATE_YYYYMMDD2,
            'Memo'              => Format::FORMAT_GENERAL,
            'Status'            => Format::FORMAT_GENERAL,
            'Member Rate Name'  => Format::FORMAT_GENERAL
        ];
    }

}