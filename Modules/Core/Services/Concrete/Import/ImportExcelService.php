<?php

namespace Modules\Core\Services\Concrete\Import;

use Carbon\Carbon;
use File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use DB;
use Lang;
use Exception;
use PHPExcel_Cell;

abstract class ImportExcelService
{
    protected $table;
    protected $timestamps = false;
    protected $userAudit = false;
    protected $branchAudit = false;
    protected $file;
    protected $batch = 100;
    protected $attributes = [];
    protected $logs = [];
    protected $autogenerate = [];
    protected $iteration = 0;
    protected $dates = [];

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        return $this;
    }

    public function execute()
    {
        $this->setDateColumns();

        Excel::filter('chunk')
            ->load($this->file->getRealPath())
            ->chunk($this->batch, function($results) {
                foreach ($results as $index => $row) {
                    if ($index == 0) {
                        if (!$this->isValidContentFormat($row)) {
                            throw new Exception(Lang::get('core::validation.invalid.content.format'));
                        }
                    }

                    $this->iteration = $index;

                    if (!$this->validations($row)) {
                        continue;
                    }

                    $formatted = $this->mappings($row);

                    if ($this->timestamps) {
                        $formatted = $this->appendTimestamps($formatted);
                    }

                    if ($this->userAudit) {
                        $formatted = $this->appendUserAudit($formatted);
                    }

                    if ($this->branchAudit) {
                        $formatted = $this->appendBranchAudit($formatted);
                    }

                    DB::transaction(function() use ($formatted, $row) {
                        if (count($this->autogenerate) > 0) {
                            $formatted = $this->appendAutoGenerateSeriesColumns($formatted);
                        }

                        $id = DB::table($this->table)->insertGetId($formatted);

                        $this->callback($id, $row);

                        $this->logsSuccess();
                    }, 5);
                }
            }, false);

        return $this;
    }

    public function exportDefaultTemplate()
    {
        Excel::create('sample_template', function($excel) {
            $excel->sheet('Sheet 1', function($sheet) {
                $columns = array_keys($this->getHeadersForSampleTemplate());

                $sheet->row(1, $columns);

                $formats = [];

                foreach ($this->getHeadersForSampleTemplate() as $column => $format) {
                    $formats[PHPExcel_Cell::stringFromColumnIndex(array_search($column, $columns))] = $format;
                }

                $sheet->setColumnFormat($formats);
                $sheet->rows([]);
            });
        })->export('xlsx');
    }

    public function getLogs()
    {
        return $this->logs;
    }

    protected function callback($id, $data)
    {
        return true;
    }

    protected function logsSuccess()
    {
        $this->logs[] = sprintf("%s: [%s]: %s",
            Lang::get('core::label.row'),
            ($this->iteration + 2),
            Lang::get('core::success.inserted')
        );

        return $this;
    }

    protected function logEmptyData($field)
    {
        $this->logs[] = sprintf("%s: [%s]: %s [%s]",
            Lang::get('core::error.row'),
            ($this->iteration + 2),
            Lang::get('core::validation.empty.data'),
            str_replace('_', ' ', Str::title($field))
        );

        return $this;
    }

    protected function logExistingData($field, $value)
    {
        $this->logs[] = sprintf("%s: [%s]: %s [%s : %s].",
            Lang::get('core::error.row'),
            ($this->iteration + 2),
            Lang::get('core::validation.data.exists'),
            str_replace('_', ' ', Str::title($field)),
            $value
        );

        return $this;
    }

    protected function logDataNotExisting($field, $value)
    {
        $this->logs[] = sprintf("%s: [%s]: %s [%s].",
            Lang::get('core::error.row'),
            ($this->iteration + 2),
            Lang::get('core::validation.data.doesnt.exists'),
            str_replace('_', ' ', Str::title($field)),
            $value
        );

        return $this;
    }

    protected function logMessage($message)
    {
        $this->logs[] = sprintf("%s: [%s]: %s",
            Lang::get('core::error.row'),
            ($this->iteration + 2),
            $message
        );

        return $this;
    }

    protected function appendSeriesColumns($formatted, $list)
    {
        foreach ($list as $column => $type) {
            $record = DB::table('module_series')
                ->where('type', $type)
                ->where('column', $column)
                ->lockForUpdate()
                ->first();

            $value = trim($formatted[$column]);

            if(($value == '' || is_null($value))) {
                $formatted[$column] = $record->series;
                $this->updateSeries($type, $column, $record->series);
            } else if(
                is_numeric($value)
                && $record->series <= $value
                && bccomp($record->limit, $value) == 1
            ) {
                $this->updateSeries($type, $column, $value);
            }
        }

        return $formatted;
    }

    private function updateSeries($type, $column, $value)
    {
        DB::table('module_series')
            ->where('type', $type)
            ->where('column', $column)
            ->update(['series' => bcadd($value, 1)]);
    }

    protected function areFieldsValid($data)
    {
        return true;
    }

    private function appendTimestamps($data)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');

        return array_merge($data, [
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }

    private function appendUserAudit($data)
    {
        return array_merge($data, [
            'created_by' => Auth::user()->id,
            'modified_by' => Auth::user()->id
        ]);
    }

    private function appendBranchAudit($data)
    {
        return array_merge($data, ['created_from' => Auth::branch()->id]);
    }

    private function appendAutoGenerateSeriesColumns($formatted)
    {
        return $this->appendSeriesColumns($formatted, $this->autogenerate);
    }

    private function validations($data)
    {
        if (count($this->getRequiredFields()) > 0) {
            if ($this->areFieldsEmpty($data)) {
                return false;
            }
        }

        if (!$this->areFieldsValid($data)) {
            return false;
        }

        return true;
    }

    private function areFieldsEmpty($data)
    {
        $fields = $this->getRequiredFields();

        foreach ($fields as $field) {
            if (trim($data->$field) === '') {
                $this->logEmptyData($field);
                return true;
            }
        }

        return false;
    }

    private function setDateColumns()
    {
        config(['excel.import.dates.columns' => array_keys($this->dates)]);
    }

    private function isValidContentFormat(CellCollection $data)
    {
        $fields = array_keys($this->getHeadersForSampleTemplate());

        foreach ($fields as $field) {
            $field = str_replace(' ', '_', Str::lower($field));

            if (!$data->has($field)) {
                return false;
            }
        }

        return true;
    }

    abstract protected function mappings($data);
    abstract protected function getHeadersForSampleTemplate();
    abstract protected function getRequiredFields();
}