<?php


namespace Modules\Core\Services\Concrete\Import;


use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\PaymentMethodImportExcelServiceInterface;

class PaymentMethodImportExcelService extends SystemCodeImportExcelService implements PaymentMethodImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::PAYMENT_METHOD
    ];
    protected $timestamps = true;
    protected $table = 'system_code';
}