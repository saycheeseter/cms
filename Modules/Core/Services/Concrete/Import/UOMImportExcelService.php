<?php

namespace Modules\Core\Services\Concrete\Import;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Services\Contracts\Import\UOMImportExcelServiceInterface;

class UOMImportExcelService extends SystemCodeImportExcelService implements UOMImportExcelServiceInterface
{
    protected $attributes = [
        'type_id' => SystemCodeType::UOM
    ];

    protected $autogenerate = [
        'code' => 'UOM'
    ];
}