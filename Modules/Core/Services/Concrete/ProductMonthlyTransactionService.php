<?php

namespace Modules\Core\Services\Concrete;

use Illuminate\Support\Str;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Contracts\GenerateAutoConversionProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateConversionProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateInventoryAdjustmentProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Enums\GroupType;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;
use DB;

class ProductMonthlyTransactionService implements ProductMonthlyTransactionServiceInterface
{
    public function generateFromTransaction(GenerateTransactionProductMonthlySummary $model)
    {
        $this->processFromTransaction($model, 'generate');
    }

    public function generateFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model)
    {
        $this->processFromInventoryAdjust($model, 'generate');
    }

    public function generateFromConversion(GenerateConversionProductMonthlySummary $model)
    {
        $this->processFromConversion($model, 'generate');
    }

    public function generateFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model)
    {
        $this->processFromAutoConversion($model, 'generate');
    }

    private function generate($identifiers, $fields)
    {

        $this->process($identifiers, $fields, '+');
    }

    private function revert($identifiers, $fields)
    {
        $this->process($identifiers, $fields, '-');
    }

    private function process($identifiers, $fields, $operator)
    {
        $summary = ProductMonthlyTransactions::take(1)
            ->where($identifiers)
            ->first();

        if (is_null($summary)) {
            $summary = new ProductMonthlyTransactions($identifiers);

            foreach ($fields as $attribute => $value) {
                $summary->{ $attribute } = $value;
            }

            $summary->save();
        } else {
            $columns = [];

            foreach ($fields as $attribute => $value) {
                $columns[$attribute] = DB::raw($attribute.$operator.$value);
            }

            $summary->where($identifiers)->update($columns);
        }
    }

    private function processFromTransaction(GenerateTransactionProductMonthlySummary $model, $method)
    {
        $details = $model->details;

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => null
        ];

        foreach ($details as $detail) {
            $fields = [];

            $identifiers['product_id'] = $detail->{ $model->getProductMonthlyTransactionProductIdColumn() };

            foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                $fields[$summaryField] = $detail->{ $itemField };
            }

            $this->$method($identifiers, $fields);
        }
    }

    private function processFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model, $method)
    {
        $details = $model->details;

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => null
        ];

        foreach ($details as $detail) {
            if(in_array($detail->group_type, [
                GroupType::AUTOMATIC_GROUP,
                GroupType::AUTOMATIC_UNGROUP
            ])) {
                continue;
            }

            $fields = [];

            $identifiers['product_id'] = $detail->{ $model->getProductMonthlyTransactionProductIdColumn() };

            foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                $fields[$summaryField] = $detail->{ $itemField } instanceof Decimal
                    ? $detail->{ $itemField }->abs()
                    : $detail->{ $itemField };
            }

            $this->$method($identifiers, $fields);
        }
    }

    private function processFromConversion(GenerateConversionProductMonthlySummary $model, $method)
    {
        if(!in_array($model->group_type, [
            GroupType::MANUAL_GROUP,
            GroupType::MANUAL_UNGROUP
        ])) {
            return;
        }

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $model->product_monthly_transaction_product_id
        ];

        $fields = [];

        foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
            $fields[$summaryField] = $model->{ $itemField };
        }

        $this->$method($identifiers, $fields);

        $details = $model->details;

        foreach ($details as $detail) {
            $identifiers['product_id'] = $detail->product_monthly_transaction_product_id;

            $fields = [];

            foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                $fields[$summaryField] = $detail->{ $itemField };
            }

            $this->$method($identifiers, $fields);
        }
    }

    private function processFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model, $method)
    {
        if(!in_array($model->group_type, [
            GroupType::AUTOMATIC_GROUP,
            GroupType::AUTOMATIC_UNGROUP
        ])) {
            return;
        }

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $model->product_monthly_transaction_product_id
        ];

        $fields = [];

        foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
            $fields[$summaryField] = $model->{ $itemField };
        }

        $this->$method($identifiers, $fields);

        $details = $model->details;

        foreach ($details as $detail) {
            $identifiers['product_id'] = $detail->product_monthly_transaction_product_id;

            $fields = [];

            foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                $fields[$summaryField] = $detail->{ $itemField };
            }

            $this->$method($identifiers, $fields);
        }
    }

    public function revertFromTransaction(GenerateTransactionProductMonthlySummary $model)
    {
        $this->processFromTransaction($model, 'revert');
    }

    public function revertFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model)
    {
        $this->processFromInventoryAdjust($model, 'revert');
    }

    public function revertFromConversion(GenerateConversionProductMonthlySummary $model)
    {
        $this->processFromConversion($model, 'revert');
    }

    public function revertFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model)
    {
        $this->processFromAutoConversion($model, 'revert');
    }

    public function generateSpecificFieldsFromTransaction(GenerateTransactionProductMonthlySummary $model, $columns)
    {
        $columns = array_wrap($columns);

        $details = $model->details;

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => null
        ];

        foreach ($details as $detail) {
            $fields = [];

            $identifiers['product_id'] = $detail->{ $model->getProductMonthlyTransactionProductIdColumn() };

            foreach ($columns as $column) {
                foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                    if (Str::contains($column, $summaryField)) {
                        $fields[$summaryField] = $detail->{ $itemField };
                    }
                }
            }

            $this->generate($identifiers, $fields);
        }
    }

    public function generateSpecificFieldsFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model, $columns)
    {
        $columns = array_wrap($columns);

        $details = $model->details;

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => null
        ];

        foreach ($details as $detail) {
            if(in_array($detail->group_type, [
                GroupType::AUTOMATIC_GROUP,
                GroupType::AUTOMATIC_UNGROUP
            ])) {
                continue;
            }

            $fields = [];

            $identifiers['product_id'] = $detail->{ $model->getProductMonthlyTransactionProductIdColumn() };

            foreach ($columns as $column) {
                foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                    if (Str::contains($column, $summaryField)) {
                        $fields[$summaryField] = $detail->{ $itemField } instanceof Decimal
                            ? $detail->{ $itemField }->abs()
                            : $detail->{ $itemField };
                    }
                }
            }

            $this->generate($identifiers, $fields);
        }
    }

    public function generateSpecificFieldsFromConversion(GenerateConversionProductMonthlySummary $model, $columns)
    {
        if(!in_array($model->group_type, [
            GroupType::MANUAL_GROUP,
            GroupType::MANUAL_UNGROUP
        ])) {
            return;
        }

        $columns = array_wrap($columns);

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $model->product_monthly_transaction_product_id
        ];

        $fields = [];

        foreach ($columns as $column) {
            foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                if (Str::contains($column, $summaryField)) {
                    $fields[$summaryField] = $model->{ $itemField };
                }
            }
        }

        $this->generate($identifiers, $fields);

        $details = $model->details;

        foreach ($details as $detail) {
            $identifiers['product_id'] = $detail->product_monthly_transaction_product_id;

            $fields = [];

            foreach ($columns as $column) {
                foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                    if (Str::contains($column, $summaryField)) {
                        $fields[$summaryField] = $detail->{$itemField};
                    }
                }
            }

            $this->generate($identifiers, $fields);
        }
    }

    public function generateSpecificFieldsFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model, $columns)
    {
        if(!in_array($model->group_type, [
            GroupType::AUTOMATIC_GROUP,
            GroupType::AUTOMATIC_UNGROUP
        ])) {
            return;
        }

        $columns = array_wrap($columns);

        $identifiers = [
            'created_for' => $model->product_monthly_transaction_created_for,
            'for_location' => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => null
        ];

        $details = $model->details;

        foreach ($details as $detail) {
            $identifiers['product_id'] = $detail->product_monthly_transaction_product_id;

            $fields = [];

            foreach ($columns as $column) {
                foreach ($detail->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
                    if (Str::contains($column, $summaryField)) {
                        $fields[$summaryField] = $detail->{ $itemField };
                    }
                }
            }

            $this->generate($identifiers, $fields);
        }
    }
}