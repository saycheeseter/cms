<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\Printer\Generator;
use Modules\Core\Services\Contracts\PrinterServiceInterface;

class PrinterService implements PrinterServiceInterface
{
    protected $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    public function template($data, $format)
    {
        return $this->generator
            ->data($data)
            ->format($format)
            ->generate('core::printout.template');
    }
}