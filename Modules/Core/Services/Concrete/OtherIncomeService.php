<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\OtherIncomeServiceInterface;
use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherIncomeDetail;

class OtherIncomeService extends OtherFinancialFlowService implements OtherIncomeServiceInterface
{
    protected function models()
    {
        return [
            'info'    => OtherIncome::class,
            'details' => OtherIncomeDetail::class,
        ];
    }
}