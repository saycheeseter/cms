<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\BranchPrice;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Services\Contracts\BranchPriceServiceInterface;
use Modules\Core\Enums\PurchaseInboundAutoUpdateCostSettings;

class BranchPriceService implements BranchPriceServiceInterface
{
    /**
     * Copy branch prices of main branch to another branch id
     *
     * @param  Branch  $branch
     */
    public function copy(Branch $branch)
    {
        $main = setting('main.branch');

        $limit = 170;

        for ($i = 0;;$i++) {
            $prices = BranchPrice::where('branch_id', '=', $main)
                ->skip(($i * $limit))
                ->take($limit)
                ->get();

            if ($prices->isEmpty()) {
                break;
            }

            $collection = [];

            foreach ($prices as $key => $price) {
                $collection[] = [
                    'branch_id'         => $branch->id,
                    'product_id'        => $price->product_id,
                    'purchase_price'    => $price->purchase_price,
                    'selling_price'     => $price->selling_price,
                    'wholesale_price'   => $price->wholesale_price,
                    'price_a'           => $price->price_a,
                    'price_b'           => $price->price_b,
                    'price_c'           => $price->price_c,
                    'price_d'           => $price->price_d,
                    'price_e'           => $price->price_e,
                    'editable'          => $price->editable,
                    'copy_from'         => $main,
                ];
            }

            BranchPrice::insert($collection);
        }
    }

    /**
     * Update the branch price of each product base from the details of Purchase Inbound
     * 
     * @param  PurchaseInbound $transaction
     * @return $this;
     */
    public function updatePurchasePriceFromTransaction(PurchaseInbound $transaction)
    {
        foreach ($transaction->details as $detail) {
            $builder = BranchPrice::where('product_id', $detail->product_id);

            switch(setting('purchase.inbound.auto.update.cost')) {
                case PurchaseInboundAutoUpdateCostSettings::OWN_BRANCH: 
                    $builder = $builder->where('branch_id', $transaction->created_for);
                    break;

                case PurchaseInboundAutoUpdateCostSettings::COPIED_TO_BRANCH: 
                    $builder = $builder->where(function($query) use ($transaction){
                        $query->where('branch_id', $transaction->created_for)
                            ->orWhere('copy_from', $transaction->created_for);
                    });
                    break;
            }
            
            $builder->update(['purchase_price' => $detail->price]);
        }
    }

    public function deleteFromBranch(Branch $branch) 
    {
        BranchPrice::where('branch_id', $branch->id)->delete();
    }
}
