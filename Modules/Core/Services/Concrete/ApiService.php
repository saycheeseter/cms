<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ApiServiceInterface;
use Modules\Core\Services\Contracts\ApiManagementServiceInterface as ApiManagementService;

abstract class ApiService implements ApiServiceInterface
{
    protected $api;

    public function __construct(ApiManagementService $api)
    {
        $this->api = $api;
    }
}