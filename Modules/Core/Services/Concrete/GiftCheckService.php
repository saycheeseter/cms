<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\GiftCheckServiceInterface;
use Modules\Core\Entities\GiftCheck;
use Modules\Core\Enums\GiftCheckStatus;
use Carbon\Carbon;
use Auth;
use App;

class GiftCheckService extends BaseService implements GiftCheckServiceInterface
{
    protected function model()
    {
        return GiftCheck::class;
    }

    /**
     * Change the given check id's status as redeemed
     *
     * @param $id
     * @param $referenceNumber
     * @return mixed
     */
    public function redeem($id, $referenceNumber)
    {   
        $model = App::make($this->model())->findOrFail($id);

        $model->fill([
            'status' => GiftCheckStatus::REDEEMED,
            'redeemed_date' => Carbon::now(),
            'redeemed_by' => Auth::user()->id,
            'reference_number' =>  $referenceNumber
        ]);

        $model->save();

        return $this;
    }

    public function bulkEntry($data)
    {
        return $this->transaction(function() use ($data) {
            $collection = [];

            $checkNumberStart = $data['check_number_series'];
            $codeStart = $data['code_series'];

            for ($i = 0; $i < $data['qty']; $i++) {
                $collection[] = [
                    'code'         => bcadd($codeStart, $i),
                    'check_number' => bcadd($checkNumberStart, $i),
                    'amount'       => $data['amount'],
                    'date_from'    => Carbon::parse($data['date_from']),
                    'date_to'      => Carbon::parse($data['date_to']),
                ];
            }

            return $this->bulkStore($collection);
        });
    }

    protected function transform($data)
    {
        return [
            'code'         => $data['code'],
            'check_number' => $data['check_number'],
            'amount'       => $data['amount'],
            'date_from'    => Carbon::parse($data['date_from']),
            'date_to'      => Carbon::parse($data['date_to']),
        ];
    }
}