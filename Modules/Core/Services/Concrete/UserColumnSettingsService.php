<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\UserColumnSettingsServiceInterface;
use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Entities\UserColumnSettings;
use Auth;

class UserColumnSettingsService extends BaseService implements UserColumnSettingsServiceInterface
{
    protected function model()
    {
        return UserColumnSettings::class;
    }

    public function save($module, $settings)
    {
        $data = [
            'user_id' => Auth::user()->id,
            'module_id' => $module
        ];

        $settings = [
            'settings' => $settings
        ];

        return $this->updateOrCreate($data, $settings);
    }
}