<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Events\SalesOutboundApproved;
use Modules\Core\Events\SalesOutboundReverted;
use Modules\Core\Services\Contracts\SalesOutboundServiceInterface;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesOutboundDetail;

class SalesOutboundService extends InventoryChainService implements SalesOutboundServiceInterface
{
    protected $events = [
        'approved' => SalesOutboundApproved::class,
        'revert'   => SalesOutboundReverted::class
    ];

    protected function models()
    {
        $this->info = SalesOutbound::class;
        $this->details = SalesOutboundDetail::class;
    }
    
    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'customer_id' => $data['customer_id'],
            'customer_detail_id' => $data['customer_detail_id'],
            'salesman_id' => $data['salesman_id'],
            'customer_type' => $data['customer_type'],
            'customer_walk_in_name' => $data['customer_walk_in_name'],
            'term' => $data['term'],
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'salesman_id' => $data['salesman_id'],
            'term' => $data['term']
        ]);
    }
}
