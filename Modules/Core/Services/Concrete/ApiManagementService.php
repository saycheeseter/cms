<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ApiManagementServiceInterface;
use kamermans\OAuth2\Persistence\FileTokenPersistence;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\GrantType\NullGrantType;
use kamermans\OAuth2\Token\RawToken;
use kamermans\OAuth2\OAuth2Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;

class ApiManagementService implements ApiManagementServiceInterface
{
    private $method = 'GET';

    private $route = '';

    private $parameters = [];

    private $client;

    private $response;

    public function __construct()
    {
        //Initialize Client Token
        $tokenPath = storage_path('app/client_token.json');
        $tokenPersistence = new FileTokenPersistence($tokenPath);

        //Holds oauth authentication
        $stack = HandlerStack::create();
        
        $oauth = '';

        //Checks if token exists
        if(!file_exists(storage_path('app/client_token.json'))) {
            //Request for token and persists it
            $oauth = $this->requestToken($tokenPersistence);
        } else {
            //Retrieve persisted token
            $oauth = $this->retrieveToken($tokenPersistence);
        }
        
        //Adds client token to oauth holder
        $stack->push($oauth);

        //Creates Guzzle Client
        $this->client = new Client([
            'handler' => $stack,
            'auth'    => 'oauth',
        ]);
    }

    public function setMethod(string $method = 'get')
    {
        $this->method = $method;

        return $this;
    }

    public function setRoute(string $route)
    {
        $this->route = $route;

        return $this;
    }

    public function setParameters($parameters = [])
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function request()
    {
        $this->response = $this->client->request(
            $this->method,
            config('management.api').$this->route,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'http_errors' => false, //to avoid guzzle from catching errorResponse as errors
                'json' => $this->parameters
            ]
        );

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getJsonDecodeResponse($assoc = true)
    {
        return json_decode($this->response->getBody(), $assoc);
    }

    private function requestToken(FileTokenPersistence $tokenPersistence)
    {
        //Guzzle Client for token request server
        $reauthClient = new Client([
            'base_uri' => config('management.token_server'),
        ]);

        //Client Credentials
        $reauthConfig = [
            "client_id" => config('management.oauth_client_id'),
            "client_secret" => config('management.oauth_client_secret')
        ];

        $grantType = new ClientCredentials($reauthClient, $reauthConfig);
        $oauth = new OAuth2Middleware($grantType);
        $oauth->setTokenPersistence($tokenPersistence);

        return $oauth;
    }

    private function retrieveToken(FileTokenPersistence $tokenPersistence)
    {
        $clientToken = $tokenPersistence->restoreToken(new RawToken());
        
        $oauth = new OAuth2Middleware(new NullGrantType);
        $oauth->setAccessToken($clientToken);

        return $oauth;
    }
}