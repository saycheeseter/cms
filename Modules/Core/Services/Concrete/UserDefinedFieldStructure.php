<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\UserDefinedFieldStructureInterface;
use Storage;

class UserDefinedFieldStructure implements UserDefinedFieldStructureInterface
{
    private $structure;

    public function __construct()
    {
        $this->structure = Storage::disk('local')->exists('udf.json')
            ? json_decode(Storage::disk('local')->get('udf.json'), true)
            : [];
    }

    public function getColumns($table)
    {
        if (!array_key_exists($table, $this->structure)) {
            return [];
        }

        return $this->structure[$table];
    }

    public function getColumnType($table, $column)
    {
        if (!array_key_exists($table, $this->structure) || !array_key_exists($column, $this->structure[$table])) {
            return '';
        }

        return $this->structure[$table][$column];
    }

    public function getTables()
    {
        return array_keys($this->structure);
    }
}