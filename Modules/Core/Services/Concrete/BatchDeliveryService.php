<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Contracts\BatchTransaction;
use Modules\Core\Services\Contracts\BatchDeliveryServiceInterface;
use DB;

class BatchDeliveryService implements BatchDeliveryServiceInterface
{
    /**
     * Update the Delivery / Return InboundDetail's reference batch remaining qty
     *
     * @param BatchTransaction $transaction
     */
    public function decrementReferenceBatchRemainingQty(BatchTransaction $transaction)
    {
        $this->processReferenceBatchRemainingQty($transaction, '-');
    }

    public function incrementReferenceBatchRemainingQty(BatchTransaction $transaction)
    {
        $this->processReferenceBatchRemainingQty($transaction, '+');
    }

    private function processReferenceBatchRemainingQty(BatchTransaction $transaction, $operator)
    {
        foreach ($transaction->details as $detail) {
            if ($detail->batches->count() === 0) {
                continue;
            }

            $reference = $detail->reference;

            foreach ($detail->batches as $batch) {
                $reference->batches()->updateExistingPivot($batch->id, [
                    'remaining_qty' => DB::raw('remaining_qty ' . $operator . $batch->pivot->qty->innerValue())
                ]);
            }
        }
    }
}