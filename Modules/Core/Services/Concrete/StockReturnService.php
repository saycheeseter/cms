<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockReturnServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockReturnDetail;
use Carbon\Carbon;

class StockReturnService extends InvoiceService implements StockReturnServiceInterface
{
    protected function models()
    {
        $this->info = StockReturn::class;
        $this->details = StockReturnDetail::class;
    }

    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data),[
            'deliver_to' => $data['deliver_to'],
            'deliver_to_location' => $data['deliver_to_location'],
        ]);
    }
}