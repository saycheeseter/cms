<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\Supplier;
use Modules\Core\Entities\TransactionSerialNumberLogs;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface;
use Modules\Core\Entities\ProductSerialNumber;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\Contracts\SeriableTransaction;
use Carbon\Carbon;

class ProductSerialNumberService implements ProductSerialNumberServiceInterface
{
    /**
     * Create or Update serials based on the Purchase Inbound data
     * 
     * @param  PurchaseInbound $transaction
     * @param  int $product
     * @param  array $serials
     * @return array
     */
    public function createOrUpdateFromTransaction(PurchaseInbound $transaction, $product, $serials)
    {
        $models = [];

        foreach ($serials as $serial) {
            $model = ProductSerialNumber::updateOrCreate(
                ['id' => $serial['id'] ?? 0],
                 $this->transformSerial($serial)
                + ['product_id' => $product, 'supplier_id' => $transaction->supplier_id]
            );

            $models[] = $model;
        }

        return collect($models);
    }

    /**
     * Create or Update serials based on the given product and serial data
     *
     * @param  int $product
     * @param  array $serials
     * @return array
     */
    public function createOrUpdate($product, $serials)
    {
        $models = [];

        $supplier = Supplier::where('code', 0)
            ->where('full_name', 'None')
            ->first();

        foreach ($serials as $serial) {
            $model = ProductSerialNumber::updateOrCreate(
                ['id' => $serial['id'] ?? 0],
                $this->transformSerial($serial)
                + ['product_id' => $product, 'supplier_id' => $supplier->id]
            );

            $models[] = $model;
        }

        return collect($models);
    }

    /**
     * Updates the status of the serial numbers based on the model info
     *
     * @param SeriableTransaction $transaction
     * @return bool
     */
    public function updateStatusFromTransaction(SeriableTransaction $transaction)
    {
        $serials = [];

        foreach ($transaction->details as $key => $detail) {
            $serials = array_merge($serials,  $detail->serials->pluck('id')->toArray());
        }

        if (count($serials) === 0) {
            return false;
        }

        ProductSerialNumber::whereIn('id', $serials)
            ->update([
                'status'      => $transaction->serial_status_assignment,
                'transaction' => $transaction->serial_transaction_assignment,
                'owner_type'  => $transaction->serial_owner_type_assignment,
                'owner_id'    => $transaction->serial_owner_id_assignment,
                'updated_at'  => Carbon::now()
            ]);
    }

    /**
     * Updates the status of the serial numbers based on the inventory adjust
     *
     * @param InventoryAdjust $transaction
     * @return bool
     */
    public function updateStatusFromInventoryAdjust(InventoryAdjust $transaction)
    {
        $attached = [];
        $detached = [];

        foreach ($transaction->details as $key => $detail) {
            switch ($detail->process_flow) {
                case 'adjustment_increase':
                    $attached = array_merge($attached,  $detail->serials->pluck('id')->toArray());
                    break;

                case 'adjustment_decrease':
                    $detached = array_merge($detached,  $detail->serials->pluck('id')->toArray());
                    break;
            }
        }

        if (count($attached) > 0) {
            ProductSerialNumber::whereIn('id', $attached)
                ->update([
                    'status'      => SerialStatus::AVAILABLE,
                    'transaction' => SerialTransaction::INVENTORY_ADJUST_INCREASE,
                    'owner_type'  => $transaction->serial_owner_type_assignment,
                    'owner_id'    => $transaction->serial_owner_id_assignment,
                    'updated_at'  => Carbon::now()
                ]);
        }

        if (count($detached) > 0) {
            ProductSerialNumber::whereIn('id', $detached)
                ->update([
                    'status'      => SerialStatus::NOT_AVAILABLE,
                    'transaction' => SerialTransaction::INVENTORY_ADJUST_DECREASE,
                    'owner_type'  => $transaction->serial_owner_type_assignment,
                    'owner_id'    => null,
                    'updated_at'  => Carbon::now()
                ]);
        }
    }

    /**
     * Transform serial number data
     * 
     * @param  array  $data
     * @return array
     */
    protected function transformSerial(array $data)
    {
        return [
            'serial_number'               => $data['serial_number'],
            'expiration_date'             => !is_null($data['expiration_date'])
                ? Carbon::parse($data['expiration_date'])
                : null,
            'manufacturing_date'          => !is_null($data['manufacturing_date'])
                ? Carbon::parse($data['manufacturing_date'])
                : null,
            'admission_date'              => !is_null($data['admission_date'])
                ? Carbon::parse($data['admission_date'])
                : null,
            'manufacturer_warranty_start' => !is_null($data['manufacturer_warranty_start'])
                ? Carbon::parse($data['manufacturer_warranty_start'])
                : null,
            'manufacturer_warranty_end'   => !is_null($data['manufacturer_warranty_end'])
                ? Carbon::parse($data['manufacturer_warranty_end'])
                : null,
            'remarks'                     => $data['remarks'] ?? null
        ];
    }

    public function revertStatusFromTransaction(SeriableTransaction $transaction)
    {
        $serials = [];

        foreach ($transaction->details as $key => $detail) {
            $serials = array_merge($serials,  $detail->serials->pluck('id')->toArray());
        }

        if (count($serials) === 0) {
            return false;
        }

        $cursor = ProductSerialNumber::whereIn('id', $serials)->cursor();

        foreach ($cursor as $serial) {
            $log = TransactionSerialNumberLogs::where('serial_id', $serial->id)
                ->orderBy('approval_date', 'DESC')
                ->take(1)
                ->first();

            $transaction = $log ? $log->transaction : null;

            $serial->status = $transaction->serial_status_assignment ?? SerialStatus::NOT_AVAILABLE;
            $serial->transaction = $transaction->serial_transaction_assignment ?? 0;
            $serial->owner_type = $transaction->serial_owner_type_assignment ?? null;
            $serial->owner_id = $transaction->serial_owner_id_assignment ?? null;
            $serial->save();
        }

        return $this;
    }

    public function revertStatusFromInventoryAdjust(InventoryAdjust $transaction)
    {
        $attached = [];
        $detached = [];

        foreach ($transaction->details as $key => $detail) {
            switch ($detail->process_flow) {
                case 'adjustment_increase':
                    $attached = array_merge($attached,  $detail->serials->pluck('id')->toArray());
                    break;

                case 'adjustment_decrease':
                    $detached = array_merge($detached,  $detail->serials->pluck('id')->toArray());
                    break;
            }
        }

        if (count($attached) > 0) {
            $cursor = ProductSerialNumber::whereIn('id', $attached)->cursor();

            foreach ($cursor as $serial) {
                $serial->status = SerialStatus::NOT_AVAILABLE;
                $serial->transaction = 0;
                $serial->owner_type = null;
                $serial->owner_id = null;
                $serial->save();
            }
        }

        if (count($detached) > 0) {
            $cursor = ProductSerialNumber::whereIn('id', $detached)->cursor();

            foreach ($cursor as $serial) {
                $log = TransactionSerialNumberLogs::where('serial_id', $serial->id)
                    ->orderBy('approval_date', 'DESC')
                    ->take(1)
                    ->first();

                $transaction = $log ? $log->transaction : null;

                $status = $transaction->serial_status_assignment ?? SerialStatus::NOT_AVAILABLE;
                $transactionType = $transaction->serial_transaction_assignment ?? 0;

                if ($transaction instanceof InventoryAdjust) {
                    $status = SerialStatus::AVAILABLE;
                    $transactionType = SerialTransaction::INVENTORY_ADJUST_INCREASE;
                }

                $serial->status = $status;
                $serial->transaction = $transactionType;
                $serial->owner_type = $transaction->serial_owner_type_assignment ?? null;
                $serial->owner_id = $transaction->serial_owner_id_assignment ?? null;
                $serial->save();
            }
        }

        return $this;
    }
}
