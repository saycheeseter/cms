<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as Application;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Services\Contracts\GenericSettingServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Exception;
use Modules\Core\Entities\GenericSetting;
use Auth;

class GenericSettingService implements GenericSettingServiceInterface
{
    use DatabaseTransaction;

    /**
     * Persist entity in database
     *
     * @param array $data
     * @return Model
     */
    public function store(array $data)
    {
        $this->transaction(function() use ($data) {
            foreach ($data as $key => $value) {

                $key = str_replace('_', '.', $key);

                $setting = GenericSetting::create([
                    'key' => $key,
                    'value' => $value,
                    'type' => 'string'
                ]);
            }
        });
    }

    /**
     * Persist entity in database
     *
     * @param array $data
     * @return Model
     */
    public function update(array $data)
    {
        $excluded = [
            'transaction.summary.computation.last.post.date'
        ];

        $this->transaction(function() use ($data, $excluded) {
            $data = collect($data)->mapWithKeys(function ($item, $key) {
                return [str_replace('_', '.', $key) => $item];
            });

            foreach ($data as $key => $value) {
                if (in_array($key, $excluded)) {
                    continue;
                }

                $model = GenericSetting::where('key', $key)->first();

                if ($model) {
                    $model->value = $value;
                    $this->beforeUpdate($model);
                    $model->save();
                }
            }
        });
    }

    private function beforeUpdate($model)
    {
        switch ($model->key) {
            case 'transaction.summary.computation.mode':
                if ($model->value == TransactionSummaryComputation::MANUAL
                    && $model->getOriginal('value') == TransactionSummaryComputation::AUTOMATIC
                ) {
                    GenericSetting::where('key', 'transaction.summary.computation.last.post.date')
                        ->update(['value' => Carbon::now()->toDateString()]);
                }
                break;
        }
    }
}
