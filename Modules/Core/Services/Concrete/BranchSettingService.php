<?php

namespace Modules\Core\Services\Concrete;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as Application;
use Modules\Core\Services\Contracts\BranchSettingServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Exception;
use Modules\Core\Entities\BranchSetting;
use Modules\Core\Entities\Branch;
use Auth;

class BranchSettingService implements BranchSettingServiceInterface
{
    use DatabaseTransaction;

    /**
     * Persist entity in database
     * 
     * @param  array  $attributes
     * @return Model            
     */
    public function store(array $data)
    {
        $this->transaction(function() use ($data) {
            foreach ($data as $key => $value) {

                $key = str_replace('_', '.', $key);

                $setting = BranchSetting::create([
                    'branch_id' => Auth::branch()->id,
                    'key' => $key,
                    'value' => $value,
                    'type' => 'string',
                ]);
            }
        });
    }

    /**
     * Persist entity in database
     *
     * @param  array  $attributes
     * @return Model
     */
    public function update(array $data)
    {
        $this->transaction(function() use ($data) {
            $data = collect($data)->mapWithKeys(function ($item, $key) {
                return [str_replace('_', '.', $key) => $item];
            });

            foreach ($data as $key => $value) {
                $model = BranchSetting::where('key', $key)
                    ->where('branch_id', Auth::branch()->id)
                    ->first();

                if ($model) {
                    $model->value = $value;
                    $model->save();
                }
            }
        });
    }

    /**
     * Copy settings of main branch to another branch id
     *
     * @param  Branch  $branch
     * @return Model
     */
    public function copy(Branch $branch)
    {
        $main = setting('main.branch');

        $settings = BranchSetting::where('branch_id', '=', $main)->get();

        $collection = [];

        foreach ($settings as $setting) {
            $collection[] = [
                'type'      => $setting->type,
                'branch_id' => $branch->id,
                'key'       => $setting->key,
                'value'     => $setting->value,
                'comments'  => $setting->comments,
            ];
        }

        BranchSetting::insert($collection);
    }

    public function deleteFromBranch(Branch $branch) 
    {
        BranchSetting::where('branch_id', $branch->id)->delete();
    }
}
