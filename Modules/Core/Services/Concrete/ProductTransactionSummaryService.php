<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Contracts\TransactionSummary;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface;
use Modules\Core\Entities\ProductTransactionSummary;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Enums\GroupType;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Traits\HasMorphMap;
use Carbon\Carbon;

class ProductTransactionSummaryService implements ProductTransactionSummaryServiceInterface
{
    use HasMorphMap;

    public function summarizeTransaction(TransactionSummary $model) 
    {
        return $this->processTransaction($model, 'increment');
    }

    public function revertTransaction(TransactionSummary $model) 
    {
        return $this->processTransaction($model, 'decrement');
    }

    /**
     * Process transaction models and create or update the record of the summary on per day basis
     * 
     * @param  TransactionSummary $model
     * @return $this
     */
    private function processTransaction(TransactionSummary $model, $method)
    {
        $details = $model->details;

        $column = $model->transaction_summary_field;

        $attributes = [
            'created_for' => $model->created_for,
            'for_location' => $model->for_location,
            'transaction_date' => Carbon::parse($model->transaction_date)->toDateString()
        ];

        foreach ($details as $detail) {
            $attributes['product_id'] = $detail->product_id;

            $this->createOrUpdate($attributes, $column, $detail->added_inventory_value->abs(), $method);
        }

        return $this;
    }

    public function summarizeProductConversion(ProductConversion $model)
    {
        $this->processProductConversion($model, 'increment');
    }

    public function revertProductConversion(ProductConversion $model)
    {
        $this->processProductConversion($model, 'decrement');
    }

    /**
     * Create a transaction summary data based on Product Conversion Entity
     *
     * @param ProductConversion $model
     * @return $this
     */
    private function processProductConversion(ProductConversion $model, $method)
    {
        $headColumn = 'product_conversion_increase';
        $detailColumn = 'product_conversion_decrease';

        if($model->group_type == GroupType::MANUAL_UNGROUP) {
            $headColumn = 'product_conversion_decrease';
            $detailColumn = 'product_conversion_increase';
        }

        $attributes = [
            'created_for' => $model->created_for,
            'for_location' => $model->for_location,
            'transaction_date' => Carbon::parse($model->transaction_date)->toDateString(),
            'product_id' => $model->product_id
        ];

        $this->createOrUpdate($attributes, $headColumn, $model->qty, $method);

        foreach($model->details as $detail) {
            $attributes['product_id'] = $detail->component_id;

            $this->createOrUpdate($attributes, $detailColumn, $detail->total_qty, $method);
        }

        return $this;
    }

    public function summarizeAutoProductConversion(AutoProductConversion $model)
    {
        $this->processAutoProductConversion($model, 'increment');
    }

    public function revertAutoProductConversion(AutoProductConversion $model)
    {
        $this->processAutoProductConversion($model, 'decrement');
    }

    /**
     * Create a transaction summary data based on Auto Product Conversion Entity
     *
     * @param AutoProductConversion $model
     * @return $this
     */
    private function processAutoProductConversion(AutoProductConversion $model, $method)
    {
        $attributes = [
            'created_for' => $model->created_for,
            'for_location' => $model->for_location,
            'transaction_date' => Carbon::parse($model->transaction_date)->toDateString(),
            'product_id' => $model->product_id
        ];

        $column = $model->transaction_summary_field;

        $this->createOrUpdate($attributes, $column, $model->qty, $method);

        $details = $model->details;

        foreach ($details as $detail) {
            $attributes['product_id'] = $detail->product_id;

            $this->createOrUpdate($attributes, $detail->transaction_summary_field, $detail->total_qty, $method);
        }

        return $this;
    }


    /**
     * Create or update Product Transaction Summary data based on attributes on column specified
     *
     * @param $attributes
     * @param $column
     * @param Decimal $value
     */
    public function createOrUpdate($attributes, $column, Decimal $value, $method = 'increment')
    {
        $record = ProductTransactionSummary::firstOrNew($attributes);

        if(!$record->exists) {
            $record->$column = $value;
            $record->save();
        } else {
            $record->getQuery()
                ->where('created_for', '=', $attributes['created_for'])
                ->where('for_location', '=', $attributes['for_location'])
                ->where('transaction_date', '=', $attributes['transaction_date'])
                ->where('product_id', '=', $attributes['product_id'])
                ->$method($column, $value->innerValue());
        }
    }

    public function summarizeInventoryAdjust(InventoryAdjust $model) 
    {
        $this->processInventoryAdjust($model, 'increment');
    }

    public function revertInventoryAdjust(InventoryAdjust $model)
    {
        $this->processInventoryAdjust($model, 'decrement');
    }

    /**
     * Create a Product Branch Summary based on Inventory Adjust data
     *
     * @param InventoryAdjust $model
     * @return $this
     */
    private function processInventoryAdjust(InventoryAdjust $model, $method)
    {
        $details = $model->details;

        $attributes = [
            'created_for' => $model->created_for,
            'for_location' => $model->for_location,
            'transaction_date' => Carbon::parse($model->transaction_date)->toDateString()
        ];

        foreach ($details as $detail) {
            if((in_array($detail->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP]))) {
                continue;
            }

            $attributes['product_id'] = $detail->product_id;

            $this->createOrUpdate($attributes, $detail->transaction_summary_field, $detail->added_inventory_value->abs(), $method);
        }

        return $this;
    }
}