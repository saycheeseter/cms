<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\PaymentMethodServiceInterface;
use Modules\Core\Entities\PaymentMethod;

class PaymentMethodService extends BaseService implements PaymentMethodServiceInterface
{
    protected function model()
    {
        return PaymentMethod::class;
    }
}
