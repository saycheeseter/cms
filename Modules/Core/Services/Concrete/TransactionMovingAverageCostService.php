<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\Contracts\ConversionMovingAverageCost;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Enums\GroupType;
use Modules\Core\Repositories\Contracts\ProductBranchSummaryRepository;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use App;

class TransactionMovingAverageCostService implements TransactionMovingAverageCostServiceInterface
{
    private $repository;

    public function __construct(ProductBranchSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Updates the transaction details' ma_cost attribute based on the latest current_cost
     * of the Product Branch Summary
     *
     * @param TransactionMovingAverageCost $model
     * @return $this
     */
    public function updateTransactionDetails(TransactionMovingAverageCost $model)
    {
        $details = $model->details->whereNotIn('group_type', [
            GroupType::AUTOMATIC_GROUP,
            GroupType::AUTOMATIC_UNGROUP,
        ]);

        if ($details->count() === 0) {
            return $model;
        }
        
        $productIds = $details->pluck('product_id')->toArray();

        $summaries = $this->repository->findByFilters(
            $productIds,
            $model->created_for,
            $model->for_location
        );

        $details->each(function ($detail, $key) use ($summaries) {
            $summary = $summaries->firstWhere('product_id', $detail->product_id);

            $detail->ma_cost = $summary->current_cost;
            $detail->save();
        });

        return $model->fresh();
    }

    /**
     * Update the transaction's and details ma_cost based on the current_cost of
     * Product Branch Summary
     *
     * @param ConversionMovingAverageCost $model
     * @return $this
     */
    public function updateManualConversion(ConversionMovingAverageCost $model)
    {
        return $this->updateConversionDetails($model, 'component_id');
    }

    /**
     * Update the details' ma_cost based on the current_cost of Product Branch Summary
     *
     * @param ConversionMovingAverageCost $model
     * @return $this
     */
    public function updateAutoConversion(ConversionMovingAverageCost $model)
    {
        return $this->updateConversionDetails($model, 'product_id');
    }

    protected function updateConversionDetails($model, $field)
    {
        $collection = [];

        $details = $model->details;

        $productIds = $details->pluck($field)->toArray();

        $summaries = $this->repository->findByFilters(
            $productIds,
            $model->created_for,
            $model->for_location
        );

        $details->each(function ($detail, $key) use ($summaries, $field, $collection) {
            $summary = $summaries->firstWhere('product_id', $detail->$field);

            $detail->ma_cost = $summary->current_cost;
            $collection[] = $detail->save();
        });

        return $model->fresh();
    }
}