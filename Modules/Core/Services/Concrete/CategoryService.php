<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\CategoryServiceInterface;
use Modules\Core\Entities\Category;

class CategoryService extends BaseService implements CategoryServiceInterface
{
    protected function model()
    {
        return Category::class;
    }
}