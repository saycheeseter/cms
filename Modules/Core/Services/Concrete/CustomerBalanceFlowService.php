<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\CustomerBalanceFlowServiceInterface;
use Modules\Core\Entities\CustomerBalanceFlow;
use Modules\Core\Entities\Collection;
use Modules\Core\Enums\FlowType;
use App;

class CustomerBalanceFlowService extends BalanceFlowService implements CustomerBalanceFlowServiceInterface
{
    public function __construct(CustomerBalanceFlow $model)
    {
        $this->model = $model;
    }

    /**
     * Formats Collection instance model to a Customer Balance Flow data by using the from balance data
     * 
     * @param  Collection $model
     * @return CustomerBalanceFlow
     */
    public function fromBalance(Collection $model)
    {
        $attributes = [
            'transactionable_id'   => $model->id,
            'transactionable_type' => 'collection',
            'customer_id'          => $model->customer_id,
            'flow_type'            => FlowType::OUT,
            'transaction_date'     => $model->transaction_date,
            'amount'               => $model->total_from_balance
        ];

        return $this->create($attributes);
    }

    /**
     * Formats Collection instance model to a Customer Balance Flow data by using the excess amount data
     * 
     * @param  Collection $model
     * @return CustomerBalanceFlow
     */
    public function fromExcess(Collection $model)
    {
        $attributes = [
            'transactionable_id'   => $model->id,
            'transactionable_type' => 'collection',
            'customer_id'          => $model->customer_id,
            'flow_type'            => FlowType::IN,
            'transaction_date'     => $model->transaction_date,
            'amount'               => $model->total_excess_amount
        ];

        return $this->create($attributes);
    }

    /**
     * Shortcut method for deleting a Customer Balance Flow record using the Collection as the
     * transaction type
     * 
     * @param  int $id
     * @return CustomerBalanceFlow
     */
    public function deleteByCollection($id)
    {
        return $this->delete($id, 'collection');
    }
}