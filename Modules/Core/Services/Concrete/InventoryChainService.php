<?php

namespace Modules\Core\Services\Concrete;

use Illuminate\Container\Container as Application;
use Carbon\Carbon;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\TransactionSource;

abstract class InventoryChainService extends InventoryFlowService
{
    /**
     * Store data to parent and child model, and create new serials
     *
     * @param  array  $attributes
     * @return Model
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $transaction = $that->createOrUpdate($attributes);

            $that->attachItemManagement($attributes, $transaction);
            $that->syncComponents($attributes, $transaction->details);

            return $transaction->info->fresh();
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Model
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $transaction = $that->createOrUpdate($attributes, $id);

            $that->attachItemManagement($attributes, $transaction);
            $that->syncComponents($attributes, $transaction->details);

            return $transaction->info;
        });
    }

    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $fields = [
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks'],
            'for_location'     => $data['for_location'],
            'transaction_type' => $data['transaction_type'],
            'created_for'      => $data['created_for'],
            'requested_by'     => $data['requested_by'],
            'reference_id'     => $data['reference_id'] ?? null,
        ];

        foreach ($this->app->make($this->info)->getUDFColumns() as $key => $column) {
            $fields[$key] = $data[$key] ?? null;
        }
        
        return $fields;
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformDetail(array $data)
    {
        return [
            'id'           => $data['id'] ?? 0,
            'product_id'   => $data['product_id'],
            'unit_id'      => $data['unit_id'],
            'unit_qty'     => $data['unit_qty'],
            'qty'          => $data['qty'],
            'oprice'       => $data['oprice'],
            'price'        => $data['price'],
            'cost'         => $data['cost'],
            'discount1'    => $data['discount1'],
            'discount2'    => $data['discount2'],
            'discount3'    => $data['discount3'],
            'discount4'    => $data['discount4'],
            'reference_id' => $data['reference_id'] ?? null,
            'remarks'      => $data['remarks'],
            'group_type'   => $data['group_type'] ?? GroupType::NONE,
            'manageable'   => $data['manageable'],
            'product_type' => $data['product_type'],
            'source' => $data['source'] ?? TransactionSource::WEB
        ];
    }

    /**
     * Create new serials|batches and attached the created data to the transaction details
     * 
     * @param  array $attributes
     * @param  InventoryChain $transaction
     */
    private function attachItemManagement($attributes, $transaction)
    {
        $attributes = (object) $attributes;

        $attached = clone $transaction->details['attached'];

        $query = $this->app->make($this->details);

        foreach ($attributes->{$this->childRootKey()} as $key => $attribute) {
            $attribute = (object) $attribute;

            if (!is_null($attribute->id) && $attribute->id !== 0) {
                $detail = $query->find($attribute->id);
            } else {
                $detail = $attached->first();
                $attached->pull(0);
                $attached = $attached->values();
            }

            if (!$this->isSerialsOrBatchesSet($attribute)) {
                continue;
            }

            // If there are serials set, sync the ids to the corresponding detail
            if (isset($attribute->serials) && count($attribute->serials) > 0) {
                $detail->serials()->sync($this->transformSerials($attribute->serials));
            }
            // If there are batches set, sync the ids to the corresponding detail
            else if (isset($attribute->batches) && count($attribute->batches) > 0) {
                $detail->batches()->sync($this->transformBatches($attribute->batches, $transaction->info));
            }
        }
    }

    /**
     * Check whether serials or batches data existing in the object
     *
     * @param $attribute
     * @return bool
     */
    protected function isSerialsOrBatchesSet($attribute)
    {
        return (isset($attribute->serials) && count($attribute->serials) > 0)
            || (isset($attribute->batches) && count($attribute->batches) > 0);
    }

    protected function transformSerials($data)
    {
        return array_pluck($data, 'id');
    }

    protected function transformBatches($data, $transaction)
    {
        $batches = [];

        // If the corresponding transaction is an instance of StockDeliveryOutbound or StockReturnOutbound
        // add a value in the remaining_qty column
        foreach ($data as $key => $batch) {
            $batches[$batch['id']] = [
                'qty' => $batch['qty'],
                'remaining_qty' => $transaction instanceof StockDeliveryOutbound
                || $transaction instanceof StockReturnOutbound
                    ? $batch['qty']
                    : 0
            ];
        }

        return $batches;
    }

    protected function transformUpdateApprovedInfo($data)
    {
        $fields = [
            'requested_by' => $data['requested_by'],
            'remarks'      => $data['remarks'] ?? null
        ];

        foreach ($model = $this->app->make($this->info)->getUDFColumns() as $key => $type) {
            $fields[$key] = $data[$key] ?? null;
        }

        return $fields;
    }
}
