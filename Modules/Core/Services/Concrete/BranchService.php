<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\BranchServiceInterface;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchDetail;

class BranchService extends TransactionService implements BranchServiceInterface
{
    protected function models()
    {
        return [
            'info'    => Branch::class,
            'details' => BranchDetail::class,
        ];
    }

    /**
     * Transforms parent array
     *
     * @param  array  $data
     * @return array
     */
    protected function parentTransformer(array $data)
    {
        return [
            'code'          => $data['code'],
            'name'          => $data['name'],
            'contact'       => $data['contact'],
            'address'       => $data['address'],
            'business_name' => $data['business_name']
        ];
    }

    /**
     * Transforms child array
     * 
     * @param  array  $data
     * @return array      
     */
    protected function childTransformer(array $data)
    {
        return [
            'id'            => $data['id'] ?? 0,
            'code'          => $data['code'],
            'name'          => $data['name'],
            'contact'       => $data['contact'],
            'address'       => $data['address'],
            'business_name' => $data['business_name']
        ];
    }
}