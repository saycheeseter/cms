<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\TransactionServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Illuminate\Database\Eloquent\Model;
use App;
use DB;

abstract class TransactionService implements TransactionServiceInterface
{
    use DatabaseTransaction;

    protected $models = [
        'info' => '',
        'details' => ''
    ];

    protected $events = [
        'created' => null,
        'updated' => null,
        'deleted' => null
    ];

    public function __construct()
    {
        $this->assignModels();
    }

    /**
     * Store data to parent and child models
     * 
     * @param  array  $attributes 
     * @return Model
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            // Make a new instance of the parent model
            $info = App::make($that->models['info'])->newInstance(
                $this->parentTransformer($attributes[$that->parentRootKey()])
            );

            $info->save();

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes[$that->childRootKey()] as $data) {
                $transformed = $that->childTransformer($data);

                array_pull($transformed, 'id');

                $collection[] = App::make($that->models['details'])
                    ->newInstance($transformed);
            }

            call_user_func(array($info, $that->relationship()))->saveMany($collection);

            if (!is_null($this->events['created'])) {
                event(new $this->events['created']($info->fresh()));
            }

            return $info->fresh();
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     * 
     * @param  array  $attributes
     * @param  integer $id        
     * @return Model            
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $info = App::make($that->models['info'])->findOrFail($id);

            $info->fill($this->parentTransformer($attributes[$that->parentRootKey()]));

            if (!is_null($this->events['updated'])) {
                event(new $this->events['updated']($info));
            }

            $info->save();

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes[$that->childRootKey()] as $data) {
                $collection[] = $that->childTransformer($data);
            }

            call_user_func(array($info, $that->relationship()))->sync($collection);

            return $info;
        });
    }

    /**
     * Delete parent model (Also deletes relationship model)
     * If there are other models to be deleted aside from the relationship,
     * leverage the deleting process using an observer.
     * 
     * @param  integer $id 
     * @return bool     
     */
    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $info = App::make($that->models['info'])->findOrFail($id);

            foreach ($info->{$that->relationship()} as $key => $value) {
                $value->delete();
            }

            $info->delete();

            if (!is_null($this->events['deleted'])) {
                event(new $this->events['deleted']($info));
            }
        });
    }

    /**
     * Relationship to be accessed by the parent table to save collection
     * 
     * @return string
     */
    protected function relationship()
    {
        return 'details';
    }

    /**
     * Relationship id to accessed inside the array
     * 
     * @return string
     */
    protected function relationshipKey()
    {
        return 'id';
    }

    /**
     * Key to be used upon accessing the parent data in the array
     * 
     * @return string
     */
    protected function parentRootKey()
    {
        return 'info';
    }

    /**
     * Key to be used upon accessing the child data in the array
     * 
     * @return string
     */
    protected function childRootKey()
    {
        return 'details';
    }

    private function assignModels()
    {
        foreach ($this->models() as $key => $model) {
            $this->models[$key] = $model;
        }
    }

    abstract protected function models();
    abstract protected function parentTransformer(array $data);
    abstract protected function childTransformer(array $data);
}