<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\MemberServiceInterface;
use Modules\Core\Entities\Member;

class MemberService extends BaseService implements MemberServiceInterface
{
    protected function model()
    {
        return Member::class;
    }
}