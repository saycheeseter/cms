<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\BankAccountServiceInterface;
use Modules\Core\Entities\BankAccount;

class BankAccountService extends BaseService implements BankAccountServiceInterface
{
    protected function model()
    {
        return BankAccount::class;
    }
}