<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Events\CollectionApproved;
use Modules\Core\Events\CollectionReverted;
use Modules\Core\Services\Contracts\CollectionServiceInterface;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\CollectionDetail;
use Modules\Core\Entities\CollectionReference;
use Modules\Core\Enums\Customer;

class CollectionService extends FinancialFlowService implements CollectionServiceInterface
{
    public function __construct()
    {
        $this->info = Collection::class;
        $this->details = CollectionDetail::class;
        $this->reference = CollectionReference::class;
        $this->events = [
            'approved' => CollectionApproved::class,
            'revert'   => CollectionReverted::class
        ];
    }

    /**
     * Update the each transaction's remaining amount based on the Collection Reference data
     *
     * @param  Collection $collection
     * @return $this
     */
    public function updateTransactionsRemainingAmount(Collection $collection)
    {
        return $this->changeTransactionsRemainingAmount($collection, 'sub');
    }

    /**
     * Revert each transaction's remaining amount based on the Collection Reference data
     *
     * @param  Collection $collection
     * @return $this
     */
    public function revertTransactionsRemainingAmount(Collection $collection)
    {
        return $this->changeTransactionsRemainingAmount($collection);
    }

    /**
     * Trigger add or subtract remaining amount of Sales Outbound
     *
     * @param  Collection $collection
     * @param  string  $method
     * @return $this
     */
    private function changeTransactionsRemainingAmount(Collection $collection, $method = 'add')
    {
        $references = $collection->references;

        foreach ($references as $reference) {
            $transaction = $reference->reference;

            $transaction->remaining_amount = $transaction->remaining_amount->$method($reference->collectible->abs());

            $transaction->save();
        }

        return $this;
    }

    /**
     * Transform's info data
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $customer = [];

        if($data['customer_type'] == Customer::WALK_IN) {
            $customer = [
                'customer_walk_in_name' => $data['customer']
            ];
        } else if($data['customer_type'] == Customer::REGULAR) {
            $customer = [
                'customer_id' => $data['customer']
            ];
        }

        return array_merge([
            'id'               => $data['id'] ?? 0,
            'created_for'      => $data['created_for'],
            'transaction_date' => Carbon::parse($data['transaction_date']),
            'remarks'          => $data['remarks'],
            'customer_type'    => $data['customer_type']
        ], $customer);
    }

    /**
     * Transform's detail data
     * @param  array  $data
     * @return array
     */
    protected function transformReference(array $data)
    {
        return [
            'id'             => $data['id'] ?? 0,
            'reference_id'   => $data['reference_id'],
            'reference_type' => $data['reference_type'],
            'collectible'    => $data['collectible']
        ];
    }
}