<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\SalesServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\SalesDetail;

class SalesService extends InvoiceService implements SalesServiceInterface
{
    protected function models()
    {
        $this->info = Sales::class;
        $this->details = SalesDetail::class;
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'customer_id' => $data['customer_id'],
            'customer_detail_id' => $data['customer_detail_id'],
            'salesman_id' => $data['salesman_id'],
            'customer_type' => $data['customer_type'],
            'customer_walk_in_name' => $data['customer_walk_in_name'],
            'term' => $data['term'],
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'salesman_id' => $data['salesman_id'],
            'term' => $data['term']
        ]);
    }
}