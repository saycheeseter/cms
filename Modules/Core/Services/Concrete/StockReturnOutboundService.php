<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockReturnOutboundServiceInterface;
use Modules\Core\Services\Concrete\InventoryChainService;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Entities\StockReturnOutboundDetail;
use Modules\Core\Entities\StockReturnOutboundSerialNumber;

class StockReturnOutboundService extends InventoryChainService implements StockReturnOutboundServiceInterface
{
    protected function models()
    {
        $this->info = StockReturnOutbound::class;
        $this->details = StockReturnOutboundDetail::class;
        $this->serial = StockReturnOutboundSerialNumber::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'deliver_to' => $data['deliver_to'],
            'deliver_to_location' => $data['deliver_to_location']
        ]);
    }
}
