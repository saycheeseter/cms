<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\PermissionSectionServiceInterface;
use Modules\Core\Entities\PermissionSection;
use App;
use DB;

class PermissionSectionService extends BaseService implements PermissionSectionServiceInterface
{
    protected function model()
    {
        return PermissionSection::class;
    }

    public function create()
    {   
        $lastSectionSeqNo = App::make($this->model())
            ->orderBy('sequence_number', 'desc')
            ->first()
            ->sequence_number;

        return App::make($this->model())->firstOrCreate(
            ['alias' => 'custom_report'],
            [
                'name' => 'Custom Report',
                'alias' => 'custom_report',
                'translation' => 'label.custom.report',
                'sequence_number' => $lastSectionSeqNo + 1
            ]
        );
    }

    public function deleteFromReportBuilder()
    {
        $section = App::make($this->model())
            ->with(['modules'])
            ->where('alias', 'custom_report')
            ->first();

        if($section->modules->count() == 0) {
            $this->delete($section->id);
        }
    }
}