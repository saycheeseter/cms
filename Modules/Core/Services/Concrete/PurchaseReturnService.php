<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\PurchaseReturnServiceInterface;
use Modules\Core\Services\Concrete\InvoiceService;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\PurchaseReturnDetail;

class PurchaseReturnService extends InvoiceService implements PurchaseReturnServiceInterface
{
    protected function models()
    {
        $this->info = PurchaseReturn::class;
        $this->details = PurchaseReturnDetail::class;
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'supplier_id' => $data['supplier_id'],
            'payment_method' => $data['payment_method'],
            'term' => $data['term']
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'payment_method' => $data['payment_method'],
            'term' => $data['term']
        ]);
    }
}