<?php
namespace Modules\Core\Services\Concrete\Repository;

use Modules\Core\Enums\ModuleDefinedFields;

use App;

class Factory
{
    public static function instance($module)
    {
        switch($module) {
            case ModuleDefinedFields::PRODUCT:
                return App::make(\Modules\Core\Repositories\Contracts\ProductRepository::class);
                break;
            case ModuleDefinedFields::PURCHASE:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseRepository::class);
                break;
            case ModuleDefinedFields::PURCHASE_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseInboundRepository::class); 
                break;
            case ModuleDefinedFields::PURCHASE_RETURN:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseReturnRepository::class); 
                break;
            case ModuleDefinedFields::PURCHASE_RETURN_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseReturnOutboundRepository::class); 
                break;
            case ModuleDefinedFields::DAMAGE:
                return App::make(\Modules\Core\Repositories\Contracts\DamageRepository::class); 
                break;
            case ModuleDefinedFields::DAMAGE_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\DamageOutboundRepository::class); 
                break;
            case ModuleDefinedFields::SALES:
                return App::make(\Modules\Core\Repositories\Contracts\SalesRepository::class); 
                break;
            case ModuleDefinedFields::SALES_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\SalesOutboundRepository::class); 
                break;
            case ModuleDefinedFields::SALES_RETURN:
                return App::make(\Modules\Core\Repositories\Contracts\SalesReturnRepository::class); 
                break;
            case ModuleDefinedFields::SALES_RETURN_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\SalesReturnInboundRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_REQUEST:
                return App::make(\Modules\Core\Repositories\Contracts\StockRequestRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_DELIVERY:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_DELIVERY_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_DELIVERY_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_RETURN:
                return App::make(\Modules\Core\Repositories\Contracts\StockReturnRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_RETURN_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockReturnOutboundRepository::class); 
                break;
            case ModuleDefinedFields::STOCK_RETURN_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockReturnInboundRepository::class); 
                break;
            case ModuleDefinedFields::INVENTORY_ADJUST:
                return App::make(\Modules\Core\Repositories\Contracts\InventoryAdjustRepository::class); 
                break;
            case ModuleDefinedFields::PAYMENT:
                return App::make(\Modules\Core\Repositories\Contracts\PaymentRepository::class); 
                break;
            case ModuleDefinedFields::COLLECTION:
                return App::make(\Modules\Core\Repositories\Contracts\CollectionRepository::class); 
                break;
            case ModuleDefinedFields::USER:
                return App::make(\Modules\Core\Repositories\Contracts\UserRepository::class); 
                break;
        }
    }
}