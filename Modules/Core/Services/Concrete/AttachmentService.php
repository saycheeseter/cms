<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\AttachmentServiceInterface;
use Modules\Core\Services\Concrete\Repository\Factory;
use Exception;
use Storage;

class AttachmentService implements AttachmentServiceInterface
{   
    private $temporaryPath = 'attachments/temporary';

    /**
     * temporarily upload attachments to temporary directory
     *
     * @param  $files
     * @return array attachments
     */
    public function temporary($files)
    {
        $attachments = [];
        
        foreach ($files as $key => $attachment) {
            $attached = Storage::disk('public')->put($this->temporaryPath, $attachment);
            $path = sprintf("storage/%s", $attached);

            $attachment = [
                'link' => asset($path),
                'key' => basename($attached),
                'original' => $attachment->getClientOriginalName(),
                'status' => 'temporary',
                'attribute' => getimagesize($path)
            ];
        }

        return $attachment;
    }

    /**
     * attach files to entity and add to attachments directory
     *
     * @param  $files
     * @param  $record
     * @return array attachments
     */
    public function attach($files, $module, $id)
    {
        $record = $this->findModel($module, $id);

        $attachments = [];
        
        foreach ($files as $key => $attachment) {
            $attachments[] = $record->attach($attachment);
        }

        return $record->attachments()->get();
    }

    /**
     * attach files to entity and add to attachments directory
     *
     * @param  $files
     * @param  $record
     * @return array attachments
     */
    public function update($files, $module, $id)
    {   
        $record = $this->findModel($module, $id);

        foreach ($record->attachments()->get() as $attachment) {
            $record->attachment($attachment->key)->delete();
        }

        $attachments = [];
        
        foreach ($files as $key => $attachment) {
            $attachments[] = $record->attach($attachment);
        }

        return $record->attachments()->get();
    }

    /**
     * detach files to entity
     *
     * @param  $request
     * @param  $record
     * @param  $key
     */
    public function detach($key, $module, $id)
    {   
        $record = $this->findModel($module, $id);

        $record->attachment($key)->delete();
    }

    /**
     * remove temporary files to attachments directory
     *
     * @param  $request
     * @param  $record
     * @param  $key
     */
    public function removeTemporary($key)
    {   
        Storage::disk('public')->delete(sprintf('%s/%s', $this->temporaryPath, $key));
    }

    /**
     * [find model instance of record]
     * @param  int $module [description]
     * @param  int $id     [description]
     * @return model instance
     */
    private function findModel($module, $id)
    {
        $record = Factory::instance($module)->findOrFail($id);

        return $record;
    }
}