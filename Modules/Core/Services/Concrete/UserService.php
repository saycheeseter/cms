<?php

namespace Modules\Core\Services\Concrete;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Core\Enums\UserType;
use Modules\Core\Events\UserDeleted;
use Modules\Core\Events\UserUpdating;
use Modules\Core\Services\Contracts\UserServiceInterface;
use Modules\Core\Entities\User;
use Modules\Core\Entities\UserLoginSchedule;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Events\UserCreated;
use Carbon\Carbon;
use Auth;

class UserService implements UserServiceInterface
{
    use DatabaseTransaction;

    /**
     * Create a new user record in database and insert a new data in
     * branch_permission_user table
     *
     * @param array $attributes
     * @return mixed
     */
    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $attributes = $that->only($attributes);

            $user = User::create($attributes['info']);

            $this->attachPermissions($user, $attributes['permissions']);

            $user->schedule()->create($attributes['schedule']);

            event(new UserCreated($user));

            return $user;
        });
    }

    /**
     * Update the details and permissions of the indicated resource
     *
     * @param array $attributes
     * @param $id
     * @return mixed
     */
    public function update(array $attributes, $id)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $attributes = $that->only($attributes);

            $user = User::where('type', '<>', UserType::SUPERADMIN)
                ->whereKey($id)
                ->first();

            if (!$user) {
                throw (new ModelNotFoundException)->setModel(
                    User::class, $id
                );
            }

            if (empty($attributes['info']['password'])) {
                unset($attributes['info']['password']);
            }

            $user->fill($attributes['info']);
            $user->updated_at = Carbon::now();

            event(new UserUpdating($user));

            $user->save();

            $user->schedule->fill($attributes['schedule'])->save();

            $this->attachPermissions($user, $attributes['permissions']);
            
            return $user;
        });
    }

    /**
     * Delete the specified user
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->transaction(function() use ($id) {
            $user = User::where('type', '<>', UserType::SUPERADMIN)
                ->whereKey($id)
                ->first();

            if (!$user) {
                throw (new ModelNotFoundException)->setModel(
                    User::class, $id
                );
            }

            $user->delete();

            event(new UserDeleted($user));

            return $user;
        });
    }

    /**
     * Change the password of the currently authenticated user
     *
     * @param string $password
     * @return mixed
     */
    public function changeCurrentUserPassword(string $password)
    {
        Auth::user()->password = $password;
        Auth::user()->save();

        return Auth::user();
    }

    public function changeAdminPassword($id, string $password)
    {
        $user = User::where('type', UserType::ADMIN)->findOrFail($id);
        $user->password = $password;
        $user->save();

        return $user;
    }

    private function attachPermissions($user, $permissions)
    {
        $user->permissions()->detach();

        $collection = $this->formatPivotPermissions($permissions);

        foreach ($collection as $key => $set) {
            $user->permissions()->attach($set);
        }
    }

    private function formatPivotPermissions(array $permissions)
    {
        $collection = array();

        foreach ($permissions as $permission) {
            $formatted = [];

            foreach ($permission['code'] as $code) {
                $formatted = array_add($formatted, $code, array(
                    'branch_id' => $permission['branch']
                ));
            }

            $collection[] = $formatted;
        }

        return $collection;
    }

    private function getFormat()
    {
        return [
            'info' => [
                'code',
                'username',
                'full_name',
                'password',
                'status',
                'telephone',
                'mobile',
                'position',
                'email',
                'address',
                'memo',
                'role_id',
                'license_key'
            ],
            'permissions' => [
                'branch',
                'code'
            ],
            'schedule' => [
                'mon',
                'tue',
                'wed',
                'thu',
                'fri',
                'sat',
                'sun',
                'start',
                'end'
            ]
        ];
    }

    private function only($data)
    {
        $format = $this->getFormat();

        $filtered = [
            'info' => [],
            'permissions' => [],
            'schedule' => []
        ];

        if (isset($data['info'])) {
            $filtered['info'] = array_only($data['info'], $format['info']);
        }

        if (isset($data['permissions'])) {
            $keys = $format['permissions'];

            $filtered['permissions'] = array_map(function ($item) use ($keys){
                return array_only($item, $keys);
            }, $data['permissions']);
        }

        if (isset($data['schedule'])) {
            $filtered['schedule'] = array_only($data['schedule'], $format['schedule']);
        }

        return $filtered;
    }
}
