<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\ExcelTemplateExportServiceInterface;
use Excel;
use PHPExcel_Style_NumberFormat;
use Lang;

class ExcelTemplateExportService extends ExportService implements ExcelTemplateExportServiceInterface
{
    public function generate($collection = array())
    {
        
        $that = $this;

        $this->data($collection);

        Excel::create($this->fileName, function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->setWidth($that->widths);
                
                $that->setHeaders($sheet);
                
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
                
                $that->setTextAlign($sheet);
            });
        })->export('xlsx');
    }
}
