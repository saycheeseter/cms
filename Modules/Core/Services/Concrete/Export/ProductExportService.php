<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\ProductExportServiceInterface;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat;

class ProductExportService extends ExportService implements ProductExportServiceInterface
{
    public function list($collection) 
    {
        $headers = [
            'barcode'           => Lang::get('core::label.unit.barcode'),
            'stock_no'          => Lang::get('core::label.stock.no'),
            'name'              => Lang::get('core::label.name'),
            'chinese_name'      => Lang::get('core::label.chinese.name'),
            'description'       => Lang::get('core::label.description'),
            'memo'              => Lang::get('core::label.memo'),
            'brand'             => Lang::get('core::label.brand'),
            'category'          => Lang::get('core::label.category'),
            'supplier'          => Lang::get('core::label.supplier'),
            'group_type_label'  => Lang::get('core::label.group.type'),
            'purchase_price'    => Lang::get('core::label.cost'),
            'wholesale_price'   => Lang::get('core::label.wholesale'),
            'selling_price'     => Lang::get('core::label.retail'),
            'price_a'           => Lang::get('core::label.price.a'),
            'price_b'           => Lang::get('core::label.price.b'),
            'price_c'           => Lang::get('core::label.price.c'),
            'price_d'           => Lang::get('core::label.price.d'),
            'price_e'           => Lang::get('core::label.price.e'),
            'inventory'         => Lang::get('core::label.inventory')
        ];

        $format = [
            'purchase_price'    => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'selling_price'     => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'wholesale_price'   => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price_a'           => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price_b'           => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price_c'           => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price_d'           => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price_e'           => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'inventory'         => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];

        $this->headers($headers);
        $this->data($collection);
        $this->format($format);

        $that = $this;

        Excel::create(sprintf('products_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}