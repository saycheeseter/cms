<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\NegativeProfitProductExportServiceInterface;
use Excel;
use PHPExcel_Style_NumberFormat;
use Lang;

class NegativeProfitProductExportService extends ExportService implements NegativeProfitProductExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'barcode'      => Lang::get('core::label.barcode'),
            'product'      => Lang::get('core::label.product'),
            'sheet_number' => Lang::get('core::label.sheet.number'),
            'qty'          => Lang::get('core::label.qty'),
            'cost'         => Lang::get('core::label.cost'),
            'price'        => Lang::get('core::label.price'),
            'loss'         => Lang::get('core::label.loss'),
        ));

        $this->data($collection);

        $this->format(array(
            'qty'   => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'cost'  => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'loss'  => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('negative_profit_product_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}