<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\ProductPeriodicSalesExportServiceInterface;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat;

class ProductPeriodicSalesExportService extends ExportService implements ProductPeriodicSalesExportServiceInterface
{
    public function list($collection) 
    {
        $headers = [
            'barcode'      => Lang::get('core::label.barcode'),
            'name'         => Lang::get('core::label.product'),
            'chinese_name' => Lang::get('core::label.chinese.name'),
            'inventory'    => Lang::get('core::label.inventory')
        ];

        $format = [
            'inventory' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];

        $keys = array_keys(array_first($collection));

        $headers = array_merge($headers, $this->buildDynamicHeaders($keys));
        $format = array_merge($format, $this->buildDynamicFormat($keys));

        $this->headers($headers);
        $this->data($collection);
        $this->format($format);

        $that = $this;

        Excel::create(sprintf('product_periodic_sales_report_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }

    private function buildDynamicHeaders($keys)
    {
        $columns = [];

        $dynamic = [
            'total_qty' => Lang::get('core::label.total.qty'),
            'total_amount' => Lang::get('core::label.total.amount')
        ];

        foreach ($keys as $key) {
            foreach ($dynamic as $column => $label) {
                if (strpos($key, $column) !== false) {
                    $date = array_first(explode('_', $key));
                    $columns[$key] = $date.' '.$label;
                    break;
                }
            }
        }

        return $columns;
    }

    private function buildDynamicFormat($keys)
    {
        $format = [];

        $dynamic = [
            'total_qty' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'total_amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];

        foreach ($keys as $key) {
            foreach ($dynamic as $column => $style) {
                if (strpos($key, $column) !== false) {
                    $format[$key] = $style;
                    break;
                }
            }
        }

        return $format;
    }
}