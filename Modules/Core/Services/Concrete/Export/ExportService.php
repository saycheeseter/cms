<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Enums\ExcelColumnType;
use PHPExcel_Cell;
use PHPExcel_Style_NumberFormat;
use Lang;

abstract class ExportService
{   
    protected $settings = [];
    protected $headers = [];
    protected $data = [];
    protected $format = [];
    protected $udfs = [];
    protected $widths = [];
    protected $textAlign = [];
    protected $templateFormat;
    protected $fileName = "";
    protected $isPreview = false;
    
    /**
    * set template format
    * parse width, headers and aligned
    *
    * @param $data
    * @return $this
    */
    public function setTemplateFormat($format)
    {   
        $this->templateFormat = $format;
        
        $this->parseTemplateWidths();
        $this->parseTemplateHeaders();
        $this->parseTemplateAlignment();
        $this->parseTemplateType();

        return $this;
    }

    /**
     * set excel file name
     * 
     * @param string $name
     */
    public function setFileName($name)
    {
        $this->fileName = sprintf("%s (%s)", $name, date("Y-m-d"));

        return $this;
    }

    /**
     * Set visibility or other settings 
     * 
     * @param  array $settings
     * @return $this
     */
    public function settings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Set header columns based on settings
     * 
     * @param  array $headers
     * @return $this         
     */
    public function headers($headers)
    {
        $this->headers = $this->parseFromSettings($headers);
        if (count($this->udfs) > 0) {
            $this->appendUDFHeaders();
        } else {
            unset($this->headers['udfs']);
        }

        return $this;
    }

    /**
     * Set data based on settings
     * 
     * @param  array $data
     * @return $this         
     */
    public function data($data)
    {   
        if($this->isPreview){
            $data = [];

            for ($i=0; $i < 10; $i++) { 
                $data[] = $this->headers;
            }

            return $this;
        }

        for ($i=0; $i < count($data); $i++) { 
            $sorted = $this->sortDataByHeaderArrangement($data[$i]);

            $this->data[] = $this->parseFromSettings($sorted);
        }

        return $this;
    }

    /**
     * sort data based on header
     * 
     * @param  array $data
     * @return $sorted         
     */
    
    protected function sortDataByHeaderArrangement($data)
    {
        $sorted = [];

        foreach ($this->headers as $key => $header) {
            if(isset($data[$key])) {
                $sorted[$key] = $data[$key];
            } else {
                $sorted[$key] = "";
            }
        }

        return $sorted;
    }

    /**
     * set text alignment per cell
     * @param  sheet $sheet
     */
    public function setTextAlign($sheet)
    {
        $that = $this;

        $rowIndex = 2;

        foreach ($this->data as $record) {
            foreach($record as $key => $value) {
                $numericKey = array_search($key, array_keys($record));
                $columnIndex = PHPExcel_Cell::stringFromColumnIndex($numericKey);

                $sheet->cell($columnIndex.$rowIndex, function($cell) use ($that, $columnIndex) {
                    $cell->setAlignment($that->textAlign[$columnIndex]); 
                });
            }
            $rowIndex++;
        }
    }

    /**
     * set header values and styles
     * 
     * @param  sheet $sheet
     */
    public function setHeaders($sheet)
    {
        $sheet->row(1, $this->headers);
        
        $headerStyle = $this->templateFormat->header_style;

        $sheet->row(1, function($row) use ($headerStyle) {
            $row->setFont(array(
                'bold' =>  $headerStyle->bold,
            ));
            $row->setAlignment($headerStyle->textAlign);
        });
    }

    /**
     * Set udfs to be appended in excel
     * 
     * @param  array $data
     * @return $this         
     */
    public function udfs($data)
    {
        $this->udfs = $data;

        return $this;
    }

    /**
     * Set format of columns. Index of format will be automatically
     * be generated based on settings
     * 
     * @param  array $formats [description]
     * @return $this          [description]
     */
    public function format($formats)
    {
        $formats = $this->parseFromSettings($formats);

        foreach ($formats as $key => $format) {
            $numericKey = array_search($key, array_keys($this->headers));
            
            $this->format[PHPExcel_Cell::stringFromColumnIndex($numericKey)] = $this->parseFormat($format);
        }

        return $this;
    }

    /*
        set generate as preview only
     */
    public function previewOnly()
    {

        $this->isPreview = true;

        return $this;
    }

    /**
     * Parse and return the data according to settings
     * 
     * @param  array $data 
     * @return array
     */
    protected function parseFromSettings($data)
    {
        foreach ($this->settings as $key => $value) {
            if (!filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                array_pull($data, $key);
            }
        }

        return $data;
    }

    /**
     * Overrides default format of Laravel excel 
     * 
     * @param  string $format
     * @return string        
     */
    protected function parseFormat($format)
    {
        switch ($format) {
            case PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1:
                $count = (strlen(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1) - 2) + (int) setting('monetary.precision');
                $format = str_pad($format, $count, '0', STR_PAD_RIGHT);
                break;
        }

        return $format;
    }


    /**
    * Set headers from parsed template data columns
    * 
    */
    protected function parseTemplateHeaders()
    {   
        $columns = $this->templateFormat->columns;

        $headers = [];

        foreach ($columns as $key => $value) {
            $headers[$key] = $value->header_name;
        }

        $this->headers($headers);
    }

    /**
    * Set column width from parsed template data columns
    * 
    */
    protected function parseTemplateWidths()
    {   
        $columns = get_object_vars($this->templateFormat->columns);

        foreach ($columns as $key => $value) {
            $numericKey = array_search($key, array_keys($columns));
            $this->widths[PHPExcel_Cell::stringFromColumnIndex($numericKey)] = $value->width;
        }
    }

    /**
    * Set column alignment from parsed template data columns
    * 
    */
    protected function parseTemplateAlignment()
    {   
        $columns = get_object_vars($this->templateFormat->columns);

        foreach ($columns as $key => $value) {
            $numericKey = array_search($key, array_keys($columns));
            $this->textAlign[PHPExcel_Cell::stringFromColumnIndex($numericKey)] = $value->textAlign;
        }
    }

    /**
    * Set per column type
    * 
    */
    protected function parseTemplateType()
    {   
        $columns = get_object_vars($this->templateFormat->columns);
        $types = [];

        foreach ($columns as $key => $value) {
           switch ($value->type) {
               case ExcelColumnType::NUMERIC:
                    $types[$key] = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1;
                    break;
           }
        }

        $this->format($types);
    }

    /**
     * Append udfs label in headers
     * 
     * @return void
     */
    protected function appendUDFHeaders()
    {   
        if(is_null($this->templateFormat)){
            foreach ($this->udfs as $key => $udf) {
                $this->headers[$udf['alias']] = $udf['label'];
            }    
        }
        
    }
}