<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Entities\BranchDetail;
use Modules\Core\Services\Contracts\Export\ProductBranchInventoryExportServiceInterface;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Str;

class ProductBranchInventoryExportService extends ExportService implements ProductBranchInventoryExportServiceInterface
{
    public function list($collection) 
    {
        $headers = [
            'barcode'      => Lang::get('core::label.barcode'),
            'name'         => Lang::get('core::label.product')
        ];

        $keys = array_keys(array_first($collection));

        $headers = array_merge($headers, $this->buildDynamicHeaders($keys));
        $format = $this->buildDynamicFormat($keys);

        $this->headers($headers);
        $this->data($collection);
        $this->format($format);

        $that = $this;

        Excel::create(sprintf('product_branch_inventory_report_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }

    private function buildDynamicHeaders($keys)
    {
        $columns = [];

        $excluded = [
            'barcode', 'name'
        ];

        $locations = BranchDetail::all()->keyBy('id');

        foreach($keys as $key) {
            if(!in_array($key, $excluded)) {
                $id = Str::replaceFirst('location_', '', $key);

                $columns[$key] = $locations->get($id)->name;
            }
        }

        return $columns;
    }

    private function buildDynamicFormat($keys)
    {
        $format = [];

        $excluded = [
            'barcode', 'name'
        ];

        foreach($keys as $key) {
            if(!in_array($key, $excluded)) {
                $format[$key] = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1;
            }
        }

        return $format;
    }
}