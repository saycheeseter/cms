<?php

namespace Modules\Core\Services\Concrete\Export;

use Excel;
use Lang;
use Modules\Core\Services\Contracts\Export\ProductAvailableSerialsExportServiceInterface;

class ProductAvailableSerialsExportService extends ExportService implements ProductAvailableSerialsExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'branch'                      => Lang::get('core::label.branch'),
            'supplier_name'               => Lang::get('core::label.supplier'),
            'location'                    => Lang::get('core::label.location'),
            'serial_number'               => Lang::get('core::label.serial.number'),
            'expiration_date'             => Lang::get('core::label.expiration.date'),
            'manufacturing_date'          => Lang::get('core::label.manufacturing.date'),
            'admission_date'              => Lang::get('core::label.admission.date'),
            'manufacturer_warranty_start' => Lang::get('core::label.manufacturer.warranty.start'),
            'manufacturer_warranty_end'   => Lang::get('core::label.manufacturer.warranty.end'),
            'remarks'                     => Lang::get('core::label.remarks')
        ));

        $this->data($collection);

        $that = $this;

        Excel::create(sprintf('product_serials_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}