<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\ProductTransactionListExportServiceInterface;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat;

class ProductTransactionListExportService extends ExportService implements ProductTransactionListExportServiceInterface
{
    public function list($collection) 
    {
        $this->headers(array(
            'barcode'                          => Lang::get('core::label.barcode'),
            'product'                          => Lang::get('core::label.product'),
            'chinese_name'                     => Lang::get('core::label.chinese.name'),
            'beginning'                        => Lang::get('core::label.beginning'),
            'purchase_inbound'                 => Lang::get('core::label.purchase.inbound'),
            'purchase_return_outbound'         => Lang::get('core::label.purchase.return.outbound'),
            'sales_outbound'                   => Lang::get('core::label.sales.outbound'),
            'sales_return_inbound'             => Lang::get('core::label.sales.return.inbound'),
            'stock_delivery_inbound'           => Lang::get('core::label.stock.delivery.inbound'),
            'stock_delivery_outbound'          => Lang::get('core::label.stock.delivery.outbound'),
            'stock_return_inbound'             => Lang::get('core::label.stock.return.inbound'),
            'adjustment_increase'              => Lang::get('core::label.adjustment.increase'),
            'stock_return_outbound'            => Lang::get('core::label.stock.return.outbound'),
            'damage_outbound'                  => Lang::get('core::label.damage.outbound'),
            'adjustment_decrease'              => Lang::get('core::label.adjustment.decrease'),
            'product_conversion_increase'      => Lang::get('core::label.product.conversion.increase'),
            'product_conversion_decrease'      => Lang::get('core::label.product.conversion.decrease'),
            'auto_product_conversion_increase' => Lang::get('core::label.auto.product.conversion.increase'),
            'auto_product_conversion_decrease' => Lang::get('core::label.auto.product.conversion.decrease'),
            'pos_sales'                        => Lang::get('core::label.pos.sales'),
            'inventory'                        => Lang::get('core::label.inventory')
        ));

        $this->data($collection);

        $this->format(array(
            'beginning' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'purchase_inbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'purchase_return_outbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'sales_outbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'sales_return_inbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'stock_delivery_inbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'stock_delivery_outbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'stock_return_inbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'adjustment_increase' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'stock_return_outbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'damage_outbound' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'adjustment_decrease' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'product_conversion_increase' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'product_conversion_decrease' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'auto_product_conversion_increase' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'auto_product_conversion_decrease' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'pos_sales' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'inventory' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('product_transaction_list_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}