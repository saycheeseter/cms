<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\SalesReturnExportServiceInterface;
use Excel;
use PHPExcel_Style_NumberFormat;
use Lang;

class SalesReturnExportService extends ExportService implements SalesReturnExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'approval_status'       => Lang::get('core::label.approval.status'),
            'transaction_date'      => Lang::get('core::label.transaction.date'),
            'sheet_number'          => Lang::get('core::label.sheet.no'),
            'created_from'          => Lang::get('core::label.created.from'),
            'created_for'           => Lang::get('core::label.created.for'),
            'for_location'          => Lang::get('core::label.for.location'),
            'customer'              => Lang::get('core::label.customer'),
            'amount'                => Lang::get('core::label.amount'),
            'transaction_status'    => Lang::get('core::label.transaction.status'),
            'remarks'               => Lang::get('core::label.remarks'),
            'salesman'              => Lang::get('core::label.salesman'),
            'term'                  => Lang::get('core::label.term'),
            'requested_by'          => Lang::get('core::label.request.by'),
            'created_by'            => Lang::get('core::label.created.by'),
            'created_date'          => Lang::get('core::label.created.date'),
            'audited_by'            => Lang::get('core::label.audit.by'),
            'audited_date'          => Lang::get('core::label.audit.date'),
            'deleted'               => Lang::get('core::label.deleted'),
        ));

        $this->data($collection);

        $this->format(array(
            'amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('sales_return_list_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}