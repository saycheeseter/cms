<?php

namespace Modules\Core\Services\Concrete\Export;

use Excel;
use PHPExcel_Style_NumberFormat;
use Lang;
use Modules\Core\Services\Contracts\Export\DetailSalesReportExportServiceInterface;

class DetailSalesReportExportService extends ExportService implements DetailSalesReportExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'created_from_name' => Lang::get('core::label.created.from'),
            'created_for_name' => Lang::get('core::label.created.for'),
            'for_location_name' => Lang::get('core::label.for.location'),
            'transaction_date' => Lang::get('core::label.transaction.date'),
            'sheet_number' => Lang::get('core::label.sheet.no'),
            'reference_id' => Lang::get('core::label.reference'),
            'approval' => Lang::get('core::label.approval.status'),
            'customer_type' => Lang::get('core::label.customer.type'),
            'customer_name' => Lang::get('core::label.customer'),
            'total_amount' => Lang::get('core::label.total.amount'),
            'remaining_amount' => Lang::get('core::label.remaining.amount'),
            'remarks' => Lang::get('core::label.remarks'),
            'salesman_name' => Lang::get('core::label.salesman'),
            'term' => Lang::get('core::label.term'),
            'requested_by_name' => Lang::get('core::label.request.by'),
            'created_by_name' => Lang::get('core::label.created.by'),
            'created_at' => Lang::get('core::label.created.date'),
            'audited_by_name' => Lang::get('core::label.audit.by'),
            'audited_date' => Lang::get('core::label.audit.date'),
            'deleted' => Lang::get('core::label.deleted'),
            'product_barcode' => Lang::get('core::label.barcode'),
            'stock_no' => Lang::get('core::label.stock.no'),
            'chinese_name' => Lang::get('core::label.chinese.name'),
            'product_name' => Lang::get('core::label.product.name'),
            'qty' => Lang::get('core::label.qty'),
            'unit_name' => Lang::get('core::label.uom'),
            'unit_qty' => Lang::get('core::label.unit.specs'),
            'total_qty' => Lang::get('core::label.total.qty'),
            'product_serial' => Lang::get('core::label.serial'),
            'product_batch' => Lang::get('core::label.batch'),
            'oprice' => Lang::get('core::label.o.price'),
            'price' => Lang::get('core::label.price'),
            'discount1' => Lang::get('core::label.discount.1'),
            'discount2' => Lang::get('core::label.discount.2'),
            'discount3' => Lang::get('core::label.discount.3'),
            'discount4' => Lang::get('core::label.discount.4'),
            'detail_total_amount' => Lang::get('core::label.amount'),
            'detail_remarks' => Lang::get('core::label.detail.remarks')
        ));

        $this->data($collection);

        $this->format(array(
            'total_amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'remaining_amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'qty' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'unit_qty' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'oprice' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'price' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'discount1' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'discount2' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'discount3' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'discount4' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'total_qty' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'detail_total_amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('detail_sales_report_list_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}