<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\MemberReportExportServiceInterface;
use Excel;
use PHPExcel_Style_NumberFormat;
use Lang;

class MemberReportExportService extends ExportService implements MemberReportExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'full_name'    => Lang::get('core::label.full.name'),
            'rate'         => Lang::get('core::label.member.rate'),
            'card_id'      => Lang::get('core::label.card.id'),
            'barcode'      => Lang::get('core::label.barcode'),
            'date_created' => Lang::get('core::label.date.created'),
            'points'       => Lang::get('core::label.points')
        ));

        $this->data($collection);

        $this->format(array(
            'points' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('member_report_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}
