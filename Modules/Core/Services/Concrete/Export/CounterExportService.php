<?php

namespace Modules\Core\Services\Concrete\Export;

use Modules\Core\Services\Contracts\Export\CounterExportServiceInterface;
use Excel;
use Lang;
use PHPExcel_Style_NumberFormat;

class CounterExportService extends ExportService implements CounterExportServiceInterface
{
    public function list($collection)
    {
        $this->headers(array(
            'transaction_date' => Lang::get('core::label.transaction.date'),
            'sheet_number'     => Lang::get('core::label.sheet.no'),
            'created_from'     => Lang::get('core::label.created.from'),
            'created_for'      => Lang::get('core::label.created.for'),
            'customer'         => Lang::get('core::label.customer'),
            'total_amount'     => Lang::get('core::label.total.amount'),
            'remarks'          => Lang::get('core::label.remarks'),
            'created_by'       => Lang::get('core::label.created.by'),
        ));

        $this->data($collection);

        $this->format(array(
            'total_amount' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ));

        $that = $this;

        Excel::create(sprintf('counter_list_%s', date('Y-m-d')), function($excel) use ($that) {
            $excel->sheet('Sheet 1', function($sheet) use ($that) {
                $sheet->row(1, $that->headers);
                $sheet->setColumnFormat($that->format);
                $sheet->rows($that->data);
            });
        })->export('xlsx');
    }
}