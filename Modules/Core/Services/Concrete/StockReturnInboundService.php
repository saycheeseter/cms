<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockReturnInboundServiceInterface;
use Modules\Core\Services\Concrete\InventoryChainService;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnInboundDetail;
use Modules\Core\Entities\StockReturnInboundSerialNumber;

class StockReturnInboundService extends InventoryChainService implements StockReturnInboundServiceInterface
{
    protected function models()
    {
        $this->info = StockReturnInbound::class;
        $this->details = StockReturnInboundDetail::class;
        $this->serial = StockReturnInboundSerialNumber::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $transformation =  array_merge(
            array_except(parent::transformInfo($data), 'transaction_type'), [
            'delivery_from' => $data['delivery_from'],
            'delivery_from_location' => $data['delivery_from_location'],
        ]);

        array_pull($transformation, 'transaction_type');

        return $transformation;
    }
}
