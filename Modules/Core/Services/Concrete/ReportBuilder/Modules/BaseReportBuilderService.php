<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;

use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;

use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;

use Carbon\Carbon;
use Auth;
use App;
use Lang;

class BaseReportBuilderService
{
    use FractalTransformer;

    protected function supplierFilter($columnName, $name = '', $selected = false)
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.supplier') : $name,
            'columnName' => $columnName,
            'type' => 'string',
            'optionBool' => true,
            'selected' => $selected,
            'tableOptions' => [
                'data' => App::make(SupplierRepository::class)->all(),
                'headers' => [Lang::get('core::label.code'), Lang::get('core::label.supplier.name')],
                'columns' => ['code', 'full_name'],
                'returncolumn' => 'full_name'
            ]
        ];
    }

    protected function categoryFilter($columnName, $name = '', $selected = false) 
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.category') : $name,
            'columnName' => $columnName,
            'type' => 'string',
            'optionBool' => true,
            'selected' => $selected,
            'tableOptions' => [
                'data' => App::make(CategoryRepository::class)->all(),
                'headers' => [Lang::get('core::label.code'), Lang::get('core::label.category.name')],
                'columns' => ['code', 'name'],
                'returncolumn' => 'name'
            ]
        ];
    }

    protected function brandFilter($columnName, $name = '', $selected = false) 
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.brand') : $name,
            'columnName' => $columnName,
            'type' => 'string',
            'optionBool' => true,
            'selected' => $selected,
            'tableOptions' => [
                'data' => App::make(BrandRepository::class)->all(),
                'headers' => [Lang::get('core::label.code'), Lang::get('core::label.brand.name')],
                'columns' => ['code', 'name'],
                'returncolumn' => 'name'
            ]
        ];
    }

    protected function branchFilter($columnName, $name = '', $selected = false) 
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.branch') : $name,
            'columnName' => $columnName,
            'type' => 'select',
            'selectOptions' => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            'selected' => $selected,
            'default' => [ 
                'value' => Auth::branch()->id,
            ]
        ];
    }

    protected function transactionDateFilter($columnName, $name = '', $selected = false) 
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.transaction.date.time') : $name,
            'columnName' => $columnName,
            'type'=> 'datetime', 
            'optionBool'=> false,
            'selected' => $selected,
            'default'=> [
                [ 
                    'comparison' => '>=', 
                    'value' => Carbon::now()->startOfDay()->toDateTimeString()
                ],
                [ 
                    'comparison' =>'<', 
                    'value' => Carbon::tomorrow()->toDateTimeString()
                ],
            ]
        ];
    }

    protected function datetimeFilter($columnName, $name = '', $selected = false)
    {
        return [
            'name' => $name == '' ? Lang::get('core::label.date.time') : $name,
            'columnName' => $columnName,
            'type' => 'datetime',
            'selected' => $selected
        ];
    }

    protected function stringOnlyFilter($columnName, $name = '', $selected = false)
    {
        return [
            'name' => $name,
            'columnName' => $columnName,
            'type' => 'string',
            'optionBool' => false,
            'selected' => $selected
        ];
    }
}