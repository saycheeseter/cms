<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\SalesOutboundReportBuilderServiceInterface;
use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Contracts\UserRepository;

use Modules\Core\Transformers\User\ChosenTransformer as UserChosenTransformer;
use Lang;
use App;

class SalesOutboundReportBuilderService extends InventoryChainReportBuilderService implements SalesOutboundReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $info = 'sales_outbound';

    protected function additionalFilters()
    {
        return [
           'customer' => [
                'name' => Lang::get('core::label.regular.customer'),
                'columnName' => 'sales_outbound_customer.name',
                'type' => 'string',
                'optionBool' => true,
                'tableOptions' => [
                    'data' => App::make(CustomerRepository::class)->all(),
                    'headers' => [Lang::get('core::label.code'), Lang::get('core::label.regular.customer')],
                    'columns' => ['code', 'name'],
                    'returncolumn' => 'name'
                ]
            ],
            'customer_walk_in' => [
                'name' => Lang::get('core::label.walk.in.name'),
                'columnName' => 'sales_outbound.customer_walk_in_name',
                'type'=> 'string', 
                'optionBool'=> false,
            ],
            'salesman' => [
                'name' => Lang::get('core::label.salesman'),
                'columnName' => 'sales_outbound.salesman_id',
                'type' => 'select',
                'selectOptions' => $this->collection(App::make(UserRepository::class)->all(), UserChosenTransformer::class)['data'],
            ]
        ];
    }
}