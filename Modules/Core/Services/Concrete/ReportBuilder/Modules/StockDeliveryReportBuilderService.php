<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\StockDeliveryReportBuilderServiceInterface;

use Lang;

class StockDeliveryReportBuilderService extends InvoiceReportBuilderService implements StockDeliveryReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $info = 'stock_delivery';

    protected function additionalFilters()
    {
        return [
            'deliver.to' => $this->branchFilter(sprintf('%s.deliver_to', $this->info), Lang::get('core::label.deliver.to'))
        ];
    }
}