<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\StockDeliveryInboundReportBuilderServiceInterface;

use Lang;

class StockDeliveryInboundReportBuilderService extends InventoryChainReportBuilderService implements StockDeliveryInboundReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $info = 'stock_delivery_inbound';

    protected function additionalFilters()
    {
        return [
            'delivery_from' => $this->branchFilter(sprintf('%s.delivery_from', $this->info), Lang::get('core::label.delivery.from'))
        ];
    }
}