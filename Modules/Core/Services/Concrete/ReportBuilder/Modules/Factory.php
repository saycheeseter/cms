<?php
namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Enums\ModuleGroup;

use App;

class Factory
{   

    public function getInstance($module)
    {   
        return [
            $this->getServiceInstance($module),
            $this->getRepositoryInstance($module),
            \Modules\Core\Transformers\CustomReport\BasicTransformer::class
        ];
    }

    public static function getServiceInstance($module)
    {   
        switch($module) {
            case ModuleGroup::PURCHASE_INBOUND:
                return App::make(PurchaseInboundReportBuilderService::class);
                break;
                
            case ModuleGroup::PRODUCT:
                return App::make(ProductReportBuilderService::class);
                break;

            case ModuleGroup::INVENTORY_ADJUST:
                return App::make(InventoryAdjustReportBuilderService::class); 
                break;

            case ModuleGroup::STOCK_DELIVERY_OUTBOUND:
                return App::make(StockDeliveryOutboundReportBuilderService::class); 
                break;

            case ModuleGroup::SALES:
                return App::make(SalesReportBuilderService::class); 
                break;

            case ModuleGroup::SALES_RETURN:
                return App::make(SalesReturnReportBuilderService::class);
                break;

            case ModuleGroup::SALES_OUTBOUND:
                return App::make(SalesOutboundReportBuilderService::class); 
                break;

            case ModuleGroup::STOCK_REQUEST:
                return App::make(StockRequestReportBuilderService::class);
                break;

            case ModuleGroup::STOCK_DELIVERY_INBOUND:
                return App::make(StockDeliveryInboundReportBuilderService::class); 
                break;

            case ModuleGroup::PURCHASE:
                return App::make(PurchaseReportBuilderService::class);
                break;

            case ModuleGroup::STOCK_DELIVERY:
                return App::make(StockDeliveryReportBuilderService::class);
                break;
        }
    }

    public static function getRepositoryInstance($module)
    {
        switch($module) {
            case ModuleGroup::PURCHASE_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseInboundRepository::class); 
                break;

            case ModuleGroup::STOCK_DELIVERY_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository::class); 
                break;

            case ModuleGroup::PRODUCT:
                return App::make(\Modules\Core\Repositories\Contracts\ProductRepository::class);
                break;

            case ModuleGroup::INVENTORY_ADJUST:
                return App::make(\Modules\Core\Repositories\Contracts\InventoryAdjustRepository::class); 
                break;

            case ModuleGroup::SALES:
                return App::make(\Modules\Core\Repositories\Contracts\SalesRepository::class); 
                break;

            case ModuleGroup::SALES_RETURN:
                return App::make(\Modules\Core\Repositories\Contracts\SalesReturnRepository::class); 
                break;

            case ModuleGroup::SALES_OUTBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\SalesOutboundRepository::class); 
                break;

            case ModuleGroup::STOCK_REQUEST:
                return App::make(\Modules\Core\Repositories\Contracts\StockRequestRepository::class); 
                break;

            case ModuleGroup::STOCK_DELIVERY_INBOUND:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository::class); 
                break;

            case ModuleGroup::PURCHASE:
                return App::make(\Modules\Core\Repositories\Contracts\PurchaseRepository::class);
                break;

            case ModuleGroup::STOCK_DELIVERY:
                return App::make(\Modules\Core\Repositories\Contracts\StockDeliveryRepository::class); 
                break;
        }
    }
}