<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\SalesReturnReportBuilderServiceInterface;
use Modules\Core\Repositories\Contracts\CustomerRepository;

use Lang;
use App;

class SalesReturnReportBuilderService extends InvoiceReportBuilderService implements SalesReturnReportBuilderServiceInterface
{   
    protected $info = 'sales_return';

    protected function additionalFilters()
    {
        return [
            'customer' => [
                'name' => Lang::get('core::label.regular.customer'),
                'columnName' => 'sales_return_customer.name',
                'type' => 'string',
                'optionBool' => true,
                'tableOptions' => [
                    'data' => App::make(CustomerRepository::class)->all(),
                    'headers' => [Lang::get('core::label.code'), Lang::get('core::label.regular.customer')],
                    'columns' => ['code', 'name'],
                    'returncolumn' => 'name'
                ]
            ],
            'customer_walk_in' => [
                'name' => Lang::get('core::label.walk.in.name'),
                'columnName' => 'sales_return.customer_walk_in_name',
                'type'=> 'string', 
                'optionBool'=> false,
            ]
        ];
    }
}