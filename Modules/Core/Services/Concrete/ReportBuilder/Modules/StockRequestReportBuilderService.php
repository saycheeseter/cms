<?php 

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Services\Contracts\ReportBuilder\Modules\StockRequestReportBuilderServiceInterface;

class StockRequestReportBuilderService extends InvoiceReportBuilderService implements StockRequestReportBuilderServiceInterface
{   
    protected $info = 'stock_request';
}