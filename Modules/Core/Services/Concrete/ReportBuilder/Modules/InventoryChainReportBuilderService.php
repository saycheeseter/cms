<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\ProductSeniorDiscount;

use Lang;


class InventoryChainReportBuilderService extends BaseReportBuilderService
{   
    use FractalTransformer;

    protected $info;

    public function getFilters()
    {
        return array_merge(
            $this->filters(),
            $this->additionalFilters()
        );
    }

    protected function additionalFilters()
    {
        return [];
    }

    protected function filters()
    {   
        return [
            'approval_status' => [
                'name' => Lang::get('core::label.approval.status'),
                'columnName' => sprintf('%s.approval_status', $this->info),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ApprovalStatus::DRAFT, 'label' => Lang::get('core::label.draft') ],
                        [ 'id' => ApprovalStatus::FOR_APPROVAL, 'label' => Lang::get('core::label.for.approval') ],
                        [ 'id' => ApprovalStatus::APPROVED, 'label' => Lang::get('core::label.approved') ],
                        [ 'id' => ApprovalStatus::DECLINED, 'label' => Lang::get('core::label.declined') ],
                ]
            ],
            'product_status' => [
                'name' => Lang::get('core::label.product.status'),
                'columnName' => sprintf('%s_detail_product.status', $this->info),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ProductStatus::INACTIVE, 'label' => Lang::get('core::label.inactive') ],
                        [ 'id' => ProductStatus::ACTIVE, 'label' => Lang::get('core::label.active') ],
                        [ 'id' => ProductStatus::DISCONTINUED, 'label' => Lang::get('core::label.discontinued') ]
                ]
            ],
            'product_senior' => [
                'name' => Lang::get('core::label.senior'),
                'columnName' => sprintf('%s_detail_product.senior', $this->info),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ProductSeniorDiscount::NON_SENIOR, 'label' => Lang::get('core::label.non.senior') ],
                        [ 'id' => ProductSeniorDiscount::SENIOR_5, 'label' => Lang::get('core::label.senior.5%') ],
                        [ 'id' => ProductSeniorDiscount::SENIOR_20, 'label' => Lang::get('core::label.senior.20%') ]
                ]
            ],
            'sheet_number' => $this->stringOnlyFilter(sprintf('%s.sheet_number', $this->info), Lang::get('core::label.sheet.no')),
            'remarks' => $this->stringOnlyFilter(sprintf('%s.remarks', $this->info), Lang::get('core::label.remarks')),
            'transaction_date' => $this->transactionDateFilter(sprintf('%s.transaction_date', $this->info), Lang::get('core::label.transaction.date.time'), true),
            'branch' => $this->branchFilter(sprintf('%s.created_for', $this->info), Lang::get('core::label.branch'), true),
            'product_supplier' => $this->supplierFilter(sprintf('%s_detail_product_supplier.full_name', $this->info), Lang::get('core::label.product.supplier')),
            'product_category' => $this->categoryFilter(sprintf('%s_detail_product_category.name', $this->info), Lang::get('core::label.product.category')),
            'product_brand' => $this->brandFilter(sprintf('%s_detail_product_brand.name', $this->info), Lang::get('core::label.product.brand'))
        ];
    }
}