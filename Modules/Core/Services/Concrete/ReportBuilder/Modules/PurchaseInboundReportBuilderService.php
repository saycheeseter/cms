<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\PurchaseInboundReportBuilderServiceInterface;
use Modules\Core\Repositories\Contracts\SupplierRepository;

use Lang;
use Carbon\Carbon;
use App;
use Auth;
use DB;

class PurchaseInboundReportBuilderService extends InventoryChainReportBuilderService implements PurchaseInboundReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $info = 'purchase_inbound';

    protected function additionalFilters()
    {
        return [
            'supplier' => [
                'name' => Lang::get('core::label.supplier'),
                'columnName' => 'purchase_inbound_supplier.full_name',
                'type' => 'string',
                'optionBool' => true,
                'tableOptions' => [
                    'data' => App::make(SupplierRepository::class)->all(),
                    'headers' => [Lang::get('core::label.code'), Lang::get('core::label.supplier.name')],
                    'columns' => ['code', 'full_name'],
                    'returncolumn' => 'full_name'
                ]
            ]
        ];
    }
}