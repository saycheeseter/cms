<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\InventoryAdjustReportBuilderServiceInterface;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\ProductSeniorDiscount;
use Lang;

class InventoryAdjustReportBuilderService extends BaseReportBuilderService implements InventoryAdjustReportBuilderServiceInterface
{   
    use FractalTransformer;

    protected $tables = [
        'head' => 'inventory_adjust',
        'detail' => 'inventory_adjust_detail',
        'location' => 'inventory_adjust_for_location',
        'requested' => 'inventory_adjust_requested_by',
        'unit' => 'inventory_adjust_detail_unit'
    ];

    public function getFilters() 
    {
        return [
            'product_status' => [
                'name' => Lang::get('core::label.product.status'),
                'columnName' => sprintf('%s_detail_product.status', $this->tables['head']),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ProductStatus::INACTIVE, 'label' => Lang::get('core::label.inactive') ],
                        [ 'id' => ProductStatus::ACTIVE, 'label' => Lang::get('core::label.active') ],
                        [ 'id' => ProductStatus::DISCONTINUED, 'label' => Lang::get('core::label.discontinued') ]
                ]
            ],
            'product_senior' => [
                'name' => Lang::get('core::label.senior'),
                'columnName' => sprintf('%s_detail_product.senior', $this->tables['head']),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ProductSeniorDiscount::NON_SENIOR, 'label' => Lang::get('core::label.non.senior') ],
                        [ 'id' => ProductSeniorDiscount::SENIOR_5, 'label' => Lang::get('core::label.senior.5%') ],
                        [ 'id' => ProductSeniorDiscount::SENIOR_20, 'label' => Lang::get('core::label.senior.20%') ]
                ]
            ],
            'transaction_date' => $this->transactionDateFilter(sprintf('%s.transaction_date', $this->tables['head']), Lang::get('core::label.transaction.date.time'), true),
            'branch' => $this->branchFilter(sprintf('%s.for_location', $this->tables['head']), Lang::get('core::label.branch'), true),
            'supplier' => $this->supplierFilter(sprintf('%s_product_supplier.full_name', $this->tables['detail'])),
            'category' => $this->categoryFilter(sprintf('%s_product_category.name', $this->tables['detail'])),
            'brand' => $this->brandFilter(sprintf('%s_product_brand.name', $this->tables['detail']))
        ];
    }
}