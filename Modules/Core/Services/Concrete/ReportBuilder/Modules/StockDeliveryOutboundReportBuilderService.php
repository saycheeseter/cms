<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\StockDeliveryOutboundReportBuilderServiceInterface;

use Lang;

class StockDeliveryOutboundReportBuilderService extends InventoryChainReportBuilderService implements StockDeliveryOutboundReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $info = 'stock_delivery_outbound';

    protected function additionalFilters()
    {
        return [
            'deliver.to' => $this->branchFilter(sprintf('%s.deliver_to', $this->info), Lang::get('core::label.deliver.to'))
        ];
    }
}