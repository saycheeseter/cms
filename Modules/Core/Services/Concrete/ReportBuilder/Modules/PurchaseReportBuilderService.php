<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\PurchaseReportBuilderServiceInterface;
use Modules\Core\Repositories\Contracts\SupplierRepository;

use Lang;
use App;

class PurchaseReportBuilderService extends InvoiceReportBuilderService implements PurchaseReportBuilderServiceInterface
{   
    protected $info = 'purchase';

     protected function additionalFilters()
    {
        return [
            'supplier' => $this->supplierFilter('purchase_supplier.full_name', Lang::get('core::label.supplier'))
        ];
    }
}