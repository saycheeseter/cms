<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\ProductReportBuilderServiceInterface;

use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Repositories\Contracts\BranchRepository;

use Modules\Core\Transformers\Branch\ChosenTransformer as BranchChosenTransformer;

use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\InventoryStatus;
use Lang;
use App;
use Auth;

class ProductReportBuilderService extends BaseReportBuilderService implements ProductReportBuilderServiceInterface
{   
    use FractalTransformer;
    
    protected $tables = [
        'info' => 'product',
        'barcode' => 'product_barcode',
        'inventory' => 'product_branch_summary',
        'price' => 'branch_price',
        'branch_detail' => 'product_branch_detail',
        'branch_info' => 'product_branch_info',
        'tax' => 'product_tax',
        'unit' => 'product_unit'
    ];

    public function getFilters()
    {
        return array_merge(
            $this->filters()
        );
    }

    protected function filters()
    {
        return [
            'stock_no' => $this->stringOnlyFilter(sprintf('%s.stock_no', $this->tables['info']), Lang::get('core::label.stock.no')),
            'unit_barcode' => $this->stringOnlyFilter(sprintf('%s.code', $this->tables['barcode']), Lang::get('core::label.unit.barcode')),
            'name' => $this->stringOnlyFilter(sprintf('%s.name', $this->tables['info']), Lang::get('core::label.name')),
            'chinese_name' => $this->stringOnlyFilter(sprintf('%s.chinese_name', $this->tables['info']), Lang::get('core::label.chinese.name')),
            'status' => [
                'name' => Lang::get('core::label.status'),
                'columnName' => sprintf('%s.status', $this->tables['info']),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => ProductStatus::INACTIVE, 'label' => Lang::get('core::label.inactive') ],
                        [ 'id' => ProductStatus::ACTIVE, 'label' => Lang::get('core::label.active') ],
                        [ 'id' => ProductStatus::DISCONTINUED, 'label' => Lang::get('core::label.discontinued') ],
                ]
            ],
            'supplier' => $this->supplierFilter(sprintf('%s_supplier.full_name', $this->tables['info']), Lang::get('core::label.supplier')),
            'category' => $this->categoryFilter(sprintf('%s_category.name', $this->tables['info']), Lang::get('core::label.category')),
            'brand' => $this->brandFilter(sprintf('%s_brand.name', $this->tables['info']), Lang::get('core::label.brand')),
            'date_created' => $this->datetimeFilter(sprintf('%s.created_at', $this->tables['info']), Lang::get('core::label.date.created')),
            'last_modified' => $this->datetimeFilter(sprintf('%s.updated_at', $this->tables['info']), Lang::get('core::label.last.modified')),
            'inventory_status' => [
                'name' => Lang::get('core::label.inventory.status'),
                'columnName' => sprintf('product_%s.qty', $this->tables['inventory']),
                'type' => 'select',
                'selectOptions' => [
                        [ 'id' => InventoryStatus::NEGATIVE, 'label' => Lang::get('core::label.negative') ],
                        [ 'id' => InventoryStatus::NO_INVENTORY, 'label' => Lang::get('core::label.no.inventory') ],
                        [ 'id' => InventoryStatus::WITH_INVENTORY, 'label' => Lang::get('core::label.with.inventory') ],
                ]
            ],
            'product_branch_summary_branch' => [
                'name' => Lang::get('core::label.product').' '.Lang::get('core::label.branch').' '.Lang::get('core::label.inventory').' '.Lang::get('core::label.branch'),
                'columnName' => sprintf('product_%s.branch_id', $this->tables['inventory']),
                'type' => 'select',
                'selectOptions' => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            ],
            'branch_price_branch' => [
                'name' => Lang::get('core::label.branch.price').' '.Lang::get('core::label.branch'),
                'columnName' => sprintf('product_%s.branch_id', $this->tables['price']),
                'type' => 'select',
                'selectOptions' => $this->collection(App::make(BranchRepository::class)->all(), BranchChosenTransformer::class)['data'],
            ]
        ];
    }
}