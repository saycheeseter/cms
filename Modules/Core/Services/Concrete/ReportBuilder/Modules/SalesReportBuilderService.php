<?php

namespace Modules\Core\Services\Concrete\ReportBuilder\Modules;

use Modules\Core\Traits\FractalTransformer;
use Modules\Core\Services\Contracts\ReportBuilder\Modules\SalesReportBuilderServiceInterface;
use Modules\Core\Repositories\Contracts\CustomerRepository;

use Lang;
use App;

class SalesReportBuilderService extends InvoiceReportBuilderService implements SalesReportBuilderServiceInterface
{   
    protected $info = 'sales';

    protected function additionalFilters()
    {
        return [
            'customer' => [
                'name' => Lang::get('core::label.regular.customer'),
                'columnName' => 'sales_customer.name',
                'type' => 'string',
                'optionBool' => true,
                'tableOptions' => [
                    'data' => App::make(CustomerRepository::class)->all(),
                    'headers' => [Lang::get('core::label.code'), Lang::get('core::label.regular.customer')],
                    'columns' => ['code', 'name'],
                    'returncolumn' => 'name'
                ]
            ],
            'customer_walk_in' => [
                'name' => Lang::get('core::label.walk.in.name'),
                'columnName' => 'sales.customer_walk_in_name',
                'type'=> 'string', 
                'optionBool'=> false,
            ]
        ];
    }
}