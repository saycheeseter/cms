<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\IncomeTypeServiceInterface;
use Modules\Core\Entities\IncomeType;

class IncomeTypeService extends BaseService implements IncomeTypeServiceInterface
{
    protected function model()
    {
        return IncomeType::class;
    }
}