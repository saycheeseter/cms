<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\DamageOutboundServiceInterface;
use Modules\Core\Services\Concrete\InventoryChainService;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\DamageOutboundDetail;
use Modules\Core\Entities\DamageOutboundSerialNumber;

class DamageOutboundService extends InventoryChainService implements DamageOutboundServiceInterface
{
    protected function models()
    {
        $this->info = DamageOutbound::class;
        $this->details = DamageOutboundDetail::class;
        $this->serial = DamageOutboundSerialNumber::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'reason_id' => $data['reason_id'],
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'reason_id' => $data['reason_id']
        ]);
    }
}
