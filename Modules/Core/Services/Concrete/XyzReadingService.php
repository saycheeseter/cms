<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Modules\Core\Services\Contracts\XyzReadingServiceInterface;
use Modules\Core\Entities\XyzReading;

class XyzReadingService extends BaseService implements XyzReadingServiceInterface
{
    protected function model()
    {
        return XyzReading::class;
    }

    protected function transform($data)
    {
        $returned =  array_only($data, [
            'created_from',
            'read_count',
            'read_type',
            'start_or',
            'end_or',
            'date',
            'terminal_number',
            'branch_id',
            'old_grand_total',
            'new_grand_total',
            'total_discount_amount',
            'vat_returns_amount',
            'nonvat_returns_amount',
            'senior_returns_amount',
            'cash_sales_amount',
            'credit_sales_amount',
            'debit_sales_amount',
            'bank_sales_amount',
            'gift_sales_amount',
            'senior_sales_amount',
            'vatable_sales_amount',
            'nonvat_sales_amount',
            'total_qty_sold',
            'total_qty_void',
            'void_transaction_amount',
            'success_transaction_count',
            'return_qty_count',
            'member_points_amount'
        ]);

        $returned['date'] = Carbon::parse($returned['date']);

        return $returned;
    }
}