<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\UserDefinedFieldServiceInterface;
use Modules\Core\Entities\UserDefinedField;

class UserDefinedFieldService extends BaseService implements UserDefinedFieldServiceInterface
{
    protected function model()
    {
        return UserDefinedField::class;
    }
}