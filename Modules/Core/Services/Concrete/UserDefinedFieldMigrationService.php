<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\UserDefinedFieldMigrationServiceInterface;
use Modules\Core\Entities\UserDefinedField;
use Illuminate\Database\Schema\Blueprint;
use Artisan;
use App;
use Schema;

class UserDefinedFieldMigrationService implements UserDefinedFieldMigrationServiceInterface
{
    public function migrate(UserDefinedField $udf)
    {
        if (!Schema::hasColumn($udf->table, $udf->alias)) {
            Schema::table($udf->table, function (Blueprint $table) use ($udf){

                $fieldType = is_array($udf->field_type)
                    ? $udf->field_type[0]
                    : $udf->field_type;

                $fieldArgs = [$udf->alias];

                if (is_array($udf->field_type)) {
                    for ($i=1; $i < count($udf->field_type); $i++) {
                        $fieldArgs[] = $udf->field_type[$i];
                    }
                }

                call_user_func_array(array($table, $fieldType), $fieldArgs)->nullable();
            });
        }

        Artisan::call('project:dump-udf');
    }

    public function dropColumn(UserDefinedField $udf)
    {
        if (Schema::hasColumn($udf->table, $udf->alias)) {
            Schema::table($udf->table, function (Blueprint $table) use ($udf) {
                $table->dropColumn($udf->alias);
            });
        }

        Artisan::call('project:dump-udf');
    }
}