<?php 

namespace Modules\Core\Services\Concrete;

use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Services\Contracts\PurchaseServiceInterface;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\PurchaseDetail;
use Modules\Core\Transformers\Product\SupplierPriceTransformer;
use Carbon\Carbon;
use Auth;
use App;
use Fractal;

class PurchaseService extends InvoiceService implements PurchaseServiceInterface
{
    protected function models()
    {
        $this->info = Purchase::class;
        $this->details = PurchaseDetail::class;
    }

    /**
     * Create a new transaction with a polymorphic relationship to Stock Request and 
     * each transaction is grouped according to the product's supplier
     *
     * @param  array  $attributes
     * @return Model
     */
    public function createFromStockRequestBySupplier(array $attributes)
    {
        // Convert the data to a collection to easily manipulate the content
        $attributes = collect([
            'info' => collect(array_merge($attributes['info'], [
                'transaction_date' => Carbon::create(), 
                'remarks' => '', 
                'created_for' => Auth::branch()->id,
                'for_location' => Auth::branch()->details->first()->id,
                'requested_by' => Auth::user()->id,
                'udfs' => [],
                'transactionable_type' => 'stock_request',
                'payment_method' => PaymentMethod::CASH,
                'deliver_until' => Carbon::create()->toDateString(),
                'term' => 0
            ])),
            'details' => collect($attributes['details'])
        ]);

        $transactions = [];

        $productGroupBySuppliers = $this->groupProductsBySuppliers($attributes->get('details'));
        
        $productPrices = $this->getPricesByProductSuppliers($productGroupBySuppliers);

        foreach ($productGroupBySuppliers as $supplier => $products) {
            $transaction = [
                'info' => array_merge(
                    $attributes->get('info')->toArray(),
                    ['supplier_id' => $supplier]
                ),
                'details' => []
            ];

            foreach ($products as $product) {
                $prices = $productPrices->get($product->id);

                foreach ($attributes->get('details')->where('product_id', $product->id) as $detail) {
                    $transaction['details'][] = array_merge(
                        $detail, 
                        [
                            'transactionable_type' => 'stock_request_detail', 
                            'price' => $prices['price'],
                            'oprice' => $prices['price'],
                        ]
                    );
                }
            }

            $transactions[] = $transaction;
        }

        return $this->bulkStore($transactions)->pluck('info');
    }

    /**
     * Get the details of the selected product and group them by supplier
     * 
     * @param  Collection $details
     * @return Collection
     */
    private function groupProductsBySuppliers($details)
    {
        $ids = $details->pluck('product_id');

        $results = App::make(ProductRepository::class)->findMany($ids);

        return $results->groupBy('supplier_id');
    }

    /**
     * Get current prices of each product based on supplier and settings
     * 
     * @param  Collection $productSuppliers
     * @return Collection
     */
    private function getPricesByProductSuppliers($productSuppliers)
    {
        $prices = [];

        $repository = App::make(ProductRepository::class);

        foreach ($productSuppliers as $key => $group) {
            $results = $repository->getSupplierPrices(
                $group->pluck('id')->toArray(),
                $key,
                Auth::branch()->id,
                Auth::branch()->details->first()->id
            );

            // Merge current prices with the new one
            $prices = array_merge(
                array_values($prices),
                array_values(Fractal::collection($results, new SupplierPriceTransformer)->getArray()['data'])
            );
        }

        return collect($prices)->keyBy('id');
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'supplier_id' => $data['supplier_id'],
            'payment_method' => $data['payment_method'],
            'deliver_to' => $data['deliver_to'],
            'deliver_until' => $data['deliver_until'],
            'term' => $data['term'],
            'transactionable_type' => $data['transactionable_type'] ?? null,
            'transactionable_id' => $data['transactionable_id'] ?? null,
        ]);
    }

    /**
     * Transforms data
     * 
     * @param  array  $data
     * @return array      
     */
    protected function transformDetail(array $data)
    {
        return array_merge(parent::transformDetail($data), [
            'transactionable_type' => $data['transactionable_type'] ?? null,
            'transactionable_id' => $data['transactionable_id'] ?? null,
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'payment_method' => $data['payment_method'],
            'term' => $data['term']
        ]);
    }
}