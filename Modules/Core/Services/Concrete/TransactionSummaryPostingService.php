<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\Contracts\GenerateAutoConversionProductMonthlySummary;
use Modules\Core\Entities\GenericSetting;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\CustomerPricingType;
use Modules\Core\Services\Contracts\CustomerProductPricingServiceInterface;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface;
use Modules\Core\Services\Contracts\SupplierProductPricingServiceInterface;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Traits\GenerateConversionProductMonthlySummary;
use Modules\Core\Traits\GenerateInventoryAdjustProductMonthlySummary;
use Modules\Core\Traits\GenerateTransactionProductMonthlySummary;
use Modules\Core\Traits\HasCurrentCostAsMAC;
use Modules\Core\Traits\HasInventoryAndManagementProcess;
use App;

class TransactionSummaryPostingService implements TransactionSummaryPostingServiceInterface
{
    use DatabaseTransaction;

    public function insert($model)
    {
        $posting = new TransactionSummaryPosting;
        $posting->transaction_date = $model->transaction_date;
        $model->posting()->save($posting);
    }

    public function delete($model)
    {
        $model->posting()->delete();
    }

    public function process($date)
    {
        $this->transaction(function() use($date) {
            $from = setting('transaction.summary.computation.last.post.date');
            $to = $date;

            $builder = TransactionSummaryPosting::where('transaction_date', '<=', $to);

            if (null !== $from) {
                $builder->where('transaction_date', '>', $from);
            }

            $postings = $builder->orderBy('transaction_date', 'ASC')
                ->orderBy('id', 'ASC')
                ->cursor();

            $supplierProductPricingService = App::make(SupplierProductPricingServiceInterface::class);
            $customerProductPricingService = App::make(CustomerProductPricingServiceInterface::class);
            $transactionMovingAverageCostService = App::make(TransactionMovingAverageCostServiceInterface::class);
            $productStockCardService = App::make(ProductStockCardServiceInterface::class);
            $productMonthlyTransactionService = App::make(ProductMonthlyTransactionServiceInterface::class);

            foreach ($postings as $posting) {
                $model = $posting->transaction;

                $traits = class_uses_recursive(get_class($model));

                if (in_array(HasCurrentCostAsMAC::class, $traits)) {
                    $model = $transactionMovingAverageCostService->updateTransactionDetails($model);
                }

                if (in_array(HasInventoryAndManagementProcess::class, $traits)) {
                    $productStockCardService->create($model);
                }

                if ($model instanceof AutoProductConversion) {
                    $model = $transactionMovingAverageCostService->updateAutoConversion($model);
                    $productStockCardService->createFromProductConversion($model);
                    $productMonthlyTransactionService->generateFromAutoConversion($model);
                }

                if ($model instanceof ProductConversion) {
                    $model = $transactionMovingAverageCostService->updateManualConversion($model);
                    $productStockCardService->createFromProductConversion($model);
                }

                if ($model instanceof PurchaseInbound) {
                    $supplierProductPricingService->updateHistory($model);
                }

                if ($model instanceof SalesOutbound) {
                    if ($model->customer_type != Customer::WALK_IN
                        && !is_null($model->customer)
                        && $model->customer->pricing_type  == CustomerPricingType::HISTORICAL
                    ) {
                        $customerProductPricingService->updateHistory($model);
                    }
                }

                if ($model instanceof InventoryAdjust) {
                    $productStockCardService->create($model);
                }

                if (in_array(GenerateTransactionProductMonthlySummary::class, $traits)) {
                    $productMonthlyTransactionService->generateFromTransaction($model);
                }

                if (in_array(GenerateInventoryAdjustProductMonthlySummary::class, $traits)) {
                    $productMonthlyTransactionService->generateFromInventoryAdjust($model);
                }

                if (in_array(GenerateConversionProductMonthlySummary::class, $traits)) {
                    $productMonthlyTransactionService->generateFromConversion($model);
                }
            }

            $lastPostDate = GenericSetting::where('key', 'transaction.summary.computation.last.post.date')->first();
            $lastPostDate->value = $date;
            $lastPostDate->save();

            $builder = TransactionSummaryPosting::getQuery()->where('transaction_date', '<=', $to);

            if (null !== $from) {
                $builder->where('transaction_date', '>', $from);
            }

            $builder->delete();
        });
    }
}