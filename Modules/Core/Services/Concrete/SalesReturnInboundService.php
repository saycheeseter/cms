<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Events\SalesReturnInboundApproved;
use Modules\Core\Events\SalesReturnInboundReverted;
use Modules\Core\Services\Contracts\SalesReturnInboundServiceInterface;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\SalesReturnInboundDetail;

class SalesReturnInboundService extends InventoryChainService implements SalesReturnInboundServiceInterface
{
    protected $events = [
        'approved' => SalesReturnInboundApproved::class,
        'revert'   => SalesReturnInboundReverted::class
    ];

    protected function models()
    {
        $this->info = SalesReturnInbound::class;
        $this->details = SalesReturnInboundDetail::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'customer_id' => $data['customer_id'],
            'customer_detail_id' => $data['customer_detail_id'],
            'salesman_id' => $data['salesman_id'],
            'customer_type' => $data['customer_type'],
            'customer_walk_in_name' => $data['customer_walk_in_name'],
            'term' => $data['term'],
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'salesman_id' => $data['salesman_id'],
            'term' => $data['term']
        ]);
    }
}
