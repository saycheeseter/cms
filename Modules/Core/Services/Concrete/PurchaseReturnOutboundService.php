<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\PurchaseReturnOutboundServiceInterface;
use Modules\Core\Services\Concrete\InventoryChainService;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\PurchaseReturnOutboundDetail;
use Modules\Core\Entities\PurchaseReturnOutboundSerialNumber;

class PurchaseReturnOutboundService extends InventoryChainService implements PurchaseReturnOutboundServiceInterface
{
    protected function models()
    {
        $this->info = PurchaseReturnOutbound::class;
        $this->details = PurchaseReturnOutboundDetail::class;
        $this->serial = PurchaseReturnOutboundSerialNumber::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        return array_merge(parent::transformInfo($data), [
            'supplier_id' => $data['supplier_id'],
            'payment_method' => $data['payment_method'],
            'term' => $data['term']
        ]);
    }

    protected function transformUpdateApprovedInfo($data)
    {
        return array_merge(parent::transformUpdateApprovedInfo($data), [
            'payment_method' => $data['payment_method'],
            'term' => $data['term']
        ]);
    }
}
