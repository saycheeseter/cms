<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\StockDeliveryInboundServiceInterface;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryInboundDetail;

class StockDeliveryInboundService extends InventoryChainService implements StockDeliveryInboundServiceInterface
{
    protected function models()
    {
        $this->info = StockDeliveryInbound::class;
        $this->details = StockDeliveryInboundDetail::class;
    }

    /**
     * Transforms data
     *
     * @param  array  $data
     * @return array
     */
    protected function transformInfo(array $data)
    {
        $transformation =  array_merge(
            array_except(parent::transformInfo($data), 'transaction_type'), [
            'delivery_from' => $data['delivery_from'],
            'delivery_from_location' => $data['delivery_from_location'],
        ]);

        array_pull($transformation, 'transaction_type');

        return $transformation;
    }
}
