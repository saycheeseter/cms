<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\CustomerProductPricingServiceInterface;
use Modules\Core\Entities\ProductCustomer;
use Modules\Core\Entities\SalesOutbound;

class CustomerProductPricingService extends ProductPricingService implements CustomerProductPricingServiceInterface
{
    protected $primaryKey = 'customer_id';

    public function __construct(ProductCustomer $productEntityLog, SalesOutbound $inventoryChain)
    {
        $this->productEntityLog = $productEntityLog;
        $this->inventoryChain = $inventoryChain;
    }

    protected function getUniqueFields($detail)
    {
        return array(
            'customer_id' => $detail->transaction->customer_id
        );
    }
}
