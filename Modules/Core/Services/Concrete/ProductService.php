<?php

namespace Modules\Core\Services\Concrete;

use App;
use Modules\Core\Entities\InventoryChain;
use Modules\Core\Entities\Invoice;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\Damage;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\Purchase;
use Modules\Core\Enums\ProductType;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Services\Contracts\ProductServiceInterface;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Carbon\Carbon;
use DB;

class ProductService implements ProductServiceInterface
{
    use DatabaseTransaction;

     /**
     * Persist entity in database
     *
     * @param  array  $attributes
     * @return Model
     */
    public function store(array $attributes)
    {
       $that = $this;

        return $this->transaction(function() use ($attributes, $that) {

            $product = new Product($that->transformInfo($attributes['product']));
            
            $product->save();

            $product = $that->saveRelationships($product, $attributes);

            $this->initializeSummmary($product);

            return $product;
        });
    }

    /**
     * Updates parent model data, and process data on child model (create, update, delete)
     *
     * @param  array  $attributes
     * @param  integer $id
     * @return Model
     */
    public function update(array $attributes, $id)
    {
         $that = $this;

        return $this->transaction(function() use ($attributes, $id, $that) {
            $product = Product::findOrFail($id);

            $product->fill($that->transformInfo($attributes['product']))->save();

            $product = $that->saveRelationships($product, $attributes);

            return $product;
        });
    }

    /**
     * Soft deletes product data
     * 
     * @param  int $id
     * @return Model
     */
    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $product = Product::findOrFail($id);

            $product->delete();

            return $product;
        });
    }

    public function updateTransactionsPurchasePrice($productId, $branchId, $to, $from, $cost)
    {
        return $this->transaction(function() use ($to, $from, $branchId, $cost, $productId) {
            $modules = [
                Purchase::class,
                PurchaseReturn::class,
                Damage::class,
                Sales::class,
                SalesReturn::class,
                StockRequest::class,
                StockDelivery::class,
                StockReturn::class,
                PurchaseInbound::class,
                PurchaseReturnOutbound::class,
                DamageOutbound::class,
                SalesOutbound::class,
                SalesReturnInbound::class,
                StockDeliveryOutbound::class,
                StockDeliveryInbound::class,
                StockReturnOutbound::class,
                StockReturnInbound::class,
                InventoryAdjust::class
            ];

            $to = new Carbon($to);
            $from = new Carbon($from);

            foreach ($modules as $module) {
                $transactions = App::make($module)
                    ->with(['details' => function($query) use ($productId){
                        $query->where('product_id', $productId);
                    }])
                    ->where('created_for', $branchId)
                    ->where(function($query) use($to, $from){
                        $query->where('transaction_date', '>=', $to->startOfDay()->toDateTimeString())
                            ->where('transaction_date', '<=', $from->endOfDay()->toDateTimeString());
                    })
                    ->whereHas('details', function($query) use ($productId){
                        $query->where('product_id', $productId);
                    })
                    ->cursor();
                
                foreach ($transactions as $transaction) {
                    foreach ($transaction->details as $item) {
                        $old = clone $item;

                        $item->cost = $cost;
                        $item->save();

                        $this->computePMTTotalCost($item, $old);
                    }
                }
            }

            $conversions = [
                ProductConversion::class,
                AutoProductConversion::class
            ];

            foreach ($conversions as $conversion) {
                $transactions = App::make($conversion)
                    ->where('created_for', $branchId)
                    ->where(function($query) use($to, $from){
                        $query->where('transaction_date', '>=', $to->startOfDay()->toDateTimeString())
                            ->where('transaction_date', '<=', $from->endOfDay()->toDateTimeString());
                    })
                    ->cursor();
                
                foreach ($transactions as $transaction) {
                    if ($transaction->product_id == $productId) {
                        $old = clone $transaction;
                        $transaction->cost = $cost;
                        $transaction->save();

                        if($transaction instanceof ProductConversion) {
                            $this->computeConversionPMTTotalCost($transaction, $old);
                        }
                    } else {
                        $transaction->load(['details']);

                        foreach ($transaction->details as $item) {
                            if($item->product_id == $productId 
                            || (is_null($item->product_id) && $item->component_id == $productId)) {
                                $old = clone $item;

                                $item->cost = $cost;
                                $item->save();

                                $this->computePMTTotalCost($item, $old);
                            }
                        }
                    }
                }
            }

            return true;
        });
    }

    private function computePMTTotalCost($item, $old)
    {
        $transaction = $item->transaction;

        if (is_subclass_of($transaction, Invoice::class)
            || (is_subclass_of($transaction, InventoryChain::class) && $transaction->approval_status !== ApprovalStatus::APPROVED)
            || ($transaction instanceof ProductConversion && $transaction->approval_status !== ApprovalStatus::APPROVED)
            || ($transaction instanceof InventoryAdjust && in_array($item->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP]))
            || ($transaction instanceof ProductConversion && !in_array($transaction->group_type, [GroupType::MANUAL_GROUP, GroupType::MANUAL_UNGROUP]))
            || ($transaction instanceof AutoProductConversion && !in_array($transaction->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP]))
        ) {
            return $this;
        }

        $identifiers = [
            'created_for' => $transaction->product_monthly_transaction_created_for,
            'for_location' => $transaction->product_monthly_transaction_for_location,
            'transaction_date' => $transaction->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $item->product_id ?? 0
        ];

        $fields = [];

        if (is_subclass_of($transaction, InventoryChain::class)) {
            $prefix = $transaction->getProductMonthlyTransactionAttributeMappingPrefix();

            $fields[$prefix.'_total_cost'] = $item->total_cost_amount->sub($old->total_cost_amount);
        } else if ($transaction instanceof InventoryAdjust) {
            $prefix = $item->process_flow;
            $fields[$prefix.'_total_cost'] = $item->total_cost_amount->abs()->sub($old->total_cost_amount->abs());
        } else if ($transaction instanceof ProductConversion) {
            $prefix = $item->process_flow;

            $fields[$prefix.'_total_cost'] = $item->total_cost->sub($old->total_cost);
            $identifiers['product_id'] = $item->component_id;
        } else if ($transaction instanceof AutoProductConversion) {
            $prefix = $transaction->process_flow;

            $fields[$prefix.'_total_cost'] = $item->total_cost->sub($old->total_cost);
        }

        $this->updateTotalCostOnPMT($identifiers, $fields);

        return $this;
    }

    private function updateTotalCostOnPMT($identifiers, $fields)
    {
        $summary = ProductMonthlyTransactions::take(1)
            ->where($identifiers)
            ->first();

        if (is_null($summary)) {
            $summary = new ProductMonthlyTransactions($identifiers);

            foreach ($fields as $attribute => $value) {
                $summary->{ $attribute } = $value;
            }

            $summary->save();
        } else {
            $columns = [];

            foreach ($fields as $attribute => $value) {
                $columns[$attribute] = DB::raw($attribute.'+'.$value);
            }

            $summary->where($identifiers)->update($columns);
        }
    }

    private function computeConversionPMTTotalCost($transaction, $old)
    {
        $identifiers = [
            'created_for' => $transaction->product_monthly_transaction_created_for,
            'for_location' => $transaction->product_monthly_transaction_for_location,
            'transaction_date' => $transaction->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $transaction->product_id
        ];

        $prefix = $transaction->process_flow;

        $fields[$prefix.'_total_cost'] = $transaction->total_cost->sub($old->total_cost);

        $this->updateTotalCostOnPMT($identifiers, $fields);
    }

    /**
     * Initiate saving of product relationships
     * 
     * @param  Product  $product
     * @param  array  $attributes
     * @param  integer $id
     * @return Product
     */
    private function saveRelationships(Product $product, $attributes)
    {
        if(isset($attributes['prices'])) {
            $product->prices()->sync($this->transformData($attributes['prices'], 'transformPrice'));
        }

        if(isset($attributes['units'])) {
            $product->units()->sync($this->transformData($attributes['units'], 'transformUnit'));
        }

        if(isset($attributes['barcodes'])) {
            $product->barcodes()->sync($this->transformData($attributes['barcodes'], 'transformBarcode'));
        }

        if(isset($attributes['branches'])) {
            $product->branches()->sync($this->transformData($attributes['branches'], 'transformBranch'));
        }

        if(isset($attributes['locations'])) {
            $product->locations()->sync($this->transformData($attributes['locations'], 'transformLocation'));
        }

        if(isset($attributes['components'])) {
            $changes = $product->components()->sync($this->transformComponents($attributes['components']));
            $this->processComponents($changes);
        }

        if(isset($attributes['taxes'])){
            $product->taxes()->sync($attributes['taxes']);
        }

        if(isset($attributes['alternatives'])){
            $product->alternatives()->sync($attributes['alternatives']);
        }

        return $product;
    }

    /**
     * Transforms product data
     * 
     * @param  array  $data
     * @return array
     */
    private function transformInfo(array $data)
    {
        return [
            'stock_no'     => $data['stock_no'] ?? '',
            'supplier_sku' => $data['supplier_sku'] ?? '',
            'name'         => $data['name'] ?? '',
            'description'  => $data['description'] ?? '',
            'chinese_name' => $data['chinese_name'] ?? '',
            'supplier_id'  => $data['supplier_id'] ?? '',
            'brand_id'     => $data['brand_id'] ?? '',
            'category_id'  => $data['category_id'] ?? '',
            'manage_type'  => $data['manage_type'] ?? '',
            'senior'       => $data['senior'] ?? '',
            'status'       => $data['status'] ?? '',
            'memo'         => $data['memo'] ?? '',
            'group_type'   => $data['group_type'] ?? 0,
            'manageable'   => $data['manageable'] ?? true,
            'type'         => $this->type($data),
            'default_unit' => $data['default_unit'],
        ];
    }

    private function type($data)
    {
        if(is_null($data['group_type']) || (int) $data['group_type'] == GroupType::NONE) {
            return ProductType::REGULAR;
        } else {
            return ProductType::GROUP;
        }
    }

    /**
     * Transforms units array
     *
     * @param  array  $data
     * @return array
     */
    private function transformUnit(array $data)
    {
        return [
            'uom_id' => $data['uom_id'],
            'qty' => $data['qty'],
            'length' => $data['length'] ?? 0,
            'width' => $data['width'] ?? 0,
            'height' => $data['height'] ?? 0,
            'weight' => $data['weight'] ?? 0,
        ];
    }

    /**
     * Transforms prices array
     *
     * @param  array  $data
     * @return array
     */
    private function transformPrice(array $data)
    {
        return [
            'branch_id' => $data['branch_id'],
            'purchase_price' => $data['purchase_price'],
            'selling_price' => $data['selling_price'],
            'wholesale_price' => $data['wholesale_price'],
            'price_a' => $data['price_a'] ?? 0,
            'price_b' => $data['price_b'] ?? 0,
            'price_c' => $data['price_c'] ?? 0,
            'price_d' => $data['price_d'] ?? 0,
            'price_e' => $data['price_e'] ?? 0,
            'editable' => $data['editable'] ?? 0,
            'copy_from' => $data['copy_from'] ?? null,
        ];
    }

    /**
     * Transforms barcodes array
     *
     * @param  array  $data
     * @return array
     */
    private function transformBarcode(array $data)
    {
        return [
            'uom_id' => $data['uom_id'],
            'code' => $data['code'],
            'memo' => $data['memo'] ?? ''
        ];
    }

    /**
     * Transforms branch array
     *
     * @param  array  $data
     * @return array
     */
    private function transformBranch(array $data)
    {
        return [
            'branch_id' => $data['branch_id'],
            'status' => $data['status'] ?? 0
        ];
    }

    /**
     * Transforms locations array
     *
     * @param  array  $data
     * @return array
     */
    private function transformLocation(array $data)
    {
        return [
            'branch_detail_id' => $data['branch_detail_id'],
            'min' => $data['min'] ?? 0,
            'max' => $data['max'] ?? 0
        ];
    }

    /**
     * Transforms inventory array
     *
     * @param  array  $data
     * @return array
     */
    private function transformSummary(array $data)
    {
        return [
            'branch_id' => $data['branch_id'],
            'branch_detail_id' => $data['branch_detail_id'],
            'qty' => $data['qty'] ?? 0,
        ];
    }

    /**
     * Transforms components array
     *
     * @param  array  $data
     * @return array
     */
    private function transformComponents(array $data)
    {
        $return = [];

        foreach ($data as $key => $value) {
            $return[$value['component_id']] = [
                'qty' => $value['qty']
            ];
        }

        return $return;
    }

    /**
     * Transform each data as model and push it to a collection instance
     * 
     * @param  Model $model
     * @param  array  $attributes
     * @param  string $transformer
     * @return Collection
     */
    private function hydrateToCollection($model, array $attributes, $transformer)
    {
        $collection = [];

        foreach($attributes as $data) {
            $collection[] = App::make($model)->newInstance($this->$transformer($data));
        }

        return $collection;
    }

    private function transformData($data, $transformer)
    {
        $collection = [];

        foreach($data as $value) {
            $collection[] = $this->$transformer($value);
        }

        return $collection;
    }

    /**
     * Inserts new product branch inventory row in database. Value will be set
     * to zero
     * 
     * @param  Product $info
     * @return $this
     */
    private function initializeSummmary(Product $product)
    {
        $product->summaries()->sync($this->transformData($this->getBranchAndLocationCollection(), 'transformSummary'));

        return $this;
    }

    /**
     * Get formatted branch and branch detail array from
     * branch table
     * 
     * @return array
     */
    private function getBranchAndLocationCollection()
    {
        $branches = Branch::with('details')->get();

        return $branches->flatMap(function($item) {
            return $item->details->map(function($detail) {
                return array(
                    'branch_id' => $detail->branch_id,
                    'branch_detail_id' => $detail->id
                );
            })->toArray();
        })->toArray();
    }

    private function processComponents($changes)
    {
        $this->changedProductToRegular($changes['detached']);
        $this->changedProductToComponent($changes['attached']);
        $this->changedProductToComponent($changes['updated']);

        return true;
    }

    private function changedProductToRegular($ids)
    {
        return $this->changeProductType($ids, ProductType::REGULAR);
    }

    private function changedProductToComponent($ids)
    {
        return $this->changeProductType($ids, ProductType::COMPONENT);
    }

    private function changeProductType($ids = [], $status)
    {
        if(count($ids) == 0) {
            return false;
        }

        $products = Product::whereIn('id', $ids)->get();

        $products->each(function ($item, $key) use ($status) {
            $item->fill(['type' => $status])->save();
        });

        return $products;
    }
}
