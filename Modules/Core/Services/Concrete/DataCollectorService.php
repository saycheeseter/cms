<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Services\Contracts\DataCollectorServiceInterface;
use Modules\Core\Entities\DataCollectorTransaction;
use Modules\Core\Entities\DataCollectorTransactionDetail;
use App;

class DataCollectorService implements DataCollectorServiceInterface
{
    use DatabaseTransaction;

    public function deleteByReference($reference)
    {
        $that = $this;

        return $this->transaction(function() use ($reference, $that) {
            $model = DataCollectorTransaction::where('reference_no', $reference)->first();

            $model->details()->delete();

            $deleted = $model->delete();

            return $deleted;
        });
    }

    public function store(array $attributes)
    {
        $that = $this;

        return $this->transaction(function() use ($attributes, $that) {
            $attributes['info'] = $this->transformInfo($attributes['info']);

            // Make a new instance of the parent model
            $info = new DataCollectorTransaction($attributes['info']);

            $info->save();

            $collection = [];

            // Loop through each data, pull only the needed data and create
            // a new instance of the child model
            foreach($attributes['details'] as $data) {

                $data = $this->transformDetail($data);

                array_pull($data, 'id');

                $collection[] = new DataCollectorTransactionDetail($data);
            }

            $info->details()->saveMany($collection);

            return $info->fresh();
        });
    }

    private function transformInfo(array $data)
    {
        return [
            'branch_id'        => $data['branch_id'],
            'filesize'         => $data['filesize'],
            'client_id'        => $data['client_id'],
            'transaction_type' => $data['transaction_type'],
            'source'           => $data['source'],
            'row_count'        => $data['row_count'],
            'import_date'      => $data['import_date'],
            'imported_by'      => $data['imported_by']
        ];
    }

    private function transformDetail(array $data)
    {
        return [
            'product_id' => $data['product_id'],
            'qty'        => $data['qty'],
            'price'      => $data['price'],
            'unit_id'    => $data['unit_id'],
            'unit_qty'   => $data['unit_qty']
        ];
    }

    public function delete($id)
    {
        $that = $this;

        return $this->transaction(function() use ($id, $that) {
            $model = DataCollectorTransaction::find($id);

            $model->details()->delete();

            $deleted = $model->delete();

            return $deleted;
        });
    }
}