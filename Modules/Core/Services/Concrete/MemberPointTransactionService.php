<?php

namespace Modules\Core\Services\Concrete;

use Carbon\Carbon;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Member;
use Modules\Core\Enums\MemberPointType;
use Modules\Core\Services\Contracts\MemberPointTransactionServiceInterface;
use Modules\Core\Entities\MemberPointTransaction;
use Auth;

class MemberPointTransactionService extends BaseService implements MemberPointTransactionServiceInterface
{
    protected function model()
    {
        return MemberPointTransaction::class;
    }

    public function earn($memberId, $amount, $reference, $remarks)
    {
        $member = Member::with(['rate.details'])->find($memberId);

        $now = Carbon::now();

        $amount = Decimal::create($amount, setting('monetary.precision'));

        $range = $member->rate->details
            ->filter(function($item, $key) use ($amount, $now) {
                return $amount->isLessOrEqualTo($item->amount_to) &&
                    $amount->isGreaterOrEqualTo($item->amount_from) &&
                    $now->lessThanOrEqualTo($item->date_to) &&
                    $now->greaterThanOrEqualTo($item->date_from);
            })
            ->first();

        if (null === $range) {
            return null;
        }

        $points = $amount->div($range->amount_per_point, setting('monetary.precision'));

        $data = [
            'member_id'        => $memberId,
            'reference_type'   => MemberPointType::SALES_EARN_POINTS,
            'reference_number' => $reference,
            'amount'           => $points,
            'transaction_date' => $now,
            'created_by'       => Auth::user()->id,
            'remarks'          => $remarks
        ];

        return $this->store($data);
    }

    public function use($memberId, $amount, $reference, $remarks)
    {
        $data = [
            'member_id'        => $memberId,
            'reference_type'   => MemberPointType::SALES_USE_POINTS,
            'reference_number' => $reference,
            'amount'           => $amount,
            'transaction_date' => Carbon::now(),
            'created_by'       => Auth::user()->id,
            'remarks'          => $remarks
        ];

        return $this->store($data);
    }

    public function load($memberId, $amount, $reference, $remarks)
    {
        $data = [
            'member_id'        => $memberId,
            'reference_type'   => MemberPointType::LOAD_POINTS,
            'reference_number' => $reference,
            'amount'           => $amount,
            'transaction_date' => Carbon::now(),
            'created_by'       => Auth::user()->id,
            'remarks'          => $remarks
        ];

        return $this->store($data);
    }

    protected function transform($data)
    {
        $returned =  array_only($data, [
            'member_id',
            'reference_type',
            'reference_number',
            'amount',
            'transaction_date',
            'created_by',
            'remarks'
        ]);

        $returned['transaction_date'] = Carbon::parse($returned['transaction_date']);

        return $returned;
    }
}