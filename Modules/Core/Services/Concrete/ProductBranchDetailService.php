<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Contracts\ProductBranchDetailServiceInterface;

use Modules\Core\Entities\ProductBranchDetail;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\BranchDetail;

class ProductBranchDetailService implements ProductBranchDetailServiceInterface
{
    public function createInitialData(BranchDetail $detail)
    {
        $limit = 510;

        for ($i = 0;;$i++) {
            $products = Product::skip(($i * $limit))
                ->take($limit)
                ->get();

            if ($products->isEmpty()) {
                break;
            }

            $collection = [];

            foreach ($products as $product) {
                $collection[] = [
                    'product_id'       => $product->id,
                    'branch_detail_id' => $detail->id,
                    'min'              => 0,
                    'max'              => 0
                ];
            }

            ProductBranchDetail::insert($collection);
        }
    }

    public function deleteFromBranchDetail(BranchDetail $detail) 
    {
        ProductBranchDetail::where('branch_detail_id', $detail->id)->delete();
    }
}