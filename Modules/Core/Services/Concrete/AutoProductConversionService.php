<?php

namespace Modules\Core\Services\Concrete;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\BranchPrice;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Events\AutoProductConversionDeleted;
use Modules\Core\Repositories\Contracts\ProductBranchSummaryRepository;
use Modules\Core\Services\Contracts\AutoProductConversionServiceInterface;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Entities\AutoProductConversionDetail;
use Modules\Core\Enums\GroupType;
use Modules\Core\Events\AutoProductConversionCreated;
use App;

class AutoProductConversionService implements AutoProductConversionServiceInterface
{
    use DatabaseTransaction;

    public function processFromTransaction($model)
    {
        $that = $this;

        //return $this->transaction(function() use ($model, $that) {
            $details = $model->details;

            foreach ($details as $key => $detail) {
                if (!in_array($detail->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP])) {
                    continue;
                }

                $conversion = $detail->autoConversion()->create($that->transformInfo($detail));

                $details = $that->transformDetails($detail);

                $collection = $conversion->details()->saveMany($details);

                event(new AutoProductConversionCreated($conversion->fresh()));
            }
        //});
    }

    public function deleteByTransaction($model)
    {
        //return $this->transaction(function() use ($model) {
            foreach ($model->details as $detail) {
                $conversion = $detail->autoConversion;

                if (!$conversion) {
                    continue;
                }

                $conversion->delete();

                event(new AutoProductConversionDeleted($conversion));
            }
        //});
    }

    private function transformDetails($detail)
    {
        $data = [];

        $components = $detail->components;

        $productIds = $components->pluck('id')->toArray();

        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
            $summaries = App::make(ProductBranchSummaryRepository::class)->findByFilters(
                $productIds,
                $detail->transaction->created_for,
                $detail->transaction->for_location
            );
        }

        $costs = $this->getPurchasePriceByBranch($detail->transaction->created_for, $productIds);

        foreach ($detail->components as $key => $component) {
            $currentCost = setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC
                ? $summaries->firstWhere('product_id', $component->id)->current_cost
                : 0;

            $data[] = new AutoProductConversionDetail([
                'product_id'   => $component->id,
                'required_qty' => $component->pivot->qty,
                'ma_cost'      => $currentCost,
                'cost'         => $costs->get($component->id)
            ]);
        }

        return collect($data);
    }

    private function transformInfo($detail)
    {
        return [
            'created_from'     => $detail->transaction->created_from,
            'created_by'       => $detail->transaction->created_by,
            'created_for'      => $detail->transaction->created_for,
            'for_location'     => $detail->transaction->for_location,
            'transaction_date' => $detail->transaction->transaction_date,
            'product_id'       => $detail->product_id,
            'qty'              => $detail->qty,
            'group_type'       => $detail->group_type,
            'cost'             => $this->getPurchasePriceByBranch($detail->transaction->created_for, $detail->product_id)
        ];
    }

    private function getPurchasePriceByBranch($branch, $products)
    {
        $ids = array_wrap($products);

        $prices = BranchPrice::whereIn('product_id', $ids)
            ->where('branch_id', $branch)
            ->get();

        return is_array($products)
            ? $prices->pluck('purchase_price', 'product_id')
            : $prices->first()->purchase_price;
    }
}