<?php

namespace Modules\Core\Services\Concrete;

use Modules\Core\Services\Concrete\BaseService;
use Modules\Core\Services\Contracts\ExcelTemplateServiceInterface;
use Modules\Core\Entities\ExcelTemplate;

class ExcelTemplateService extends BaseService implements ExcelTemplateServiceInterface
{
    protected function model()
    {
        return ExcelTemplate::class;
    }
}