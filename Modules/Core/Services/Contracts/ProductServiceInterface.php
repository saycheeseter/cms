<?php

namespace Modules\Core\Services\Contracts;

interface ProductServiceInterface
{
    public function store(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
    public function updateTransactionsPurchasePrice($productId, $branchId, $to, $from, $cost);
}
