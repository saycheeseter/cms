<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Branch;

interface ProductBranchServiceInterface
{
    public function createInitialData(Branch $info);
    public function deleteFromBranch(Branch $branch);
}