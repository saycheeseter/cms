<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Payment;

interface PaymentServiceInterface extends FinancialFlowServiceInterface
{
    public function updateTransactionsRemainingAmount(Payment $payment);
    public function revertTransactionsRemainingAmount(Payment $payment);
}