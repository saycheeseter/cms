<?php

namespace Modules\Core\Services\Contracts;

interface LicenseApiServiceInterface 
{
    public function isValidKey($key);
    public function activate($model);
    public function verify($key);
}