<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\UserDefinedField;

interface UserDefinedFieldMigrationServiceInterface
{
    public function migrate(UserDefinedField $model);
    public function dropColumn(UserDefinedField $model);
}