<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Services\Contracts\ServiceInterface;
use Modules\Core\Entities\ReportBuilder;

interface PermissionSectionServiceInterface extends TransactionServiceInterface
{
    public function create();
    public function deleteFromReportBuilder();
}