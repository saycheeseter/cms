<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\ConversionMovingAverageCost;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;

interface TransactionMovingAverageCostServiceInterface
{
    public function updateTransactionDetails(TransactionMovingAverageCost $model);
    public function updateManualConversion(ConversionMovingAverageCost $model);
    public function updateAutoConversion(ConversionMovingAverageCost $model);
}