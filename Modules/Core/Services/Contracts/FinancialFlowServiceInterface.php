<?php

namespace Modules\Core\Services\Contracts;

interface FinancialFlowServiceInterface
{
    public function approval($transactions, $status);
    public function store(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
}