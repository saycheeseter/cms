<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Services\Contracts\ServiceInterface;
use Modules\Core\Entities\ReportBuilder;

interface PermissionServiceInterface extends TransactionServiceInterface
{
    public function storeFromReportBuilder(ReportBuilder $sourceModel, $moduleId);
    public function updateFromReportBuilder(ReportBuilder $sourceModel);
    public function deleteFromReportBuilder(ReportBuilder $sourceModel);
}