<?php

namespace Modules\Core\Services\Contracts;

interface UserServiceInterface
{
    public function changeCurrentUserPassword(string $password);
    public function store(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
    public function changeAdminPassword($id, string $password);
}