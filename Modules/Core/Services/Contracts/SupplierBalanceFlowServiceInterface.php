<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Payment;

interface SupplierBalanceFlowServiceInterface extends BalanceFlowServiceInterface
{
    public function fromBalance(Payment $model);
    public function fromExcess(Payment $model);
    public function deleteByPayment($id);
}