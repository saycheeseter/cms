<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\BatchTransaction;

interface BatchDeliveryServiceInterface
{
    public function decrementReferenceBatchRemainingQty(BatchTransaction $transaction);
    public function incrementReferenceBatchRemainingQty(BatchTransaction $transaction);
}