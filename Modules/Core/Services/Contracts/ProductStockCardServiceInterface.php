<?php

namespace Modules\Core\Services\Contracts;

interface ProductStockCardServiceInterface
{
    public function create($model);
    public function createFromProductConversion($model);
}