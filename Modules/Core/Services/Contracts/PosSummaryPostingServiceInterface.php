<?php

namespace Modules\Core\Services\Contracts;

interface PosSummaryPostingServiceInterface
{
    public function generateSummary($id, $date);
}