<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\TransactionDetailRemainingReference;
use Modules\Core\Entities\Contracts\TransactionRemainingReference;

interface RemainingQuantityServiceInterface
{
    public function deductRemaining(TransactionRemainingReference $model);
    public function addRemaining(TransactionRemainingReference $model);
    public function deductDetailRemaining(TransactionDetailRemainingReference $model);
    public function addDetailRemaining(TransactionDetailRemainingReference $model);
    public function updateTransactionStatus(TransactionRemainingReference $model);
}
