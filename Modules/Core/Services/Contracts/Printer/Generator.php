<?php

namespace Modules\Core\Services\Contracts\Printer;

use Modules\Core\Entities\PrintoutTemplate;

interface Generator
{
    public function data($data);
    public function format(PrintoutTemplate $data);
    public function generate($file);
}