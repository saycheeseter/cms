<?php

namespace Modules\Core\Services\Contracts\Printer;

interface TransactionPrinterServiceInterface
{
    public function transaction($object);
}
