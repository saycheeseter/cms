<?php

namespace Modules\Core\Services\Contracts\Printer;

interface Paper
{
    public function getWidth();
    public function getHeight();
}