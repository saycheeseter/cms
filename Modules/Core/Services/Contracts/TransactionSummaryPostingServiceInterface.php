<?php

namespace Modules\Core\Services\Contracts;

interface TransactionSummaryPostingServiceInterface
{
    public function insert($model);
    public function delete($model);
    public function process($date);
}