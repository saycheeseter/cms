<?php

namespace Modules\Core\Services\Contracts;

interface PaperServiceInterface
{
    public function set($size, $orientation);
    public function setSize($size);
    public function setOrientation($orientation);
    public function getDimension();
}