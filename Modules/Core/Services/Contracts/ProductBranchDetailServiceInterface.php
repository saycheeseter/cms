<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\BranchDetail;

interface ProductBranchDetailServiceInterface
{
    public function createInitialData(BranchDetail $info);
    public function deleteFromBranchDetail(BranchDetail $detail);
}