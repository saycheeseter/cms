<?php

namespace Modules\Core\Services\Contracts;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\Contracts\TransactionSummary;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\ProductConversion;

interface ProductTransactionSummaryServiceInterface
{
    public function summarizeTransaction(TransactionSummary $model);
    public function revertTransaction(TransactionSummary $model);
    public function summarizeProductConversion(ProductConversion $model);
    public function revertProductConversion(ProductConversion $model);
    public function summarizeAutoProductConversion(AutoProductConversion $model);
    public function revertAutoProductConversion(AutoProductConversion $model);
    public function createOrUpdate($attributes, $column, Decimal $value, $method = 'increment');
    public function summarizeInventoryAdjust(InventoryAdjust $model);
    public function revertInventoryAdjust(InventoryAdjust $model);
}