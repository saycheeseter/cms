<?php

namespace Modules\Core\Services\Contracts;

interface PrinterServiceInterface
{
    public function template($data, $format);
}