<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\Contracts\BranchModuleSeries as BranchModuleSeriesInterface;

interface BranchModuleSeriesServiceInterface
{
    public function execute(BranchModuleSeriesInterface $model);
    public function createInitialData(Branch $model);
    public function deleteFromBranch(Branch $branch);
}