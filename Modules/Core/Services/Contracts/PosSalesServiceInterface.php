<?php

namespace Modules\Core\Services\Contracts;

interface PosSalesServiceInterface
{
    public function sync(array $transactions);
}