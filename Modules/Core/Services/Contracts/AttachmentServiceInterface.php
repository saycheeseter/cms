<?php

namespace Modules\Core\Services\Contracts;

interface AttachmentServiceInterface
{
    public function temporary($files);
    public function attach($files, $module, $id);
    public function detach($key, $module, $id);
    public function update($files, $module, $id);
    public function removeTemporary($key);
}