<?php

namespace Modules\Core\Services\Contracts;

interface DataCollectorServiceInterface
{
    public function deleteByReference($referenceNumber);
    public function store(array $attributes);
    public function delete($id);
}