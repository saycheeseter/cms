<?php

namespace Modules\Core\Services\Contracts;

interface AutoProductConversionServiceInterface
{
    public function processFromTransaction($model);
    public function deleteByTransaction($model);
}