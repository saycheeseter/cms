<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Services\Contracts\TransactionServiceInterface;
use Modules\Core\Entities\ReportBuilder;

interface PermissionModuleServiceInterface extends TransactionServiceInterface
{
    public function storeFromReportBuilder(ReportBuilder $sourceModel, $sectionId);
    public function updateFromReportBuilder(ReportBuilder $sourceModel);
    public function deleteFromReportBuilder(ReportBuilder $sourceModel);
}