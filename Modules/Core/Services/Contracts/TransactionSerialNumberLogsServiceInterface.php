<?php

namespace Modules\Core\Services\Contracts;

interface TransactionSerialNumberLogsServiceInterface
{
    public function log($model);
    public function remove($model);
}