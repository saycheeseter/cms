<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Collection;
use Modules\Core\Entities\ReceivedCheck;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;

interface CustomerCreditServiceInterface
{
    public function addDueFromSalesOutbound(SalesOutbound $model);
    public function deductDueFromSalesOutbound(SalesOutbound $model);
    public function addDueFromCollection(Collection $model);
    public function deductDueFromCollection(Collection $model);
    public function deductDueFromReceivedCheck(ReceivedCheck $model);
    public function addDueFromSalesReturnInbound(SalesReturnInbound $model);
    public function deductDueFromSalesReturnInbound(SalesReturnInbound $model);
}