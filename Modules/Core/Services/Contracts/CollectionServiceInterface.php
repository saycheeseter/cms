<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Collection;

interface CollectionServiceInterface extends FinancialFlowServiceInterface
{
    public function updateTransactionsRemainingAmount(Collection $collection);
    public function revertTransactionsRemainingAmount(Collection $collection);
}