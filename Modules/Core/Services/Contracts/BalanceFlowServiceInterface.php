<?php

namespace Modules\Core\Services\Contracts;

interface BalanceFlowServiceInterface
{
    public function create($attributes);
    public function delete($id, $type);
}