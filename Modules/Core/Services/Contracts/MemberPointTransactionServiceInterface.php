<?php

namespace Modules\Core\Services\Contracts;

interface MemberPointTransactionServiceInterface extends ServiceInterface
{
    public function earn($memberId, $amount, $reference, $remarks);
    public function use($memberId, $amount, $reference, $remarks);
    public function load($memberId, $amount, $reference, $remarks);
}