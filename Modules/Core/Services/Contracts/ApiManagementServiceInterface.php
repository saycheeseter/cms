<?php

namespace Modules\Core\Services\Contracts;

interface ApiManagementServiceInterface
{
    public function setMethod(string $method = 'get');
    public function setRoute(string $route);
    public function setParameters($parameters);
    public function request();
    public function getResponse();
    public function getJsonDecodeResponse($assoc = true);
}