<?php

namespace Modules\Core\Services\Contracts\Import;

use Illuminate\Http\UploadedFile;

interface ImportExcelServiceInterface
{
    public function execute();
    public function setFile(UploadedFile $file);
    public function exportDefaultTemplate();
    public function getLogs();
}