<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\PaymentCheckManagement;
use Modules\Core\Entities\Contracts\OtherPaymentCheckManagement;
use Modules\Core\Entities\Contracts\PaymentDetailCheckManagement;

interface CheckManagementServiceInterface extends ServiceInterface
{
    public function createFromPayment(PaymentCheckManagement $model);
    public function createFromOtherPayment(OtherPaymentCheckManagement $model);
    public function deleteFromPaymentDetail(PaymentDetailCheckManagement $model);
    public function deleteFromPayment(PaymentCheckManagement $model);
    public function updateFromOtherPayment(OtherPaymentCheckManagement $model);
    public function deleteFromOtherPayment(OtherPaymentCheckManagement $model);
    public function transfer(Array $data);
}