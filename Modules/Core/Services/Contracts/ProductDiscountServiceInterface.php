<?php

namespace Modules\Core\Services\Contracts;

interface ProductDiscountServiceInterface
{
    public function store(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
}