<?php

namespace Modules\Core\Services\Contracts;

interface ProductPricingServiceInterface
{
    public function updateHistory($transaction);
    public function populate($id);
}
