<?php

namespace Modules\Core\Services\Contracts;

interface UserDefinedFieldStructureInterface
{
    public function getColumns($table);
    public function getColumnType($table, $column);
    public function getTables();
}