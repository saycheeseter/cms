<?php

namespace Modules\Core\Services\Contracts\ReportBuilder;

interface BaseReportBuilderServiceInterface
{
    public function getFilters();
}