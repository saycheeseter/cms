<?php

namespace Modules\Core\Services\Contracts\ReportBuilder\Modules;

use Modules\Core\Services\Contracts\ReportBuilder\BaseReportBuilderServiceInterface;

interface SalesOutboundReportBuilderServiceInterface extends BaseReportBuilderServiceInterface
{

}