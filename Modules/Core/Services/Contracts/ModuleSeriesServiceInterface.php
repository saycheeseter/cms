<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\ModuleSeries as ModuleSeriesInterface;

interface ModuleSeriesServiceInterface
{
    public function execute(ModuleSeriesInterface $model);
}