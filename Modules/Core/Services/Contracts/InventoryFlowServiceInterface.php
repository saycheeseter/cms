<?php

namespace Modules\Core\Services\Contracts;

interface InventoryFlowServiceInterface
{
    public function bulkStore($transactions);
    public function approval($transactions, $status);
    public function createOrUpdate($attributes, $id = 0);
    public function store(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
    public function updateApproved($attributes, $id);
}
