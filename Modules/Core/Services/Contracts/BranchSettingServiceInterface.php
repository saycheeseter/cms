<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Branch;

interface BranchSettingServiceInterface
{
    public function store(array $data);
    public function update(array $data);
    public function copy(Branch $branch);
    public function deleteFromBranch(Branch $branch);
}
