<?php

namespace Modules\Core\Services\Contracts\Export;

interface InventoryChainExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}
