<?php

namespace Modules\Core\Services\Contracts\Export;

interface CounterExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}
