<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}