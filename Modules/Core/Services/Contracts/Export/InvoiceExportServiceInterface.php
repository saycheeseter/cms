<?php

namespace Modules\Core\Services\Contracts\Export;

interface InvoiceExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}