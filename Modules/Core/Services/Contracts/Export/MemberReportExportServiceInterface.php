<?php

namespace Modules\Core\Services\Contracts\Export;

interface MemberReportExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}