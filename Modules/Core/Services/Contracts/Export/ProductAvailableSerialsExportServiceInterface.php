<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductAvailableSerialsExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}