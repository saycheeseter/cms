<?php

namespace Modules\Core\Services\Contracts\Export;

interface InventoryAdjustExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}
