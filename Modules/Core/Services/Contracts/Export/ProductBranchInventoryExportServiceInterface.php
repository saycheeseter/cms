<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductBranchInventoryExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}