<?php

namespace Modules\Core\Services\Contracts\Export;

interface DetailSalesReportExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}