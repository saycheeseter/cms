<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductTransactionListExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}