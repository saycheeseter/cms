<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductPeriodicSalesExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}