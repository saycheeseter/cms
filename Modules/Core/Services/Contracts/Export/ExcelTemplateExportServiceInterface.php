<?php

namespace Modules\Core\Services\Contracts\Export;

interface ExcelTemplateExportServiceInterface
{
    public function generate($collection = array());
}
