<?php

namespace Modules\Core\Services\Contracts\Export;

interface NegativeProfitProductExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}