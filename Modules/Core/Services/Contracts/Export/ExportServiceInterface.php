<?php

namespace Modules\Core\Services\Contracts\Export;

interface ExportServiceInterface
{
    public function headers($headers);
    public function settings($settings);
    public function format($format);
    public function data($data);
    public function udfs($data);
}