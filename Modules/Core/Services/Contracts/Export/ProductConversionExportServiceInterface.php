<?php

namespace Modules\Core\Services\Contracts\Export;

interface ProductConversionExportServiceInterface extends ExportServiceInterface
{
    public function list($collection);
}
