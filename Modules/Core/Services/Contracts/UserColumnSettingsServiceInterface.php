<?php

namespace Modules\Core\Services\Contracts;

interface UserColumnSettingsServiceInterface
{
    public function save($module, $settings);
}