<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\PurchaseInbound;

interface BranchPriceServiceInterface
{
    public function copy(Branch $branch);
    public function updatePurchasePriceFromTransaction(PurchaseInbound $transaction);
    public function deleteFromBranch(Branch $branch);
}
