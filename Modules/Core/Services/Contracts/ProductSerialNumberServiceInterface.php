<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\Contracts\SeriableTransaction;

interface ProductSerialNumberServiceInterface
{
    public function createOrUpdateFromTransaction(PurchaseInbound $transaction, $product, $serials);
    public function createOrUpdate($product, $serials);
    public function updateStatusFromTransaction(SeriableTransaction $transaction);
    public function updateStatusFromInventoryAdjust(InventoryAdjust $transaction);
    public function revertStatusFromTransaction(SeriableTransaction $transaction);
    public function revertStatusFromInventoryAdjust(InventoryAdjust $transaction);
}