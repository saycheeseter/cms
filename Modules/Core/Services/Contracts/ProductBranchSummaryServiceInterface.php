<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\BranchDetail;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\ProductStockCard;

interface ProductBranchSummaryServiceInterface
{
    public function createInitialData(BranchDetail $detail);
    public function computeInventory($model);
    public function computeTransactionPendingQuantity($model);
    public function revertTransactionPendingQuantity($model);
    public function computePendingQuantity($model, $qty, $method = 'increment');
    public function incrementPendingQuantity($model, $qty);
    public function decrementPendingQuantity($model, $qty);
    public function manualConversion(ProductConversion $model);
    public function manualGroupConversion(ProductConversion $model);
    public function manualUngroupConversion(ProductConversion $model);
    public function autoConversion(AutoProductConversion $model);
    public function updateCurrentCostFromStockCard(ProductStockCard $card);
    public function deleteFromBranchDetail(BranchDetail $detail);
    public function revertInventory($model);
    public function revertAutoConversion(AutoProductConversion $model);
    public function revertManualConversion(ProductConversion $model);
}
