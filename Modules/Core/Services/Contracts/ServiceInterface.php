<?php

namespace Modules\Core\Services\Contracts;

interface ServiceInterface 
{
    public function store(array $attributes);
    public function bulkStore(array $attributes);
    public function update(array $attributes, $id);
    public function delete($id);
}