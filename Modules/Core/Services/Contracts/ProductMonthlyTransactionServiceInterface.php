<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Contracts\GenerateAutoConversionProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateConversionProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateInventoryAdjustmentProductMonthlySummary;
use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;

interface ProductMonthlyTransactionServiceInterface
{
    public function generateFromTransaction(GenerateTransactionProductMonthlySummary $model);
    public function generateFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model);
    public function generateFromConversion(GenerateConversionProductMonthlySummary $model);
    public function generateFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model);
    public function revertFromTransaction(GenerateTransactionProductMonthlySummary $model);
    public function revertFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model);
    public function revertFromConversion(GenerateConversionProductMonthlySummary $model);
    public function revertFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model);
    public function generateSpecificFieldsFromTransaction(GenerateTransactionProductMonthlySummary $model, $columns);
    public function generateSpecificFieldsFromInventoryAdjust(GenerateInventoryAdjustmentProductMonthlySummary $model, $columns);
    public function generateSpecificFieldsFromConversion(GenerateConversionProductMonthlySummary $model, $columns);
    public function generateSpecificFieldsFromAutoConversion(GenerateAutoConversionProductMonthlySummary $model, $columns);
}