<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Collection;

interface CustomerBalanceFlowServiceInterface extends BalanceFlowServiceInterface
{
    public function fromBalance(Collection $model);
    public function fromExcess(Collection $model);
    public function deleteByCollection($id);
}