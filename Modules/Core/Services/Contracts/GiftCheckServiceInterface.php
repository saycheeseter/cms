<?php

namespace Modules\Core\Services\Contracts;

interface GiftCheckServiceInterface extends ServiceInterface
{
    public function redeem($id, $referenceNumber);
    public function bulkEntry($data);
}