<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Services\Contracts\ServiceInterface;

interface PurchaseServiceInterface extends InvoiceServiceInterface
{
    public function createFromStockRequestBySupplier(array $attributes);
}