<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\Contracts\BatchTransaction;

interface ProductBatchServiceInterface
{
    public function createOrUpdateFromTransaction(PurchaseInbound $transaction, $product, $batches);
    public function createOrUpdateBatchOwnerFromTransaction(BatchTransaction $transaction);
    public function setAvailableStatusFromPurchaseInbound(PurchaseInbound $transaction);
    public function setNotAvailableStatusFromPurchaseInbound(PurchaseInbound $transaction);
    public function revertBatchOwnerFromTransaction(BatchTransaction $transaction);
}