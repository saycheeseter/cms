<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Entities\Branch;

interface GenericSettingServiceInterface
{
    public function store(array $data);
    public function update(array $data);
}
