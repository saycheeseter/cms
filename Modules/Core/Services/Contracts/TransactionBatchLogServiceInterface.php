<?php

namespace Modules\Core\Services\Contracts;

interface TransactionBatchLogServiceInterface
{
    public function log($model);
    public function remove($model);
}