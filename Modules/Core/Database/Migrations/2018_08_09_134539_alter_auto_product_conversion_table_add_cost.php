<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAutoProductConversionTableAddCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('auto_product_conversion_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_product_conversion_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }
}
