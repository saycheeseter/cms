<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdInGenericAndBranchSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
        });

        Schema::table('branch_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $genericSettingsData = object_to_array(DB::table('generic_settings')
            ->select('key', 'value', 'type', 'comments')
            ->get()
            ->toArray()
        );

        $branchSettingsData = object_to_array(DB::table('branch_settings')
            ->select('key', 'value', 'type', 'comments', 'branch_id')
            ->get()
            ->toArray()
        );

        Schema::drop('branch_settings');
        Schema::drop('generic_settings');

        Schema::create('generic_settings', function (Blueprint $table) {
            $table->string('key', 100);
            $table->string('value', 255);
            $table->string('type', 45);
            $table->text('comments')->nullable();
        });

        Schema::create('branch_settings', function (Blueprint $table) {
            $table->string('key', 100);
            $table->string('value', 255);
            $table->string('type', 45);
            $table->unsignedBigInteger('branch_id');
            $table->text('comments')->nullable();
        });

        Schema::table('branch_settings', function (Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('branch');
        });

        DB::table('generic_settings')->insert($genericSettingsData);
        DB::table('branch_settings')->insert($branchSettingsData);
    }
}
