<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimitColumnInModuleAndBranchModuleSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_series', function (Blueprint $table) {
            $table->bigInteger('limit')->nullable();
        });

        Schema::table('branch_module_series', function (Blueprint $table) {
            $table->bigInteger('limit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module_series', function (Blueprint $table) {
            $table->dropColumn(['limit']);
        });

        Schema::table('branch_module_series', function (Blueprint $table) {
            $table->dropColumn(['limit']);
        });
    }
}
