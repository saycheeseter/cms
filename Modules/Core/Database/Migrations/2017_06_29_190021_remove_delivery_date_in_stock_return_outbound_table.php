<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDeliveryDateInStockReturnOutboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_return_outbound', function (Blueprint $table) {
            $table->dropColumn(['delivery_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_return_outbound', function (Blueprint $table) {
            $table->date('delivery_date')->after('deliver_to_location');
        });

        Schema::enableForeignKeyConstraints();
    }
}
