<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReportBuilderTableChangeSlugAndNameLength extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_builder', function (Blueprint $table) {
            $table->string('name', 128)->change();
            $table->string('slug', 128)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_builder', function (Blueprint $table) {
            $table->string('name', 45)->change();
            $table->string('slug', 45)->change();
        });
    }
}
