<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataCollectorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_collector_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->string('reference_no', '100')->unique();
            $table->integer('filesize')->nullable();
            $table->integer('client_id');
            $table->smallInteger('transaction_type');
            $table->smallInteger('source');
            $table->integer('row_count');
            $table->date('import_date');
            $table->unsignedBigInteger('imported_by');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('data_collector_transaction_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('qty', 16, 6);
            $table->decimal('price', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('data_collector_transaction', function (Blueprint $table) {
            $table->foreign('imported_by')->references('id')->on('user');
            $table->foreign('branch_id')->references('id')->on('branch');
        });

        Schema::table('data_collector_transaction_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('data_collector_transaction');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_collector_transaction_detail');
        Schema::dropIfExists('data_collector_transaction');
    }
}
