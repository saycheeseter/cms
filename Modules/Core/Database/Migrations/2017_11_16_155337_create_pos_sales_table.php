<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cashier');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('customer_detail_id')->nullable();
            $table->unsignedBigInteger('salesman_id')->nullable();
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('for_location');
            $table->text('remarks')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('customer_type');
            $table->string('customer_walk_in_name', 100)->nullable();
            $table->decimal('total_amount', 23, 6);
            $table->string('transaction_number', 100);
            $table->string('or_number', 100);
            $table->unsignedBigInteger('member_id')->nullable();
            $table->integer('terminal_number');
            $table->string('senior_number', 100)->nullable();
            $table->string('senior_name', 100)->nullable();
            $table->smallInteger('is_non_vat');
            $table->smallInteger('is_wholesale');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('pos_sales_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->decimal('vat', 23, 6);
            $table->smallInteger('is_senior');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('pos_sales', function (Blueprint $table) {
            $table->foreign('cashier')->references('id')->on('user');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->foreign('customer_detail_id')->references('id')->on('customer_detail');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('member_id')->references('id')->on('member');
        });

        Schema::table('pos_sales_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('pos_sales');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_sales_detail');
        Schema::dropIfExists('pos_sales');
    }
}
