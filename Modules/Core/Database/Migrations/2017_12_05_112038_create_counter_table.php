<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('customer_id');
            $table->dateTime('transaction_date');
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->decimal('total_amount', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('counter_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('reference_id');
            $table->decimal('remaining_amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('counter', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('customer_id')->references('id')->on('customer');
        });

        Schema::table('counter_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('counter');
            $table->foreign('reference_id')->references('id')->on('sales_outbound');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counter_detail');
        Schema::dropIfExists('counter');
    }
}
