<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRequestedByInFinancialFlowTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collection', function (Blueprint $table) {
            $table->dropForeign('collection_requested_by_foreign');
            $table->dropColumn('requested_by');
        });

        Schema::table('payment', function (Blueprint $table) {
            $table->dropForeign('payment_requested_by_foreign');
            $table->dropColumn('requested_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection', function (Blueprint $table) {
            $table->unsignedBigInteger('requested_by')->nullable();
            $table->foreign('requested_by')->references('id')->on('user');
        });

        Schema::table('payment', function (Blueprint $table) {
            $table->unsignedBigInteger('requested_by')->nullable();
            $table->foreign('requested_by')->references('id')->on('user');
        });
    }
}
