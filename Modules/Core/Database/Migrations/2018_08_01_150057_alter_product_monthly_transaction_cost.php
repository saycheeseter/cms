<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductMonthlyTransactionCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->renameColumn('purchase_inbound_total_cost', 'purchase_inbound_total_ma_cost');
            $table->renameColumn('stock_delivery_inbound_total_cost', 'stock_delivery_inbound_total_ma_cost');
            $table->renameColumn('stock_return_inbound_total_cost', 'stock_return_inbound_total_ma_cost');
            $table->renameColumn('sales_return_inbound_total_cost', 'sales_return_inbound_total_ma_cost');
            $table->renameColumn('adjustment_increase_total_cost', 'adjustment_increase_total_ma_cost');
            $table->renameColumn('purchase_return_outbound_total_cost', 'purchase_return_outbound_total_ma_cost');
            $table->renameColumn('stock_delivery_outbound_total_cost', 'stock_delivery_outbound_total_ma_cost');
            $table->renameColumn('stock_return_outbound_total_cost', 'stock_return_outbound_total_ma_cost');
            $table->renameColumn('sales_outbound_total_cost', 'sales_outbound_total_ma_cost');
            $table->renameColumn('damage_outbound_total_cost', 'damage_outbound_total_ma_cost');
            $table->renameColumn('adjustment_decrease_total_cost', 'adjustment_decrease_total_ma_cost');
            $table->renameColumn('product_conversion_increase_total_cost', 'product_conversion_increase_total_ma_cost');
            $table->renameColumn('product_conversion_decrease_total_cost', 'product_conversion_decrease_total_ma_cost');
            $table->renameColumn('auto_product_conversion_increase_total_cost', 'auto_product_conversion_increase_total_ma_cost');
            $table->renameColumn('auto_product_conversion_decrease_total_cost', 'auto_product_conversion_decrease_total_ma_cost');
        });

        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->decimal('purchase_inbound_total_cost', 23, 6)->default(0);
            $table->decimal('stock_delivery_inbound_total_cost', 23, 6)->default(0);
            $table->decimal('stock_return_inbound_total_cost', 23, 6)->default(0);
            $table->decimal('sales_return_inbound_total_cost', 23, 6)->default(0);
            $table->decimal('adjustment_increase_total_cost', 23, 6)->default(0);
            $table->decimal('purchase_return_outbound_total_cost', 23, 6)->default(0);
            $table->decimal('stock_delivery_outbound_total_cost', 23, 6)->default(0);
            $table->decimal('stock_return_outbound_total_cost', 23, 6)->default(0);
            $table->decimal('sales_outbound_total_cost', 23, 6)->default(0);
            $table->decimal('damage_outbound_total_cost', 23, 6)->default(0);
            $table->decimal('adjustment_decrease_total_cost', 23, 6)->default(0);
            $table->decimal('product_conversion_increase_total_cost', 23, 6)->default(0);
            $table->decimal('product_conversion_decrease_total_cost', 23, 6)->default(0);
            $table->decimal('auto_product_conversion_increase_total_cost', 23, 6)->default(0);
            $table->decimal('auto_product_conversion_decrease_total_cost', 23, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->dropColumn('purchase_inbound_total_cost');
            $table->dropColumn('stock_delivery_inbound_total_cost');
            $table->dropColumn('stock_return_inbound_total_cost');
            $table->dropColumn('sales_return_inbound_total_cost');
            $table->dropColumn('adjustment_increase_total_cost');
            $table->dropColumn('purchase_return_outbound_total_cost');
            $table->dropColumn('stock_delivery_outbound_total_cost');
            $table->dropColumn('stock_return_outbound_total_cost');
            $table->dropColumn('sales_outbound_total_cost');
            $table->dropColumn('damage_outbound_total_cost');
            $table->dropColumn('adjustment_decrease_total_cost');
            $table->dropColumn('product_conversion_increase_total_cost');
            $table->dropColumn('product_conversion_decrease_total_cost');
            $table->dropColumn('auto_product_conversion_increase_total_cost');
            $table->dropColumn('auto_product_conversion_decrease_total_cost');
        });

        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->renameColumn('purchase_inbound_total_ma_cost', 'purchase_inbound_total_cost');
            $table->renameColumn('stock_delivery_inbound_total_ma_cost', 'stock_delivery_inbound_total_cost');
            $table->renameColumn('stock_return_inbound_total_ma_cost', 'stock_return_inbound_total_cost');
            $table->renameColumn('sales_return_inbound_total_ma_cost', 'sales_return_inbound_total_cost');
            $table->renameColumn('adjustment_increase_total_ma_cost', 'adjustment_increase_total_cost');
            $table->renameColumn('purchase_return_outbound_total_ma_cost', 'purchase_return_outbound_total_cost');
            $table->renameColumn('stock_delivery_outbound_total_ma_cost', 'stock_delivery_outbound_total_cost');
            $table->renameColumn('stock_return_outbound_total_ma_cost', 'stock_return_outbound_total_cost');
            $table->renameColumn('sales_outbound_total_ma_cost', 'sales_outbound_total_cost');
            $table->renameColumn('damage_outbound_total_ma_cost', 'damage_outbound_total_cost');
            $table->renameColumn('adjustment_decrease_total_ma_cost', 'adjustment_decrease_total_cost');
            $table->renameColumn('product_conversion_increase_total_ma_cost', 'product_conversion_increase_total_cost');
            $table->renameColumn('product_conversion_decrease_total_ma_cost', 'product_conversion_decrease_total_cost');
            $table->renameColumn('auto_product_conversion_increase_total_ma_cost', 'auto_product_conversion_increase_total_cost');
            $table->renameColumn('auto_product_conversion_decrease_total_ma_cost', 'auto_product_conversion_decrease_total_cost');  
        });
    }
}
