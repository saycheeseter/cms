<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemainingAmountInPurchaseInboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->decimal('remaining_amount', 23, 6)->after('total_amount')->default('0.000000');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->dropColumn(['remaining_amount']);
        });
    }
}
