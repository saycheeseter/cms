<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosCollectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_collection', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('payment_method');
            $table->decimal('amount', 23, 6);
            $table->unsignedBigInteger('bank_account_id')->nullable();
            $table->text('check_no')->nullable();
            $table->date('check_date')->nullable();
            $table->date('gift_check_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('pos_collection', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('pos_sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_collection');
    }
}
