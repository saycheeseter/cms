<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('code', 45);
            $table->string('name', 45);
            $table->string('address', 512)->nullable();
            $table->string('contact', 512)->nullable();
            $table->string('business_name', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('branch_detail', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('branch_id')->references('id')->on('branch');
        });

        Schema::table('branch', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_detail');

        Schema::table('branch', function (Blueprint $table) {
            $table->dropForeign(['created_by']);
            $table->dropForeign(['modified_by']);
        });
    }
}
