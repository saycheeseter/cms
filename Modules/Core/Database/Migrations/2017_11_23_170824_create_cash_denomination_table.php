<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashDenominationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_denomination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('terminal_number');
            $table->dateTime('create_date');
            $table->integer('b1000')->default(0);
            $table->integer('b500')->default(0);
            $table->integer('b200')->default(0);
            $table->integer('b100')->default(0);
            $table->integer('b50')->default(0);
            $table->integer('b20')->default(0);
            $table->integer('c10')->default(0);
            $table->integer('c5')->default(0);
            $table->integer('c1')->default(0);
            $table->integer('c25c')->default(0);
            $table->integer('c10c')->default(0);
            $table->integer('c5c')->default(0);
            $table->smallInteger('type');
            $table->timestamps();
        });

        Schema::table('cash_denomination', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('terminal_number')->references('id')->on('terminal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_denomination');
    }
}
