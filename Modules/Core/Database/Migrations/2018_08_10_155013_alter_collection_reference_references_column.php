<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCollectionReferenceReferencesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collection_reference', function(Blueprint $table) {
            $table->dropForeign('collection_reference_sales_outbound_id_foreign');
            $table->renameColumn('sales_outbound_id', 'reference_id');
        });

        Schema::table('collection_reference', function(Blueprint $table) {
            $table->string('reference_type')->nullable()->after('reference_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection_reference', function(Blueprint $table) {
            $table->dropColumn(['reference_type']);
            $table->renameColumn('reference_id', 'sales_outbound_id');
        });

        Schema::table('collection_reference', function(Blueprint $table) {
            $table->foreign('sales_outbound_id')->references('id')->on('sales_outbound');
        });
    }
}
