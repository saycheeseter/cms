<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentReferenceReferencesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_reference', function(Blueprint $table) {
            $table->dropForeign('payment_reference_purchase_inbound_id_foreign');
            $table->renameColumn('purchase_inbound_id', 'reference_id');
        });

        Schema::table('payment_reference', function(Blueprint $table) {
            $table->string('reference_type')->nullable()->after('reference_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_reference', function(Blueprint $table) {
            $table->dropColumn(['reference_type']);
            $table->renameColumn('reference_id', 'purchase_inbound_id');
        });

        Schema::table('payment_reference', function(Blueprint $table) {
            $table->foreign('purchase_inbound_id')->references('id')->on('purchase_inbound');
        });
    }
}
