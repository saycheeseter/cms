<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('customer_detail_id')->nullable();
            $table->unsignedBigInteger('salesman_id');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('requested_by');
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->dateTime('transaction_date');
            $table->dateTime('audited_date')->nullable();
            $table->integer('term');
            $table->smallInteger('approval_status');
            $table->smallInteger('transaction_status');
            $table->smallInteger('customer_type');
            $table->string('customer_walk_in_name', 100)->nullable();
            $table->decimal('total_amount', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sales_return_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->decimal('remaining_qty', 16, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('sales_return', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->foreign('customer_detail_id')->references('id')->on('customer_detail');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('requested_by')->references('id')->on('user');
        });

        Schema::table('sales_return_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('sales_return');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_return_detail');
        Schema::dropIfExists('sales_return');
    }
}
