<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchDetailIdOnProductBranchInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_detail_id')->after('branch_id');
        });

        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->foreign('branch_detail_id')->references('id')->on('branch_detail');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->dropForeign(['branch_detail_id']);
        });

        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->dropColumn('branch_detail_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
