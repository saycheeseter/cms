<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenceIdInStockDeliveryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropColumn('stock_request_detail_id');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('reference_id')->nullable()->after('unit_id');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->foreign('reference_id')->references('id')->on('stock_request_detail');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropForeign(['reference_id']);
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropColumn('reference_id');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('stock_request_detail_id')->nullable()->after('unit_id');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->foreign('stock_request_detail_id')->references('id')->on('stock_request_detail');
        });

        Schema::enableForeignKeyConstraints();
    }
}
