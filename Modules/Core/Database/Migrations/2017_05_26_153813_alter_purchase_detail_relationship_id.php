<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseDetailRelationshipId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('purchase_detail', function(Blueprint $table) {
            $table->renameColumn('purchase_id', 'transaction_id');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('purchase_detail', function(Blueprint $table) {
            $table->renameColumn('transaction_id', 'purchase_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
