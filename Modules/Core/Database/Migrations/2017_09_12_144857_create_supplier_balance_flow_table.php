<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierBalanceFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_balance_flow', function (Blueprint $table) {
            $table->unsignedBigInteger('transactionable_id');
            $table->string('transactionable_type', 45);
            $table->unsignedBigInteger('supplier_id');
            $table->smallInteger('flow_type');
            $table->dateTime('transaction_date');
            $table->decimal('amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('supplier_balance_flow', function (Blueprint $table) {
            $table->foreign('supplier_id')->references('id')->on('supplier');
            $table->primary(['transactionable_id', 'transactionable_type', 'flow_type'], 'sbf_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_balance_flow');
    }
}
