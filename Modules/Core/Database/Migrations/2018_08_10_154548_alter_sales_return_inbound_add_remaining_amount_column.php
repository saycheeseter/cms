<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSalesReturnInboundAddRemainingAmountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_inbound', function(Blueprint $table) {
            $table->decimal('remaining_amount', 23, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_inbound', function(Blueprint $table) {
            $table->dropColumn(['remaining_amount']);
        });
    }
}
