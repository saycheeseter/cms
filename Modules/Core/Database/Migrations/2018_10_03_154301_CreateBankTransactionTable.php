<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bank_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->dateTime('transaction_date');
            $table->smallInteger('type');
            $table->decimal('amount', 23, 6);
            $table->unsignedBigInteger('from_bank')->nullable();
            $table->unsignedBigInteger('to_bank')->nullable();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('bank_transaction', function (Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('from_bank')->references('id')->on('bank_account');
            $table->foreign('to_bank')->references('id')->on('bank_account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('bank_transaction');
    }
}
