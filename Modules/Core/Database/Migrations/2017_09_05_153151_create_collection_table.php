<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('requested_by');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->smallInteger('customer_type');
            $table->string('customer_walk_in_name', 100)->nullable();
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('approval_status');
            $table->decimal('total_amount', 23, 6);
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('collection_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('payment_method');
            $table->decimal('amount', 23, 6);
            $table->unsignedBigInteger('bank_account_id')->nullable();
            $table->text('check_no')->nullable();
            $table->date('check_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('collection_reference', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('sales_outbound_id');
            $table->decimal('collectible', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('collection', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('customer_id')->references('id')->on('customer');
        });

        Schema::table('collection_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('collection');
            $table->foreign('payment_method')->references('id')->on('system_code');
            $table->foreign('bank_account_id')->references('id')->on('bank_account');
        });

        Schema::table('collection_reference', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('collection');
            $table->foreign('sales_outbound_id')->references('id')->on('sales_outbound');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_reference');
        Schema::dropIfExists('collection_detail');
        Schema::dropIfExists('collection');
    }
}
