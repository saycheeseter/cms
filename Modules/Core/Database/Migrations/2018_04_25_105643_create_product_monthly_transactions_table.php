<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMonthlyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_monthly_transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('for_location');
            $table->integer('year');
            $table->integer('month');
            $table->decimal('purchase_inbound_total_qty', 16, 6);
            $table->decimal('purchase_inbound_total_amount', 23, 6);
            $table->decimal('purchase_inbound_total_cost', 23, 6);
            $table->decimal('stock_delivery_inbound_total_qty', 16, 6);
            $table->decimal('stock_delivery_inbound_total_amount', 23, 6);
            $table->decimal('stock_delivery_inbound_total_cost', 23, 6);
            $table->decimal('stock_return_inbound_total_qty', 16, 6);
            $table->decimal('stock_return_inbound_total_amount', 23, 6);
            $table->decimal('stock_return_inbound_total_cost', 23, 6);
            $table->decimal('sales_return_inbound_total_qty', 16, 6);
            $table->decimal('sales_return_inbound_total_amount', 23, 6);
            $table->decimal('sales_return_inbound_total_cost', 23, 6);
            $table->decimal('adjustment_increase_total_qty', 16, 6);
            $table->decimal('adjustment_increase_total_amount', 23, 6);
            $table->decimal('adjustment_increase_total_cost', 23, 6);
            $table->decimal('purchase_return_outbound_total_qty', 16, 6);
            $table->decimal('purchase_return_outbound_total_amount', 23, 6);
            $table->decimal('purchase_return_outbound_total_cost', 23, 6);
            $table->decimal('stock_delivery_outbound_total_qty', 16, 6);
            $table->decimal('stock_delivery_outbound_total_amount', 23, 6);
            $table->decimal('stock_delivery_outbound_total_cost', 23, 6);
            $table->decimal('stock_return_outbound_total_qty', 16, 6);
            $table->decimal('stock_return_outbound_total_amount', 23, 6);
            $table->decimal('stock_return_outbound_total_cost', 23, 6);
            $table->decimal('sales_outbound_total_qty', 16, 6);
            $table->decimal('sales_outbound_total_amount', 23, 6);
            $table->decimal('sales_outbound_total_cost', 23, 6);
            $table->decimal('damage_outbound_total_qty', 16, 6);
            $table->decimal('damage_outbound_total_amount', 23, 6);
            $table->decimal('damage_outbound_total_cost', 23, 6);
            $table->decimal('adjustment_decrease_total_qty', 16, 6);
            $table->decimal('adjustment_decrease_total_amount', 23, 6);
            $table->decimal('adjustment_decrease_total_cost', 23, 6);
            $table->decimal('product_conversion_increase_total_qty', 16, 6);
            $table->decimal('product_conversion_increase_total_cost', 23, 6);
            $table->decimal('product_conversion_decrease_total_qty', 16, 6);
            $table->decimal('product_conversion_decrease_total_cost', 23, 6);
            $table->decimal('auto_product_conversion_increase_total_qty', 16, 6);
            $table->decimal('auto_product_conversion_increase_total_cost', 23, 6);
            $table->decimal('auto_product_conversion_decrease_total_qty', 16, 6);
            $table->decimal('auto_product_conversion_decrease_total_cost', 23, 6);
            $table->timestamps();
        });

        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_monthly_transactions');
    }
}
