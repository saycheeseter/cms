<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionableColumnsInPurchaseInboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->unsignedBigInteger('transactionable_id')->nullable()->after('udfs');
            $table->string('transactionable_type')->nullable()->after('transactionable_id');
        });

        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('transactionable_id')->nullable()->after('transaction_id');
            $table->string('transactionable_type')->nullable()->after('transactionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->dropColumn(['transactionable_id', 'transactionable_type']);
        });

        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->dropColumn(['transactionable_id', 'transactionable_type']);
        });
    }
}
