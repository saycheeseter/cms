<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_check', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('payment_detail_id')->nullable();
            $table->unsignedBigInteger('bank_account_id');
            $table->text('check_no');
            $table->date('check_date');
            $table->decimal('amount', 23, 6);
            $table->text('remarks')->nullable();
            $table->date('transfer_date')->nullable();
            $table->date('issued_date');
            $table->smallInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('issued_check', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('payment_detail_id')->references('id')->on('payment_detail');
            $table->foreign('bank_account_id')->references('id')->on('bank_account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_check');
    }
}
