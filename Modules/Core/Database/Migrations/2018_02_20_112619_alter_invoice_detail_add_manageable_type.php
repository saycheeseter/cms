<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvoiceDetailAddManageableType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('purchase_return_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('damage_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('sales_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('sales_return_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_request_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_return_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('pos_sales_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('purchase_return_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('damage_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('sales_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('sales_return_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_request_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_return_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('pos_sales_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });
    }
}
