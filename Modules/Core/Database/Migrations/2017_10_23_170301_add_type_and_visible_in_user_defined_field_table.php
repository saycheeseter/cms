<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndVisibleInUserDefinedFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_defined_fields', function (Blueprint $table) {
            $table->smallInteger('type');
            $table->tinyInteger('visible');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_defined_fields', function (Blueprint $table) {
            $table->dropColumns(['type', 'visible']);
        });
    }
}
