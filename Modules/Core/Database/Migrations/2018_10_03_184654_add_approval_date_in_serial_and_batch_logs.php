<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovalDateInSerialAndBatchLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_serial_number_logs', function (Blueprint $table) {
            $table->dateTime('approval_date')->nullable();
        });

        Schema::table('transaction_batch_logs', function (Blueprint $table) {
            $table->dateTime('approval_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_serial_number_logs', function (Blueprint $table) {
            $table->dropColumn(['approval_date']);
        });

        Schema::table('transaction_batch_logs', function (Blueprint $table) {
            $table->dropColumn(['approval_date']);
        });
    }
}
