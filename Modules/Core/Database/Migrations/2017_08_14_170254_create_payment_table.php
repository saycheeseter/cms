<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('requested_by');
            $table->unsignedBigInteger('supplier_id');
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('approval_status');
            $table->decimal('total_amount', 23, 6);
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('payment_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('payment_method');
            $table->decimal('amount', 23, 6);
            $table->unsignedBigInteger('bank_account_id')->nullable();
            $table->text('check_no')->nullable();
            $table->date('check_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('payment_reference', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('purchase_inbound_id');
            $table->decimal('payable', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('payment', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('supplier_id')->references('id')->on('supplier');
        });

        Schema::table('payment_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('payment');
            $table->foreign('payment_method')->references('id')->on('system_code');
            $table->foreign('bank_account_id')->references('id')->on('bank_account');
        });

        Schema::table('payment_reference', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('payment');
            $table->foreign('purchase_inbound_id')->references('id')->on('purchase_inbound');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_reference');
        Schema::dropIfExists('payment_detail');
        Schema::dropIfExists('payment');
    }
}
