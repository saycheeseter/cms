<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('product_customer', function(Blueprint $table) {
            $table->dropForeign(['branch_id']);
        });

        Schema::table('product_customer', function (Blueprint $table) {
            $table->dropColumn(['branch_id', 'min_qty', 'order_lead_time', 'default']);
        });

        Schema::table('product_customer', function (Blueprint $table) {
            $table->dateTime('latest_fields_updated_date')->after('latest_price');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('product_customer', function (Blueprint $table) {
           $table->unsignedBigInteger('branch_id');
           $table->smallInteger('order_lead_time');
           $table->smallInteger('default');
           $table->decimal('min_qty', 16, 6);
        });

        Schema::table('product_customer', function (Blueprint $table) {
           $table->foreign('branch_id')->references('id')->on('branch');
        });

        Schema::table('product_customer', function (Blueprint $table) {
            $table->dropColumn(['latest_fields_updated_date']);
        });

        Schema::enableForeignKeyConstraints();
    }
}
