<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductDiscountDetailAndTargetAddId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_discount_detail', function (Blueprint $table) {
            $table->bigIncrements('id')->first();
        });

        Schema::table('product_discount_target', function (Blueprint $table) {
            $table->bigIncrements('id')->first();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_discount_target', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        Schema::table('product_discount_detail', function (Blueprint $table) {
            $table->dropColumn('id');
        });
    }
}
