<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForLocationColumnInStockRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_request', function (Blueprint $table) {
            $table->unsignedBigInteger('for_location')->after('created_for');
        });

        Schema::table('stock_request', function (Blueprint $table) {
            $table->foreign('for_location')->references('id')->on('branch_detail');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_request', function (Blueprint $table) {
            $table->dropForeign(['for_location']);
        });

        Schema::table('stock_request', function (Blueprint $table) {
            $table->dropColumn('for_location');
        });

        Schema::enableForeignKeyConstraints();
    }
}
