<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPosSalesColumnsInPmt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->decimal('pos_sales_total_qty', 23, 6)->default(0);
            $table->decimal('pos_sales_total_amount', 23, 6)->default(0);
            $table->decimal('pos_sales_total_ma_cost', 23, 6)->default(0);
            $table->decimal('pos_sales_total_cost', 23, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->dropColumn(['pos_sales_total_qty']);
            $table->dropColumn(['pos_sales_total_amount']);
            $table->dropColumn(['pos_sales_total_ma_cost']);
            $table->dropColumn(['pos_sales_total_cost']);
        });
    }
}
