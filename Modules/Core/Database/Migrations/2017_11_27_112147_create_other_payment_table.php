<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_name', 100);
            $table->datetime('transaction_date');
            $table->unsignedBigInteger('payment_method');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('bank_account_id')->nullable();
            $table->datetime('check_date')->nullable();
            $table->string('check_number', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('other_payment', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('payment_method')->references('id')->on('system_code');
            $table->foreign('bank_account_id')->references('id')->on('bank_account');
        });

         Schema::create('other_payment_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('type');
            $table->decimal('amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('other_payment_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('other_payment');
            $table->foreign('type')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_payment_detail');
        Schema::dropIfExists('other_payment');
    }
}
