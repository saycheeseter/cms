<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRolePermissionForBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('permission_role');

        Schema::create('branch_permission_role', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_role_id');
            $table->unsignedBigInteger('permission_id');
        });

        Schema::table('branch_permission_role', function (Blueprint $table) {
            $table->primary(['branch_role_id', 'permission_id']);
            $table->foreign('branch_role_id')->references('id')->on('branch_role');
            $table->foreign('permission_id')->references('id')->on('permission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('permission_role', function (Blueprint $table) {
            $table->unsignedBigInteger('permission_id');
            $table->unsignedBigInteger('role_id');
        });

        Schema::table('permission_role', function (Blueprint $table) {
            $table->primary(['permission_id', 'role_id']);
            $table->foreign('permission_id')->references('id')->on('permission');
            $table->foreign('role_id')->references('id')->on('role');
        });

        Schema::dropIfExists('branch_permission_role');
    }
}
