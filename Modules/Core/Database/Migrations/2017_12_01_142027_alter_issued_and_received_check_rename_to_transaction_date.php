<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIssuedAndReceivedCheckRenameToTransactionDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issued_check', function (Blueprint $table) {
            $table->renameColumn('issued_date', 'transaction_date');
        });

        Schema::table('received_check', function (Blueprint $table) {
            $table->renameColumn('received_date', 'transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issued_check', function (Blueprint $table) {
            $table->renameColumn('transaction_date', 'issued_date');
        });

        Schema::table('received_check', function (Blueprint $table) {
            $table->renameColumn('transaction_date', 'received_date');
        });
    }
}
