<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discount', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 45);
            $table->string('name', 45);
            $table->dateTime('valid_from');
            $table->dateTime('valid_date');
            $table->smallInteger('status');
            $table->smallInteger('mon');
            $table->smallInteger('tue');
            $table->smallInteger('wed');
            $table->smallInteger('thur');
            $table->smallInteger('fri');
            $table->smallInteger('sat');
            $table->smallInteger('sun');
            $table->smallInteger('target_type');
            $table->smallInteger('discount_scheme');
            $table->smallInteger('select_type');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('product_discount_target', function (Blueprint $table) {
            $table->unsignedBigInteger('product_discount_id');
            $table->unsignedBigInteger('target_id');
            $table->smallInteger('target_type');
        });

        Schema::create('product_discount_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('product_discount_id');
            $table->smallInteger('entity_type');
            $table->unsignedBigInteger('entity_id')->nullable();
            $table->smallInteger('discount_type');
            $table->decimal('discount_value', 16, 6);
            $table->decimal('qty', 16, 6);
        });

        Schema::table('product_discount', function (Blueprint $table) {
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });

        Schema::table('product_discount_target', function (Blueprint $table) {
            $table->foreign('product_discount_id')->references('id')->on('product_discount');
        });

        Schema::table('product_discount_detail', function (Blueprint $table) {
            $table->foreign('product_discount_id')->references('id')->on('product_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('product_discount_detail');
        Schema::dropIfExists('product_discount_target');
        Schema::dropIfExists('product_discount');
    }
}
