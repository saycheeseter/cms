<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseTableOrderForColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('purchase', function(Blueprint $table) {
            $table->renameColumn('order_for', 'created_for');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('purchase', function(Blueprint $table) {
            $table->renameColumn('created_for', 'order_for');
        });

        Schema::enableForeignKeyConstraints();
    }
}
