<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductDiscountForPolymorphicRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_discount_detail', function (Blueprint $table) {
            $table->string('entity_type')->change();
        });

        Schema::table('product_discount_target', function (Blueprint $table) {
            $table->string('target_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_discount_target', function (Blueprint $table) {
            $table->smallInteger('target_type')->change();
        });

        Schema::table('product_discount_detail', function (Blueprint $table) {
            $table->smallInteger('entity_type')->change();
        });
    }
}
