<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosDetailComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_detail_components', function (Blueprint $table) {
            $table->unsignedBigInteger('pos_sales_detail_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('qty', 16, 6);
            $table->smallInteger('group_type');
        });

        Schema::table('pos_detail_components', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('pos_sales_detail_id')->references('id')->on('pos_sales_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_detail_components');
    }
}
