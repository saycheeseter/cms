<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 50);
            $table->string('username', 128);
            $table->string('password', 128);
            $table->string('full_name', 128);
            $table->string('position', 128)->nullable();
            $table->string('address', 512)->nullable();
            $table->string('telephone', 128)->nullable();
            $table->string('mobile', 128)->nullable();
            $table->string('email', 128)->nullable();
            $table->text('memo')->nullable();
            $table->smallInteger('type');
            $table->smallInteger('status');
            $table->bigInteger('created_from')->unsigned()->nullable();
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('modified_by')->unsigned()->nullable();
            $table->softDeletes();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
