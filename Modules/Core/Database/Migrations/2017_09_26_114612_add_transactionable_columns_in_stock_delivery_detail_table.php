<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionableColumnsInStockDeliveryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('transactionable_id')->nullable()->after('transaction_id');
            $table->string('transactionable_type')->nullable()->after('transactionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropColumn(['transactionable_id', 'transactionable_type']);
        });
    }
}
