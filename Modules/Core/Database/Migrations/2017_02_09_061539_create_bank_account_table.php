<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->string('bank', 45);
            $table->string('account_name', 45);
            $table->string('account_number', 45);
            $table->decimal('opening_balance', 23, 6)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bank_account', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account');
    }
}
