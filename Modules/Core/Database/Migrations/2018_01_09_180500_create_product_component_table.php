<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_component', function (Blueprint $table) {
            $table->unsignedBigInteger('group_id')->index();
            $table->unsignedBigInteger('component_id')->index();
            $table->decimal('qty', 16, 6);
        });

        Schema::table('product_component', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('product');
            $table->foreign('component_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_component');
    }
}
