<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToBranchPermissionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('created_from')->references('id')->on('branch');
        });

        Schema::table('branch_permission_user', function (Blueprint $table) {
            $table->primary(['branch_id', 'permission_id', 'user_id']);
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('permission_id')->references('id')->on('permission');
            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign(['created_by']);
            $table->dropForeign(['modified_by']);
            $table->dropForeign(['created_from']);

            $builder = Schema::getConnection()
                ->getDoctrineSchemaManager()
                ->listTableDetails($table->getTable());

            if($builder->hasIndex('user_created_from_foreign')) {
                $table->dropIndex('user_created_from_foreign');
            }

            if($builder->hasIndex('user_created_by_foreign')) {
                $table->dropIndex('user_created_by_foreign');
            }

            if($builder->hasIndex('user_modified_by_foreign')) {
                $table->dropIndex('user_modified_by_foreign');
            }
        });

        Schema::table('branch_permission_user', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['permission_id']);
            $table->dropForeign(['user_id']);

            $builder = Schema::getConnection()
                ->getDoctrineSchemaManager()
                ->listTableDetails($table->getTable());

            if($builder->hasIndex('branch_permission_user_permission_id_foreign')) {
                $table->dropIndex('branch_permission_user_permission_id_foreign');
            }

            if($builder->hasIndex('branch_permission_user_user_id_foreign')) {
                $table->dropIndex('branch_permission_user_user_id_foreign');
            }
        });
        
        Schema::table('branch_permission_user', function (Blueprint $table) {
            $table->dropPrimary(['branch_id', 'permission_id', 'user_id']);
        });
    }
}
