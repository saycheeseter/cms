<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCashDenominationTerminalForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_denomination', function (Blueprint $table) {
            $table->dropForeign(['terminal_number']);
        });

        Schema::table('cash_denomination', function (Blueprint $table) {
            $table->integer('terminal_number')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_denomination', function (Blueprint $table) {
            $table->unsignedBigInteger('terminal_number')->change();
        });

        Schema::table('cash_denomination', function (Blueprint $table) {
            $table->foreign('terminal_number')->references('id')->on('terminal');
        });
    }
}
