<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('permission', function (Blueprint $table) {
            $table->unsignedBigInteger('module_id')->after('id');
            $table->string('name', 50)->after('module_id');
            $table->string('translation', 50);
            $table->integer('sequence');
            $table->foreign('module_id')->references('id')->on('permission_module');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('permission', function (Blueprint $table) {
            $table->dropForeign(['module_id']);
            $table->dropColumn(['name', 'translation', 'sequence', 'module_id']);
        });

        Schema::enableForeignKeyConstraints();
    }
}
