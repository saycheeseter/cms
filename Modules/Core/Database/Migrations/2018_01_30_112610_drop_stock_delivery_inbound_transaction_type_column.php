<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropStockDeliveryInboundTransactionTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_delivery_inbound', function (Blueprint $table) {
            $table->dropColumn('transaction_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_delivery_inbound', function (Blueprint $table) {
            $table->smallInteger('transaction_type')->after('amount');
        });
    }
}
