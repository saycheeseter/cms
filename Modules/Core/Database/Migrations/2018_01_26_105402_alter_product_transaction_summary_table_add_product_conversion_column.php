<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductTransactionSummaryTableAddProductConversionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_transaction_summary', function (Blueprint $table) {
            $table->decimal('product_conversion_increase', 16, 6)->default(0);
            $table->decimal('product_conversion_decrease', 16, 6)->default(0);
            $table->decimal('auto_product_conversion_increase', 16, 6)->default(0);
            $table->decimal('auto_product_conversion_decrease', 16, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_transaction_summary', function (Blueprint $table) {
            $table->dropColumn('auto_product_conversion_decrease');
            $table->dropColumn('auto_product_conversion_increase');
            $table->dropColumn('product_conversion_decrease');
            $table->dropColumn('product_conversion_increase');
        });
    }
}
