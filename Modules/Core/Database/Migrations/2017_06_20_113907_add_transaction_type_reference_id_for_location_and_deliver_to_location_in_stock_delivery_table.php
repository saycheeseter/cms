<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionTypeReferenceIdForLocationAndDeliverToLocationInStockDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->smallInteger('transaction_type')->after('transaction_status');
            $table->unsignedBigInteger('for_location')->after('created_for');
            $table->unsignedBigInteger('deliver_to_location')->after('deliver_to');
            $table->unsignedBigInteger('reference_id')->nullable()->after('deliver_to_location');
        });

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->foreign('reference_id')->references('id')->on('stock_request');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('deliver_to_location')->references('id')->on('branch_detail');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->dropForeign(['reference_id']);
            $table->dropForeign(['for_location']);
            $table->dropForeign(['deliver_to_location']);
        });

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->dropColumn(['reference_id', 'transaction_type', 'for_location', 'deliver_to_location']);
        });

        Schema::enableForeignKeyConstraints();
    }
}
