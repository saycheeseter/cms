<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXyzReadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xyz_reading', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->integer('read_count');
            $table->smallInteger('read_type');
            $table->string('start_or', 100);
            $table->string('end_or', 100);
            $table->dateTime('date');
            $table->integer('terminal_number');
            $table->unsignedBigInteger('branch_id');
            $table->decimal('old_grand_total', 23, 6);
            $table->decimal('new_grand_total', 23, 6);
            $table->decimal('total_discount_amount', 23, 6);
            $table->decimal('vat_returns_amount', 23, 6);
            $table->decimal('nonvat_returns_amount', 23, 6);
            $table->decimal('senior_returns_amount', 23, 6);
            $table->decimal('cash_sales_amount', 23, 6);
            $table->decimal('credit_sales_amount', 23, 6);
            $table->decimal('debit_sales_amount', 23, 6);
            $table->decimal('bank_sales_amount', 23, 6);
            $table->decimal('gift_sales_amount', 23, 6);
            $table->decimal('senior_sales_amount', 23, 6);
            $table->decimal('vatable_sales_amount', 23, 6);
            $table->decimal('nonvat_sales_amount', 23, 6);
            $table->decimal('total_qty_sold', 16, 6);
            $table->decimal('total_qty_void', 16, 6);
            $table->decimal('void_transaction_amount', 23, 6);
            $table->decimal('success_transaction_count', 16, 6);
            $table->decimal('return_qty_count', 16, 6);
            $table->decimal('member_points_amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('xyz_reading', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('branch_id')->references('id')->on('branch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xyz_reading');
    }
}
