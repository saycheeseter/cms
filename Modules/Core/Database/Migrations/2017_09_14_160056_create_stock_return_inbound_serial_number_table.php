<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockReturnInboundSerialNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_return_inbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('stock_return_inbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'stri_detail_id_foreign')->references('id')->on('stock_return_inbound_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_return_inbound_serial_number');
    }
}
