<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTransactionSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_transaction_summary', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('for_location');
            $table->decimal('purchase_inbound', 16, 6);
            $table->decimal('stock_delivery_inbound', 16, 6);
            $table->decimal('stock_return_inbound', 16, 6);
            $table->decimal('sales_return_inbound', 16, 6);
            $table->decimal('adjustment_increase', 16, 6);
            $table->decimal('purchase_return_outbound', 16, 6);
            $table->decimal('stock_delivery_outbound', 16, 6);
            $table->decimal('stock_return_outbound', 16, 6);
            $table->decimal('sales_outbound', 16, 6);
            $table->decimal('damage_outbound', 16, 6);
            $table->decimal('adjustment_decrease', 16, 6);
            $table->date('transaction_date');
            $table->timestamps();
        });

        Schema::table('product_transaction_summary', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_transaction_summary');
    }
}
