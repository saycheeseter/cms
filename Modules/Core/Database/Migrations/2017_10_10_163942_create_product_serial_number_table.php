<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSerialNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_serial_number', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('supplier_id');
            $table->smallInteger('status');
            $table->smallInteger('transaction');
            $table->string('owner_type', '45')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->string('serial_number', 100);
            $table->date('expiration_date')->nullable();
            $table->date('manufacturing_date')->nullable();
            $table->date('admission_date')->nullable();
            $table->date('manufacturer_warranty_start')->nullable();
            $table->date('manufacturer_warranty_end')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });

        Schema::table('product_serial_number', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('supplier_id')->references('id')->on('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_serial_number');
    }
}
