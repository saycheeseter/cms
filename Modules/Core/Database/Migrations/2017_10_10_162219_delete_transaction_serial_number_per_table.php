<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTransactionSerialNumberPerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('purchase_inbound_serial_number');
        Schema::dropIfExists('purchase_return_outbound_serial_number');
        Schema::dropIfExists('damage_outbound_serial_number');
        Schema::dropIfExists('sales_outbound_serial_number');
        Schema::dropIfExists('sales_return_inbound_serial_number');
        Schema::dropIfExists('stock_delivery_outbound_serial_number');
        Schema::dropIfExists('stock_delivery_inbound_serial_number');
        Schema::dropIfExists('stock_return_outbound_serial_number');
        Schema::dropIfExists('stock_return_inbound_serial_number');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('purchase_inbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('purchase_inbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'pi_detail_id_foreign')->references('id')->on('purchase_inbound_detail');
        });

        Schema::create('purchase_return_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('purchase_return_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'pro_detail_id_foreign')->references('id')->on('purchase_return_outbound_detail');
        });

        Schema::create('damage_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('damage_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'do_detail_id_foreign')->references('id')->on('damage_outbound_detail');
        });

        Schema::create('sales_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('sales_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'so_detail_id_foreign')->references('id')->on('sales_outbound_detail');
        });

        Schema::create('sales_return_inbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('sales_return_inbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'sri_detail_id_foreign')->references('id')->on('sales_return_inbound_detail');
        });

        Schema::create('stock_delivery_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('stock_delivery_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'sdo_detail_id_foreign')->references('id')->on('stock_delivery_outbound_detail');
        });

        Schema::create('stock_delivery_inbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('stock_delivery_inbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'sdi_detail_id_foreign')->references('id')->on('stock_delivery_inbound_detail');
        });

        Schema::create('stock_return_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('stock_return_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'sro_detail_id_foreign')->references('id')->on('stock_return_outbound_detail');
        });

        Schema::create('stock_return_inbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('stock_return_inbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'stri_detail_id_foreign')->references('id')->on('stock_return_inbound_detail');
        });
    }
}
