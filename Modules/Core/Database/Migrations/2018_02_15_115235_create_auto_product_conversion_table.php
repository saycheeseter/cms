<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoProductConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_product_conversion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('transaction_id');
            $table->string('transaction_type');
            $table->dateTime('transaction_date');
            $table->unsignedBigInteger('product_id');
            $table->decimal('qty', 16, 6);
            $table->smallInteger('group_type')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('auto_product_conversion_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('required_qty', 16, 6);
        });

        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('product_id')->references('id')->on('product');
        });

        Schema::table('auto_product_conversion_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('auto_product_conversion');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_product_conversion_detail');
        Schema::dropIfExists('auto_product_conversion');
    }
}
