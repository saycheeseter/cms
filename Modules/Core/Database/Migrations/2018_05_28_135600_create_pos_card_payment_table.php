<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosCardPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_card_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('collection_id');
            $table->string('card_number', 128);
            $table->string('full_name', 128);
            $table->smallInteger('type');
            $table->decimal('amount', 23, 6);
            $table->string('expiration_date');
            $table->string('approval_code');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('pos_card_payment', function (Blueprint $table) {
            $table->foreign('collection_id')->references('id')->on('pos_collection');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_card_payment');
    }
}
