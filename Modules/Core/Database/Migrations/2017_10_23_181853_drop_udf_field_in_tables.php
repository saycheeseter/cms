<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUdfFieldInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('purchase_return', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('damage', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('sales_return', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_request', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_return', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('purchase_return_outbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('damage_outbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('sales_outbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('sales_return_inbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_delivery_outbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_delivery_inbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_return_outbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('stock_return_inbound', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });

        Schema::table('inventory_adjust', function (Blueprint $table) {
            $table->dropColumn('udfs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('purchase_return', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('damage', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('sales_return', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_request', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_delivery', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_return', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('purchase_inbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('purchase_return_outbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('damage_outbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('sales_outbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('sales_return_inbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_delivery_outbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_delivery_inbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_return_outbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('stock_return_inbound', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });

        Schema::table('inventory_adjust', function (Blueprint $table) {
            $table->text('udfs')->after('remarks')->nullable();
        });
    }
}
