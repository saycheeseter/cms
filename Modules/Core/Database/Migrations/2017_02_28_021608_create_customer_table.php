<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_head', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('name', 256);
            $table->string('code', 45);
            $table->string('memo', 512)->nullable();
            $table->decimal('credit_limit', 23, 6);
            $table->smallInteger('pricing_type');
            $table->smallInteger('historical_change_revert');
            $table->smallInteger('historical_default_price');
            $table->decimal('pricing_rate', 9, 6);
            $table->unsignedBigInteger('salesman_id');
            $table->string('website', 512)->nullable();
            $table->integer('term');
            $table->smallInteger('status');
            $table->softDeletes(); 
            $table->timestamps(); 
        });

        Schema::create('customer_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('head_id');
            $table->string('name', 100);
            $table->string('address', 512)->nullable();
            $table->string('chinesename', 512)->nullable();
            $table->string('ownername', 256)->nullable();
            $table->string('contact', 512)->nullable();
            $table->string('landline', 256)->nullable();
            $table->string('fax', 256)->nullable();
            $table->string('mobile', 256)->nullable();
            $table->string('email', 512)->nullable();
            $table->string('contactperson', 256)->nullable();
            $table->string('tin', 45)->nullable();
            $table->softDeletes();
            $table->timestamps(); 
        });

        Schema::table('customer_head', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->foreign('head_id')->references('id')->on('customer_head');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_detail');
        Schema::dropIfExists('customer_head');
    }
}
