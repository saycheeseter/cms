<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_customer', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('latest_unit_id');
            $table->decimal('latest_unit_qty', 16, 6);
            $table->decimal('min_qty', 16, 6);
            $table->decimal('highest_price', 23, 6);
            $table->decimal('lowest_price', 23, 6);
            $table->decimal('latest_price', 23, 6);
            $table->text('remarks')->nullable();
            $table->smallInteger('order_lead_time');
            $table->smallInteger('default');
            $table->timestamps();
        });

        Schema::table('product_customer', function (Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('latest_unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_customer');
    }
}
