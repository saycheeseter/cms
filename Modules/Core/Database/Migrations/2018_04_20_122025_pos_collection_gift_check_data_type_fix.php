<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PosCollectionGiftCheckDataTypeFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pos_collection', function (Blueprint $table) {
            $table->dropColumn('gift_check_id');
        });

        Schema::table('pos_collection', function (Blueprint $table) {
            $table->unsignedBigInteger('gift_check_id')->nullable();
        });

        Schema::table('pos_collection', function (Blueprint $table) {
            $table->foreign('gift_check_id')->references('id')->on('gift_check');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pos_collection', function (Blueprint $table) {
            $table->dropForeign(['gift_check_id']);
            $table->dropColumn('gift_check_id');
        });

        Schema::table('pos_collection', function (Blueprint $table) {
            $table->date('gift_check_id')->nullable();
        });
    }
}
