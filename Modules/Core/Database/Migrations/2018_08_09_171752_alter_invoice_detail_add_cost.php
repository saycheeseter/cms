<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvoiceDetailAddCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('purchase_return_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('damage_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('sales_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('sales_return_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_request_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_return_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('purchase_return_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('damage_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('sales_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('sales_return_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_request_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_delivery_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_return_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }
}
