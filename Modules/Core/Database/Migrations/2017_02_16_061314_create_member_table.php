<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rate_head_id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('card_id', 45);
            $table->string('barcode', 45)->nullable();
            $table->string('full_name', 50);
            $table->string('mobile', 50)->nullable();
            $table->string('telephone', 50)->nullable();
            $table->string('address', 512)->nullable();
            $table->string('email', 128)->nullable();
            $table->smallInteger('gender');
            $table->dateTime('birth_date');
            $table->dateTime('registration_date');
            $table->dateTime('expiration_date');
            $table->smallInteger('status');
            $table->string('memo', 128)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('member_rate_head', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('name', 45);
            $table->string('memo', 128)->nullable();
            $table->smallInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('member_rate_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_rate_head_id');
            $table->decimal('amount_from', 23, 6);
            $table->decimal('amount_to', 23, 6);
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->decimal('amount_per_point', 23, 6);
            $table->decimal('discount', 9, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('member', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('rate_head_id')->references('id')->on('member_rate_head');
        });

        Schema::table('member_rate_detail', function (Blueprint $table) {
            $table->foreign('member_rate_head_id')->references('id')->on('member_rate_head');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_rate_detail');
        Schema::dropIfExists('member');
        Schema::dropIfExists('member_rate_head');
    }
}
