<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedCheckTableToTransactionableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_check', function (Blueprint $table) {
            $table->dropForeign('received_check_collection_detail_id_foreign');
            $table->renameColumn('collection_detail_id', 'transactionable_id');
        });

        Schema::table('received_check', function (Blueprint $table) {
            $table->string('transactionable_type')->nullable()->after('transactionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issued_check', function (Blueprint $table) {
            $table->dropColumn(['transactionable_type']);
            $table->renameColumn('transactionable_id', 'collection_detail_id');
            $table->foreign('collection_detail_id')->references('id')->on('collection_detail');
        });
    }
}
