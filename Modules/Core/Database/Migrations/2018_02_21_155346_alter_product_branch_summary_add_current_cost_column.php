<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductBranchSummaryAddCurrentCostColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_branch_summary', function (Blueprint $table) {
            $table->decimal('current_cost', 23, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_branch_summary', function (Blueprint $table) {
            $table->dropColumn(['current_cost']);
        });
    }
}
