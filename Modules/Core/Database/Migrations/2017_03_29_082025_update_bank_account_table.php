<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account', function (Blueprint $table) {
            $table->dropColumn('bank');
        });

        Schema::table('bank_account', function (Blueprint $table) {
             $table->unsignedBigInteger('bank_id')->after('created_from');
        });

        Schema::table('bank_account', function (Blueprint $table) {
             $table->foreign('bank_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account', function (Blueprint $table) {
            $table->dropForeign(['bank_id']);
            $table->dropColumn('bank_id');
            $table->string('bank', 45)->after('created_from');
        });
    }
}
