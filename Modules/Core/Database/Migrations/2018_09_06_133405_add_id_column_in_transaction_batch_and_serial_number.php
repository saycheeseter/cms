<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdColumnInTransactionBatchAndSerialNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_serial_number', function (Blueprint $table) {
            $table->bigIncrements('id');
        });

        Schema::table('transaction_batch', function (Blueprint $table) {
            $table->bigIncrements('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_serial_number', function (Blueprint $table) {
            $table->dropColumn(['id']);
        });

        Schema::table('transaction_batch', function (Blueprint $table) {
            $table->dropColumn(['id']);
        });
    }
}
