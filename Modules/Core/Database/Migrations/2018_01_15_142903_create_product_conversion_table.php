<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_conversion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('requested_by');
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('approval_status');
            $table->unsignedBigInteger('product_id');
            $table->smallInteger('group_type')->default(1);
            $table->decimal('qty', 16, 6);
            $table->string('sheet_number', '100')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('product_conversion_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('component_id');
            $table->decimal('required_qty', 16, 6);
        });

        Schema::table('product_conversion', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('product_id')->references('id')->on('product');
        });

        Schema::table('product_conversion_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('product_conversion');
            $table->foreign('component_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_conversion_detail');
        Schema::dropIfExists('product_conversion');
    }
}
