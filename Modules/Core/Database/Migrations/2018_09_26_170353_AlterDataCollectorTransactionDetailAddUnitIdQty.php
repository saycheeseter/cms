<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDataCollectorTransactionDetailAddUnitIdQty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_collector_transaction_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->decimal('unit_qty', 16, 6)->default(0);
        });

        Schema::table('data_collector_transaction_detail', function (Blueprint $table) {
            $table->foreign('unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_collector_transaction_detail', function (Blueprint $table) {
            $table->dropForeign(['unit_id']);
        });

        Schema::table('data_collector_transaction_detail', function (Blueprint $table) {
            $table->dropColumn('unit_id');
            $table->dropColumn('unit_qty');
        });
    }
}
