<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_batch', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('supplier_id');
            $table->string('name', 45);
            $table->decimal('qty', 16, 6);
            $table->smallInteger('status');
            $table->timestamps();
        });

        Schema::create('product_batch_owner', function (Blueprint $table) {
            $table->unsignedBigInteger('batch_id');
            $table->string('owner_type')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->decimal('current_qty', 16, 6);
            $table->decimal('pending_qty', 16, 6);
        });

        Schema::create('transaction_batch', function (Blueprint $table) {
            $table->string('transactionable_type');
            $table->unsignedBigInteger('transactionable_id');
            $table->unsignedBigInteger('batch_id');
            $table->decimal('qty', 16, 6);
            $table->decimal('remaining_qty', 16, 6);
        });

        Schema::table('product_batch', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('supplier_id')->references('id')->on('supplier');
        });
    

        Schema::table('product_batch_owner', function(Blueprint $table) {
            $table->foreign('batch_id')->references('id')->on('product_batch');
        });

        Schema::table('transaction_batch', function(Blueprint $table) {
            $table->foreign('batch_id')->references('id')->on('product_batch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_batch');
        Schema::dropIfExists('product_batch_owner');
        Schema::dropIfExists('product_batch');
    }
}
