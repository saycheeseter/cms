<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockDeliveryInboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_delivery_inbound', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('requested_by');
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->unsignedBigInteger('delivery_from');
            $table->unsignedBigInteger('delivery_from_location');
            $table->unsignedBigInteger('for_location');
            $table->dateTime('transaction_date');
            $table->text('remarks')->nullable();
            $table->string('sheet_number', '100')->unique();
            $table->dateTime('audited_date')->nullable();
            $table->smallInteger('approval_status');
            $table->decimal('total_amount', 23, 6);
            $table->smallInteger('transaction_type');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('stock_delivery_inbound', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('delivery_from')->references('id')->on('branch');
            $table->foreign('delivery_from_location')->references('id')->on('branch_detail');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('reference_id')->references('id')->on('stock_delivery_outbound');
        });

        Schema::table('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('stock_delivery_inbound');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
            $table->foreign('reference_id')->references('id')->on('stock_delivery_outbound_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_delivery_inbound_detail');
        Schema::dropIfExists('stock_delivery_inbound');
    }
}
