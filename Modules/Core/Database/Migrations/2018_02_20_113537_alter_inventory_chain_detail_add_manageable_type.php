<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInventoryChainDetailAddManageableType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('damage_outbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('sales_outbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('sales_return_inbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_delivery_outbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_return_outbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });

        Schema::table('stock_return_inbound_detail', function (Blueprint $table) {
            $table->smallInteger('product_type')->default(1);
            $table->smallInteger('manageable')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('damage_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('sales_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('sales_return_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_delivery_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_return_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });

        Schema::table('stock_return_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('manageable');
            $table->dropColumn('product_type');
        });
    }
}
