<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryChainComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_chain_component', function (Blueprint $table) {
            $table->unsignedBigInteger('inventory_chain_detail_id');
            $table->string('inventory_chain_detail_type');
            $table->unsignedBigInteger('product_id');
            $table->decimal('qty', 16, 6);
            $table->smallInteger('group_type')->default(0);
        });

        Schema::table('inventory_chain_component', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_chain_component');
    }
}
