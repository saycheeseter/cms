<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForLocationInDamageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('damage', function (Blueprint $table) {
            $table->unsignedBigInteger('for_location')->default('1')->after('created_for');
        });

        Schema::table('damage', function (Blueprint $table) {
            $table->foreign('for_location')->references('id')->on('branch_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('damage', function (Blueprint $table) {
            $table->dropForeign(['for_location']);
            $table->dropColumn('for_location');
        });
    }
}
