<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostColumnToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('damage_outbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('sales_outbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('sales_return_inbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_delivery_outbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_return_outbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('stock_return_inbound_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('inventory_adjust_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });

        Schema::table('pos_sales_detail', function (Blueprint $table) {
            $table->decimal('cost', 23,  6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('damage_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('sales_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('sales_return_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_delivery_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_delivery_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_return_outbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('stock_return_inbound_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('inventory_adjust_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });

        Schema::table('pos_sales_detail', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }
}
