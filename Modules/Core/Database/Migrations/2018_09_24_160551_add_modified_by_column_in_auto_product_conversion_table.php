<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModifiedByColumnInAutoProductConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->unsignedBigInteger('modified_by')->nullable();
        });

        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->foreign('modified_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_product_conversion', function (Blueprint $table) {
            $table->dropForeign(['modified_by']);
            $table->dropColumn('modified_by');
        });
    }
}
