<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReservedAndRequestedQtyForProductBranchInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->decimal('reserved_qty', 16, 6)->after('product_id')->default('0.000000');
            $table->decimal('requested_qty', 16, 6)->after('reserved_qty')->default('0.000000');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_branch_inventory', function (Blueprint $table) {
            $table->dropColumn(['reserved_qty', 'requested_qty']);
        });
    }
}
