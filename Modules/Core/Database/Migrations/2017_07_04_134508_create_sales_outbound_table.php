<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOutboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_outbound', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('requested_by');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('customer_detail_id')->nullable();
            $table->unsignedBigInteger('salesman_id');
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('transaction_type');
            $table->smallInteger('approval_status');
            $table->decimal('total_amount', 23, 6);
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->smallInteger('customer_type');
            $table->string('customer_walk_in_name', 100)->nullable();
            $table->integer('term');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sales_outbound_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('sales_outbound', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('reference_id')->references('id')->on('sales');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->foreign('customer_detail_id')->references('id')->on('customer_detail');
        });

        Schema::table('sales_outbound_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('sales_outbound');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
            $table->foreign('reference_id')->references('id')->on('sales_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_outbound_detail');
        Schema::dropIfExists('sales_outbound');
    }
}
