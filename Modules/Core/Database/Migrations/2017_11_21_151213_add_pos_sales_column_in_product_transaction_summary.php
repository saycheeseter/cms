<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPosSalesColumnInProductTransactionSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_transaction_summary', function (Blueprint $table) {
            $table->decimal('pos_sales', 16, 6)->after('sales_outbound')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_transaction_summary', function (Blueprint $table) {
            $table->dropColumn('pos_sales');
        });
    }
}
