<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemCodeForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_code', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('system_code_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_code', function (Blueprint $table) {
            $table->dropForeign(['type_id']);

            $builder = Schema::getConnection()
                ->getDoctrineSchemaManager()
                ->listTableDetails($table->getTable());

            if($builder->hasIndex('system_code_type_id_foreign')) {
                $table->dropIndex('system_code_type_id_foreign');
            }
        });
    }
}
