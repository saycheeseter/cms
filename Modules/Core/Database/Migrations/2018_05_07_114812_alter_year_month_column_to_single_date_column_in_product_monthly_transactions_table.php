<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterYearMonthColumnToSingleDateColumnInProductMonthlyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->dropColumn(['year', 'month']);
        });

        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->date('transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->dropColumn('transaction_date');
        });

        Schema::table('product_monthly_transactions', function (Blueprint $table) {
            $table->integer('year');
            $table->integer('month');
        });
    }
}
