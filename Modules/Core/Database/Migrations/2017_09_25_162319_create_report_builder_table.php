<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportBuilderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_builder', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('group_id');
            $table->string('name', 45);
            $table->string('slug', 45);
            $table->text('format');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->timestamps();
        });

        Schema::table('report_builder', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_builder');
    }
}
