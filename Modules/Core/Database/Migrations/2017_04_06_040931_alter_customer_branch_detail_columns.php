<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerBranchDetailColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('customer_head', 'customer');

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('chinesename', 'chinese_name');
        });

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('ownername', 'owner_name');
        });

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('contactperson', 'contact_person');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('customer', 'customer_head');

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('chinese_name', 'chinesename');
        });

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('owner_name', 'ownername');
        });

        Schema::table('customer_detail', function (Blueprint $table) {
            $table->renameColumn('contact_person', 'contactperson');
        });
    }
}
