<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('stock_no', 50)->nullable();
            $table->string('supplier_sku', 50)->nullable();
            $table->string('name', 300);
            $table->string('description', 300)->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('category_id');
            $table->string('chinese_name', 256)->nullable();
            $table->smallInteger('senior');
            $table->smallInteger('discontinued');
            $table->string('memo', 1024)->nullable();
            $table->smallInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('product', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('supplier_id')->references('id')->on('supplier');
            $table->foreign('brand_id')->references('id')->on('system_code');
            $table->foreign('category_id')->references('id')->on('category');
        });

        Schema::create('tax', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 300);
            $table->decimal('percentage', 16, 6);
            $table->smallInteger('is_inclusive');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('product_tax', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('tax_id');
        });

        Schema::table('product_tax', function (Blueprint $table) {
            $table->primary(['product_id', 'tax_id']);
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('tax_id')->references('id')->on('tax');
        });

        Schema::create('product_unit', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('uom_id');
            $table->decimal('qty', 16, 6);
            $table->decimal('length', 16, 6)->nullable();
            $table->decimal('width', 16, 6)->nullable();
            $table->decimal('height', 16, 6)->nullable();
            $table->decimal('weight', 16, 6)->nullable();
            $table->smallInteger('default');
        });

        Schema::table('product_unit', function (Blueprint $table) {
            $table->primary(['product_id', 'uom_id']);
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('uom_id')->references('id')->on('system_code');
        });

        Schema::create('product_barcode', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('uom_id');
            $table->string('code', 50);
            $table->string('memo', 50)->nullable();
        });

        Schema::table('product_barcode', function (Blueprint $table) {
            $table->primary(['product_id', 'uom_id', 'code']);
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('uom_id')->references('id')->on('system_code');
        });

        Schema::create('branch_price', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('purchase_price', 16, 6);
            $table->decimal('selling_price', 16, 6);
            $table->decimal('wholesale_price', 16, 6);
            $table->decimal('price_a', 16, 6)->nullable();
            $table->decimal('price_b', 16, 6)->nullable();
            $table->decimal('price_c', 16, 6)->nullable();
            $table->decimal('price_d', 16, 6)->nullable();
            $table->decimal('price_e', 16, 6)->nullable();
            $table->smallInteger('editable');
            $table->smallInteger('status');
        });

        Schema::table('branch_price', function (Blueprint $table) {
            $table->primary(['product_id', 'branch_id']);
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_price');
        Schema::dropIfExists('product_barcode');
        Schema::dropIfExists('product_unit');
        Schema::dropIfExists('product_tax');
        Schema::dropIfExists('tax');
        Schema::dropIfExists('product');
    }
}
