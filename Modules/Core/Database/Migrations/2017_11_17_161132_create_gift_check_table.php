<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('gift_check', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 50);
            $table->string('check_number', 50);
            $table->decimal('amount', 16, 6);
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->smallInteger('status');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('redeemed_by')->nullable();
            $table->dateTime('redeemed_date')->nullable();
            $table->string('reference_number', 50)->nullable();
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('generated_rand', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('gift_check', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('redeemed_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gift_check');
    }
}
