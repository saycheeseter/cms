<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCustomerCreditForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_credits', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
        });

        Schema::table('customer_credits', function (Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_credits', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
        });

        Schema::table('customer_credits', function (Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('user');
        });
    }
}
