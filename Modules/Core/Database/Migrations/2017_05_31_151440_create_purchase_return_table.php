<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('requested_by');
            $table->unsignedBigInteger('payment_method');
            $table->unsignedBigInteger('created_for');
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->dateTime('transaction_date');
            $table->dateTime('audited_date')->nullable();
            $table->integer('term');
            $table->smallInteger('approval_status');
            $table->smallInteger('transaction_status');
            $table->decimal('total_amount', 23, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('purchase_return_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->decimal('remaining_qty', 16, 6);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('purchase_return', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('supplier_id')->references('id')->on('supplier');
            $table->foreign('requested_by')->references('id')->on('user');
            $table->foreign('payment_method')->references('id')->on('system_code');
            $table->foreign('created_for')->references('id')->on('branch');
        });

        Schema::table('purchase_return_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('purchase_return');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_detail');
        Schema::dropIfExists('purchase_return');
    }
}
