<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnOutboundSerialNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_outbound_serial_number', function (Blueprint $table) {
            $table->unsignedBigInteger('transaction_detail_id');
            $table->string('serial_no');
            $table->text('remarks')->nullable();
        });

        Schema::table('purchase_return_outbound_serial_number', function (Blueprint $table) {
            $table->foreign('transaction_detail_id', 'pro_detail_id_foreign')->references('id')->on('purchase_return_outbound_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_outbound_serial_number');
    }
}
