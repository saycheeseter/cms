<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnOutboundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_outbound', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('payment_method');
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('requested_by');
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('transaction_type');
            $table->smallInteger('approval_status');
            $table->decimal('total_amount', 23, 6);
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->integer('term');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('oprice', 23, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('discount1', 9, 6);
            $table->decimal('discount2', 9, 6);
            $table->decimal('discount3', 9, 6);
            $table->decimal('discount4', 9, 6);
            $table->string('remarks', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('purchase_return_outbound', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('reference_id')->references('id')->on('purchase_return');
            $table->foreign('supplier_id')->references('id')->on('supplier');
            $table->foreign('payment_method')->references('id')->on('system_code');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('requested_by')->references('id')->on('user');
        });

        Schema::table('purchase_return_outbound_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('purchase_return_outbound');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
            $table->foreign('reference_id')->references('id')->on('purchase_return_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_outbound_detail');
        Schema::dropIfExists('purchase_return_outbound');
    }
}
