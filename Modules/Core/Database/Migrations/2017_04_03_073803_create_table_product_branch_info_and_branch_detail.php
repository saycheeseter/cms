<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductBranchInfoAndBranchDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_branch_info', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('product_id');
            $table->smallInteger('status');
        });

        Schema::table('product_branch_info', function (Blueprint $table) {
            $table->primary(['product_id', 'branch_id']);
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->foreign('product_id')->references('id')->on('product');
        });

        Schema::create('product_branch_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('branch_detail_id');
            $table->decimal('min', 16, 6);
            $table->decimal('max', 16, 6);
        });

        Schema::table('product_branch_detail', function (Blueprint $table) {
            $table->primary(['product_id', 'branch_detail_id']);
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('branch_detail_id')->references('id')->on('branch_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_branch_detail');
        Schema::dropIfExists('product_branch_info');
    }
}
