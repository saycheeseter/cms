<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_login_schedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('mon');
            $table->smallInteger('tue');
            $table->smallInteger('wed');
            $table->smallInteger('thu');
            $table->smallInteger('fri');
            $table->smallInteger('sat');
            $table->smallInteger('sun');
            $table->time('start');
            $table->time('end');
        });

        Schema::table('user_login_schedule', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_login_schedule');
    }
}
