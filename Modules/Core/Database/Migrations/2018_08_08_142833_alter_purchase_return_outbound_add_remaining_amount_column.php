<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseReturnOutboundAddRemainingAmountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_return_outbound', function(Blueprint $table) {
            $table->decimal('remaining_amount', 23, 6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_return_outbound', function(Blueprint $table) {
            $table->dropColumn(['remaining_amount']);
        });
    }
}
