<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerBalanceFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_balance_flow', function (Blueprint $table) {
            $table->unsignedBigInteger('transactionable_id');
            $table->string('transactionable_type', 45);
            $table->unsignedBigInteger('customer_id');
            $table->smallInteger('flow_type');
            $table->dateTime('transaction_date');
            $table->decimal('amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('customer_balance_flow', function (Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->primary(['transactionable_id', 'transactionable_type', 'flow_type'], 'cbf_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_balance_flow');
    }
}
