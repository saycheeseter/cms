<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePermissionTablesNullableTranslationAliasLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('permission_section', function(Blueprint $table) {
            $table->string('alias', 128)->change();
            $table->string('translation', 128)->nullable()->change();
        });

        Schema::table('permission_module', function(Blueprint $table) {
            $table->string('alias', 128)->change();
            $table->string('translation', 128)->nullable()->change();
        });

        Schema::table('permission', function(Blueprint $table) {
            $table->string('alias', 128)->change();
            $table->string('translation', 128)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('permission_section', function(Blueprint $table) {
            $table->string('alias', 45)->change();
            $table->string('translation', 45)->change();
        });

        Schema::table('permission_module', function(Blueprint $table) {
            $table->string('alias', 45)->change();
            $table->string('translation', 45)->change();
        });

        Schema::table('permission', function(Blueprint $table) {
            $table->string('alias', 45)->change();
            $table->string('translation', 45)->change();
        });
    }
}
