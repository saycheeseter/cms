<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_component', function (Blueprint $table) {
            $table->unsignedBigInteger('invoice_detail_id');
            $table->string('invoice_detail_type');
            $table->unsignedBigInteger('product_id');
            $table->decimal('qty', 16, 6);
            $table->smallInteger('group_type')->default(0);
        });

        Schema::table('invoice_component', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_component');
    }
}
