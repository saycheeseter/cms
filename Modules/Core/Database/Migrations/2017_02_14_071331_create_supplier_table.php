<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('code', 50);
            $table->string('full_name', 128);
            $table->string('chinese_name', 128)->nullable();
            $table->string('owner_name', 128)->nullable();
            $table->string('contact', 128)->nullable();
            $table->string('address', 512)->nullable();
            $table->string('landline', 128)->nullable();
            $table->string('telephone', 128)->nullable();
            $table->string('fax', 128)->nullable();
            $table->string('mobile', 128)->nullable();
            $table->string('email', 128)->nullable();
            $table->string('website', 128)->nullable();
            $table->integer('term');
            $table->smallInteger('status');
            $table->string('contact_person', 512)->nullable();
            $table->string('memo', 128)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('supplier', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
