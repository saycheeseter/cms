<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStockCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stock_card', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('previous_id')->nullable();
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('product_id');
            $table->bigInteger('sequence_no');
            $table->integer('reference_type');
            $table->unsignedBigInteger('reference_id');
            $table->dateTime('transaction_date');
            $table->decimal('qty', 16, 6);
            $table->decimal('price', 23, 6);
            $table->decimal('amount', 23, 6);
            $table->decimal('cost', 23, 6);
            $table->decimal('pre_inventory', 16, 6);
            $table->decimal('pre_avg_cost', 23, 6);
            $table->decimal('pre_amount', 23, 6);
            $table->decimal('post_inventory', 16, 6);
            $table->decimal('post_avg_cost', 23, 6);
            $table->decimal('post_amount', 23, 6);
            $table->timestamps();
        });

        Schema::table('product_stock_card', function (Blueprint $table) {
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stock_card');
    }
}
