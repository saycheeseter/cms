<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIssuedCheckTableToTransactionableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issued_check', function (Blueprint $table) {
            $table->dropForeign('issued_check_payment_detail_id_foreign');
            $table->renameColumn('payment_detail_id', 'transactionable_id');
        });

        Schema::table('issued_check', function (Blueprint $table) {
            $table->string('transactionable_type')->nullable()->after('transactionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issued_check', function (Blueprint $table) {
            $table->dropColumn(['transactionable_type']);
            $table->renameColumn('transactionable_id', 'payment_detail_id');
            $table->foreign('payment_detail_id')->references('id')->on('payment_detail');
        });
    }
}
