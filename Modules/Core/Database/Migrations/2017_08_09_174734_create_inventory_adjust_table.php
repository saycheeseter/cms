<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_adjust', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_from');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('created_for');
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->unsignedBigInteger('audited_by')->nullable();
            $table->unsignedBigInteger('for_location');
            $table->unsignedBigInteger('requested_by');
            $table->dateTime('audited_date')->nullable();
            $table->dateTime('transaction_date');
            $table->smallInteger('approval_status');
            $table->decimal('total_gain_amount', 23, 6);
            $table->decimal('total_lost_amount', 23, 6);
            $table->string('sheet_number', '100')->unique();
            $table->text('remarks')->nullable();
            $table->text('udfs')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('inventory_adjust_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('unit_id');
            $table->decimal('unit_qty', 16, 6);
            $table->decimal('qty', 16, 6);
            $table->decimal('pc_qty', 16, 6);
            $table->decimal('inventory', 16, 6);
            $table->decimal('price', 23, 6);
            $table->string('remarks', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('inventory_adjust', function (Blueprint $table) {
            $table->foreign('created_from')->references('id')->on('branch');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('modified_by')->references('id')->on('user');
            $table->foreign('audited_by')->references('id')->on('user');
            $table->foreign('created_for')->references('id')->on('branch');
            $table->foreign('for_location')->references('id')->on('branch_detail');
            $table->foreign('requested_by')->references('id')->on('user');
        });

        Schema::table('inventory_adjust_detail', function (Blueprint $table) {
            $table->foreign('transaction_id')->references('id')->on('inventory_adjust');
            $table->foreign('product_id')->references('id')->on('product');
            $table->foreign('unit_id')->references('id')->on('system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_adjust_detail');
        Schema::dropIfExists('inventory_adjust');
    }
}
