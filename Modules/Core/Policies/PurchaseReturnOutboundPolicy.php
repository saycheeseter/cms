<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class PurchaseReturnOutboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'purchase_return_outbound';
    }
}