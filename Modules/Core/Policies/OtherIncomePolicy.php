<?php

namespace Modules\Core\Policies;

class OtherIncomePolicy extends OtherFinancialFlowPolicy
{   
    protected $key = 'other_income';
}