<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

abstract class SystemCodePolicy
{   
    protected $key;

    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(
            [
                'view_'.$this->key, 
                'create_'.$this->key, 
                'update_'.$this->key, 
                'delete_'.$this->key
            ]
        );
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo('create_'.$this->key);
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo('update_'.$this->key);
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo('delete_'.$this->key);
    }

    /**
     * Determine if the user has access to import excel data.
     *
     * @param  $user
     * @return bool
     */
    public function importExcel(User $user)
    {
        return $user->hasAccessTo('import_excel_'.$this->key);
    }
}