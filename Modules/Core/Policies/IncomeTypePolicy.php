<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class IncomeTypePolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'income_type';
    }
}