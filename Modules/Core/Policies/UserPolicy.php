<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class UserPolicy
{   
    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(['view_user', 'view_user_detail', 'create_user', 'update_user', 'delete_user']);
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetail(User $user)
    {
        return $user->isEitherGrantedTo(['view_user_detail', 'create_user', 'update_user']);
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo('create_user');
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo('update_user');
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo('delete_user');
    }
}