<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class PaymentMethodPolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'payment_method';
    }
}