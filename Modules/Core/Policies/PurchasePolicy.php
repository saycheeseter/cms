<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class PurchasePolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'purchase_order';
    }
}