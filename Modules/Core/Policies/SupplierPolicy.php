<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class SupplierPolicy
{   
    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(
            [
                'view_supplier', 
                'view_supplier_detail', 
                'create_supplier', 
                'update_supplier', 
                'delete_supplier'
            ]
        );
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetail(User $user)
    {
        return $user->isEitherGrantedTo(['view_supplier_detail', 'create_supplier', 'update_supplier']);
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo('create_supplier');
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo('update_supplier');
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo('delete_supplier');
    }
}