<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class StockDeliveryInboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'stock_delivery_inbound';
    }
}