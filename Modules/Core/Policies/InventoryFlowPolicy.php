<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

abstract class InventoryFlowPolicy
{   
    protected $key;

    /**
     * Determine if the user has access to the page
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(
            [
                sprintf("view_%s", $this->key), 
                sprintf("view_%s_detail", $this->key), 
                sprintf("create_%s", $this->key), 
                sprintf("update_%s", $this->key), 
                sprintf("delete_%s", $this->key),
                sprintf("approve_%s", $this->key),
                sprintf("decline_%s", $this->key)
            ]
        );
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetail(User $user)
    {
        return $user->isEitherGrantedTo([
            sprintf("view_%s_detail", $this->key), 
            sprintf("create_%s", $this->key),
            sprintf("update_%s", $this->key),
            sprintf("approve_%s", $this->key),
            sprintf("decline_%s", $this->key),
        ]);
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo(sprintf('create_%s', $this->key));
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo(sprintf('update_%s', $this->key));
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo(sprintf('delete_%s', $this->key));
    }

    /**
     * Determine if the user has access to approve transaction.
     *
     * @param  $user
     * @return bool
     */
    public function approve(User $user)
    {
        return $user->hasAccessTo(sprintf('approve_%s', $this->key));
    }

    /**
     * Determine if the user has access to decline transaction.
     *
     * @param  $user
     * @return bool
     */
    public function decline(User $user)
    {
        return $user->hasAccessTo(sprintf('decline_%s', $this->key));
    }

    /**
     * Determine if the user has access to export excel from list.
     *
     * @param  $user
     * @return bool
     */
    public function export(User $user)
    {
        return $user->hasAccessTo(sprintf('export_%s', $this->key));
    }

    /**
     * Determine if the user has access to print detail
     *
     * @param  $user
     * @return bool
     */
    public function print(User $user)
    {
        return $user->hasAccessTo(sprintf('print_%s_detail', $this->key));
    }

    /**
     * Determine if the user has access to column settings functionality
     *
     * @param  $user
     * @return bool
     */
    public function useColumnSettings(User $user)
    {   
        return $user->hasAccessTo(sprintf('column_settings_%s', $this->key));
    }

    /**
     * Determine if the user has access to view detail price and amount
     *
     * @param  $user
     * @return bool
     */
    public function showPriceAmount(User $user)
    {
        return $user->hasAccessTo(sprintf('show_price_amount_%s', $this->key));
    }
}