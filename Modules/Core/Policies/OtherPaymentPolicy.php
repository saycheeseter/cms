<?php

namespace Modules\Core\Policies;

class OtherPaymentPolicy extends OtherFinancialFlowPolicy
{   
    protected $key = 'other_payment';
}