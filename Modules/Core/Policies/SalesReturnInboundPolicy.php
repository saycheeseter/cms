<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class SalesReturnInboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'sales_return_inbound';
    }
}