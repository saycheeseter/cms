<?php

namespace Modules\Core\Policies;

class PaymentPolicy extends FinancialFlowPolicy
{   
    protected $key = 'payment';
}