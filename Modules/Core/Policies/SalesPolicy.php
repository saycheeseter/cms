<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class SalesPolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'sales';
    }
}