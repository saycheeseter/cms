<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class StockReturnInboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'stock_return_inbound';
    }
}