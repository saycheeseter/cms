<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class StockReturnPolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'stock_return';
    }
}