<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class BankPolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'bank';
    }
}