<?php

namespace Modules\Core\Policies;

class CollectionPolicy extends FinancialFlowPolicy
{   
    protected $key = 'collection';
}