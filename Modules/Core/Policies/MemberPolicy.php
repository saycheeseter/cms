<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class MemberPolicy
{   
    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(
            [
                'view_member', 
                'view_member_detail', 
                'create_member', 
                'update_member', 
                'delete_member'
            ]
        );
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetail(User $user)
    {
        return $user->isEitherGrantedTo(['view_member_detail', 'create_member', 'update_member']);
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo('create_member');
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo('update_member');
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo('delete_member');
    }

    /**
     * Determine if the user has access to import data.
     *
     * @param  $user
     * @return bool
     */
    public function importExcel(User $user)
    {
        return $user->hasAccessTo('import_excel');
    }
}