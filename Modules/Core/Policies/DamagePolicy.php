<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class DamagePolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'damage';
    }
}