<?php

namespace Modules\Core\Policies;

class InventoryAdjustPolicy extends InventoryFlowPolicy
{
    public function __construct()
    {
        $this->key = 'inventory_adjust';
    }
}