<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class SalesOutboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'sales_outbound';
    }

    public function allowExceedCreditLimit(User $user)
    {
        return $user->hasAccessTo('allow_customer_exceed_credit_limit_sales_outbound');
    }
}