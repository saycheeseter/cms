<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class CheckManagementPolicy
{   
    protected $key;

    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {   
        return $user->isEitherGrantedTo(
            [
                sprintf('view_%s', $this->key), 
                sprintf('transfer_%s',  $this->key), 
            ]
        );
    }

    /**
     * Determine if the user has access to transfer check.
     *
     * @param  $user
     * @return bool
     */
    public function transfer(User $user)
    {   
        return $user->isEitherGrantedTo([
            sprintf('transfer_%s', $this->key),
        ]);
    }

    /**
     * Determine if the user has access to transfer check.
     *
     * @param  $user
     * @return bool
     */
    public function useColumnSettings(User $user)
    {   
        return $user->hasAccessTo(sprintf('column_settings_%s', $this->key));
    }
}