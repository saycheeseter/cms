<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class StockDeliveryOutboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'stock_delivery_outbound';
    }

    /**
     * Determine if the user has access to the page
     *
     * @param  $user
     * @return bool
     */
    public function viewFrom(User $user)
    {
        return $user->isEitherGrantedTo(
            [
                'view_stock_delivery_outbound_from', 
                'view_stock_delivery_outbound_from_detail'
            ]
        );
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetailFrom(User $user)
    {
        return $user->hasAccessTo('view_stock_delivery_outbound_from_detail');
    }

    /**
     * Determine if the user has access to export excel from list on stock delivery outbound from module.
     *
     * @param  $user
     * @return bool
     */
    public function exportFrom(User $user)
    {
        return $user->hasAccessTo(sprintf('export_stock_delivery_outbound_from'));
    }

    /**
     * Determine if the user has access to print detail on stock delivery outbound from module.
     *
     * @param  $user
     * @return bool
     */
    public function printFrom(User $user)
    {
        return $user->hasAccessTo(sprintf('print_stock_delivery_outbound_from_detail'));
    }

    /**
     * Determine if the user has access to column settings functionality on stock delivery outbound from module.
     *
     * @param  $user
     * @return bool
     */
    public function useColumnSettingsFrom(User $user)
    {
        return $user->hasAccessTo(sprintf('column_settings_stock_delivery_outbound_from'));
    }

    /**
     * Determine if the user has access to view amount and price 
     *
     * @param  $user
     * @return bool
     */
    public function showPriceAmountFrom(User $user)
    {
        return $user->hasAccessTo(sprintf('show_price_amount_stock_delivery_outbound_from'));
    }
}