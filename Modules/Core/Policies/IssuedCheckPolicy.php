<?php

namespace Modules\Core\Policies;

class IssuedCheckPolicy extends CheckManagementPolicy
{   
    protected $key = 'issued_check';
}