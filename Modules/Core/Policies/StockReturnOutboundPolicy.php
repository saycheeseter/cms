<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class StockReturnOutboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'stock_return_outbound';
    }
}