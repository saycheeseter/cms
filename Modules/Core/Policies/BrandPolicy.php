<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class BrandPolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'brand';
    }
}