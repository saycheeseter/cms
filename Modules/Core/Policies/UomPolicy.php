<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class UomPolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'uom';
    }
}