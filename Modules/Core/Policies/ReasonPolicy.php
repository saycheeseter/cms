<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class ReasonPolicy extends SystemCodePolicy
{   
    public function __construct()
    {
        $this->key = 'reason';
    }
}