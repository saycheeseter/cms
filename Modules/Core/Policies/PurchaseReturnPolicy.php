<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class PurchaseReturnPolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'purchase_return';
    }
}