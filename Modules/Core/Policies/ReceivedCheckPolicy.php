<?php

namespace Modules\Core\Policies;

class ReceivedCheckPolicy extends CheckManagementPolicy
{   
    protected $key = 'received_check';
}