<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class ProductPolicy
{    
    /**
     * Determine if the user has access to view page.
     *
     * @param  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isEitherGrantedTo(['view_product', 'view_product_detail', 'create_product', 'update_product', 'delete_product']);
    }

    /**
     * Determine if the user has access to view data details.
     *
     * @param  $user
     * @return bool
     */
    public function viewDetail(User $user)
    {
        return $user->isEitherGrantedTo(['view_product_detail', 'create_product', 'update_product']);
    }

    /**
     * Determine if the user has access to store data.
     *
     * @param  $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->hasAccessTo('create_product');
    }

    /**
     * Determine if the user has access to update data.
     *
     * @param  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasAccessTo('update_product');
    }

    /**
     * Determine if the user has access to delete data.
     *
     * @param  $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasAccessTo('delete_product');
    }

    /**
     * Determine if the user has access to use column settings feature
     *
     * @param  $user
     * @return bool
     */
    public function useColumnSettings(User $user)
    {
        return $user->hasAccessTo('column_settings_product');
    }

    /**
     * Determine if the user has access to view product purchase price
     *
     * @param  $user
     * @return bool
     */
    public function showCost(User $user)
    {
        return $user->hasAccessTo('show_cost_product');
    }

    /**
     * Determine if the user has access to import excel
     *
     * @param  $user
     * @return bool
     */
    public function importExcel(User $user)
    {
        return $user->hasAccessTo('import_excel_product');
    }


    /**
     * Determine if the user has access to export excel.
     *
     * @param  $user
     * @return bool
     */
    public function exportExcel(User $user)
    {
        return $user->hasAccessTo('export_excel_product');
    }
}