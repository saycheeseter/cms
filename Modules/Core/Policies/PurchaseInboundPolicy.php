<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class PurchaseInboundPolicy extends InventoryChainPolicy
{   
    public function __construct()
    {
        $this->key = 'purchase_inbound';
    }
}