<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class SalesReturnPolicy extends InvoicePolicy
{   
    public function __construct()
    {
        $this->key = 'sales_return';
    }
}