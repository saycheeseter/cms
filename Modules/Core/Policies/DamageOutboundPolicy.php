<?php

namespace Modules\Core\Policies;

use Modules\Core\Entities\User;

class DamageOutboundPolicy extends InventoryChainPolicy
{  
    public function __construct()
    {
        $this->key = 'damage_outbound';
    }
}