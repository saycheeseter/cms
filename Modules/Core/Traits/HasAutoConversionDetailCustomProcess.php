<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\AutoProductConversionDetailObserver;

trait HasAutoConversionDetailCustomProcess
{
    public static function bootHasAutoConversionDetailCustomProcess()
    {
        static::observe(new AutoProductConversionDetailObserver);
    }
}