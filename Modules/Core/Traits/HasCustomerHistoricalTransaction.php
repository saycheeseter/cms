<?php

namespace Modules\Core\Traits;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Observers\TransactionCustomerHistoricalDataObserver;

trait HasCustomerHistoricalTransaction
{
    public static function bootHasCustomerHistoricalTransaction()
    {
        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
            return;
        }

        static::observe(new TransactionCustomerHistoricalDataObserver);
    }
}