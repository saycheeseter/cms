<?php

namespace Modules\Core\Traits;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Observers\TransactionSupplierHistoricalDataObserver;

trait HasSupplierHistoricalTransaction
{
    public static function bootHasSupplierHistoricalTransaction()
    {
        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
            return;
        }

        static::observe(new TransactionSupplierHistoricalDataObserver);
    }
}