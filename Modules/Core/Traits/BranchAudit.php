<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\BranchAuditObserver;

trait BranchAudit
{
    public static function bootBranchAudit()
    {
        static::observe(new BranchAuditObserver());
    }
}