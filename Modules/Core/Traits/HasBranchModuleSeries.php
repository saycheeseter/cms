<?php

namespace Modules\Core\Traits;

trait HasBranchModuleSeries 
{
    /**
     * Module Series boot logic.
     *
     * @return void
     */
    public static function bootHasBranchModuleSeries()
    {
        static::observe(new \Modules\Core\Observers\BranchModuleSeriesObserver());
    }

    public function getSeriesTypeAttribute()
    {
        return $this->getMorphMapKey(static::class);
    }

    public function getSeriesBranchAttribute()
    {
        return branch();
    }
}