<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryChainDetailItemManagementObserver;

trait DetachesItemManagementUponDeletion
{
    public static function bootDetachesItemManagementUponDeletion()
    {
        static::observe(new InventoryChainDetailItemManagementObserver);
    }
}