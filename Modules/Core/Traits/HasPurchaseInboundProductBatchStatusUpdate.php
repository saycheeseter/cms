<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\PurchaseInboundProductBatchObserver;

trait HasPurchaseInboundProductBatchStatusUpdate
{
    public static function bootHasPurchaseInboundProductBatchStatusUpdate()
    {
        static::observe(new PurchaseInboundProductBatchObserver);
    }
}