<?php

namespace Modules\Core\Traits;

use Modules\Core\Presenters\NumberFormat;

trait FormatsNumber
{
    /**
     * Formats number according to system config precision
     * 
     * @return NumberFormat
     */
    public function number()
    {
        return new NumberFormat($this);
    }
}