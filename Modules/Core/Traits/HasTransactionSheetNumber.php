<?php

namespace Modules\Core\Traits;

trait HasTransactionSheetNumber 
{
    public function setSheetNumberAttribute($value)
    {
        $this->attributes['sheet_number'] = $this->sheet_number_prefix.$value;
    }
}