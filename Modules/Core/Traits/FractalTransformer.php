<?php

namespace Modules\Core\Traits;

use League\Fractal\Serializer\SerializerAbstract;
use Illuminate\Support\Collection;
use Fractal;

trait FractalTransformer
{
    /**
     * API shortcut for accessing array values of fractal item
     * 
     * @param  Entity $item
     * @return array      
     */
    protected function item($item, $transformer)
    {
        return Fractal::item($item, new $transformer)->getArray();
    }

    /**
     * API shortcut for accessing array values of fractal collection
     * @param               $collection  
     * @param  Callable     $transformer 
     * @param  string     $key   
     * @return array                  
     */
    protected function collection($collection, $transformer, $key = null)
    {
        if (($collection instanceof Collection && $collection->count() === 0)
            || (is_array($collection) && count($collection) === 0)
        ) {
            return [$key ?? 'data' => []];
        }

        $collection = Fractal::collection($collection, new $transformer)->getArray();

        if (!is_null($key)) {
            $collection[$key] = $collection['data'];
            unset($collection['data']);
        }

        return $collection;
    }

    /**
     * Set serializer for Fractal
     * @param  SerializerAbstract $serializer
     * @return $this                        
     */
    protected function serializer(SerializerAbstract $serializer)
    {
        Fractal::setSerializer($serializer);

        return $this;
    }
}