<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\PaymentObserver;

trait HasPaymentProcess
{
    public static function bootHasPaymentProcess()
    {
        static::observe(new PaymentObserver);
    }
}