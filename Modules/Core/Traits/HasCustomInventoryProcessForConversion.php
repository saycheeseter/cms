<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ProductConversionObserver;

trait HasCustomInventoryProcessForConversion
{
    public static function bootHasCustomInventoryProcessForConversion()
    {
        static::observe(new ProductConversionObserver);
    }
}