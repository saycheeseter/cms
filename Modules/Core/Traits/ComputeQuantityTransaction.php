<?php

namespace Modules\Core\Traits;

use Litipk\BigNumbers\Decimal;

trait ComputeQuantityTransaction
{
    public function getTotalUnsignedRemainingQtyAttribute()
    {
        $remaining_qty = Decimal::create(0, setting('monetary.precision'));

        foreach($this->details as $detail) {
            $remaining_qty = $remaining_qty->add(
                $detail->remaining_qty->isLessThan(Decimal::create(0, setting('monetary.precision')))
                    ? Decimal::create(0, setting('monetary.precision'))
                    : $detail->remaining_qty
            );
        }

        return $remaining_qty;
    }

    public function getTotalSignedRemainingQtyAttribute()
    {
        $remaining_qty = Decimal::create(0, setting('monetary.precision'));

        foreach ($this->details as $detail) {
            $remaining_qty = $remaining_qty->add($detail->remaining_qty);
        }

        return $remaining_qty;
    }
}