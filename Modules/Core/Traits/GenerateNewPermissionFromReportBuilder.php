<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ReportBuilderObserver;

trait GenerateNewPermissionFromReportBuilder
{
    public static function bootGenerateNewPermissionFromReportBuilder()
    {
        static::observe(new ReportBuilderObserver);
    }
}