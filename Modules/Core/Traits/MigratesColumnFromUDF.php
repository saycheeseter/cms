<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\UserDefinedFieldObserver;

trait MigratesColumnFromUDF
{
    public static function bootMigratesColumnFromUDF()
    {
        static::observe(new UserDefinedFieldObserver);
    }
}