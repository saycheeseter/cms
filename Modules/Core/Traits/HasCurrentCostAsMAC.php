<?php

namespace Modules\Core\Traits;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Observers\TransactionMovingAverageCostObserver;

trait HasCurrentCostAsMAC
{
    public static function bootHasCurrentCostAsMAC()
    {
        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
            return;
        }

        static::observe(new TransactionMovingAverageCostObserver);
    }
}