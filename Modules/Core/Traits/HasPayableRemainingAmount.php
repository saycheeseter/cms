<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\HasPayableRemainingAmountObserver;

trait HasPayableRemainingAmount
{
    public static function bootHasPayableRemainingAmount()
    {
        static::observe(new HasPayableRemainingAmountObserver);
    }
}