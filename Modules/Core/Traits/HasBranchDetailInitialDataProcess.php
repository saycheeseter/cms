<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\BranchDetailObserver;

trait HasBranchDetailInitialDataProcess
{
    public static function bootHasBranchDetailInitialDataProcess()
    {
        static::observe(new BranchDetailObserver);
    }
}