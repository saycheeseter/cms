<?php

namespace Modules\Core\Traits;

use Modules\Core\Entities\Product;

trait AuditableToProduct
{
    protected function getAuditableKey()
    {
        return $this->product->id;
    }

    protected function getAuditableMorphClass()
    {
        return $this->getMorphMapKey(get_class($this->product));
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}