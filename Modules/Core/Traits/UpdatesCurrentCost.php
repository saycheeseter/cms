<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ProductStockCardObserver;

trait UpdatesCurrentCost
{
    public static function bootUpdatesCurrentCost()
    {
        static::observe(new ProductStockCardObserver);
    }
}