<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\GenerateConversionProductMonthlySummaryObserver;

trait GenerateConversionProductMonthlySummary
{
    public static function bootGenerateConversionProductMonthlySummary()
    {
        static::observe(new GenerateConversionProductMonthlySummaryObserver);
    }
}