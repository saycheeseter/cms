<?php

namespace Modules\Core\Traits;

use Modules\Core\Presenters\UDFFormat;

trait HasUDF
{
    /**
     * Formats number according to system config precision
     * 
     * @return UDFFormat
     */
    public function udf()
    {
        return new UDFFormat($this);
    }
}