<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Collection;
use Request;

trait PaginatesCollection
{
    protected function paginate(Collection $items, $perPage = 10)
    {
        $pageStart           = Request::get('page', 1);
        $offSet              = ($pageStart * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $items, 
            $items->count(), 
            $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }
}