<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\CounterDetailObserver;

trait HasCounterTotalAmountComputationProcess
{
    public static function bootHasTotalAmountComputationProcess()
    {
        static::observe(new CounterDetailObserver);
    }
}