<?php

namespace Modules\Core\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;

trait HasMorphMap
{
    /**
     * Get the specified morph map key of the given class name
     *  
     * @param  string $class
     * @return string
     */
    protected function getMorphMapKey($class)
    {
        return array_search($class, Relation::morphMap());
    }
}