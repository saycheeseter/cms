<?php

namespace Modules\Core\Traits;

trait HasPriceAsMovingAverageCost
{
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value;
        $this->attributes['ma_cost'] = $value;
    }
}