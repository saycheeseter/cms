<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\AutoProductConversionObserver;

trait HasCustomAutoConversionProcess
{
    public static function bootHasCustomAutoConversionProcess()
    {
        static::observe(new AutoProductConversionObserver);
    }
}