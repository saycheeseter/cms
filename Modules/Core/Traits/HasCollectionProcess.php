<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\CollectionObserver;

trait HasCollectionProcess
{
    public static function bootHasCollectionProcess()
    {
        static::observe(new CollectionObserver);
    }
}