<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\TransactionWithReferenceObserver;

trait HasReferenceRemainingQtyProcess
{
    public static function bootHasReferenceRemainingQtyProcess()
    {
        static::observe(new TransactionWithReferenceObserver);
    }
}