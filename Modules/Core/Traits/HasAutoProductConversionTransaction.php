<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\TransactionGenerateAutoProductConversionObserver;

trait HasAutoProductConversionTransaction
{
    public static function bootHasAutoProductConversionTransaction()
    {
        static::observe(new TransactionGenerateAutoProductConversionObserver);
    }
}