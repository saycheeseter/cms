<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryAdjustDetailObserver;

trait HasAdjustmentDetailCustomProcess
{
    public static function bootHasAdjustmentDetailCustomProcess()
    {
        static::observe(new InventoryAdjustDetailObserver);
    }
}