<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\SummaryPendingQuantityTransactionObserver;

trait HasSummaryPendingQuantityComputation
{
    public static function bootHasSummaryPendingQuantityComputation()
    {
        static::observe(new SummaryPendingQuantityTransactionObserver);
    }
}