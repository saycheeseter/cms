<?php

namespace Modules\Core\Traits;

use Modules\Core\Entities\TransactionSerialNumberLogs;
use Modules\Core\Observers\TransactionSerialNumberLogsObserver;

trait HasSerialNumberLogs
{
    public static function bootHasSerialNumberLogs()
    {
        static::observe(new TransactionSerialNumberLogsObserver);
    }

    public function serialLogs()
    {
        return $this->morphMany(TransactionSerialNumberLogs::class, 'transaction');
    }
}