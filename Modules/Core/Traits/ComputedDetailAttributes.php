<?php

namespace Modules\Core\Traits;

use Modules\Core\Enums\TransactionStatus;

use Litipk\BigNumbers\Decimal;

trait ComputedDetailAttributes
{
    public function getComputedUnitIdAttribute()
    {
        if($this->remaining_qty->mod($this->unit_qty)->equals(Decimal::create(0, setting('monetary.precision'))) 
            || $this->transaction->transaction_status == TransactionStatus::NO_DELIVERY){
            return $this->unit_id;
        }

        $units = $this->units->sortByDesc('qty');
        $unitId = null;

        foreach ($units as $unit) {
            if($this->remaining_qty->mod($unit->qty)->equals(Decimal::create(0, setting('monetary.precision')))) {
                $unitId = $unit->uom_id;
                break;
            }
        }

        if(is_null($unitId)) {
            $unitId = $units->last()->uom_id;
        }

        return $unitId;
    }

    public function getComputedUnitQtyAttribute()
    {
        if($this->remaining_qty->mod($this->unit_qty)->equals(Decimal::create(0, setting('monetary.precision'))) 
            || $this->transaction->transaction_status == TransactionStatus::NO_DELIVERY){
            return $this->unit_qty;
        }

        $units = $this->units->sortByDesc('qty');
        $unitQty = null;

        foreach ($units as $unit) {
            if($this->remaining_qty->mod($unit->qty)->equals(Decimal::create(0, setting('monetary.precision')))) {
                $unitQty = $unit->qty;
                break;
            }
        }

        if(is_null($unitQty)) {
            $unitQty = $units->last()->qty;
        }

        return $unitQty;
    }

    public function getComputedQtyAttribute()
    {
        if($this->remaining_qty->mod($this->unit_qty)->equals(Decimal::create(0, setting('monetary.precision'))) 
            || $this->transaction->transaction_status == TransactionStatus::NO_DELIVERY){
            return $this->remaining_qty->div($this->unit_qty);
        }

        $units = $this->units->sortByDesc('qty');
        $qty = null;

        foreach ($units as $unit) {
            if($this->remaining_qty->mod($unit->qty)->equals(Decimal::create(0, setting('monetary.precision')))) {
                $qty = $this->remaining_qty->div($unit->qty);
                break;
            }
        }

        if(is_null($qty)) {
            $qty = $this->remaining_qty;
        }

        return $qty;
    }

    public function getComputedTotalQtyAttribute()
    {
        return $this->computed_qty->mul($this->computed_unit_qty, setting('monetary.precision'));
    }

    public function getComputedAmountAttribute()
    {
        return $this->price->mul($this->computed_total_qty, setting('monetary.precision'));
    }
}