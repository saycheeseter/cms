<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\CollectionDetailObserver;

trait HasCollectionDetailProcess
{
    public static function bootHasCollectionDetailProcess()
    {
        static::observe(new CollectionDetailObserver);
    }
}