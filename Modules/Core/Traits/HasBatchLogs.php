<?php

namespace Modules\Core\Traits;

use Modules\Core\Entities\TransactionBatchLog;
use Modules\Core\Observers\TransactionBatchLogObserver;

trait HasBatchLogs
{
    public static function bootHasBatchLogs()
    {
        static::observe(new TransactionBatchLogObserver);
    }

    public function batchLogs()
    {
        return $this->morphMany(TransactionBatchLog::class, 'transaction');
    }
}