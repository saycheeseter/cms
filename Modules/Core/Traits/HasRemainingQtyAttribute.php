<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\TransactionDetailRemainingQtyObserver;

trait HasRemainingQtyAttribute
{
    public static function bootHasRemainingQtyAttribute()
    {
        static::observe(new TransactionDetailRemainingQtyObserver);
    }
}