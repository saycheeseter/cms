<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryAdjustObserver;

trait HasAdjustmentCustomProcess
{
    public static function bootHasAdjustmentCustomProcess()
    {
        static::observe(new InventoryAdjustObserver);
    }
}