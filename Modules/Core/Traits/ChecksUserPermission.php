<?php

namespace Modules\Core\Traits;

use Illuminate\Auth\Access\AuthorizationException;
use Auth;

trait ChecksUserPermission
{
    protected function checkPermission($alias)
    {
        if (!Auth::user()->hasAccessTo($alias)) {
            throw new AuthorizationException('This action is unauthorized.');
        }
    }
}