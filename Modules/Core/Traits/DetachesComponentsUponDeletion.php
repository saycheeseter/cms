<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryFlowDetailComponentsObserver;

trait DetachesComponentsUponDeletion
{
    public static function bootDetachesComponentsUponDeletion()
    {
        static::observe(new InventoryFlowDetailComponentsObserver);
    }
}