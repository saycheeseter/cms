<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\GenerateTransactionProductMonthlySummaryObserver;

trait GenerateTransactionProductMonthlySummary
{
    public static function bootGenerateTransactionProductMonthlySummary()
    {
        static::observe(new GenerateTransactionProductMonthlySummaryObserver);
    }
}