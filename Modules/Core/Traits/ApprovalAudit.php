<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ApprovalAuditObserver;

trait ApprovalAudit
{
    public static function bootApprovalAudit()
    {
        static::observe(new ApprovalAuditObserver());
    }
}