<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\BranchObserver;

trait HasBranchInitialDataProcess
{
    public static function bootHasBranchInitialDataProcess()
    {
        static::observe(new BranchObserver);
    }
}