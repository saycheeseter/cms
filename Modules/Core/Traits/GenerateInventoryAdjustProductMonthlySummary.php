<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\GenerateInventoryAdjustProductMonthlySummaryObserver;

trait GenerateInventoryAdjustProductMonthlySummary
{
    public static function bootGenerateInventoryAdjustProductMonthlySummary()
    {
        static::observe(new GenerateInventoryAdjustProductMonthlySummaryObserver());
    }
}