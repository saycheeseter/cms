<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\PurchaseInboundSettingsObserver;

trait HasPurchaseInboundSettingsProcess
{
    public static function bootHasPurchaseInboundSettingsProcess()
    {
        static::observe(new PurchaseInboundSettingsObserver);
    }
}