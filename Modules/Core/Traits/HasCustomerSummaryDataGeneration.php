<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\CustomerObserver;

trait HasCustomerSummaryDataGeneration
{
    public static function bootHasCustomerSummaryDataGeneration()
    {
        static::observe(new CustomerObserver);
    }
}