<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ProductConversionDetailObserver;

trait HasCustomConversionDetailProcess
{
    public static function bootHasCustomConversionDetailProcess()
    {
        static::observe(new ProductConversionDetailObserver);
    }
}