<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryChainObserver;

trait HasInventoryAndManagementProcess
{
    public static function bootHasInventoryAndManagementProcess()
    {
        static::observe(new InventoryChainObserver);
    }
}