<?php

namespace Modules\Core\Traits;

trait GeneratesRule
{
    protected function rule()
    {
        $args = func_get_args();

        $rule = resolve($args[1]);
        $resource = $args[0];
        $arguments = [];

        for ($i=2; $i < func_num_args(); $i++) { 
            $arguments[] = $args[$i];
        }

        if (!method_exists($rule, $resource)) {
            return $rule;
        }

        return call_user_func_array([$rule, $resource], $arguments);
    }
}