<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ProductBatchDeliveryInboundReferenceObserver;

trait HasProductBatchDeliveryInboundReferenceRemainingComputation
{
    public static function bootHasProductBatchDeliveryInboundReferenceRemainingComputation()
    {
        static::observe(new ProductBatchDeliveryInboundReferenceObserver);
    }
}