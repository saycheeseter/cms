<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\InventoryFlowDetailTotalTransactionAmountObserver;

trait HasTransactionTotalAmountComputation
{
    public static function bootHasTransactionTotalAmountComputation()
    {
        static::observe(new InventoryFlowDetailTotalTransactionAmountObserver);
    }
}