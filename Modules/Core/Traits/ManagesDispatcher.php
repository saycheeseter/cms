<?php

namespace Modules\Core\Traits;

use Modules\Core\Entities\Base;

trait ManagesDispatcher
{
    protected $dispatcher;

    /**
     * Disable all models events
     * 
     * @return $this
     */
    protected function disableDispatcher()
    {
        $this->dispatcher = Base::getEventDispatcher();

        Base::unsetEventDispatcher();

        return $this;
    }

    /**
     * Enable all model events
     * 
     * @return $this
     */
    protected function enableDispatcher()
    {
        Base::setEventDispatcher($this->dispatcher);
        
        return $this;
    }
}