<?php

namespace Modules\Core\Traits;

use Litipk\BigNumbers\Decimal;
use App;
use Carbon\Carbon;
use DateTime;
use Auth;
use Modules\Core\Services\Contracts\UserDefinedFieldStructureInterface;

trait HasAttributes
{
    protected $udfs = null;

    protected $fields = null;

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'real':
            case 'float':
            case 'double':
                return (float) $value;
            case 'string':
                return (string) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                return new BaseCollection($this->fromJson($value));
            case 'date':
                return $this->asDate($value);
            case 'datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimestamp($value);
            case 'decimal':
                return Decimal::create(app('NumberFormatter')->parse($value), setting('monetary.precision'));
            default:
                return $value;
        }
    }

    /**
     * Get the list of udf columns in the corresponding model
     * 
     * @return array
     */
    public function getUDFColumns()
    {
        return $this->udfs = App::make(UserDefinedFieldStructureInterface::class)->getColumns($this->getTable());
    }

    /**
     * Override hasCast behaviour to check first if field is a udf
     * 
     * @param  string  $key
     * @param  string  $types
     * @return boolean
     */
    public function hasCast($key, $types = null)
    {
        if ($this->isUDFColumn($key) && is_null($types)) {
            return true;
        }

        return parent::hasCast($key, $types);
    }

    /**
     * Check if the specified key is a udf
     * 
     * @param  string  $key
     * @return boolean
     */
    public function isUDFColumn($key)
    {
        return array_key_exists($key, $this->getUDFColumns());
    }
    
    /**
     * Override getCastType behaviour and determine if field is a udf and get the udf cast type
     * based on the column data type in DB
     * 
     * @param  string $key
     * @return string
     */
    protected function getCastType($key)
    {
        if ($this->isUDFColumn($key)) {
            return $this->udfs[$key];
        }

        return parent::getCastType($key);
    }

    /**
     * Override standard date time format to check if its an Instance of Carbon or Datetime Interface
     * 
     * @param  string  $value
     * @return boolean
     */
    protected function isStandardDateFormat($value)
    {
        return (!($value instanceof Carbon) && !($value instanceof DateTime))
            ? parent::isStandardDateFormat($value)
            : false;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if ($this->hasCast($key) && $this->getCastType($key) === 'decimal') {
            $value = app('NumberFormatter')->parse($value);
        }

        if ($this->isUDFColumn($key)) {
            $this->setUDFAttribute($key, $value);
        } else {
            parent::setAttribute($key, $value);
        }
    }


    /**
     * Set UDF attribute format. 
     * 
     * @param string $key
     * @param string $value
     */
    protected function setUDFAttribute($key, $value)
    {
        $type = $this->getUDFColumns()[$key];

        switch ($type) {
            case 'date':
            case 'datetime':
                $value = $value instanceof DateTime
                    ? Carbon::instance($value)
                    : Carbon::parse($value);
                break;
        }

        $this->attributes[$key] = $value;
    }

    /**
     * Determine if the new and old values for a given key are equivalent.
     *
     * @param  string $key
     * @param  mixed  $current
     * @return bool
     */
    protected function originalIsEquivalent($key, $current)
    {
        if (! array_key_exists($key, $this->original)) {
            return false;
        }

        $original = $this->getOriginal($key);

        if (!is_null($current) && $this->hasCast($key) && $this->getCastType($key) === 'decimal') {
            return $this->castAttribute($key, $current)->equals($this->castAttribute($key, $original));
        }

        return parent::originalIsEquivalent($key, $current);
    }

    /**
     * Convert a DateTime to a storable string.
     * SQL Server will not accept 4 digit second fragment (PHP default: see getDateFormat Y-m-d H:i:s.u)
     * trim three digits off the value returned from the parent.
     *
     * @param  \DateTime|int  $value
     * @return string
     */
    public function fromDateTime($value)
    {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    /**
     * Parse the list of current attributes and return it
     *
     * @param null $key
     * @return mixed
     */
    public function getParsedAttributes($key = null)
    {
        return $this->parseGivenAttributes($this->attributes, $key);
    }

    /**
     * Parse the list of original attributes and return it
     *
     * @param null $key
     * @return mixed
     */
    public function getParsedOriginal($key = null)
    {
        return $this->parseGivenAttributes($this->original, $key);
    }

    protected function parseGivenAttributes($data, $key)
    {
        foreach ($data as $attribute => $value) {

            if(!array_key_exists($attribute, $this->casts)) {
                continue;
            }

            switch ($this->getCastType($attribute)) {
                case 'decimal':
                    $value = (string) Decimal::create(app('NumberFormatter')->parse($value), setting('monetary.precision'));
                    break;
            }

            $data[$attribute] = $value;
        }

        return ! is_null($key) && isset($data[$key])
            ? $data[$key]
            : $data;
    }

    /**
     * Add the casted attributes to the attributes array. (Override)
     *
     * @param  array  $attributes
     * @param  array  $mutatedAttributes
     * @return array
     */
    protected function addCastAttributesToArray(array $attributes, array $mutatedAttributes)
    {
        foreach ($this->getCasts() as $key => $value) {
            if (! array_key_exists($key, $attributes) || in_array($key, $mutatedAttributes)) {
                continue;
            }

            // Here we will cast the attribute. Then, if the cast is a date or datetime cast
            // then we will serialize the date for the array. This will convert the dates
            // to strings based on the date format specified for these Eloquent models.
            $attributes[$key] = $this->castAttribute(
                $key, $attributes[$key]
            );

            // If the attribute cast was a date or a datetime, we will serialize the date as
            // a string. This allows the developers to customize how dates are serialized
            // into an array without affecting how they are persisted into the storage.
            if ($attributes[$key] &&
                ($value === 'date' || $value === 'datetime')) {
                $attributes[$key] = $this->serializeDate($attributes[$key]);
            } else if ($attributes[$key] && $value === 'decimal') {
                $attributes[$key] = (string) $attributes[$key];
            }
        }

        return $attributes;
    }
}