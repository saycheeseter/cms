<?php

namespace Modules\Core\Traits;

use Modules\Core\Extensions\Database\Query\Builder as QueryBuilder;
use Modules\Core\Extensions\Database\Eloquent\Builder;

trait HasBuilders
{
    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Modules\Core\Extensions\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        return new QueryBuilder(
            $connection, $connection->getQueryGrammar(), $connection->getPostProcessor()
        );
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Modules\Core\Extensions\Database\Query\Builder  $query
     * @return \Modules\Core\Extensions\Database\Query\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
}