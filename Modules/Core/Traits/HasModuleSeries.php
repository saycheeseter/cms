<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\ModuleSeriesObserver;

trait HasModuleSeries
{
    /**
     * Module Series boot logic.
     *
     * @return void
     */
    public static function bootHasModuleSeries()
    {
        static::observe(ModuleSeriesObserver::class);
    }

    public function getSeriesTypeAttribute()
    {
        return $this->getMorphMapKey(static::class);
    }
}