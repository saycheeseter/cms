<?php

namespace Modules\Core\Traits;

use Modules\Core\Observers\PaymentDetailObserver;

trait HasPaymentDetailProcess
{
    public static function bootHasPaymentDetailProcess()
    {
        static::observe(new PaymentDetailObserver);
    }
}