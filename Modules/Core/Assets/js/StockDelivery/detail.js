import Invoice from '../Invoice/detail.js';
import StockDeliveryService from '../Services/StockDeliveryService';
import StockRequestService from '../Services/StockRequestService';
import Outbound from '../Common/OutboundMixins.js';
import BranchService from '../Services/BranchService';
import PurchaseInboundService from '../Services/PurchaseInboundService';
import DeliveryProductSelectorAPIMixins from "../Common/DeliveryProductSelectorAPIMixins";
import LocalCache from "../Classes/LocalCache";
import SalesOutboundImportMixins from "../Common/SalesOutboundImportMixins";

let app = new Vue({
    el: '#app',

    mixins: [Invoice, Outbound, DeliveryProductSelectorAPIMixins, SalesOutboundImportMixins],

    data: {
        buffer: {
            data: {
                reference_id: null,
                transaction_type: TRANSACTION_TYPE.DIRECT
            },
            reference: {
                requests: []
            }
        },

        reference: {
            deliveryLocations: [],
            requests: []
        },

        importing: {
            purchaseInbound: {
                reference: ''
            }
        },

        transactionable: {
            visible: false,
            details: []
        },

        attachment: {
            module: MODULE.STOCK_DELIVERY
        },

        service: new StockDeliveryService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_DELIVERY,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.STOCK_DELIVERY
        },

        selector: {
            invoice: {
                visible: false,
                filter: {
                    visible: false,
                    options: [
                        { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                        { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                        {
                            name: Lang.t('label.transaction_date_time'),
                            columnName: 'transaction_date',
                            type: 'datetime',
                            optionBool: false,
                            limit: 2
                        },
                    ],
                    data: []
                },
                datatable: {
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withActionCell: false,
                        withPaginateCallBack: true
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: [],
                },
                columnSettings: {
                    columns: [
                        { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                        { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                        { alias: 'created_from', label: Lang.t('label.created_from') },
                        { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                        { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                        { alias: 'amount', label: Lang.t('label.amount'), default: true },
                        { alias: 'transaction_status', label: Lang.t('label.transaction_status'), default: true },
                        { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                        { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                        { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                        { alias: 'created_date', label: Lang.t('label.created_date') },
                        { alias: 'audited_by', label: Lang.t('label.audit_by') },
                        { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                        { alias: 'deleted', label: Lang.t('label.deleted') },
                    ],
                    visible: false,
                    selections: {},
                    module: {
                        id: MODULE.STOCK_REQUEST,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            deliver_to: null,
            deliver_to_location: null,
            transaction_type: TRANSACTION_TYPE.DIRECT,
            reference_id: null,
        });
    },

    methods: {
        revertInfo() {
            this.reference.requests = this.buffer.reference.requests;
            this.info.set(this.buffer.data);
            this.warning.visible = false;
            this.action.reset();
        },

        getDeliveryLocations(def = null) {
            let that = this;

            this.processing = true;

            BranchService.details(this.info.deliver_to)
                .then(function (response) {
                    that.reference.deliveryLocations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.deliver_to_location = location;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getStockRequest(string) {
            let that = this;

            if (this.processing) {
                return false;
            }

            this.processing = true;

            return StockRequestService.instance().remaining({
                sheet_number: string,
                branch: this.info.created_for
            })
            .then(function (response) {
                that.reference.requests = response.data.values.invoices;
                that.processing = false;
                that.action.reset();
            })
            .catch(function (error) {
                that.processing = false;
                that.action.reset();
            });
        },

        retrieve() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            StockRequestService.instance().retrieve(this.info.reference_id)
                .then(function (response) {
                    that.processing = false;
                    that.parseStockRequestTransaction(response.data.values);
                    that.checkTransactionForInventoryWarning();
                    that.action.reset();
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.action.reset();
                });
        },

        clearStockRequests() {
            this.info.reference_id = null;
            this.reference.requests = [];
        },

        onSuccessfulLoad(response) {
            this.reference.requests = response.data.values.requests;
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        loadCache() {
            this.cache.modal = false;

            if (!this.hasLocalCache(this.info.id)) {
                return false;
            }

            let cached = LocalCache.get(this.cache.module);

            this.reference.requests = cached.requests;
            this.info.set(cached.info);
            this.datatable.details.value.data = cached.details;
            this.onCacheLoaded();
        },

        cacheData() {
            let data = Object.assign({}, this.data, {
                requests: this.reference.requests
            });

            LocalCache.set(this.cache.module, data);
        },

        onCacheLoaded() {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        setTotalQty(index) {
            let row = this.datatable.details.value.data[index];

            row['total_qty'] =  Numeric.format(
                Numeric.parse(row['unit_qty']) *
                Numeric.parse(row['qty'])
            );

            this.setAmount(index);
            this.setSpare(index);
        },

        setSpare(index) {
            let row = this.datatable.details.value.data[index];

            if(this.approvedTransaction){
                row['spare_qty'] = Numeric.format(row['remaining_qty']);
            } else {
                row['spare_qty'] = Numeric.format(row['remaining_qty'] - row['total_qty']);
            }
        },

        setControlsAndSelection() {
            if (this.direct) {
                this.clearStockRequests();
                this.controls.create = true;
            } else {
                this.controls.create = false;
            }

            this.action.reset();
        },

        getPurchaseInboundTransaction() {
            if (this.processing || this.importing.purchaseInbound.reference === '') {
                return false;
            }

            let that = this;

            this.processing = true;

            PurchaseInboundService.instance().findApprovedTransactionByReference(this.importing.purchaseInbound.reference)
                .then(function (response){
                    that.processing = false;
                    that.parsePurchaseInboundTransaction(response.data.values);
                    that.checkTransactionForInventoryWarning();
                    that.importing.purchaseInbound.reference = '';
                    that.action.reset();
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        formatStockRequestResponseToTransactionDetails(details) {
            return this.formatAttributesToTransactionDetails(details, {}, function(formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0,
                    reference_id: detail.id,
                    invoice_qty: Numeric.format(Numeric.parse(detail.qty) * Numeric.parse(detail.unit_qty))
                });
            });
        },

        parsePurchaseInboundTransaction(data) {
            let transaction = data.transaction;

            if (transaction.transactionable_type === 'stock_request') {
                this.info.deliver_to = transaction.transactionable.created_for;
                this.getDeliveryLocations(transaction.transactionable.for_location);

                this.info.transaction_type = TRANSACTION_TYPE.FROM_INVOICE;
                this.reference.requests = [{
                    id: transaction.transactionable_id,
                    label: transaction.transactionable.sheet_number
                }];

                this.info.reference_id = transaction.transactionable_id;
            }

            this.datatable.details.value.data = [];
            this.datatable.details.value.data = this.formatPurchaseInboundToTransactionDetails(data);
        },

        parseStockRequestTransaction(transaction) {
            this.info.deliver_to = transaction.invoice.created_for;
            this.info.deliver_to_location = transaction.invoice.for_location;
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.datatable.details.value.data = [];
            this.datatable.details.value.data = this.formatStockRequestResponseToTransactionDetails(transaction.details.data);
        },

        formatPurchaseInboundToTransactionDetails(data) {
            let transaction = data.transaction;
            let details = data.details.data;

            return this.formatAttributesToTransactionDetails(details, {}, function(formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0,
                    reference_id: detail.transactionable_type === 'stock_request_detail'
                        ? detail.transactionable_id
                        : null,
                    invoice_qty: detail.transactionable_type === 'stock_request_detail'
                        ? Numeric.format(Numeric.parse(detail.qty) * Numeric.parse(detail.unit_qty))
                        : 0,
                    remaining_qty: detail.transactionable_type === 'stock_request_detail'
                        ? detail.transactionable.remaining_qty
                        : 0,
                    spare_qty: detail.transactionable_type === 'stock_request_detail'
                        ? Numeric.format(
                            Numeric.parse(detail.transactionable.remaining_qty)
                            - (Numeric.parse(detail.qty) * Numeric.parse(detail.unit_qty))
                          )
                        : 0,
                    transactionable_type: 'purchase_inbound_detail',
                    transactionable_id: detail.id,
                    references: {
                        id: transaction.id,
                        sheet_number: transaction.sheet_number
                    }
                });
            });
        },

        viewPurchaseInbound(id) {
            PurchaseInboundService.instance().edit(id);
        },

        formatAttributesToTransactionDetails(details, override = {}, hook = null) {
            let collection = [];

            details = Arr.wrap(details);

            for (let i = 0; i < details.length; i++) {
                let detail = details[i];

                let formatted = {
                    id: detail.hasOwnProperty('id') ? detail.id : 0,
                    product_id: detail.hasOwnProperty('product_id') ? detail.product_id : 0,
                    barcode_list: detail.barcode_list,
                    barcode: detail.barcode,
                    name: detail.name,
                    stock_no: detail.stock_no,
                    chinese_name: detail.chinese_name,
                    units: detail.units,
                    unit_id: detail.unit_id,
                    unit_qty: detail.unit_qty,
                    old_unit: detail.hasOwnProperty('old_unit') ? detail.old_unit : true,
                    qty: detail.hasOwnProperty('qty') ? detail.qty : Numeric.format(0),
                    price: detail.price,
                    cost: detail.cost,
                    oprice: detail.hasOwnProperty('oprice') ? detail.oprice : detail.price,
                    discount1: detail.hasOwnProperty('discount1') ? detail.discount1 : Numeric.format(0),
                    discount2: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    discount3: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    discount4: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    remarks: detail.hasOwnProperty('remarks') ? detail.remarks : '',
                    group_type: detail.group_type,
                    components: detail.components,
                    manageable: detail.manageable,
                    product_type: detail.product_type,
                    reference_id: detail.hasOwnProperty('reference_id') ? detail.reference_id : null,
                    invoice_qty: detail.hasOwnProperty('invoice_qty') ? detail.invoice_qty : Numeric.format(0),
                    remaining_qty: detail.hasOwnProperty('remaining_qty') ? detail.remaining_qty : Numeric.format(0),
                    spare_qty: detail.hasOwnProperty('spare_qty') ? detail.spare_qty : Numeric.format(0),
                    transactionable_type: detail.hasOwnProperty('transactionable_type') ? detail.transactionable_type : null,
                    transactionable_id: detail.hasOwnProperty('transactionable_id') ? detail.transactionable_id : null,
                    references: detail.hasOwnProperty('references') ? detail.references : null
                };

                formatted['total_qty'] = Numeric.format(
                    Numeric.parse(formatted['unit_qty']) *
                    Numeric.parse(formatted['qty'])
                );

                formatted['amount'] = Numeric.format(
                    Numeric.parse(formatted['total_qty']) *
                    Numeric.parse(formatted['price'])
                );

                if (hook !== null) {
                    formatted = hook(formatted, detail);
                }

                collection.push(Object.assign({}, formatted, override));
            }

            return collection;
        },

        getInvoicesForSelector(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.selector.invoice.filter.visible = false;

            let filters = this.selector.invoice.filter.data.concat([
                {
                    value: this.info.created_for,
                    operator: '=',
                    join: 'and',
                    column: 'request_to'
                },
                {
                    value: APPROVAL_STATUS.APPROVED,
                    operator: '=',
                    join: 'and',
                    column: 'approval_status'
                },
                {
                    value: VOIDED.NO,
                    operator: '=',
                    join: 'and',
                    column: 'deleted_at'
                },
                {
                    value: TRANSACTION_STATUS.COMPLETE,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },

                {
                    value: TRANSACTION_STATUS.EXCESS,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },
            ]);

            StockRequestService.instance().paginate({
                per_page: this.selector.invoice.datatable.settings.perPage,
                page: page,
                filters: filters
            })
            .then(function (response) {
                that.processing = false;
                that.selector.invoice.datatable.value.data = response.data.values.invoices;
                that.selector.invoice.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (response) {
                that.processing = false;
            });
        },

        showInvoiceSelector() {
            this.selector.invoice.visible = true;
            this.getInvoicesForSelector();
        },

        importInvoiceFromSelector(reference_id, sheet_number) {
            let that = this;

            this.selector.invoice.visible = false;

            this.reference.requests = [
                { id: reference_id, label: sheet_number }
            ];

            this.info.reference_id = reference_id;
            this.retrieve();
        },
    },

    watch: {
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.reference.requests = this.reference.requests;
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.reference_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.transaction_type = this.info.transaction_type;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.transaction_type = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.reference.requests = this.reference.requests;
        },
        'action.value': function(action) {
            switch(action) {
                case 'transaction-type':
                    this.setControlsAndSelection();
                    break;

                case 'import-purchase-inbound':
                    this.getPurchaseInboundTransaction();
                    break;
            }
        }
    },

    computed: {
        direct: function() {
            return Number(this.info.transaction_type) === TRANSACTION_TYPE.DIRECT;
        },
        fromInvoice: function() {
            return Number(this.info.transaction_type) === TRANSACTION_TYPE.FROM_INVOICE;
        },
        showBarcodeStockNoInput: function() {
            return !this.disabled && this.controls.create && this.info.transaction_type === TRANSACTION_TYPE.DIRECT;
        },
        enableSalesOutboundImport: function() {
            return this.fullyEnabled && this.direct;
        },
    }
});