import CoreMixins from '../Common/CoreMixins';

export default {
    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: false,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    { 
                        name: Lang.t('label.transaction_date'), 
                        columnName: 'transaction_date', 
                        type:'dateRange',
                        limit: 1,
                        inputVal: {
                            start: Moment().startOf('day').format(),
                            end: Moment().add(1, 'day').startOf('day').format()
                        }
                    },
                    { 
                        name: Lang.t('label.flow_type'), 
                        columnName: 'flow_type', 
                        type: 'select', 
                        selectOptions: [
                            { id: FLOW_TYPE.IN, label: Lang.t('label.in') },
                            { id: FLOW_TYPE.OUT, label: Lang.t('label.out') }
                        ],
                        limit: 1
                    },
                ],
                data: []
            }
        },
        
        message: new Notification,
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            Promises.push(
                this.service.paginate({
                    filters: that.modal.filter.data,
                })
                .then(function (response) {
                    that.processing = false;
                    that.message.info = response.data.message;
                    that.datatable.value.pagination.current_page = 1;
                    that.createRunningBalance(response.data.values.records, response.data.values.beginning);
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
            );
        },

        createRunningBalance(data, beg) {
            let beginning = Numeric.parse(beg.amount);
            
            let running = beginning;
            let records = [];

            for (var i = 0; i < data.length; i++) {
                let current = data[i];

                running += (Numeric.parse(current.in) - Numeric.parse(current.out));

                current['balance'] = Numeric.format(running);

                records.push(current);
            }

            this.datatable.value.data = records;

            this.createInitialBalance(beginning);
        },

        createInitialBalance(beginning) {
            this.datatable.value.data.unshift({
                date: '',
                particular: Lang.t('label.beginning'),
                in: Numeric.format(0),
                out: Numeric.format(0),
                balance: Numeric.format(beginning)
            });
        }
    },

    computed: {
        totalIn: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['in']);
            }

            return Numeric.format(total);
        },
        totalOut: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['out']);
            }

            return Numeric.format(total);
        },
        total: function() {
            return Numeric.format(Numeric.parse(this.totalIn) - Numeric.parse(this.totalOut));
        }
    }
}