window.COMPARISON_OPERATOR = {
    EQUAL: {
        name: '=', symbol: '='
    },
    LESS_THAN: {
        name: '<', symbol: '<'
    },
    GREATER_THAN: {
        name: '>', symbol: '>'
    },
    LESS_THAN_EQUAL: {
        name: '<=', symbol: '<='
    },
    GREATER_THAN_EQUAL: {
        name: '>=', symbol: '>='
    },
    NOT_EQUAL: {
        name: '<>', symbol: 'not equal'
    },
    CONTAINS: {
        name: 'contains', symbol: 'contains'
    },
    LEFT_CONTAINS: {
        name: 'startsWith', symbol: 'starts with'
    },
    RIGHT_CONTAINS: {
        name: 'endsWith', symbol: 'ends with'
    },
    NOT_CONTAINS: {
        name: 'notContains', symbol: 'not contains'
    }
}

window.CONDITIONAL_OPERATOR = {
    AND: {
        name: 'and', symbol: 'AND'
    },
    OR: {
        name: 'or', symbol: 'OR'
    }
}

window.SELLING_PRICE_BASED_ON = {
    HISTORICAL: 1,
    WHOLESALE: 2,
    RETAIL: 3,
    PERCENT_PURCHASE: 4,
    PERCENT_WHOLESALE: 5,
    PRICE_A: 6,
    PRICE_B: 7,
    PRICE_C: 8,
    PRICE_D: 9,
    PRICE_E: 10
}

window.APPROVAL_STATUS = {
    NEW: 0,
    DRAFT: 1,
    FOR_APPROVAL: 2,
    APPROVED: 3,
    DECLINED: 4
}

window.TRANSACTION_STATUS = {
    COMPLETE: 1,
    INCOMPLETE: 2,
    EXCESS: 3,
    NO_DELIVERY: 4
}

window.VOIDED = {
    YES: 1,
    NO: 0
}

window.PRODUCT_STATUS = {
    INACTIVE: 0,
    ACTIVE: 1,
    DISCONTINUED: 2
}

window.INVENTORY_STATUS = {
    NEGATIVE: 0,
    NO_INVENTORY: 1,
    WITH_INVENTORY: 2
}

window.PRICES = {
    PURCHASE: 1,
    SELLING: 2,
    WHOLESALE: 3,
    A: 4,
    B: 5,
    C: 6,
    D: 7,
    E: 8,
}

window.CUSTOMER = {
    WALK_IN: 0,
    REGULAR: 1
}

window.WALK_IN_PRICE_SETTINGS = {
    WHOLESALE: 0,
    RETAIL: 1
}

window.TRANSACTION_TYPE = {
    DIRECT: 0,
    FROM_INVOICE: 1
}

window.PRICE_SCHEME = {
    DEFAULT: 0,
    CUSTOMER: 1,
    SUPPLIER: 2,
    INVENTORY: 3
}

window.SETUP = {
    ENABLED: 1,
    VIEW: 2
}

window.FLOW_TYPE = {
    IN: 0,
    OUT: 1
}

window.VIEW_PRICES = {
    BRANCH_ONLY: 0,
    ALL_BRANCHES: 1
}

window.PAYMENT_METHOD = {
    CASH: 2,
    CHECK: 3,
    BANK_DEPOSIT: 4,
    FROM_BALANCE: 5
}

window.MODULE = {
    PURCHASE: 1,
    PURCHASE_RETURN: 2,
    DAMAGE: 3,
    SALES: 4,
    SALES_RETURN: 5,
    STOCK_REQUEST: 6,
    STOCK_DELIVERY: 7,
    STOCK_RETURN: 8,
    PURCHASE_INBOUND: 9,
    PURCHASE_RETURN_OUTBOUND: 10,
    DAMAGE_OUTBOUND: 11,
    SALES_OUTBOUND: 12,
    SALES_RETURN_INBOUND: 13,
    STOCK_DELIVERY_OUTBOUND: 14,
    STOCK_DELIVERY_INBOUND: 15,
    STOCK_RETURN_OUTBOUND: 16,
    STOCK_RETURN_INBOUND: 17,
    INVENTORY_ADJUST: 18,
    PRODUCT: 19,
    COUNTER: 20,
    PAYMENT: 21,
    USER: 22,
    COLLECTION: 23,
    PRODUCT_CONVERSION: 24,
    STOCK_DELIVERY_OUTBOUND_FROM: 25,
    STOCK_REQUEST_FROM: 26,
    BRANCH: 27,
    SUPPLIER: 28,
    CUSTOMER: 29,
    MEMBER: 30,
    PRODUCT_DISCOUNT: 31,
    RECEIVED_CHECK: 32,
    ISSUED_CHECK: 33,
    STOCK_CARD: 34,
    POS: 35
}

window.MODULE_ALIAS = {
    1: 'purchase',
    2: 'purchase_return',
    3: 'damage',
    4: 'sales',
    5: 'sales_return',
    6: 'stock_request',
    7: 'stock_delivery',
    8: 'stock_return',
    9: 'purchase_inbound',
    10: 'purchase_return_outbound',
    11: 'damage_outbound',
    12: 'sales_outbound',
    13: 'sales_return_inbound',
    14: 'stock_delivery_outbound',
    15: 'stock_delivery_inbound',
    16: 'stock_return_outbound',
    17: 'stock_return_inbound',
    18: 'inventory_adjust',
    19: 'product',
    20: 'counter',
    21: 'payment',
    22: 'user',
    23: 'collection'
}

window.COLUMN_TYPE = {
    TEXT: 1,
    NUMERIC: 2,
}

window.ITEM_MANAGEMENT_OWNER = {
    SUPPLIER: 1,
    CUSTOMER: 2,
    BRANCH_DETAIL: 3
}

window.SERIAL_TRANSACTION = {
    PURCHASE_INBOUND: 1,
    PURCHASE_RETURN_OUTBOUND: 2,
    DAMAGE_OUTBOUND: 3,
    SALES_OUTBOUND: 4,
    SALES_RETURN_INBOUND: 5,
    STOCK_DELIVERY_OUTBOUND: 6,
    STOCK_DELIVERY_INBOUND: 7,
    STOCK_RETURN_OUTBOUND: 8,
    STOCK_RETURN_INBOUND: 9,
    INVENTORY_ADJUST: 10,
}

window.SERIAL_STATUS = {
    NOT_AVAILABLE: 0,
    AVAILABLE: 1,
    ON_TRANSIT: 2
}

window.UDF_TYPE = {
    STR: 1,
    INTR: 2,
    DECIMAL: 3,
    DATE: 4,
    DATETIME: 5
}

window.PRODUCT_MANAGE_TYPE = {
    NONE: 0,
    SERIAL: 1,
    BATCH: 2
}

window.GIFT_CHECK_STATUS = {
    AVAILABLE: 1,
    REDEEMED: 2
}

window.CHECK_TRANSACTION_TYPE = {
    PAYMENT: 'payment_detail',
    COLLECTION: 'collection_detail',
    OTHER_PAYMENT: 'other_payment',
    OTHER_INCOME: 'other_income'
}

window.MENU_TYPE = {
    SECTION: 1,
    FOLDER: 2,
    MODULE: 3,
}

window.ACTIVE_MENU = {
    DEFAULT: 1,
    CUSTOM: 2
}

window.DISCOUNT_SCHEME = {
    SIMPLE: 1,
    BUY_X_FOR_PRICE_Y: 2,
    BUY_X_FOR_FIXED_PRICE_Y: 3
}

window.TARGET_TYPE = {
    ALL_CUSTOMERS: 1,
    ALL_MEMBERS: 2,
    SPECIFIC_MEMBER_RATE: 3,
    SPECIFIC_MEMBER: 4,
    SPECIFIC_CUSTOMER: 5
}

window.SELECT_TYPE = {
    ALL_PRODUCTS: 1,
    SPECIFIC_PRODUCT: 2,
    SPECIFIC_BRAND: 3,
    SPECIFIC_CATEGORY: 4,
    SPECIFIC_SUPPLIER: 5,
}

window.DISCOUNT_TYPE = {
    PERCENTAGE: 1,
    DEDUCT_AMOUNT: 2,
    DISCOUNTED_PRICE: 3
}

window.GROUP_TYPE = {
    NONE: 1,
    MANUAL_GROUP: 2,
    MANUAL_UNGROUP: 3,
    AUTOMATIC_GROUP: 4,
    AUTOMATIC_UNGROUP: 5
}

window.PRODUCT_TYPE = {
    REGULAR: 1,
    GROUP: 2,
    COMPONENT: 3,
    UNMANAGEABLE: 4
}

window.AUDIT_EVENT = {
    CREATED: 'created',
    UPDATED: 'updated',
    DELETED: 'deleted'
}

window.CHECK_STATUS = {
    PENDING: 1,
    TRANSFERRED: 2
}

window.WARNING_TYPE = {
    MIN: 1,
    MAX: 2
}

window.TRANSACTION_SOURCE = {
    WEB: 1,
    DATA_COLLECTOR: 2,
    CSV: 3
}

window.DATA_COLLECTOR_TRANSACTION = {
    PURCHASE_INBOUND: 1,
    PURCHASE_RETURN_OUTBOUND: 2,
    DAMAGE_OUTBOUND: 3,
    SALES_OUTBOUND: 4,
    SALES_RETURN_INBOUND: 5,
    STOCK_DELIVERY_OUTBOUND: 6,
    STOCK_RETURN_OUTBOUND: 7,
    INVENTORY_ADJUST: 8
}

window.TRANSACTION_SUMMARY_COMPUTATION = {
    AUTOMATIC: 1,
    MANUAL: 2
}
window.SYSTEM_CODE_DELETABLE = {
    NO: 0,
    YES: 1
}

window.USER_TYPE = {
    SUPERADMIN: 0,
    ADMIN: 1,
    DEFAULT: 2
}

window.BANK_TRANSACTION_TYPE = {
    DEPOSIT: 0,
    WITHDRAWAL: 1,
    TRANSFER: 2
}