import CoreMixins from '../Common/CoreMixins.js';
import ProductService from '../Services/ProductService';
import ProductMixins from '../Common/ProductMixins';
import StockRequestService from '../Services/StockRequestService';
import Attachment from '../Attachment/attachment';
import ColumnSettingsMixins from '../Common/ColumnSettingsMixins.js';
import BranchService from "../Services/BranchService";
import DataCollectorService from "../Services/DataCollectorService";
import LocalCache from "../Classes/LocalCache";

export default {
    mixins: [CoreMixins, ProductMixins, Attachment, ColumnSettingsMixins],

    components: {
        Datepicker,
        DateTimePicker
    },

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_GROUP
                    },
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_UNGROUP
                    },
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
        
        datatable: {
            details: {
                id: 'detail-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                }
            },
        },

        info: {},

        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),

        columnSettings: {
            details: {
                visible: false,
                columns: [],
                selections: {}
            }
        },

        format: {
            detail: {}, 
            info: {}
        },

        validation: {
            rules: {
                detail: {},
                info: {}
            },
            messages: {
                detail: {},
                info: {}
            }
        },

        controls: {
            draft: true,
            forApproval: false,
            approveDecline: false,
            revert: false,
            create: true,
        },

        warning: new Data({
            visible: false,
            value: false
        }),

        action: new Data({
            buffer: '',
            value: '',
            params: null
        }),

        reference: {
            users: users,
            branches: branches,
            locations: locations,
            approvalStatus: APPROVAL_STATUS,
            products: []
        },

        buffer: {
            data: {}
        },
      
        scan: {
            quantity: 1
        },
      
        dialog: {
            process: {
                message: ''
            },
            alternativeItems: {
                content: [],
                index: 0
            },
            directApproval: {
                message: '',
                visible: false
            }
        },

        message: new Notification(['transaction']),

        service: '',

        udfs: {
            data: udfs,
            type: UDF_TYPE
        },

        key: '',

        options: {
            setup: SETUP.ENABLED,
        },

        dataCollector: {
            referenceNumber: '',
            data: {},
            transactionType: '',
            visible: false,
            source: TRANSACTION_SOURCE
        },

        cache: {
            modal: false,
            module: 0
        },

        reload: false
    },

    mounted: function() {
        
        this.columnSettings.product.columns = this.removeFromColumnSettings(this.columnSettings.product.columns);

        this.load();
        this.createUDFFormat();
    },

    methods: {
        searchProductByName(value){
            let that = this;

            ProductService.searchProductByName({
                filters: that.filter.product.forced.concat({
                    column: 'name',
                    join: 'and',
                    operator: 'contains',
                    value: value
                })
            })
            .then(function(response) {
                that.reference.products = response.data.values;
            })
            .catch(function(error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        visibility() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.DRAFT:
                    this.controls.forApproval = true;
                    break;
                    
                case APPROVAL_STATUS.FOR_APPROVAL:
                    this.controls.draft = false;
                    this.controls.approveDecline = true;
                    break;

                case APPROVAL_STATUS.APPROVED: 
                    this.controls.draft = false;

                    if (Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.MANUAL) {
                        this.controls.revert = true;
                    }
                    break;

                case APPROVAL_STATUS.DECLINED:
                    this.controls.draft = false;
                    this.controls.revert = true;
                    break;
            }
        },

        load() {
            if(this.processing || id === 'create') {
                if (this.hasLocalCache()) {
                    this.loadCache();
                }

                return false;
            }

            this.processing = true;

            let that = this;
            
            this.service.show(id)
                .then(function (response){
                    that.processing = false;
                    that.info.set(response.data.values[that.key]);
                    that.getLocations(that.info.for_location);
                    that.datatable.details.value.data = that.formatAttributesToTransactionDetails(response.data.values.details.data);
                    that.setAttachmentProperties(id, response.data.values.attachments);

                    if (that.info.is_deleted) {
                        that.options.setup = SETUP.VIEW;
                    }

                    if (!that.forViewingOnly) {
                        that.visibility();
                    } else if (that.forViewingOnly) {
                        that.hideControls();
                    }

                    if(that.hasLocalCache(that.info.id)) {
                        that.cache.modal = true;
                    }

                    that.onSuccessfulLoad(response);
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getLocations(def = null) {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            BranchService.details(this.info.created_for)
                .then(function (response) {
                    that.reference.locations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.for_location = location;
                    that.action.reset();
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.locations = [];
                    that.processing = false;
                });
        },

        validate() {
            if (this.processing) {
                return false;
            }

            if(!this.finalized('detail')) {
                this.message.transaction.error.set([Lang.t('validation.finalize_all_rows')]);
                return false;
            }

            let validation = Validator.make(
                Format.make(this.info.data(), this.format.info), 
                this.validation.rules.info, 
                this.validation.messages.info
            );

            if (validation.fails()) {
                this.message.transaction.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if(!this.validateDetailQty()) {
                return false;
            }

            let callback = this.onSuccessfulValidation();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            return true;
        },

        process(reload = false) {
            this.reload = reload;

            this.action.value = 'process';
            this.action.params = reload;

            if (!this.validate() 
                || (this.info.id === 0 && ! permissions.create)
                || (this.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            if (this.info.id === 0) {
                this.store(this.data);
            } else {
                this.update(this.data);
            }
        },

        store(data) {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            this.cacheData();

            this.service.store(data)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }

                    that.info.id = response.data.values[that.key].id;
                    that.attachFiles(that.info.id);
                    that.clearCache();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        update(data) {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.message.reset();
            this.processing = true;
            this.cacheData();

            this.service.update(data, id)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }

                    that.clearCache();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        loadCache() {
            this.cache.modal = false;

            if (!this.hasLocalCache(this.info.id)) {
                return false;
            }

            let cached = LocalCache.get(this.cache.module);

            this.info.set(cached.info);
            this.datatable.details.value.data = cached.details;
            this.onCacheLoaded();
        },

        cacheData() {
            LocalCache.set(this.cache.module, this.data);
        },

        clearCache() {
            LocalCache.remove(this.cache.module);
        },

        updateApprovedTransaction() {
            if (this.processing) {
                return false;
            }

            let that = this;
            let data = this.getDataForUpdateApproved();

            this.message.reset();
            this.processing = true;
            this.cacheData();

            this.service.updateApproved(data.info, this.info.id)
                .then(function (response) {
                    that.clearCache();
                    that.dialog.process.message = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        getDataForUpdateApproved() {
            let data = Obj.copy(this.data);

            let included = {
                info: {},
                details: []
            };

            let keys = {
                info: [
                    'requested_by',
                    'remarks'
                ],
                details: []
            };

            let extras = {
                info: this.getExtraInfoForUpdateApproved(),
                details: []
            };

            keys.info = keys.info.concat(this.getUdfsKeys(), extras.info);

            for(let i in keys.info) {
                let key = keys.info[i];

                included.info[key] = data.info[key];
            }

            return included;
        },

        getExtraInfoForUpdateApproved() {
            return [];
        },

        getUdfsKeys() {
            let keys = [];

            for(let index in this.udfs.data) {
                let row = this.udfs.data[index];

                keys.push(row.alias);
            }

            return keys;
        },

        approval(status) {
            this.action.value = 'approval';
            this.action.params = status;

            if(this.processing || !this.validate()) {
                return false;
            }

            this.processing = true;
            
            let that = this;

            this.message.reset();
            
            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL: 
                    this.updateBeforeForApproval();
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        updateBeforeForApproval() {
            let that = this;

            this.cacheData();

            let update = this.service.update(this.data, that.info.id);

            axios.all([update])
                .then(axios.spread(function (update){
                    that.clearCache();
                    that.approvalRequest(APPROVAL_STATUS.FOR_APPROVAL);
                }))
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        approvalRequest(status) {
            let that = this;

            this.cacheData();

            this.service.approval({
                transactions: that.info.id,
                status: status
            })
            .then(function (response) {
                that.dialog.process.message = response.data.message;
                that.processing = false;
                that.clearCache();
            })
            .catch(function (error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
                that.clearCache();
            });
        },

        validateDetail(data, index, dataIndex) {
            let validation = Validator.make(
                Format.make(data, this.format.detail),
                this.validation.rules.detail, 
                this.validation.messages.detail
            );

            if (validation.fails()) {
                this.message.transaction.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (this.showBarcodeStockNoInput) {
                this.$refs.barcodeInput.focus();
            }
            
            this.changeState(index, 'destroy');
        },

        validateDetailQty() {

            let details = this.datatable.details.value.data;
            let errors =  [];

            for (var i = 0; i < details.length; i++) {
                if(Numeric.parse(details[i].qty) == 0) {
                    let row = parseInt(i) + parseInt(1);
                    errors.push(Lang.t('error.row') + ' [' + row + '] ' + Lang.t('validation.qty_greater_than_zero'));
                }
            }

            if(errors.length > 0) {
                this.message.transaction.error.set(errors);
                return false;
            }

            return true
        },

        finalizeAll(target = "") {
            if(!this.finalized(target)) {
                this.triggerDetailValidation(target);
                return false;
            }

            this.message.transaction.error.set([Lang.t('validation.nothing_to_finalize')]);
        },

        triggerDetailValidation(target = '') {
            for(var i = 0; i  < this.$refs[target].$children.length; i++){
                let child =  this.$refs[target].$children[i];
                
                if(child.$options._componentTag == 'table-display' && child.$data.action == "update"){
                     this.validateDetail(child.$options.propsData.passdata, child.$options.propsData.index, child.$options.propsData.dataIndex);
                }
            }
        },

        //datatable autocompute
        setBarcode(index) {
            let row = this.datatable.details.value.data[index];
            let unit = Number(this.datatable.details.value.data[index].unit_id);
            row['barcode'] = row['barcode_list'][unit];
        },

        setUnitSpecs(data) {
            let current = this.datatable.details.value.data[data.row];

            // If there's an old unit set, (upon load process),
            // do not set the new attribute field
            if (current.old_unit === false) {
                this.setAttributeField(data, this.datatable.details.value.data, 'unit_qty');
            } else {
                current.old_unit = false;
            }
            
            this.setTotalQty(data.row);
            this.setBarcode(data.row);
        },

        setTotalQty(index) {
            let row = this.datatable.details.value.data[index];

            row['total_qty'] =  Numeric.format(
                Numeric.parse(row['unit_qty']) *
                Numeric.parse(row['qty'])
            );

            this.setAmount(index);
        },

        setAmount(index) {
            let row = this.datatable.details.value.data[index];

            row['amount'] = Numeric.format(
                Numeric.parse(row['total_qty']) *
                Numeric.parse(row['price'])
            );
        },

        // Delete product
        confirmation(data, index, dataIndex) {

            if(this.disabled) {
                return false;
            }

            this.confirm.visible = true;
            this.confirm.index = dataIndex;
        },

        removeProduct() {
            this.datatable.details.value.data.splice(this.confirm.index, 1);
            this.confirm.visible = false;
        },

        redirect() {
            if(this.info.id != 0 && !this.reload) {
                this.service.redirect(this.info.id);
            } else {
                this.service.create(this.reload);
            }
        },

        // Event for triggering warning, also check if there's a product count
        notify(action) {
            if(this.datatable.details.value.data.length > 0) {
                this.warning.visible = true;
                this.action.buffer = action;
            } else {
                this.action.value = action;
            }
        },

        revertInfo() {
            this.info.set(this.buffer.data);
            this.warning.visible = false;
            this.action.reset();
        },

        // Event for warning
        removeProducts() {
            this.datatable.details.value.data = [];
            this.warning.visible = false;
            this.action.value = this.action.buffer;
        },

        hideControls() {
            this.datatable.details.settings.withActionCell = false;
            this.controls.forApproval = false;
            this.controls.draft = false;
            this.controls.approveDecline = false;
            this.controls.revert = false;
        },

        viewReference(id) {
            switch(this.info.transactionable_type) {
                case 'stock_request':
                    StockRequestService.instance().view(id);
                    break;
            }
        },

        createInfo(data = {}) {
            let initial = {
                id: 0,
                sheet_number: '-',
                requested_by: Laravel.auth.user,
                created_for: Laravel.auth.branch,
                for_location: '',
                remarks: '',
                transaction_date: Moment().format(),
                modified_date: '-',
                created_date: '-',
                approval_status: APPROVAL_STATUS.NEW,
                approval_status_label: Lang.t('label.new'),
                total_amount: '',
                created_by: '-',
                modified_by: '-',
                is_deleted: false
            };

            let udfs = this.createUDFs();

            this.info = new Data(Object.assign({}, initial, data, udfs));
        },

        createUDFs() {
            let udfs = {};

            for(let index in this.udfs.data) {
                let udf = this.udfs.data[index];
                let value = '';

                switch(udf.type) {
                    case UDF_TYPE.DATE:
                        value = Moment().format('YYYY-MM-DD');
                        break;

                    case UDF_TYPE.DATETIME:
                        value = Moment().format();
                        break;

                    case UDF_TYPE.INTR:
                        value = 0;
                        break;

                    case UDF_TYPE.DECIMAL:
                        value = Numeric.format(0);
                        break;
                }

                udfs[udf.alias] = value;
            }

            return udfs;
        },

        createUDFFormat() {
            for(let index in udfs) {
                let udf = udfs[index];
                let type = 'string';

                switch(udf.type) {
                    case UDF_TYPE.DECIMAL:
                    case UDF_TYPE.INTR:
                        type = 'numeric';
                        break;

                    case UDF_TYPE.STR:
                        type = 'string';
                        break;

                    case UDF_TYPE.DATE:
                        type = 'date';
                        break;

                    case UDF_TYPE.DATETIME:
                        type = 'datetime';
                        break;
                }

                this.format.info[udf.alias] = type;
            }
        },

        findByBarcode(event) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            let params = this.getBarcodeScanApiParams(event.target.value);

            return this.getBarcodeScanApi(params)
                .then(function (response) {
                    that.processing = false;

                    let callback = that.onCompleteBarcodeScanRequest(response, event);

                    if (typeof callback !== 'undefined' && !callback) {
                        return false;
                    }

                    let details = that.formatProductResponseToTransactionDetails(response.data.values.product, { qty: that.scan.quantity});

                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    event.target.value = '';
                    that.onSuccessfulBarcodeScan(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    event.target.select();
                });
        },

        findByBarcodeInput(event) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            let params = this.getBarcodeScanApiParams(event.target.value);

            return this.getBarcodeScanApi(params)
                .then(function (response) {
                    that.processing = false;

                    let callback = that.onCompleteBarcodeScanRequest(response, event);

                    if (typeof callback !== 'undefined' && !callback) {
                        return false;
                    }

                    let details = that.formatProductResponseToTransactionDetails(response.data.values.product, { qty: that.scan.quantity});

                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    event.target.value = '';
                    that.focusOnLastQty();
                    that.onSuccessfulBarcodeScan(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    event.target.select();
                });
        },

        findByProductSelected(data) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            this.datatable.product.checked.push(data.id);

            let callback = this.beforeProductSelect();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            let params = this.getSelectProductsApiParams();

            return this.getSelectProductsApi(params)
                .then(function (response) {
                    let details = that.formatProductResponseToTransactionDetails(response.data.values.products);

                    that.datatable.product.checked = [];
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    that.processing = false;
                    that.focusOnLastQty();
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        findByStockNoInput(event) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            let params = this.getStockNoApiParams(event.target.value);

            return this.getStockNoApi(params)
                .then(function (response) {
                    that.processing = false;

                    let callback = that.onCompleteStockNoScanRequest(response, event);

                    if (typeof callback !== 'undefined' && !callback) {
                        return false;
                    }

                    let details = that.formatProductResponseToTransactionDetails(response.data.values.product);

                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    event.target.value = '';
                    that.focusOnLastQty();
                    that.onSuccessfulStockNoScan(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    event.target.select();
                });
        },

        focusOnLastQty() {
            let that = this;

            Vue.nextTick(function() {
                that.$refs['qty-' + (that.datatable.details.value.data.length - 1)].$el.focus();
            });
        },

        selectProducts() {
            let that = this;

            let callback = this.beforeProductSelect();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            let params = this.getSelectProductsApiParams();

            return this.getSelectProductsApi(params)
                .then(function (response) {
                    let details = that.formatProductResponseToTransactionDetails(response.data.values.products);

                    that.datatable.product.checked = [];
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        selectProductsFromDataCollector() {
            let that = this;

            this.datatable.product.checked = Arr.pluck(this.dataCollector.data.details.data, 'product_id');

            let callback = this.beforeProductSelect();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            let params = this.getSelectProductsApiParams();

            this.getSelectProductsApi(params)
                .then(function (response) {
                    let products = that.setDataFromDataCollector(response.data.values.products);
                    let details = that.formatProductResponseToTransactionDetails(products);

                    that.datatable.product.checked = [];
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    that.processing = false;
                    that.dataCollector.visible = true;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getBarcodeScanApi(params){
            return ProductService.findByBarcodeWithSummaryAndPurchasePrice(params);
        },

        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: PRODUCT_STATUS.INACTIVE
                }
            };
        },

        getStockNoApi(params){
            return ProductService.findByStockNoWithSummaryAndPurchasePrice(params);
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: PRODUCT_STATUS.INACTIVE
                }
            };
        },

        getSelectProductsApi(params) {
            return ProductService.selectWithSummaryAndPurchasePrice(params);
        },

        getSelectProductsApiParams() {
            return {
                product_ids: this.datatable.product.checked,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
            };
        },

        onSuccessfulLoad(response) {
            return true;
        },

        onSuccessfulBarcodeScan(response) {
            return true;
        },

        onSuccessfulStockNoScan(response) {
            return true;
        },

        onSuccessfulImportTransaction(response) {
            return true;
        },

        onCompleteBarcodeScanRequest(response, event) {
            return true;
        },

        onCompleteStockNoScanRequest(response, event) {
            return true;
        },

        onSuccessfulValidation() {
            return true;
        },

        beforeProductSelect() {
            return true;
        },

        print(template) {
            if(this.info.id == 0 || !permissions.print) {
                return false;
            }

            this.service.print({
                id: this.info.id,
                template: template
            });
        },

        getExportColumnSettings() {
            return {
                cost: permissions.show_cost_product
            }
        },

        /**
         * Method for checking inventory warning. Possible to use for checking min/max checker
         * or inventory - current qty checker. This will is always called upon qty or uom change
         *
         * @param data
         */
        checkInventoryForWarning(data) {

        },

        formatAttributesToTransactionDetails(details, override = {}, hook = null) {
            return details;
        },

        formatProductResponseToTransactionDetails(details, override = {}) {
            return this.formatAttributesToTransactionDetails(details, override, function(formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0,
                    product_id: detail.id
                });
            });
        },

        formatImportResponseToTransactionDetails(details, override = {}) {
            return this.formatAttributesToTransactionDetails(details, override, function(formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0
                });
            });
        },

        directApproval() {
            this.approveTransaction();
            this.hideDirectApprovalDialog();
        },

        showDirectApprovalDialog(message) {
            this.dialog.directApproval.message = message;
            this.dialog.directApproval.visible = true;
            this.$nextTick(() => {
                this.$refs.saveOnly.focus();
            });
        },

        hideDirectApprovalDialog() {
            this.dialog.directApproval.message = '';
            this.dialog.directApproval.visible = false;
        },

        forApprovalTransaction() {
            this.approval(APPROVAL_STATUS.FOR_APPROVAL);
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },

        revertTransaction() {
            this.approval(APPROVAL_STATUS.DRAFT);
        },

        clearProductDropdownAndValue() {
            this.reference.products = [];
        },
        
        setDataFromDataCollector(products) {
            let data = [];

            for(let x in this.dataCollector.data.details.data) {
                let collector = this.dataCollector.data.details.data[x];

                for(let i in products) {
                    let product = Obj.copy(products[i]);

                    if(collector.product_id == product.id) {
                        product.qty = collector.qty;
                        product.oprice = product.price;
                        product.price = collector.price;
                        product.source = TRANSACTION_SOURCE.DATA_COLLECTOR;
                        product.unit_id = collector.unit_id;
                        product.unit_qty = collector.unit_qty;

                        data.push(product);
                    }
                }
            }

            return data;
        },

        importDataCollectorTransaction() {
            if(this.processing) {
                return false;
            }

            let callback = this.beforeImportDataCollector();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            DataCollectorService.findByReferenceNumber({
                reference_number: this.dataCollector.referenceNumber,
                transaction_type: this.dataCollector.transactionType
            })
            .then(function (response){
                that.dataCollector.data = response.data.values;
                that.selectProductsFromDataCollector();
                that.processing = false;
            })
            .catch(function (error){
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        deleteDataCollectorTransaction() {
            this.message.reset();

            let that = this;

            DataCollectorService.destroyByReference(this.dataCollector.referenceNumber)
                .then(function (response){
                    that.dataCollector.visible = false;
                    that.message.transaction.success = response.data.message;
                })
                .catch(function (error){
                    that.dataCollector.visible = false;
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        beforeImportDataCollector() {
            return true;
        },

        hasLocalCache(id = 0) {
            if (this.forViewingOnly) {
                return false;
            }

            let cached = LocalCache.get(this.cache.module);

            if (undefined === cached || !cached.hasOwnProperty('info')) {
                return false;
            }

            return cached.info.id === id;
        },

        onCacheLoaded() {

        }
    },

    watch: {
        'confirm.visible': function(visible) {
            if(!visible) {
                this.confirm.reset();
            }
        },
        'warning.visible': function(visible) {
            if(!visible) {
                this.warning.reset();
            }
        },
        'productPicker.visible': function(visible) {
            if(visible) {
                this.productSearch();
            }
        },
        'disabled': function(disabled) {
            if (disabled) {
                this.datatable.details.settings.withActionCell = false;
            }
        },
    },

    computed: {
        totalQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['qty']);
            }

            return Numeric.format(total);
        },
        totalAmount: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index].amount);
            }

            return Numeric.format(total);
        },
        data: function () {
            return {
                info: Format.make(this.info.data(), this.format.info),
                details: Format.make(this.datatable.details.value.data, this.format.detail)
            }
        },
        disabled: function () {
            return !this.forViewingOnly
                ? ([APPROVAL_STATUS.NEW, APPROVAL_STATUS.DRAFT].indexOf(Number(this.info.approval_status)) === -1)
                : true;
        },
        updatable: function () {
            return !this.forViewingOnly
                ? ([APPROVAL_STATUS.NEW, APPROVAL_STATUS.DRAFT, APPROVAL_STATUS.APPROVED].indexOf(Number(this.info.approval_status)) !== -1)
                : false;
        },
        approvalClass: function() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.NEW:
                    return 'new';
                    break;

                case APPROVAL_STATUS.DRAFT:
                    return 'draft';
                    break;

                case APPROVAL_STATUS.FOR_APPROVAL:
                    return 'for-approval';
                    break;

                case APPROVAL_STATUS.APPROVED:
                    return 'approved';
                    break;
                    
                case APPROVAL_STATUS.DECLINED:
                    return 'declined';
                    break;
            }
        },

        showBarcodeStockNoInput: function() {
            return !this.disabled && this.controls.create;
        },

        fullyEnabled: function() {
            return !this.disabled && this.controls.create;
        },

        newTransaction: function() {
            return this.info.approval_status === APPROVAL_STATUS.NEW;
        },

        approvedTransaction: function() {
            return this.info.approval_status === APPROVAL_STATUS.APPROVED;
        },

        declinedTransaction: function() {
            return this.info.approval_status === APPROVAL_STATUS.DECLINED;
        },

        forViewingOnly() {
            return this.options.setup === SETUP.VIEW;
        },

        direct: function() {
            return true;
        },

        datepickerFormat: function() {
            let options = {
                format: 'yyyy-MM-dd'
            };

            if (Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.MANUAL) {
                options.disabled = {
                    to: new Date(Moment(Laravel.settings.transaction_summary_computation_last_post_date).add(1, 'days').format('YYYY-MM-DD'))
                }
            }

            return options;
        },

        disabledDate: function () {
            return (this.disabled || Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.AUTOMATIC);
        }
    }
}