import CoreMixins from '../Common/CoreMixins.js';
import ColumnSettingsMixins from '../Common/ColumnSettingsMixins.js';
import LocalCache from "../Classes/LocalCache";

export default {
    mixins: [CoreMixins, ColumnSettingsMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withSelect: {
                    value: 'id'
                },
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            },
            selected:[]
        },
        message: new Notification,
        processing: false,
        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),
        filter: {
            visible: false,
            options: [],
            data: []
        },
        columnSettings: {
            id: 'column-settings',
            visible: false,
            selections: {},
            columns: []
        },
        service: {},
        options: {
            setup: SETUP.ENABLED,
        },
        udfs: udfs,
        key: '',
        cache: {
            module: 0
        }
    },

    mounted: function() {
        if (!this.enabled) {
            this.hideControls();
        }

        this.paginate();
        this.columnSettings.columns = this.removeFromColumnSettings(this.columnSettings.columns);
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.filter.visible = false;
            that.processing = true;
            that.message.reset();

            Promises.push(
                this.service.paginate({
                    'perPage': that.datatable.settings.perPage,
                    'page': page,
                    'filters': that.filter.data,
                })
                .then(function (response) {
                    that.processing = false;
                    that.message.info = response.data.message;
                    that.datatable.value.data = response.data.values[that.key];
                    that.datatable.value.pagination = response.data.values.meta.pagination;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
            );
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            this.service.destroy(that.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.visible = false;
                })
        },

        approval(status) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            if(that.datatable.selected.length == 0){
                that.message.error.set([Lang.t('validation.please_select_one_transaction')]);
                that.processing = false;
                return false
            }

            this.service.approval({
                transactions: that.datatable.selected,
                status: status
            })
            .then(function (response) {
                that.processing = false;
                that.message.success = response.data.message;
                that.paginate(that.datatable.value.pagination.current_page);
                that.clearCache();
                that.datatable.selected = [];
             })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        excel(id = null) {
            let params = {
                filters: this.filter.data,
                settings: Object.assign({}, this.columnSettings.selections)
            };

            params.settings = this.appendExportColumnSettingsParams(params.settings);

            if (null !== id) {
                params.settings = this.getExportColumnSettings();
                params['template_id'] = id;
            }

            this.service.export(params);
        },
        
        create() {
            this.service.create();
        },

        show(id) {
            if (this.enabled) {
                if(! permissions.view_detail && ! permissions.update) {
                    return false;
                }
                this.service.edit(id)
            } else {
                if(! permissions.from_view_detail) {
                    return false;
                }
                this.service.view(id);
            }
        },

        confirmation(data, index) {
            this.confirm.visible = true
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        hideControls() {
            delete this.datatable.settings.withSelect;
            this.datatable.settings.withActionCell = false;
            this.datatable.settings.withDelete = false;
        },

        getExportColumnSettings() {
            return {
                amount: permissions.show_price_amount,
                remaining_amount: permissions.show_price_amount,
                lost_amount: permissions.show_price_amount,
                gain_amount: permissions.show_price_amount
            }
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },

        hasLocalCache() {
            let cached = LocalCache.get(this.cache.module);

            return !(undefined === cached || !cached.hasOwnProperty('info'));
        },

        clearCache() {
            let cached = LocalCache.get(this.cache.module);

            if (!this.hasLocalCache()
                || cached.info.id === 0
                || !Arr.in(cached.info.id, this.datatable.selected)
            ) {
                return false;
            }

            LocalCache.remove(this.cache.module);
        }
    },

    watch: {
        'confirm.visible': function(visible) {
            if (!visible) {
                this.confirm.reset();
            }
        },
    },

    computed: {
        'enabled': function() {
            return this.options.setup === SETUP.ENABLED;
        }
    }
}