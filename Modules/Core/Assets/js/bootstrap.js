import Vue from 'vue';
import axios from 'axios';
import router from 'vue-router';
import Errors from './Classes/Errors';
import Form from './Classes/Form';
import Data from './Classes/Data';
import Arr from './Classes/Arr';
import Obj from './Classes/Obj';
import Format from './Classes/Format';
import Toggle from './Classes/Toggle';
import Notification from './Classes/Notification';
import Modal from './Components/Modal/Modal.vue';
import Message from './Components/Message/Message.vue';
import Sidebar from './Components/Sidebar/Sidebar.vue';
import Accordion from './Components/Sidebar/Accordion.vue';
import AccordionLink from './Components/Sidebar/AccordionLink.vue';
import AccordionGroup from './Components/Sidebar/AccordionGroup.vue';
import ControlBar from './Components/ControlBar/ControlBar.vue';
import ControlButton from './Components/ControlBar/ControlButton.vue';
import ControlDropdown from './Components/ControlBar/ControlDropdown.vue';
import StateHandler from './Classes/StateHandler.js';
import Chosen from './Components/Chosen/Chosen.vue';
import ChosenMixins from './Components/Chosen/ChosenMixins.js';
import NumberInput from './Components/NumberInput/NumberInput.vue';
import Confirm from './Components/Confirm/Confirm.vue';
import DialogBox from './Components/DialogBox/DialogBox.vue';
import Validator from 'Validator'
import Datatable from './Components/Datatable/Datatable.vue'
import TableHeader from './Components/Datatable/TableHeader.vue'
import TableInput from './Components/Datatable/TableInput.vue'
import TableCellData from './Components/Datatable/TableCellData.vue'
import DatatableMixins from './Components/Datatable/DatatableMixins.js'
import FilterModal from './Components/FilterModal/FilterModal.vue';
import Datepicker from 'vuejs-datepicker';
import ColumnSelect from './Components/ColumnSelect/ColumnSelect.vue';
import DateTimePicker from './Components/DateTimePicker/DateTimePicker.vue';
import './enums';
import VueInternalization from 'vue-i18n';
import Locales from './vue-i18n-locales.generated.js';
import QueryString from 'qs';
import Timepicker from './Components/Timepicker/Timepicker.vue';
import Moment from 'moment';
import Numeric from './Classes/Numeric';
import DeepMerge from 'deepmerge';
import MyMenu from './Components/MyMenu/MyMenu.vue';
import Carousel from './Components/Carousel/Carousel.vue';

if (process.env.MIX_APP_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
}

Vue.use(VueInternalization);

window.Vue = Vue;
window.axios = axios;
window.Errors = Errors;
window.Form = Form;
window.Data = Data;
window.Arr = Arr;
window.Obj = Obj;
window.Format = new Format;
window.Toggle = Toggle;
window.Notification = Notification;
window.Modal = Modal;
window.Message = Message;
window.Sidebar = Sidebar;
window.Accordion = Accordion;
window.AccordionLink = AccordionLink;
window.AccordionGroup = AccordionGroup;
window.ControlBar = ControlBar;
window.ControlButton = ControlButton;
window.ControlDropdown = ControlDropdown;
window.router = router;
window.StateHandler = StateHandler;
window.Chosen = Chosen;
window.NumberInput = NumberInput;
window.Confirm = Confirm;
window.DialogBox = DialogBox;
window.Validator = Validator;
window.Datatable = Datatable;
window.TableHeader = TableHeader;
window.TableInput = TableInput;
window.TableCellData = TableCellData;
window.DatatableMixins = DatatableMixins;
window.FilterModal = FilterModal;
window.Datepicker = Datepicker;
window.ChosenMixins = ChosenMixins;
window.ColumnSelect = ColumnSelect;
window.DateTimePicker = DateTimePicker;
window.Lang = new VueInternalization({
    locale: Laravel.locale,
    messages: Locales
});
window.QueryString = QueryString;
window.Timepicker = Timepicker;
window.Moment = Moment;
window.Numeric = new Numeric;
window.DeepMerge = DeepMerge;
window.MyMenu = MyMenu;
window.Carousel = Carousel;

window.Promises = [];

window.TreeItemDispatcher = new Vue();
window.ChosenDispatcher = new Vue();
window.DatatableDispatcher = new Vue();
window.MyMenuDispatcher = new Vue();

Moment.defaultFormat = 'YYYY-MM-DD HH:mm:ss';

axios.interceptors.request.use(function (config) {
    config.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;
    
    config.paramsSerializer = function(params) {
        return QueryString.stringify(params);
    };

    return config;
}, function (error) {
    return Promise.reject(error);
});
