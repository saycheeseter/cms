import Invoice from '../Invoice/detail.js';
import SalesService from '../Services/SalesService';
import CustomerService from '../Services/CustomerService';
import Outbound from '../Common/OutboundMixins.js';
import SalesProductSelectorAPIMixins from "../Common/SalesProductSelectorAPIMixins";

let app = new Vue({
    el: '#app',

    mixins: [Invoice, Outbound, SalesProductSelectorAPIMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
        
        buffer: {
            data: {
                customer_id: 0,
                customer_type: 0
            }
        },

        reference: {
            customers: customers,
            customerDetails: [],
        },

        attachment: {
            module: MODULE.SALES
        },

        service: new SalesService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.SALES,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.SALES
        }
    },

    created() {
        this.createInfo({
            term: 0,
            customer_type: CUSTOMER.REGULAR,
            customer_id: null,
            customer_detail_id: null,
            customer_walk_in_name: '',
            salesman_id: Laravel.auth.user,
        });
    },

    methods: {
        onSuccessfulLoad(response) {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomerDetails(this.info.customer_detail_id);
            }

            this.checkTransactionForInventoryWarning();
        },

        onCacheLoaded() {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomerDetails(this.info.customer_detail_id);
            }

            this.checkTransactionForInventoryWarning();
        },

        /**
         * Get customer details by based on the current customer id
         */
        getCustomerDetails(def = null) {
            let that = this;

            this.processing = true;

            CustomerService.details(this.info.customer_id)
                .then(function (response) {
                    that.reference.customerDetails = response.data.values.details;

                    let detail = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.customer_detail_id = detail;
                    that.processing = false;
                    that.action.reset();
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.action.reset();
                });
        },

        /**
         * Clears customer and detail id if current customer type is walk in
         * Clears customer name if current customer type is regular
         */
        clearCustomerByType() {
            if (this.isWalkInCustomer) {
                this.info.customer_id = null;
                this.info.customer_detail_id = null;
                this.reference.customerDetails = [];
            } else {
                this.info.customer_walk_in_name = '';
            }
        },

        getExtraInfoForUpdateApproved() {
            return [
                'salesman_id',
                'term'
            ];
        }
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'customer':
                    this.getCustomerDetails();
                    break;

                case 'customer-type':
                    this.clearCustomerByType();
                    break;
            }
        },
        'info.customer_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer_type': function(newValue, oldValue) {
            this.buffer.data.customer_type = oldValue;
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.created_for = this.info.created_for;
            
            if(newValue == CUSTOMER.WALK_IN) {
                this.$nextTick(() => {
                    this.$refs.walkin.focus();
                })
            }
        },
    },

    computed: {
        isRegularCustomer: function () {
            return this.info.customer_type == CUSTOMER.REGULAR;
        },

        isWalkInCustomer: function () {
            return this.info.customer_type == CUSTOMER.WALK_IN;
        },

        regularCustomer: function () {
            return CUSTOMER.REGULAR;
        },

        walkInCustomer: function () {
            return CUSTOMER.WALK_IN;
        }
    }
});