import CoreMixins from '../Common/CoreMixins';
import BankAccountService from '../Services/BankAccountService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        fields: new Data({
            id: 0,
            account_number: '',
            account_name: '',
            bank_id: '',
            opening_balance: 0,
        }),
        format: {
            opening_balance: 'numeric',
        },
        message: new Notification,
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.account_number'), columnName: 'account_number', type: 'string' },
                    { name: Lang.t('label.account_name'), columnName: 'account_name', type: 'string' },
                ],
                data: []
            }
        },
        banks: banks,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            BankAccountService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.accounts;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        store() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            BankAccountService.store(Format.make(that.fields.data(), that.format))
                .then(function (response) {
                    that.datatable.value.data.push(response.data.values.account);
                    that.message.success = response.data.message;
                    that.processing = false;
                    that.computePagination(that.datatable.value.pagination, 'add');
                    that.toggleFocus();
                    that.fields.reset();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        update(data, index) {
            let that = this;

            if(that.processing){
                return false
            }

            that.message.reset();
            that.processing = true;

            BankAccountService.update(Format.make(data, that.format), data.id)
                .then(function (response) {
                    that.changeState(index, 'destroy');
                    that.toggleFocus();
                    that.message.success = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        destroy(data) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            BankAccountService.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                })
        },

        confirm(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        validate(data, index) {
            var data = typeof data === 'undefined' ? this.fields.data() : data;

            var validation = Validator.make(Format.make(data, this.format), {
                bank_id: 'required',
                account_name: 'required',
                account_number: 'required',
                opening_balance: 'numeric'
            }, {
                'bank_id.required': Lang.t('validation.bank_required'),
                'account_name.required': Lang.t('validation.account_name_required'),
                'account_number.required': Lang.t('validation.account_number_required'),
                'opening_balance.numeric': Lang.t('validation.invalid_opening_balance'),
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (data.id === 0) {
                this.store();
            } else {
                this.update(data, index);
            }
        }
    },

    watch: {
        'modal.confirm.visible': function(visible){
            if (!visible) {
                this.modal.confirm.reset();
            }
        },
    }

});
