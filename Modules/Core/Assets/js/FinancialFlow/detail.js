import CoreMixins from '../Common/CoreMixins';
import attachment from '../Attachment/attachment';

export default {
    mixins: [CoreMixins, attachment],

    components: {
        DateTimePicker,
        Datepicker
    },

    data: {
        datatable: {
            transactions: {
                settings: {
                    id: 'transactions-table',
                    withPaging : true,
                    perPage: 10,
                    withEdit: false,
                    withPaginateCallBack: false
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            payments: {
                settings: {
                    id: 'payments-table',
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false
                },
                value: {
                    data: [],
                    pagination: {}
                },
                input: new Data({
                    id: 0,
                    payment_method: '',
                    amount: '',
                    check_no: '',
                    check_date: Moment().format(),
                    bank_account_id: null,
                })
            }
        },

        selector: {
            transaction: {
                visible: false,
                datatable: {
                    id: 'datatable-id',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withActionCell: false,
                        withPaginateCallBack: true,
                        withSelect: {
                            value: 'id'
                        }
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: [],
                },
                columnSettings: {
                    id: 'selector-column-settings',
                    visible: false,
                    selections: {},
                },
                type: ''
            }
        },

        accountBalance: Numeric.format(0),

        format: {
            payments: {
                amount: 'numeric',
                check_date: 'date',
            }
        },

        datetimepicker: {
            date: {
                format: 'yyyy-MM-dd'
            }
        },

        references: {
            branches: branches,
            paymentMethods: paymentMethods,
            bankAccounts: bankAccounts
        },

        message: new Notification(['transaction', 'selector']),

        controls: {
            draft: true,
            forApproval: false,
            approveDecline: false,
            revert: false
        },

        confirm: {
            deleteTransactions: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            deletePayments: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            warning: new Data({
                visible: false,
                value: false
            }),
        },

        action: new Data({
            buffer: '',
            value: ''
        }),

        buffer: {
            data: {}
        },

        dialog: {
            transaction: {
                message: ''
            },
            directApproval: {
                message: '',
                visible: false
            }
        },

        service: {
            payment: '',
            transaction: '',
            balance: ''
        },
    },

    mounted: function() {
        this.load();
    },

    methods: {
        visibility() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.DRAFT:
                    this.controls.forApproval = true;
                    break;
                    
                case APPROVAL_STATUS.FOR_APPROVAL:
                    this.controls.draft = false;
                    this.controls.approveDecline = true;
                    break;

                case APPROVAL_STATUS.APPROVED: 
                    this.controls.draft = false;
                    this.controls.revert = true;
                    break;

                case APPROVAL_STATUS.DECLINED:
                    this.controls.draft = false;
                    this.controls.revert = true;
                    break;
            }
        },

        showTransactionSelector() {
            if(this.validateRequiredEntity()) {
                this.selector.transaction.visible = true;
                this.getTransactions(1);
            }
        },

        load() {
            if(this.processing || id == 'create') {
                return false;
            }

            this.processing = true;

            let that = this;
            
            this.service.payment.show(id)
                .then(function (response){
                    that.info.set(response.data.values.record);
                    that.datatable.payments.value.data = response.data.values.details.data;
                    that.datatable.transactions.value.data = response.data.values.references.data;
                    that.setAttachmentProperties(id, response.data.values.attachments);

                    that.processing = false;

                    if (that.info.is_deleted) {
                        that.hideControls();
                    } else {
                        that.visibility();
                    }

                    that.afterSuccessfulLoad(response);
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        afterSuccessfulLoad(response) {
            // this.getBalance();
        },

        addTransactions(cont = false) {
            if(this.processing) {
                return false;
            }

            this.message.reset();

            if(this.selector.transaction.datatable.selected.length === 0) {
                this.message.selector.error.set([Lang.t('validation.details_min_one')]);
                return false;
            }

            if(!cont) {
                this.selector.transaction.visible = false;
            }

            let that = this;
            let ids = this.getUniqueTransactionIds();

            if (ids.length === 0) {
                return false;
            }

            this.processing = true;

            this.service.transaction.fetchTransactions({
                ids: ids
            })
            .then(function (response) {
                that.selector.transaction.datatable.selected = [];
                that.datatable.transactions.value.data = that.datatable.transactions.value.data.concat(response.data.values.transactions.data);
                that.processing = false;
            })
            .catch(function (response) {
                that.message.selector.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        // getBalance() {
        //     let that = this;
        //
        //     this.processing = true;
        //
        //     this.service.balance.balance(this.info.entity_id)
        //         .then(function (response) {
        //             that.accountBalance = response.data.values.balance.amount;
        //             that.processing = false;
        //             that.action.reset();
        //         })
        //         .catch(function (response) {
        //             that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
        //             that.processing = false;
        //             that.action.reset();
        //         });
        // },

        process(reload = false) {
            this.reload = reload;

            if (!this.validate()
                || (this.info.id === 0 && ! permissions.create)
                || (this.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            if (this.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            this.service.payment.store(this.data)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.transaction.message = response.data.message;
                    }

                    that.info.id = response.data.values.record.id;
                    that.attachFiles(that.info.id);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.message.reset();
            
            this.service.payment.update(this.data, that.info.id)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.transaction.message = response.data.message;
                    }

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        approval(status) {
            if(this.processing || !this.validate()) {
                return false;
            }

            this.processing = true;
            
            let that = this;

            this.message.reset();

            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL: 
                    let update = this.service.payment.update(this.data, that.info.id);

                    axios.all([update])
                        .then(axios.spread(function (update){
                            that.approvalRequest(status);
                        }))
                        .catch(function (error) {
                            that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                            that.processing = false;
                        });
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        approvalRequest(status) {
            let that = this;

            this.service.payment.approval({
                transactions: that.info.id,
                status: status
            })
            .then(function (response) {
                that.dialog.transaction.message = response.data.message;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        // Event for triggering warning, also check if there's a product count
        notify(action) {
            if(this.datatable.transactions.value.data.length > 0) {
                this.confirm.warning.visible = true;
                this.action.buffer = action;
            } else {
                this.action.value = action;
            }
        },

        removeTransactionsAndPayments() {
            this.datatable.transactions.value.data = [];
            this.datatable.payments.value.data = [];
            this.confirm.warning.visible = false;
        },

        revertInfo() {
            this.info.set(this.buffer.data);
            this.confirm.warning.visible = false;
            this.action.reset();
        },

        getTotalByKey(array, key) {
            if(array.length > 0) {
                let total = Numeric.parse(0);

                for(let index in array) {
                    let row = array[index];

                    total += Numeric.parse(row[key]);
                }

                return Numeric.format(total);
            } else {
                return Numeric.format(0);
            }
        },

        confirmRemoveTransaction(data, index, dataIndex) {
            this.confirm.deleteTransactions.visible = true;
            this.confirm.deleteTransactions.index = dataIndex;
        },

        removeTransaction() {
            this.datatable.transactions.value.data.splice(this.confirm.deleteTransactions.index, 1);
            this.confirm.deleteTransactions.visible = false;
        },

        confirmRemovePayment(data, index, dataIndex) {
            this.confirm.deletePayments.visible = true;
            this.confirm.deletePayments.index = dataIndex;
        },

        removePayment() {
            this.datatable.payments.value.data.splice(this.confirm.deletePayments.index, 1);
            this.confirm.deletePayments.visible = false;
        },

        redirect() {
            if(this.info.id != 0 && !this.reload) {
                this.service.payment.redirect(this.info.id);
            } else {
                this.service.payment.create(this.reload);
            }
        },

        getExistingTransactionIds() {
            let ids = [];
            let vm = this;

            let transactions = this.datatable.transactions.value.data.filter((transaction) => {
                return transaction.reference_type === vm.selector.transaction.type;
            });

            for(let index in transactions) {
                let row = transactions[index];
                ids.push(row.reference_id);
            }

            return ids;
        },
        
        storePayment(){
            let payment = Object.assign({}, this.datatable.payments.input.data());

            // Double check
            if (payment.amount === '') {
                payment.amount = this.totalRemainingAmount;
            } else {
                payment.amount = Numeric.format(payment.amount);
            }

            if (!this.isPaymentValid(payment)) {
                return false;
            }

            this.datatable.payments.value.data.push(payment);
            this.datatable.payments.input.reset();
        },

        updatePayment(data, index){
            let payment = Object.assign({}, data);

            if (Numeric.parse(payment.amount) === 0 && Numeric.parse(this.totalRemainingAmount) > 0) {
                payment.amount = this.totalRemainingAmount;
            }

            if (!this.isPaymentValid(payment)) {
                return false;
            }

            this.$refs.amountInput.$el.focus();
            this.changeState(index, 'destroy', 'payments');
        },

        validate() {
            if (this.processing) {
                return false;
            }

            if (!this.arePaymentsValid()) {
                return false;
            }

            // if(Numeric.parse(this.totalSelectedTransactionAmount) == 0) {
            //     this.message.transaction.error.set([Lang.t('validation.' + this.keys.amount + '_amount_not_zero')]);
            //     return false;
            // }

            if (this.datatable.transactions.value.data.length === 0) {
                this.message.transaction.error.set([Lang.t('validation.please_select_one_transaction')]);
                return false;
            }

            if ((Numeric.parse(this.totalRemainingAmount) !== 0 && this.datatable.transactions.value.data.length > 0)) {
                this.message.transaction.error.set([Lang.t('validation.remaining_amount_zero')]);
                return false;
            }

            if(!this.finalized('payments')) {
                this.message.transaction.error.set([Lang.t('validation.finalize_all_rows')]);
                return false;
            }

            return true;
        },

        isPaymentValid(data) {
            //let references = Obj.toArray(this.data.references);

            // Double check
            if(Numeric.parse(this.totalSelectedRemainingAmount) === 0
                || this.datatable.transactions.value.data.length < 1
            ) {
                return false;
            }

            // if(Numeric.parse(data.amount) === 0) {
            //     if(!(Numeric.parse(this.totalSelectedTransactionAmount) === 0
            //         && this.datatable.transactions.value.data.length > 1
            //         && Arr.pluck(references, this.keys.amount).indexOf(Numeric.parse('0')) == -1
            //         && Arr.pluck(references, this.keys.amount).indexOf('') == -1 // Double check
            //         && this.datatable.payments.value.data.length === 0)
            //     ) {
            //         return false;
            //     }
            // }

            // if(data.payment_method == PAYMENT_METHOD.FROM_BALANCE) {
            //     if(!this.isFromBalancePaymentValid(data)) {
            //         this.message.transaction.error.set([Lang.t('validation.' + this.keys.balance + '_balance_should_not_exceed')]);
            //         return false;
            //     }
            // }

            return true;
        },

        arePaymentsValid() {
            let count = 0;

            for (let i in this.datatable.payments.value.data) {
                let payment = this.datatable.payments.value.data[i];

                if (Numeric.parse(payment.amount) === 0) {
                    count++;
                }
            }

            if (Numeric.parse(this.totalSelectedTransactionAmount) === 0 && this.datatable.transactions.value.data.length > 1) {
                if (count > 1) {
                    this.message.transaction.error.set([Lang.t('validation.multiple_zero_amount_not_allowed')]);
                    return false;
                }

                return true;
            } else if (Numeric.parse(this.totalSelectedTransactionAmount) !== 0) {
                if (count > 0) {
                    this.message.transaction.error.set([Lang.t('validation.zero_amount_not_allowed')]);
                    return false;
                }

                return true;
            }

            return true;
        },

        fillRemainingPayableTransaction(index, type) {
            let remaining = Numeric.parse(this.datatable.transactions.value.data[index].remaining);

            switch(type) {
                case types.sales_return_inbound:
                case types.purchase_return_outbound:
                    remaining *= -1;
                    break;
            }

            Vue.set(
                this.datatable.transactions.value.data[index],
                this.keys.amount,
                Numeric.format(remaining)
            );
        },

        isFromBalancePaymentValid(data) {
            let total = Numeric.parse(data.amount) + Numeric.parse(this.accountBalancePayment);

            return ! (Numeric.parse(total) > Numeric.parse(this.accountBalance));
        },

        getUniqueTransactionIds() {
            let existing = this.getExistingTransactionIds();
            let selected = this.selector.transaction.datatable.selected;
            let difference = Arr.difference(selected, existing);
            
            return Arr.intersection(selected, difference);
        },

        getTransactions(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            this.getTransactionSelectorApi({
                per_page: this.selector.transaction.datatable.settings.perPage,
                page: page
            })
            .then(function (response) {
                that.processing = false;
                that.selector.transaction.datatable.value.data = response.data.values.transactions.data;
                that.selector.transaction.datatable.value.pagination = response.data.values.transactions.meta.pagination;
            })
            .catch(function (response) {
                that.processing = false;
                that.message.selector.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        hideControls() {
            this.controls.forApproval = false;
            this.controls.draft = false;
            this.controls.approveDecline = false;
            this.controls.revert = false;
        },

        directApproval() {
            this.approveTransaction();
            this.hideDirectApprovalDialog();
        },

        showDirectApprovalDialog(message) {
            this.dialog.directApproval.message = message;
            this.dialog.directApproval.visible = true;

            this.$nextTick(() => {
                this.$refs.saveOnly.focus();
            });
        },

        hideDirectApprovalDialog() {
            this.dialog.directApproval.message = '';
            this.dialog.directApproval.visible = false;
        },

        forApprovalTransaction() {
            this.approval(APPROVAL_STATUS.FOR_APPROVAL);
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },

        revertTransaction() {
            this.approval(APPROVAL_STATUS.DRAFT);
        },

        isCashPayment(payment) {
            return payment === PAYMENT_METHOD.CASH;
        },

        isCheckPayment(payment) {
            return payment === PAYMENT_METHOD.CHECK;
        },

        isBankDepositPayment(payment) {
            return payment === PAYMENT_METHOD.BANK_DEPOSIT;
        },

        isFromBalancePayment(payment) {
            return payment === PAYMENT_METHOD.FROM_BALANCE;
        },

        isCheckOrBankDepositPayment(payment) {
            return Arr.in(payment, [
                PAYMENT_METHOD.CHECK,
                PAYMENT_METHOD.BANK_DEPOSIT
            ]);
        },

        negativeRemainingAmount(remaining) {
            return Numeric.format(Numeric.parse(remaining) * -1)
        }
    },

    watch: {
        'confirm.deleteTransactions.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteTransactions.reset();
            }
        },
        'confirm.deletePayments.visible': function(visible) {
            if(!visible) {
                this.confirm.deletePayments.reset();
            }
        },
        'confirm.warning.visible': function(visible) {
            if(!visible) {
                this.confirm.warning.reset();
            }
        },
        'disabled': function(disabled) {
            if (disabled) {
                this.datatable.payments.settings.withActionCell = false;
                this.datatable.transactions.settings.withActionCell = false;
            }
        },
        'selector.transaction.visible': function(value) {
            if(!value) {
                this.selector.transaction.datatable.selected = [];
                this.selector.transaction.datatable.value.data = [];
            }
        }
    },

    computed: {
        totalSelectedAmount: function() {
            return this.getTotalByKey(this.datatable.transactions.value.data, 'amount');
        },
        totalSelectedRemainingAmount: function() {
            return this.getTotalByKey(this.datatable.transactions.value.data, 'remaining');
        },
        disabled: function () {
            return this.info.is_deleted
                ? true
                : [APPROVAL_STATUS.NEW, APPROVAL_STATUS.DRAFT].indexOf(Number(this.info.approval_status)) === -1;
        },
        approvalClass: function() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.NEW:
                    return 'new';
                    break;

                case APPROVAL_STATUS.DRAFT:
                    return 'draft';
                    break;

                case APPROVAL_STATUS.FOR_APPROVAL:
                    return 'for-approval';
                    break;

                case APPROVAL_STATUS.APPROVED:
                    return 'approved';
                    break;
                    
                case APPROVAL_STATUS.DECLINED:
                    return 'declined';
                    break;
            }
        },
        data: function() {
            return {
                info: this.info,
                references: Format.make(this.datatable.transactions.value.data, this.format.references),
                details: Format.make(this.datatable.payments.value.data, this.format.payments)
            };
        },
        totalSelectedTransactionAmount: function() {
            return this.getTotalByKey(this.datatable.transactions.value.data, this.keys.amount);
        },
        totalPaymentAmount: function() {
            return this.getTotalByKey(this.datatable.payments.value.data, 'amount');
        },
        totalRemainingAmount: function() {
            return Numeric.format(Numeric.parse(this.totalSelectedTransactionAmount) - Numeric.parse(this.totalPaymentAmount))
        },
        accountBalancePayment: function() {
            let sum = 0;

            for(let index in this.datatable.payments.value.data) {
                let row = this.datatable.payments.value.data[index];

                if(Numeric.parse(row.payment_method) === PAYMENT_METHOD.FROM_BALANCE) {
                    sum = Numeric.parse(sum) + Numeric.parse(row.amount);
                }
            }

            return sum;
        },
        actualAccountBalance: function() {
            return this.info.approval_status === APPROVAL_STATUS.APPROVED
                ? Numeric.format(Numeric.parse(this.accountBalance))
                : Numeric.format(Numeric.parse(this.accountBalance) - Numeric.parse(this.accountBalancePayment));
        }
    }
}