import CoreMixins from '../Common/CoreMixins';

export default {
    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withSelect: {
                    value: 'id'
                },
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            },
            selected:[]
        },
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: String(Laravel.auth.branch) },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                    { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.transaction_date_time'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                        ]
                    },
                    {
                        name: Lang.t('label.approval_status'),
                        columnName: 'approval_status',
                        type: 'select',
                        selectOptions: [
                            { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                            { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                            { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                            { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                        ]
                    },
                ],
                data: []
            },
            columnSettings: {
                id: 'column-settings',
                visible: false,
                selections: {}
            }
        },
        message: new Notification,
        service: {}
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            this.service.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.records;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            this.service.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                })
        },

        approval(status) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            if(that.datatable.selected.length === 0){
                that.message.error.set([Lang.t('validation.please_select_one_transaction')]);
                that.processing = false;
                return false
            }

            this.service.approval({
                transactions: that.datatable.selected,
                status: status
            })
            .then(function (response) {
                that.processing = false;
                that.message.success = response.data.message;
                that.paginate(that.datatable.value.pagination.current_page);
                that.datatable.selected = [];
             })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        create() {
            this.service.create();
        },

        search() {
            this.modal.filter.visible = true;
        },

        columnSettings() {
            this.modal.columnSettings.visible = true;
        },

        show(id) {
            if(! permissions.view_detail && ! permissions.update) {
                return false;
            }
            
            this.service.edit(id);
        },

        confirmation(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },
    },

    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        },
    },
}