import InventoryChain from '../InventoryChain/detail.js';
import SalesReturnInboundService from '../Services/SalesReturnInboundService';
import ProductService from '../Services/ProductService';
import CustomerService from '../Services/CustomerService';
import SalesReturnService from '../Services/SalesReturnService';
import InventoryFlow from "../InventoryFlow/detail";
import SalesProductSelectorAPIMixins from "../Common/SalesProductSelectorAPIMixins";

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, SalesProductSelectorAPIMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
        
        service: {
            invoice: new SalesReturnService,
            inventoryChain: new SalesReturnInboundService
        },

        buffer: {
            data: {
                customer_type: '',
                customer_id: '',
            },
        },

        reference: {
            customers: customers,
            customerDetails: [],
        },

        attachment: {
            module: MODULE.SALES_RETURN_INBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.SALES_RETURN_INBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.SALES_RETURN_INBOUND
        },

        cache: {
            module: MODULE.SALES_RETURN_INBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.SALES_RETURN,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            term: 0,
            customer_type: CUSTOMER.REGULAR,
            customer_id: null,
            customer_detail_id: null,
            customer_walk_in_name: '',
            salesman_id: Laravel.auth.user,
        });
    },

    methods: {
        onSuccessfulLoad(response) {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomerDetails(this.info.customer_detail_id);
            }
        },

        onCacheLoaded() {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomerDetails(this.info.customer_detail_id);
            }
        },

        onSuccessfulImportInvoice(response) {
            this.getCustomerDetails(this.info.customer_detail_id);
        },

        /**
         * Get customer details by based on the current customer id
         */
        getCustomerDetails(def = null) {
            let that = this;

            this.processing = true;

            CustomerService.details(this.info.customer_id)
                .then(function (response) {
                    that.reference.customerDetails = response.data.values.details;

                    let detail = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.customer_detail_id = detail;

                    that.processing = false;
                    that.action.reset();
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.action.reset();
                });
        },

        /**
         * Clears customer and detail id if current customer type is walk in
         * Clears customer name if current customer type is regular
         */
        clearCustomerByType() {
            if (this.isWalkInCustomer) {
                this.info.customer_id = null;
                this.info.customer_detail_id = null;
                this.reference.customerDetails = [];
            } else {
                this.info.customer_walk_in_name = '';
            }
        },

        getSerialsConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.CUSTOMER,
                entity: this.info.customer_id,
                status: SERIAL_STATUS.NOT_AVAILABLE,
                transaction: SERIAL_TRANSACTION.SALES_OUTBOUND
            };
        },

        getBatchesConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.CUSTOMER,
                entity: this.info.customer_id,
                supplier: null
            };
        },

        removeItemManagementDataWarning() {
            return;
        },

        getExtraInfoForUpdateApproved() {
            return [
                'salesman_id',
                'term'
            ];
        },

        beforeImportDataCollector() {
            if(this.info.customer_id === null && this.info.customer_type === CUSTOMER.REGULAR) {
                this.message.transaction.error.set([Lang.t('validation.customer_required')]);
                return false;
            }

            return true;
        },
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'customer':
                    this.getCustomerDetails();
                    break;

                case 'customer-type':
                    this.clearCustomerByType();
                    break;
            }
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.customer_type = this.info.customer_type;
            this.buffer.reference.invoice = this.reference.invoice;
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer_type': function(newValue, oldValue) {
            this.buffer.data.customer_type = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.reference.invoice = this.reference.invoice;

            if(newValue == CUSTOMER.WALK_IN) {
                this.$nextTick(() => {
                    this.$refs.walkin.focus();
                })
            }
        },
    },

    computed: {
        isRegularCustomer: function () {
            return this.info.customer_type == CUSTOMER.REGULAR;
        },

        isWalkInCustomer: function () {
            return this.info.customer_type == CUSTOMER.WALK_IN;
        },

        regularCustomer: function () {
            return CUSTOMER.REGULAR;
        },

        walkInCustomer: function () {
            return CUSTOMER.WALK_IN;
        }
    }
});
