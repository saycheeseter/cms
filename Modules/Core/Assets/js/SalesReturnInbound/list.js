import SalesReturnInboundService from '../Services/SalesReturnInboundService';
import InventoryChain from '../InventoryChain/list.js';
import CustomerService from '../Services/CustomerService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain],

    data: {
        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'reference', label: Lang.t('label.reference'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'customer', label: Lang.t('label.customer'), default: true },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'salesman', label: Lang.t('label.salesman') },
                { alias: 'term', label: Lang.t('label.term'), default: true },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.SALES_RETURN_INBOUND,
                key: 'info'
            }
        },
        service: new SalesReturnInboundService,
        cache: {
            module: MODULE.SALES_RETURN_INBOUND
        }
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.salesman'),
            columnName: 'salesman_id',
            type: 'select',
            selectOptions: users
        }, {
            name: Lang.t('label.customer_type'),
            columnName: 'customer_type',
            type: 'select',
            selectOptions: [
                { id: String(CUSTOMER.WALK_IN), label: Lang.t('label.walk_in_customer') },
                { id: String(CUSTOMER.REGULAR), label: Lang.t('label.regular') }
            ]
        }, {
            name: Lang.t('label.customer'),
            columnName: 'customer_id',
            type:'doubleSelect',
            secondaryColumn: 'customer_detail_id',
            selectOptions: customers,
            withAll: true,
            withCallback: true,
            callback: function(customer) {
                return CustomerService.details(customer);
            }
        });
    }
});
