import CoreMixins from '../Common/CoreMixins';
import RoleService from '../Services/RoleService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        message: new Notification,

        datatable: {
            id: 'datatable-id',

            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },

            value: {
                data: [],
                pagination: {}
            },
        },

        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),

            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string' },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string' },
                ],
                data: []
            }
        }
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            RoleService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.roles;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            RoleService.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.paginate(that.datatable.value.pagination.current_page);
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                });
        },

        view(id) {
            if(
                !permissions.view_detail
                && !permissions.update
            ){
                return false;
            }
            
            RoleService.edit(id);
        },

        create() {
            RoleService.create();
        },

        confirm(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        }
    },

    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        }
    }
});