import CoreMixins from '../Common/CoreMixins';
import RoleService from '../Services/RoleService';
import { range } from 'lodash';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        tabs: {
            permissions: new Toggle(sections)
        },

        role: new Data({
            info : {
                id: 0,
                code: '',
                name:''
            },
            permissions: [],
        }),

        selected: {
            code: [],
            branch: ''
        },

        allBranches: {
            confirm: false,
            value: false
        },

        confirm: {
            visible: false
        },

        message: new Notification,

        branches: branches
    },

    mounted: function() {
        this.load();
        this.tabs.permissions.active('general');
    },

    methods: {
        load() {
            let that = this;

            if(id == 'create'){
                return;
            }

            this.processing = true;
            this.message.reset();

            RoleService.show(id)
                .then(function (response) {
                    that.role.info.id = response.data.values.role.id;
                    that.role.info.code = response.data.values.role.code;
                    that.role.info.name = response.data.values.role.name;
                    that.role.permissions = response.data.values.role.permissions;
                    that.evaluateAllBranchesCheckbox();

                    for(let index in response.data.values.role.permissions) {
                        let row = Object.assign({}, response.data.values.role.permissions[index]);
                        if(row['branch'] == that.selected.branch) {
                            that.selected = row;
                        }
                    }

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        store() {
            let that = this;
            
            that.processing = true;
            that.message.reset();

            RoleService.store(that.role.data())
                .then(function (response) {
                    that.message.success = response.data.message;
                    that.redirect(response.data.values.role.id);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update(apply) {
            if (!userPermissions.update) {
                return false;
            }

            this.confirm.visible = false;
            
            let that = this;

            this.processing = true;
            this.message.reset();

            let data = this.role.data();

            data['apply'] = apply;
            
            RoleService.update(data, this.role.info.id)
                .then(function (response) {
                    that.message.success = response.data.message;
                    that.redirect(id);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        save() {
            if(this.role.info.id == 0) {
                this.store();
            } else {
                this.confirm.visible = true;
            }
        },

        redirect(iden) {
            setTimeout(function() {
                RoleService.edit(iden);
            }, 2000);
        },

        toggleSectionPermissions(section) {
            var permissions = this.sectionPermissions(section);

            this.selected.code = (this.tabs.permissions[section].all)
                ? Arr.merge(this.selected.code, permissions)
                : Arr.remove(this.selected.code, permissions);
        },

        sectionPermissions(section) {
            var permissions = [];
            var modules = this.tabs.permissions[section].modules;

            for (var i = 0; i < modules.length; i++) {
                for (var x = 0; x < modules[i].permissions.length; x++) {
                    permissions.push(modules[i].permissions[x].code);
                }
            }

            return permissions;
        },

        permissions() {
            let permissions = this.role.permissions;
            let exit = false;

            for (let i = 0; i < permissions.length; i++) {
                if (permissions[i].branch == this.selected.branch) {
                    this.selected.code = permissions[i].code;
                    exit = true;
                    break;
                }
            }

            if (exit) {
                return false;
            }

            this.selected.code = [];
        },

        toggleModulePermissions(section, module) {
            let permissions = this.modulePermissions(section, module);

            this.selected.code = (this.tabs.permissions[section].modules[module].all)
                ? Arr.merge(this.selected.code, permissions)
                : Arr.remove(this.selected.code, permissions);
        },

        modulePermissions(section, module) {
            let permissions = [];
            let modulePermissions = this.tabs.permissions[section].modules[module].permissions;

            for (let i = 0; i < modulePermissions.length; i++) {
                permissions.push(modulePermissions[i].code);
            }

            return permissions;
        },

        range(start, end) {
            return range(start, end);
        },

        getLowerRangePermission(number) {
            let value;

            if (number <= 4) {
                value = 0;
            } else if ((number + 1) % 5 === 0) {
                value = number - 4;
            } else {
                value = (number + 1) - ((number + 1) % 5);
            }

            return value;
        },

        clearPermissions() {
            this.selected.code = [];
            this.role.permissions = [];
            this.allBranches.confirm = false;
        },

        closeAllBranchConfirm() {
            this.allBranches.confirm = false;
            this.allBranches.value = !this.allBranches.value;
        },

        evaluateAllBranchesCheckbox() {
            let equal = true;

            if(this.role.permissions.length !== this.branches.length) {
                equal = false;
            } else {
                for(let index in this.role.permissions) {
                    let row = this.role.permissions[index];
    
                    for(let i = Number(index) + 1; i < this.role.permissions.length; i++) {
                        let row2 = this.role.permissions[i];
    
                        if(Arr.difference(row.code, row2.code).length !== 0) {
                            equal = false;
                            break;
                        }
                    }

                    if (!equal) {
                        break;
                    }
                }
            }

            if(equal) {
                this.allBranches.value = true;
            }
        },

        onClickAllBranch() {
            if(this.role.permissions.length !== 0) {
                this.allBranches.confirm = true;
            } else {
                this.selected.code = [];
                this.role.permissions = [];
            }
        }
    },

    watch: {
        'selected.code': function(codes) {
            let permissions = this.role.permissions;
            var sections = this.tabs.permissions.get();
            let exit = false;

            // For auto checking section checkbox
            for(let section in sections) {
                let _permissions = this.sectionPermissions(section);
                
                this.tabs.permissions[section].all = (Arr.has(codes, _permissions));

                for(let i = 0; i < this.tabs.permissions[section].modules.length; i++) {
                    let _modulePermissions = this.modulePermissions(section, i);

                    this.tabs.permissions[section].modules[i].all = (Arr.has(codes, _modulePermissions))
                }
            }

            if(this.allBranches.value) {
                let existing = false;

                for(let index in this.branches) {
                    let branchId = this.branches[index].id;
                    for(let i=0; i < permissions.length; i++) {
                        if (permissions[i].branch === branchId) {
                            if (codes.length === 0) {
                                permissions.splice(i, 1);
                            } else {
                                permissions[i].code = codes;
                            }
                            
                            existing = true;
                            break;
                        }
                    }
                    
                    if(!existing && codes.length !== 0 && codes.length > 0) {
                        this.role.permissions.push({
                            branch: branchId,
                            code: codes
                        });
                    }

                    existing = false;
                }
            } else {
                // Push or removes a code in the user permission array
                for(let i=0; i < permissions.length; i++) {
                    if (permissions[i].branch === this.selected.branch) {
                        if (codes.length === 0) {
                            permissions.splice(i, 1);
                        } else {
                            permissions[i].code = codes;
                        }
                        
                        exit = true;
                        break;
                    }
                }

                if (exit) {
                    return false;
                }
                
                if(this.selected.branch !== null && codes.length > 0) {
                    // Push new array in user permission
                    this.role.permissions.push({
                        branch: this.selected.branch,
                        code: codes
                    });
                }
            }
        }
    }
});