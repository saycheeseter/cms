import PurchaseInboundService from '../Services/PurchaseInboundService';
import InventoryChain from '../InventoryChain/list.js';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain],

    data: {
        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'reference', label: Lang.t('label.reference'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'supplier', label: Lang.t('label.supplier'), default: true },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'remaining_amount', label: Lang.t('label.remaining_amount'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'dr_no', label: Lang.t('label.dr_no') },
                { alias: 'supplier_invoice_no', label: Lang.t('label.supplier_invoice_no') },
                { alias: 'supplier_box_count', label: Lang.t('label.supplier_box_count') },
                { alias: 'payment_method', label: Lang.t('label.payment_method') },
                { alias: 'term', label: Lang.t('label.term') },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.PURCHASE_INBOUND,
                key: 'info'
            }
        },
        service: new PurchaseInboundService,
        cache: {
            module: MODULE.PURCHASE_INBOUND
        }
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.supplier'),
            columnName: 'supplier_id',
            type: 'select',
            selectOptions: suppliers
        }, { 
            name: Lang.t('label.dr_no'), 
            columnName: 'dr_no', 
            type: 'string'
        }, { 
            name: Lang.t('label.supplier_invoice_no'), 
            columnName: 'supplier_invoice_no', 
            type: 'string'
        });
    }
});
