import InventoryChain from '../InventoryChain/detail.js';
import PurchaseInboundService from '../Services/PurchaseInboundService';
import AutoGroupProductBreakdownMixins from '../Common/AutoGroupProductBreakdownMixins.js';
import ProductService from '../Services/ProductService';
import PurchaseService from '../Services/PurchaseService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, AutoGroupProductBreakdownMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '=',
                        value: PRODUCT_STATUS.ACTIVE
                    },
                ]
            }
        },

        reference: {
            tsuppliers: tsuppliers,
            paymentMethods: paymentMethods,
        },

        buffer: {
            data: {
                supplier_id: ''
            }
        },

        datatable: {
            serial: {
                form: {
                    id: 'serial-number',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withPaginateCallBack: false,
                        withDelete: true
                    },
                    value: {
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            per_page: 10,
                            total: 1,
                            total_pages: 1
                        }
                    },
                    fields: new Data({
                        serial_number: '',
                        expiration_date: null,
                        manufacturing_date: null,
                        admission_date: null,
                        manufacturer_warranty_start: null,
                        manufacturer_warranty_end: null,
                        remarks: null,
                    }),
                }
            },
            batch: {
                form: {
                    id: 'batch',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withPaginateCallBack: false,
                        withDelete: true
                    },
                    value: {
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            per_page: 10,
                            total: 1,
                            total_pages: 1
                        }
                    },
                    fields: new Data({
                        name: '',
                        qty: 0
                    }),
                }
            }
        },

        format: {
            serial: {
                expiration_date: 'date',
                manufacturing_date: 'date',
                admission_date: 'date',
                manufacturer_warranty_start: 'date',
                manufacturer_warranty_end: 'date',
            },
            batch: {
                qty: 'numeric'
            }
        },

        validation: {
            rules: {
                serial: {
                    serial_number: 'required',
                    expiration_date: 'date',
                    manufacturing_date: 'date',
                    admission_date: 'date',
                    manufacturer_warranty_start: 'date|before:manufacturer_warranty_end',
                    manufacturer_warranty_end: 'date|after:manufacturer_warranty_start',
                },
                batch: {
                    name: 'required',
                    qty: 'required|numeric'
                }
            },
            messages: {
                serial: {
                    'serial_number.required': Lang.t('validation.serial_no_required'),
                    'expiration_date.date': Lang.t('validation.expiration_date_invalid'),
                    'manufacturing_date.date': Lang.t('validation.manufacturing_date_invalid'),
                    'admission_date.date': Lang.t('validation.admission_date_invalid'),
                    'manufacturer_warranty_start.date': Lang.t('validation.manufacturer_warranty_start_invalid'),
                    'manufacturer_warranty_end.date': Lang.t('validation.manufacturer_warranty_end_invalid'),
                    'manufacturer_warranty_start.before': Lang.t('validation.manufacturer_warranty_start_invalid_range'),
                    'manufacturer_warranty_end.after': Lang.t('validation.manufacturer_warranty_end_invalid_range'),
                },
                batch: {
                    'qty.required': Lang.t('validation.qty_required'),
                    'qty.numeric': Lang.t('validation.qty_numeric')
                }
            }
        },

        itemManagement: {
            serial: {
                visible: false,
                confirmDelete : new Data({
                    visible: false,
                    value: false,
                    index: 0
                }),
                row: 0,
                message: new Notification
            },
            batch: {
                visible: false,
                confirmDelete : new Data({
                    visible: false,
                    value: false,
                    index: 0
                }),
                row: 0,
                message: new Notification
            },
        },

        service: {
            invoice: new PurchaseService,
            inventoryChain: new PurchaseInboundService,
        },

        attachment: {
            module: MODULE.PURCHASE_INBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.PURCHASE_INBOUND,
                    key: 'detail'
                }
            }
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.PURCHASE,
                        key: 'info'
                    }
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.PURCHASE_INBOUND
        },

        cache: {
            module: MODULE.PURCHASE_INBOUND
        }
    },

    created() {
        this.createInfo({
            supplier_id: '',
            payment_method: '',
            term: 0,
            dr_no: '',
            supplier_invoice_no: '',
            supplier_box_count: '',
            transactionable_id: null,
            transactionable_type: null
        });
    },

    methods: {
        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                supplier_id: this.info.supplier_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            }
        },

        getBarcodeScanApi(params){
            return ProductService.findByBarcodeWithSummaryAndSupplierPrice(params);
        },

        getStockNoApi(params){
            return ProductService.findByStockNoWithSummaryAndSupplierPrice(params);
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                supplier_id: this.info.supplier_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            }
        },

        getSelectProductsApi(params) {
            return ProductService.selectWithSupplierPrice(params);
        },

        getSelectProductsApiParams() {
            return {
                product_ids: this.datatable.product.checked,
                supplier_id: this.info.supplier_id,
                location_id: this.info.for_location,
                branch_id: this.info.created_for
            };
        },

        /**
         * Show serial form and currently encoded data
         *
         * @param  array data
         * @param  int row
         */
        showSerials(data, row) {
            this.itemManagement.serial.visible = true;
            this.datatable.serial.form.value.data = data.serials;
            this.itemManagement.serial.row = row;
            this.datatable.serial.form.value.pagination.current_page = 1;
            this.datatable.serial.form.fields.reset();

            if (this.disabled) {
                this.datatable.serial.form.settings.withActionCell = false;
            }
        },

        /**
         * Show batch form and currently encoded data
         *
         * @param  array data
         * @param  int row
         */
        showBatches(data, row) {
            this.itemManagement.batch.visible = true;
            this.datatable.batch.form.value.data = data.batches;
            this.itemManagement.batch.row = row;
            this.datatable.batch.form.fields.reset();

            if (this.disabled) {
                this.datatable.batch.form.settings.withActionCell = false;
            }
        },

        /**
         * Validate serial fields and its uniqueness
         *
         * @param  array  data
         * @param  int|null  current
         * @return Boolean
         */
        isSerialValid(data, current = null) {
            data = Format.make(data, Object.assign(this.format.serial, {
                expiration_date: data.expiration_date !== null ? 'date' : '',
                manufacturing_date: data.manufacturing_date !== null ? 'date' : '',
                admission_date: data.admission_date !== null ? 'date' : '',
                manufacturer_warranty_start: data.manufacturer_warranty_start !== null ? 'date' : '',
                manufacturer_warranty_end: data.manufacturer_warranty_end !== null ? 'date' : '',
            }));

            var validation = Validator.make(
                data,
                Object.assign(this.validation.rules.serial, {
                    expiration_date: data.expiration_date !== null ? 'date' : '',
                    manufacturing_date: data.manufacturing_date !== null ? 'date' : '',
                    admission_date: data.admission_date !== null ? 'date' : '',
                    manufacturer_warranty_start: data.manufacturer_warranty_start !== null
                        ? 'date|before:manufacturer_warranty_end'
                        : '',
                    manufacturer_warranty_end: data.manufacturer_warranty_end !== null
                        ? 'date|after:manufacturer_warranty_start'
                        : ''
                }),
                this.validation.messages.serial
            );

            if (validation.fails()) {
                this.itemManagement.serial.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (!this.isSerialUnique(data.serial_number, current)) {
                this.itemManagement.serial.message.error.set([Lang.t('validation.serial_no_unique')]);
                return false;
            }

            return true;
        },

        /**
         * Check if currrent batch count qty reaches the total qty of the selected product
         *
         * @return Boolean
         */
        isBatchAlreadyMax() {
            return Numeric.parse(this.maxBatchCount) < (Numeric.parse(this.currentBatchCount));
        },

        /**
         * Validate batch fields and its uniqueness
         *
         * @param  array  data
         * @param  int|null  current
         * @return Boolean
         */
        isBatchValid(data, current = null) {
            data = Format.make(data, this.format.batch);

            var validation = Validator.make(
                data,
                this.validation.rules.batch,
                this.validation.messages.batch
            );

            if (validation.fails() || Numeric.parse(data.qty) === 0) {
                let errors = Obj.toArray(validation.getErrors());

                if (Numeric.parse(data.qty) === 0) {
                    errors.push(Lang.t('validation.qty_greater_than_zero'));
                }

                this.itemManagement.batch.message.error.set(errors);

                return false;
            }

            if (!this.isBatchNameUnique(data.name, current)) {
                this.itemManagement.batch.message.error.set([Lang.t('validation.name_unique')]);
                return false;
            }

            return true;
        },

        /**
         * Add serial to array data if validation passes
         */
        storeSerial() {
            this.itemManagement.serial.message.reset();

            if (!this.isNewSerialValid(this.datatable.serial.form.fields.data())) {
                return false;
            }

            this.datatable.serial.form.value.data.push(Format.make(this.datatable.serial.form.fields.data(), this.format.serial));
            this.datatable.serial.form.fields.reset();

            this.$nextTick(function() {
                this.$refs.serialNumberFormFirstField.focus();
            });
        },

        /**
         * Add batch to array data if validation passes
         */
        storeBatch() {
            this.itemManagement.batch.message.reset();

            if (!this.validBatch(this.datatable.batch.form.fields.data())) {
                return false;
            }

            this.datatable.batch.form.value.data.push(Format.make(this.datatable.batch.form.fields.data(), this.format.batch));
            this.datatable.batch.form.fields.reset();

            this.$nextTick(function() {
                this.$refs.batchFormFirstField.focus();
            });
        },

        /**
         * Update serial to array data if validation passes
         */
        updateSerial(data, index, dataIndex) {
            this.itemManagement.serial.message.reset();

            if(!this.isSerialValid(data, dataIndex)) {
                return false;
            }

            this.$set(this.datatable.details.value.data[this.itemManagement.serial.row].serials, dataIndex, Format.make(data, this.format.serial));

            this.changeState(index, 'destroy', this.datatable.serial.form.id);
        },

        /**
         * Update batch to array data if validation passes
         */
        updateBatch(data, index, dataIndex) {
            this.itemManagement.batch.message.reset();

            if (!this.validBatch(data, dataIndex)) {
                return false;
            }

            this.$set(this.datatable.details.value.data[this.itemManagement.batch.row].batches, dataIndex, Format.make(data, this.format.batch));

            this.changeState(index, 'destroy', this.datatable.batch.form.id);
        },

        /**
         * Show confirm dialog upon click the delete icon in serial form
         *
         * @param  array data
         * @param  int index
         */
        confirmDeleteSerial(data, index, dataIndex) {
            this.itemManagement.serial.confirmDelete.visible = true;
            this.itemManagement.serial.confirmDelete.index = dataIndex;
        },

        /**
         * Show confirm dialog upon click the delete icon in batch form
         *
         * @param  array data
         * @param  int index
         */
        confirmDeleteBatch(data, index, dataIndex) {
            this.itemManagement.batch.confirmDelete.visible = true;
            this.itemManagement.batch.confirmDelete.index = dataIndex;
        },

        /**
         * Remove serial data from the array
         */
        deleteSerial() {
            this.datatable.serial.form.value.data.splice(this.itemManagement.serial.confirmDelete.index, 1);
            this.itemManagement.serial.confirmDelete.visible = false;
        },

        /**
         * Remove batch data from the array
         */
        deleteBatch() {
            this.datatable.batch.form.value.data.splice(this.itemManagement.batch.confirmDelete.index, 1);
            this.itemManagement.batch.confirmDelete.visible = false;
        },

        validBatch(data, current = null) {
            // Check limit on encoded batch
            if(this.isBatchAlreadyMax()) {
                this.itemManagement.batch.message.error.set([Lang.t('validation.batch_count_invalid')]);
                return false;
            }

            return this.isBatchValid(data, current);
        },

        isSerialUnique(serial, current) {
            let serials = [];
            let except = null;

            for(let index in this.datatable.details.value.data) {
                except = index == this.itemManagement.serial.row
                    ? current
                    : null;

                serials = serials.concat(Arr.pluck(this.datatable.details.value.data[index].serials, 'serial_number', except));
            }

            return !Arr.in(serial, serials);
        },

        isBatchNameUnique(batch, current) {
            let batches = [];
            let except = null;

            for(let index in this.datatable.details.value.data) {
                except = index == this.itemManagement.batch.row
                    ? current
                    : null;

                batches = batches.concat(Arr.pluck(this.datatable.details.value.data[index].batches, 'name', except));
            }

            return !Arr.in(batch, batches);
        },

        removeItemManagementDataWarning() {
            return;
        },


        parseSerialData() {
            this.$set(
                this.datatable.details.value.data[this.itemManagement.serial.row],
                'serials',
                Format.make(this.datatable.details.value.data[this.itemManagement.serial.row].serials, this.format.serial)
            );
        },

        parseBatchData() {
            this.$set(
                this.datatable.details.value.data[this.itemManagement.batch.row],
                'batches',
                Format.make(this.datatable.details.value.data[this.itemManagement.batch.row].batches, this.format.batch)
            );
        },

        getExtraInfoForUpdateApproved() {
            return [
                'reason_id',
                'payment_method',
                'term',
                'dr_no',
                'supplier_invoice_no',
                'supplier_box_count'
            ];
        },

        storeScannedSerial(event) {
            let serial = Obj.copy(this.datatable.serial.form.fields.data());

            serial.serial_number = event.target.value;

            this.itemManagement.serial.message.reset();

            if (!this.isNewSerialValid(serial)) {
                event.target.select();
                return false;
            }

            event.target.value = '';

            this.datatable.serial.form.value.data.push(Format.make(serial, this.format.serial));
        },

        isNewSerialValid(serial) {
            if(Numeric.parse(this.maxSerialCount) < (Numeric.parse(this.currentSerialCount) + 1)) {
                this.itemManagement.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            if (!this.isSerialValid(serial)) {
                return false;
            }

            return true;
        },

        closeSerialForm() {
            this.itemManagement.serial.visible = false;
            this.parseSerialData();
            this.itemManagement.serial.row = 0;
            this.itemManagement.serial.confirmDelete.reset();
            this.datatable.serial.form.value.data = [];
            this.$refs.serialFormScan.value = '';
        },

        closeBatchForm() {
            if (this.isAnyBatchesQtyZero()) {
                return false;
            }

            this.itemManagement.batch.visible = false;
            this.parseBatchData();
            this.itemManagement.batch.row = 0;
            this.itemManagement.batch.confirmDelete.reset();
            this.datatable.batch.form.value.data = [];
        },

        isAnyBatchesQtyZero() {
            if (this.datatable.batch.form.value.data.length === 0) {
                return false;
            }

            let errors = [];

            for (let i in this.datatable.batch.form.value.data) {
                let batch = this.datatable.batch.form.value.data[i];
                
                if (Numeric.parse(batch.qty) === 0) {
                    errors.push(Lang.t('error.row') + '[' + (Number(i) + 1) + '] ' + Lang.t('validation.qty_greater_than_zero'));
                }
            }

            if (errors.length > 0) {
                this.itemManagement.batch.message.error.set(errors);
                return true;
            }

            return false;
        }
    },

    watch: {
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.supplier_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = oldValue;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.reference.invoice = this.reference.invoice;
        },
    },

    computed: {
        maxSerialCount: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagement.serial.row] === 'undefined'
                    ? 0
                    : Math.ceil(Numeric.parse(this.datatable.details.value.data[this.itemManagement.serial.row].total_qty))
            );
        },

        maxBatchCount: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagement.batch.row] === 'undefined'
                    ? 0
                    : this.datatable.details.value.data[this.itemManagement.batch.row].total_qty
            );
        },

        currentSerialCount: function() {
            return Numeric.format(this.datatable.serial.form.value.data.length);
        },

        currentBatchCount: function() {
            let count = 0;

            for (var i = 0; i < this.datatable.batch.form.value.data.length; i++) {
                count += Numeric.parse(this.datatable.batch.form.value.data[i].qty);
            }

            count += Numeric.parse(this.datatable.batch.form.fields.qty);

            return Numeric.format(count);
        },
    }
});