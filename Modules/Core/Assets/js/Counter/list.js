import CoreMixins from '../Common/CoreMixins';
import CounterService from '../Services/CounterService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: String(Laravel.auth.branch) },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.customer'),
                        columnName: 'customer_id', //special
                        type: 'select',
                        selectOptions: customers
                    },
                    {
                        name: Lang.t('label.transaction_date_time'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                        ]
                    },
                    { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                ],
                data: []
            },
            columnSettings: {
                id: 'column-settings',
                visible: false,
                selections: {},
                columns: [
                    { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                    { alias: 'created_from', label: Lang.t('label.created_from') },
                    { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                    { alias: 'customer', label: Lang.t('label.customer'), default: true },
                    { alias: 'total_amount', label: Lang.t('label.total_amount'), default: true },
                    { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                    { alias: 'created_by', label: Lang.t('label.created_by') },
                ],
                module: {
                    id: MODULE.COUNTER,
                    key: 'info'
                }
            }
        },

        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),

        message: new Notification
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            CounterService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.records;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        destroy() {
            let that = this;

            if(that.processing || !permissions.delete){
                return false
            }

            that.processing = true;
            that.message.reset();

            CounterService.destroy(that.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.paginate(that.datatable.value.pagination.current_page);
                    that.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.visible = false;
                })
        },

        confirmation(data, index) {
            this.confirm.visible = true
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        create() {
            if(!permissions.create) {
                return false;
            }

            CounterService.create();
        },

        redirect(id) {
            if(!permissions.view_detail) {
                return false;
            }

            CounterService.redirect(id);
        },

        excel(id = null) {
            var params = {
                filters: this.modal.filter.data,
                settings: Object.assign({}, this.modal.columnSettings.selections)
            };

            if(id !== null) {
                delete params['settings'];

                params['template_id'] = id;
            }

            CounterService.export(params);
        }
    }
});