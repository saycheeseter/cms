import CoreMixins from '../Common/CoreMixins';
import CounterService from '../Services/CounterService';
import SalesOutboundService from '../Services/SalesOutboundService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker,
        DateTimePicker
    },

    mixins: [CoreMixins],

    data: {
        info: new Data({
            id: 0,
            sheet_number: '-',
            transaction_date: Moment().format(),
            created_for: Laravel.auth.branch,
            customer_id: null,
            remarks: ''
        }),

        datatable: {
            transactions: {
                settings: {
                    id: 'transactions-table',
                    withPaging : true,
                    perPage: 10,
                    withEdit: false,
                    withPaginateCallBack: false
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
        },

        selector: {
            transaction: {
                visible: false,
                datatable: {
                    id: 'datatable-id',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withActionCell: false,
                        withPaginateCallBack: true,
                        withSelect: {
                            value: 'id'
                        }
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: [],
                },
                columnSettings: {
                    id: 'selector-column-settings',
                    visible: false,
                    columns: [
                        { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                        { alias: 'created_for', label: Lang.t('label.created_for') },
                        { alias: 'for_location', label: Lang.t('label.for_location') },
                        { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                        { alias: 'customer', label: Lang.t('label.customer') },
                        { alias: 'customer_detail', label: Lang.t('label.customer_detail') },
                        { alias: 'salesman', label: Lang.t('label.salesman') },
                        { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                        { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                        { alias: 'created_date', label: Lang.t('label.created_date') },
                        { alias: 'audited_by', label: Lang.t('label.audit_by') },
                        { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                        { alias: 'deleted', label: Lang.t('label.deleted') },
                        { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                        { alias: 'reference', label: Lang.t('label.reference'), },
                        { alias: 'amount', label: Lang.t('label.amount'), default: true },
                        { alias: 'remaining_amount', label: Lang.t('label.remaining_amount'), default: true },
                    ],
                    selections: [],
                    module: {
                        id: MODULE.COUNTER,
                        key: 'transaction'
                    }
                }
            }
        },

        references: {
            branches: branches,
            customers: customers
        },

        confirm: {
            deleteTransactions: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            warning: new Data({
                visible: false,
                value: false
            }),
        },

        format: {
            details: {
                remaining_amount: 'numeric'
            }
        },

        action: new Data({
            buffer: '',
            value: ''
        }),

        dialog: {
            message: ''
        },

        buffer: {
            data: {}
        },
        
        message: new Notification(['transaction', 'selector']),
    },

    mounted: function() {
        this.load();
    },

    methods: {
        load() {
            if(this.processing || id == 'create' || !permissions.view_detail) {
                return false;
            }

            this.processing = true;

            let that = this;
            
            CounterService.show(id)
                .then(function (response){
                    that.info.set(response.data.values.record);
                    that.datatable.transactions.value.data = response.data.values.details.data;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        openTransactionSelector() {
            if(this.info.customer_id == null) {
                this.message.transaction.error.set([Lang.t('validation.customer_required')]);
                return false;
            }

            this.selector.transaction.visible = true;
            this.getTransactions();
        },

        addTransactions(cont = false) {
            if(this.processing) {
                return false;
            }

            if(!cont) {
                this.selector.transaction.visible = false;
            }

            this.processing = true;

            let that = this;
            let ids = this.getUniqueTransactionIds();

            SalesOutboundService.instance().fetchTransactions({ ids: ids })
                .then(function (response) {
                    that.selector.transaction.datatable.selected = [];
                    that.datatable.transactions.value.data = that.datatable.transactions.value.data.concat(
                        that.formatSalesOutboundResponse(response.data.values.transactions.data)
                    );
                    that.processing = false;
                })
                .catch(function (response) {
                    that.message.selector.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getTransactions(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            SalesOutboundService.instance().transactionsWithBalance({
                branch_id: this.info.created_for,
                customer: this.info.customer_id,
                customer_type: CUSTOMER.REGULAR,
                per_page: this.selector.transaction.datatable.settings.perPage,
                page: page
            })
            .then(function (response) {
                that.processing = false;
                that.selector.transaction.datatable.value.data = response.data.values.transactions.data;
                that.selector.transaction.datatable.value.pagination = response.data.values.transactions.meta.pagination;
            })
            .catch(function (response) {
                that.processing = false;
                that.message.selector.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        getUniqueTransactionIds() {
            let existing = this.getExistingTransactionIds();
            let selected = this.selector.transaction.datatable.selected;
            let difference = Arr.difference(selected, existing);
            
            return Arr.intersection(selected, difference);
        },

        getExistingTransactionIds() {
            let ids = [];

            for(let index in this.datatable.transactions.value.data) {
                let row = this.datatable.transactions.value.data[index];
                ids.push(row.reference_id);
            }

            return ids;
        },

        process() {
            if (this.info.id === 0) {
                if(!permissions.create) {
                    return false;
                }

                this.store();
            } else {
                if(!permissions.update) {
                    return false;
                }

                this.update();
            }
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            CounterService.store(this.data)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.info.id = response.data.values.record.id;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.message.reset();
            
            CounterService.update(this.data, id)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        confirmRemoveTransaction(data, index, dataIndex) {
            this.confirm.deleteTransactions.visible = true;
            this.confirm.deleteTransactions.index = dataIndex;
        },

        removeTransaction() {
            this.datatable.transactions.value.data.splice(this.confirm.deleteTransactions.index, 1);
            this.confirm.deleteTransactions.visible = false;
        },

        getTotalByKey(array, key) {
            if(array.length > 0) {
                let total = Numeric.parse(0);

                for(let index in array) {
                    let row = array[index];

                    total += Numeric.parse(row[key]);
                }

                return Numeric.format(total);
            } else {
                return Numeric.format(0);
            }
        },

        notify(action) {
            if(this.datatable.transactions.value.data.length > 0) {
                this.confirm.warning.visible = true;
                this.action.buffer = action;
            } else {
                this.action.value = action;
            }
        },

        removeAllTransactions() {
            this.datatable.transactions.value.data = [];
            this.confirm.warning.visible = false;
        },

        revertInfo() {
            this.info.set(this.buffer.data);
            this.confirm.warning.visible = false;
            this.action.reset();
        },

        redirect() {
            CounterService.redirect(this.info.id);
        },

        formatSalesOutboundResponse(data) {
            let returnArray = [];

            for(let index in data) {
                returnArray.push({
                    reference_id: data[index]['reference_id'],
                    transaction_date: data[index]['transaction_date'],
                    sheet_number: data[index]['sheet_number'],
                    remaining_amount: data[index]['remaining']
                });
            }

            return returnArray;
        },

        print(template) {
            if(this.info.id == 0 || !permissions.print) {
                return false;
            }

            CounterService.print({
                id: this.info.id,
                template: template
            });
        },
    },

    watch: {
        'info.customer_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.created_for = oldValue;
        },
        'selector.transaction.visible': function(value) {
            if(!value) {
                this.selector.transaction.datatable.selected = [];
                this.selector.transaction.datatable.value.data = [];
            }
        },
        'confirm.warning.visible': function(visible) {
            if(!visible) {
                this.confirm.warning.reset();
            }
        },
    },

    computed: {
        totalSelectedRemainingAmount: function() {
            return this.getTotalByKey(this.datatable.transactions.value.data, 'remaining_amount');
        },
        data: function() {
            return {
                info: this.info,
                details: Format.make(this.datatable.transactions.value.data, this.format.details)
            };
        },
        exists: function() {
            return id !== 'create';
        }
    }
});