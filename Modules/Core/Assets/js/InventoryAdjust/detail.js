import InventoryFlow from '../InventoryFlow/detail.js';
import InventoryAdjustService from '../Services/InventoryAdjustService';
import ProductService from '../Services/ProductService';
import Arr from '../Classes/Arr';

let app = new Vue({
    el: '#app',

    mixins: [InventoryFlow],

    data: {
        columnSettings: {
            details: {
                columns: [
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true },
                    { alias: 'stockNo', label: Lang.t('label.stock_no'), default: true},
                    { alias: 'chineseName', label: Lang.t('label.chinese_name') },
                    { alias: 'remarks', label: Lang.t('label.remarks') },
                ],
                module: {
                    id: MODULE.INVENTORY_ADJUST,
                    key: 'detail'
                }
            }
        },

        format: {
            detail: {
                qty: 'numeric',
                pc_qty: 'numeric',
                price: 'numeric',
                inventory: 'numeric',
            },
            serial: {
                expiration_date: 'date',
                manufacturing_date: 'date',
                admission_date: 'date',
                manufacturer_warranty_start: 'date',
                manufacturer_warranty_end: 'date',
            },
            batch: {
                qty: 'numeric'
            }
        },

        validation: {
            rules: {
                detail: {
                    qty: 'numeric',
                    pc_qty: 'numeric',
                    price: 'numeric',
                },
                serial: {
                    serial_number: 'required',
                    expiration_date: 'date',
                    manufacturing_date: 'date',
                    admission_date: 'date',
                    manufacturer_warranty_start: 'date|before:manufacturer_warranty_end',
                    manufacturer_warranty_end: 'date|after:manufacturer_warranty_start',
                },
                batch: {
                    name: 'required',
                    qty: 'required|numeric'
                }
            },
            messages: {
                detail: {
                    'qty.numeric': Lang.t('validation.qty_numeric'),
                    'pc_qty.numeric': Lang.t('validation.pc_qty_numeric'),
                    'price.numeric': Lang.t('validation.price_numeric'),
                },
                serial: {
                    'serial_number.required': Lang.t('validation.serial_no_required'),
                    'expiration_date.date': Lang.t('validation.expiration_date_invalid'),
                    'manufacturing_date.date': Lang.t('validation.manufacturing_date_invalid'),
                    'admission_date.date': Lang.t('validation.admission_date_invalid'),
                    'manufacturer_warranty_start.date': Lang.t('validation.manufacturer_warranty_start_invalid'),
                    'manufacturer_warranty_end.date': Lang.t('validation.manufacturer_warranty_end_invalid'),
                    'manufacturer_warranty_start.before': Lang.t('validation.manufacturer_warranty_start_invalid_range'),
                    'manufacturer_warranty_end.after': Lang.t('validation.manufacturer_warranty_end_invalid_range'),
                },
                batch: {
                    'qty.required': Lang.t('validation.qty_required'),
                    'qty.numeric': Lang.t('validation.qty_numeric')
                }
            }
        },

        buffer: {
            data: {
                created_for: '',
                for_location: ''
            },
            reference: {
                locations: []
            },
            itemManagementSelection: {
                serials: {},
                batches: {},
            },
        },

        discrepancyWarning: {
            visible: false,
            messages: [],
            option: '1',
            action: 'save',
            status: 0
        },

        attachment: {
            module: MODULE.INVENTORY_ADJUST
        },

        service: new InventoryAdjustService,

        key: 'record',

        datatable: {
            serial: {
                form: {
                    id: 'serial-number-form',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withPaginateCallBack: false,
                        withDelete: true
                    },
                    value: {
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            per_page: 10,
                            total: 1,
                            total_pages: 1
                        }
                    },
                    fields: new Data({
                        serial_number: '',
                        expiration_date: null,
                        manufacturing_date: null,
                        admission_date: null,
                        manufacturer_warranty_start: null,
                        manufacturer_warranty_end: null,
                        remarks: null,
                    }),
                },
                list: {
                    id: 'serial-number-list',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                },
                selected: {
                    id: 'serial-number-selected',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                }
            },
            batch: {
                form: {
                    id: 'batch-form',
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withPaginateCallBack: false,
                        withDelete: true
                    },
                    value: {
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            per_page: 10,
                            total: 1,
                            total_pages: 1
                        }
                    },
                    fields: new Data({
                        name: '',
                        qty: 0
                    }),
                },
                list: {
                    id: 'batch-number-list',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                },
                selected: {
                    id: 'batch-number-selected',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                }
            }
        },

        itemManagement: {
            serial: new Data({
                visible: false,
                confirmDelete : {
                    visible: false,
                    value: false,
                    index: 0
                },
                row: 0,
                message: new Notification
            }),
            batch: new Data({
                visible: false,
                confirmDelete: {
                    visible: false,
                    value: false,
                    index: 0
                },
                row: 0,
                message: new Notification
            }),
        },

        itemManagementSelection: {
            serial: new Data({
                visible: false,
                message: new Notification,
                row: 0,
                searchString: '',
            }),
            batch: new Data({
                visible: false,
                message: new Notification,
                row: 0,
                searchString: '',
            })
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.INVENTORY_ADJUST
        },

        cache: {
            module: MODULE.INVENTORY_ADJUST
        },

        filter: {
            product: {
                forced: [
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_GROUP
                    },
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_UNGROUP
                    },
                    {
                        column: 'manage_type',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_MANAGE_TYPE.BATCH
                    },
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
    },

    created() {
        this.createInfo();
    },

    methods: {
        formatAttributesToTransactionDetails(details, override = {}, hook = null) {
            let collection = [];

            details = Arr.wrap(details);

            for(let i = 0; i < details.length; i++) {
                let detail = details[i];
                let formatted = {
                    id: detail.hasOwnProperty('id') ? detail.id: 0,
                    manage_type: detail.manage_type,
                    product_id: detail.hasOwnProperty('product_id') ? detail.product_id : 0,
                    barcode_list: detail.barcode_list,
                    barcode: detail.barcode,
                    name: detail.name,
                    stock_no: detail.stock_no,
                    chinese_name: detail.chinese_name,
                    units: detail.units,
                    unit_id: detail.unit_id,
                    inventory: detail.inventory,
                    unit_qty: detail.unit_qty,
                    old_unit: detail.hasOwnProperty('old_unit') ? detail.old_unit : true,
                    qty: detail.hasOwnProperty('qty') ? detail.qty : Numeric.format(0),
                    pc_qty: detail.hasOwnProperty('pc_qty') ? detail.pc_qty : Numeric.format(0),
                    price: detail.price,
                    cost: detail.cost,
                    remarks: detail.hasOwnProperty('remarks') ? detail.remarks : '',
                    serials: detail.hasOwnProperty('serials') ? detail.serials : [],
                    source: detail.hasOwnProperty('source') ? detail.source : TRANSACTION_SOURCE.WEB
                };

                formatted['total_qty'] = Numeric.format(
                    (Numeric.parse(formatted['unit_qty'])
                        * Numeric.parse(formatted['qty']))
                    + Numeric.parse(formatted['pc_qty'])
                );

                formatted['diff_qty'] = Numeric.format(
                    Numeric.parse(formatted['total_qty'])
                    - Numeric.parse(formatted['inventory'])
                );

                formatted['prev_diff_qty'] = formatted.diff_qty;

                formatted['amount'] = Numeric.format(
                    Numeric.parse(formatted['diff_qty']) *
                    Numeric.parse(formatted['price'])
                );

                if (hook !== null) {
                    formatted = hook(formatted, detail);
                }

                collection.push(Object.assign({}, formatted, override));
            }

            return collection;
        },

        process(reload = false) {
            this.reload = reload;
            let that = this;

            if (!this.validate() 
                || (this.info.id === 0 && ! permissions.create)
                || (this.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            this.discrepancyWarning.action = 'save';
            this.processing = true;
            this.cacheData();

            // If there are no discrepancy, trigger the save (create | update)
            this.service.checkInventoryDiscrepancy(this.data)
                .then(function (response) {
                    that.clearCache();
                    that.processing = false;
                    that.save();
                })
                .catch(that.inventoryDiscrepancyError);
        },

        save() {
            if (this.info.id === 0) {
                this.store(this.data);
            } else {
                this.update(this.data);
            }
        },

        proceed() {
            this.discrepancyWarning.visible = false;

            if (this.discrepancyWarning.option === '2') {
                this.refreshInventory();
                return false;
            }

            if (this.discrepancyWarning.action === 'save') {
                this.save();
            } else {
                this.assent(this.discrepancyWarning.status);
            }
        },

        inventoryDiscrepancyError(error) {
            if (error.response.data.errors.hasOwnProperty('inventory.warning')) {
                this.discrepancyWarning.visible = true;
                this.discrepancyWarning.messages = error.response.data.errors['inventory.warning'];
            } else {
                this.message.transaction.error.set(Obj.toArray(error.response.data.errors));
            }
            
            this.processing = false;
            this.clearCache();
        },

        onCompleteBarcodeScanRequest(response, event) {
            if(this.isProductsExists([response.data.values.product.id])) {
                event.target.select();
                this.processing = false;
                return false;
            }
        },

        beforeProductSelect() {
            if (this.isProductsExists(this.datatable.product.checked)) {
                this.processing = false;
                return false;
            }
        },

        onCompleteStockNoScanRequest(response, event){
            if(this.isProductsExists([response.data.values.product.id])) {
                event.target.select();
                this.processing = false;
                return false;
            }
        },

        setTotalQty(index) {
            let row = this.datatable.details.value.data[index];

            this.$set(row, 'total_qty', Numeric.format(
                (Numeric.parse(row['unit_qty'])
                    * Numeric.parse(row['qty']))
                + Numeric.parse(row['pc_qty'])
            ));

            this.setDiffQty(index);
            this.setAmount(index);
        },

        setDiffQty(index) {
            let row = this.datatable.details.value.data[index];

            this.$set(row, 'diff_qty', Numeric.format(
                Numeric.parse(row['total_qty'])
                - Numeric.parse(row['inventory'])
            ));
        },

        setPrevDiffQty(data) {
            data.prev_diff_qty = Numeric.parse(data.diff_qty);
        },

        setAmount(index) {
            let row = this.datatable.details.value.data[index];

            this.$set(row, 'amount', Numeric.format(
                Numeric.parse(row['diff_qty']) *
                Numeric.parse(row['price'])
            ));
        },

        /**
         * Check if selected products already exists in the table
         * 
         * @param  array  selected
         * @return bool
         */
        isProductsExists(selected) {
            let products = Arr.pluck(this.datatable.details.value.data, 'product_id');

            if (Arr.intersection(products, selected).length > 0) {
                this.message.transaction.error.set([Lang.t('validation.selected_products_already_exists')]);
                return true;
            }

            return false;
        },

        /**
         * Overrided approval process to initiate checking of inventory discrepancy
         * before triggering the approval request
         * 
         * @param  int status
         */
        approval(status) {
            let that = this;

            if(this.processing || !this.validate()) {
                return false;
            }

            this.discrepancyWarning.action = 'approval';
            this.discrepancyWarning.status = status;
            this.processing = true;

            // Do not trigger the checker if status action is decline or
            // revert
            if (Arr.in(
                Number(status), 
                [APPROVAL_STATUS.DECLINED, APPROVAL_STATUS.DRAFT])
            ) {
                that.assent(status);
                return false; 
            }

            this.cacheData();

            // If there are no discrepancy, trigger the assent
            this.service.checkInventoryDiscrepancy(this.data)
                .then(function (response) {
                    that.clearCache();
                    that.processing = false;
                    that.assent(status);
                })
                .catch(that.inventoryDiscrepancyError);
        },

        assent(status) {
            let that = this;

            this.processing = true;
            this.message.reset();
            
            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL:
                    this.cacheData();

                    let update = this.service.update(this.data, that.info.id);

                    axios.all([update])
                        .then(axios.spread(function (update){
                            that.clearCache();
                            that.approvalRequest(status);
                        }))
                        .catch(function (error) {
                            that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                            that.processing = false;
                            that.clearCache();
                        });
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        /**
         * Get the updated inventory value of the encoded products and replace the
         * details in the table
         */
        refreshInventory() {
            let that = this;

            let products = Arr.pluck(this.datatable.details.value.data, 'product_id');

            this.processing = true;
            this.cacheData();

            ProductService.summaries({
                product_ids: products,
                branch_id: this.info.created_for,
                location_id: this.info.for_location
            })
            .then(function (response) {
                that.clearCache();
                that.replaceOldInventory(response.data.values.products);
                that.processing = false;
            })
            .catch(function (error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
                that.clearCache();
            });
        },

        /**
         * Replace the old inventory of the encoded products with the currently
         * given one
         * 
         * @param  array products 
         */
        replaceOldInventory(products) {
            for (var i = 0; i < this.datatable.details.value.data.length; i++) {
                let current = this.datatable.details.value.data[i];

                for (var x = 0; x < products.length; x++) {
                    let product = products[x];

                    if (current.product_id == product.id) {
                        current.inventory = product.inventory;
                        break;
                    }
                }
            }
        },

        validateDetailQty() {
            return true;
        },

        showItemManagement(data, row) {
            if (this.processing) {
                return false;
            }

            if (data.manage_type === PRODUCT_MANAGE_TYPE.SERIAL) {
                if (Numeric.parse(data.diff_qty) > 0) {
                    this.showSerialForm(data, row);
                } else if (Numeric.parse(data.diff_qty) < 0) {
                    this.showSerialSelection(data, row);
                }
            } else {
                //this.showBatches(data, row);
            }
        },

        showSerialForm(data, row) {
            this.itemManagement.serial.visible = true;
            this.datatable.serial.form.value.data = data.serials;
            this.itemManagement.serial.row = row;
            this.datatable.serial.form.value.pagination.current_page = 1;
            this.datatable.serial.form.fields.reset();

            if (this.disabled) {
                this.datatable.serial.form.settings.withActionCell = false;
            }
        },

        showSerialSelection(data, row) {
            this.processing = true;
            this.datatable.serial.selected.value.data = data.serials;

            if (!this.disabled) {
                if (!this.buffer.itemManagementSelection.serials.hasOwnProperty(data.product_id)) {
                    this.getNewSerials(data.product_id);
                } else {
                    this.datatable.serial.list.value.data = this.buffer.itemManagementSelection.serials[data.product_id];
                }
            } else {
                this.datatable.serial.selected.settings.withSelect.value = false;
            }

            this.processing = false;
            this.itemManagementSelection.serial.visible = true;
            this.itemManagementSelection.serial.row = row;
        },

        getNewSerials(id) {
            let that = this;

            ProductService.serials(Object.assign({product_id: id}, this.getSerialsConstraint()))
                .then(function (response) {
                    let serials = that.filterSerials(response.data.values.serials);

                    that.buffer.itemManagementSelection.serials[id] = serials;
                    that.datatable.serial.list.value.data = that.buffer.itemManagementSelection.serials[id];
                    that.processing = false;
                })
                .catch(function (error) {
                    //that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getSerialsConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.BRANCH_DETAIL,
                entity: this.info.for_location,
                status: SERIAL_STATUS.AVAILABLE,
                transaction: null
            };
        },

        filterSerials(serials) {
            let selected = [];
            let filtered = [];

            for (let index in this.datatable.details.value.data) {
                selected = selected.concat(Arr.pluck(this.datatable.details.value.data[index].serials, 'id'));
            }

            for (let index in serials) {
                let current = serials[index];

                if (Arr.in(current.id, selected)) {
                    continue;
                }

                filtered.push(current);
            }

            return filtered;
        },
        /**
         * Show confirm dialog upon click the delete icon in serial form
         *
         * @param  array data
         * @param  int index
         */
        confirmDeleteSerial(data, index, dataIndex) {
            this.itemManagement.serial.confirmDelete.visible = true;
            this.itemManagement.serial.confirmDelete.index = dataIndex;
        },

        /**
         * Add serial to array data if validation passes
         */
        storeSerial() {
            this.itemManagement.serial.message.reset();

            if (!this.isNewSerialValid(this.datatable.serial.form.fields.data())) {
                return false;
            }

            this.datatable.serial.form.value.data.push(Format.make(this.datatable.serial.form.fields.data(), this.format.serial));
            this.datatable.serial.form.fields.reset();

            this.$nextTick(function() {
                this.$refs.serialNumberFormFirstField.focus();
            });
        },

        /**
         * Update serial to array data if validation passes
         */
        updateSerial(data, index, dataIndex) {
            this.itemManagement.serial.message.reset();

            if(!this.isSerialValid(data, dataIndex)) {
                return false;
            }

            this.$set(this.datatable.details.value.data[this.itemManagement.serial.row].serials, dataIndex, Format.make(data, this.format.serial));

            this.changeState(index, 'destroy', this.datatable.serial.form.id);
        },

        isSerialValid(data, current = null) {
            data = Format.make(data, Object.assign(this.format.serial, {
                expiration_date: data.expiration_date !== null ? 'date' : '',
                manufacturing_date: data.manufacturing_date !== null ? 'date' : '',
                admission_date: data.admission_date !== null ? 'date' : '',
                manufacturer_warranty_start: data.manufacturer_warranty_start !== null ? 'date' : '',
                manufacturer_warranty_end: data.manufacturer_warranty_end !== null ? 'date' : '',
            }));

            let validation = Validator.make(
                data,
                Object.assign(this.validation.rules.serial, {
                    expiration_date: data.expiration_date !== null ? 'date' : '',
                    manufacturing_date: data.manufacturing_date !== null ? 'date' : '',
                    admission_date: data.admission_date !== null ? 'date' : '',
                    manufacturer_warranty_start: data.manufacturer_warranty_start !== null
                        ? 'date|before:manufacturer_warranty_end'
                        : '',
                    manufacturer_warranty_end: data.manufacturer_warranty_end !== null
                        ? 'date|after:manufacturer_warranty_start'
                        : ''
                }),
                this.validation.messages.serial
            );

            if (validation.fails()) {
                this.itemManagement.serial.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (!this.isSerialUnique(data.serial_number, current)) {
                this.itemManagement.serial.message.error.set([Lang.t('validation.serial_no_unique')]);
                return false;
            }

            return true;
        },

        isSerialUnique(serial, current) {
            let serials = [];
            let except = null;

            for(let index in this.datatable.details.value.data) {
                except = Number(index) === Number(this.itemManagement.serial.row)
                    ? current
                    : null;

                serials = serials.concat(Arr.pluck(this.datatable.details.value.data[index].serials, 'serial_number', except));
            }

            return !Arr.in(serial, serials);
        },

        /**
         * Remove serial data from the array
         */
        deleteSerial() {
            this.datatable.serial.form.value.data.splice(this.itemManagement.serial.confirmDelete.index, 1);
            this.itemManagement.serial.confirmDelete.visible = false;
        },

        parseSerialData() {
            this.$set(
                this.datatable.details.value.data[this.itemManagement.serial.row],
                'serials',
                Format.make(this.datatable.details.value.data[this.itemManagement.serial.row].serials, this.format.serial)
            );
        },

        putSerials() {
            if (this.isSerialsLimitReached()) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            this.moveSerials(this.datatable.serial.list.value, this.datatable.serial.selected.value);
        },

        returnSerials() {
            this.moveSerials(this.datatable.serial.selected.value, this.datatable.serial.list.value);
        },

        moveSerials(base, target) {
            for (let index in base.selected) {
                let selected = base.selected[index];

                for (let i = base.data.length - 1; i >= 0; i--) {
                    let serial = base.data[i];

                    if (Number(selected) !== Number(serial.id)) {
                        continue;
                    }

                    target.data.push(serial);
                    base.data.splice(i, 1);
                }
            }

            base.selected = [];
            target.selected = [];
        },

        putAllSerials() {
            if (this.isSerialsLimitReachedAll()) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            this.moveAllSerials(this.datatable.serial.list.value, this.datatable.serial.selected.value);
        },

        returnAllSerials() {
            this.moveAllSerials(this.datatable.serial.selected.value, this.datatable.serial.list.value);
        },

        moveAllSerials(base, target) {
            for (var i = base.data.length - 1; i >= 0; i--) {
                let serial = base.data[i];

                target.data.push(serial);
                base.data.splice(i, 1);
            }

            base.selected = [];
            target.selected = [];
        },

        isSerialsLimitReached() {
            return (Numeric.parse(this.datatable.serial.list.value.selected.length)
                + Numeric.parse(this.selectedSerialCount))
                > Numeric.parse(this.availableSerialQty);
        },

        isSerialsLimitReachedAll() {
            return (Numeric.parse(this.datatable.serial.list.value.data.length)) 
                > Numeric.parse(this.availableSerialQty);
        },

        clearItemManagementData(data) {
            if ((Numeric.parse(data.prev_diff_qty) > 0 && Numeric.parse(data.diff_qty) > 0)
                || (Numeric.parse(data.prev_diff_qty) < 0 && Numeric.parse(data.diff_qty) < 0)) {
                return false;
            }

            data.serials = [];

            if (this.buffer.itemManagementSelection.serials.hasOwnProperty(data.product_id)) {
                delete this.buffer.itemManagementSelection.serials[data.product_id];
            }
        },

        onSuccessfulValidation() {
            return this.isSerialsQtyComplete();
        },

        isSerialsQtyComplete() {
            let complete = true;
            let errors = [];

            for (let index in this.datatable.details.value.data) {
                let current = this.datatable.details.value.data[index];

                if (current.manage_type === PRODUCT_MANAGE_TYPE.SERIAL
                    && Math.ceil(Math.abs(Numeric.parse(current.diff_qty))) !== current.serials.length
                ) {
                    errors.push(Lang.t('error.row') + '[' + (Number(index) + 1) + '] ' + Lang.t('validation.serial_count_invalid'));
                    complete = false;
                }
            }

            if (errors.length > 0) {
                this.message.transaction.error.set(errors);
            }

            return complete;
        },

        isDiffQtyZero(diffQty) {
            return Numeric.parse(diffQty) === 0;
        },

        searchSerialSelectionByString(event) {
            this.itemManagementSelection.serial.searchString = event.target.value;
        },

        searchBatchSelectionByString(event) {
            this.itemManagementSelection.batch.searchString = event.target.value;
        },

        scanSerialSelection(event) {
            this.datatable.serial.list.value.selected = [];
            this.itemManagementSelection.serial.message.reset();

            for (let i in this.datatable.serial.list.value.data) {
                let serial = this.datatable.serial.list.value.data[i];

                if (serial.serial_number === event.target.value) {
                    this.datatable.serial.list.value.selected.push(serial.id);
                    break;
                }
            }

            if (this.datatable.serial.list.value.selected.length === 0) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_not_found')]);
                event.target.select();
                return false;
            }

            this.putSerials();

            event.target.value = '';
        },

        storeScannedSerial(event) {
            let serial = Obj.copy(this.datatable.serial.form.fields.data());

            serial.serial_number = event.target.value;

            this.itemManagement.serial.message.reset();

            if (!this.isNewSerialValid(serial)) {
                event.target.select();
                return false;
            }

            event.target.value = '';

            this.datatable.serial.form.value.data.push(Format.make(serial, this.format.serial));
        },

        isNewSerialValid(serial) {
            if(Numeric.parse(this.maxSerialCount) < (Numeric.parse(this.currentSerialCount) + 1)) {
                this.itemManagement.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            if (!this.isSerialValid(serial)) {
                return false;
            }

            return true;
        },

        closeSerialSelection() {
            this.itemManagementSelection.serial.visible = false;
            this.itemManagementSelection.serial.reset();
            this.datatable.serial.list.value.data = [];
            this.datatable.serial.list.value.selected = [];
            this.datatable.serial.selected.value.data = [];
            this.datatable.serial.selected.value.selected = [];
            this.$refs.serialSelectionSearchString.value = '';
            this.$refs.serialSelectionScan.value = '';
        },

        closeBatchSelection() {
            // this.itemManagementSelection.batch.visible = false;
            // this.itemManagementSelection.batch.reset();
            // this.datatable.batch.list.value.data = [];
            // this.datatable.batch.list.value.selected = [];
            // this.datatable.batch.selected.value.data = [];
            // this.datatable.batch.selected.value.selected = [];
            // this.$refs.batchSelectionSearchString.value = '';
        },

        closeSerialForm() {
            this.itemManagement.serial.visible = false;
            this.parseSerialData();
            this.itemManagement.serial.reset();
            this.datatable.serial.form.value.data = [];
            this.$refs.serialFormScan.value = '';
        },

        closeBatchForm() {
            // this.itemManagement.batch.visible = false;
            // this.parseBatchData();
            // this.itemManagement.batch.row = 0;
            // this.itemManagement.batch.confirmDelete.reset();
            // this.datatable.batch.form.value.data = [];
        },

        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    manage_type: PRODUCT_MANAGE_TYPE.BATCH,
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    manage_type: PRODUCT_MANAGE_TYPE.BATCH,
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        }
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'created-for':
                    this.getLocations();
                    this.reference.invoice = [];
                    break;
            }
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.created_for = oldValue;
            this.buffer.data.for_location = this.info.for_location;
            this.buffer.reference.locations = this.reference.locations;
        },
        'info.for_location': function(newValue, oldValue) {
            this.buffer.data.for_location = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.reference.locations = this.reference.locations;
        },
    },

    computed: {
        totalQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['total_qty']);
            }

            return Numeric.format(total);
        },
        totalLostQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                let value = Numeric.parse(data[index]['diff_qty']);

                if (value < 0) {
                    total += value * -1;
                }
            }

            return Numeric.format(total);
        },
        totalGainQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                let value = Numeric.parse(data[index]['diff_qty']);

                if (value > 0) {
                    total += value;
                }
            }

            return Numeric.format(total);
        },
        totalGainLostQty: function () {
            let total = Numeric.parse(this.totalGainQty) - Numeric.parse(this.totalLostQty);

            return Numeric.format(total);
        },
        totalGainLostAmount: function () {
            let total = Numeric.parse(this.totalGainCost) - Numeric.parse(this.totalLostCost);

            return Numeric.format(total);
        },
        totalLostCost: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                let amount = Numeric.parse(data[index]['amount']);

                if (amount < 0) {
                    total += amount * -1;
                }
            }

            return Numeric.format(total);
        },
        totalGainCost: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                let amount = Numeric.parse(data[index]['amount']);

                if (amount > 0) {
                    total += amount;
                }
            }

            return Numeric.format(total);
        },

        isForApprovalOrDecline: function() {
            return Arr.in(Number(this.info.approval_status), [
                APPROVAL_STATUS.FOR_APPROVAL,
                APPROVAL_STATUS.DECLINED
            ])
        },

        maxSerialCount: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagement.serial.row] === 'undefined'
                    ? 0
                    : Math.ceil(Math.abs(Numeric.parse(this.datatable.details.value.data[this.itemManagement.serial.row].diff_qty)))
            );
        },

        currentSerialCount: function() {
            return Numeric.format(this.datatable.serial.form.value.data.length);
        },

        availableSerialQty: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagementSelection.serial.row] === 'undefined'
                    ? 0
                    : Math.ceil(Math.abs(this.datatable.details.value.data[this.itemManagementSelection.serial.row].diff_qty))
            );
        },

        selectedSerialCount: function() {
            return Numeric.format(this.datatable.serial.selected.value.data.length);
        },

        hasSerialOrBatch: function () {
            let count = 0;

            for (let i in this.datatable.details.value.data) {
                let row = this.datatable.details.value.data[i];

                if (row.manage_type !== PRODUCT_MANAGE_TYPE.NONE) {
                    count++;
                    break;
                }
            }

            return count > 0;
        }
    }
});
