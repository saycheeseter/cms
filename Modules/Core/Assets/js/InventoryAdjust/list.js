import InventoryFlow from '../InventoryFlow/list.js';
import InventoryAdjustService from '../Services/InventoryAdjustService';
import QueryStringLocation from "../Classes/QueryStringLocation";

let app = new Vue({
    el: '#app',

    mixins: [InventoryFlow],

    data: {
        key: 'records',

        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'lost_amount', label: Lang.t('label.lost_amount'), default: true },
                { alias: 'gain_amount', label: Lang.t('label.gained_amount'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.INVENTORY_ADJUST,
                key: 'info'
            }
        },

        filter: {
            options: [
                {
                    name: Lang.t('label.branch'),
                    columnName: 'created_for',
                    type: 'select',
                    selectOptions: branches,
                    selected: true,
                    default: { value: String(Laravel.auth.branch) },
                    freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                },
                {
                    name: Lang.t('label.for_location'),
                    columnName: 'for_location',
                    type: 'select',
                    selectOptions: locations
                },
                {
                    name: Lang.t('label.request_by'),
                    columnName: 'requested_by',
                    type: 'select',
                    selectOptions: users
                },
                { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string' },
                { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string' },
                {
                    name: Lang.t('label.transaction_date_time'),
                    columnName: 'transaction_date',
                    type: 'datetime',
                    selected: true,
                    default: [
                        { comparison: '>=', value: Moment().startOf('day').format() },
                        { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                    ]
                },
                {
                    name: Lang.t('label.approval_status'),
                    columnName: 'approval_status',
                    type: 'select',
                    selectOptions: [
                        { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                        { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                        { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                        { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                    ]
                },
                {
                    name: Lang.t('label.deleted'),
                    columnName: 'deleted_at',
                    type: 'select',
                    selected: true,
                    selectOptions: [
                        { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                        { id: String(VOIDED.NO), label: Lang.t('label.no') },
                    ],
                    default: { value: String(VOIDED.NO) }
                }
            ],
        },

        service: new InventoryAdjustService,
        cache: {
            module: MODULE.INVENTORY_ADJUST
        }
    },

    created: function() {
        (new QueryStringLocation({
            created_for: 'created_for',
            transaction_date: {
                type: 'datetime',
                column: 'year_month'
            }
        }))
        .setDefaultOnFilters(this.filter.options);
    },
});
