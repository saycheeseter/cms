import Invoice from '../Invoice/detail.js';
import AutoGroupProductBreakdownMixins from '../Common/AutoGroupProductBreakdownMixins.js';
import PurchaseService from '../Services/PurchaseService';
import ProductService from '../Services/ProductService';

let app = new Vue({
    el: '#app',

    mixins: [Invoice, AutoGroupProductBreakdownMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '=',
                        value: PRODUCT_STATUS.ACTIVE
                    },
                ]
            }
        },

        buffer: {
            data: {
                supplier_id: ''
            }
        },

        format: {
            info: {
                term: 'numeric',
                deliver_until: 'date'
            }
        },

        reference: {
            tsuppliers: tsuppliers,
            paymentMethods: paymentMethods,
            branchDetails: branchDetails,
        },

        service: new PurchaseService,

        attachment: {
            module: MODULE.PURCHASE
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.PURCHASE,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.PURCHASE
        }
    },

    created() {
        this.createInfo({
            term: 0,
            supplier_id: '',
            payment_method: '',
            deliver_until: Moment().format('YYYY-MM-DD'),
            deliver_to: '',
            transactionable_type: null,
            transactionable_id: null,
            transactionable_sheet_number: null,
        });
    },

    methods: {
        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                supplier_id: this.info.supplier_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            }
        },

        getBarcodeScanApi(params){
            return ProductService.findByBarcodeWithSummaryAndSupplierPrice(params);
        },

        getStockNoApi(params){
            return ProductService.findByStockNoWithSummaryAndSupplierPrice(params);
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                supplier_id: this.info.supplier_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            }
        },

        getSelectProductsApi(params) {
            return ProductService.selectWithSupplierPrice(params);
        },

        getSelectProductsApiParams() {
            return {
                product_ids: this.datatable.product.checked,
                supplier_id: this.info.supplier_id,
                location_id: this.info.for_location,
                branch_id: this.info.created_for
            };
        },

        onSuccessfulLoad(response) {
            if (response.data.values[this.key].transactionable_type !== null) {
                this.controls.create = false;
            }
        },

        getExtraInfoForUpdateApproved() {
            return [
                'payment_method',
                'term'
            ];
        }
    },

    watch: {
        'info.supplier_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
    },
});