import CoreMixins from '../Common/CoreMixins';
import GiftCheckService from '../Services/GiftCheckService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        fields: new Data({
            id: 0,
            code: '',
            check_number: '',
            date_from: new Date(),
            date_to: new Date(),
            amount: Numeric.format(0),
            status: GIFT_CHECK_STATUS.AVAILABLE,
            status_label: Lang.t('label.new'),
            redeemed_by: '-',
            redeemed_date: '-',
            reference_number: '-'
        }),
        bulkEntry: {
            id: 'bulk-entry',
            visible: false,
            fields: new Data({
                code_series: '',
                check_number_series: '',
                date_from: new Date(),
                date_to: new Date(),
                amount: Numeric.format(0),
                qty: 1
            })
        },
        format: {
            amount: 'numeric',
            date_from: 'date',
            date_to: 'date',
        },
        message: new Notification(['list', 'details', 'bulkEntry']),
        filter: {
            visible: false,
            options: [
                { name: Lang.t('label.code'), columnName: 'code', type: 'string' },
                { name: Lang.t('label.check_no'), columnName: 'check_number', type: 'string' },
                {
                    name: Lang.t('label.status'),
                    columnName: 'status',
                    type: 'select',
                    selectOptions: [
                        { id: GIFT_CHECK_STATUS.AVAILABLE, label: Lang.t('label.available') },
                        { id: GIFT_CHECK_STATUS.REDEEMED, label: Lang.t('label.redeemed') },
                    ],
                }
            ],
            data: []
        },
        confirmation: {
            delete: {
                visible: false,
                value: false,
                id: 0,
                index: 0
            },
        },
        form: {
            id: 'modal-detail',
            visible: false
        },
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.filter.visible = false;

            let that = this;

            that.processing = true;
            that.message.reset();

            GiftCheckService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.list.info = response.data.message;
                that.datatable.value.data = response.data.values.gift_checks;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            GiftCheckService.store(Format.make(that.fields.data(), that.format))
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;

                    setTimeout(function() {
                        that.form.visible = false;
                        that.datatable.value.data.push(response.data.values.gift_check);
                        that.computePagination(that.datatable.value.pagination, 'add');
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            GiftCheckService.update(Format.make(that.fields.data(), that.format), that.fields.id)
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;

                    setTimeout(function() {
                        that.form.visible = false;
                        that.paginate(that.datatable.value.pagination.current_page);
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            GiftCheckService.destroy(that.confirmation.delete.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.list.success = response.data.message;
                    that.datatable.value.data.splice(that.confirmation.delete.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.confirmation.delete.visible = false;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirmation.delete.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            this.processing = true;

            GiftCheckService.show(id)
                .then(function (response) {
                    that.fields.set(response.data.values.gift_check);
                    that.processing = false;
                    that.form.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        save() {
            if(!this.valid()) {
                return false;
            }

            if(this.fields.id == 0){
                this.store();
            } else {
                this.update();
            }
        },

        valid() {
            if (this.fields.id != 0 && !permissions.update) {
                return false;
            }

            var validation = Validator.make(Format.make(this.fields.data(), this.format), {
                code: 'required',
                check_number: 'required',
                amount: 'required|numeric|gtzero',
                date_from: 'required|date',
                date_to: 'required|date',
            }, {
                'code.required': Lang.t('validation.code_required'),
                'check_number.required': Lang.t('validation.check_number_required'),
                'amount.required': Lang.t('validation.amount_required'),
                'amount.numeric': Lang.t('validation.amount_numeric'),
                'amount.gtzero': Lang.t('validation.amount_greater_than_zero'),
                'date_from.required': Lang.t('validation.date_from_required'),
                'date_from.date': Lang.t('validation.date_from_invalid_format'),
                'date_to.required': Lang.t('validation.date_to_required'),
                'date_to.date': Lang.t('validation.date_to_invalid_format'),
            });

            validation.extend('gtzero', function(name, value, params) {
                return value > 0
            }, ":attr should be greater than zero.");

            if (validation.fails()) {
                this.message.details.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        },

        confirm(data, index) {
            this.confirmation.delete.visible = true;
            this.confirmation.delete.id = data.id;
            this.confirmation.delete.index = index;
        },

        createBulkEntry() {
            if (! this.validBulkEntry()) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            GiftCheckService.bulkEntry(Format.make(that.bulkEntry.fields.data(), that.format))
                .then(function (response) {
                    that.message.bulkEntry.success = response.data.message;
                    that.processing = false;

                    setTimeout(function() {
                        that.bulkEntry.visible = false;
                        that.paginate();
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.bulkEntry.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validBulkEntry() {
            if (!permissions.create) {
                return false;
            }

            var validation = Validator.make(Format.make(this.bulkEntry.fields.data(), this.format), {
                code_series: 'required',
                check_number_series: 'required',
                amount: 'required|numeric|gtzero',
                qty: 'required|numeric|gtzero',
                date_from: 'required|date',
                date_to: 'required|date',
            }, {
                'code_series.required': Lang.t('validation.code_required'),
                'check_number_series.required': Lang.t('validation.check_number_required'),
                'amount.required': Lang.t('validation.amount_required'),
                'amount.numeric': Lang.t('validation.amount_numeric'),
                'amount.gtzero': Lang.t('validation.amount_greater_than_zero'),
                'qty.required': Lang.t('validation.qty_required'),
                'qty.numeric': Lang.t('validation.qty_numeric'),
                'qty.gtzero': Lang.t('validation.qty_greater_than_zero'),
                'date_from.required': Lang.t('validation.date_from_required'),
                'date_from.date': Lang.t('validation.date_from_invalid_format'),
                'date_to.required': Lang.t('validation.date_to_required'),
                'date_to.date': Lang.t('validation.date_to_invalid_format'),
            });

            validation.extend('gtzero', function(name, value, params) {
                return value > 0
            }, ":attr should be greater than zero.");

            if (validation.fails()) {
                this.message.bulkEntry.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        }
    },
    watch: {
        'confirmation.delete.visible': function(visible) {
            if (!visible) {
                this.confirmation.delete.reset();
            }
        },
        'form.visible': function(visible) {
            if (!visible) {
                this.fields.reset();
                this.message.reset();
            }
        },
        'bulkEntry.visible': function(visible) {
            if (!visible) {
                this.bulkEntry.fields.reset();
                this.message.reset();
            }
        },
    },
    computed: {
        disabled: function() {
            return this.fields.status === GIFT_CHECK_STATUS.REDEEMED;
        }
    }
});