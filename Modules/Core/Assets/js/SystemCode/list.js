import CoreMixins from '../Common/CoreMixins';
import ImportExcel from '../ImportExcel/import-excel';
import SystemCodeService from '../Services/SystemCodeService';
let Service = new SystemCodeService(segment);

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ImportExcel],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        fields: new Data({
            id: 0,
            code: '',
            name: '',
            remarks: '',
        }),
        message: new Notification,
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type:'string' },
                    { name: Lang.t('label.name'), columnName: 'name', type:'string' },
                    { name: Lang.t('label.remarks'), columnName: 'remarks', type:'string' }
                ],
                data: []
            }
        },
        permissions: permissions,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            Service.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.system_codes;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            Service.store(this.fields.data())
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.push(response.data.values.system_code);
                    that.computePagination(that.datatable.value.pagination, 'add');
                    that.toggleFocus();
                    that.fields.reset();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update(data, index) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            Service.update(data, data.id)
                .then(function (response) {
                    that.processing = false;
                    that.changeState(index, 'destroy');
                    that.message.success = response.data.message;
                    that.toggleFocus();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            Service.destroy(this.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.processing = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.modal.confirm.visible = false;
                });
        },

        confirm(data, index) {
            if (data.deletable === SYSTEM_CODE_DELETABLE.NO) {
                return false;
            }

            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        validate(data, index) {
            var data = typeof data === 'undefined' ? this.fields.data() : data;

            var validation = Validator.make(data, {
                name: 'required'
            }, {
                'name.required' : Lang.t('validation.name_required'),
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (data.id === 0) {
                this.store();
            } else {
                this.update(data, index);
            }
        },

        onSuccessfulImport() {
            this.paginate();
        },

        importExcelService() {
            return Service;
        },

        importExcelMessage() {
            return this.message;
        },

        disabled(deletable) {
            return deletable === SYSTEM_CODE_DELETABLE.NO;
        },
    },
    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        }
    }
});
