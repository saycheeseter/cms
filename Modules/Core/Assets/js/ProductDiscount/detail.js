import CoreMixins from '../Common/CoreMixins';
import ProductMixins from '../Common/ProductMixins';
import MemberRateService from '../Services/MemberRateService';
import MemberService from '../Services/MemberService';
import CustomerService from '../Services/CustomerService';
import SystemCodeService from '../Services/SystemCodeService';
import CategoryService from '../Services/CategoryService';
import SupplierService from '../Services/SupplierService';
import ProductService from '../Services/ProductService';
import ProductDiscountService from '../Services/ProductDiscountService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins, ProductMixins],

    data: {
        info: new Data({
            id: 0,
            code: '',
            name: '',
            created_for: Laravel.auth.branch,
            valid_from: Moment().format(),
            valid_to: Moment().format(),
            discount_scheme: '',
            select_type: '',
            target_type: '',
            status: true,
            mon: false,
            tue: false,
            wed: false,
            thur: false,
            fri: false,
            sat: false,
            sun: false,
            everyday: false
        }),

        references: {
            discountSchemes: [
                {id: DISCOUNT_SCHEME.SIMPLE, label: Lang.t('label.simple')},
                {id: DISCOUNT_SCHEME.BUY_X_FOR_PRICE_Y, label: Lang.t('info.buy_x_for_price_y')},
                {id: DISCOUNT_SCHEME.BUY_X_FOR_FIXED_PRICE_Y, label: Lang.t('info.buy_x_for_fixed_price_y')},
            ],
            selectTypes: [
                {id: SELECT_TYPE.ALL_PRODUCTS, label: Lang.t('label.all_products')},
                {id: SELECT_TYPE.SPECIFIC_PRODUCT, label: Lang.t('label.specific_product')},
                {id: SELECT_TYPE.SPECIFIC_BRAND, label: Lang.t('label.specific_brand')},
                {id: SELECT_TYPE.SPECIFIC_CATEGORY, label: Lang.t('label.specific_category')},
                {id: SELECT_TYPE.SPECIFIC_SUPPLIER, label: Lang.t('label.specific_supplier')}
            ],
            targetTypes: [
                {id: TARGET_TYPE.ALL_CUSTOMERS, label: Lang.t('label.all_customers')},
                {id: TARGET_TYPE.ALL_MEMBERS, label: Lang.t('label.all_members')},
                {id: TARGET_TYPE.SPECIFIC_MEMBER_RATE, label: Lang.t('label.specific_member_rate')},
                {id: TARGET_TYPE.SPECIFIC_MEMBER, label: Lang.t('label.specific_member')},
                {id: TARGET_TYPE.SPECIFIC_CUSTOMER, label: Lang.t('label.specific_customer')}
            ],
            discountTypes: [
                {id: DISCOUNT_TYPE.PERCENTAGE, label: Lang.t('label.percentage')},
                {id: DISCOUNT_TYPE.DEDUCT_AMOUNT, label: Lang.t('label.deduct_amount')},
                {id: DISCOUNT_TYPE.DISCOUNTED_PRICE, label: Lang.t('label.discounted_price')},
            ],
            branches: branches,
            categories: categories,
            suppliers: suppliers,
            brands: brands,
            discountSchemeEnum: DISCOUNT_SCHEME
        },

        datatable: {
            target: {
                id: 'table-target',
                settings: {
                    withPaging : true,
                    withPaginateCallBack: false,
                    perPage: 10
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            detail: {
                id: 'table-detail',
                settings: {
                    withPaging: true,
                    withPaginateCallBack: false,
                    perPage: 10,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },

        confirm: {
            deleteTargets: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            deleteDetails: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            targetWarning: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            detailWarning: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
        },

        action: new Data({
            buffer: '',
            value: ''
        }),

        buffer: {
            data: {}
        },

        selector: {
            transaction: {
                datatable: {
                    id: 'table-selector',
                    settings: {
                        withPaging : true,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id'
                        },
                        perPage: 10
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: []
                },
                columnSettings: {
                    columns: [],
                    selections: [],
                    visible: false
                }
            },
            visible: {
                memberRate: false,
                member: false,
                customer: false,
                brand: false,
                category: false,
                supplier: false
            }
        },

        dialog: {
            message: ''
        },

        message: new Notification,

        format: {
            info: {
                valid_from: 'date',
                valid_to: 'date',
            },
            details: {
                discount_value: 'numeric',
                qty: 'numeric'
            }
        },
    },

    mounted: function(){
        this.load();
    },

    methods: {
        load() {
            if(this.processing || id == 'create') {
                return false;
            }

            this.processing = true;

            let that = this;

            ProductDiscountService.show(id)
                .then(function (response){
                    that.info.set(response.data.values.record);
                    that.datatable.target.value.data = response.data.values.targets.data;
                    that.datatable.detail.value.data = response.data.values.details.data;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        process() {
            if ((this.info.id === 0 && ! permissions.create) || (this.info.id !== 0 && ! permissions.update)) {
                return false;
            }

            if (this.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            ProductDiscountService.store(this.data)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.info.id = response.data.values.record.id;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            ProductDiscountService.update(this.data, id)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        showDetailSelector() {
            switch(parseInt(this.info.select_type)) {
                case SELECT_TYPE.SPECIFIC_PRODUCT:
                    this.productPicker.visible = true;
                    break;
                case SELECT_TYPE.SPECIFIC_BRAND:
                    this.selector.visible.brand = true;
                    break;
                case SELECT_TYPE.SPECIFIC_CATEGORY:
                    this.selector.visible.category = true;
                    break;
                case SELECT_TYPE.SPECIFIC_SUPPLIER:
                    this.selector.visible.supplier = true;
                    break;
            }
        },

        showTargetSelector() {
            switch(parseInt(this.info.target_type)) {
                case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                    this.selector.visible.memberRate = true;
                    break;
                case TARGET_TYPE.SPECIFIC_MEMBER:
                    this.selector.visible.member = true;
                    break;
                case TARGET_TYPE.SPECIFIC_CUSTOMER:
                    this.selector.visible.customer = true;
                    break;
            }
        },

        getTargets() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.selector.transaction.datatable.selected = [];
            this.selector.transaction.datatable.value.data = [];

            this.targetService.paginate({
                    perPage: 10,
                    page: 1,
                    filters: []
                })
                .then(function (response) {
                    that.processing = false;

                    let key;
                    switch(parseInt(that.info.target_type)) {
                        case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                            key = 'rates';
                            break;
                        case TARGET_TYPE.SPECIFIC_MEMBER:
                            key = 'members';
                            break;
                        case TARGET_TYPE.SPECIFIC_CUSTOMER:
                            key = 'customers';
                            break;
                    }
                    
                    that.selector.transaction.datatable.value.data = response.data.values[key];
                })
                .catch(function (response) {
                    that.processing = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        addTargets(cont = false) {
            if(this.processing) {
                return false;
            }

            if(!cont) {
                this.selector.visible.memberRate = false;
                this.selector.visible.member = false;
                this.selector.visible.customer = false;
            }

            this.processing = true;

            let that = this;
            let ids = this.getUniqueTargetIds();

            if (ids.length === 0) {
                this.processing = false;
                return false;
            }

            this.targetService.findMany(ids)
            .then(function (response) {
                that.selector.transaction.datatable.selected = [];

                that.datatable.target.value.data = that.datatable.target.value.data.concat(
                    that.formatTargetResponse(response.data.values)
                );

                that.processing = false;
            })
            .catch(function (response) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        getUniqueTargetIds() {
            return this.getUniqueIds(this.selector.transaction.datatable.selected, this.datatable.target.value.data, 'target_id');
        },

        getSelect() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.selector.transaction.datatable.selected = [];
            this.selector.transaction.datatable.value.data = [];

            if(parseInt(this.info.select_type) == SELECT_TYPE.SPECIFIC_CATEGORY) {
                this.selectService.load()
                .then(function (response) {
                    that.processing = false;
                    that.selector.transaction.datatable.value.data = response.data.values.categories;
                })
                .catch(function (response) {
                    that.processing = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
            } else {
                this.selectService.paginate({
                    perPage: 10,
                    page: 1,
                    filters: []
                })
                .then(function (response) {
                    that.processing = false;

                    let key;
                    switch(parseInt(that.info.select_type)) {
                        case SELECT_TYPE.SPECIFIC_BRAND: 
                            key = 'system_codes';
                            break;
                        case SELECT_TYPE.SPECIFIC_SUPPLIER: 
                            key = 'suppliers';
                            break;
                    }

                    that.selector.transaction.datatable.value.data = response.data.values[key];
                })
                .catch(function (response) {
                    that.processing = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
            }
        },

        addSelect(cont = false) {
            if(this.processing) {
                return false;
            }

            if(!cont) {
                this.selector.visible.brand = false;
                this.selector.visible.category = false;
                this.selector.visible.supplier = false;
            }

            this.processing = true;

            let that = this;
            let ids = this.getUniqueSelectTypeIds();

            if (ids.length === 0) {
                this.processing = false;
                return false;
            }

            this.selectService.findMany(ids)
            .then(function (response) {
                that.selector.transaction.datatable.selected = [];

                that.datatable.detail.value.data = that.datatable.detail.value.data.concat(
                    that.formatSelectResponse(response.data.values)
                );
                
                that.processing = false;
            })
            .catch(function (response) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        getUniqueSelectTypeIds() {
            return this.getUniqueIds(this.selector.transaction.datatable.selected, this.datatable.detail.value.data, 'entity_id');
        },

        add(cont = false) {
            if(this.processing) {
                return false;
            }

            if (this.datatable.product.checked.length === 0) {
                this.message.error.set([Lang.t('validation.details_min_one')]);
                return false;
            }

            this.processing = true;

            if(!cont) {
                this.productPicker.visible = false;
            }

            this.addProducts();
        },

        addProducts() {
            let that = this;
            let ids = this.getUniqueProductIds();

            if (ids.length === 0) {
                this.processing = false;
                return false;
            }

            ProductService.findManyBasic(ids)
                .then(function (response) {
                    that.datatable.product.checked = [];
                    
                    that.datatable.detail.value.data = that.datatable.detail.value.data.concat(
                        that.formatSelectResponse(response.data.values)
                    );

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getUniqueProductIds() {
            return this.getUniqueIds(this.datatable.product.checked, this.datatable.detail.value.data, 'entity_id');
        },

        formatTargetResponse(response) {
            let key, data;
            switch(parseInt(this.info.target_type)) {
                case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                    key = 'memberRates';
                    break;
                case TARGET_TYPE.SPECIFIC_MEMBER:
                    key = 'members';
                    break;
                case TARGET_TYPE.SPECIFIC_CUSTOMER:
                    key = 'customers';
                    break;
            }

            data = response[key];

            let returnArray = [];

            for(let index in data) {
                let target_name;
                let target_id = data[index]['id'];

                switch(parseInt(this.info.target_type)) {
                    case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                        target_name = this.formatMemberRateData(data[index]);
                        break;
                    case TARGET_TYPE.SPECIFIC_MEMBER:
                        target_name = this.formatMemberData(data[index]);
                        break;
                    case TARGET_TYPE.SPECIFIC_CUSTOMER:
                        target_name = this.formatCustomerData(data[index]);
                        break;
                }

                returnArray.push({
                    target_id: target_id,
                    target_type: this.info.target_type,
                    target_type_label: this.targetLabel,
                    target_name: target_name
                });
            }

            return returnArray;
        },

        formatMemberRateData(data) {
            return data['name'];
        },

        formatMemberData(data) {
            return data['full_name'];
        },

        formatCustomerData(data) {
            return data['name'];
        },

        formatSelectResponse(response) {
            let key, data;

            switch(parseInt(this.info.select_type)) {
                case SELECT_TYPE.SPECIFIC_PRODUCT:
                    key = 'products';
                    break;
                case SELECT_TYPE.SPECIFIC_BRAND:
                    key = 'system_codes';
                    break;
                case SELECT_TYPE.SPECIFIC_CATEGORY:
                    key = 'categories';
                    break;
                case SELECT_TYPE.SPECIFIC_SUPPLIER:
                    key = 'suppliers';
                    break;
            }

            data = response[key];

            let returnArray = [];

            for(let index in data) {
                let name;
                let entity_id = data[index]['id'];

                switch(parseInt(this.info.select_type)) {
                    case SELECT_TYPE.SPECIFIC_PRODUCT:
                        name = this.formatProductData(data[index]);
                        break;
                    case SELECT_TYPE.SPECIFIC_BRAND:
                        name = this.formatBrandData(data[index]);
                        break;
                    case SELECT_TYPE.SPECIFIC_CATEGORY:
                        name = this.formatCategoryData(data[index]);
                        break;
                    case SELECT_TYPE.SPECIFIC_SUPPLIER:
                        name = this.formatSupplierData(data[index]);
                        break;
                }

                returnArray.push({
                    entity_id: entity_id,
                    entity_type: this.info.select_type,
                    entity_type_label: this.selectLabel,
                    name: name,
                    discount_type: '',
                    discount_value: '',
                    qty: this.info.discount_scheme == DISCOUNT_SCHEME.SIMPLE ? 1 : ''
                });
            }

            return returnArray;
        },

        formatProductData(data) {
            return data['name'];
        },

        formatBrandData(data) {
            return data['name'];
        },

        formatCategoryData(data) {
            return data['name'];
        },

        formatSupplierData(data) {
            return data['full_name'];
        },

        confirmRemoveTarget(data, index, dataIndex) {
            this.confirm.deleteTargets.visible = true;
            this.confirm.deleteTargets.index = dataIndex;
        },

        removeTarget() {
            this.datatable.target.value.data.splice(this.confirm.deleteTargets.index, 1);
            this.confirm.deleteTargets.visible = false;
        },

        clearTargets() {
            this.datatable.target.value.data = [];
            this.confirm.targetWarning.visible = false;
        },

        confirmRemoveDetail(data, index, dataIndex) {
            if(this.info.select_type == SELECT_TYPE.ALL_PRODUCTS) {
                return false;
            }

            this.confirm.deleteDetails.visible = true;
            this.confirm.deleteDetails.index = dataIndex;
        },

        removeDetail() {
            this.datatable.detail.value.data.splice(this.confirm.deleteDetails.index, 1);
            this.confirm.deleteDetails.visible = false;
        },

        clearDetails() {
            this.datatable.detail.value.data = [];
            this.confirm.detailWarning.visible = false;

            if(this.info.select_type == SELECT_TYPE.ALL_PRODUCTS) {
                this.setDefaultDetail();
            }
        },

        setDefaultDetail() {
            this.datatable.detail.value.data = [];
            this.datatable.detail.value.data.push({
                entity_id: null,
                entity_type: this.info.select_type,
                entity_type_label: this.selectLabel,
                name: 'All Products',
                discount_type: '',
                discount_value: '',
                qty: this.info.discount_scheme == DISCOUNT_SCHEME.SIMPLE ? 1 : ''
            });
        },

        getUniqueIds(ids, source, key) {
            let existing = this.getExistingIds(source, key);
            let selected = ids;
            let difference = Arr.difference(selected, existing);
            
            return Arr.intersection(selected, difference);
        },

        getExistingIds(source, key) {
            let ids = [];

            for(let index in source) {
                let row = source[index];
                ids.push(row[key]);
            }

            return ids;
        },

        revertInfo() {
            this.info.set(this.buffer.data);
            this.confirm.targetWarning.visible = false;
            this.confirm.detailWarning.visible = false;
            this.action.reset();
        },

        notifyTarget(action) {
            if(this.datatable.target.value.data.length > 0) {
                this.confirm.targetWarning.visible = true;
                this.action.buffer = action;
            } else {
                this.action.value = action;
            }
        },

        notifyDetail(action) {
            if(this.datatable.detail.value.data.length > 0) {
                this.confirm.detailWarning.visible = true;
                this.action.buffer = action;
            } else {
                this.action.value = action;
            }
        },

        redirect() {
            ProductDiscountService.redirect(this.info.id);
        },
    },

    computed: {
        targetVisibility: function() {
            switch(parseInt(this.info.target_type)) {
                case TARGET_TYPE.ALL_CUSTOMERS:
                case TARGET_TYPE.ALL_MEMBERS:
                    return false;
                    break;
                default: 
                    return true;
                    break;
            }
        },
        detailVisibility: function() {
            if(parseInt(this.info.select_type) == SELECT_TYPE.ALL_PRODUCTS) {
                return false;
            } else {
                return true;
            }
        },
        targetService: function() {
            switch(parseInt(this.info.target_type)) {
                case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                    return MemberRateService;
                    break;
                case TARGET_TYPE.SPECIFIC_MEMBER:
                    return MemberService;
                    break;
                case TARGET_TYPE.SPECIFIC_CUSTOMER:
                    return CustomerService;
                    break;
            }
        },
        targetLabel: function() {
            switch(parseInt(this.info.target_type)) {
                case TARGET_TYPE.SPECIFIC_MEMBER_RATE:
                    return Lang.t('label.specific_member_rate');
                    break;
                case TARGET_TYPE.SPECIFIC_MEMBER:
                    return Lang.t('label.specific_member');
                    break;
                case TARGET_TYPE.SPECIFIC_CUSTOMER:
                    return Lang.t('label.specific_customer');
                    break;
            }
        },
        selectService: function() {
            switch(parseInt(this.info.select_type)) {
                case SELECT_TYPE.SPECIFIC_BRAND:
                    return new SystemCodeService('brand');
                    break;
                case SELECT_TYPE.SPECIFIC_CATEGORY:
                    return CategoryService;
                    break;
                case SELECT_TYPE.SPECIFIC_SUPPLIER:
                    return SupplierService;
                    break;
            }
        },
        selectLabel: function() {
            switch(parseInt(this.info.select_type)) {
                case SELECT_TYPE.ALL_PRODUCTS:
                    return Lang.t('label.all_products');
                    break;
                case SELECT_TYPE.SPECIFIC_PRODUCT:
                    return Lang.t('label.specific_product');
                    break;
                case SELECT_TYPE.SPECIFIC_BRAND:
                    return Lang.t('label.specific_brand');
                    break;
                case SELECT_TYPE.SPECIFIC_CATEGORY:
                    return Lang.t('label.specific_category');
                    break;
                case SELECT_TYPE.SPECIFIC_SUPPLIER:
                    return Lang.t('label.specific_supplier');
                    break;
            }
        },
        data: function() {
            return {
                info: Format.make(this.info.data(), this.format.info),
                targets: this.datatable.target.value.data,
                details: Format.make(this.datatable.detail.value.data, this.format.details)
            };
        }
    },

    watch: {
        'confirm.deleteTargets.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteTargets.reset();
            }
        },
        'confirm.deleteDetails.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteDetails.reset();
            }
        },
        'selector.visible.memberRate': function(value) {
            if(value) {
                this.getTargets();
            }
        },
        'selector.visible.member': function(value) {
            if(value) {
                this.getTargets();
            }
        },
        'selector.visible.customer': function(value) {
            if(value) {
                this.getTargets();
            }
        },
        'selector.visible.brand': function(value) {
            if(value) {
                this.getSelect();
            }
        },
        'selector.visible.category': function(value) {
            if(value) {
                this.getSelect();
            }
        },
        'selector.visible.supplier': function(value) {
            if(value) {
                this.getSelect();
            }
        },
        'info.discount_scheme': function(value) {
            if(value == DISCOUNT_SCHEME.SIMPLE) {
                for(let index in this.datatable.detail.value.data) {
                    this.datatable.detail.value.data[index]['qty'] = 1;
                }
            }
        },
        'productPicker.visible': function(visible) {
            if(visible) {
                this.productSearch();
            }
        },
        'confirm.targetWarning.visible': function(visible) {
            if(!visible) {
                this.confirm.targetWarning.reset();
            }
        },
        'confirm.detailWarning.visible': function(visible) {
            if(!visible) {
                this.confirm.detailWarning.reset();
            }
        },
        'info.target_type': function(newValue, oldValue) {
            this.buffer.data.target_type = oldValue;
            this.buffer.data.select_type = this.info.select_type;
        },
        'info.select_type': function(newValue, oldValue) {
            this.buffer.data.target_type = this.info.target_type;
            this.buffer.data.select_type = oldValue;

            if(newValue == SELECT_TYPE.ALL_PRODUCTS && !this.confirm.detailWarning.visible) {
                this.setDefaultDetail();
            }
        },
        'info.everyday': function(value) {
            this.info.mon = value;
            this.info.tue = value;
            this.info.wed = value;
            this.info.thur = value;
            this.info.fri = value;
            this.info.sat = value;
            this.info.sun = value;
        }
    }
});
