import CoreMixins from '../Common/CoreMixins';
import ProductDiscountService from '../Services/ProductDiscountService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string', optionBool: false },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.valid_from'),
                        columnName: 'valid_from',
                        type: 'datetime',
                        optionBool: false,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() }
                        ]
                    },
                    {
                        name: Lang.t('label.valid_to'),
                        columnName: 'valid_to',
                        type: 'datetime',
                        optionBool: false,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() }
                        ]
                    },
                    {
                        name: Lang.t('label.target_type'),
                        columnName: 'target_type',
                        type: 'select',
                        selectOptions: [
                            {id: TARGET_TYPE.ALL_CUSTOMERS, label: Lang.t('label.all_customers')},
                            {id: TARGET_TYPE.ALL_MEMBERS, label: Lang.t('label.all_members')},
                            {id: TARGET_TYPE.SPECIFIC_MEMBER_RATE, label: Lang.t('label.specific_member_rate')},
                            {id: TARGET_TYPE.SPECIFIC_MEMBER, label: Lang.t('label.specific_member')},
                            {id: TARGET_TYPE.SPECIFIC_CUSTOMER, label: Lang.t('label.specific_customer')}
                        ]
                    },
                    {
                        name: Lang.t('label.discount_scheme'),
                        columnName: 'discount_scheme',
                        type: 'select',
                        selectOptions: [
                            {id: DISCOUNT_SCHEME.SIMPLE, label: Lang.t('label.simple')},
                            {id: DISCOUNT_SCHEME.BUY_X_FOR_PRICE_Y, label: Lang.t('info.buy_x_for_price_y')},
                            {id: DISCOUNT_SCHEME.BUY_X_FOR_FIXED_PRICE_Y, label: Lang.t('info.buy_x_for_fixed_price_y')},
                        ]
                    },
                    {
                        name: Lang.t('label.select_type'),
                        columnName: 'select_type',
                        type: 'select',
                        selectOptions: [
                            {id: SELECT_TYPE.ALL_PRODUCTS, label: Lang.t('label.all_products')},
                            {id: SELECT_TYPE.SPECIFIC_PRODUCT, label: Lang.t('label.specific_product')},
                            {id: SELECT_TYPE.SPECIFIC_BRAND, label: Lang.t('label.specific_brand')},
                            {id: SELECT_TYPE.SPECIFIC_CATEGORY, label: Lang.t('label.specific_category')},
                            {id: SELECT_TYPE.SPECIFIC_SUPPLIER, label: Lang.t('label.specific_supplier')}
                        ]
                    },
                    {
                        name: Lang.t('label.status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: PRODUCT_STATUS.INACTIVE, label: Lang.t('label.inactive') },
                            { id: PRODUCT_STATUS.ACTIVE, label: Lang.t('label.active') }
                        ],
                    },
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        default: { value: String(Laravel.auth.branch) }
                    },
                ],
                data: []
            },
            columnSettings: {
                id: 'column-settings',
                visible: false,
                selections: {},
                columns: [
                    { alias: 'name', label: Lang.t('label.name'), default: true },
                    { alias: 'valid_from', label: Lang.t('label.valid_from'), default: true },
                    { alias: 'valid_to', label: Lang.t('label.valid_to'), default: true },
                    { alias: 'target_type', label: Lang.t('label.target_type'), default: true },
                    { alias: 'discount_scheme', label: Lang.t('label.discount_scheme'), default: true },
                    { alias: 'select_type', label: Lang.t('label.select_type'), default: true },
                    { alias: 'status', label: Lang.t('label.status'), default: true },
                    { alias: 'created_for', label: Lang.t('label.created_for'), default: true }
                ],
                module: MODULE.PRODUCT_DISCOUNT
            }
        },

        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),

        message: new Notification
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            ProductDiscountService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.records;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            ProductDiscountService.destroy(that.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.paginate(that.datatable.value.pagination.current_page);
                    that.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.visible = false;
                })
        },

        confirmation(data, index) {
            this.confirm.visible = true
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        edit(id) {
            if(!permissions.view_detail) {
                return false;
            }
            
            ProductDiscountService.edit(id);
        },

        create() {
            ProductDiscountService.create();
        }
    }
});
