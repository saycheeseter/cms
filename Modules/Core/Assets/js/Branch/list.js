import CoreMixins from '../Common/CoreMixins';
import BranchService from '../Services/BranchService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            list: {
                id: 'table-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            detail: {
                id: 'table-modal',
                settings: {
                    withPaging: true,
                    withPaginateCallBack: false,
                    perPage: 10,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },
        modal: {
            list: {
                confirm: new Data({
                    visible: false,
                    value: false,
                    id: 0,
                    index: 0
                })
            },
            detail: {
                visible: false,
                id: 'modal-detail',
                confirm: new Data({
                    visible: false,
                    value: false,
                    index: 0
                })
            },
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type:'string' },
                    { name: Lang.t('label.name'), columnName: 'name', type:'string' },
                    { name: Lang.t('label.business_name'), columnName: 'business_name', type:'string' },
                ],
                data: []
            }
        },
        branch: {
            info: new Data({
                id: 0,
                code: '',
                name: '',
                contact: '',
                address: '',
                business_name: ''
            }),
            detail: new Data({
                id: 0,
                code: '',
                name: '',
                contact: '',
                address: '',
                business_name: ''
            })
        },
        message: new Notification(['list', 'detail']),
        columnSettings: {
            branch: {
                visible: false,
                columns: [
                    { alias: 'name', label: Lang.t('label.name'), default: true },
                    { alias: 'address', label: Lang.t('label.address'), default: true },
                    { alias: 'contact', label: Lang.t('label.contact'), default: false },
                    { alias: 'businessName', label: Lang.t('label.business_name'), default: false },
                ],
                selections: {},
                module: MODULE.BRANCH
            }
        }
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page) {
            this.modal.filter.visible = false;
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            BranchService.paginate({
                perPage: that.datatable.list.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.list.info = response.data.message;
                that.datatable.list.value.data = response.data.values.branches;
                that.datatable.list.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            BranchService.store({
                info: that.branch.info.data(),
                details: that.datatable.detail.value.data
            })
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.datatable.list.value.data.push(response.data.values.branch);
                    that.computePagination(that.datatable.list.value.pagination, 'add');
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            BranchService.update({
                info: that.branch.info.data(),
                details: that.datatable.detail.value.data
            }, that.branch.info.id)
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.paginate(that.datatable.list.value.pagination.current_page);
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            BranchService.destroy(that.modal.list.confirm.id)
                .then(function (response) {
                    that.message.list.success = response.data.message;
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                    that.datatable.list.value.data.splice(that.modal.list.confirm.index, 1);
                    that.computePagination(that.datatable.list.value.pagination, 'delete');
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            this.processing = true;

            BranchService.show(id)
                .then(function (response) {
                    that.branch.info.set(response.data.values.branch);
                    that.datatable.detail.value.data = response.data.values.details;
                    that.processing = false;
                    that.modal.detail.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validate() {
            if (this.branch.info.id != 0 && !permissions.update) {
                return false;
            } 

            var validation = Validator.make(this.branch.info.data(), {
                name: 'required',
            }, {
                'name.required': Lang.t('validation.name_required'),
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();
            
            if (this.branch.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        confirm(data, index) {
            this.modal.list.confirm.visible = true;
            this.modal.list.confirm.id = data.id;
            this.modal.list.confirm.index = index;
        },

        // Branch Detail Functions
        createDetail() {
            let data = Object.assign({}, this.branch.detail.data());

            if (!this.isValidDetail(data)) {
                return false;
            }

            this.datatable.detail.value.data.push(data);
            this.branch.detail.reset();
        },

        updateDetail(data, index, dataIndex) {
            if (!this.isValidDetail(data, dataIndex)) {
                return false;
            }

            this.changeState(index, 'destroy', 'detail');
        },

        isValidDetail(data, index = -1) {
            let names = Arr.pluck(this.datatable.detail.value.data, 'name', index).join(',');

            let validation = Validator.make(data, {
                name: 'required' + (names.length > 0 ? '|not_in:' + names : ''),
            }, {
                'name.required': Lang.t('validation.name_required'),
                'name.not_in': Lang.t('validation.name_exists')
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();

            return true
        },

        confirmDetail(data, index, dataIndex) {
            this.modal.detail.confirm.visible = true;
            this.modal.detail.confirm.index = dataIndex;
        },

        destroyDetail() {
            this.datatable.detail.value.data.splice(this.modal.detail.confirm.index, 1);
            this.modal.detail.confirm.visible = false;
        },
    },

    watch: {
        'modal.list.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.list.confirm.reset();
            }
        },
        'modal.detail.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.detail.confirm.reset();
            }
        },
        'modal.detail.visible': function(visible) {
            if (!visible) {
                this.branch.info.reset();
                this.branch.detail.reset();
                this.datatable.detail.value.data = [];
                this.message.reset();
            }
        }
    },
});
