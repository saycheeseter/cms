import Filtermodal from '../Components/FilterModal/FilterModal.vue'; //required

new Vue({
    el: '#app',
    data: {
        //required
        modal: {
                visible: false
        },
        filters:
        [
            //filters
            // name - label
            // columnName - backend reference
            // type - defines input type

            // optionBool - if option is included 
            // data - data from backend compact
            // headers - option headers
            // columns - columns from collection to be displayed
            // returncolumn - return column
            // 
            // selectOptions - used for select
            {
                name:'Product Name', 
                columnName: 'productname', 
                type:'string', 
                optionBool: false
            },
            {
                name:'Brand', 
                columnName: 'brand', 
                type: 'string', 
                optionBool: true,
                tableOptions: {
                    data: [],
                    headers: ['id', 'code', 'Brand Name'], 
                    columns: ['id', 'code', 'name'], 
                    returncolumn: 'name' 
                }
            },
            {
                name: 'Inventory Status',
                columnName: '', //special
                type: 'select',
                selectOptions: [
                    {value: '1', label: 'all'}
                ]
            }
        ]
        //end required
    },
    components: {
        //required
        Filtermodal
        //end required
    },
    methods: {
        //required button click event
        openModal(){
            this.modal.visible = this.modal.visible == false ? true : false
        }
        //end required
    }
});