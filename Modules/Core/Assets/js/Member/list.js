import CoreMixins from '../Common/CoreMixins';
import ImportExcel from '../ImportExcel/import-excel';
import MemberService from '../Services/MemberService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ImportExcel],

    components: {
        Datepicker
    },
    
    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        fields: new Data({
            id: 0,
            card_id: '',
            full_name: '',
            telephone: '',
            birth_date: Date(),
            gender: 0,
            email: '',
            address: '',
            barcode: '',
            rate_head_id: 0,
            status: 1,
            mobile: '',
            registration_date: Moment().format(),
            expiration_date: Moment().add(1, 'year').format(),
            memo: '',
        }),
        format: {
            registration_date: 'date',
            expiration_date: 'date',
            birth_date: 'date'
        },
        modal: {
            confirm: new Data({
                value: false,
                visible: false,
                id: 0,
                index: 0
            }),
            details: {
                visible: false,
                id: 'details'
            },
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.full_name'), columnName: 'full_name', type: 'string' },
                    { name: Lang.t('label.card_id'), columnName: 'card_id', type: 'string' },
                    { name: Lang.t('label.barcode'), columnName: 'barcode', type: 'string' }
                ],
                data: []
            }
        },
        message: {
            list: new Notification,
            details: new Notification
        },
        rates: rates,
        columnSettings: {
            member: {
                visible: false,
                columns: [
                    { alias: 'memberType', label: Lang.t('label.member_type'), default: true },
                    { alias: 'cardId', label: Lang.t('label.card_id'), default: true },
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true },
                    { alias: 'mobileNo', label: Lang.t('label.mobile_no'), default: false },
                    { alias: 'telephoneNo', label: Lang.t('label.telephone_no'), default: false },
                    { alias: 'birthDate', label: Lang.t('label.birth_date'), default: false },
                    { alias: 'gender', label: Lang.t('label.gender'), default: false },
                    { alias: 'email', label: Lang.t('label.email'), default: false },
                    { alias: 'address', label: Lang.t('label.address'), default: false },
                    { alias: 'memo', label: Lang.t('label.memo'), default: false },
                    { alias: 'registrationDate', label: Lang.t('label.registration_date'), default: false },
                    { alias: 'expirationDate', label: Lang.t('label.expiration_date'), default: false },
                    { alias: 'status', label: Lang.t('label.status'), default: false },
                ],
                selections: {},
                module: MODULE.MEMBER
            }
        },
        permissions: permissions,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            that.processing = true;
            that.message.list.reset();

            MemberService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.list.info = response.data.message;
                that.datatable.value.data = response.data.values.members;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.details.reset();

            MemberService.store(Format.make(that.fields.data(), that.format))
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;

                    setTimeout(function() {
                        that.modal.details.visible = false;
                        that.datatable.value.data.push(response.data.values.member);
                        that.computePagination(that.datatable.value.pagination, 'add');
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.details.reset();

            MemberService.update(Format.make(that.fields.data(), that.format), that.fields.id)
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;
                    
                    setTimeout(function() {
                        that.modal.details.visible = false;
                        that.paginate(that.datatable.value.pagination.current_page);
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.list.reset();

            MemberService.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.list.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            MemberService.show(id)
                .then(function (response) {
                    that.fields.set(response.data.values.member);
                    that.processing = false;
                    that.modal.details.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validate() {
            if (this.fields.id !== 0 && !permissions.update) {
                return false;
            } 
            
            let validation = Validator.make(Format.make(this.fields.data(), this.format), {
                full_name: 'required',
                barcode: 'required|numeric',
                registration_date: 'required|date',
                expiration_date: 'required|date|after:registration_date',
                birth_date: 'required|date',
                email: this.fields.email !== '' ? 'email' : ''
            }, {
                'full_name.required': Lang.t('validation.full_name_required'),
                'barcode.required': Lang.t('validation.barcode_required'),
                'barcode.numeric': Lang.t('validation.barcode_numeric'),
                'registration_date.required': Lang.t('validation.registration_date_required'),
                'registration_date.date': Lang.t('validation.registration_date_format'),
                'expiration_date.required': Lang.t('validation.expiration_date_required'),
                'expiration_date.date': Lang.t('validation.expiration_date_format'),
                'expiration_date.after': Lang.t('validation.expiration_date_invalid_range'),
                'email.email': Lang.t('validation.email_invalid'),
                'birth_date.required': Lang.t('validation.birth_date_required'),
                'birth_date.date': Lang.t('validation.birth_date_invalid_format'),
            });

            if (validation.fails()) {
                this.message.details.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if(this.fields.id === 0){
                this.store();
            } else {
                this.update();
            }
        },

        confirm(data, index) {
            this.modal.confirm.visible = true;
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        onSuccessfulImport() {
            this.paginate();
        },

        importExcelService() {
            return MemberService;
        },

        importExcelMessage() {
            return this.message.list;
        }
    },
    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        },
        'modal.details.visible': function(visible) {
            if (!visible) {
                this.fields.reset();
                this.message.list.reset();
                this.message.details.reset();
            }
        },
    }
});
