export default {
    data: {
        importExcel: {
            logs: {
                visible: false,
                content: []
            },
            attachment: null,
            service: null,
            types: [
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
                'application/vnd.ms-excel'
            ],
            maxSize: 2000000
        },
    },

    methods: {
        onImportExcelChangeFile(e){
            this.importExcel.attachment = e.target.files[0];
        },

        importExcelFile() {
            if (this.processing || !this.isValidExcelFile()) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.importExcelMessage().reset();

            this.importExcelService().importExcel(this.importExcel.attachment)
                .then(function (response) {
                    that.processing = false;
                    that.importExcel.logs.content = response.data.values.logs;
                    that.importExcel.logs.visible = true;
                    that.importExcel.attachment = null;
                    that.$refs.excelFile.value = '';
                    that.onSuccessfulImport();
                })
                .catch(function (error) {
                    that.importExcelMessage().error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        downloadImportExcelTemplate() {
            this.importExcelService().downloadExcelTemplate();
        },

        viewImportExcelLogs() {
            if (this.importExcel.logs.content.length === 0) {
                this.importExcelMessage().error.set([Lang.t('error.no_logs_to_show')]);
                return;
            }

            this.importExcel.logs.visible = true;
        },

        isValidExcelFile() {
            if (null === this.importExcel.attachment) {
                this.importExcelMessage().error.set([Lang.t('error.no_file_selected')]);
                return false;
            }

            if (this.importExcel.types.indexOf(this.importExcel.attachment.type) === -1) {
                this.importExcelMessage().error.set([Lang.t('validation.invalid_file_type')]);
                return false;
            }

            if (this.importExcel.attachment.size > this.importExcel.maxSize) {
                this.importExcelMessage().error.set([Lang.t('validation.file_over_allowed_size')]);
                return false;
            }

            return true;
        },

        onSuccessfulImport() {

        },

        importExcelService() {

        },

        importExcelMessage() {
            return this.message;
        }
    }
}