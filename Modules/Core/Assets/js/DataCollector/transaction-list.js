import CoreMixins from '../Common/CoreMixins';
import DataCollectorService from '../Services/DataCollectorService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        Datepicker
    },

    data: {
        datatable: {
            settings: {
                id: 'datatable-transactions',
                withPaging : true,
                withPaginateCallBack: true,
                perPage: 10,
                withDelete: true
            },
            value: {
                data: [],
                pagination: {}
            }
        },

        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'importDate', label: Lang.t('label.import_date'), default: true },
                { alias: 'fileSize', label: Lang.t('label.file_size'), default: true },
                { alias: 'totalCount', label: Lang.t('label.total_count'), default: true },
                { alias: 'transactionType', label: Lang.t('label.transaction_type'), default: true },
                { alias: 'source', label: Lang.t('label.source'), default: true },
                { alias: 'importedBy', label: Lang.t('label.imported_by'), default: true }
            ],
            selections: []
        },

        filters: {
            dateFrom: Moment().startOf('day').format(),
            dateTo: Moment().endOf('day').format(),
            transactionType: ''
        },

        references: {
            transactionType: [
                {id: null, label: Lang.t('label.all')},
                {id: DATA_COLLECTOR_TRANSACTION.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound')},
                {id: DATA_COLLECTOR_TRANSACTION.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound')},
                {id: DATA_COLLECTOR_TRANSACTION.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound')},
                {id: DATA_COLLECTOR_TRANSACTION.SALES_OUTBOUND, label: Lang.t('label.sales_outbound')},
                {id: DATA_COLLECTOR_TRANSACTION.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound')},
                {id: DATA_COLLECTOR_TRANSACTION.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound')},
                {id: DATA_COLLECTOR_TRANSACTION.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound')},
                {id: DATA_COLLECTOR_TRANSACTION.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust')},
            ]
        },

        message: new Notification,

        confirmation: {
            visible: false,
            id: 0,
            index: 0
        }
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            DataCollectorService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.formattedFilters,
            })
            .then(function (response) {
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.message.info = response.data.message;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        confirm(data, index, dataIndex) {
            this.confirmation.visible = true;
            this.confirmation.id = data.id;
            this.confirmation.index = index;
        },

        destroy() {
            let that = this;

            this.processing = true;
            this.message.reset();

            DataCollectorService.destroy(this.confirmation.id)
                .then(function (response){
                    that.confirmation.visible = false;
                    that.datatable.value.data.splice(that.confirmation.index, 1);
                    that.message.info = response.data.message;
                    that.processing = false;
                    that.computePagination(that.datatable.value.pagination, 'delete');
                })
                .catch(function (error){
                    that.confirmation.visible = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },
    },

    computed: {
        formattedFilters: function() {
            let formatted = Format.make(this.filters, {
                dateFrom: 'date',
                dateTo: 'date'
            });

            let filters = [
                {
                    column: 'import_date',
                    join: 'and',
                    operator: '>=',
                    value: formatted.dateFrom
                },
                {
                    column: 'import_date',
                    join: 'and',
                    operator: '<=',
                    value: formatted.dateTo
                },
            ];

            if(this.filters.transactionType !== null) {
                filters.push({
                    column: 'transaction_type',
                    join: 'and',
                    operator: '=',
                    value: formatted.transactionType
                });
            }

            return filters;
        }
    }
});