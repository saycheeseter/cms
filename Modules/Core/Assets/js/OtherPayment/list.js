import OtherPaymentService from '../Services/OtherPaymentService';
import OtherFinancialFlow from '../OtherFinancialFlow/list.js';

let app = new Vue({
    el: '#app',

    mixins: [OtherFinancialFlow],

    data: {
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_from',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: Laravel.auth.branch },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    { name: Lang.t('label.pay_to'), columnName: 'reference_name', type: 'string', optionBool: false },
                    { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.transaction_date'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                        ]
                    },
                    {
                        name: Lang.t('label.payment_method'),
                        columnName: 'payment_method',
                        type: 'select',
                        selectOptions: paymentMethods
                    }
                ],
                data: []
            }
        },
        
        service: OtherPaymentService,
        
        validation: {
            messages: {
                info: {
                    'reference_name.required': Lang.t('validation.pay_to_required')
                }
            }
        }
    },
});
