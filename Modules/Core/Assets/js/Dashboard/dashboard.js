import CoreMixins from '../Common/CoreMixins';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {},

    methods: {}
});