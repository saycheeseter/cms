
import InventoryChain from '../InventoryChain/detail.js';
import DamageOutboundService from '../Services/DamageOutboundService';
import Outbound from '../Common/OutboundMixins.js';
import DamageService from '../Services/DamageService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, Outbound],

    data: {
        reference: {
            reasons: reasons
        },

        service: {
            invoice: new DamageService,
            inventoryChain: new DamageOutboundService
        },

        attachment: {
            module: MODULE.DAMAGE_OUTBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.DAMAGE_OUTBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.DAMAGE_OUTBOUND
        },

        cache: {
            module: MODULE.DAMAGE_OUTBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.DAMAGE,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            reason_id: '',
        });
    },

    methods: {
        getExtraInfoForUpdateApproved() {
            return [
                'reason_id'
            ];
        }
    }
});