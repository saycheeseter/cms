import ReceivedCheckService from '../Services/ReceivedCheckService';
import CheckManagement from '../CheckManagement/list.js';

let app = new Vue({
    el: '#app',

    mixins: [CheckManagement],

    data: {
        service: new ReceivedCheckService,
        modal: {
            columnSettings: {
                module: MODULE.RECEIVED_CHECK
            }
        }
    },
});