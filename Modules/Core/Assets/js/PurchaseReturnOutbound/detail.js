import InventoryChain from '../InventoryChain/detail.js';
import PurchaseReturnOutboundService from '../Services/PurchaseReturnOutboundService';
import AutoGroupProductBreakdownMixins from '../Common/AutoGroupProductBreakdownMixins.js';
import Outbound from '../Common/OutboundMixins.js';
import PurchaseReturnService from '../Services/PurchaseReturnService';
import ProductService from '../Services/ProductService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, Outbound, AutoGroupProductBreakdownMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
        
        reference: {
            tsuppliers: tsuppliers,
            paymentMethods: paymentMethods,
        },

        service: {
            invoice: new PurchaseReturnService,
            inventoryChain: new PurchaseReturnOutboundService
        },

        buffer: {
            data: {
                supplier_id: ''
            }
        },

        attachment: {
            module: MODULE.PURCHASE_RETURN_OUTBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.PURCHASE_RETURN_OUTBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.PURCHASE_RETURN_OUTBOUND
        },

        cache: {
            module: MODULE.PURCHASE_RETURN_OUTBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.PURCHASE_RETURN,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            supplier_id: '',
            payment_method: '',
            term: 0,
        });
    },

    methods: {
        getSerialsConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.SUPPLIER,
                entity: this.info.for_location,
                status: SERIAL_STATUS.AVAILABLE,
                transaction: null,
                supplier_id: this.info.supplier_id
            };
        },

        getBatchesConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.SUPPLIER,
                entity: this.info.for_location,
                supplier: this.info.supplier_id
            }
        },

        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getExtraInfoForUpdateApproved() {
            return [
                'payment_method',
                'term'
            ];
        }
    },

    watch: {
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.supplier_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = oldValue;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.reference.invoice = this.reference.invoice;
        }
    }
});