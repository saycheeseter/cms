import Filtermodal from '../Components/FilterModal/FilterModal.vue';

new Vue({
el: '#app',
data: {
    //required
    modal: {
            visible: false
    },
    filters:
    [
        //filters
        // name - label
        // columnName - backend reference
        // type - defines input type

        // optionBool - if option is included 
        // service - service to be called
        // headers - option headers
        // columns - columns from collection to be displayed
        // returncolumn - return column
        {
            name:'Product Name', 
            columnName: 'productname', 
            type:'string', 
            optionBool: false
        },
        {
            name:'Barcode', 
            columnName: 'barcode', 
            type:'number',
            optionBool: false
        },
        {
            name:'Date', 
            columnName: 'date', 
            type:'date', 
            optionBool: false
        },
        {
            name:'Brand', 
            columnName: 'brand', 
            type: 'string', 
            optionBool: true,
            tableOptions: {
                service: 'SystemCode',
                headers: ['id', 'code', 'Brand Name'], 
                columns: ['id', 'code', 'name'], 
                returncolumn: 'name' 
            }
        },
        {
            name: 'Inventory Status',
            columnName: '', //special
            type: 'select',
            selectOptions: [
                {value: '1', label: 'all'}
            ],
            default: '1'
        }
    ]
    //end required
},
components: {
    //required
    Filtermodal
    //end required
},
methods: {
    //required button click event
    openModal(){
        this.modal.visible = this.modal.visible == false ? true : false
    }
    //end required
}
});