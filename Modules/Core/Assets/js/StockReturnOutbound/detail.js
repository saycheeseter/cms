import InventoryChain from '../InventoryChain/detail.js';
import StockReturnOutboundService from '../Services/StockReturnOutboundService';
import Outbound from '../Common/OutboundMixins.js';
import BranchService from '../Services/BranchService';
import StockReturnService from '../Services/StockReturnService';
import DeliveryProductSelectorAPIMixins from "../Common/DeliveryProductSelectorAPIMixins";

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, Outbound, DeliveryProductSelectorAPIMixins],

    data: {
        service: {
            invoice: new StockReturnService,
            inventoryChain: new StockReturnOutboundService
        },

        reference: {
            deliveryLocations: locations
        },
        
        attachment: {
            module: MODULE.STOCK_RETURN_OUTBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_RETURN_OUTBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.STOCK_RETURN_OUTBOUND
        },

        cache: {
            module: MODULE.STOCK_RETURN_OUTBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.STOCK_RETURN,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            deliver_to: null,
            deliver_to_location: null,
        });
    },

    methods: {
        onSuccessfulLoad(response) {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        onCacheLoaded() {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        onSuccessfulImportInvoice(response) {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        getDeliveryLocations(def = null) {
            let that = this;

            this.processing = true;

            BranchService.details(this.info.deliver_to)
                .then(function (response) {
                    that.reference.deliveryLocations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.deliver_to_location = location;
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.deliveryLocations = [];
                });
        },
    },
});
