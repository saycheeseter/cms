import CoreMixins from '../Common/CoreMixins';
import TreeItem from '../Components/TreeView/TreeItem.vue';
import CategoryService from '../Services/CategoryService';

let app = new Vue({
    el: '#app',

    components: {
        TreeItem
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : false,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        fields: new Data({
            id: 0,
            code: '',
            name: '',
            parent_id: null
        }),
        treeData: {
            id: null,
            name: 'Category',
            children: []
        },
        parentId: null,
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string' },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string' }
                ],
                data: []
            }
        },
        message: new Notification
    },

    mounted: function() {
        this.load();
    },

    methods: {
        load() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            CategoryService.load()
                .then(function(response){
                    that.treeData.children = response.data.values.categories;
                    that.listChild(that.treeData);
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        paginate(page) {
            if(this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            that.processing = true;
            that.datatable.value.data = [];
            that.message.reset();

            CategoryService.paginate({
                filters: that.modal.filter.data,
                parent_id: that.parentId
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = that.childDescendants({
                    children: response.data.values.categories
                });
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if(this.processing || !this.isValid(this.fields.data())) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            this.fields.parent_id = that.parentId;

            CategoryService.store(this.fields.data())
                .then(function(response){
                    that.message.success = response.data.message;
                    that.fields.id = response.data.values.category.id;
                    that.fields.code = response.data.values.category.code;
                    that.datatable.value.data.push(Object.assign({}, that.fields.data()));
                    TreeItemDispatcher.$emit('addChild', that.fields.data());
                    that.fields.reset();
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update(data, index) {
            if(this.processing || !this.isValid(data)) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            
            data['parent_id'] = that.parentId;

            CategoryService.update(data, data.id)
                .then(function (response) {
                    TreeItemDispatcher.$emit('updateChild', data);
                    that.changeState(index, 'destroy');
                    that.processing = false;
                    that.message.success = response.data.message;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        destroy() {
            if(this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            CategoryService.destroy(this.modal.confirm.id)
                .then(function (response) {
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.message.success = response.data.message;
                    that.modal.confirm.visible = false;
                    that.processing = false;

                    TreeItemDispatcher.$emit('deleteChild', {
                        parentId: that.parentId,
                        index: that.modal.confirm.index
                    });
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                })
        },

        listChild(node) {
            this.datatable.value.data = [];
            this.datatable.value.data = this.childDescendants(node);
        },

        selected(id) {
            this.parentId = id;
        },

        // descendants(node) {
        //     var mainArray = [];

        //     if(node.id != null && node.id != undefined) {
        //         mainArray.push({
        //             'id': node.id,
        //             'name': node.name,
        //             'code': node.code
        //         });
        //     }

        //     for(var i = 0; i < node.children.length; i++) {
        //         var childArray = this.descendants(node.children[i]);
        //         mainArray = mainArray.concat(childArray);
        //     }

        //     return mainArray;
        // },

        childDescendants(node) {
            let children = node.children;
            let mainArray = [];

            for(let index in children) {
                if(children[index].id != null && children[index].id != undefined) {
                    mainArray.push({
                        id: children[index].id,
                        name: children[index].name,
                        code: children[index].code
                    });
                }
            }

            return mainArray;
        },

        confirm(data, index, dataIndex) {
            this.modal.confirm.visible = true;
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = dataIndex;
        },

        isValid(data) {
            this.message.reset();
            
            let validation = Validator.make(data, {
                name: 'required',
            }, {
                'name.required': Lang.t('validation.name_required'),
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        },
    },

    created: function(){
        TreeItemDispatcher.$on('listChild', this.listChild);
        TreeItemDispatcher.$on('selected', this.selected);
    },

    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        }
    }
});
