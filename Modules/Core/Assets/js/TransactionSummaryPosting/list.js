import CoreMixins from '../Common/CoreMixins';
import TransactionSummaryPostingService from "../Services/TransactionSummaryPostingService";

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: false,
                withEdit: false,
                withActionCell: false,
                withPaginateCallBack: true
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        posting: {
            form: {
                visible: false,
                data: new Data({
                    date: Moment().format('YYYY-MM-DD'),
                    lastPostDate: Laravel.settings.transaction_summary_computation_last_post_date
                }),
            }
        },
        message: new Notification,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            TransactionSummaryPostingService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        showPostingForm(id, index) {
            this.posting.form.visible = true;
            this.posting.form.data.date = Moment().format('YYYY-MM-DD');
        },

        closePostingForm() {
            this.posting.form.visible = false;
        },

        generateSummary() {
            if (this.processing || !this.isValidDateRange()) {
                return false;
            }

            this.processing = true;

            let that = this;
            let date = Moment(this.posting.form.data.date).format('YYYY-MM-DD');

            TransactionSummaryPostingService.generateSummary({
                date: date
            })
            .then(function (response) {
                that.processing = false;
                that.message.success = response.data.message;
                that.posting.form.data.lastPostDate = date;
                that.paginate();
                that.closePostingForm();
            })
            .catch(function (error) {
                that.closePostingForm();
                that.processing = false;
                that.message.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        isValidDateRange() {
            let dateFrom = Moment(Laravel.settings.transaction_summary_computation_last_post_date);
            let dateTo = Moment(this.posting.form.data.date);

            if (dateFrom >= dateTo) {
                this.message.error.set([
                    Lang.t('validation.invalid_date_range')
                ]);

                this.closePostingForm();

                return false;
            }

            return true;
        }
    },

    computed: {
        datepickerFormat: function() {
            let options = {
                format: 'yyyy-MM-dd',
                disabled: {
                    to: new Date(Moment(this.posting.form.data.lastPostDate).add(1, 'days').format('YYYY-MM-DD'))
                }
            };

            return options;
        },
    }
});
