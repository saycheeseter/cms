import CoreMixins from '../Common/CoreMixins';
import ExcelTemplateService from '../Services/ExcelTemplateService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        message: new Notification,
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    { name:'Name', columnName: 'name', type: 'string' },
                    {
                        name: Lang.t('label.module'),
                        columnName: 'module',
                        type: 'select',
                        selectOptions: [
                            { id: MODULE.PURCHASE, label: Lang.t('label.purchase_order') },
                            { id: MODULE.PURCHASE_RETURN, label: Lang.t('label.purchase_return') },
                            { id: MODULE.DAMAGE, label: Lang.t('label.damage') },
                            { id: MODULE.SALES, label: Lang.t('label.sales') },
                            { id: MODULE.SALES_RETURN, label: Lang.t('label.sales_return') },
                            { id: MODULE.STOCK_REQUEST, label: Lang.t('label.stock_request') },
                            { id: MODULE.STOCK_DELIVERY, label: Lang.t('label.stock_delivery') },
                            { id: MODULE.STOCK_RETURN, label: Lang.t('label.stock_return') },
                            { id: MODULE.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound') },
                            { id: MODULE.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound') },
                            { id: MODULE.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound') },
                            { id: MODULE.SALES_OUTBOUND, label: Lang.t('label.sales_outbound') },
                            { id: MODULE.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound') },
                            { id: MODULE.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound') },
                            { id: MODULE.STOCK_DELIVERY_INBOUND, label: Lang.t('label.stock_delivery_inbound') },
                            { id: MODULE.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound') },
                            { id: MODULE.STOCK_RETURN_INBOUND, label: Lang.t('label.stock_return_inbound') },
                            { id: MODULE.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust') },
                        ]
                    },
                ],
                data: []
            }
        },

        datatable: {
            value: {
                data: [],
                pagination: {}
            },
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withActionCell: permissions.delete,
            },
        }
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            ExcelTemplateService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.templates;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        destroy(data) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            ExcelTemplateService.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                })
        },

        create() {
            ExcelTemplateService.create();
        },

        view(id) {
            if(! permissions.view_detail && ! permissions.update) {
                return false;
            }
            
            ExcelTemplateService.view(id);
        },

        confirm(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },
    },
    
    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        }
    }
});
