import CoreMixins from '../Common/CoreMixins';
import ExcelTemplateService from '../Services/ExcelTemplateService';
import ExcelColumn from '../Components/ExcelColumn/ExcelColumn.vue'
import ColumnsMixins from '../ExcelTemplate/ColumnsMixins';
import Sortable from 'sortablejs'

Vue.directive('sortable', {
    inserted: function (el, binding) {
        var sortable = new Sortable(el, binding.value || {});
    }
});

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ColumnsMixins],

    components: {
        ExcelColumn,
    },

    data: {
        template: new Data({
            info : {
                id: 0,
                name: '',
                module: null,
                format: '',
            },
            format : {
                header : {
                    bold: false,
                    textAlign: "center",
                },
                columns : {}
            },
            udfs : [],
        }),

        reference : {
            modules: [
                { id: MODULE.PURCHASE, label: Lang.t('label.purchase_order') },
                { id: MODULE.PURCHASE_RETURN, label: Lang.t('label.purchase_return') },
                { id: MODULE.DAMAGE, label: Lang.t('label.damage') },
                { id: MODULE.SALES, label: Lang.t('label.sales') },
                { id: MODULE.SALES_RETURN, label: Lang.t('label.sales_return') },
                { id: MODULE.STOCK_REQUEST, label: Lang.t('label.stock_request') },
                { id: MODULE.STOCK_DELIVERY, label: Lang.t('label.stock_delivery') },
                { id: MODULE.STOCK_RETURN, label: Lang.t('label.stock_return') },
                { id: MODULE.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound') },
                { id: MODULE.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound') },
                { id: MODULE.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound') },
                { id: MODULE.SALES_OUTBOUND, label: Lang.t('label.sales_outbound') },
                { id: MODULE.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound') },
                { id: MODULE.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound') },
                { id: MODULE.STOCK_DELIVERY_INBOUND, label: Lang.t('label.stock_delivery_inbound') },
                { id: MODULE.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound') },
                { id: MODULE.STOCK_RETURN_INBOUND, label: Lang.t('label.stock_return_inbound') },
                { id: MODULE.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust') },
                { id: MODULE.COUNTER, label: Lang.t('label.counter') },
            ]
        },

        sortables : [],
        headDisable: false,
        
        allColumns: false,

        message: new Notification,

        dialog: {
            message: ''
        },
    },

    mounted: function() {
        this.setDefaults();
        this.load();
    },

    methods: {
        reorder ({oldIndex, newIndex}) {
            const movedItem = this.sortables.splice(oldIndex, 1)[0]
            this.sortables.splice(newIndex, 0, movedItem)
        },

        load() {
            if(this.processing || id == 'create') {
                return false;
            }

            this.processing = true;
            this.headDisable = true;
            let that = this;
            
            ExcelTemplateService.show(id)
                .then(function (response){
                    that.template.info = response.data.values.template;
                    that.template.udfs = response.data.values.udfs;
                    that.template.format.columns = Obj.copy(that.excel_modules[MODULE_ALIAS[that.template.info.module]]);
                    that.addUDFS(that.template.udfs);

                    that.setTemplateData(that.template.info.format);
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        moduleChange() {
            if (this.processing) {
                return false;
            }

            let that = this;
            this.processing = true;

            ExcelTemplateService.udf(this.template.info.module)
                .then(function (response) {
                    that.sortables = [];
                    that.template.udfs = response.data.values.udfs;
                    that.template.format.columns = Obj.copy(that.excel_modules[MODULE_ALIAS[that.template.info.module]]);
                    that.addUDFS(that.template.udfs);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        setDefaults() {
            for (var key in  this.excel_modules) {
                for (var column in  this.excel_modules[key]) {
                    let value = {
                        header_name: this.parseKeyAsReadable(column),
                        width: '40',
                        textAlign: 'left',
                        type: COLUMN_TYPE.TEXT,
                        checked: false,
                    };
                    this.excel_modules[key][column] = value;
                }
            }
        },

        processData() {
            let obj = {
                header_style: this.template.format.header,
                columns : {},
            };

            for (var i = 0; i < this.sortables.length; i++) {
                let key = this.sortables[i]['key'];

                obj.columns[key] = JSON.parse(JSON.stringify(this.sortables[i]));

                delete obj.columns[key]['checked'];
                delete obj.columns[key]['key'];
            }
            
            this.template.info.format = obj;
        },

        process() {
            if ((this.template.info.id === 'create' && ! permissions.create)
                || (this.template.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }
            
            if(this.sortables.length == 0) {
                this.message.error.set([Lang.t('validation.select_one_column')]);
                return false;
            };

            this.processData();

            if (this.template.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        preview() {
           if (this.processing) {
                return false;
            }

            var params = {
                template_id: this.template.info.id,
            };

            ExcelTemplateService.preview(params);
        },

        parseKeyAsReadable(column) {
            return column.replace(/_/g, " ").replace(/\b\w/g, function(txt) {
                return String(txt).toUpperCase();
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            ExcelTemplateService.store(this.template.info)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.template.info.id = response.data.values.template.id;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.message.reset();
            
            ExcelTemplateService.update(this.template.info, id)
                .then(function (response) {
                    that.dialog.message = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        setTemplateData(obj) {
            for (var key in obj.columns) {
                if(key in this.template.format.columns) {
                    for (var prop in this.template.format.columns[key]) {
                        this.template.format.columns[key][prop] =  obj.columns[key][prop];
                        this.template.format.columns[key].checked = true;
                    }
                    this.template.format.columns[key].key = key;
                    this.sortables.push(this.template.format.columns[key])
                }
            }

            this.template.format.header = obj.header_style;
        },

        redirect() {
            if(this.template.info.id != 0) {
                ExcelTemplateService.redirect(this.template.info.id);
            }
        },

        toggleAllColumns(){
            for (var key in this.template.format.columns) {
                this.template.format.columns[key].checked = this.allColumns;
            }
        }
    },

    watch:{
        'template.format.columns': {
            handler:  function(val) {
                let all = true;
                for (var key in val) {
                    if(val[key].checked) {
                        val[key]['key'] = key;
                        if(!this.sortables.includes(val[key])){
                            this.sortables.push(val[key]);
                        }
                    }else {
                        if(this.sortables.includes(val[key])){
                            this.sortables.splice(this.sortables.indexOf(val[key]), 1);
                        }

                        all = false;
                    }
                }

                this.allColumns = all;
            },
            deep: true
        },
    },
});
