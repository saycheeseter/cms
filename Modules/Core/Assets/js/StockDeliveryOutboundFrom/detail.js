import StockDeliveryOutbound from '../StockDeliveryOutbound/detail.js';

let app = new Vue({
    el: '#app',

    mixins: [StockDeliveryOutbound],

    data: {
        link: 'stock-delivery-outbound-from',
        options: {
            setup: SETUP.VIEW,
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_DELIVERY_OUTBOUND_FROM,
                    key: 'detail'
                }
            }
        }
    },
});
