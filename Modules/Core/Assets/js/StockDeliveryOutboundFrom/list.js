import StockDeliveryOutbound from '../StockDeliveryOutbound/list.js';
import InventoryChain from '../InventoryChain/list.js';
import Arr from '../Classes/Arr';
import BranchService from '../Services/BranchService';

let app = new Vue({
    el: '#app',

    mixins: [StockDeliveryOutbound],

    data: {
        link: 'stock-delivery-outbound-from',
        options: {
            setup: SETUP.VIEW,
        },
        filter: {
            options: Arr.replace(0, InventoryChain.data.filter.options, {
                name: Lang.t('label.deliver_to'),
                columnName: 'deliver_to',
                type: 'select',
                selectOptions: branches,
                selected: true,
                default: { value: String(Laravel.auth.branch) }
            })
        },
        columnSettings: {
            module: {
                id: MODULE.STOCK_DELIVERY_OUTBOUND_FROM,
                key: 'info'
            }
        }
    },

    created: function() {
        this.filter.options[0].selected = false;
        this.filter.options.push({
            name: Lang.t('label.deliver_to'),
            columnName: 'deliver_to',
            type:'doubleSelect',
            secondaryColumn: 'deliver_to_location',
            selectOptions: branches,
            withAll: true,
            withCallback: true,
            selected: true,
            callback: function(branch) {
                return BranchService.details(branch);
            },
            default: { value: { head: Laravel.auth.branch } }
        });
        delete this.datatable.settings.withSelect;
    }
});
