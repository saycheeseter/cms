import StockRequest from '../StockRequest/detail.js';
import SalesOutboundImportMixins from "../Common/SalesOutboundImportMixins";

let app = new Vue({
    el: '#app',

    mixins: [StockRequest, SalesOutboundImportMixins],

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'created-for':
                    this.getLocations();
                    break;
            }
        },
    },
    computed: {
        enableSalesOutboundImport: function() {
            return this.fullyEnabled;
        },
    }
});
