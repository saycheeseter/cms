import StockRequest from '../StockRequest/list.js';

let app = new Vue({
    el: '#app',

    mixins: [StockRequest],

    data: {
        link: 'stock-request-to'
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.request_to'),
            columnName: 'request_to',
            type: 'select',
            selectOptions: branches,
        });
    }
});
