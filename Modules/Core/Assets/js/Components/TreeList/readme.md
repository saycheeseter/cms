prop tree

[
    {id: 1, label: 'Child 1', level: 1, parent: null, hasChild: true},
    {id: 2, label: 'Child 1.1', level: 2, parent: 1, hasChild: true},
    {id: 3, label: 'Child 1.1.1', level: 3, parent: 2, hasChild: false},
    {id: 4, label: 'Child 1.1.2', level: 3, parent: 2, hasChild: false},
    {id: 5, label: 'Child 2', level: 1, parent: null, hasChild: true},
    {id: 6, label: 'Child 2.1', level: 2, parent: 5, hasChild: false},
    {id: 7, label: 'Child 2.2', level: 2, parent: 5, hasChild: false}
]