PROP NAMES
nodes - for nodes
level - start level(optional)
optional - code beside name option(optional, uses code property)

PROP FORMAT
nodes: {
    children: Array(contains same format as node)
    id:
    name:
    code: 
    parent_id: (optional)
}

level - Number

optional - Boolean