Wrapper component for date and time picker

v-model='yyyy-MM-dd HH:MM:SS'
:disabled = 'variable' or v-bind:disabled = 'true' (if string)
:date = 'object'
:time = 'object'
:left = 'boolean' or v-bind:left = 'true' (if string)

Pls refer to corresponding readme for props
datepicker vuejs-datepicker
timepicker vue2-timepicker

use new Date() in v-model to set current time