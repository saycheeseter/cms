export default {

    data: function(){
        return {
        }
    },

    directives: {
        focus: {
            inserted: function(el, binding) {
                el.focus();
                el.id = 'focused';
            },

            updated: function() {
                el.focus();
            }
        }
    },

    methods: {
        computePagination(pagination, action) {
            let that = this;

            let addRecord = (action == "delete") ? -1 : 1;

            //add or subtract record from the that.table.pagination total
            pagination.total += addRecord;

            //check first if the total count plus or minus the record for this page is greater or less than to the per page setting..
            //then decide what to do
            if((pagination.count + addRecord) > pagination.per_page ){
                pagination.current_page = pagination.total_pages;

                let page = pagination.total_pages;

                if((pagination.total % pagination.per_page) == 1  || pagination.per_page ==1){
                    page += 1;
                }

                //database paginate for main table only
                that.paginate(page);
            }else{
                pagination.count += addRecord;

                //computation of pagination will only apply on delete action for this scenario
                // and if the current page is not one
                if(action == 'delete'){
                    let page = pagination.current_page

                    //if total count of the current page is exhausted
                    //go back to previous page
                    if(pagination.count <= 0){
                        that.paginate(page - 1);
                    }

                    if(pagination.total_pages != pagination.current_page){
                        that.paginate(page);
                    }
                }
            }
        },

        changeState(index, action, target = "") {
            if(target == "") {
                //for single datatable instance
                for (var i = 0; i < this.$children.length; i++)
                {
                    let componentName = this.$children[i].$options._componentTag
                    if(componentName == 'datatable'){
                        this.$children[i].catchState(index, action);
                    }
                }
            }else{
                //for multiple datatable instance with target argument
                this.$refs[target].catchState(index, action);
            }
        },

        toggleFocus() {
            document.getElementById("focused").focus();
        },

        finalized(target = "") {
            let isFinalized = true;

            for(var i = 0; i  < this.$refs[target].$children.length; i++){
                let child =  this.$refs[target].$children[i];
                if(child.$options._componentTag == 'table-display' && child.$data.action == "update"){
                    isFinalized = false;
                    break;
                }
            }

            return isFinalized;
        },

        selectRow(data, target = "") {
            let datatable = this.$refs[target];
            let select = datatable.settings.withSelect.value;
            let selected = data[select];

            for(var i = 0; i  < datatable.$children.length; i++){
                let child =  datatable.$children[i];
                if(child.$options._componentTag == 'table-display' && child.passdata[select] == selected){
                    let selected = child.selected();
                    child.checkRow((selected == '') ? true : false );
                }
            }
        },
    }
}

