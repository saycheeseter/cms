PROP NAMES
filters
v-modal

PROP FORMAT
filters:
    [
        //filters
        // name - label
        // columnName - backend reference
        // type - defines input type

        // optionBool - if option is included 
        // default - default value
        // selected - default selected filter
        // data - data from backend compact
        // headers - option headers
        // columns - columns from collection to be displayed
        // returncolumn - return column
        // 
        // selectOptions - used for select
        {
            name:'Product Name', 
            columnName: 'productname', 
            type:'string', 
            optionBool: false,
            default: 'name',
            selected: true,
            limit: 1
        },
        {
            name:'Brand', 
            columnName: 'brand', 
            type: 'string', 
            optionBool: true,
            tableOptions: {
                data: [],
                headers: ['id', 'code', 'Brand Name'], 
                columns: ['id', 'code', 'name'], 
                returncolumn: 'name' 
            }
        },
        {
            name: 'Inventory Status',
            columnName: '', //special
            type: 'select',
            selectOptions: [
                {id: '1', label: 'all'}
            ]
        },
        {
            name: Lang.t('label.branch'),
            columnName: 'branch_id',
            secondaryColumn: 'branch_detail_id',
            type: 'doubleSelect',
            withAll: true,
            selectOptions: [
                { id: 1, label: 'Main' },
                { id: 2, label: 'Branch 2' }
            ],
            secondarySelectOptions: [
                { id: 1, label: 'Main' },
                { id: 2, label: 'Branch 2' }
            ],
            withCallback: true,
            callback: function(id) {
                return BranchService.details(id);
            }
        },

         {
            name: Lang.t('label.member_rate'),
            columnName: 'rate_head_id',
            type: 'chosen',
            selectOptions: rates
        }
    ]

filtermodal requires v-show variable

emits input for v-modal and search for event
search_obj: {
    value: ,
    operator: ,
    join: ,
    column: ,
}