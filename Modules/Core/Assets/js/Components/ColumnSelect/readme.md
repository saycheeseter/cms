Prop names
columns
v-model = selections

Column Format
columns: [
    {alias: 'unitBarcode', label: 'Unit Barcode', default: true}, 
    {alias: 'chineseName', label: 'Chinese Name'},
    {alias: 'brand', label: 'Brand'},
    {alias: 'category', label: 'Category'},
    {alias: 'supplier', label: 'Supplier'},
    {alias: 'inventory', label: 'Inventory'}
],

default - if checked

Modal visibility
columnSettings: {
    visible: false
},

Container for boolean value, property name will be alias from column prop
selections: {},
