PROP NAMES
options - Requires id and label
v-bind:multiple - Transforms into required type
v-bind:disabled - disable element
v-model - variable to bind(will default)
row - for attribute

Prop Options
options
[
    {id: '1', label: 'First', attribute: (optional)},
    {id: '2', label: 'Second', attribute: (optional)},
    {id: '3', label: 'Third', attribute: (optional)},
]

multiple
'false'
-multiple option

value
['1'] (multiple)
 or 
'1' (single)

Emits 'select' and returns an array of selected ids(multiple) or single id(single)

Event Dispatcher
ChosenDispatcher

Attribute
emits 'attribute' with object 
{
    row: '',
    attribute: ''
}

<chosen :options='uoms' v-model='row.data.uom_id' :row='row.index' @attribute='function_to_call'></chosen>

function_to_call(data) {
    this.setAttributeField(data, this.tableUnit.data, 'qty');
}