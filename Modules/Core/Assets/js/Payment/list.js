import FinancialFlow from '../FinancialFlow/list.js';
import PaymentService from '../Services/PaymentService';
import Arr from '../Classes/Arr';

let app = new Vue({
    el: '#app',

    mixins: [FinancialFlow],

    data: {
        modal: {
            columnSettings: {
                columns: [
                    { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                    { alias: 'date', label: Lang.t('label.date'), default: true },
                    { alias: 'location', label: Lang.t('label.location'), default: true },
                    { alias: 'supplier', label: Lang.t('label.supplier'), default: true },
                    { alias: 'amount', label: Lang.t('label.amount'), default: true },
                    { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                    { alias: 'deleted', label: Lang.t('label.deleted'), default: true },
                ],
                module: {
                    id: MODULE.PAYMENT,
                    key: 'info'
                }
            },
            filter: {
                options: Arr.merge(FinancialFlow.data.modal.filter.options, [
                    {
                        name: Lang.t('label.deleted'),
                        columnName: 'payment.deleted_at',
                        type: 'select',
                        selected: true,
                        selectOptions: [
                            { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                            { id: String(VOIDED.NO), label: Lang.t('label.no') },
                        ],
                        default: { value: String(VOIDED.NO) }
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier.full_name',
                        type: 'string', 
                        optionBool: true,
                        tableOptions: {
                            data: suppliers,
                            headers: ['id', Lang.t('label.supplier')], 
                            columns: ['id', 'full_name'], 
                            returncolumn: 'full_name' 
                        }
                    }
                ])
            }
        },
        service: new PaymentService
    },
});