import FinancialFlow from '../FinancialFlow/detail.js';
import PurchaseInboundService from '../Services/PurchaseInboundService';
import PaymentService from '../Services/PaymentService';
import SupplierService from '../Services/SupplierService';
import PurchaseReturnOutboundService from "../Services/PurchaseReturnOutboundService";

let app = new Vue({
    el: '#app',

    mixins: [FinancialFlow],

    data: {
        selector: {
            transaction: {
                columnSettings: {
                    columns: [
                        { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                        { alias: 'created_for', label: Lang.t('label.created_for') },
                        { alias: 'for_location', label: Lang.t('label.for_location') },
                        { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                        { alias: 'supplier', label: Lang.t('label.supplier') },
                        { alias: 'payment_method', label: Lang.t('label.payment_method') },
                        { alias: 'term', label: Lang.t('label.term') },
                        { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                        { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                        { alias: 'created_date', label: Lang.t('label.created_date') },
                        { alias: 'audited_by', label: Lang.t('label.audit_by') },
                        { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                        { alias: 'deleted', label: Lang.t('label.deleted') },
                        { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                        { alias: 'reference', label: Lang.t('label.reference'), },
                        { alias: 'amount', label: Lang.t('label.amount'), default: true },
                        { alias: 'remaining_amount', label: Lang.t('label.remaining_amount'), default: true },
                    ],
                    module: {
                        id: MODULE.PAYMENT,
                        key: 'transaction'
                    }
                }
            }
        },

        format: {
            references: {
                payable: 'numeric'
            }
        },

        info: new Data({
            id: 0,
            created_for: Laravel.auth.branch,
            supplier_id: null,
            entity_id: null,
            transaction_date: '',
            approval_status: APPROVAL_STATUS.NEW,
            remarks: '',
            sheet_number: '-',
            is_deleted: false
        }),

        attachment: {
            module: MODULE.PAYMENT
        },

        references: {
            suppliers: suppliers,
        },

        service: {
            payment: new PaymentService,
            balance: SupplierService,
        },

        keys: {
            // balance: 'supplier',
            amount: 'payable'
        },
    },

    methods: {
        showPurchaseInboundTransactionSelector() {
            this.service.transaction = new PurchaseInboundService();
            this.selector.transaction.type = types.purchase_inbound;
            this.showTransactionSelector();
        },

        showPurchaseReturnOutboundTransactionSelector() {
            this.service.transaction = new PurchaseReturnOutboundService();
            this.selector.transaction.type = types.purchase_return_outbound;
            this.showTransactionSelector();
        },

        getTransactionSelectorApi(settings) {
            return this.service.transaction.transactionsWithBalance(Object.assign({},
                {
                    branch_id: this.info.created_for,
                    supplier_id: this.info.supplier_id
                },
                settings)
            );
        },

        validateRequiredEntity() {
            if(this.info.supplier_id === null) {
                this.message.transaction.error.set([Lang.t('validation.supplier_required')]);
                return false;
            }

            return true;
        },

        isPurchaseInbound(type) {
            return type === types.purchase_inbound;
        }
    },

    watch: {
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.created_for = oldValue;
            this.buffer.data.supplier_id = this.info.supplier_id;
        },
        'info.supplier_id': function(newValue, oldValue) {
            this.buffer.data.supplier_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.info.entity_id = newValue;
        },
        'action.value': function(newValue) {
            // switch(newValue) {
            //     case 'supplier': {
            //         this.getBalance();
            //     }
            // }
        }
    },
});