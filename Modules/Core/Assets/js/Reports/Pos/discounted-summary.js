import CoreMixins from '../../Common/CoreMixins';
import DiscountSummaryReportService from "../../Services/DiscountSummaryReportService";
import TerminalService from "../../Services/TerminalService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        DateTimePicker
    },

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        filter: {
            transaction: {
                date_from: Moment().startOf('day').format(),
                date_to: Moment().endOf('day').format(),
                branch: Laravel.auth.branch,
                terminal: null
            }
        },
        references: {
            branches: branches,
            terminals: []
        },
        modal: {
            filter: {
                visible: false,
                options: [],
                data: {}
            }
        },
        message: new Notification
    },
    mounted() {
        this.populateTerminals();
    },
    methods: {
        paginate(page = 1) {
            let that = this;

            if (that.processing || !that.isValidDateRange()) {
                return false;
            }

            this.processing = true;
            this.message.reset();

            DiscountSummaryReportService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: this.filters
            })
            .then(function (response) {
                that.datatable.value.data = response.data.values.data;
                that.message.info = response.data.message;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },
        isValidDateRange(){
            if (this.filter.transaction.date_from > this.filter.transaction.date_to) {
                this.message.error.set({ error: Lang.t('error.date_range_incorrect') });
                return false;
            }

            return true;
        },
        populateTerminals(){
            let that = this;

            TerminalService.getByBranch(this.filter.transaction.branch)
                .then(function (response) {
                    that.references.terminals = that.formatTerminalFilter(response.data.values.terminals);
                    that.filter.transaction.terminal = null;
                })
                .catch(function (error) {
                    that.references.terminals = [];
                    that.filter.transaction.terminal = 0;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },
        formatTerminalFilter(data){
            let terminals = [{
                id: null,
                label: Lang.t('label.all_terminals')
            }];

            for(let x in data) {
                terminals.push({
                    id: data[x].number,
                    label: data[x].number
                })
            }

            return terminals;
        }

    },

    computed: {
        filters: function() {
            return [
                {
                    value: this.filter.transaction.date_from,
                    operator: '>=',
                    join: 'AND',
                    column: 'date_from'
                },
                {
                    value: this.filter.transaction.date_to,
                    operator: '<=',
                    join: 'AND',
                    column: 'date_to'
                },
                {
                    value: this.filter.transaction.branch,
                    operator: '=',
                    join: 'AND',
                    column: 'transaction_branch_id'
                },
                {
                    value: this.filter.transaction.terminal,
                    operator: '=',
                    join: 'AND',
                    column: 'transaction_terminal_number'
                },
            ];
        }
    },
    watch: {
        'filter.transaction': {
            handler: function (data) {
                this.datatable.value.data = [];
                this.datatable.value.pagination = {};
            },
            deep: true
        }
    }
});