import CoreMixins from '../../Common/CoreMixins';
import SeniorTransactionService from "../../Services/SeniorTransactionService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: Laravel.auth.branch }
                    },
                    {
                        name: Lang.t('label.or_si_no'),
                        columnName: 'or_number',
                        type: 'string'
                    },
                    {
                        name : Lang.t('label.transaction_date_time'),
                        columnName : 'transaction_date',
                        type : 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().endOf('day').format() },
                        ],
                        limit: 2
                    }
                ],
                data: [],
            }
        },
        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [],
            selections: []
        },

        message: new Notification
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            SeniorTransactionService.paginate({
                perPage: this.datatable.settings.perPage,
                page: page,
                filters: this.modal.filter.data
            })
            .then(function (response) {
                let transactions = response.data.values.data;

                for (let index = 0; index < transactions.length; index++) {
                    let transaction = transactions[index];

                    transaction['amount'] = Numeric.format(
                        Numeric.parse(transaction['qty']) *
                        Numeric.parse(transaction['price'])
                    );

                    transactions[index] = transaction;
                }

                that.message.info = response.data.message;
                that.datatable.value.data = transactions;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        }
    }
});