import CoreMixins from '../Common/CoreMixins';
import CashTransactionService from "../Services/CashTransactionService";
import OtherIncomeService from "../Services/OtherIncomeService";
import PaymentService from "../Services/PaymentService";
import CollectionService from "../Services/CollectionService";
import OtherPaymentService from "../Services/OtherPaymentService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        DateTimePicker
    },

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: false,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        references: {
            branches: branches,
        },
        filter:{
            transaction:{
                date_from: Moment().startOf('day'),
                date_to: Moment().endOf('day'),
                branch: null,
            },
        },
        columnSettings: {
            id: 'column-settings',
            visible: false,
            columns: [],
            selections: []
        },

        message: new Notification,
    },

    created() {
        this.references.branches.unshift({
            id: null,
            label: Lang.t('label.all_branches')
        });
    },

    mounted: function(){
        this.search();
    },

    methods: {
        search() {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            CashTransactionService.search({
                'filters': that.filter.transaction
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.pagination.current_page = 1;
                that.createRunningBalance(response.data.values.records, response.data.values.beginning);
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },
        createRunningBalance(data, beg) {
            let beginning = Numeric.parse(beg.total);

            let running = beginning;
            let records = [];

            for (let i = 0; i < data.length; i++) {
                let current = data[i];

                running += (Numeric.parse(current.in) - Numeric.parse(current.out));

                current['amount'] = Numeric.format(running);

                records.push(current);
            }

            this.datatable.value.data = records;

            this.createInitialBalance(beginning);
        },
        createInitialBalance(beginning) {
            this.datatable.value.data.unshift({
                date: '',
                particular: '',
                in: '',
                out: '',
                type: null,
                reference_id: null,
                amount: Numeric.format(beginning),
                remarks: ''
            });
        },
        goToPage(type, id) {
            let service = null;

            switch (type) {
                case 'payment':
                    service = new PaymentService();
                    break;

                case 'other_payment':
                    service = OtherPaymentService;
                    break;

                case 'collection':
                    service = new CollectionService();
                    break;

                case 'other_income':
                    service = OtherIncomeService;
                    break;
            }

            if (null !== service) {
                service.view(id);
            }
        }
    },
    computed: {
        totalIn: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['in']);
            }

            return Numeric.format(total);
        },
        totalOut: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['out']);
            }

            return Numeric.format(total);
        },
        total: function() {
            return Numeric.format(Numeric.parse(this.totalIn) - Numeric.parse(this.totalOut));
        }
    }
});