import ProductPrice from '../ProductPrice/main.js';
import SupplierProductPriceService from '../../Services/SupplierProductPriceService';

let app = new Vue({
    el: '#app',

    mixins: [ProductPrice],

    data: {
        modal: {
            filter: {
                options: [
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier.full_name',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: suppliers,
                            headers: [Lang.t('label.code'), Lang.t('label.full_name')],
                            columns: ['code', 'full_name'],
                            returncolumn: 'full_name'
                        }
                    },
                ],
            }    
        },
    },

    methods: {
      paginate(page) {
            let that = this;

            if (that.processing) {
                return false;
            }

            this.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            SupplierProductPriceService.paginate({
                'perPage': that.datatable.details.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
                'products': that.datatable.product.checked,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.details.value.data = response.data.values.history;
                that.datatable.details.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },
    },
});
