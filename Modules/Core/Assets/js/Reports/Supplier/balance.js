import BalanceFlow from '../../BalanceFlow/balance.js';
import SupplierBalanceFlowService from '../../Services/SupplierBalanceFlowService.js';

let app = new Vue({
    el: '#app',

    mixins: [BalanceFlow],

    data: {
        modal: {
            filter: {
                options: Arr.unshift(BalanceFlow.data.modal.filter.options, {
                    name: Lang.t('label.supplier'),
                    columnName: 'supplier.full_name',
                    type: 'string',
                    optionBool: true,
                    tableOptions: {
                        data: suppliers,
                        headers: [Lang.t('label.code'), Lang.t('label.full_name')],
                        columns: ['code', 'full_name'],
                        returncolumn: 'full_name'
                    },
                    limit: 1
                })
            }
        },
        
        service: new SupplierBalanceFlowService
    }
});