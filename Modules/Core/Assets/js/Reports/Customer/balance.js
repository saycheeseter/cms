import BalanceFlow from '../../BalanceFlow/balance.js';
import CustomerBalanceFlowService from '../../Services/CustomerBalanceFlowService.js';

let app = new Vue({
    el: '#app',

    mixins: [BalanceFlow],

    data: {
        modal: {
            filter: {
                options: Arr.unshift(BalanceFlow.data.modal.filter.options, {
                    name: Lang.t('label.customer'),
                    columnName: 'customer.name',
                    type: 'string',
                    optionBool: true,
                    tableOptions: {
                        data: customers,
                        headers: [Lang.t('label.code'), Lang.t('label.name')],
                        columns: ['code', 'name'],
                        returncolumn: 'name'
                    },
                    limit: 1
                })
            }
        },
        
        service: new CustomerBalanceFlowService
    }
});