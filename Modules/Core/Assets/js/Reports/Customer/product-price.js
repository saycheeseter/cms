import ProductPrice from '../ProductPrice/main.js';
import CustomerProductPriceService from '../../Services/CustomerProductPriceService';

let app = new Vue({
    el: '#app',

    mixins: [ProductPrice],

    data: {
        modal: {
            filter: {
                options: [
                    {
                        name: Lang.t('label.customer'),
                        columnName: 'customer.name',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: customers,
                            headers: [Lang.t('label.code'), Lang.t('label.name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                ],
            }    
        },
    },

    methods: {
        paginate(page) {
            let that = this;

            if (that.processing) {
                return false;
            }

            this.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            CustomerProductPriceService.paginate({
                'perPage': that.datatable.details.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
                'products': that.datatable.product.checked,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.details.value.data = response.data.values.history;
                that.datatable.details.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },
    },
});
