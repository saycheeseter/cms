import CoreMixins from '../../Common/CoreMixins';
import CategoryMonthlySalesRankingService from '../../Services/CategoryMonthlySalesRankingService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier_id',
                        type: 'select',
                        selectOptions: suppliers,
                    },
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                    },
                    {
                        name: Lang.t('label.location'),
                        columnName: 'for_location',
                        type: 'select',
                        selectOptions: locations
                    },
                    {
                        name: Lang.t('label.category'),
                        columnName: 'category_id',
                        type: 'select',
                        selectOptions: categories
                    },
                    {
                        name: Lang.t('label.brand'),
                        columnName: 'brand_id',
                        type: 'select',
                        selectOptions: brands
                    },
                    {
                        name: Lang.t('label.date'),
                        type:'yearMonth',
                        columnName: 'year_month',
                        limit: 2
                    }
                ],
                data: {},
            }
        },
        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'code', label: Lang.t('label.code'), default: true },
                { alias: 'total_qty', label: Lang.t('label.total_qty'), default: true },
                { alias: 'total_amount', label: Lang.t('label.total_amount'), default: true },
                { alias: 'total_profit', label: Lang.t('label.total_profit'), default: true },
            ],
            selections: []
        },

        message: new Notification
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.modal.filter.visible = false;
            this.message.reset();

            CategoryMonthlySalesRankingService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.datatable.value.data = response.data.values.categories.data;
                that.message.info = response.data.message;
                that.datatable.value.pagination = response.data.values.categories.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        }
    }
});