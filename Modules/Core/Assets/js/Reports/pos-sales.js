import CoreMixins from '../Common/CoreMixins';
import PosSalesReportService from "../Services/PosSalesReportService";
import TerminalService from "../Services/TerminalService";
import Arr from "../Classes/Arr";
import PosSalesService from "../Services/PosSalesService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],
    data: {
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        limit: 1,
                        selected: true,
                        default: {
                            value: Laravel.auth.branch
                        }
                    },
                    {
                        name: Lang.t('label.transaction_date_time'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                        ],
                        limit: 2
                    },
                ],
                data: []
            }
        },
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        references: {
            terminals: []
        },
        message: new Notification,
    },

    mounted: function() {
        this.search();
    },

    methods: {
        search()  {
            let that = this;
            let branch = this.getBranchFromFilter();

            if (that.processing || null === branch) {
                return false;
            }

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            TerminalService.getByBranch(branch)
                .then(function (response) {
                    that.references.terminals = Arr.pluck(response.data.values.terminals, 'number');
                    that.processing = false;
                    that.paginate();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.visible = false;
            this.processing = true;
            this.message.reset();

            PosSalesReportService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
                'terminals': that.references.terminals,
            }).then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.summary.data;
                that.datatable.value.pagination = response.data.values.summary.meta.pagination;
            }).catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        getBranchFromFilter() {
            for(let index in this.modal.filter.data) {
                let filter = this.modal.filter.data[index];

                if (filter.column === 'created_for') {
                    return filter.value;
                }
            }

            return null;
        },

        transactions(date) {
            if (!Moment(date).isValid()) {
                return false;
            }

            let filters = Arr.findBy(this.modal.filter.data, [['column', 'created_for']])
                .concat([
                    {
                        column: "transaction_date",
                        join: "and",
                        operator: ">=",
                        value: Moment(date).startOf('day').format()
                    },
                    {
                        column: "transaction_date",
                        join: "and",
                        operator: "<",
                        value: Moment(date).add(1, 'day').startOf('day').format()
                    }
                ]);

            PosSalesService.open(filters);
        }
    },
});
