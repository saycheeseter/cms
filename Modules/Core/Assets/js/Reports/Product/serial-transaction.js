import CoreMixins from '../../Common/CoreMixins';
import SerialNumberService from "../../Services/SerialNumberService";
import PurchaseInboundService from "../../Services/PurchaseInboundService";
import PurchaseReturnOutboundService from "../../Services/PurchaseReturnOutboundService";
import DamageOutboundService from "../../Services/DamageOutboundService";
import SalesOutboundService from "../../Services/SalesOutboundService";
import SalesReturnInboundService from "../../Services/SalesReturnInboundService";
import StockDeliveryOutboundService from "../../Services/StockDeliveryOutboundService";
import StockDeliveryInboundService from "../../Services/StockDeliveryInboundService";
import StockReturnOutboundService from "../../Services/StockReturnOutboundService";
import StockReturnInboundService from "../../Services/StockReturnInboundService";
import InventoryAdjustService from "../../Services/InventoryAdjustService";
import StockDeliveryInboundService from "../../Services/StockDeliveryInboundService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: false,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
        },
        serialNumberText: '',
        serial: new Data({
            id: null,
            product: {
                name: '-',
                chineseName: '-',
                barcodes: '-',
                stockNo: '-'
            },
        }),
        message: new Notification,
    },

    methods: {
        findSerialByNumber() {
            let searchable = this.serialNumberText.replace(/ +(?= )/g,'').trim();

            if (this.processing || searchable === '') {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            SerialNumberService.findByNumber(searchable)
                .then((response) => {
                    that.serial.id = response.data.values.serial.id;
                    that.serial.product.name = response.data.values.serial.product.name;
                    that.serial.product.chineseName = response.data.values.serial.product.chinese_name;
                    that.serial.product.barcodes = response.data.values.serial.product.barcodes;
                    that.serial.product.stockNo = response.data.values.serial.product.stock_no;
                    that.serialNumberText = response.data.values.serial.serial_number;
                    that.getTransactions();
                })
                .catch((error) => {
                    that.serial.reset();
                    that.datatable.value.data = [];
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        getTransactions() {
            let that = this;

            SerialNumberService.getTransactions(this.serial.id)
                .then((response) => {
                    that.processing = false;
                    that.datatable.value.data = response.data.values.transactions;
                })
                .catch((error) => {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        goToTransaction(type, transactionId) {
            let service = null;

            switch (type) {
                case SERIAL_TRANSACTION.PURCHASE_INBOUND:
                    service = new PurchaseInboundService();
                    break;

                case SERIAL_TRANSACTION.PURCHASE_RETURN_OUTBOUND:
                    service = new PurchaseReturnOutboundService();
                    break;

                case SERIAL_TRANSACTION.DAMAGE_OUTBOUND:
                    service = new DamageOutboundService();
                    break;

                case SERIAL_TRANSACTION.SALES_OUTBOUND:
                    service = new SalesOutboundService();
                    break;

                case SERIAL_TRANSACTION.SALES_RETURN_INBOUND:
                    service = new SalesReturnInboundService();
                    break;

                case SERIAL_TRANSACTION.STOCK_DELIVERY_OUTBOUND:
                    service = new StockDeliveryOutboundService();
                    break;

                case SERIAL_TRANSACTION.STOCK_DELIVERY_INBOUND:
                    service = new StockDeliveryInboundService();
                    break;

                case SERIAL_TRANSACTION.STOCK_RETURN_OUTBOUND:
                    service = new StockReturnOutboundService();
                    break;

                case SERIAL_TRANSACTION.STOCK_RETURN_INBOUND:
                    service = new StockReturnInboundService();
                    break;

                case SERIAL_TRANSACTION.INVENTORY_ADJUST:
                    service = new InventoryAdjustService();
                    break;
            }

            if (null === service) {
                return;
            }

            service.edit(transactionId);
        },
    }
});