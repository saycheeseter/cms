import CoreMixins from '../../Common/CoreMixins';
import ProductMixins from '../../Common/ProductMixins';
import ProductInventoryValueService from '../../Services/ProductInventoryValueService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }  
        },

        summaries: {
            positive_ma_cost_amount: 0,
            negative_ma_cost_amount: 0,
            positive_inventory: 0,
            negative_inventory: 0,
            positive_cost_amount: 0,
            negative_cost_amount: 0
        },

        message: new Notification,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            let that = this;

            if (that.processing) {
                return false;
            }

            this.filter.product.visible = false;
            this.processing = true;
            this.message.reset();

            ProductInventoryValueService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.filter.product.data
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.products;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;

                if(page === undefined) {
                    that.getSummary();
                }
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        getSummary() {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.processing = true;

            ProductInventoryValueService.getSummary(that.filter.product.data)
                .then(function (response) {
                    that.summaries = response.data.values.summaries;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },
    },

    computed: {
        totalMACostAmount : function () {
            return Numeric.format(
                Numeric.parse(this.summaries.positive_ma_cost_amount)
                + Numeric.parse(this.summaries.negative_ma_cost_amount)
            );
        },

        netInventoryQty: function () {
            return Numeric.format(
                Numeric.parse(this.summaries.positive_inventory)
                + Numeric.parse(this.summaries.negative_inventory)
            );
        },

        totalCostAmount: function() {
            return Numeric.format(
                Numeric.parse(this.summaries.positive_cost_amount)
                + Numeric.parse(this.summaries.negative_cost_amount)
            );
        }
    }
});
