import ProductMixins from '../../Common/ProductMixins';
import CoreMixins from '../../Common/CoreMixins';
import ProductTransactionListService from '../../Services/ProductTransactionListService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins, ProductMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging: true,
                withPaginateCallBack: true,
                withActionCell: false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        filter: {
            transaction: {
                from: Moment().format(),
                to: Moment().format(),
                location: null
            },
            product: {
                options: [
                    { name: Lang.t('label.stock_no'), columnName: 'stock_no', type: 'string', optionBool: false },
                    { name: Lang.t('label.unit_barcode'), columnName: 'barcode', type: 'string', optionBool: false },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    { name: Lang.t('label.chinese_name'), columnName: 'chinese_name', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: PRODUCT_STATUS.INACTIVE, label: Lang.t('label.inactive') },
                            { id: PRODUCT_STATUS.ACTIVE, label: Lang.t('label.active') },
                            { id: PRODUCT_STATUS.DISCONTINUED, label: Lang.t('label.discontinued') },
                        ],
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.suppliers,
                            headers: [Lang.t('label.code'), Lang.t('label.supplier_name')],
                            columns: ['code', 'full_name'],
                            returncolumn: 'full_name'
                        }
                    },
                    {
                        name: Lang.t('label.category'),
                        columnName: 'category',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.categories,
                            headers: [Lang.t('label.code'), Lang.t('label.category_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    {
                        name: Lang.t('label.brand'),
                        columnName: 'brand',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.brands,
                            headers: [Lang.t('label.code'), Lang.t('label.brand_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    { 
                        name: Lang.t('label.date_created'),
                        columnName: 'created_at',
                        type: 'datetime',
                    },
                    { 
                        name: Lang.t('label.last_modified'),
                        columnName: 'updated_at',
                        type: 'datetime',
                    },
                    {
                        name: Lang.t('label.group_type'),
                        columnName: 'group_type',
                        type: 'select',
                        selectOptions: [
                            {id: GROUP_TYPE.NONE, label: Lang.t('label.none')},
                            {id: GROUP_TYPE.MANUAL_GROUP, label: Lang.t('label.manual_group')},
                            {id: GROUP_TYPE.MANUAL_UNGROUP, label: Lang.t('label.manual_ungroup')},
                            {id: GROUP_TYPE.AUTOMATIC_GROUP, label: Lang.t('label.automatic_group')},
                            {id: GROUP_TYPE.AUTOMATIC_UNGROUP, label: Lang.t('label.automatic_ungroup')},
                        ],
                    }
                ]
            },
            advancedInfo:{
                with: {
                    purchase_inbound: false,
                    purchase_return_outbound: false,
                    sales_outbound: false,
                    sales_return_inbound: false,
                    stock_delivery_inbound: false,
                    stock_delivery_outbound: false,
                    stock_return_inbound: false,
                    adjustment_increase: false,
                    stock_return_outbound: false,
                    damage_outbound: false,
                    adjustment_decrease: false,
                    product_conversion_increase: false,
                    product_conversion_decrease: false,
                    auto_product_conversion_increase: false,
                    auto_product_conversion_decrease: false,
                    pos_sales: false,
                },
                without: {
                    purchase_inbound: false,
                    purchase_return_outbound: false,
                    sales_outbound: false,
                    sales_return_inbound: false,
                    stock_delivery_inbound: false,
                    stock_delivery_outbound: false,
                    stock_return_inbound: false,
                    adjustment_increase: false,
                    stock_return_outbound: false,
                    damage_outbound: false,
                    adjustment_decrease: false,
                    product_conversion_increase: false,
                    product_conversion_decrease: false,
                    auto_product_conversion_increase: false,
                    auto_product_conversion_decrease: false,
                    pos_sales: false,
                },
                all: {
                    with: false,
                    without: false
                }
            }
        },
        references: {
            locations: locations
        },
        advancedInfo: false,
        message: new Notification
    },

    mounted: function(){
        this.references.locations.unshift({
            id: null,
            label: Lang.t('label.all')
        });

        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            this.filter.product.visible = false;

            if (that.processing || !that.isValidDateRange()) {
                return false;
            }

            this.processing = true;
            this.message.reset();

            ProductTransactionListService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                basicFilters: this.basicFilters,
                advancedFilters: this.advancedFilters
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.products.data;
                that.datatable.value.pagination = response.data.values.products.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        isValidDateRange(){
            if(Moment(this.filter.transaction.from) > Moment(this.filter.transaction.to)) {
                this.message.error.set({ error: Lang.t('error.date_range_incorrect') });
                return false;
            }

            return true;
        },

        check(event, key, target) {
            if(event.target.checked) {
                this.filter.advancedInfo[target][key] = false;
            } else {
                let other = 'without';
                if(target === 'without') {
                    other = 'with';
                }

                this.filter.advancedInfo.all[other] = false;
            }
        },

        checkAll(event, target) {
            let other = 'without';

            if(target === 'without') {
                other = 'with';
            }

            if(event.target.checked) {
                this.setAll(target, true);
                this.setAll(other, false);
                this.filter.advancedInfo.all[other] = false;
            } else {
                this.setAll(target, false);
            }
        },

        setAll(target, value) {
            for(let key in this.filter.advancedInfo[target]) {
                this.filter.advancedInfo[target][key] = value;
            }
        },

        excel() {
            ProductTransactionListService.export({
                basicFilters: this.basicFilters,
                advancedFilters: this.advancedFilters
            });
        }
    },

    computed: {
        basicFilters: function() {
            let filters = this.filter.product.data.concat([
                {
                    value: this.filter.transaction.from,
                    operator: '>=',
                    join: 'AND',
                    column: 'transaction_date'
                },
                {
                    value: this.filter.transaction.to,
                    operator: '<=',
                    join: 'AND',
                    column: 'transaction_date'
                }
            ]);

            if(this.filter.transaction.location !== null) {
                filters.push({
                    value: this.filter.transaction.location,
                    operator: '=',
                    join: 'AND',
                    column: 'branch_detail_id'
                });
            }

            return filters;
        },
        advancedFilters: function() {
            let withFilters = [];
            let withoutFilters = [];

            for(let key in this.filter.advancedInfo.with) {
                if(this.filter.advancedInfo.with[key]) {
                    withFilters.push(key);
                }
            }

            for(let key in this.filter.advancedInfo.without) {
                if(this.filter.advancedInfo.without[key]) {
                    withoutFilters.push(key);
                }
            }

            return {
                with: withFilters,
                without: withoutFilters
            };
        }
    },

    watch: {
        'filter.transaction': {
            handler: function(data) {
                this.datatable.value.data = [];
                this.datatable.value.pagination = {};
            },
            deep: true
        },
        'filter.advancedInfo': {
            handler: function(data) {
                this.datatable.value.data = [];
                this.datatable.value.pagination = {};
            },
            deep: true
        }
    }
});