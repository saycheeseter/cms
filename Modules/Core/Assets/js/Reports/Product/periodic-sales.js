import ProductMixins from '../../Common/ProductMixins';
import CoreMixins from '../../Common/CoreMixins';
import DateUnit from '../../Classes/DateUnit';
import ProductPeriodicSalesService from '../../Services/ProductPeriodicSalesService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging: true,
                withPaginateCallBack: true,
                withActionCell: false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        filter: {
            transaction: {
                fromMonth: Moment().format('M'),
                fromYear: Moment().format('Y'),
                toMonth: Moment().format('M'),
                toYear: Moment().format('Y'),
                branch: Laravel.auth.branch
            }
        },
        references: {
            month: DateUnit.enumMonths(),
            year: DateUnit.enumYears(),
            monthRange: new Data([]),
            branches: branches
        },
        message: new Notification
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            if (that.processing || !that.isValidDateRange()) {
                return false;
            }

            this.filter.product.visible = false;
            this.processing = true;
            this.message.reset();
            this.buildMonthRange();

            ProductPeriodicSalesService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: this.filters
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        isValidDateRange(){
            if(Moment([this.filter.transaction.fromYear, this.filter.transaction.fromMonth])
                > Moment([this.filter.transaction.toYear, this.filter.transaction.toMonth])
            ) {
                this.message.error.set({ error: Lang.t('error.date_range_incorrect') });
                return false;
            }

            return true;
        },

        buildMonthRange() {
            let fromYearMonth = Moment([this.filter.transaction.fromYear, this.filter.transaction.fromMonth - 1]);
            let toYearMonth = Moment([this.filter.transaction.toYear, this.filter.transaction.toMonth - 1]);
            let range = [];

            while (toYearMonth > fromYearMonth || fromYearMonth.format('M') === toYearMonth.format('M')) {
               range.push(fromYearMonth.format('YYYY-MM'));
               fromYearMonth.add(1,'month');
            }

            this.references.monthRange = range;
        },

        excel() {
            ProductPeriodicSalesService.export({
                filters: this.filters
            });
        }
    },

    computed: {
        filters: function() {
            return this.filter.product.data.concat([
                {
                    value: this.filter.transaction.fromYear + '-' + this.filter.transaction.fromMonth,
                    operator: '>=',
                    join: 'AND',
                    column: 'year_month'
                },
                {
                    value: this.filter.transaction.toYear + '-' + this.filter.transaction.toMonth,
                    operator: '<=',
                    join: 'AND',
                    column: 'year_month'
                },
                {
                    value: this.filter.transaction.branch,
                    operator: '=',
                    join: 'AND',
                    column: 'transaction_branch_id'
                },
            ]);
        }
    }
});