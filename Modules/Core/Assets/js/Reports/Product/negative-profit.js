import CoreMixins from '../../Common/CoreMixins';
import NegativeProfitService from '../../Services/NegativeProfitService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.stock_no'), columnName: 'stock_no', type: 'string', optionBool: false },
                    { name: Lang.t('label.unit_barcode'), columnName: 'barcode', type: 'string', optionBool: false },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    { name: Lang.t('label.chinese_name'), columnName: 'chinese_name', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: PRODUCT_STATUS.INACTIVE, label: Lang.t('label.inactive') },
                            { id: PRODUCT_STATUS.ACTIVE, label: Lang.t('label.active') },
                            { id: PRODUCT_STATUS.DISCONTINUED, label: Lang.t('label.discontinued') },
                        ],
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: suppliers,
                            headers: [Lang.t('label.code'), Lang.t('label.supplier_name')],
                            columns: ['code', 'full_name'],
                            returncolumn: 'full_name'
                        }
                    },
                    {
                        name: Lang.t('label.category'),
                        columnName: 'category',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: categories,
                            headers: [Lang.t('label.code'), Lang.t('label.category_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    {
                        name: Lang.t('label.brand'),
                        columnName: 'brand',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: brands,
                            headers: [Lang.t('label.code'), Lang.t('label.brand_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    { 
                        name: Lang.t('label.date_created'),
                        columnName: 'created_at',
                        type: 'datetime',
                    },
                    { 
                        name: Lang.t('label.last_modified'),
                        columnName: 'updated_at',
                        type: 'datetime',
                    },
                    {
                        name: Lang.t('label.group_type'),
                        columnName: 'group_type',
                        type: 'select',
                        selectOptions: [
                            {id: GROUP_TYPE.NONE, label: Lang.t('label.none')},
                            {id: GROUP_TYPE.MANUAL_GROUP, label: Lang.t('label.manual_group')},
                            {id: GROUP_TYPE.MANUAL_UNGROUP, label: Lang.t('label.manual_ungroup')},
                            {id: GROUP_TYPE.AUTOMATIC_GROUP, label: Lang.t('label.automatic_group')},
                            {id: GROUP_TYPE.AUTOMATIC_UNGROUP, label: Lang.t('label.automatic_ungroup')},
                        ],
                    }
                ],
                data: {}
            }
        },
        filter: {
            from: Moment().format(),
            to: Moment().format(),
            branch: null 
        },
        references: {
            branches: branches
        },
        message: new Notification
    },

    mounted: function(){
        this.references.branches.unshift({
            id: null,
            label: Lang.t('label.all')
        });

        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            this.modal.filter.visible = false;

            if (this.processing || !this.isValidDateRange()) {
                return false;
            }

            this.processing = true;
            this.message.reset();

            NegativeProfitService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: this.filters
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.transactions.data;
                that.datatable.value.pagination = response.data.values.transactions.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        isValidDateRange(){
            if(Moment(this.filter.from) > Moment(this.filter.to)) {
                this.message.error.set({ error: Lang.t('error.date_range_incorrect') });
                return false;
            }

            return true;
        },

        excel() {
            NegativeProfitService.export({
                filters: this.filters
            });
        }
    },

    computed: {
        filters: function() {
            let filters = this.modal.filter.data.concat([
                {
                    value: Moment(this.filter.from).startOf('day').format(),
                    operator: '>=',
                    join: 'AND',
                    column: 'transaction_date'
                },
                {
                    value: Moment(this.filter.to).endOf('day').format(),
                    operator: '<=',
                    join: 'AND',
                    column: 'transaction_date'
                }
            ]);

            if(this.filter.branch !== null) {
                filters.push({
                    value: this.filter.branch,
                    operator: '=',
                    join: 'AND',
                    column: 'created_for'
                });
            }

            return filters;
        }
    },

    watch: {
        'filter': {
            handler: function(data) {
                this.datatable.value.data = [];
                this.datatable.value.pagination = {};
            },
            deep: true
        },
    }
});