import ProductMixins from '../../Common/ProductMixins';
import CoreMixins from '../../Common/CoreMixins';
import ProductBranchInventoryService from '../../Services/ProductBranchInventoryService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
        },
        filter: {
            locations: [],
            product: {
                options: [
                    { name: Lang.t('label.stock_no'), columnName: 'stock_no', type: 'string', optionBool: false },
                    { name: Lang.t('label.unit_barcode'), columnName: 'barcode', type: 'string', optionBool: false },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    { name: Lang.t('label.chinese_name'), columnName: 'chinese_name', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: PRODUCT_STATUS.INACTIVE, label: Lang.t('label.inactive') },
                            { id: PRODUCT_STATUS.ACTIVE, label: Lang.t('label.active') },
                            { id: PRODUCT_STATUS.DISCONTINUED, label: Lang.t('label.discontinued') },
                        ],
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.suppliers,
                            headers: [Lang.t('label.code'), Lang.t('label.supplier_name')],
                            columns: ['code', 'full_name'],
                            returncolumn: 'full_name'
                        }
                    },
                    {
                        name: Lang.t('label.category'),
                        columnName: 'category',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.categories,
                            headers: [Lang.t('label.code'), Lang.t('label.category_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    {
                        name: Lang.t('label.brand'),
                        columnName: 'brand',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.brands,
                            headers: [Lang.t('label.code'), Lang.t('label.brand_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    { 
                        name: Lang.t('label.date_created'),
                        columnName: 'created_at',
                        type: 'datetime',
                    },
                    { 
                        name: Lang.t('label.last_modified'),
                        columnName: 'updated_at',
                        type: 'datetime',
                    },
                    {
                        name: Lang.t('label.group_type'),
                        columnName: 'group_type',
                        type: 'select',
                        selectOptions: [
                            {id: GROUP_TYPE.NONE, label: Lang.t('label.none')},
                            {id: GROUP_TYPE.MANUAL_GROUP, label: Lang.t('label.manual_group')},
                            {id: GROUP_TYPE.MANUAL_UNGROUP, label: Lang.t('label.manual_ungroup')},
                            {id: GROUP_TYPE.AUTOMATIC_GROUP, label: Lang.t('label.automatic_group')},
                            {id: GROUP_TYPE.AUTOMATIC_UNGROUP, label: Lang.t('label.automatic_ungroup')},
                        ],
                    }
                ]
            }
        },
        references: {
            locations: {
                list: locations,
                selected: []
            }
        },
        message: new Notification
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            this.filter.product.visible = false;

            if (this.processing || !that.hasSelectedLocations()) {
                return false;
            }

            this.processing = true;
            this.message.reset();

            ProductBranchInventoryService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: this.filters
            })
            .then(function (response) {
                that.buildTableHeaderLocations();
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        hasSelectedLocations() {
            if(this.filter.locations.length === 0) {
                this.message.error.set({ error: Lang.t('error.branch_location_required') });
                return false;
            }

            return true;
        },

        buildTableHeaderLocations() {
            let locations = []

            for(let i in this.filter.locations) {
                for(let x in this.references.locations.list) {
                    let current = this.references.locations.list[x];
                    if(current.id === this.filter.locations[i]) {
                        locations.push(current);
                    }
                }
            }

            this.references.locations.selected = locations;
        },

        excel() {
            if(this.datatable.value.data.length === 0) {
                this.message.error.set({ error: Lang.t('info.no_results_found') });
                return false;
            }

            ProductBranchInventoryService.export({
                filters: this.filters
            });
        }
    },

    computed: {
        filters: function() {
            let locations = [];

            for(let index in this.filter.locations) {
                locations.push({
                    value: this.filter.locations[index],
                    operator: '=',
                    join: 'OR',
                    column: 'branch_detail_id'
                });
            }

            return this.filter.product.data.concat(locations);
        }
    },

    watch: {
        'filter.locations': function(newValue, oldValue) {
            this.datatable.value.data = [];
            this.datatable.value.pagination = {};
        }
    }
});