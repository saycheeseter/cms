import CoreMixins from '../../Common/CoreMixins';
import ProductMonthlySalesRankingService from "../../Services/ProductMonthlySalesRankingService";
import ProductMixins from "../../Common/ProductMixins";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],
    data: {
        modal: {
            filter: {
                product: {
                    visible: false,
                    options: ProductMixins.data.filter.product.options,
                    data: []
                },
                monthlySales: {
                    visible: false,
                    options: [
                        {
                            name: Lang.t('label.branch'),
                            columnName: 'created_for',
                            type: 'select',
                            selectOptions: branches,
                            selected: true,
                            default: { value: Laravel.auth.branch }
                        },
                        {
                            name: Lang.t('label.for_location'),
                            columnName: 'for_location',
                            type: 'select',
                            selectOptions: locations,
                        },
                        {
                            name: Lang.t('label.date'),
                            type:'yearMonth',
                            columnName: 'year_month',
                            limit: 2
                        }
                    ],
                    data: []
                }
            }
        },
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        columnSettings: {
            id: 'column-settings',
            visible: false,
            columns: [
                { alias: 'stock_no', label: Lang.t('label.stock_no'), default: true },
                { alias: 'chinese_name', label: Lang.t('label.chinese_name'), default: false },
                { alias: 'description', label: Lang.t('label.description'), default: false },
                { alias: 'brand', label: Lang.t('label.brand'), default: true },
                { alias: 'category', label: Lang.t('label.category'), default: true },
                { alias: 'supplier', label: Lang.t('label.supplier'), default: true },
                { alias: 'total_qty', label: Lang.t('label.total_qty'), default: true },
                { alias: 'total_amount', label: Lang.t('label.total_amount'), default: true },
                { alias: 'total_profit', label: Lang.t('label.total_profit'), default: true }
            ],
            selections: []
        },
        message: new Notification,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.product.visible = false;
            this.modal.filter.monthlySales.visible = false;
            this.processing = true;
            this.message.reset();

            ProductMonthlySalesRankingService.paginate({
                'perPage': this.datatable.settings.perPage,
                'page': page,
                'filters': this.filters,
            }).then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.products.data;
                that.datatable.value.pagination = response.data.values.products.meta.pagination;
            }).catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },
    },

    computed: {
        filters: function () {
            return this.modal.filter.monthlySales.data.concat(this.modal.filter.product.data);
        }
    }
});
