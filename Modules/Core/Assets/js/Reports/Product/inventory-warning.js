import CoreMixins from '../../Common/CoreMixins';
import ProductService from '../../Services/ProductService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'branch_id',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: Laravel.auth.branch }
                    },
                    {
                        name: Lang.t('label.location'),
                        columnName: 'branch_detail_id',
                        type: 'select',
                        selectOptions: locations,
                        default: { value: locations[0].id }
                    },
                    { name: Lang.t('label.product_name'), columnName: 'product_name', type: 'string' },
                    { 
                        name: Lang.t('label.type'), 
                        columnName: 'warning_type', 
                        type: 'select', 
                        selectOptions: [
                            { id: WARNING_TYPE.MIN, label: Lang.t('label.min') },
                            { id: WARNING_TYPE.MAX, label: Lang.t('label.max') }
                        ],
                        limit: 1
                    },
                ],
                data: {},
            }
        },
        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'inventory', label: Lang.t('label.inventory'), default: true },
                { alias: 'reservedQty', label: Lang.t('label.reserved_qty'), default: true },
                { alias: 'requestedQty', label: Lang.t('label.requested_qty'), default: true },
                { alias: 'projectedInventory', label: Lang.t('label.projected_inventory'), default: true },
                { alias: 'min', label: Lang.t('label.min'), default: true },
                { alias: 'max', label: Lang.t('label.max'), default: true },
            ],
            selections: []
        },

        message: new Notification
    },

    mounted: function() {
        this.paginate(1);
    },

    methods: {
        paginate(page) {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.modal.filter.visible = false;
            that.processing = true;
            that.datatable.value.data = [];
            that.message.reset();

            ProductService.inventoryWarning({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.products.data;
                that.datatable.value.pagination = response.data.values.products.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.processing = false;
            })
        }
    }
});