import CoreMixins from '../../Common/CoreMixins';
import SalesmanReportService from "../../Services/SalesmanReportService";
import Notification from "../../Classes/Notification";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            transactions: {
                settings: {
                    id: 'datatable-transactions',
                    withPaging : true,
                    withPaginateCallBack: true,
                    withActionCell : false,
                    perPage: 10
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            products: {
                settings: {
                    id: 'datatable-products',
                    withPaging : true,
                    withPaginateCallBack: false,
                    withActionCell : false,
                    perPage: 10
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },

        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: {
                            value: Laravel.auth.branch
                        }
                    },
                    {
                        name: Lang.t('label.location'),
                        columnName: 'for_location',
                        type: 'select',
                        selectOptions: locations
                    },
                    {
                        name: Lang.t('label.salesman'),
                        columnName: 'salesman_id',
                        type: 'select',
                        selectOptions: salesmen
                    },
                    {
                        name: Lang.t('label.transaction_date_time'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        selected: true,
                        default: [
                            { comparison: '>=', value: Moment().startOf('day').format() },
                            { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                        ]
                    }
                ],
                data: {},
            }
        },

        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'branch_name', label: Lang.t('label.branch_name'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'sheet_number', label: Lang.t('label.sheet_number'), default: true },
                { alias: 'customer_name', label: Lang.t('label.customer'), default: true },
                { alias: 'total_amount', label: Lang.t('label.total_amount'), default: true },
            ],
            selections: []
        },

        message: new Notification
    },

    mounted: function(){
        this.search();
    },

    methods: {
        search() {
            let that = this;

            this.modal.filter.visible = false;
            this.message.reset();
            this.processing = true;

            axios.all([
                SalesmanReportService.products({
                    filters: this.modal.filter.data,
                }),
                SalesmanReportService.transactions({
                    perPage: this.datatable.transactions.settings.perPage,
                    page: 1,
                    filters: this.modal.filter.data,
                }),
            ])
            .then(axios.spread((products, transactions) => {
                that.message.info = transactions.data.message;
                that.datatable.transactions.value.data = transactions.data.values.transactions.data;
                that.datatable.transactions.value.pagination = transactions.data.values.transactions.meta.pagination;
                that.datatable.products.value.data = products.data.values.products.data;
                that.processing = false;
            }));
        },

        paginateTransactions(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.modal.filter.visible = false;
            this.message.reset();

            SalesmanReportService.transactions({
                perPage: that.datatable.transactions.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.transactions.value.data = response.data.values.transactions.data;
                that.datatable.transactions.value.pagination = response.data.values.transactions.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        }
    },

    computed: {
        totalAmount: function() {
            let total = 0;

            for(let index in this.datatable.products.value.data) {
                total += Numeric.parse(this.datatable.products.value.data[index]['total_amount']);
            }

            return Numeric.format(total);
        }
    }
});