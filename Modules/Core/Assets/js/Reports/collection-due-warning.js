import CoreMixins from '../Common/CoreMixins';
import CollectionDueWarningService from "../Services/CollectionDueWarningService";
import SalesOutboundService from "../Services/SalesOutboundService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        DateTimePicker
    },

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
        },
        references: {
            branches: branches,
            customers: customers
        },
        filter: {
            transaction:{
                date_from: Moment().startOf('day').format(),
                date_to: Moment().endOf('day').format(),
                branch: null,
                customer: null
            },
            all: false
        },

        summary: {
            noOfTransactions: '-',
            totalAmount: '-',
            totalRemaining: '-'
        },

        message: new Notification,
    },

    created() {
        this.references.customers.unshift({
            id: null,
            label: Lang.t('label.all_customers')
        },{
            id: -1,
            label: Lang.t('label.all_walk_in_customers')
        });

        this.references.branches.unshift({
            id: null,
            label: Lang.t('label.all_branches')
        });
    },

    mounted: function() {
        this.search();
    },

    methods: {
        search() {
            let that = this;

            this.message.reset();
            this.processing = true;

            axios.all([
                CollectionDueWarningService.paginate({
                    'perPage': this.datatable.settings.perPage,
                    'page': 1,
                    'filters': this.filter.transaction,
                    'all': this.filter.all
                }),
                CollectionDueWarningService.summary({
                    'filters': this.filter.transaction,
                    'all': this.filter.all
                })
            ])
            .then(axios.spread((transactions, summary) => {
                that.processing = false;
                that.summary.noOfTransactions = summary.data.values.total_count;
                that.summary.totalAmount = summary.data.values.total_amount;
                that.summary.totalRemaining = summary.data.values.total_remaining;
                that.message.info = transactions.data.message;
                that.datatable.value.data = transactions.data.values.data;
                that.datatable.value.pagination = transactions.data.values.meta.pagination;
            }));
        },

        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            CollectionDueWarningService.paginate({
                'perPage': this.datatable.settings.perPage,
                'page': page,
                'filters': this.filter.transaction,
                'all': this.filter.all
            }).then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            }).catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        goToSalesOutbound(id) {
            (new SalesOutboundService()).edit(id);
        }
    },
});