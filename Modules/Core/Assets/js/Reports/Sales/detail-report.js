import CoreMixins from '../../Common/CoreMixins';
import SalesReportService from '../../Services/SalesReportService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'sales_outbound.created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: String(Laravel.auth.branch) },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    {
                        name: Lang.t('label.location'),
                        columnName: 'sales_outbound.for_location',
                        type: 'select',
                        selectOptions: locations
                    },
                    { 
                        name: Lang.t('label.transaction_date'), 
                        columnName: 'sales_outbound.transaction_date', 
                        type:'dateRange',
                        selected: true,
                        default: [
                            {
                                value: {
                                    start: Moment().startOf('day').format(),
                                    end: Moment().add(1, 'day').startOf('day').format()
                                }
                            }
                        ],
                    },
                    { name: Lang.t('label.sheet_no'), columnName: 'sales_outbound.sheet_number', type: 'string' },
                    { name: Lang.t('label.remarks'), columnName: 'sales_outbound.remarks', type:'string' },
                    {
                        name: Lang.t('label.approval_status'),
                        columnName: 'sales_outbound.approval_status',
                        type: 'select',
                        selected: true,
                        selectOptions: [
                            { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                            { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                            { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                            { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                        ],
                        default: { value: String(APPROVAL_STATUS.APPROVED) }
                    },
                    {
                        name: Lang.t('label.deleted'),
                        columnName: 'sales_outbound.deleted_at',
                        type: 'select',
                        selected: true,
                        selectOptions: [
                            { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                            { id: String(VOIDED.NO), label: Lang.t('label.no') },
                        ],
                        default: { value: String(VOIDED.NO) }
                    },
                    {
                        name: Lang.t('label.customer_type'),
                        columnName: 'sales_outbound.customer_type',
                        type: 'select',
                        selectOptions: [
                            {id: CUSTOMER.WALK_IN, label: Lang.t('label.walk_in_customer')},
                            {id: CUSTOMER.REGULAR, label: Lang.t('label.regular_customer')}
                        ]
                    },
                    {
                        name: Lang.t('label.customer'),
                        columnName: 'sales_outbound.customer_id', 
                        type: 'select',
                        selectOptions: customers
                    },
                    {
                        name: Lang.t('label.collection_status'),
                        columnName: 'collection_status',
                        type: 'select',
                        selectOptions: [
                            {id: 0, label: Lang.t('label.incomplete')},
                            {id: 1, label: Lang.t('label.complete')}
                        ]
                    },
                    {
                        name: Lang.t('label.salesman'),
                        columnName: 'sales_outbound.salesman_id',
                        type: 'select',
                        selectOptions: users
                    },
                    {
                        name: Lang.t('label.requested_by'),
                        columnName: 'sales_outbound.requested_by',
                        type: 'select',
                        selectOptions: users
                    },
                    {
                        name: Lang.t('label.created_by'),
                        columnName: 'sales_outbound.created_by',
                        type: 'select',
                        selectOptions: users
                    },
                    { 
                        name: Lang.t('label.created_date'), 
                        columnName: 'sales_outbound.created_at', 
                        type:'dateRange',
                        inputVal: {
                            start: Moment().startOf('day').format(),
                            end: Moment().add(1, 'day').startOf('day').format()
                        }
                    },
                    {
                        name: Lang.t('label.last_modified_by'),
                        columnName: 'sales_outbound.modified_by',
                        type: 'select',
                        selectOptions: users
                    },
                    { 
                        name: Lang.t('label.last_modified_date'), 
                        columnName: 'sales_outbound.updated_at', 
                        type:'dateRange',
                        inputVal: {
                            start: Moment().startOf('day').format(),
                            end: Moment().add(1, 'day').startOf('day').format()
                        }
                    },
                    {
                        name: Lang.t('label.audit_by'),
                        columnName: 'sales_outbound.audited_by',
                        type: 'select',
                        selectOptions: users
                    },
                    { 
                        name: Lang.t('label.audit_date'), 
                        columnName: 'sales_outbound.audited_date', 
                        type:'dateRange',
                        inputVal: {
                            start: Moment().startOf('day').format(),
                            end: Moment().add(1, 'day').startOf('day').format()
                        }
                    },
                    { name: Lang.t('label.reference_no'), columnName: 'sales.sheet_number', type: 'string'},
                    { name: Lang.t('label.product_barcode'), columnName: 'product_barcode.code', type: 'string'},
                    { name: Lang.t('label.product_name'), columnName: 'product.name', type: 'string'},
                    {
                        name: Lang.t('label.product_brand'),
                        columnName: 'product.brand_id',
                        type: 'select',
                        selectOptions: brands
                    },
                    {
                        name: Lang.t('label.product_category'),
                        columnName: 'product.category_id',
                        type: 'select',
                        selectOptions: categories
                    },
                    {
                        name: Lang.t('label.product_supplier'),
                        columnName: 'product.supplier_id',
                        type: 'select',
                        selectOptions: suppliers
                    }
                ],
                data: {},
            }
        },
        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'created_from_name', label: Lang.t('label.created_from'), default: true },
                { alias: 'created_for_name', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location_name', label: Lang.t('label.for_location'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'reference_id', label: Lang.t('label.reference'), default: true },
                { alias: 'approval', label: Lang.t('label.approval_status'), default: true },
                { alias: 'customer_type', label: Lang.t('label.customer_type'), default: true },
                { alias: 'customer_name', label: Lang.t('label.customer'), default: true },
                { alias: 'total_amount', label: Lang.t('label.amount'), default: true },
                { alias: 'remaining_amount', label: Lang.t('label.remaining_amount'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'salesman_name', label: Lang.t('label.salesman'), default: true },
                { alias: 'term', label: Lang.t('label.term'), default: true },
                { alias: 'requested_by_name', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by_name', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_at', label: Lang.t('label.created_date'), default: true },
                { alias: 'audited_by_name', label: Lang.t('label.audit_by'), default: true },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true },
                { alias: 'deleted', label: Lang.t('label.deleted'), default: true },
                { alias: 'product_barcode', label: Lang.t('label.barcode'), default: true },
                { alias: 'stock_no', label: Lang.t('label.stock_no'), default: true },
                { alias: 'chinese_name', label: Lang.t('label.chinese_name'), default: true },
                { alias: 'product_name', label: Lang.t('label.product_name'), default: true },
                { alias: 'qty', label: Lang.t('label.qty'), default: true },
                { alias: 'unit_name', label: Lang.t('label.uom'), default: true },
                { alias: 'unit_qty', label: Lang.t('label.unit_specs'), default: true },
                { alias: 'total_qty', label: Lang.t('label.total_qty'), default: true },
                { alias: 'product_serial', label: Lang.t('label.serial_no'), default: true },
                { alias: 'product_batch', label: Lang.t('label.batch'), default: true },
                { alias: 'oprice', label: Lang.t('label.o_price'), default: true },
                { alias: 'price', label: Lang.t('label.price'), default: true },
                { alias: 'discount1', label: Lang.t('label.discount_1'), default: true },
                { alias: 'discount2', label: Lang.t('label.discount_2'), default: true },
                { alias: 'discount3', label: Lang.t('label.discount_3'), default: true },
                { alias: 'discount4', label: Lang.t('label.discount_4'), default: true },
                { alias: 'detail_total_amount', label: Lang.t('label.amount'), default: true },
                { alias: 'detail_remarks', label: Lang.t('label.detail_remarks'), default: true },
            ],
            selections: []
        },

        summary: {
            total_qty: '-',
            total_amount: '-',
            total_remaining_amount: '-',
        },

        message: new Notification
    },

    mounted: function(){
        this.search();
    },

    methods: {
        search() {
            let that = this;

            this.modal.filter.visible = false;
            this.processing = true;
            this.message.reset();

            axios.all([
                SalesReportService.paginate({
                    perPage: that.datatable.settings.perPage,
                    page: 1,
                    filters: that.modal.filter.data
                }),
                SalesReportService.summary({
                    filters: that.modal.filter.data
                })
            ])
            .then(axios.spread((response, summary) => {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.summary = summary.data.values;
                that.processing = false;
            }));
        },

        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.visible = false;
            this.processing = true;
            this.message.reset();

            SalesReportService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        excel() {
            SalesReportService.export({
                filters: this.modal.filter.data,
                columns: this.columnSettings.selections
            });
        }
    }
});