import CoreMixins from '../../Common/CoreMixins';
import MemberReportService from '../../Services/MemberReportService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.member_rate'),
                        columnName: 'rate_head_id',
                        type: 'chosen',
                        selectOptions: rates
                    },
                    { name: Lang.t('label.full_name'), columnName: 'full_name', type:'string' }
                ],
                data: []
            }
        },
        message: new Notification
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            if (that.processing) {
                return false;
            }

            this.modal.filter.visible = false;
            this.message.reset();
            this.processing = true;

            MemberReportService.paginate({
                perPage: this.datatable.settings.perPage,
                page: page,
                filters: this.modal.filter.data
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.members.data;
                that.datatable.value.pagination = response.data.values.members.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        excel() {
            MemberReportService.export({
                filters: this.modal.filter.data
            });
        }
    }
});