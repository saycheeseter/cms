import CoreMixins from '../../Common/CoreMixins';
import ReportMixins from '../Builder/ReportMixins';
import ReportBuilderService from '../../Services/ReportBuilderService';
import UDFService from '../../Services/UDFService';
import Sortable from 'sortablejs';

Vue.directive('sortable', {
    inserted: function (el, binding) {
        var sortable = new Sortable(el, binding.value || {});
    }
});

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ReportMixins],

    data: {
        references: {
            module: [],
            fields: []
        },

        module: {
            selections: {
                columnArrangement: [],
                grouping: [],
                order: [],
                total: []
            },
            name: '',
            groupId: null,
            id: id,
            excel: {
                headerStyle : {
                    bold: false,
                    textAlign: 'left'
                }
            }
        },

        excel: {
            id: 'excel-settings',
            settings: {
                withPaging : false,
                withActionCell : false
            },
            value: {
                data: [],
                pagination: {}
            },
            visible: false
        },

        sections: {
            columnArrangement: "columnArrangement",
            grouping: "grouping",
            order: "order",
            total: "total"
        },

        type: COLUMN_TYPE,
        service: ReportBuilderService,
        message: new Notification,
        dialog: {
            message: ''
        },

        defaults: {
            sections: {
                fields: {
                    visibility: true,
                    checkbox: {
                        columnArrangement: 0,
                        grouping: 0,
                        order: 0,
                        total: 0,
                    }
                },
                columnArrangement: {
                    excel: {
                        width: '40',
                        textAlign: 'left',
                        type: COLUMN_TYPE.TEXT,
                    }
                },
                order_status: 'ASC',
            }
        },

        search: ''
    },

    mounted: function() {
        this.setDefaults();
        this.loadModules();
        this.load();
    },

    methods: {
        //FOR SORTABLE
        sortColumnArrangement ({oldIndex, newIndex}) {
            const movedItem = this.module.selections.columnArrangement.splice(oldIndex, 1)[0];
            this.module.selections.columnArrangement.splice(newIndex, 0, movedItem);
        },

        sortGrouping ({oldIndex, newIndex}) {
            const movedItem = this.module.selections.grouping.splice(oldIndex, 1)[0];
            this.module.selections.grouping.splice(newIndex, 0, movedItem);
        },

        sortOrder ({oldIndex, newIndex}) {
            const movedItem = this.module.selections.order.splice(oldIndex, 1)[0];
            this.module.selections.order.splice(newIndex, 0, movedItem);
        },

        //MODULE SELECT
        loadModules(){
            for(let module in this.modules) {
                this.references.module.push({
                    id: this.modules[module].id,
                    label: this.modules[module].label
                });
            }
        },

        changeModule() {
            this.setDefaults();
            this.loadUDFByModule();
            this.clearsections();
        },

        setDefaults() {
            for (let module in this.modules) {
                for(let column in this.modules[module].columns){
                    this.modules[module].columns[column] = Object.assign({
                        key: column,
                        header_name: this.parseKeyAsReadable(column),
                        formula: (this.modules[module].columns[column].formula === undefined) ? null : this.modules[module].columns[column].formula,
                        column_name: this.extractColumnName(module, column),
                        table_alias: this.extractTableAlias(this.extractColumnName(module, column), column),
                    }, this.defaultFieldValues());
                }

                for(let reference in this.modules[module].references) {
                    for(let reference_column in this.references.columns[this.modules[module].references[reference]]) {
                        this.modules[module].columns[reference + '_' + reference_column] = Object.assign({
                            key: reference + '_' + reference_column,
                            column_name: reference_column,
                            table_alias: reference,
                            formula: (this.references.columns[this.modules[module].references[reference]][reference_column].formula === undefined) ? null : this.references.columns[this.modules[module].references[reference]][reference_column].formula,
                            header_name: this.parseKeyAsReadable(reference + '_' + reference_column)
                        }, this.defaultFieldValues());
                    }
                }
            }
        },

        extractColumnName(module, column) {
            return  column.replace(module + '_','');
        },

        extractTableAlias(columnName, column) {
            return column.replace('_' + columnName, '');
        },

        displayFields() {
            this.references.fields = this.modules[MODULE_ALIAS[this.module.groupId]].columns;
            this.references.fields = Object.assign(this.references.fields, this.defaults.columns);
        },

        clearsections() {
            for(let key in this.sections) {
                this.module.selections[this.sections[key]] = [];
            }
        },

        loadUDFByModule() {
            let that = this;

            return UDFService.byModule(this.module.groupId)
                .then(function(response) {
                    let module = MODULE_ALIAS[that.module.groupId];

                    for(let index in response.data.values.udfs) {
                        let udf = response.data.values.udfs[index];

                        if(udf.visible) {
                            Vue.set(that.modules[module].columns, module + '_' + udf.alias, Object.assign({
                                    key: module + '_' + udf.alias,
                                    header_name: that.parseKeyAsReadable(module + '_' + udf.label)
                                }, that.defaultFieldValues())
                            );
                        }
                    }

                    that.displayFields();
                })
                .catch(function(error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        //BACKEND METHODS
        save() {
            if ((this.module.id === 'create' && ! permissions.create)
                || (this.module.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            if(this.module.id == 'create') {
                this.store();
            } else {
                this.update();
            }
        },

        store() {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            this.service.store(this.data)
                .then(function (response){
                    that.dialog.message = response.data.message;
                    that.module.id = response.data.values.template.id;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            this.service.update(this.data, this.module.id)
                .then(function (response){
                    that.dialog.message = response.data.message;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        //SECTION METHODS
        toggleColumnArrangementSection(field, status, index) {
            let section = this.sections.columnArrangement;

            if(status) {
                this.module.selections[section].push(Object.assign({
                    key: field.key,
                    header_name: field.header_name,
                    formula: (field.formula === undefined) ? null : field.formula,
                    column_name: field.column_name,
                    table_alias: field.table_alias,
                }, this.defaultColumnArrangementValues()));
            } else {
                this.uncheckSectionEvent(field, section, index);
            }
        },

        toggleGroupingSection(field, status, index) {
            let section = this.sections.grouping;

            if(status) {
                if(this.isSectionContentMaxed(section)) {
                    this.message.error.set([Lang.t('validation.max_group_by')]);
                    this.references.fields[index].checkbox[section] = 0;
                    return false;
                } else {
                    this.module.selections[section].push({
                        key: field.key,
                        header_name: field.header_name
                    });

                    if(!this.references.fields[index].checkbox[this.sections.columnArrangement]) {
                        this.references.fields[index].checkbox[this.sections.columnArrangement] = 1;
                        this.toggleColumnArrangementSection(field, 1, index);
                    }
                }
            } else {
                this.uncheckSectionEvent(field, section, index);
            }
        },

        toggleOrderSection(field, status, index) {
            let section = this.sections.order;

            if(status) {
                if(this.isSectionContentMaxed(section)) {
                    this.message.error.set([Lang.t('validation.max_order_by')]);
                    this.references.fields[index].checkbox[section] = 0;
                    return false;
                } else {
                    this.module.selections[section].push({
                        key: field.key,
                        header_name: field.header_name,
                        order_status: (field.order_status == undefined) ? this.defaults.sections.order_status : field.order_status 
                    });

                    if(!this.references.fields[index].checkbox[this.sections.columnArrangement]) {
                        this.references.fields[index].checkbox[this.sections.columnArrangement] = 1;
                        this.toggleColumnArrangementSection(field, 1, index);
                    }
                }
            } else {
                this.uncheckSectionEvent(field, section, index);
            }
        },

        toggleTotalSection(field, status, index) {
            let section = this.sections.total;

            if(status) {
                if(this.isSectionContentMaxed(section)) {
                    this.message.error.set([Lang.t('validation.max_group_by')]);
                    this.references.fields[index].checkbox[section] = 0;
                    return false;
                } else {
                    this.module.selections[section].push({
                        key: field.key,
                        header_name: field.header_name
                    });

                    if(!this.references.fields[index].checkbox[this.sections.columnArrangement]) {
                        this.references.fields[index].checkbox[this.sections.columnArrangement] = 1;
                        this.toggleColumnArrangementSection(field, 1, index);
                    }
                }
            } else {
                this.uncheckSectionEvent(field, section, index);
            }
        },

        uncheckSectionEvent(field, section, index) {
            if(section == this.sections.columnArrangement){
                for(let key in this.sections) {
                    this.removeInSection(field.key, this.sections[key]);
                    this.removeCheckInFields(index, this.sections[key]);
                }
            } else {
                this.removeInSection(field.key, section);
            }
        },

        //LOAD METHODS
        load() {
            if(this.module.id == 'create' || this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            this.service.show(this.module.id)
                .then(function (response){
                    that.parseTemplate(response.data.values.template);
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        parseTemplate(data) {
            //module
            this.module.name = data.name;
            this.module.groupId = data.group_id;
            this.module.excel.headerStyle = data.format.excel.header_style;

            let that = this;
            
            axios.all([this.loadUDFByModule()]).then(function() {
                // column arrangement
                that.parseFields(data.format.columns);
                
                // column arrangement
                that.parseFormulas(data.format.formula);

                // grouping
                that.parseGrouping(data.format.group_by);
                
                // order by
                that.parseOrderBy(data.format.order_by);

                // excel
                that.parseExcel(data.format.excel.columns);

                //total
                that.parseTotal(data.format.total);
            });
        },

        parseFields(data) {
            for(let key in data) {
                let fieldIndex = this.findByKeyInFields(key);
                let fieldData = Object.assign({
                    key: key
                }, data[key]);

                this.references.fields[fieldIndex].checkbox[this.sections.columnArrangement] = 1;

                this.toggleColumnArrangementSection(fieldData, 1, fieldIndex);
            }
        },

        parseFormulas(data) {
            for(let key in data) {
                let fieldIndex = this.findByKeyInFields(key);
                let fieldData = Object.assign({
                    key: key
                }, data[key]);

                this.references.fields[fieldIndex].checkbox[this.sections.columnArrangement] = 1;

                this.toggleColumnFormulaSection(fieldData, 1, fieldIndex);
            }
        },

        parseGrouping(data) {
            for(let index in data) {
                let key = data[index];
                let fieldIndex = this.findByKeyInFields(key);
                let fieldData = {
                    key: key,
                    header_name: this.parseKeyAsReadable(key)
                };

                this.references.fields[fieldIndex].checkbox[this.sections.grouping] = 1;

                this.toggleGroupingSection(fieldData, 1, fieldIndex);
            }
        },

        parseOrderBy(data) {
            for(let index in data) {
                let key = data[index].key;
                let fieldIndex = this.findByKeyInFields(key);
                let fieldData = Object.assign({
                    key: key,
                    order_status: data[index].order,
                    header_name: this.parseKeyAsReadable(key)
                });

                this.references.fields[fieldIndex].checkbox[this.sections.order] = 1;

                this.toggleOrderSection(fieldData, 1, fieldIndex);
            }
        },

        parseExcel(data) {
            let index = 0;

            for(let key in data) {
                this.module.selections.columnArrangement[index]['excel'] = data[key];
                index++;
            }
        },

        parseTotal(data) {
            for(let index in data) {
                let key = data[index];
                let fieldIndex = this.findByKeyInFields(key);
                let fieldData = {
                    key: key,
                    header_name: this.parseKeyAsReadable(key)
                };

                this.references.fields[fieldIndex].checkbox[this.sections.total] = 1;

                this.toggleTotalSection(fieldData, 1, fieldIndex);
            }
        },

        //HELPER METHODS
        removeInSection(key, section) {
            for(let index in this.module.selections[section]) {
                if(this.module.selections[section][index].key == key) {
                    this.module.selections[section].splice(index, 1);
                }
            }
        },

        removeCheckInFields(index, pane) {
            this.references.fields[index].checkbox[pane] = 0;
        },

        parseKeyAsReadable(column) {
            return column.replace(/_/g, " ").replace(/\b\w/g, function(txt) {
                return String(txt).toUpperCase();
            });
        },

        toggleOrder(value, index) {
            value == 'ASC' 
                ? this.module.selections.order[index].order_status = 'DESC'
                : this.module.selections.order[index].order_status = 'ASC';
        },

        findByKeyInFields(key) {
            let index;

            for(let field in this.references.fields) {
                if(this.references.fields[field].key == key) {
                    index = field;
                }
            }

            return index;
        },

        redirect() {
            if(this.module.id != 0) {
                let that = this;

                window.location = "/reports/builder/" + this.module.id + "/edit";
            }
        },

        isSectionContentMaxed(targetPanel) {
            return this.module.selections[targetPanel].length >= 10;
        },

        getFields() {
            let fields = {};

            for(let index in this.excel.value.data) {
                let row = this.excel.value.data[index];
                fields[row.key] = {
                    header_name: row.header_name,
                    formula: row.formula,
                    column_name: row.column_name,
                    table_alias: row.table_alias,
                }
            }

            return fields;
        },

        getGrouping() {
            let grouping = [];

            for(let index in this.module.selections.grouping) {
                grouping.push(this.module.selections.grouping[index].key);
            }

            return grouping;
        },

        getOrder() {
            let order = [];

            for(let index in this.module.selections.order) {
                order.push({
                    key: this.module.selections.order[index].key,
                    order: this.module.selections.order[index].order_status
                });
            }

            return order;
        },

        getTotal() {
            let total = [];

            for(let index in this.module.selections.total) {
                total.push(this.module.selections.total[index].key);
            }

            return total;
        },


        getExcel() {
            let fields = {};

            for(let index in this.excel.value.data) {
                let row = this.excel.value.data[index];
                fields[row.key] = {
                    header_name: row.header_name,
                    width: row.excel.width,
                    textAlign: row.excel.textAlign,
                    type: row.excel.type
                }
            }

            return fields;
        },

        defaultFieldValues() {
            return {
                visibility: true,
                checkbox: {
                    columnArrangement: 0,
                    grouping: 0,
                    order: 0,
                }
            };
        },

        defaultColumnArrangementValues() {
            return {
                excel: {
                    width: '40',
                    textAlign: 'left',
                    type: COLUMN_TYPE.TEXT,
                }
            };
        },

        defaultOrderValues() {
            return {
                order_status: false
            };
        }
    },

    watch: {
        'module.selections.columnArrangement': function() {
            this.excel.value.data = this.module.selections.columnArrangement;
        },
        'search': function() {
            this.search = String(this.search).replace(/[^a-zA-Z0-9\s]/g, '');
            let regexString = new RegExp(this.search, "i");
            for(let index in this.references.fields) {
                let header = this.references.fields[index].header_name;
                this.references.fields[index].visibility = String(header).match(regexString);
            }
        },
        'module.name': function() {
            this.module.name = String(this.module.name).replace(/[^a-zA-Z0-9\s]/g, '');
        }
    },

    computed: {
        data: function() {
            return {
                name: this.module.name,
                group_id: this.module.groupId,
                format: {
                    columns: this.getFields(),
                    order_by: this.getOrder(),
                    group_by: this.getGrouping(),
                    total: this.getTotal(),
                    excel: {
                        header_style: {
                            bold: this.module.excel.headerStyle.bold,
                            textAlign: this.module.excel.headerStyle.textAlign
                        },
                        columns: this.getExcel()
                    }
                }
            }
        }
    }
});