import CoreMixins from '../../Common/CoreMixins';
import ReportMixins from '../Builder/ReportMixins';
import ReportBuilderService from '../../Services/ReportBuilderService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ReportMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string' },
                    {
                        name: Lang.t('label.module_group'),
                        columnName: 'group_id',
                        type: 'select',
                        selectOptions: []
                    },
                ],
                data: []
            }
        },
        message: new Notification,
        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),
        service: ReportBuilderService,
    },

    created: function () {
        this.loadModuleOptions();
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        create() {
            this.service.create();
        },

        view(id) {
            if(!permissions.view_detail) {
                return false;
            }

            this.service.edit(id);
        },

        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            this.service.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.templates;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            this.service.destroy(that.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.visible = false;
                })
        },

        confirmation(data, index) {
            this.confirm.visible = true
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        loadModuleOptions() {
            for(let module in this.modules) {
                this.modal.filter.options[1].selectOptions.push({
                    id: this.modules[module].id,
                    label: this.modules[module].label
                });
            }
        },
    },

    watch: {
        'confirm.visible': function(visible) {
            if (!visible) {
                this.confirm.reset();
            }
        },
    },
});