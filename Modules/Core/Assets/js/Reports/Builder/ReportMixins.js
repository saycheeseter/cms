export default {
    data: {
        modules: {
            // product: {
            //     columns: {
            //
            //     },
            //     references: {
            //         product_unit_uom: 'system_code',
            //         product_barcode_uom: 'system_code',
            //         product_branch_detail_location: 'location',
            //         product_branch_info_branch: 'branch',
            //         product_branch_price_branch: 'branch',
            //
            //         product: 'product',
            //         product_category: 'system_code',
            //         product_brand: 'system_code',
            //         product_supplier: 'supplier',
            //         product_suppliers: 'suppliers',
            //         product_branch_price: 'branch_price',
            //         product_branch_summary: 'product_branch_summary',
            //     },
            //     id: MODULE.PRODUCT,
            //     label: Lang.t('label.product_information')
            // },
            purchase: {
                columns: {
                    purchase_term: {},
                    purchase_audited_date: {},
                    purchase_transaction_date: {},
                    purchase_deliver_until: {},
                    purchase_approval_status: {},
                    purchase_transaction_status: {},
                    purchase_updated_at: {},
                    purchase_sheet_number: {},
                    purchase_remarks: {},
                    purchase_total_amount : {},
                    purchase_detail_received_qty: {
                        formula: {
                            computation: '((purchase_detail.qty * purchase_detail.unit_qty) - purchase_detail.remaining_qty)',
                            columns: 'purchase_detail.qty,purchase_detail.unit_qty,purchase_detail.remaining_qty'
                        }
                    },
                    purchase_detail_total_qty: {
                        formula: {
                            computation: 'purchase_detail.qty * purchase_detail.unit_qty',
                            columns: 'purchase_detail.qty,purchase_detail.unit_qty'
                        }
                    },
                    purchase_detail_amount: {
                        formula: {
                            computation: '(purchase_detail.price * (purchase_detail.qty * purchase_detail.unit_qty ))',
                            columns: 'purchase_detail.price,purchase_detail.qty,purchase_detail.unit_qty'
                        }
                    },
                },
                references: {
                    purchase_detail: 'invoice_detail',
                    purchase_supplier: 'supplier',
                    purchase_payment_method: 'system_code',
                    purchase_created_for: 'branch',
                    purchase_deliver_to: 'branch',
                    purchase_for_location: 'location',
                    purchase_requested_by: 'user',
                    purchase_audited_by: 'user',
                    purchase_modified_by: 'user',
                    purchase_detail_unit: 'system_code',

                    purchase_detail_product: 'product',
                    purchase_detail_product_category: 'system_code',
                    purchase_detail_product_brand: 'system_code',
                    purchase_detail_product_supplier: 'supplier',
                    purchase_detail_product_suppliers: 'suppliers',
                    purchase_detail_product_branch_price: 'branch_price',
                    purchase_detail_product_branch_summary: 'product_branch_summary',
                    purchase_detail_barcode: 'barcode',
                },
                id: MODULE.PURCHASE,
                label: Lang.t('label.purchase_order')
            },
            purchase_inbound: {
                columns: {
                    purchase_inbound_term: {},
                    purchase_inbound_dr_no: {},
                    purchase_inbound_supplier_invoice_no: {},
                    purchase_inbound_supplier_box_count: {},
                    purchase_inbound_audited_date: {},
                    purchase_inbound_updated_at: {},
                    purchase_inbound_transaction_date: {},
                    purchase_inbound_transaction_type: {},
                    purchase_inbound_approval_status: {},
                    purchase_inbound_sheet_number: {},
                    purchase_inbound_remarks : {},
                    purchase_inbound_total_amount : {},
                    purchase_inbound_detail_total_qty: {
                        formula: {
                            computation: 'purchase_inbound_detail.qty * purchase_inbound_detail.unit_qty',
                            columns: 'purchase_inbound_detail.qty,purchase_inbound_detail.unit_qty'
                        }
                    },
                    purchase_inbound_detail_amount: {
                        formula: {
                            computation: '(purchase_inbound_detail.price * (purchase_inbound_detail.qty * purchase_inbound_detail.unit_qty ))',
                            columns: 'purchase_inbound_detail.price,purchase_inbound_detail.qty,purchase_inbound_detail.unit_qty'
                        }
                    },
                },
                references: {
                    purchase_inbound_detail: 'inventory_chain_detail',
                    purchase_inbound_for_location: 'location',
                    purchase_inbound_requested_by: 'user',
                    purchase_inbound_supplier: 'supplier',
                    purchase_inbound_payment_method: 'system_code',
                    purchase_inbound_created_for: 'branch',
                    purchase_inbound_audited_by: 'user',
                    purchase_inbound_detail_unit: 'system_code',
                    purchase_inbound_modified_by: 'user',

                    purchase_inbound_detail_product: 'product',
                    purchase_inbound_detail_product_category: 'system_code',
                    purchase_inbound_detail_product_brand: 'system_code',
                    purchase_inbound_detail_product_supplier: 'supplier',
                    purchase_inbound_detail_product_suppliers: 'suppliers',
                    purchase_inbound_detail_product_branch_price: 'branch_price',
                    purchase_inbound_detail_product_branch_summary: 'product_branch_summary',
                    purchase_inbound_detail_barcode: 'barcode',
                },
                id: MODULE.PURCHASE_INBOUND,
                label: Lang.t('label.purchase_inbound')
            },
            sales: {
                columns: {
                    sales_transaction_status: {},
                    sales_audited_date: {},
                    sales_updated_at: {},
                    sales_transaction_date: {},
                    sales_approval_status: {},
                    sales_sheet_number: {},
                    sales_remarks: {},
                    sales_customer_type: {},
                    sales_customer_walk_in_name: {},
                    sales_term: {},
                    sales_total_amount : {},
                    sales_detail_received_qty: {
                        formula: {
                            computation: '((sales_detail.qty * sales_detail.unit_qty) - sales_detail.remaining_qty)',
                            columns: 'sales_detail.qty,sales_detail.unit_qty,sales_detail.remaining_qty'
                        }
                    },
                    sales_detail_total_qty: {
                        formula: {
                            computation: 'sales_detail.qty * sales_detail.unit_qty',
                            columns: 'sales_detail.qty,sales_detail.unit_qty'
                        }
                    },
                    sales_detail_amount: {
                        formula: {
                            computation: '(sales_detail.price * (sales_detail.qty * sales_detail.unit_qty ))',
                            columns: 'sales_detail.price,sales_detail.qty,sales_detail.unit_qty'
                        }
                    },
                },
                references: {
                    sales_detail: 'invoice_detail',

                    sales_created_for: 'branch',
                    sales_for_location: 'location',
                    sales_requested_by: 'user',
                    sales_modified_by: 'user',
                    sales_audited_by: 'user',
                    sales_customer_regular: 'customer',
                    sales_salesman: 'user',
                    sales_detail_unit: 'system_code',

                    sales_detail_product: 'product',
                    sales_detail_product_category: 'system_code',
                    sales_detail_product_brand: 'system_code',
                    sales_detail_product_supplier: 'supplier',
                    sales_detail_product_suppliers: 'suppliers',
                    sales_detail_product_branch_price: 'branch_price',
                    sales_detail_product_branch_summary: 'product_branch_summary',
                    sales_detail_barcode: 'barcode',
                },
                id: MODULE.SALES,
                label: Lang.t('label.sales')
            },
            sales_outbound: {
                columns: {
                    sales_outbound_reference_sheet_number: {},
                    sales_outbound_audited_date: {},
                    sales_outbound_transaction_date: {},
                    sales_outbound_transaction_type: {},
                    sales_outbound_approval_status: {},
                    sales_outbound_sheet_number: {},
                    sales_outbound_remarks: {},
                    sales_outbound_customer_type: {},
                    sales_outbound_customer_walk_in_name: {},
                    sales_outbound_term: {},
                    sales_outbound_total_amount : {},
                    sales_outbound_detail_total_qty: {
                        formula: {
                            computation: 'sales_outbound_detail.qty * sales_outbound_detail.unit_qty',
                            columns: 'sales_outbound_detail.qty,sales_outbound_detail.unit_qty'
                        }
                    },
                    sales_outbound_detail_amount: {
                        formula: {
                            computation: '(sales_outbound_detail.price * (sales_outbound_detail.qty * sales_outbound_detail.unit_qty ))',
                            columns: 'sales_outbound_detail.price,sales_outbound_detail.qty,sales_outbound_detail.unit_qty'
                        }
                    },
                },
                references: {
                    sales_outbound_detail: 'inventory_chain_detail',
                    sales_outbound_for_location: 'location',
                    sales_outbound_requested_by: 'user',
                    sales_outbound_audited_by: 'user',
                    sales_outbound_customer: 'customer',
                    sales_outbound_salesman: 'user',
                    sales_outbound_detail_unit: 'system_code',

                    sales_outbound_detail_product: 'product',
                    sales_outbound_detail_product_category: 'system_code',
                    sales_outbound_detail_product_brand: 'system_code',
                    sales_outbound_detail_product_supplier: 'supplier',
                    sales_outbound_detail_product_suppliers: 'suppliers',
                    sales_outbound_detail_product_branch_price: 'branch_price',
                    sales_outbound_detail_product_branch_summary: 'product_branch_summary',
                    sales_outbound_detail_barcode: 'barcode',
                },
                id: MODULE.SALES_OUTBOUND,
                label: Lang.t('label.sales_outbound')
            },
            sales_return: {
                columns: {
                    sales_return_transaction_status: {},
                    sales_return_audited_date: {},
                    sales_return_transaction_date: {},
                    sales_return_approval_status: {},
                    sales_return_sheet_number: {},
                    sales_return_remarks: {},
                    sales_return_customer_type: {},
                    sales_return_customer_walk_in_name: {},
                    sales_return_term: {},
                    sales_return_total_amount : {},
                    sales_return_detail_received_qty: {
                        formula: {
                            computation: '((sales_return_detail.qty * sales_return_detail.unit_qty) - sales_return_detail.remaining_qty)',
                            columns: 'sales_return_detail.qty,sales_return_detail.unit_qty,sales_return_detail.remaining_qty'
                        }
                    },
                    sales_return_detail_total_qty: {
                        formula: {
                            computation: 'sales_return_detail.qty * sales_return_detail.unit_qty',
                            columns: 'sales_return_detail.qty,sales_return_detail.unit_qty'
                        }
                    },
                    sales_return_detail_amount: {
                        formula: {
                            computation: '(sales_return_detail.price * (sales_return_detail.qty * sales_return_detail.unit_qty ))',
                            columns: 'sales_return_detail.price,sales_return_detail.qty,sales_return_detail.unit_qty'
                        }
                    },
                },
                references: {
                    sales_return_detail: 'invoice_detail',
                    sales_return_for_location: 'location',
                    sales_return_requested_by: 'user',
                    sales_return_audited_by: 'user',
                    sales_return_customer_regular: 'customer',
                    sales_return_salesman: 'user',
                    sales_return_detail_unit: 'system_code',

                    sales_return_detail_product: 'product',
                    sales_return_detail_product_category: 'system_code',
                    sales_return_detail_product_brand: 'system_code',
                    sales_return_detail_product_supplier: 'supplier',
                    sales_return_detail_product_suppliers: 'suppliers',
                    sales_return_detail_product_branch_price: 'branch_price',
                    sales_return_detail_product_branch_summary: 'product_branch_summary',
                    sales_return_detail_barcode: 'barcode',
                },
                id: MODULE.SALES_RETURN,
                label: Lang.t('label.sales_return')
            },
            stock_request: {
                columns: {
                    stock_request_sheet_number: {},
                    stock_request_remarks: {},
                    stock_request_transaction_date: {},
                    stock_request_audited_date: {},
                    stock_request_approval_status: {},
                    stock_request_transaction_status: {},
                    stock_request_total_amount : {},
                    stock_request_detail_received_qty: {
                        formula: {
                            computation: '((stock_request_detail.qty * stock_request_detail.unit_qty) - stock_request_detail.remaining_qty)',
                            columns: 'stock_request_detail.qty,stock_request_detail.unit_qty,stock_request_detail.remaining_qty'
                        }
                    },
                    stock_request_detail_total_qty: {
                        formula: {
                            computation: 'stock_request_detail.qty * stock_request_detail.unit_qty',
                            columns: 'stock_request_detail.qty,stock_request_detail.unit_qty'
                        }
                    },
                    stock_request_detail_amount: {
                        formula: {
                            computation: '(stock_request_detail.price * (stock_request_detail.qty * stock_request_detail.unit_qty ))',
                            columns: 'stock_request_detail.price,stock_request_detail.qty,stock_request_detail.unit_qty'
                        }
                    },
                },
                references: {
                    stock_request_detail: 'invoice_detail',

                    stock_request_requested_by: 'user',
                    stock_request_audited_by: 'user',
                    stock_request_created_for: 'branch',
                    stock_request_for_location: 'location',
                    stock_request_request_to: 'branch',
                    stock_request_detail_unit: 'system_code',
                    stock_request_detail_product: 'product',

                    stock_request_detail_product: 'product',
                    stock_request_detail_product_category: 'system_code',
                    stock_request_detail_product_brand: 'system_code',
                    stock_request_detail_product_supplier: 'supplier',
                    stock_request_detail_product_suppliers: 'suppliers',
                    stock_request_detail_product_branch_price: 'branch_price',
                    stock_request_detail_product_branch_summary: 'product_branch_summary',
                    stock_request_detail_barcode: 'barcode',
                },
                id: MODULE.STOCK_REQUEST,
                label: Lang.t('label.stock_request')
            },
            stock_delivery: {
                columns: {
                    stock_delivery_reference_sheet_number: {},
                    stock_delivery_sheet_number: {},
                    stock_delivery_remarks: {},
                    stock_delivery_transaction_date: {},
                    stock_delivery_updated_at: {},
                    stock_delivery_audited_date: {},
                    stock_delivery_approval_status: {},
                    stock_delivery_transaction_status: {},
                    stock_delivery_transaction_type: {},
                    stock_delivery_total_amount : {},
                    stock_delivery_detail_received_qty: {
                        formula: {
                            computation: '((stock_delivery_detail.qty * stock_delivery_detail.unit_qty) - stock_delivery_detail.remaining_qty)',
                            columns: 'stock_delivery_detail.qty,stock_delivery_detail.unit_qty,stock_delivery_detail.remaining_qty'
                        }
                    },
                    stock_delivery_detail_total_qty: {
                        formula: {
                            computation: 'stock_delivery_detail.qty * stock_delivery_detail.unit_qty',
                            columns: 'stock_delivery_detail.qty,stock_delivery_detail.unit_qty'
                        }
                    },
                    stock_delivery_detail_amount: {
                        formula: {
                            computation: '(stock_delivery_detail.price * (stock_delivery_detail.qty * stock_delivery_detail.unit_qty ))',
                            columns: 'stock_delivery_detail.price,stock_delivery_detail.qty,stock_delivery_detail.unit_qty'
                        }
                    },
                },
                references: {
                    stock_delivery_detail: 'invoice_detail',

                    stock_delivery_requested_by: 'user',
                    stock_delivery_audited_by: 'user',
                    stock_delivery_modified_by: 'user',
                    stock_delivery_created_for: 'branch',
                    stock_delivery_for_location: 'location',
                    stock_delivery_deliver_to: 'branch',
                    stock_delivery_deliver_to_location: 'location',
                    stock_delivery_detail_unit: 'system_code',
                    
                    stock_delivery_detail_product: 'product',
                    stock_delivery_detail_product_category: 'system_code',
                    stock_delivery_detail_product_brand: 'system_code',
                    stock_delivery_detail_product_supplier: 'supplier',
                    stock_delivery_detail_product_suppliers: 'suppliers',
                    stock_delivery_detail_product_branch_price: 'branch_price',
                    stock_delivery_detail_product_branch_summary: 'product_branch_summary',
                    stock_delivery_detail_barcode: 'barcode',
                },
                id: MODULE.STOCK_DELIVERY,
                label: Lang.t('label.stock_delivery')
            },
            stock_delivery_outbound: {
                columns: {
                    stock_delivery_outbound_reference_sheet_number: {},
                    stock_delivery_outbound_audited_date: {},
                    stock_delivery_outbound_transaction_date: {},
                    stock_delivery_outbound_transaction_type: {},
                    stock_delivery_outbound_approval_status: {},
                    stock_delivery_outbound_transaction_status: {},
                    stock_delivery_outbound_sheet_number: {},
                    stock_delivery_outbound_remarks: {},
                    stock_delivery_outbound_total_amount : {},
                    stock_delivery_outbound_detail_total_qty: {
                        formula: {
                            computation: 'stock_delivery_outbound_detail.qty * stock_delivery_outbound_detail.unit_qty',
                            columns: 'stock_delivery_outbound_detail.qty,stock_delivery_outbound_detail.unit_qty'
                        }
                    },
                    stock_delivery_outbound_detail_amount: {
                        formula: {
                            computation: '(stock_delivery_outbound_detail.price * (stock_delivery_outbound_detail.qty * stock_delivery_outbound_detail.unit_qty ))',
                            columns: 'stock_delivery_outbound_detail.price,stock_delivery_outbound_detail.qty,stock_delivery_outbound_detail.unit_qty'
                        }
                    },
                },
                references: {
                    stock_delivery_outbound_detail: 'inventory_chain_detail',

                    stock_delivery_outbound_detail_unit: 'system_code',
                    stock_delivery_outbound_created_for: 'branch',
                    stock_delivery_outbound_for_location: 'location',
                    stock_delivery_outbound_deliver_to_location: 'location',
                    stock_delivery_outbound_deliver_to: 'branch',
                    stock_delivery_outbound_requested_by: 'user',
                    stock_delivery_outbound_audited_by: 'user',
                    stock_delivery_outbound_detail_unit: 'system_code',

                    stock_delivery_outbound_detail_product: 'product',
                    stock_delivery_outbound_detail_product_category: 'system_code',
                    stock_delivery_outbound_detail_product_brand: 'system_code',
                    stock_delivery_outbound_detail_product_supplier: 'supplier',
                    stock_delivery_outbound_detail_product_suppliers: 'suppliers',
                    stock_delivery_outbound_detail_product_branch_price: 'branch_price',
                    stock_delivery_outbound_detail_product_branch_summary: 'product_branch_summary',
                    stock_delivery_outbound_detail_barcode: 'barcode',
                },
                id: MODULE.STOCK_DELIVERY_OUTBOUND,
                label: Lang.t('label.stock_delivery_outbound')
            },
            stock_delivery_inbound: {
                columns: {
                    stock_delivery_inbound_reference_sheet_number: {},
                    stock_delivery_inbound_audited_date: {},
                    stock_delivery_inbound_transaction_date: {},
                    stock_delivery_inbound_transaction_type: {},
                    stock_delivery_inbound_approval_status: {},
                    stock_delivery_inbound_sheet_number: {},
                    stock_delivery_inbound_remarks: {},
                    stock_delivery_inbound_total_amount : {},
                    stock_delivery_inbound_detail_total_qty: {
                        formula: {
                            computation: 'stock_delivery_inbound_detail.qty * stock_delivery_inbound_detail.unit_qty',
                            columns: 'stock_delivery_inbound_detail.qty,stock_delivery_inbound_detail.unit_qty'
                        }
                    },
                    stock_delivery_inbound_detail_amount: {
                        formula: {
                            computation: '(stock_delivery_inbound_detail.price * (stock_delivery_inbound_detail.qty * stock_delivery_inbound_detail.unit_qty ))',
                            columns: 'stock_delivery_inbound_detail.price,stock_delivery_inbound_detail.qty,stock_delivery_inbound_detail.unit_qty'
                        }
                    },
                },
                references: {
                    stock_delivery_inbound_detail: 'inventory_chain_detail',

                    stock_delivery_inbound_created_for: 'branch',
                    stock_delivery_inbound_for_location: 'location',
                    stock_delivery_inbound_delivery_from_location: 'location',
                    stock_delivery_inbound_delivery_from: 'branch',
                    stock_delivery_inbound_requested_by: 'user',
                    stock_delivery_inbound_audited_by: 'user',
                    stock_delivery_inbound_detail_unit: 'system_code',

                    stock_delivery_inbound_detail_product: 'product',
                    stock_delivery_inbound_detail_product_category: 'system_code',
                    stock_delivery_inbound_detail_product_brand: 'system_code',
                    stock_delivery_inbound_detail_product_supplier: 'supplier',
                    stock_delivery_inbound_detail_product_suppliers: 'suppliers',
                    stock_delivery_inbound_detail_product_branch_price: 'branch_price',
                    stock_delivery_inbound_detail_product_branch_summary: 'product_branch_summary',
                    stock_delivery_inbound_detail_barcode: 'barcode',
                },
                id: MODULE.STOCK_DELIVERY_INBOUND,
                label: Lang.t('label.stock_delivery_inbound')
            },
            inventory_adjust: {
                columns: {
                    inventory_adjust_audited_date: {},
                    inventory_adjust_transaction_date: {},
                    inventory_adjust_approval_status: {},
                    inventory_adjust_total_gain_amount: {},
                    inventory_adjust_total_lost_amount: {},
                    inventory_adjust_sheet_number: {},
                    inventory_adjust_remarks: {},

                    inventory_adjust_detail_changed_qty: {
                        formula: {
                            computation: '((inventory_adjust_detail.qty * inventory_adjust_detail.unit_qty ) + inventory_adjust_detail.pc_qty)',
                            columns: 'inventory_adjust_detail.qty,inventory_adjust_detail.unit_qty,inventory_adjust_detail.pc_qty'
                        }
                    },
                    inventory_adjust_detail_new_qty: {
                        formula: {
                            computation: '(inventory_adjust_detail.inventory + ((inventory_adjust_detail.qty * inventory_adjust_detail.unit_qty ) + inventory_adjust_detail.pc_qty))',
                            columns: 'inventory_adjust_detail.inventory,inventory_adjust_detail.qty,inventory_adjust_detail.unit_qty,inventory_adjust_detail.pc_qty'
                        }
                    },
                    inventory_adjust_detail_amount: {
                        formula: {
                            computation: '(inventory_adjust_detail.price * ((inventory_adjust_detail.qty * inventory_adjust_detail.unit_qty ) + inventory_adjust_detail.pc_qty))',
                            columns: 'inventory_adjust_detail.price,inventory_adjust_detail.qty,inventory_adjust_detail.unit_qty,inventory_adjust_detail.pc_qty'
                        }
                    },
                },
                references: {
                    inventory_adjust_detail: 'inventory_adjust_detail',
                    
                    inventory_adjust_for_location: 'location',
                    inventory_adjust_created_by: 'user',
                    inventory_adjust_requested_by: 'user',
                    inventory_adjust_audited_by: 'user',
                    inventory_adjust_detail_unit: 'system_code',

                    inventory_adjust_detail_product: 'product',
                    inventory_adjust_detail_product_category: 'system_code',
                    inventory_adjust_detail_product_brand: 'system_code',
                    inventory_adjust_detail_product_supplier: 'supplier',
                    inventory_adjust_detail_product_suppliers: 'suppliers',
                    inventory_adjust_detail_product_branch_price: 'branch_price',
                    inventory_adjust_detail_product_branch_summary: 'product_branch_summary',
                    inventory_adjust_detail_barcode: 'barcode',
                },
                id: MODULE.INVENTORY_ADJUST,
                label: Lang.t('label.inventory_adjust')
            },
        },
        references: {
            columns: {
                inventory_adjust_detail : {
                    unit_qty: {},
                    qty: {},
                    pc_qty: {},
                    inventory: {},
                    price: {},
                    remarks: {},
                },
                invoice_detail: {
                    unit_qty: {},
                    qty: {},
                    oprice: {},
                    price: {},
                    discount1: {},
                    discount2: {},
                    discount3: {},
                    discount4: {},
                    remarks: {},
                    remaining_qty: {},
                },
                inventory_chain_detail: {
                    transactionable_type: {},
                    unit_qty: {},
                    qty: {},
                    oprice: {},
                    price: {},
                    discount1: {},
                    discount2: {},
                    discount3: {},
                    discount4: {},
                    remarks: {},
                },
                product: {
                    stock_no: {},
                    supplier_sku: {},
                    name: {},
                    description: {},
                    chinese_name: {},
                    senior: {},
                    memo: {},
                    status: {},
                },
                system_code: {
                    code: {},
                    name: {},
                    remarks: {}
                },
                category: {
                    code: {},
                    name: {},
                },
                branch_price: {
                    price_a: {},
                    price_b: {},
                    price_c: {},
                    price_d: {},
                    price_e: {},
                    product_id: {},
                    purchase_price: {},
                    selling_price: {},
                    wholesale_price: {},
                },
                supplier: {
                    code: {},
                    full_name: {},
                    chinese_name: {},
                    owner_name: {},
                    contact: {},
                    address: {},
                    landline: {},
                    fax: {},
                    mobile: {},
                    email: {},
                    website: {},
                    term: {},
                    status: {},
                    contact_person: {},
                    memo: {},
                },
                suppliers: {
                    default: {},
                    highest_price: {},
                    latest_fields_updated_date: {},
                    latest_price: {},
                    latest_unit_id: {},
                    latest_unit_qty: {},
                    lowest_price: {},
                    min_qty: {},
                    order_lead_time: {},
                    product_id: {},
                    remarks: {},
                },
                product_branch_summary: {
                    reserved_qty: {},
                    requested_qty: {},
                    qty: {},
                },
                branch: {
                    code: {},
                    name: {},
                    address: {},
                    contact: {},
                    business_name: {},
                },
                location: {
                    code: {},
                    name: {},
                    address: {},
                    contact: {},
                    business_name: {},
                },
                user: {
                    code: {},
                    username: {},
                    full_name: {},
                    position: {},
                    address: {},
                    telephone: {},
                    mobile: {},
                    email: {},
                    memo: {},
                    type: {},
                    status: {},
                },
                customer: {
                    name: {},
                    code: {},
                    memo: {},
                    credit_limit: {},
                    pricing_type: {},
                    website: {},
                    term: {},
                    status: {},

                    detail_name: {},
                    detail_address: {},
                    detail_chinese_name: {},
                    detail_owner_name: {},
                    detail_contact: {},
                    detail_landline: {},
                    detail_fax: {},
                    detail_mobile: {},
                    detail_email: {},
                    detail_contact_person: {},
                    detail_tin: {},
                },
                barcode: {
                    code: {},
                    memo: {},
                },
            }
        }
    }
}