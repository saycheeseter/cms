import CoreMixins from '../Common/CoreMixins';
import IncomeStatementService from '../Services/IncomeStatementService';
import SalesOutboundService from '../Services/SalesOutboundService';
import OtherPaymentService from '../Services/OtherPaymentService';
import OtherIncomeService from '../Services/OtherIncomeService';
import DamageOutboundService from '../Services/DamageOutboundService';
import InventoryAdjustService from '../Services/InventoryAdjustService';
import SalesReturnInboundService from "../Services/SalesReturnInboundService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: Laravel.auth.branch },
                        limit: 1
                    },
                    { 
                        name: Lang.t('label.date'),
                        type:'yearMonth',
                        columnName: 'year_month',
                        limit: 2
                    },
                ],
                data: []
            }
        },
        summary: new Data({
            gross_sales: 0,
            return_amount: 0,
            cost_of_goods_sold: 0,
            other_payment: 0,
            other_income: 0,
            damage: 0,
            adjustment_discrepancy: 0
        }),
        labels: new Data({
            branch: Lang.t('label.all_branches'),
            date: '-'
        }),
    },

    mounted: function() {
        this.fetch();
    },

    methods: {
        fetch() {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.modal.filter.visible = false;
            that.processing = true;
            that.summary.reset();
            that.labels.reset();

            IncomeStatementService.fetch({
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.buildLabels();
                that.summary.set(response.data.values.summary);
                that.processing = false;
            })
            .catch(function (error) {
                that.processing = false;
            })
        },

        open(service) {
            let filters = this.modal.filter.data.length === 0
                ? null
                : this.modal.filter.data;

            service.open(filters);
        },

        buildLabels() {

            // let from = '-', to = '-';
            let branch = '';
            let dates = [];

            for(let index in this.modal.filter.data) {
                let currentFilter = this.modal.filter.data[index];

                switch (currentFilter.column) {
                    case 'created_for':
                        for(let branchIndex in branches) {
                            if(branches[branchIndex].id === currentFilter.value) {
                                branch = branches[branchIndex].label;
                                break;
                            }
                        }
                        break;

                    case 'year_month':
                        switch(currentFilter.operator) {
                            case '>':
                            case '>=':
                                dates.push(Lang.t('label.from') + ' ' + currentFilter.value);
                                break;

                            case '<':
                            case '<=':
                                dates.push(Lang.t('label.to') + ' ' + currentFilter.value);
                                break;

                            case '=':
                                dates.push(currentFilter.value);
                                break;
                        }
                        break;
                }
            }

            this.labels.branch = branch === ''
                ? Lang.t('label.all_branches')
                : branch;

            this.labels.date = dates.length > 0
                ? dates.join(', ')
                : '-';
        },

        goToSalesOutbound() {
            this.open(new SalesOutboundService);
        },

        goToSalesReturnInbound() {
            this.open(new SalesReturnInboundService);
        },

        goToOtherPayment() {
            this.open(OtherPaymentService);
        },

        goToOtherIncome() {
            this.open(OtherIncomeService);
        },

        goToDamageOutbound() {
            this.open(new DamageOutboundService);
        },

        goToInventoryAdjust() {
            this.open(new InventoryAdjustService);
        }
    },

    computed: {
        netSales: function() {
            return Numeric.format(Numeric.parse(this.summary.gross_sales) - Numeric.parse(this.summary.return_amount));
        },
        grossProfit: function() {
            return Numeric.format(Numeric.parse(this.netSales) - Numeric.parse(this.summary.cost_of_goods_sold));
        },
        maGrossProfit: function() {
            return Numeric.format(Numeric.parse(this.netSales) - Numeric.parse(this.summary.ma_cost_of_goods_sold));
        },
        netIncome: function() {
            return Numeric.format(Numeric.parse(this.grossProfit) 
                    - Numeric.parse(this.summary.other_payment)
                    + Numeric.parse(this.summary.other_income)
                    - Numeric.parse(this.summary.damage)
                    - Numeric.parse(this.summary.adjustment_discrepancy));
        },
        maNetIncome: function() {
            return Numeric.format(Numeric.parse(this.maGrossProfit) 
                    - Numeric.parse(this.summary.other_payment)
                    + Numeric.parse(this.summary.other_income)
                    - Numeric.parse(this.summary.damage)
                    - Numeric.parse(this.summary.adjustment_discrepancy));
        }
    }
});
