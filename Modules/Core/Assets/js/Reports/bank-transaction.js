import CoreMixins from '../Common/CoreMixins';
import BankTransactionReportService from "../Services/BankTransactionReportService";
import PaymentService from "../Services/PaymentService";
import OtherPaymentService from "../Services/OtherPaymentService";
import CollectionService from "../Services/CollectionService";
import OtherIncomeService from "../Services/OtherIncomeService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        DateTimePicker
    },

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: false,
                withActionCell : false,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            },
        },
        references: {
            branches: branches,
            bankAccounts: bankAccounts
        },
        filter: {
            transaction:{
                date_from: Moment().startOf('day').format(),
                date_to: Moment().endOf('day').format(),
                branch: null,
                bank_account: null
            },
        },

        message: new Notification,
    },

    created() {
        this.references.branches.unshift({
            id: null,
            label: Lang.t('label.all_branches')
        });
    },

    methods: {
        search() {
            if (this.processing || !this.hasBankAccountFilter()) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            BankTransactionReportService.search({
                'filters': that.filter.transaction
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.pagination.current_page = 1;
                that.createRunningBalance(response.data.values.records, response.data.values.beginning);
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        createRunningBalance(data, beg) {
            let beginning = Numeric.parse(beg.total);

            let running = beginning;
            let records = [];

            for (let i = 0; i < data.length; i++) {
                let current = data[i];

                running += (Numeric.parse(current.in) - Numeric.parse(current.out));

                current['balance'] = Numeric.format(running);

                records.push(current);
            }

            this.datatable.value.data = records;

            this.createInitialBalance(beginning);
        },

        createInitialBalance(beginning) {
            this.datatable.value.data.unshift({
                date: '',
                particular: Lang.t('label.beginning'),
                in: '',
                out: '',
                type: null,
                reference_id: null,
                balance: Numeric.format(beginning)
            });
        },

        goToPage(type, id) {
            let service = null;

            switch (type) {
                case 'payment':
                    service = new PaymentService();
                    break;

                case 'other_payment':
                    service = OtherPaymentService;
                    break;

                case 'collection':
                    service = new CollectionService();
                    break;

                case 'other_income':
                    service = OtherIncomeService;
                    break;
            }

            if (null !== service) {
                service.view(id);
            }
        },

        hasBankAccountFilter() {
            if (null === this.filter.transaction.bank_account) {
                console.log('here');
                this.message.error.set([Lang.t('validation.bank_account_required')]);
                return false;
            }

            return true;
        }
    },

    computed: {
        totalIn: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['in']);
            }

            return Numeric.format(total);
        },
        totalOut: function() {
            let total = 0;

            for(let index in this.datatable.value.data) {
                total += Numeric.parse(this.datatable.value.data[index]['out']);
            }

            return Numeric.format(total);
        },
        total: function() {
            return Numeric.format(Numeric.parse(this.totalIn) - Numeric.parse(this.totalOut));
        }
    }
});