import CoreMixins from '../Common/CoreMixins';
import AuditTrailService from '../Services/AuditTrailService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table',
            settings: {
                withPaging : true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }
        },

        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.user'),
                        columnName: 'user_id',
                        type: 'select',
                        selectOptions: users,
                    },
                    {
                        name: Lang.t('label.action'),
                        columnName: 'event',
                        type: 'select',
                        selectOptions: [
                            { id: String(AUDIT_EVENT.CREATED), label: Lang.t('label.created') },
                            { id: String(AUDIT_EVENT.UPDATED), label: Lang.t('label.updated') },
                            { id: String(AUDIT_EVENT.DELETED), label: Lang.t('label.deleted') }
                        ]
                }   ,
                ],
                data: []
            }
        },

        message: new Notification,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.visible = false;
            this.processing = true;
            this.message.reset();

            AuditTrailService.paginate({
                'perPage': that.datatable.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.audits;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },
    },
});
