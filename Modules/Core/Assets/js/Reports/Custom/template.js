import CoreMixins from '../../Common/CoreMixins';
import TemplateService from '../../Services/Report/TemplateService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table',
            settings: {
                withPaging : true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }
        },

        modal: {
            filter: {
                visible: false,
                options: Object.values(filters),
                data: []
            }
        },

        columnSettings: {
            id: 'column-settings',
            visible: false,
            selections: {},
            columns: []
        },

        reference: {
            format: template.format
        },

        message: new Notification,
    },

    mounted: function() {
        this.buildColumnSettingsColumns();
        this.paginate();
    },

    methods: {

        buildColumnSettingsColumns() {
            for (let key in this.reference.format.columns) {
                
                this.columnSettings.columns.push({ 
                    alias: key, 
                    label: this.reference.format.columns[key].header_name, 
                    default: true 
                })
            }
        },

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            TemplateService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
                name: slug
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.records;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        excel(templateId) {
            var params = {
                filters: this.modal.filter.data,
                template_id: templateId,
                settings: this.columnSettings.selections
            };

            TemplateService.export(params);
        },
    },
});
