import CoreMixins from '../../Common/CoreMixins';
import ProductService from '../../Services/ProductService';
import ProductMixins from '../../Common/ProductMixins';

export default {
    el: '#app',

    mixins: [CoreMixins, ProductMixins],

    data: {
        datatable: {
           details : {
                id: 'table-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withActionCell: false,
                },
                value: {
                    data: [],
                    pagination: {}
                }  
           }, 
           
           product: {
                id: 'product-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withActionCell: false,
                    withSelect: {
                        value: 'id'
                    }
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
                checked: []
            },
        },

        modal: {
            filter: {
                visible: false,
                options: [],
                data: []
            }    
        },

        productPicker: {
            visible: false
        },

        message: new Notification,
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        productSearch(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.filter.product.visible = false;

            ProductService.paginate({
                'perPage': that.datatable.product.settings.perPage,
                'page': page,
                'filters': that.filter.product.data,
            })
            .then(function (response) {
                that.datatable.product.value.data = response.data.values.products;
                that.datatable.product.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        filterByProduct() {
            if(this.processing) {
                return false;
            }

            if (this.datatable.product.checked.length === 0) {
                this.message.error.set([Lang.t('validation.details_min_one')]);
                return false;
            }

            this.productPicker.visible = false;

            this.paginate();
        },
    },

    watch: {
        'productPicker.visible': function(visible) {
            if(visible) {
                this.productSearch();
            }
        },
    },
};
