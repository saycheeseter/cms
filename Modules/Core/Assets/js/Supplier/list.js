import CoreMixins from '../Common/CoreMixins';
import SupplierService from '../Services/SupplierService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        tabs: new Toggle([
            { name: 'basic', label: Lang.t('label.basic_info') },
            { name: 'advance', label: Lang.t('label.advance_info') },
        ], 'basic'),
        fields: new Data({
            id: '',
            code: '',
            full_name: '',
            chinese_name: '',
            owner_name: '',
            contact: '',
            address: '',
            landline: '',
            fax: '',
            mobile: '',
            email: '',
            website: '',
            term: 0,
            contact_person: '',
            memo: '',
            status: 1,
        }),
        message: new Notification({
            success: '',
            info: ''
        }),
        modal: {
            confirm: {
                value: false,
                visible: false,
                id: 0,
                index: 0
            },
            details: {
                visible: false,
                id: 'details'
            },
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string' },
                    { name: Lang.t('label.full_name'), columnName: 'full_name', type: 'string' },
                    { name: Lang.t('label.chinese_name'), columnName: 'chinese_name', type: 'string' },
                    { name: Lang.t('label.memo'), columnName: 'memo', type: 'string' },
                ],
                data: []
            }
        },
        message: new Notification(['list', 'details']),
        columnSettings: {
            supplier: {
                visible: false,
                columns: [
                    { alias: 'name', label: Lang.t('label.name'), default: true },
                    { alias: 'chineseName', label: Lang.t('label.chinese_name'), default: false },
                    { alias: 'contact', label: Lang.t('label.contact'), default: false },
                    { alias: 'ownerName', label: Lang.t('label.owner_name'), default: false },
                    { alias: 'address', label: Lang.t('label.address'), default: false },
                    { alias: 'landline', label: Lang.t('label.landline'), default: false },
                    { alias: 'fax', label: Lang.t('label.fax'), default: false },
                    { alias: 'mobile', label: Lang.t('label.mobile'), default: false },
                    { alias: 'email', label: Lang.t('label.email'), default: false },
                    { alias: 'website', label: Lang.t('label.website'), default: false },
                    { alias: 'contactPerson', label: Lang.t('label.contact_person'), default: false },
                    { alias: 'memo', label: Lang.t('label.memo'), default: false },
                    { alias: 'term', label: Lang.t('label.term'), default: false },
                    { alias: 'status', label: Lang.t('label.status'), default: false },
                ],
                selections: {},
                module: MODULE.SUPPLIER
            }
        }
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            SupplierService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.processing = false;
                that.message.list.info = response.data.message;
                that.datatable.value.data = response.data.values.suppliers;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            SupplierService.store(that.fields.data())
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;
                    
                    setTimeout(function() {
                        that.modal.details.visible = false;
                        that.datatable.value.data.push(response.data.values.supplier);
                        that.computePagination(that.datatable.value.pagination, 'add');
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        update() {
            if (this.processing || !permissions.update) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            SupplierService.update(that.fields.data(), that.fields.id)
                .then(function (response) {
                    that.message.details.success = response.data.message;
                    that.processing = false;
                    
                    setTimeout(function() {
                        that.modal.details.visible = false;
                        that.paginate(that.datatable.value.pagination.current_page);
                    }, 2000);
                })
                .catch(function (error) {
                    that.message.details.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            
            SupplierService.destroy(this.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.list.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }
            
            let that = this;

            SupplierService.show(id)
                .then(function (response) {
                    that.fields.set(response.data.values.supplier);
                    that.processing = false;
                    that.modal.details.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        save() {
            if(this.fields.id == ""){
                this.store();
            } else {
                this.update();
            }
        },

        confirm(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        }
    },

    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.id = 0;
                this.modal.confirm.index = 0;
                this.modal.confirm.value = false;
            }
        },
        'modal.details.visible': function(visible) {
            if (!visible) {
                this.fields.reset();
                this.message.reset();
            } else {
                this.tabs.active('basic');
            }
        },
    },
});
