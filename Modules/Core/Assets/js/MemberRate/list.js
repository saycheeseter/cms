import CoreMixins from '../Common/CoreMixins';
import MemberRateService from '../Services/MemberRateService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        Datepicker,
    },

    data: {
        datatable: {
            list: {
                id: 'table-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            detail: {
                id: 'table-modal',
                settings: {
                    withPaging: true,
                    withPaginateCallBack: false,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },
        modal: {
            list: {
                confirm: new Data({
                    visible: false,
                    value: false,
                    id: 0,
                    index: 0
                })
            },
            detail: {
                visible: false,
                id: 'modal-detail',
                confirm: new Data({
                    visible: false,
                    value: false,
                    index: 0
                })
            },
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    { name: Lang.t('label.memo'), columnName: 'memo', type: 'string', optionBool: false }
                ],
                data: []
            }
        },
        rate: {
            info: new Data({
                id: 0,
                name: '',
                memo: '',
                status: true
            }),
            detail: new Data({
                id: 0,
                amount_to: 0,
                amount_from: 0,
                date_from: Moment().format('YYYY-MM-DD'),
                date_to: Moment().format('YYYY-MM-DD'),
                discount: 0,
                amount_per_point: 0
            })
        },
        format: {
            detail: {
                amount_from: 'numeric',
                amount_to: 'numeric',
                date_from: 'date',
                date_to: 'date',
                discount: 'numeric',
                amount_per_point: 'numeric'
            }
        },

        message: new Notification(['list', 'detail'])
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            this.modal.filter.visible = false;

            MemberRateService.paginate({
                'perPage': that.datatable.list.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data,
            })
            .then(function (response) {
                that.message.list.info = response.data.message;
                that.datatable.list.value.data = response.data.values.rates;
                that.datatable.list.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            MemberRateService.store({
                info: that.rate.info.data(),
                details: Format.make(that.datatable.detail.value.data, this.format.detail)
            })
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.datatable.list.value.data.push(response.data.values.rate);
                    that.computePagination(that.datatable.list.value.pagination, 'add');
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            MemberRateService.update({
                info: that.rate.info.data(),
                details: Format.make(that.datatable.detail.value.data, this.format.detail)
            }, that.rate.info.id)
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.paginate(that.datatable.list.value.pagination.current_page);
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            MemberRateService.destroy(that.modal.list.confirm.id)
                .then(function (response) {
                    that.message.list.success = response.data.message;
                    that.processing = false;
                    that.modal.list.confirm.visible = false;

                    that.datatable.list.value.data.splice(that.modal.list.confirm.index, 1);
                    that.computePagination(that.datatable.list.value.pagination, 'delete');
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            this.processing = true;

            MemberRateService.show(id)
                .then(function (response) {
                    that.rate.info.set(response.data.values.rate);
                    that.datatable.detail.value.data = response.data.values.details;
                    that.processing = false;
                    that.modal.detail.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        confirm(data, index) {
            this.modal.list.confirm.visible = true;
            this.modal.list.confirm.id = data.id;
            this.modal.list.confirm.index = index;
        },

        save() {
            if (!this.isValidInfo()) {
                return false;
            }

            if (this.rate.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        isValidInfo() {
            if (this.rate.info.id !== 0 && !permissions.update) {
                return false;
            } 

            if(!this.finalized('detail')) {
                this.message.detail.error.set([Lang.t('validation.finalize_all_rows')]);
                return false;
            }

            let validation = Validator.make(this.rate.info.data(), {
                name: 'required',
            }, {
                'name.required': Lang.t('validation.name_required'),
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();
            
            return true;
        },

        // Member Rate Detail Functions
        createDetail() {
            let data = Object.assign({}, this.rate.detail.data());

            if (!this.isValidDetail(data)) {
                return false;
            }

            this.datatable.detail.value.data.push(data);
            this.rate.detail.reset();
        },

        updateDetail(data, index) {
            if (!this.isValidDetail(data)) {
                return false;
            }

            this.changeState(index, 'destroy', 'detail');
        },

        isValidDetail(data) {
            let validation = Validator.make(Format.make(data, this.format.detail), {
                amount_from: 'required',
                amount_to: 'required',
                date_from: 'required',
                date_to: 'required',
                discount: 'required',
                amount_per_point: 'required'
            }, {
                'amount_from.required': Lang.t('validation.details_amount_from_required'),
                'amount_to.required': Lang.t('validation.details_amount_to_required'),
                'amount_per_point.required': Lang.t('validation.details_amount_per_point_required'),
                'date_from.required': Lang.t('validation.details_date_from_required'),
                'date_from.before': Lang.t('validation.date_from_invalid_range'),
                'date_to.required': Lang.t('validation.details_date_to_required'),
                'date_to.after': Lang.t('validation.date_to_invalid_range'),
                'discount.required': Lang.t('validation.details_discount_required'),
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();

            return true;
        },

        confirmDetail(data, index, dataIndex) {
            this.modal.detail.confirm.visible = true;
            this.modal.detail.confirm.index = dataIndex;
        },

        destroyDetail() {
            this.datatable.detail.value.data.splice(this.modal.detail.confirm.index, 1);
            this.modal.detail.confirm.visible = false;
        },
    },

    watch: {
        'modal.list.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.list.confirm.reset();
            }
        },
        'modal.detail.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.detail.confirm.reset();
            }
        },
        'modal.detail.visible': function(visible) {
            if (!visible) {
                this.rate.info.reset();
                this.rate.detail.reset();
                this.datatable.detail.value.data = [];
                this.message.reset();
            }
        }
    }
});
