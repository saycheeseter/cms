import PurchaseReturnService from '../Services/PurchaseReturnService';
import Invoice from '../Invoice/list.js';

let app = new Vue({
    el: '#app',

    mixins: [Invoice],

    data: {
        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'supplier', label: Lang.t('label.supplier'), default: true },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'transaction_status', label: Lang.t('label.transaction_status'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'payment_method', label: Lang.t('label.payment_method') },
                { alias: 'term', label: Lang.t('label.term') },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.PURCHASE_RETURN,
                key: 'info'
            }
        },
        service: new PurchaseReturnService,
        cache: {
            module: MODULE.PURCHASE_RETURN
        }
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.supplier'),
            columnName: 'supplier_id',
            type: 'select',
            selectOptions: suppliers
        });
    }
});
