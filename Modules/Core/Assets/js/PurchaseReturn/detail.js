import Invoice from '../Invoice/detail.js';
import PurchaseReturnService from '../Services/PurchaseReturnService';
import AutoGroupProductBreakdownMixins from '../Common/AutoGroupProductBreakdownMixins.js';
import Outbound from '../Common/OutboundMixins.js';
import ProductService from '../Services/ProductService';

let app = new Vue({
    el: '#app',

    mixins: [Invoice, Outbound, AutoGroupProductBreakdownMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },

        reference: {
            tsuppliers: tsuppliers,
            paymentMethods: paymentMethods,
        },

        attachment: {
            module: MODULE.PURCHASE_RETURN
        },

        service: new PurchaseReturnService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.PURCHASE_RETURN,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.PURCHASE_RETURN
        }
    },

    methods: {
        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getExtraInfoForUpdateApproved() {
            return [
                'payment_method',
                'term'
            ];
        }
    },

    created() {
        this.createInfo({
            term: 0,
            supplier_id: '',
            payment_method: '',
        });
    },
});