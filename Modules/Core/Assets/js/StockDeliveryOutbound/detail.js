import InventoryChain from '../InventoryChain/detail.js';
import StockDeliveryOutboundService from '../Services/StockDeliveryOutboundService';
import StockDeliveryService from '../Services/StockDeliveryService';
import BranchService from '../Services/BranchService';
import Outbound from '../Common/OutboundMixins.js';
import DeliveryProductSelectorAPIMixins from "../Common/DeliveryProductSelectorAPIMixins";

export default {
    mixins: [InventoryChain, Outbound, DeliveryProductSelectorAPIMixins],

    data: {
        service: {
            invoice: new StockDeliveryService,
            inventoryChain: new StockDeliveryOutboundService
        },

        reference: {
            deliveryLocations: locations
        },

        minimumInventoryWarning : new Data({
            id: 'warning',
            visible: false,
            messages: [],
            proceed: false,
            action: ''
        }),

        attachment: {
            module: MODULE.STOCK_DELIVERY_OUTBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_DELIVERY_OUTBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.STOCK_DELIVERY_OUTBOUND
        },

        cache: {
            module: MODULE.STOCK_DELIVERY_OUTBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.STOCK_DELIVERY,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            deliver_to: null,
            deliver_to_location: null,
        });
    },

    methods: {
        onSuccessfulLoad(response) {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        onCacheLoaded() {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        onSuccessfulImportInvoice(response) {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        process(reload = false) {
            this.reload = reload;

            if (!this.validate() 
                || (this.info.id === 0 && ! permissions.create)
                || (this.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            this.minimumInventoryWarning.action = 'save';
            this.checkIfInventoryReachesMinimum();
        },

        save() {
            if (this.info.id === 0) {
                this.store(this.data);
            } else {
                this.update(this.data);
            }
        },

        proceed() {
            this.minimumInventoryWarning.visible = false;
            this.minimumInventoryWarning.proceed = true;

            switch(this.minimumInventoryWarning.action) {
                case 'save':
                    this.save();
                    break;

                case 'for-approval':
                    this.approval(APPROVAL_STATUS.FOR_APPROVAL);
                    break;
            }

            this.minimumInventoryWarning.reset();
        },

        approval(status) {
            if(this.processing || !this.validate()) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            
            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL:
                    if (!this.minimumInventoryWarning.proceed) {
                        this.minimumInventoryWarning.action = 'for-approval';
                        this.checkIfInventoryReachesMinimum();
                        return false;
                    }
                    
                    this.updateBeforeForApproval();
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        checkIfInventoryReachesMinimum() {
            if (! this.isMinimumInventoryWarningActive()) {
                this.processing = false;
                this.proceed();
                return false;
            }

            let that = this;

            this.processing = true;
            this.cacheData();

            this.service.inventoryChain.warning(this.data)
                .then(function (response){
                    that.clearCache();
                    that.processing = false;
                    that.proceed();
                })
                .catch(function (error) {
                    that.minimumInventoryWarning.visible = true;
                    that.minimumInventoryWarning.messages = Obj.toArray(error.response.data.errors);
                    that.processing = false
                });
        },

        getDeliveryLocations(def = null) {
            let that = this;

            this.processing = true;

            BranchService.details(this.info.deliver_to)
                .then(function (response) {
                    that.reference.deliveryLocations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.deliver_to_location = location;
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.deliveryLocations = [];
                });
        },

        isMinimumInventoryWarningActive() {
            return Laravel.settings.delivery_outbound_warn_if_inventory_reaches_minimum;
        }
    }
};