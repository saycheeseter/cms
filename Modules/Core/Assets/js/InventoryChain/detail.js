import InventoryFlow from '../InventoryFlow/detail.js';
import BranchService from '../Services/BranchService';
import ProductService from '../Services/ProductService';
import LocalCache from "../Classes/LocalCache";

export default {
    mixins: [InventoryFlow],

    data: {
        importing: {
            referenceNumber: ''
        },

        reference: {
            invoice: [],
        },

        datatable: {
            serial: {
                list: {
                    id: 'serial-number-list',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                },
                selected: {
                    id: 'serial-number-selected',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                }
            },
            batch: {
                list: {
                    id: 'batch-number-list',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                },
                selected: {
                    id: 'batch-number-selected',
                    settings: {
                        withPaging : false,
                        withPaginateCallBack: false,
                        withActionCell: false,
                        withSelect: {
                            value: 'id',
                            header: false
                        }
                    },
                    value: {
                        selected: [],
                        data: [],
                        pagination: {
                            count: 1,
                            current_page: 1 ,
                            links: [],
                            total: 1,
                            total_pages: 1
                        }
                    },
                }
            }
        },

        columnSettings: {
            details: {
                columns: [
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true},
                    { alias: 'stockNo', label: Lang.t('label.stock_no'), default: true},
                    { alias: 'chineseName', label: Lang.t('label.chinese_name') },
                    { alias: 'discount1', label: Lang.t('label.discount_1') },
                    { alias: 'discount2', label: Lang.t('label.discount_2') },
                    { alias: 'discount3', label: Lang.t('label.discount_3') },
                    { alias: 'discount4', label: Lang.t('label.discount_4') },
                    { alias: 'remarks', label: Lang.t('label.remarks') },
                ],
            }
        },

        format: {
            detail: {
                qty: 'numeric',
                unit_qty: 'numeric',
                oprice: 'numeric',
                price: 'numeric',
                discount1: 'numeric',
                discount2: 'numeric',
                discount3: 'numeric',
                discount4: 'numeric',
            },
        },

        validation: {
            rules: {
                detail: {
                    qty: 'numeric',
                    price: 'numeric',
                    discount1: 'numeric',
                    discount2: 'numeric',
                    discount3: 'numeric',

                    discount4: 'numeric',
                },
            },
            messages: {
                detail: {
                    'qty.numeric': Lang.t('validation.qty_numeric'),
                    'price.numeric': Lang.t('validation.price_numeric'),
                    'discount1.numeric': Lang.t('validation.discount_numeric', { num: '1' }),
                    'discount2.numeric': Lang.t('validation.discount_numeric', { num: '2' }),
                    'discount3.numeric': Lang.t('validation.discount_numeric', { num: '3' }),
                    'discount4.numeric': Lang.t('validation.discount_numeric', { num: '4' })
                },
            }
        },

        buffer: {
            data: {
                created_for: '',
                reference_id: '',
                transaction_type: '',
            },
            reference: {
                invoice: []
            },
            itemManagement: {
                for_location: null
            },
        },

        service: {
            inventoryChain: '',
            invoice: '',
        },

        itemManagement: {
            serials: {},
            batches: {}
        },

        itemManagementSelection: {
            serial: new Data({
                visible: false,
                message: new Notification,
                row: 0,
                searchString: '',
            }),
            batch: new Data({
                visible: false,
                message: new Notification,
                row: 0,
                searchString: '',
            })
        },

        key: 'record',

        importWarning: {
            zeroQty: {
                visible: false
            },
            sameInvoice: {
                visible: false
            }
        },

        dialog: {
            removeItemManagementData: {
                visible: false
            }
        },

        selector: {
            invoice: {
                visible: false,
                filter: {
                    visible: false,
                    options: [
                        { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                        { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                        {
                            name: Lang.t('label.transaction_date_time'),
                            columnName: 'transaction_date',
                            type: 'datetime',
                            optionBool: false,
                            limit: 2
                        },
                    ],
                    data: []
                },
                datatable: {
                    settings: {
                        withPaging : true,
                        perPage: 10,
                        withActionCell: false,
                        withPaginateCallBack: true
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: [],
                },
                columnSettings: {
                    columns: [
                        { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                        { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                        { alias: 'created_from', label: Lang.t('label.created_from') },
                        { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                        { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                        { alias: 'amount', label: Lang.t('label.amount'), default: true },
                        { alias: 'transaction_status', label: Lang.t('label.transaction_status'), default: true },
                        { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                        { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                        { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                        { alias: 'created_date', label: Lang.t('label.created_date') },
                        { alias: 'audited_by', label: Lang.t('label.audit_by') },
                        { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                        { alias: 'deleted', label: Lang.t('label.deleted') },
                    ],
                    visible: false,
                    selections: {},
                }
            }
        },
    },

    methods: {
        load() {
            if (this.processing || id === 'create') {
                if (this.hasLocalCache()) {
                    this.loadCache();
                }

                return false;
            }

            this.processing = true;

            let that = this;

            this.service.inventoryChain.show(id, !this.forViewingOnly)
                .then(function (response) {
                    that.processing = false;
                    that.reference.invoice = response.data.values.reference;
                    that.info.set(response.data.values.record);
                    that.datatable.details.value.data = that.formatAttributesToTransactionDetails(response.data.values.details.data);
                    that.setAttachmentProperties(that.info.id, response.data.values.attachments);
                    that.getLocations(that.info.for_location);

                    if (that.info.is_deleted) {
                        that.options.setup = SETUP.VIEW;
                    }

                    if (!that.forViewingOnly) {
                        that.visibility();
                    } else if (that.forViewingOnly) {
                        that.hideControls();
                    }

                    if(that.hasLocalCache(that.info.id)) {
                        that.cache.modal = true;
                    }

                    that.onSuccessfulLoad(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        store(data) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.cacheData();

            this.service.inventoryChain.store(data)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }

                    that.info.id = response.data.values[that.key].id;
                    that.attachFiles(that.info.id);
                    that.clearCache();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        update(data) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.message.reset();
            this.processing = true;
            this.cacheData();

            this.service.inventoryChain.update(data, that.info.id)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }
                    that.clearCache();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        updateApprovedTransaction() {
            if (this.processing) {
                return false;
            }

            let that = this;
            let data = this.getDataForUpdateApproved();

            this.message.reset();
            this.processing = true;
            this.cacheData();

            this.service.inventoryChain.updateApproved(data.info, this.info.id)
                .then(function (response) {
                    that.clearCache();
                    that.dialog.process.message = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        approval(status) {
            this.action.value = 'approval';
            this.action.params = status;

            if (this.processing || !this.validate()) {
                return false;
            }

            this.processing = true;

            let that = this;

            this.message.reset();

            switch (status) {
                case APPROVAL_STATUS.FOR_APPROVAL:
                    this.updateBeforeForApproval();
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        updateBeforeForApproval() {
            let that = this;

            let update = this.service.inventoryChain.update(this.data, that.info.id);

            this.cacheData();

            axios.all([update])
                .then(axios.spread(function (update) {
                    that.clearCache();
                    that.approvalRequest(APPROVAL_STATUS.FOR_APPROVAL);
                }))
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.clearCache();
                });
        },

        approvalRequest(status) {
            let that = this;

            this.cacheData();

            this.service.inventoryChain.approval({
                transactions: that.info.id,
                status: status
            })
            .then(function (response) {
                that.dialog.process.message = response.data.message;
                that.processing = false;
                that.clearCache();
            })
            .catch(function (error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
                that.clearCache();
            });
        },

        redirect() {
            if (this.info.id != 0 && !this.reload) {
                this.service.inventoryChain.redirect(this.info.id);
            } else {
                this.service.inventoryChain.create(this.reload);
            }
        },

        getLocations(def = null) {
            if (this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            BranchService.details(this.info.created_for)
                .then(function (response) {
                    that.reference.locations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.for_location = location;
                    that.action.reset();
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.locations = [];
                    that.processing = false;
                });
        },

        getInvoice(string) {
            let that = this;

            if (this.processing) {
                return false;
            }

            this.processing = true;

            return this.service.invoice.remaining({
                sheet_number: string,
                branch: this.info.created_for
            })
            .then(function (response) {
                that.reference.invoice = response.data.values.invoices;
                that.processing = false;
                that.action.reset();
            })
            .catch(function (response) {
                that.processing = false;
                that.action.reset();
            });
        },

        importInvoice() {
            if (this.datatable.details.value.data.length > 0 && this.info.reference_id != null) {
                this.importWarning.sameInvoice.visible = true;
                return false;
            }

            this.replaceAndRetrieveInvoice();
        },

        importInvoiceFromSelector(reference_id, sheet_number) {
            let that = this;

            this.selector.invoice.visible = false;

            if (reference_id === this.info.reference_id) {
                this.importWarning.sameInvoice.visible = true;
            } else {
                this.reference.invoice = [
                    { id: reference_id, label: sheet_number }
                ];

                this.info.reference_id = reference_id;
                this.replaceAndRetrieveInvoice();
            }
        },

        replaceAndRetrieveInvoice() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.invoice.retrieve(this.info.reference_id)
                .then(function (response) {
                    that.processing = false;
                    that.message.transaction.success = response.data.message;
                    that.info.set(response.data.values.invoice);
                    that.getLocations(that.info.for_location);
                    that.datatable.details.value.data = [];
                    that.datatable.details.value.data = that.formatImportInvoiceResponseToTransactionDetails(response.data.values.details.data);
                    that.importWarning.sameInvoice.visible = false;
                    that.onSuccessfulImportInvoice(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validate() {
            if (this.processing) {
                return false;
            }

            if (!this.finalized('detail')) {
                this.message.transaction.error.set([Lang.t('validation.finalize_all_rows')]);
                return false;
            }

            if (!this.isSerialsQtyComplete()) {
                return false;
            }

            if (!this.isBatchesQtyComplete()) {
                return false;
            }

            let validation = Validator.make(
                Format.make(this.info.data(), this.format.info),
                this.validation.rules.info,
                this.validation.messages.info
            );

            if (validation.fails()) {
                this.message.transaction.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (this.info.transaction_type === TRANSACTION_TYPE.FROM_INVOICE
                && this.datatable.details.value.data.length > 0
                && this.isInvoiceQtyZero()
            ) {
                this.importWarning.zeroQty.visible = true;
                return false;
            }

            if (!this.validateDetailQty()) {
                return false;
            }

            let callback = this.onSuccessfulValidation();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            return true;
        },

        print(template) {
            if (this.info.id == 0) {
                return false;
            }

            this.service.inventoryChain.print({
                id: this.info.id,
                template: template
            });
        },

        setSpare(index) {
            let row = this.datatable.details.value.data[index];

            if (this.info.approval_status == APPROVAL_STATUS.APPROVED) {
                row['spare_qty'] = Numeric.format(row['remaining_qty']);
            } else {
                row['spare_qty'] = Numeric.format(row['remaining_qty'] - row['total_qty']);
            }
        },

        zeroDiscounts(index) {
            let row = this.datatable.details.value.data[index];

            row['discount1'] = Numeric.format(0);
            row['discount2'] = Numeric.format(0);
            row['discount3'] = Numeric.format(0);
            row['discount4'] = Numeric.format(0);
        },

        setDiscount(index) {
            let row = this.datatable.details.value.data[index];

            if (Numeric.parse(row['discount1']) > 0
                || Numeric.parse(row['discount2']) > 0
                || Numeric.parse(row['discount3']) > 0
                || Numeric.parse(row['discount4']) > 0
            ) {
                row['price'] = Numeric.format(
                    Numeric.parse(row['oprice'])
                    * (1 - Numeric.parse(row['discount1']) / 100)
                    * (1 - Numeric.parse(row['discount2']) / 100)
                    * (1 - Numeric.parse(row['discount3']) / 100)
                    * (1 - Numeric.parse(row['discount4']) / 100)
                );
            }

            this.setAmount(index);
        },

        removeEmptyInvoice() {
            this.importWarning.zeroQty.visible = false;

            for (let key in this.datatable.details.value.data) {
                if (this.datatable.details.value.data[key]['qty'] == 0) {
                    this.datatable.details.value.data.splice(key, 1);
                }
            }

            if (this.datatable.details.value.data.length > 0) {
                this.process();
            }
        },

        isInvoiceQtyZero() {
            for (let key in this.datatable.details.value.data) {
                if (this.datatable.details.value.data[key]['qty'] == 0) {
                    return true;
                }
            }

            return false;
        },

        showItemManagement(data, row) {
            if (this.processing) {
                return false;
            }

            if (data.manage_type === PRODUCT_MANAGE_TYPE.SERIAL) {
                this.showSerials(data, row);
            } else {
                this.showBatches(data, row);
            }
        },

        showSerials(data, row) {
            this.processing = true;
            this.datatable.serial.selected.value.data = data.serials;

            if (!this.disabled) {
                if (!this.itemManagement.serials.hasOwnProperty(data.product_id)) {
                    this.getNewSerials(data.product_id);
                } else {
                    this.datatable.serial.list.value.data = this.itemManagement.serials[data.product_id];
                }
            } else {
                this.datatable.serial.selected.settings.withSelect.value = false;
            }

            this.processing = false;
            this.itemManagementSelection.serial.visible = true;
            this.itemManagementSelection.serial.row = row;
        },

        getNewSerials(id) {
            let that = this;

            ProductService.serials(Object.assign({product_id: id}, this.getSerialsConstraint()))
                .then(function (response) {
                    let serials = that.filterSerials(response.data.values.serials);

                    that.itemManagement.serials[id] = serials;
                    that.datatable.serial.list.value.data = that.itemManagement.serials[id];
                    that.processing = false;
                })
                .catch(function (error) {
                    //that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getSerialsConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.BRANCH_DETAIL,
                entity: this.info.for_location,
                status: SERIAL_STATUS.AVAILABLE,
                transaction: null
            };
        },

        putSerials() {
            if (this.isSerialsLimitReached()) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            this.moveSerials(this.datatable.serial.list.value, this.datatable.serial.selected.value);
        },

        returnSerials() {
            this.moveSerials(this.datatable.serial.selected.value, this.datatable.serial.list.value);
        },

        moveSerials(base, target) {
            for (let index in base.selected) {
                let selected = base.selected[index];

                for (var i = base.data.length - 1; i >= 0; i--) {
                    let serial = base.data[i];

                    if (Number(selected) !== Number(serial.id)) {
                        continue;
                    }

                    target.data.push(serial);
                    base.data.splice(i, 1);
                }
            }

            base.selected = [];
            target.selected = [];
        },

        putAllSerials() {
            if (this.isSerialsLimitReachedAll()) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_count_invalid')]);
                return false;
            }

            this.moveAllSerials(this.datatable.serial.list.value, this.datatable.serial.selected.value);
        },

        returnAllSerials() {
            this.moveAllSerials(this.datatable.serial.selected.value, this.datatable.serial.list.value);
        },

        moveAllSerials(base, target) {
            for (var i = base.data.length - 1; i >= 0; i--) {
                let serial = base.data[i];

                target.data.push(serial);
                base.data.splice(i, 1);
            }

            base.selected = [];
            target.selected = [];
        },

        isSerialsLimitReached() {
            return (Numeric.parse(this.datatable.serial.list.value.selected.length)
                + Numeric.parse(this.selectedSerialCount))
                > Numeric.parse(this.availableSerialQty);
        },

        isSerialsLimitReachedAll() {
            return (Numeric.parse(this.datatable.serial.list.value.data.length)) 
                > Numeric.parse(this.availableSerialQty);
        },

        isSerialsQtyComplete() {
            let complete = true;
            let errors = [];

            for (let index in this.datatable.details.value.data) {
                let current = this.datatable.details.value.data[index];

                if (current.manage_type === PRODUCT_MANAGE_TYPE.SERIAL
                    && Math.ceil(Numeric.parse(current.total_qty)) !== current.serials.length
                ) {
                    errors.push(Lang.t('error.row') + '[' + (Number(index) + 1) + '] ' + Lang.t('validation.serial_count_invalid'));
                    complete = false;
                }
            }

            if (errors.length > 0) {
                this.message.transaction.error.set(errors);
            }

            return complete;
        },

        showBatches(data, row) {
            this.processing = true;
            this.datatable.batch.selected.value.data = data.batches;

            if (!this.disabled) {
                if (!this.itemManagement.batches.hasOwnProperty(data.product_id)) {
                    this.getNewBatches(data.product_id);
                } else {
                    this.setBatchGetQty(this.itemManagement.batches[data.product_id]);

                    this.datatable.batch.list.value.data = this.itemManagement.batches[data.product_id];
                }
            } else {
                this.datatable.batch.selected.settings.withSelect.value = false;
            }

            this.processing = false;
            this.itemManagementSelection.batch.visible = true;
            this.itemManagementSelection.batch.row = row;
        },

        getNewBatches(id) {
            let that = this;

            ProductService.batches(Object.assign({product_id: id}, this.getBatchesConstraint()))
                .then(function (response) {
                    let batches = that.filterBatches(response.data.values.batches);

                    that.setBatchGetQty(batches);

                    that.itemManagement.batches[id] = batches;
                    that.datatable.batch.list.value.data = that.itemManagement.batches[id];
                    that.processing = false;
                })
                .catch(function (error) {
                    //that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getBatchesConstraint() {
            return {
                constraint: ITEM_MANAGEMENT_OWNER.BRANCH_DETAIL,
                entity: this.info.for_location,
                supplier: null
            };
        },

        putBatches() {
            if (Numeric.parse(this.availableBatchQty) === 0) {
                this.itemManagementSelection.batch.message.error.set([Lang.t('validation.batch_count_invalid')]);
                return false;
            }

            for (let index in this.datatable.batch.list.value.selected) {
                let current = this.datatable.batch.list.value.selected[index];
                let existing = this.getBatchInSelected(current);

                for (let i in this.datatable.batch.list.value.data) {
                    let batch = this.datatable.batch.list.value.data[i];

                    if (batch.id !== current || Numeric.parse(batch.get_qty) <= 0) {
                        continue;
                    }

                    if (existing !== null) {
                        batch.remaining_qty = (Numeric.parse(batch.remaining_qty)
                            + Numeric.parse(existing.qty))
                            - Numeric.parse(batch.get_qty);

                        existing.qty = Numeric.format(0 + Numeric.parse(batch.get_qty));
                    } else {
                        this.datatable.batch.selected.value.data.push({
                            id: batch.id,
                            name: batch.name,
                            qty: batch.get_qty
                        });

                        batch.remaining_qty -= Numeric.parse(batch.get_qty);
                    }

                    batch.remaining_qty = Numeric.format(batch.remaining_qty);

                    break;
                }
            }

            this.datatable.batch.list.value.selected = [];
        },

        validateBatchGetQty(data) {
            let selected = this.getBatchInSelected(data.id);
            let remainingQty = selected != null
                ? Numeric.parse(data.remaining_qty) + Numeric.parse(selected.qty)
                : Numeric.parse(data.remaining_qty);

            if (remainingQty - Numeric.parse(data.get_qty) < 0 || Numeric.parse(data.get_qty) < 0) {
                data.get_qty = 0;
            }
        },

        returnBatches() {
            if (this.datatable.batch.selected.value.selected.length === 0) {
                return false;
            }

            for (var i = this.datatable.batch.selected.value.data.length - 1; i >= 0; i--) {
                let batch = this.datatable.batch.selected.value.data[i];

                if (!Arr.in(batch.id, this.datatable.batch.selected.value.selected)) {
                    continue;
                }

                let base = this.getBatchInList(batch.id);

                base.remaining_qty = Numeric.parse(base.remaining_qty) + Numeric.parse(batch.qty);
                base.remaining_qty = Numeric.format(base.remaining_qty);

                this.datatable.batch.selected.value.data.splice(i, 1);
            }

            this.datatable.batch.selected.value.selected = [];
        },

        getBatchInList(current) {
            let product = this.datatable.details.value.data[this.itemManagementSelection.batch.row].product_id;

            for (let index in this.itemManagement.batches[product]) {
                let batch = this.itemManagement.batches[product][index];

                if (batch.id !== current) {
                    continue;
                }

                return batch;
            }
        },

        getBatchInSelected(current) {
            let batch = null;

            for (let index in this.datatable.batch.selected.value.data) {
                let selected = this.datatable.batch.selected.value.data[index];

                if (current == selected.id) {
                    batch = selected;
                    break;
                }
            }

            return batch;
        },

        isBatchesQtyComplete() {
            let complete = true;
            let errors = [];

            for (let index in this.datatable.details.value.data) {
                let current = this.datatable.details.value.data[index];

                if (current.manage_type === PRODUCT_MANAGE_TYPE.BATCH
                    && Numeric.parse(current.total_qty) !== Arr.sum(Arr.pluck(current.batches, 'qty'))
                ) {
                    errors.push(Lang.t('error.row') + '[' + (Number(index) + 1) + '] ' + Lang.t('validation.batch_count_invalid'));
                    complete = false;
                }
            }

            if (errors.length > 0) {
                this.message.transaction.error.set(errors);
            }

            return complete;
        },

        filterSerials(serials) {
            let selected = [];
            let filtered = [];

            for (let index in this.datatable.details.value.data) {
                selected = selected.concat(Arr.pluck(this.datatable.details.value.data[index].serials, 'id'));
            }

            for (let index in serials) {
                let current = serials[index];

                if (Arr.in(current.id, selected)) {
                    continue;
                }

                filtered.push(current);
            }

            return filtered;
        },

        filterBatches(batches) {
            let selected = [];

            for (let index in this.datatable.details.value.data) {
                selected = selected.concat(this.datatable.details.value.data[index].batches);
            }

            for (let index in batches) {
                let current = batches[index];

                for (let i in selected) {
                    if (current.id === selected[i].id) {
                        batches[index].remaining_qty = Numeric.parse(batches[index].remaining_qty)
                            - Numeric.parse(selected[i].qty);
                    }
                }

                batches[index].remaining_qty = Numeric.format(batches[index].remaining_qty);
            }

            return batches;
        },

        setBatchGetQty(batches) {
            for (let index in batches) {
                batches[index]['get_qty'] = 0;
            }
        },
        
        removeProduct() {
            let data = this.datatable.details.value.data[this.confirm.index];

            delete this.itemManagement.serials[data.product_id];
            delete this.itemManagement.batches[data.product_id];

            this.datatable.details.value.data.splice(this.confirm.index, 1);
            this.confirm.visible = false;
        },

        // Event for warning
        removeProducts() {
            this.datatable.details.value.data = [];
            this.datatable.serial.list.value.data = [];
            this.datatable.serial.selected.value.data = [];
            this.itemManagement.serials = {};
            this.itemManagement.batches = {};

            this.warning.visible = false;
            this.action.value = this.action.buffer;
        },

        createInfo(data = {}) {
            let initial = {
                id: 0,
                sheet_number: '-',
                requested_by: Laravel.auth.user,
                created_for: Laravel.auth.branch,
                for_location: '',
                remarks: '',
                transaction_date: Moment().format(),
                modified_date: '-',
                created_date: '-',
                approval_status: APPROVAL_STATUS.NEW,
                transaction_type: TRANSACTION_TYPE.DIRECT,
                approval_status_label: Lang.t('label.new'),
                total_amount: '',
                created_by: '-',
                modified_by: '-',
                reference_id: null,
                is_deleted: false
            };

            let udfs = this.createUDFs();

            this.info = new Data(Object.assign({}, initial, data, udfs));
        },

        onSuccessfulImportInvoice(response) {
            return this;
        },

        formatImportInvoiceResponseToTransactionDetails(details) {
            return this.formatAttributesToTransactionDetails(details, {}, function(formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0,
                    reference_id: detail.id,
                    invoice_qty: Numeric.format(Numeric.parse(detail.qty) * Numeric.parse(detail.unit_qty))
                });
            });
        },

        formatAttributesToTransactionDetails(details, override = {}, hook = null) {
            let collection = [];

            details = Arr.wrap(details);

            for (let i = 0; i < details.length; i++) {
                let detail = details[i];

                let formatted = {
                    id: detail.hasOwnProperty('id') ? detail.id : 0,
                    manage_type: detail.manage_type,
                    product_id: detail.hasOwnProperty('product_id')  ? detail.product_id : 0,
                    barcode_list: detail.barcode_list,
                    barcode: detail.barcode,
                    name: detail.name,
                    stock_no: detail.stock_no,
                    chinese_name: detail.chinese_name,
                    units: detail.units,
                    unit_id: detail.unit_id,
                    unit_qty: detail.unit_qty,
                    old_unit: detail.hasOwnProperty('old_unit') ? detail.old_unit : true,
                    qty: detail.hasOwnProperty('qty') ? detail.qty : Numeric.format(0),
                    price: detail.price,
                    cost: detail.cost,
                    oprice: detail.hasOwnProperty('oprice') ? detail.oprice : detail.price,
                    discount1: detail.hasOwnProperty('discount1') ? detail.discount1 : Numeric.format(0),
                    discount2: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    discount3: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    discount4: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    remarks: detail.hasOwnProperty('remarks') ? detail.remarks : '',
                    group_type: detail.group_type,
                    components: detail.components,
                    manageable: detail.manageable,
                    product_type: detail.product_type,
                    serials: detail.hasOwnProperty('serials') ? detail.serials : [],
                    batches: detail.hasOwnProperty('batches') ? detail.batches : [],
                    reference_id: detail.hasOwnProperty('reference_id') ? detail.reference_id : null,
                    invoice_qty: detail.hasOwnProperty('invoice_qty') ? detail.invoice_qty : Numeric.format(0),
                    remaining_qty: detail.hasOwnProperty('remaining_qty') ? detail.remaining_qty : Numeric.format(0),
                    spare_qty: detail.hasOwnProperty('spare_qty') ? detail.spare_qty : Numeric.format(0),
                    transactionable_type: detail.hasOwnProperty('transactionable_type') ? detail.transactionable_type : null,
                    transactionable_id: detail.hasOwnProperty('transactionable_id') ? detail.transactionable_id : null,
                    source: detail.hasOwnProperty('source') ? detail.source : TRANSACTION_SOURCE.WEB
                };

                formatted['total_qty'] = Numeric.format(
                    Numeric.parse(formatted['unit_qty']) *
                    Numeric.parse(formatted['qty'])
                );

                formatted['amount'] = Numeric.format(
                    Numeric.parse(formatted['total_qty']) *
                    Numeric.parse(formatted['price'])
                );

                if (hook !== null) {
                    formatted = hook(formatted, detail);
                }

                collection.push(Object.assign({}, formatted, override));
            }

            return collection;
        },

        importTransaction() {
            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.inventoryChain.import(this.importing.referenceNumber, this.info.id)
                .then(function (response) {
                    that.message.transaction.success = response.data.message;
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(that.formatImportResponseToTransactionDetails(response.data.values.details.data));
                    that.processing = false;
                    that.importing.referenceNumber = '';
                    that.onSuccessfulImportTransaction(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        setTotalQty(index) {
            let row = this.datatable.details.value.data[index];

            row['total_qty'] =  Numeric.format(
                Numeric.parse(row['unit_qty']) *
                Numeric.parse(row['qty'])
            );

            this.setAmount(index);
            this.setSpare(index);
        },

        clearSerials() {
            this.itemManagement.serials = {};
            this.itemManagementSelection.serial.reset();
            this.datatable.serial.list.value.data = [];
            this.datatable.serial.list.value.selected = [];
            this.datatable.serial.selected.value.data = [];
            this.datatable.serial.selected.value.selected = [];
        },

        clearBatches() {
            this.itemManagement.batches = {};
            this.itemManagementSelection.batch.reset();
            this.datatable.batch.list.value.data = [];
            this.datatable.batch.list.value.selected = [];
            this.datatable.batch.selected.value.data = [];
            this.datatable.batch.selected.value.selected = [];
        },

        cancelItemManagementForLocationChanges() {
            this.info.for_location = this.buffer.itemManagement.for_location;
            this.buffer.itemManagement.for_location = null;
            this.dialog.removeItemManagementData.visible = false;
        },

        clearItemManagementData() {
            this.clearSerials();
            this.clearBatches();

            for(let index in this.datatable.details.value.data) {
                this.datatable.details.value.data[index].serials = [];
                this.datatable.details.value.data[index].batches = [];
            }

            this.dialog.removeItemManagementData.visible = false;
        },

        removeItemManagementDataWarning() {
            if (!this.hasSerialOrBatch) {
                return;
            }

            this.dialog.removeItemManagementData.visible = true;

            this.$nextTick(() => {
                this.$refs.proceedClearItemManagementData.focus();
            });
        },

        searchSerialSelectionByString(event) {
            this.itemManagementSelection.serial.searchString = event.target.value;
        },

        searchBatchSelectionByString(event) {
            this.itemManagementSelection.batch.searchString = event.target.value;
        },

        scanSerialSelection(event) {
            this.datatable.serial.list.value.selected = [];
            this.itemManagementSelection.serial.message.reset();

            for (let i in this.datatable.serial.list.value.data) {
                let serial = this.datatable.serial.list.value.data[i];

                if (serial.serial_number === event.target.value) {
                    this.datatable.serial.list.value.selected.push(serial.id);
                    break;
                }
            }

            if (this.datatable.serial.list.value.selected.length === 0) {
                this.itemManagementSelection.serial.message.error.set([Lang.t('validation.serial_not_found')]);
                event.target.select();
                return false;
            }

            this.putSerials();

            event.target.value = '';
        },

        closeSerialSelection() {
            this.itemManagementSelection.serial.visible = false;
            this.itemManagementSelection.serial.reset();
            this.datatable.serial.list.value.data = [];
            this.datatable.serial.list.value.selected = [];
            this.datatable.serial.selected.value.data = [];
            this.datatable.serial.selected.value.selected = [];
            this.$refs.serialSelectionSearchString.value = '';
            this.$refs.serialSelectionScan.value = '';
        },

        closeBatchSelection() {
            this.itemManagementSelection.batch.visible = false;
            this.itemManagementSelection.batch.reset();
            this.datatable.batch.list.value.data = [];
            this.datatable.batch.list.value.selected = [];
            this.datatable.batch.selected.value.data = [];
            this.datatable.batch.selected.value.selected = [];
            this.$refs.batchSelectionSearchString.value = '';
        },

        loadCache() {
            this.cache.modal = false;

            if (!this.hasLocalCache(this.info.id)) {
                return false;
            }

            let cached = LocalCache.get(this.cache.module);

            this.reference.invoice = cached.invoices;
            this.info.set(cached.info);
            this.datatable.details.value.data = cached.details;
            this.onCacheLoaded();
        },

        cacheData() {
            let data = Object.assign({}, this.data, {
                invoices: this.reference.invoice
            });

            LocalCache.set(this.cache.module, data);
        },

        getInvoicesForSelector(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.selector.invoice.filter.visible = false;

            let filters = this.getInvoiceSelectorParams();

            this.service.invoice.paginate({
                per_page: this.selector.invoice.datatable.settings.perPage,
                page: page,
                filters: filters
            })
            .then(function (response) {
                that.processing = false;
                that.selector.invoice.datatable.value.data = response.data.values.hasOwnProperty('invoices')
                    ? response.data.values.invoices
                    : response.data.values.records;
                that.selector.invoice.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (response) {
                that.processing = false;
            });
        },

        getInvoiceSelectorParams() {
            return this.selector.invoice.filter.data.concat([
                {
                    value: this.info.created_for,
                    operator: '=',
                    join: 'and',
                    column: 'created_for'
                },
                {
                    value: APPROVAL_STATUS.APPROVED,
                    operator: '=',
                    join: 'and',
                    column: 'approval_status'
                },
                {
                    value: VOIDED.NO,
                    operator: '=',
                    join: 'and',
                    column: 'deleted_at'
                },
                {
                    value: TRANSACTION_STATUS.COMPLETE,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },

                {
                    value: TRANSACTION_STATUS.EXCESS,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },
            ]);
        },

        showInvoiceSelector() {
            this.selector.invoice.visible = true;
            this.getInvoicesForSelector();
        },

        revertInfo() {
            this.info.set(this.buffer.data);
            this.reference.invoice = this.buffer.reference.invoice;
            this.warning.visible = false;
            this.action.reset();
        },

        processAction() {
            switch (this.action.value) {
                case 'process':
                    this.process(this.action.params);
                    break;

                case 'approval':
                    this.approval(this.action.params);
                    break;
            }
        }
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'created-for':
                    this.getLocations();
                    this.reference.invoice = [];
                    break;

                case 'transaction-type':
                    this.info.reference_id = null;
                    this.reference.invoice = [];
                    break;
            }
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.created_for = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.reference.invoice = this.reference.invoice;
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.reference_id = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.transaction_type = this.info.transaction_type;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.transaction_type = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.reference.invoice = this.reference.invoice;
        },
        'datatable.details.value.data': {
            handler: function(data) {
                if (data.length === 0) {
                    this.itemManagement.serials = {};
                    this.itemManagement.batches = {};
                }
            },
            deep: true
        },
        'info.for_location': function(newValue, oldValue) {
            this.buffer.itemManagement.for_location = oldValue;
        },
        'dataCollector.visible': function(value) {
            if(!value) {
                this.dataCollector.referenceNumber = '';
            }
        }
    },

    computed: {
        invoicedQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['invoice_qty']);
            }

            return Numeric.format(total);
        },
        remainingQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['remaining_qty']);
            }

            return Numeric.format(total);
        },
        direct: function() {
            return this.info.transaction_type === TRANSACTION_TYPE.DIRECT;
        },
        fromInvoice: function() {
            return this.info.transaction_type === TRANSACTION_TYPE.FROM_INVOICE;
        },
        availableSerialQty: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagementSelection.serial.row] === 'undefined'
                    ? 0
                    : Math.ceil(this.datatable.details.value.data[this.itemManagementSelection.serial.row].total_qty)
            );
        },
        availableBatchQty: function() {
            return Numeric.format(
                typeof this.datatable.details.value.data[this.itemManagementSelection.batch.row] === 'undefined'
                    ? 0
                    : this.datatable.details.value.data[this.itemManagementSelection.batch.row].total_qty
            );
        },
        selectedSerialCount: function() {
            return Numeric.format(this.datatable.serial.selected.value.data.length);
        },
        selectedBatchCount: function() {
            let count = 0;

            for (var i = 0; i < this.datatable.batch.selected.value.data.length; i++) {
                count += Numeric.parse(this.datatable.batch.selected.value.data[i].qty);
            }

            return Numeric.format(count);
        },

        showBarcodeStockNoInput: function() {
            return !this.disabled && this.controls.create && this.info.transaction_type === TRANSACTION_TYPE.DIRECT;
        },

        directTransaction: function () {
            return TRANSACTION_TYPE.DIRECT;
        },

        fromInvoiceTransaction: function () {
            return TRANSACTION_TYPE.FROM_INVOICE;
        },

        hasSerialOrBatch: function () {
            let count = 0;

            for (let i in this.datatable.details.value.data) {
                let row = this.datatable.details.value.data[i];

                if (row.manage_type !== PRODUCT_MANAGE_TYPE.NONE) {
                    count++;
                    break;
                }
            }

            return count > 0;
        }
    }
}