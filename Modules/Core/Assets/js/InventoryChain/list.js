import InventoryFlow from '../InventoryFlow/list.js';
import QueryStringLocation from "../Classes/QueryStringLocation";

export default {
    mixins: [InventoryFlow],

    data: {
        key: 'records',

        filter: {
            options: [
                {
                    name: Lang.t('label.branch'),
                    columnName: 'created_for',
                    type: 'select',
                    selectOptions: branches,
                    selected: true,
                    default: { value: String(Laravel.auth.branch) },
                    freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                },
                {
                    name: Lang.t('label.for_location'),
                    columnName: 'for_location',
                    type: 'select',
                    selectOptions: locations
                },
                {
                    name: Lang.t('label.request_by'),
                    columnName: 'requested_by',
                    type: 'select',
                    selectOptions: users
                },
                { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string'},
                { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string'},
                {
                    name: Lang.t('label.transaction_date_time'),
                    columnName: 'transaction_date',
                    type: 'datetime',
                    selected: true,
                    default: [
                        { comparison: '>=', value: Moment().startOf('day').format() },
                        { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                    ]
                },
                {
                    name: Lang.t('label.approval_status'),
                    columnName: 'approval_status',
                    type: 'select',
                    selectOptions: [
                        { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                        { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                        { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                        { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                    ]
                },
                {
                    name: Lang.t('label.deleted'),
                    columnName: 'deleted_at',
                    type: 'select',
                    selected: true,
                    selectOptions: [
                        { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                        { id: String(VOIDED.NO), label: Lang.t('label.no') },
                    ],
                    default: { value: String(VOIDED.NO) }
                }
            ],
        },
    },

    created: function() {
        (new QueryStringLocation({
            created_for: 'created_for',
            transaction_date: {
                type: 'datetime',
                column: 'year_month'
            }
        }))
        .setDefaultOnFilters(this.filter.options);
    },
}