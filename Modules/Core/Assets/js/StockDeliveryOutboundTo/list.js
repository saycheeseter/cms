import StockDeliveryOutbound from '../StockDeliveryOutbound/list.js';
import BranchService from '../Services/BranchService';

let app = new Vue({
    el: '#app',

    mixins: [StockDeliveryOutbound],

    data: {
        link: 'stock-delivery-outbound-to'
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.deliver_to'),
            columnName: 'deliver_to',
            type:'doubleSelect',
            secondaryColumn: 'deliver_to_location',
            selectOptions: branches,
            withAll: true,
            withCallback: true,
            callback: function(branch) {
                return BranchService.details(branch);
            }
        });
    }
});
