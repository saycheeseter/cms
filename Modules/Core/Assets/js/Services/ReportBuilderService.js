export default class ReportBuilderService {
    static store(data) {
        return axios.post('/reports/builder', data);
    }

    static update(data, id) {
        return axios.patch('/reports/builder/' + id, data);
    }

    static show(id) {
        return axios.get('/reports/builder/' + id);
    }

    static destroy(id) {
        return axios.delete('/reports/builder/' + id);
    }

    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/builder?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static create() {
        window.location = "/reports/builder/create";
    }

    static edit(id) {
        window.location = '/reports/builder/' + id + '/edit';
    }
}