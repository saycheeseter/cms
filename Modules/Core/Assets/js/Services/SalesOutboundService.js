import InventoryChainService from './InventoryChainService.js';

export default class SalesOutboundService extends InventoryChainService {
    constructor() {
        super();

        this.url = 'sales-outbound';
    }

    transactionsWithBalance(data) {
        return axios.get('/' + this.url + '/transactions/balance', {
            params: {
                branch_id: data.branch_id,
                customer: data.customer,
                customer_type: data.customer_type,
                per_page: data.per_page,
                page: data.page
            }
        });
    }

    fetchTransactions(data) {
        return axios.get('/' + this.url + '/transactions/collectible', {
            params: {
                ids: data.ids
            }
        });
    }

    getByDateAndStatus(params) {
        return axios.get('/' + this.url + '/transaction-dates?page=' + params.page + '&per_page=' + params.per_page, {
            params: {
                date_from: params.date_from,
                date_to: params.date_to,
                statuses: params.statuses
            }
        });
    }

    getBySheetNumberAndStatus(params) {
        return axios.get('/' + this.url + '/sheet-numbers?page=' + params.page + '&per_page=' + params.per_page, {
            params: {
                sheets: params.sheets,
                statuses: params.statuses
            }
        });
    }
}
