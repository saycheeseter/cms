export default class SerialNumberService {
    static findByNumber(number) {
        return axios.get('/serial-number/number/' + number);
    }

    static getTransactions(id) {
        return axios.get('/serial-number/' + id + '/transactions');
    }

    static export(params) {
        window.open('/product/' + params.id + '/serials/export?' + QueryString.stringify(params));
    }
}
