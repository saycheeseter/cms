export default class TemplateService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/custom/'+ args.name +'?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static export(params) {
        window.open('/reports/custom' + '/export?' + QueryString.stringify(params));
    }
}