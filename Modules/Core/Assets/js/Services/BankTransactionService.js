export default class BankTransactionService {
    static store(data) {
        return axios.post('/bank-transaction', data);
    }

    static update(data, id) {
        return axios.patch('/bank-transaction/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/bank-transaction/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/bank-transaction?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }
}