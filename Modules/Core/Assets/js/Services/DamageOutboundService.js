import InventoryChainService from './InventoryChainService.js';

export default class DamageOutboundService extends InventoryChainService {
    constructor() {
        super();
        this.url = 'damage-outbound';
    }
}