export default class PosSalesService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/pos-sales?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static show(id) {
        return axios.get('/pos-sales/' + id);
    }

    static open(filters = []) {
        window.open('/pos-sales?' + QueryString.stringify({
            filters: filters
        }));
    }

    static view(id) {
        window.open('/pos-sales/' + id + '/view');
    }

    static transactions(data) {
        let perPage = (data.perPage === undefined) ? 10 : data.perPage;
        let page = (data.page === undefined) ? 1 : data.page;
        let filters = (data.filters === undefined) ? '' : data.filters;

        return axios.get('/pos-sales/product/transactions' + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters,
                product_id: data.product_id
            }
        });
    }
}
