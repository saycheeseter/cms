export default class CheckService {
    paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/' + this.url + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    transfer(data) {
        return axios.get('/' + this.url + '/transfer', {
            params: {
                ids: data.ids,
                transfer_date: data.transfer_date
            }
        })
    }

    redirect(url, id) {
        window.location = '/' + url + '/' + id + '/edit';
    }
}