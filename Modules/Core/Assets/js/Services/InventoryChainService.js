import TransactionService from './TransactionService.js';

export default class InventoryChainService extends TransactionService {
    transactions(data) {
        let perPage = (data.perPage === undefined) ? 10 : data.perPage;
        let page = (data.page === undefined) ? 1 : data.page;
        let filters = (data.filters === undefined) ? '' : data.filters;

        return axios.get('/' + this.url + '/product/transactions' + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: data.filters,
                product_id: data.product_id
            }
        });
    }
}