import InvoiceService from './InvoiceService.js';

let instance = null;

export default class StockDeliveryService extends InvoiceService {
    constructor() {
        this.url = 'stock-delivery';
    }

    instance() {
        if(!instance){
            instance = new StockDeliveryService;
        }

        return instance;
    }
}
