export default class DiscountSummaryReportService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/pos/discounted-summary?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }
}