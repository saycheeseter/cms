export default class LanguageService {
    static switch(lang) {
        return axios.get('/lang/' + lang);
    }
}
