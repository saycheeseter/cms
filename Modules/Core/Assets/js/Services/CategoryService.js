export default class CategoryService {
    static load() {
        return axios.get('/category');
    }

    static store(data) {
        return axios.post('/category', data);
    }

    static update(data, id) {
        return axios.patch('/category/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/category/' + id);
    }

    static paginate(args = {}) {
        let filters = (args.filters === undefined) ? '' : args.filters;
        let parent_id = (args.parent_id === undefined) ? '' : args.parent_id;

        return axios.get('/category', {
            params: {
                filters: filters,
                parent_id: parent_id
            }
        });
    }

    static findMany(ids) {
        return axios.get('/category/m/', {
            params: {
                ids: ids,
            }
        });
    }
}