import BalanceFlowService from './BalanceFlowService.js';

export default class CustomerBalanceFlowService extends BalanceFlowService {
    constructor() {
        this.url = 'customer';
    }
}