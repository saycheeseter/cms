import InvoiceService from './InvoiceService.js';

export default class StockReturnService extends InvoiceService {
    constructor() {
        this.url = 'stock-return';
    }
}
