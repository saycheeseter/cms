import InventoryChainService from './InventoryChainService.js';

export default class StockReturnOutboundService extends InventoryChainService {
    constructor() {
        this.url = 'stock-return-outbound';
    }

    retrieve(data) {
        return axios.get('/' + this.url + '/retrieve', {
            params: {
                id: data
            }
        });
    }

    remaining(data) {
        return axios.get('/' + this.url + '/remaining', {
            params: {
                sheet_number: data.sheet_number,
                branch: data.branch
            }
        });
    }

    serials(id) {
        return axios.get('/' + this.url + '/detail/' + id + '/serials');
    }

    batches(id) {
        return axios.get('/' + this.url + '/detail/' + id + '/batches');
    }
}
