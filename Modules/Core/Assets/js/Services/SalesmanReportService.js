export default class SalesmanReportService {
    static products(args = {}) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/salesman/products', {
            params: {
                filters: filters
            }
        });
    }

    static transactions(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/salesman/transactions/?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

}
