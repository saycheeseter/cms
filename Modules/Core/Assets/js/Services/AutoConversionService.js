import SalesOutboundService from "./SalesOutboundService";
import SalesReturnInboundService from "./SalesReturnInboundService";
import PosSalesService from "./PosSalesService";

export default class AutoConversionService {
    constructor() {
        this.url = 'auto-product-conversion';
    }

    getTransactions(data) {
        let perPage = (data.perPage === undefined) ? 10 : data.perPage;
        let page = (data.page === undefined) ? 1 : data.page;
        let filters = (data.filters === undefined) ? '' : data.filters;

        return axios.get('/' + this.url + '/product/transactions' + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: data.filters,
                product_id: data.product_id,
                type: data.type
            }
        });
    }

    edit(id, module) {
        switch (module) {
            case MODULE.SALES_OUTBOUND:
                return (new SalesOutboundService).edit(id);
                break;

            case MODULE.SALES_RETURN_INBOUND:
                return (new SalesReturnInboundService).edit(id);
                break;

            case MODULE.POS:
                return PosSalesService.view(id);
                break;
        }
    }
}