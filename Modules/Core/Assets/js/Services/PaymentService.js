import FinancialFlowService from './FinancialFlowService.js';

export default class PaymentService extends FinancialFlowService {
    constructor() {
        super();

        this.url = 'payment';
    }
}