export default class ProductDiscountService {
    static show(id) {
        return axios.get('/product-discount/' + id);
    }

    static store(data) {
        return axios.post('/product-discount', data);
    }

    static update(data, id) {
        return axios.patch('/product-discount/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/product-discount/' + id);
    }

    static edit(id) {
        window.open('/product-discount/' + id + '/edit');
    }

    static redirect(id) {
        window.location = '/product-discount/' + id + '/edit';
    }

    static create() {
        window.location = '/product-discount/create';
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/product-discount?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }
}