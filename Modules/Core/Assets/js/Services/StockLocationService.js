export default class SystemCodeService
{
    static store(data)
    {
        return axios.post('/stock-location', data);
    }

    static update(data, id)
    {
        return axios.patch('/stock-location' + '/' + id, data);
    }

    static destroy(id)
    {
        return axios.delete('/stock-location' + '/' + id);
    }

    static paginate(args = {})
    {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;
        let branch_detail_id = (args.branch_detail_id === undefined) ? '' : args.branch_detail_id;

        return axios.get('/stock-location?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters,
                branch_detail_id: branch_detail_id
            }
        });
    }
}
