export default class PrintoutTemplateService {
    static store(data) {
        return axios.post('/printout-template', data);
    }

    static update(data, id) {
        return axios.patch('/printout-template/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/printout-template/' + id);
    }

    static create() {
        window.location = '/printout-template/create';
    }

    static show(id) {
        return axios.get('/printout-template/' + id);
    }
    
    static edit(id) {
        window.open('/printout-template/' + id + '/edit', '_blank');
    }

    static redirect(id) {
        window.location = '/printout-template/' + id + '/edit';
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/printout-template?page=' + page + '&per_page=' + perPage, {
            params: {
                filters: filters
            },
        });
    }
}
