export default class MemberService {

    static store(data) {
        return axios.post('/member', data);
    }

    static update(data, id) {
        return axios.patch('/member/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/member/' + id);
    }

    static show(id) {
        return axios.get('/member/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/member?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }

    static findMany(ids) {
        return axios.get('/member/m/', {
            params: {
                ids: ids,
            }
        });
    }

    static importExcel(attachment) {
        let form = new FormData;

        form.append('attachment', attachment);

        return axios.post('/member/excel/import',
            form,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
    }

    static downloadExcelTemplate() {
        window.open('/member/excel/import/template');
    }

}
