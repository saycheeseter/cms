export default class BankAccountService {
    static store(data) {
        return axios.post('/bank-account', data);
    }

    static update(data, id) {
        return axios.patch('/bank-account/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/bank-account/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/bank-account?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }
}
