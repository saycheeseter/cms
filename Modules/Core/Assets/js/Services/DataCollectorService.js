export default class DataCollectorService {
    static findByReferenceNumber(data) {
        return axios.get('/data-collector/reference-number', {
            params: {
                reference_number: data.reference_number,
                transaction_type: data.transaction_type
            }
        });
    }

    static destroyByReference(reference = '') {
        return axios.delete('/data-collector/reference-number/' + reference);
    }

    static destroy(id) {
        return axios.delete('/data-collector/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? 10 : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/data-collector?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }
}