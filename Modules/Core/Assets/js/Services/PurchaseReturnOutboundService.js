import InventoryChainService from './InventoryChainService.js';

export default class PurchaseReturnOutboundService extends InventoryChainService {
    constructor() {
        this.url = 'purchase-return-outbound';
    }

    transactionsWithBalance(data) {
        return axios.get('/' + this.url + '/transactions/balance', {
            params: {
                branch_id: data.branch_id,
                supplier_id: data.supplier_id,
                per_page: data.per_page,
                page: data.page
            }
        });
    }

    fetchTransactions(data) {
        return axios.get('/' + this.url + '/transactions/payable', {
            params: {
                ids: data.ids
            }
        });
    }
}
