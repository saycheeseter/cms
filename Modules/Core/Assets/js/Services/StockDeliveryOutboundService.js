import InventoryChainService from './InventoryChainService.js';

export default class StockDeliveryOutboundService extends InventoryChainService {
    constructor() {
        this.url = 'stock-delivery-outbound';
    }

    view(id) {
        window.open('/' + this.url + '-from/' + id + '/view', '_blank');
    }

    retrieve(data) {
        return axios.get('/' + this.url + '/retrieve', {
            params: {
                id: data
            }
        });
    }

    remaining(data) {
        return axios.get('/' + this.url + '/remaining', {
            params: {
                sheet_number: data.sheet_number,
                branch: data.branch
            }
        });
    }

    warning(data) {
        return axios.post('/' + this.url + '/warning' , data);
    }

    create(reload = false) {
        if(reload) {
            window.location = '/' + this.url + '-to/create';
        } else {
            window.open('/'+ this.url + '-to/create');
        }
    }

    edit(id) {
        window.open('/' + this.url + '-to/' + id + '/edit', '_blank');
    }
    
    serials(id) {
        return axios.get('/' + this.url + '/detail/' + id + '/serials');
    }

    redirect(id) {
        window.location = '/' + this.url + '-to/' + id + '/edit';
    }

    batches(id) {
        return axios.get('/' + this.url + '/detail/' + id + '/batches');
    }

    show(id, enabled = true) {
        return axios.get('/' + this.url + '/' + id + '?enabled=' + enabled);
    }
}