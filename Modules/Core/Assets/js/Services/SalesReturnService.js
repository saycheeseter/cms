import InvoiceService from './InvoiceService.js';

export default class SalesReturnService extends InvoiceService {
    constructor() {
        this.url = 'sales-return';
    }
}
