export default class MemberReportService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/member/report?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }

    static export(params) {
        window.open('/reports/member/report/export?' + QueryString.stringify(params));
    }
}