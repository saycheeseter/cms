export default class SupplierProductPriceService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;
        let products = (args.products === undefined) ? '' : args.products;

        return axios.get('/reports/supplier/product-price?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters,
                products: products,
            },
        });
    }
}
