export default class PosSalesReportService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;
        let terminals = (args.terminals === undefined) ? '' : args.terminals;

        return axios.get('/reports/pos-sales?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters,
                terminals: terminals
            }
        });
    }
}