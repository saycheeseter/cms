export default class MemberrateService {

    static show(id) {
        return axios.get('/member-rate/' + id);
    }

    static store(data) {
        return axios.post('/member-rate', data);
    }

    static update(data, id) {
        return axios.patch('/member-rate/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/member-rate/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/member-rate?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static findMany(ids) {
        return axios.get('/member-rate/m/', {
            params: {
                ids: ids,
            }
        });
    }
}
