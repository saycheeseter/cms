import AutoConversionService from "./AutoConversionService";

export default class AutoConversionIncreaseService extends AutoConversionService{
    transactions(data) {
        data['type'] = 1;

        return this.getTransactions(data);
    }
}