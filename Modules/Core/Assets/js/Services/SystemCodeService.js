export default class SystemCodeService {
    constructor(name) {
        this.module = name;
    }

    store(data) {
        return axios.post('/' + this.module, data);
    }

    update(data, id) {
        return axios.patch('/'+ this.module + '/' + id, data);
    }

    destroy(id) {
        return axios.delete('/'+ this.module + '/' + id);
    }

    paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/'+ this.module +'?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    findMany(ids) {
        return axios.get('/' + this.module + '/m/', {
            params: {
                ids: ids,
            }
        });
    }

    importExcel(attachment) {
        let form = new FormData;

        form.append('attachment', attachment);

        return axios.post('/' + this.module + '/excel/import',
            form,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
    }

    downloadExcelTemplate() {
        window.open('/' + this.module + '/excel/import/template');
    }

}