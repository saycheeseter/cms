import TransactionService from './TransactionService.js';

export default class InventoryAdjustService extends TransactionService {
    constructor() {
        super();
        this.url = 'inventory-adjust';
    }

    checkInventoryDiscrepancy(data) {
        return axios.post('/' + this.url + '/check-inventory', data);
    }

    getTransactions(data) {
        let perPage = (data.perPage === undefined) ? 10 : data.perPage;
        let page = (data.page === undefined) ? 1 : data.page;
        let filters = (data.filters === undefined) ? '' : data.filters;

        return axios.get('/' + this.url + '/product/transactions' + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: data.filters,
                product_id: data.product_id,
                type: data.type
            }
        });
    }
}