import TransactionService from './TransactionService.js';

export default class InvoiceService extends TransactionService {
    retrieve(id) {
        return axios.get('/' + this.url + '/retrieve', {
            params: {
                id: id
            }
        });
    }

    remaining(data) {
        return axios.get('/' + this.url + '/remaining', {
            params: {
                sheet_number: data.sheet_number,
                branch: data.branch
            }
        });
    }
}