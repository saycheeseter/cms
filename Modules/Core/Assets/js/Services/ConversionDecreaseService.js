import ProductConversionService from './ProductConversionService.js';

export default class ConversionDecreaseService extends ProductConversionService {
    transactions(data) {
        data['type'] = 2;

        return this.getTransactions(data);
    }
}