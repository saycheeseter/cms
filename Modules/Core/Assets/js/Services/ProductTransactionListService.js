export default class ProductTransactionListService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let basicFilters = (args.basicFilters === undefined) ? '' : args.basicFilters;
        let advancedFilters = (args.advancedFilters === undefined) ? '' : args.advancedFilters;

        return axios.get('/reports/product/transaction-list?page=' + page + '&per_page=' + perPage , {
            params: {
                basicFilters: basicFilters,
                advancedFilters: advancedFilters
            }
        });
    }

    static export(params) {
        window.open('/reports/product/transaction-list/export?' + QueryString.stringify(params));
    }
}