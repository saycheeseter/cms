export default class UserMenuService {
    static save(data) {
        return axios.post('/user/my-menu/save', data);
    }

    static get() {
        return axios.get('/user/my-menu');
    }

    static set(type) {
        return axios.get('/user/my-menu/set/'+ type);
    }
}