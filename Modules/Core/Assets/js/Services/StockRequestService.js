import InvoiceService from './InvoiceService.js';

export default class StockRequestService extends InvoiceService {
    constructor() {
        this.url = 'stock-request';
    }

    view(id) {
        window.open('/' + this.url + '-from/' + id + '/view', '_blank');
    }

    create(reload = false) {
        if(reload) {
            window.location = '/' + this.url + '-to/create';
        } else {
            window.open('/'+ this.url + '-to/create');
        }
    }

    edit(id) {
        window.open('/' + this.url + '-to/' + id + '/edit', '_blank');
    }

    redirect(id) {
        window.location = '/' + this.url + '-to/' + id + '/edit';
    }

    warning(data) {
        return axios.post('/' + this.url + '/warning', data);
    }

    show(id, enabled = true) {
        return axios.get('/' + this.url + '/' + id + '?enabled=' + enabled);
    }
}
