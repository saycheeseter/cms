export default class IncomeStatementService {
    static fetch(args) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/income-statement', {
            params: {
                filters: filters
            }
        });
    }
}