export default class OtherIncomeService {

    static show(id) {
        return axios.get('/other-income/' + id);
    }

    static store(data) {
        return axios.post('/other-income', data);
    }

    static update(data, id) {
        return axios.patch('/other-income/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/other-income/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/other-income?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static view(id) {
        window.open('/other-income/' + id + '/view');
    }

    static open(filters = []) {
        window.open('/other-income?' + QueryString.stringify({
            filters: filters
        }));
    }
}
