export default class ProductPeriodicSalesService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/product/periodic-sales?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static export(params) {
        window.open('/reports/product/periodic-sales/export?' + QueryString.stringify(params));
    }
}