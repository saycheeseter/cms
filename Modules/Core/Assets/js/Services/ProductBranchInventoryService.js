export default class ProductBranchInventoryService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/product/branch-inventory?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static export(params) {
        window.open('/reports/product/branch-inventory/export?' + QueryString.stringify(params));
    }
}