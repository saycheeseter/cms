export default class BankTransactionReportService {
    static search(args) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/bank-transaction', {
            params: {
                filters: filters
            }
        });
    }

}