import * as QueryString from "qs";

let instance = null;

export default class TransactionService {
    constructor() {
        this.url = '';
    }

    static instance() {
        if(!instance){
            instance = new this;
        }

        return instance;
    }
    
    store(data) {
        return axios.post('/' + this.url + '', data);
    }

    update(data, id) {
        return axios.patch('/' + this.url + '/' + id, data);
    }

    updateApproved(data, id) {
        return axios.patch('/' + this.url + '/' + id + '/approved', data);
    }

    destroy(id) {
        return axios.delete('/' + this.url + '/' + id);
    }

    show(id) {
        return axios.get('/' + this.url + '/' + id);
    }

    create(reload = false) {
        if(reload) {
            window.location = '/' + this.url + '/create';
        } else {
            window.open('/'+ this.url + '/create');
        }
    }

    edit(id) {
        window.open('/' + this.url + '/' + id + '/edit', '_blank');
    }

    view(id) {
        window.open('/' + this.url + '/' + id + '/view', '_blank');
    }

    approval(data) {
        return axios.post('/' + this.url + '/approval', data);
    }

    import(data = '', id = 0) {
        return axios.get('/' + this.url + '/import', {
            params: {
                reference_number: data,
                id: id
            }
        });
    }

    // importByRangeAndStatuses(params = {}) {
    //     return axios.get('/' + this.url + '/import-by-range-and-statuses', {
    //         params: {
    //             params: params
    //         }
    //     })
    // }

    print(data) {
        window.open('/' + this.url + '/' + data.id + '/print/' + data.template + '/template');
    }

    redirect(id) {
        window.location = '/' + this.url + '/' + id + '/edit';
    }

    export(params) {
        window.open(this.url + '/export?' + QueryString.stringify(params));
    }

    paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/' + this.url + '?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    open(filters = []) {
        window.open('/' + this.url + '?' + QueryString.stringify({
            filters: filters
        }));
    }
}