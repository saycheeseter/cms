export default class AuthService {
    static login(data) {
        return axios.post('/', data);
    }

    static authenticate(data) {
        return axios.post('/auth', data);
    }
}