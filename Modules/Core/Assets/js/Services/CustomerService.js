export default class CustomerService {
    static store(data) {
        return axios.post('/customer', data);
    }

    static update(data, id) {
        return axios.patch('/customer/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/customer/' + id);
    }

    static show(id) {
        return axios.get('/customer/' + id);
    }

    static details(id) {
        return axios.get('/customer/' + id + '/details')
    }

    static balance(id) {
        return axios.get('/customer/' + id + '/balance'); 
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/customer?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static findMany(ids) {
        return axios.get('/customer/m/', {
            params: {
                ids: ids,
            }
        });
    }
}
