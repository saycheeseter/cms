export default class ExcelTemplateService {

    static create() {
        return window.location = 'excel-template/create';
    }

    static view(id) {
        window.location = 'excel-template/' + id + '/edit';
    }

    static store(data) {
        return axios.post('/excel-template', data);
    }

    static update(data, id) {
        return axios.patch('/excel-template/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/excel-template/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/excel-template?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static edit(id){
        return axios.get('/excel-template/' + id + '/edit');
    }

    static show(id) {
        return axios.get('/excel-template/' + id);
    }

    static udf(module) {
        return axios.get('/excel-template/udf/' + module);
    }

    static redirect(id) {
        window.location = '/excel-template/' + id + '/edit';
    }
    
    static preview(params) {
            window.open('/excel-template/preview?' + QueryString.stringify(params));
    }
}
