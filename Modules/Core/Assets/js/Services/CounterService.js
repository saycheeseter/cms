export default class CounterService {
    static store(data) {
        return axios.post('/counter', data);
    }

    static update(data, id) {
        return axios.patch('/counter/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/counter/' + id);
    }

    static show(id) {
        return axios.get('/counter/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/counter/?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static redirect(id) {
        window.location = '/counter/' + id + '/edit';
    }

    static create() {
        window.location = "counter/create";
    }

    static export(params) {
        window.open('counter/export?' + QueryString.stringify(params));
    }

    static print(data) {
        window.open('/counter/' + data.id + '/print/' + data.template + '/template');
    }
}