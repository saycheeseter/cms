import BalanceFlowService from './BalanceFlowService.js';

export default class SupplierBalanceFlowService extends BalanceFlowService {
    constructor() {
        this.url = 'supplier';
    }
}