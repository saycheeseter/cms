export default class AttachmentService {
    static temporary(data, type) {
        return axios.post('/attachment/temporary/' + type, data, {
            headers : { 'Content-Type': 'multipart/form-data' }
        });
    }

    static attach(data, id, module, type) {
        return axios.post('/attachment/'+ module +'/'+ type +'/'+ id +'/attach', data, {
            headers : { 'Content-Type': 'multipart/form-data' }
        });
    }

    static update(data, id, module, type) {
        return axios.post('/attachment/'+ module +'/'+ type +'/'+ id +'/update', data, {
            headers : { 'Content-Type': 'multipart/form-data' }
        });
    }

    static detach(id, module, key, status) {
        return axios.post('/attachment/'+ key +'/detach', {
            entity_id: id,
            module: module,
            status: status
        });
    }

    static getByProduct(id) {
        return axios.get('/attachment/product/' + id);
    }
}