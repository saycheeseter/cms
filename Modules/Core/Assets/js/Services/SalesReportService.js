export default class SalesReportService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/sales/detail-report?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static summary(args) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/sales/detail-report-summary', {
            params: {
                filters: filters
            }
        });
    }

    static export(params) {
        window.open('/reports/sales/detail-report/export?' + QueryString.stringify(params));
    }
}