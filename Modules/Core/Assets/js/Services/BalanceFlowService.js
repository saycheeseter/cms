export default class BalanceFlowService {
    paginate(args = {}) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/' + this.url + '/balance', {
            params: {
                filters: filters
            }
        });
    }
}