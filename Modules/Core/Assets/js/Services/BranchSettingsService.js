export default class BranchSettingsService {
    static fetch(data) {
        return axios.get('/settings/branch');
    }

    static store(data) {
        return axios.post('/settings/branch', data);
    }

    static update(data) {
        return axios.patch('/settings/branch', data);
    }
}
