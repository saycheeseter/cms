import ProductConversionService from './ProductConversionService.js';

export default class ConversionIncreaseService extends ProductConversionService {
    transactions(data) {
        data['type'] = 1;

        return this.getTransactions(data);
    }
}