import InvoiceService from './InvoiceService.js';

export default class PurchaseReturnService extends InvoiceService {
    constructor() {
        this.url = 'purchase-return';
    }
}
