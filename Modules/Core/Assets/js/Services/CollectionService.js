import FinancialFlowService from './FinancialFlowService.js';

export default class CollectionService extends FinancialFlowService {
    constructor() {
        super();

        this.url = 'collection';
    }
}