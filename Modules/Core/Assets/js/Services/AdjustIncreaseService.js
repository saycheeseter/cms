import InventoryAdjustService from './InventoryAdjustService.js';

export default class AdjustIncreaseService extends InventoryAdjustService {
    transactions(data) {
        data['type'] = 1;

        return this.getTransactions(data);
    }
}