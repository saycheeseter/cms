export default class UserService {

    static store(data) {
        return axios.post('/user', data);
    }

    static update(data, id) {
        return axios.patch('/user/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/user/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/user?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static show(id){
        return axios.get('/user/' + id + '');
    }

    static create() {
        window.location = '/user/create';
    }

    static edit(id) {
        window.location = '/user/' + id + '/edit';
    }

    static changePassword(data) {
        let password = {
            old_password: data.old,
            new_password: data.new,
            new_password_confirmation: data.new_confirmation
        };

        return axios.patch('/user/change-password', password);
    }

    static changeAdminPassword(id, password) {
        let data = {
            password: password
        }
        return axios.patch('/user/' + id + '/change-admin-password', data);
    }
}
