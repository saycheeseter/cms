export default class PosSummaryPostingService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;

        return axios.get('/pos-summary-posting?page=' + page + '&per_page=' + perPage);
    }

    static generateSummary(id, data) {
        return axios.patch('/pos-summary-posting/' + id + '/generate', data);
    }
}