export default class BranchService {

    static store(data) {
        return axios.post('/branch', data);
    }

    static update(data, id) {
        return axios.patch('/branch/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/branch/' + id);
    }

    static show(id) {
        return axios.get('/branch/' + id);
    }

    static details(id) {
        return axios.get('/branch/' + id + '/details');
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/branch/?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }
}
