import InventoryChainService from './InventoryChainService.js';

export default class SalesReturnInboundService extends InventoryChainService {
    constructor() {
        super();

        this.url = 'sales-return-inbound';
    }

    transactionsWithBalance(data) {
        return axios.get('/' + this.url + '/transactions/balance', {
            params: {
                branch_id: data.branch_id,
                customer: data.customer,
                customer_type: data.customer_type,
                per_page: data.per_page,
                page: data.page
            }
        });
    }

    fetchTransactions(data) {
        return axios.get('/' + this.url + '/transactions/collectible', {
            params: {
                ids: data.ids
            }
        });
    }
}
