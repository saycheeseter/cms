import InvoiceService from './InvoiceService.js';

export default class DamageService extends InvoiceService {
    constructor() {
        this.url = 'damage';
    }
}
