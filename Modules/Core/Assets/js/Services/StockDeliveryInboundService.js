import InventoryChainService from './InventoryChainService.js';

export default class StockDeliveryInboundService extends InventoryChainService {
    constructor() {
        this.url = 'stock-delivery-inbound';
    }
}