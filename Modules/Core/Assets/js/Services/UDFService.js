export default class UDFService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let module = (args.module === undefined) ? null : args.module;

        return axios.get('/udf?page=' + page + '&per_page=' + perPage , {
            params: {
                module: module
            }
        });
    }

    static store(data) {
        return axios.post('/udf', data);
    }

    static update(data, id) {
        return axios.patch('/udf/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/udf/' + id);
    }

    static byModule(id) {
        return axios.get('/udf/module/' + id);
    }
}
