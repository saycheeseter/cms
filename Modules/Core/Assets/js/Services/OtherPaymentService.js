export default class OtherPaymentService {
    static show(id) {
        return axios.get('/other-payment/' + id);
    }

    static store(data) {
        return axios.post('/other-payment', data);
    }

    static update(data, id) {
        return axios.patch('/other-payment/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/other-payment/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/other-payment?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static view(id) {
        window.open('/other-payment/' + id + '/view');
    }

    static open(filters = []) {
        window.open('/other-payment?' + QueryString.stringify({
            filters: filters
        }));
    }
}
