export default class RoleService {
    static store(data) {
        return axios.post('/role', data);
    }

    static update(data, id) {
        return axios.patch('/role/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/role/' + id);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/role?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static show(id){
        return axios.get('/role/' + id + '');
    }

    static create() {
        window.location = '/role/create';
    }

    static edit(id) {
        window.location = '/role/' + id + '/edit';
    }

    static permissions(id) {
        return axios.get('/role/' + id + '/permissions');
    }
}