export default class TerminalService {
    static getByBranch(branch){
        return axios.get('/terminals/branch/' + branch);
    }
}