import TransactionService from './TransactionService.js';

export default class FinancialFlowService extends TransactionService {
    redirect(id) {
        window.location = '/' + this.url + "/" + id + "/edit";
    }

    create(reload = false) {
        if(reload) {
            window.location = '/' + this.url + '/create';
        } else {
            window.open('/'+ this.url + '/create');
        }
    }

    edit(id) {
        window.open('/' + this.url + "/" + id + "/edit");
    }

    view(id) {
        window.open('/' + this.url + "/" + id + "/edit");
    }
}