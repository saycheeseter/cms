export default class TransactionSummaryPostingService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;

        return axios.get('/transaction-summary-posting?page=' + page + '&per_page=' + perPage);
    }

    static generateSummary(data) {
        return axios.post('/transaction-summary-posting/generate', data);
    }
}