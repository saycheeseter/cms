export default class GiftCheckService {

    static store(data) {
        return axios.post('/gift-check', data);
    }

    static update(data, id) {
        return axios.patch('/gift-check/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/gift-check/' + id);
    }

    static show(id) {
        return axios.get('/gift-check/' + id);
    }

    static bulkEntry(data) {
        return axios.post('/gift-check/bulk', data);
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/gift-check?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }
}
