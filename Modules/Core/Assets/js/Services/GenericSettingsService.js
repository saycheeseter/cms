export default class GenericSettingsService {
    static fetch(data) {
        return axios.get('/settings/generic');
    }

    static store(data) {
        return axios.post('/settings/generic', data);
    }

    static update(data) {
        return axios.patch('/settings/generic', data);
    }
}