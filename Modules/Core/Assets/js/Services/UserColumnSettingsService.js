export default class UserColumnSettingsService {
    static getByModule(module) {
        return axios.get('/user-column-settings/' + module);
    }

    static save(module, data) {
        return axios.post('/user-column-settings/' + module, {
            settings: data
        });
    }
}