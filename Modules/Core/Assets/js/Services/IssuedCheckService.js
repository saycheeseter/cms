import CheckService from './CheckService.js';

export default class IssuedCheckService extends CheckService {
    constructor() {
        super();

        this.url = 'issued-check';
    }
}