export default class CollectionDueWarningService {
    static paginate(args) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;
        let all = (args.all === undefined) ? false : args.all;

        return axios.get('/reports/collection/due-warning?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters,
                all: all
            }
        });
    }

    static summary(args) {
        let filters = (args.filters === undefined) ? '' : args.filters;
        let all = (args.all === undefined) ? false : args.all;

        return axios.get('/reports/collection/due-warning-summary', {
            params: {
                filters: filters,
                all: all
            }
        });
    }
}