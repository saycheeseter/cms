import InvoiceService from './InvoiceService.js';

export default class PurchaseService extends InvoiceService {
    constructor() {
        this.url = 'purchase-order';
    }

    createFromStockRequestBySupplier(data) {
        return axios.post('/' + this.url + '/from-stock-request/create', data);
    }
}
