import InventoryChainService from './InventoryChainService.js';

export default class StockReturnInboundService extends InventoryChainService {
    constructor() {
        this.url = 'stock-return-inbound';
    }
}
