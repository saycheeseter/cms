export default class SupplierService {
    static store(data) {
        return axios.post('/supplier', data);
    }

    static update(data, id) {
        return axios.patch('/supplier/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/supplier/' + id);
    }

    static show(id) {
        return axios.get('/supplier/' + id);
    }

    static balance(id) {
        return axios.get('/supplier/' + id + '/balance'); 
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/supplier?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static findMany(ids) {
        return axios.get('/supplier/m/', {
            params: {
                ids: ids,
            }
        });
    }
}
