export default class ProductInventoryValueService {
    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/product/inventory-value?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            },
        });
    }

    static getSummary(filters) {
        
        return axios.get('/reports/product/inventory-value-summary', {
            params: {
                filters: filters
            },
        });
    }
}
