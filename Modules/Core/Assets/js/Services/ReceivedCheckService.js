import CheckService from './CheckService.js';

export default class ReceivedCheckService extends CheckService {
    constructor() {
        this.url = 'received-check';
    }
}