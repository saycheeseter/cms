export default class CashTransactionService {
    static search(args) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/cash-transaction', {
            params: {
                filters: filters
            }
        });
    }

}