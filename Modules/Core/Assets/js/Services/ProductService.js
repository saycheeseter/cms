export default class ProductService {
    static load(id) {
        return axios.get('/product/' + id);
    }

    static store(data) {
        return axios.post('/product', data);
    }

    static update(data, id) {
        return axios.patch('/product/' + id, data);
    }

    static destroy(id) {
        return axios.delete('/product/' + id);
    }

    static edit(id) {
        window.open('/product/' + id + '/edit');
    }

    static create(reload) {
        if (reload) {
            window.location = '/product/create';
        } else {
            window.open('/product/create');
        }
    }

    static redirect(id) {
        window.location = '/product/' + id + '/edit';
    }

    static searchProductByName(args = {}) {
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/product/name' , {
            params: {
                filters: filters
            }
        });
    }

    static paginate(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/product?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static selectWithSupplierPrice(data) {
        return axios.get('/product/select/supplier/price', {
            params: {
                product_ids: data.product_ids,
                supplier_id: data.supplier_id,
                branch_id: data.branch_id,
                location_id: data.location_id
            }
        });
    }

    static selectWithCustomerPrice(data) {
        return axios.get('/product/select/customer/price', {
            params: {
                product_ids: data.product_ids,
                branch_id: data.branch_id,
                location_id: data.location_id,
                customer_type: data.customer_type,
                customer_id: data.customer_id
            }
        });
    }

    static selectWithSummaryAndWholesalePrice(data) {
        return ProductService.selectWithSummaryAndPrice({
            product_ids: data.product_ids,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.WHOLESALE
        });
    }

    static selectWithSummaryAndSellingPrice(data) {
        return ProductService.selectWithSummaryAndPrice({
            product_ids: data.product_ids,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.SELLING
        });
    }

    static selectWithSummaryAndPurchasePrice(data) {
        return ProductService.selectWithSummaryAndPrice({
            product_ids: data.product_ids,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.PURCHASE
        });
    }

    static selectWithSummaryAndPrice(data) {
        return axios.get('/product/select/summary/price', {
            params: {
                product_ids: data.product_ids,
                branch_id: data.branch_id,
                location_id: data.location_id,
                price: data.price
            }
        });
    }

    static findByBarcodeWithSummaryAndPrice(data) {
        return axios.get('/product/barcode', {
            params: {
                barcode: data.barcode,
                branch_id: data.branch_id,
                location_id: data.location_id,
                price: data.price,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.DEFAULT
            }
        });
    }

    static findByBarcodeWithSummaryAndPurchasePrice(data) {
        return ProductService.findByBarcodeWithSummaryAndPrice({
            barcode: data.barcode,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.PURCHASE,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByBarcodeWithSummaryAndWholesalePrice(data) {
        return ProductService.findByBarcodeWithSummaryAndPrice({
            barcode: data.barcode,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.WHOLESALE,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByBarcodeWithSummaryAndSellingPrice(data) {
        return ProductService.findByBarcodeWithSummaryAndPrice({
            barcode: data.barcode,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.SELLING,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByBarcodeWithSummaryAndSupplierPrice(data) {
        return axios.get('/product/barcode', {
            params: {
                barcode: data.barcode,
                supplier_id: data.supplier_id,
                branch_id: data.branch_id,
                location_id: data.location_id,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.SUPPLIER
            }
        });
    }

    static findByBarcodeWithSummaryAndCustomerPrice(data) {
        return axios.get('/product/barcode', {
            params: {
                barcode: data.barcode,
                customer_type: data.customer_type,
                customer_id: data.customer_id,
                branch_id: data.branch_id,
                location_id: data.location_id,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.CUSTOMER
            }
        });
    }

    static findByStockNoWithSummaryAndPrice(data) {
        return axios.get('/product/stock-no', {
            params: {
                stock_no: data.stock_no,
                branch_id: data.branch_id,
                location_id: data.location_id,
                price: data.price,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.DEFAULT
            }
        });
    }

    static findByStockNoWithSummaryAndPurchasePrice(data) {
        return ProductService.findByStockNoWithSummaryAndPrice({
            stock_no: data.stock_no,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.PURCHASE,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByStockNoWithSummaryAndWholesalePrice(data) {
        return ProductService.findByStockNoWithSummaryAndPrice({
            stock_no: data.stock_no,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.WHOLESALE,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByStockNoWithSummaryAndSellingPrice(data) {
        return ProductService.findByStockNoWithSummaryAndPrice({
            stock_no: data.stock_no,
            branch_id: data.branch_id,
            location_id: data.location_id,
            price: PRICES.SELLING,
            excluded: (data.excluded === undefined) ? null : data.excluded,
        });
    }

    static findByStockNoWithSummaryAndSupplierPrice(data) {
        return axios.get('/product/stock-no', {
            params: {
                stock_no: data.stock_no,
                supplier_id: data.supplier_id,
                branch_id: data.branch_id,
                location_id: data.location_id,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.SUPPLIER
            }
        });
    }

    static findByStockNoWithSummaryAndCustomerPrice(data) {
        return axios.get('/product/stock-no', {
            params: {
                stock_no: data.stock_no,
                customer_type: data.customer_type,
                customer_id: data.customer_id,
                branch_id: data.branch_id,
                location_id: data.location_id,
                excluded: (data.excluded === undefined) ? null : data.excluded,
                scheme: PRICE_SCHEME.CUSTOMER
            }
        });
    }

    static transactionSummary(args) {
        return axios.get('/product/' + args.product_id + '/report/transaction-summary', {
            params: {
                filters: args.filters
            }
        });
    }

    static report(id) {
        window.open('/product/' + id + '/report', '_blank');
    }

    static stockCard(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/product/' + args.product_id + '/report/stock-card?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: args.filters
            }
        });
    }

    static serials(args = {}) {
        return axios.get('/product/' + args.product_id + '/serials', {
            params: {
                constraint: args.constraint,
                entity: args.entity,
                status: args.status,
                transaction: args.transaction,
                supplier_id: args.hasOwnProperty('supplier_id') ? args.supplier_id : null
            }
        }); 
    }

    static batches(args = {}) {
        return axios.get('/product/' + args.product_id + '/batches', {
            params: {
                constraint: args.constraint,
                entity: args.entity,
                supplier: args.supplier
            }
        });
    }

    static findManyBasic(ids) {
        return ProductService.findMany(ids, 'basic');
    }

    static findMany(ids, type) {
        return axios.get('/product/m/' + type, {
            params: {
                ids: ids,
            }
        });
    }

    static summary(args) {
        return axios.get('/product/' + args.product_id + '/summary', {
            params: {
                branch_id: args.branch_id,
                location_id: args.location_id
            }
        });
    }

    static summaries(args) {
        return axios.get('/product/m/summaries', {
            params: {
                product_ids: args.product_ids,
                branch_id: args.branch_id,
                location_id: args.location_id
            }
        });
    }

    static alternatives(args) {
        return axios.get('/product/' + args.product_id + '/alternatives', {
            params: {
                branch_id: args.branch_id,
                location_id: args.location_id
            }
        });
    }

    static components(id) {
        return axios.get('/product/' + id + '/components');
    }

    static inventoryWarning(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? '' : args.filters;

        return axios.get('/reports/product/inventory-warning?page=' + page + '&per_page=' + perPage , {
            params: {
                filters: filters
            }
        });
    }

    static availableSerials(args = {}) {
        let perPage = (args.perPage === undefined) ? '' : args.perPage;
        let page = (args.page === undefined) ? 1 : args.page;
        let filters = (args.filters === undefined) ? [] : args.filters;

        return axios.get('/product/' + args.product_id + '/available-serials?page=' + page + '&per_page=' + perPage, {
            params: {
                filters: filters
            }
        });
    }

    static importExcel(attachment) {
        let form = new FormData;

        form.append('attachment', attachment);

        return axios.post('/product/excel/import',
            form,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
    }

    static applyPurchasePriceToTransactions(args) {
        return axios.patch('/product/' + args.product_id + '/transactions/cost', {
            branch_id: args.branch_id,
            to: args.to,
            from: args.from,
            cost: args.cost
        })
    }

    static downloadExcelTemplate() {
        window.open('/product/excel/import/template');
    }

    static export(params) {
        window.open('/product/export?' + QueryString.stringify(params));
    }
}
