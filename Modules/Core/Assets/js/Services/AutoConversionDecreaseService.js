import AutoConversionService from "./AutoConversionService";

export default class AutoConversionDecreaseService extends AutoConversionService{
    transactions(data) {
        data['type'] = 2;

        return this.getTransactions(data);
    }
}