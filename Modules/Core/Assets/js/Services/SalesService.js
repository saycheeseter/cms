import InvoiceService from './InvoiceService.js';

export default class SalesService extends InvoiceService {
    constructor() {
        this.url = 'sales';
    }
}
