import InventoryAdjustService from './InventoryAdjustService.js';

export default class AdjustDecreaseService extends InventoryAdjustService {
    transactions(data) {
        data['type'] = 2;

        return this.getTransactions(data);
    }
}