import InventoryChain from '../InventoryChain/detail.js';
import SalesOutboundService from '../Services/SalesOutboundService';
import CustomerService from '../Services/CustomerService';
import SalesService from '../Services/SalesService';
import Outbound from '../Common/OutboundMixins.js';
import SalesProductSelectorAPIMixins from "../Common/SalesProductSelectorAPIMixins";

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain, Outbound, SalesProductSelectorAPIMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },
        
        service: {
            invoice: new SalesService,
            inventoryChain: new SalesOutboundService
        },

        buffer: {
            data: {
                customer_type: '',
                customer_id: '',
            },
        },

        reference: {
            customers: customers,
            customerDetails: [],
        },

        attachment: {
            module: MODULE.SALES_OUTBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.SALES_OUTBOUND,
                    key: 'detail'
                }
            }
        },

        dataCollector: {
            transactionType: DATA_COLLECTOR_TRANSACTION.SALES_OUTBOUND
        },

        cache: {
            module: MODULE.SALES_OUTBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.SALES,
                        key: 'info'
                    }
                }
            }
        },

        customer: {
            credit: {
                limit: Numeric.format(0),
                due: Numeric.format(0),
                warning: {
                    visible: false,
                    proceed: false
                },
                notify: false
            }
        }
    },

    created() {
        this.createInfo({
            term: 0,
            customer_type: CUSTOMER.REGULAR,
            customer_id: null,
            customer_detail_id: null,
            customer_walk_in_name: '',
            salesman_id: Laravel.auth.user,
        });
    },

    methods: {
        onSuccessfulLoad(response) {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomer(this.info.customer_detail_id);
            }

            this.checkTransactionForInventoryWarning();
        },

        onCacheLoaded() {
            if (this.info.customer_type === CUSTOMER.REGULAR) {
                this.getCustomer(this.info.customer_detail_id);
            }

            this.checkTransactionForInventoryWarning();
        },

        onSuccessfulImportInvoice(response) {
            this.getCustomer(this.info.customer_detail_id);
            this.checkTransactionForInventoryWarning();
        },

        /**
         * Get customer details by based on the current customer id
         */
        getCustomer(def = null) {
            let that = this;

            this.processing = true;

            CustomerService.show(this.info.customer_id)
                .then(function (response) {
                    let details = that.formatCustomerDetailsToList(response.data.values.details);

                    that.customer.credit.limit = Numeric.format(response.data.values.customer.credit_limit);
                    that.customer.credit.due = Numeric.format(response.data.values.customer.due);

                    that.reference.customerDetails = details;
                    that.info.customer_detail_id = def === null
                        ? details[0].id
                        : def;
                    that.processing = false;
                    that.action.reset();
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.action.reset();
                });
        },

        /**
         * Clears customer and detail id if current customer type is walk in
         * Clears customer name if current customer type is regular
         */
        clearCustomerByType() {
            if (this.isWalkInCustomer) {
                this.info.customer_id = null;
                this.info.customer_detail_id = null;
                this.reference.customerDetails = [];
                this.customer.credit.limit = Numeric.format(0);
                this.customer.credit.due = Numeric.format(0);
            } else {
                this.info.customer_walk_in_name = '';
            }
        },

        getExtraInfoForUpdateApproved() {
            return [
                'salesman_id',
                'term'
            ];
        },

        beforeImportDataCollector() {
            if(this.info.customer_id === null && this.info.customer_type === CUSTOMER.REGULAR) {
                this.message.transaction.error.set([Lang.t('validation.customer_required')]);
                return false;
            }

            return true;
        },

        formatCustomerDetailsToList(details) {
            let list = [];

            for(let i in details) {
                let detail = details[i];

                list.push({
                    id: detail.id,
                    label: detail.name
                });
            }

            return list;
        },

        onSuccessfulValidation() {
            if (this.enableCreditLimit
                && !this.customer.credit.warning.proceed
                && Numeric.parse(this.creditRunningDue) > Numeric.parse(this.customer.credit.limit)
                && ! (this.approvedTransaction || this.declinedTransaction)
            ) {
                if (permissions.allow_customer_exceed_credit_limit) {
                    this.customer.credit.warning.visible = true;
                } else {
                    this.customer.credit.notify = true;
                }

                return false;
            }

            return true;
        },

        proceedOverTheCreditLimitTransaction() {
            this.customer.credit.warning.visible = false;
            this.customer.credit.warning.proceed = true;
            this.processAction();
        },

        closeCustomerCreditNotify() {
            this.customer.credit.notify = false;
        }
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'customer':
                    this.getCustomer();
                    break;

                case 'customer-type':
                    this.clearCustomerByType();
                    break;
            }
        },
        'info.reference_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.transaction_type': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer_id': function(newValue, oldValue) {
            this.buffer.data.customer_id = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.customer_type = this.info.customer_type;
            this.buffer.reference.invoice = this.reference.invoice;
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer_type': function(newValue, oldValue) {
            this.buffer.data.customer_type = oldValue;
            this.buffer.data.reference_id = this.info.reference_id;
            this.buffer.data.transaction_type = this.info.transaction_type;
            this.buffer.data.customer_id = this.info.customer_id;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.reference.invoice = this.reference.invoice;

            if(newValue == CUSTOMER.WALK_IN) {
                this.$nextTick(() => {
                    this.$refs.walkin.focus();
                })
            }
        },
    },

    computed: {
        isRegularCustomer: function () {
            return this.info.customer_type === CUSTOMER.REGULAR;
        },

        isWalkInCustomer: function () {
            return this.info.customer_type === CUSTOMER.WALK_IN;
        },

        regularCustomer: function () {
            return CUSTOMER.REGULAR;
        },

        walkInCustomer: function () {
            return CUSTOMER.WALK_IN;
        },
        creditRunningDue: function() {
            return Numeric.format(
                Numeric.parse(this.customer.credit.due) + Numeric.parse(this.totalAmount)
            );
        },
        enableCreditLimit: function() {
            return this.info.customer_type === CUSTOMER.REGULAR && Laravel.settings.sales_customer_credit_limit;
        }
    }
});
