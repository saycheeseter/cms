import Invoice from '../Invoice/detail.js';
import StockReturnService from '../Services/StockReturnService';
import Outbound from '../Common/OutboundMixins.js';
import BranchService from '../Services/BranchService';
import DeliveryProductSelectorAPIMixins from "../Common/DeliveryProductSelectorAPIMixins";

let app = new Vue({
    el: '#app',

    mixins: [Invoice, Outbound, DeliveryProductSelectorAPIMixins],

    data: {
        reference: {
            deliveryLocations: [],
        },
        
        attachment: {
            module: MODULE.STOCK_RETURN
        },

        service: new StockReturnService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_RETURN,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.STOCK_RETURN
        }
    },

    created() {
        this.createInfo({
            deliver_to: null,
            deliver_to_location: null,
        });
    },

    methods: {
        revertInfo() {
            this.info.set(this.buffer.data);
            this.reference.forLocations = this.buffer.reference.forLocations;
            this.warning.visible = false;
            this.action.reset();
        },

        getDeliveryLocations(def = null) {
            let that = this;

            this.processing = true;

            BranchService.details(this.info.deliver_to)
                .then(function (response) {
                    that.reference.deliveryLocations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.deliver_to_location = location;

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        onSuccessfulLoad(response) {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },

        onCacheLoaded() {
            this.getDeliveryLocations(this.info.deliver_to_location);
            this.checkTransactionForInventoryWarning();
        },
    },
});