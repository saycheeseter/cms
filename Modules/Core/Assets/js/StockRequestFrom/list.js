import StockRequest from '../StockRequest/list.js';
import Invoice from '../Invoice/list.js';
import Arr from '../Classes/Arr';

let app = new Vue({
    el: '#app',

    mixins: [StockRequest],

    data: {
        link: 'stock-request-from',
        options: {
            setup: SETUP.VIEW,
        },
        columnSettings: {
            module: {
                id: MODULE.STOCK_REQUEST_FROM,
                key: 'info'
            }
        }
    },

    created: function() {
        this.filter.options[0].selected = false;
        this.filter.options.push({
            name: Lang.t('label.request_to'),
            columnName: 'request_to',
            type: 'select',
            selectOptions: branches,
            selected: true,
            default: { value: String(Laravel.auth.branch) }
        });

        delete this.datatable.settings.withSelect;
    }
});
