import StockRequest from '../StockRequest/detail.js';
import PurchaseService from '../Services/PurchaseService';

let app = new Vue({
    el: '#app',

    mixins: [StockRequest],

    data: {
        link: 'stock-request-from',
        options: {
            setup: SETUP.VIEW,
        },
        poModal: {
            visible: true,
        },
        
        orderForm: {
            visible: false,
            table: {
                id: 'purchase-table',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                }
            },
            message: new Notification
        },

        format: {
            detail: {
                remaining_qty: 'numeric'
            }, 
        },

        transactionable: {
            visible: false,
            details: []
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_REQUEST_FROM,
                    key: 'detail'
                }
            }
        }
    },

    methods: {
        onSuccessfulLoad(response) {
            this.copyProductsWithRemaining();
        },

        showOrderForm() {
            this.orderForm.visible = true;
        },

        removeOrder(data, index, dataIndex) {
            this.orderForm.table.value.data.splice(dataIndex, 1);
        },

        setOrderTotalQty(index) {
            let row = this.orderForm.table.value.data[index];

            row['total_qty'] = Numeric.format(
                Numeric.parse(row['unit_qty']) *
                Numeric.parse(row['qty'])
            );
        },

        setOrderUnitSpecs(data) {
            let current = this.orderForm.table.value.data[data.row];

            this.setAttributeField(data, this.orderForm.table.value.data, 'unit_qty');
            this.setOrderTotalQty(data.row);
            this.setOrderBarcode(data.row);
        },

        setOrderBarcode(index) {
            let row = this.orderForm.table.value.data[index];
            let unit = Number(this.orderForm.table.value.data[index].unit_id);

            row['barcode'] = row['barcode_list'][unit];
        },

        createOrder() {
            if (this.processing || ! this.validateOrder()) {
                return false;
            }

            let that = this;

            this.processing = true;

            PurchaseService.instance().createFromStockRequestBySupplier(this.order)
                .then(function (response) {
                    for (let i = 0; i < response.data.values.invoices.length; i++) {
                        let invoice = response.data.values.invoices[i];

                        PurchaseService.instance().edit(invoice.id);
                    }

                    that.processing = false;
                })
                .catch(function (error) {
                    that.orderForm.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validateOrder() {
            if (this.order.details.length === 0) {
                this.orderForm.message.error.set([Lang.t('validation.details_min_one')]);
                return false;
            }

            return true;
        },

        copyProductsWithRemaining() {
            let details = Format.make(this.datatable.details.value.data, this.format.detail);
            let remaining = [];

            for (let i = 0; i < details.length; i++) {
                let data = details[i];

                if (Numeric.parse(data.remaining_qty) > 0) {
                    remaining.push({
                        barcode: data.barcode,
                        barcode_list: data.barcode_list,
                        chinese_name: data.chinese_name,
                        name: data.name,
                        transactionable_id: data.id,
                        product_id: data.product_id,
                        qty: (Numeric.parse(data.remaining_qty) / Numeric.parse(data.unit_qty)),
                        units: data.units,
                        unit_id: data.unit_id,
                        unit_qty: data.unit_qty,
                        group_type: data.group_type,
                        components: data.components,
                        manageable: data.manageable,
                        product_type: data.product_type,
                    });
                }
            }

            this.orderForm.table.value.data = remaining;
        },

        viewPurchase(id) {
            PurchaseService.instance().edit(id);
        },

        viewPurchases(index) {
            this.transactionable.visible = true;
            this.transactionable.details = this.datatable.details.value.data[index].references;
        }
    },

    computed: {
        order: function () {
            return {
                info: {
                    transactionable_id: this.info.id,
                    deliver_to: this.info.created_for
                },
                details: Format.make(this.orderForm.table.value.data, this.format.detail)
            }
        },

        hasRemainingItems: function() {
            return this.orderForm.table.value.data.length > 0;
        }
    }
});
