import Core from '../Common/CoreMixins.js';
import GridInfo from '../Components/PrintoutGrid/GridInfo.vue';
import GridContent from '../Components/PrintoutGrid/GridContent.vue';
import GridLabel from '../Components/PrintoutGrid/GridLabel.vue';
import PrintoutTemplateService from '../Services/PrintoutTemplateService';
import Fields from './fields.js';

let app = new Vue({

    i18n: Lang,
    
    el: '#app',

    mixins: [Core, Fields],

    components: {
        GridInfo,
        GridContent,
        GridLabel
    },

    data: {
        template: new Data({
            id: id,
            name: '',
            module_id: null,
            format: {
                general: {
                    margin: {
                        left: 2.00,
                        right: 2.00,
                        top: 2.00,
                        bottom: 2.00,
                    },
                    paper: {
                        size: 'A4',
                        orientation: 'portrait'
                    },
                    header: {
                        rows: 9
                    },
                    content: {
                        rows: 11,
                        border: 1,
                    },
                    footer: {
                        rows: 6,
                        duplicate: false
                    },
                    pageNumber: ''
                },
                header: {},
                content: {},
                footer: {},
            },
        }),

        modules: [
            { id: MODULE.PURCHASE, label: Lang.t('label.purchase_order') },
            { id: MODULE.PURCHASE_RETURN, label: Lang.t('label.purchase_return') },
            { id: MODULE.DAMAGE, label: Lang.t('label.damage') },
            { id: MODULE.SALES, label: Lang.t('label.sales') },
            { id: MODULE.SALES_RETURN, label: Lang.t('label.sales_return') },
            { id: MODULE.STOCK_REQUEST, label: Lang.t('label.stock_request') },
            { id: MODULE.STOCK_DELIVERY, label: Lang.t('label.stock_delivery') },
            { id: MODULE.STOCK_RETURN, label: Lang.t('label.stock_return') },
            { id: MODULE.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound') },
            { id: MODULE.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound') },
            { id: MODULE.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound') },
            { id: MODULE.SALES_OUTBOUND, label: Lang.t('label.sales_outbound') },
            { id: MODULE.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound') },
            { id: MODULE.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound') },
            { id: MODULE.STOCK_DELIVERY_INBOUND, label: Lang.t('label.stock_delivery_inbound') },
            { id: MODULE.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound') },
            { id: MODULE.STOCK_RETURN_INBOUND, label: Lang.t('label.stock_return_inbound') },
            { id: MODULE.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust') },
            { id: MODULE.COUNTER, label: Lang.t('label.counter') },
        ],

        section: 'header',
        bordered: true,
        message: new Notification,
        tabs: new Toggle([
            { name: 'labels', label: Lang.t('label.labels') },
            { name: 'values', label: Lang.t('label.values') }
        ], 'labels'),
    },

    created() {
        this.initialize();
    },

    mounted() {
        if (this.template.id !== 'create') {
            this.load();
        }
    },

    methods: {
        /**
         * Initialize default values for header section
         */
        initialize() {
            this.setDefaultHeader();
            this.setDefaultContent();
            this.setDefaultFooter();
            this.setDefaultCommon();
        },

        setDefaultContentValue(module) {
            for(let property in module) {
                module[property] = Object.assign(module[property], {
                    checked: false,
                    width: 1,
                    sequence: 1,
                    alignment: 'left',
                    font: {
                        weight: 'normal',
                        style: 'normal',
                        decoration: 'normal'
                    },
                });
            }
        },

        setDefaultInfoValueLabel(module) {
            module = Object.assign(module, {
                checked: false,
                y: 1,
                x: 0,
                width: 1,
                alignment: 'left',
                font: {
                    size: 12,
                    weight: 'normal',
                    style: 'normal',
                    decoration: 'normal'
                },
                border: {
                    left: false,
                    top: false,
                    right: false,
                    bottom: false
                },
                sequenceInRow: 1,
            });
        },

        setDefaultValueLabelProperty(module) {
            for(let section in module) {
                for (let prop in module[section]) {
                    this.setDefaultInfoValueLabel(module[section][prop]);
                }
            }
        },

        setDefaultHeader() {
            for(let property in this.fields.header) {
                let module = this.fields.header[property];
                this.setDefaultValueLabelProperty(module);
            }
        },

        setDefaultContent() {
            for(let key in this.fields.content) {
                this.setDefaultContentValue(this.fields.content[key].columns);
            }
        },

        setDefaultFooter() {
            for(let property in this.fields.footer) {
                let module = this.fields.footer[property];
                this.setDefaultValueLabelProperty(module);
            }
        },

        setDefaultCommon() {
            this.setDefaultValueLabelProperty(this.fields.common.header);
            this.setDefaultContentValue(this.fields.common.content.columns);
            this.setDefaultValueLabelProperty(this.fields.common.footer);
        },

        changeSection() {
            this.triggerLoading();

            if (this.section === 'content') {
                this.tabs.active('values');
            } else {
                this.tabs.active('labels');
            }
        },

        changeTemplate() {
            this.triggerLoading();

            let fields = Object.assign({}, this.getModuleFields());

            this.template.format.header = fields.header;
            this.template.format.content = fields.content;
            this.template.format.footer = fields.footer;
        },

        getModuleFields() {
            let fields = {
                header: {},
                content: {},
                footer: {}
            };

            let alias = MODULE_ALIAS[this.template.module_id];

            for (let property in fields) {
                let section = this.fields[property][alias];

                let extras = section.hasOwnProperty('extras')
                    ? Arr.wrap(section.extras)
                    : [];

                fields[property] = section;

                for (let i in extras) {
                    fields[property] = DeepMerge(
                        fields[property],
                        this.fields[extras[i]][property]
                    );
                }

                if (fields[property].hasOwnProperty('extras')) {
                    delete fields[property].extras;
                }
            }

            return fields;
        },

        save() {
            if ((this.template.id === 'create' && ! permissions.create)
                || (this.template.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            if(this.template.id === 'create'){
                this.store();
            } else {
                this.update();
            }
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            PrintoutTemplateService.store(this.template.data())
                .then(function (response) {
                    that.message.success = response.data.message;
                    that.processing = false;
                    that.template.id = response.data.values.template.id;
                    that.redirect();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            PrintoutTemplateService.update(this.template.data(), this.template.id)
                .then(function (response) {
                    that.message.success = response.data.message;
                    that.processing = false;
                    that.redirect();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        load() {
            if (this.processing) {
                return false;
            }
            
            let that = this;

            PrintoutTemplateService.show(this.template.id)
                .then(function (response) {
                    that.template.set(response.data.values.template);
                    that.template.format = DeepMerge(that.getModuleFields(), that.template.format);
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        redirect() {
            let that = this;

            setTimeout(function() {
                PrintoutTemplateService.redirect(that.template.id);
            }, 2000);
        },

        triggerLoading() {
            let that = this;

            this.processing = true;

            setTimeout(function() {
                that.processing = false;
            }, 600);
        },

        sortObjectByKey(object) {
            return Object.keys(object).sort().reduce((r, k) => (r[k] = object[k], r), {});
        },

        sortFieldsBy(fields, by) {
            return Obj.sortBy(fields, by);
        },

        sortInfoFieldsBy(fields, by) {
            let set = [];

            for (let key in fields) {
                let section = fields[key];

                for (let i in section) {
                    set.push(section[i]);
                }
            }

            return Arr.sortBy(set, by);
        },
    },

    computed: {
        disabled() {
            return this.template.id !== 'create';
        }, 
        options() {
            let options = {
                header: {
                    y: {
                        max: this.template.format.general.header.rows
                    }
                },
                footer: {
                    y: {
                        max: this.template.format.general.footer.rows
                    }
                }
            };

            return options;
        },
        isAnyContentFieldChecked() {
            return this.contentFieldCheckedCount > 0;
        },
        isSerialsEnabled() {
            return this.template.format.content.hasOwnProperty('serials')
                && this.template.format.content.serials.enabled;
        },
        contentFieldCheckedCount() {
            let content = this.template.format.content.hasOwnProperty('columns')
                ? Obj.toArray(this.template.format.content.columns)
                : [];

            let checked = Arr.extract(Arr.pluck(content, 'checked'), [true]);

            return checked.length;
        }
    },
    watch: {
        contentFieldCheckedCount: function(count) {
            if (!this.template.format.content.hasOwnProperty('serials')
                || this.template.format.content.serials.startAt < count
            ) {
                return;
            }

            this.template.format.content.serials.startAt = count;
        }
    }
});