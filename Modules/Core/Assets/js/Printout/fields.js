export default {
    data: {
        fields: {
            header: {
                purchase: {
                    values : {
                        deliver_to: { header: 'deliver_to' },
                        deliver_until: { header: 'deliver_until' },
                        payment_method: { header: 'payment_method' },
                        supplier: { header: 'supplier' },
                        supplier_address: { header: 'supplier_address' },
                        term: { header: 'term' },
                    },
                    labels: {
                        deliver_to: { header: 'deliver_to', label: Lang.t('label.deliver_to') },
                        deliver_until: { header: 'deliver_until', label: Lang.t('label.deliver_until') },
                        payment_method: { header: 'payment_method', label: Lang.t('label.payment_method') },
                        supplier: { header: 'supplier', label: Lang.t('label.supplier') },
                        supplier_address: { header: 'supplier_address', label: Lang.t('label.supplier_address') },
                        term: { header: 'term', label: Lang.t('label.term') },
                    },
                    extras: 'common'
                },
                purchase_return: {
                    values: {
                        payment_method: { header: 'payment_method' },
                        supplier: { header: 'supplier' },
                        supplier_address: { header: 'supplier_address' },
                        term: { header: 'term' },
                    },
                    labels: {
                        payment_method: { header: 'payment_method', label: Lang.t('label.payment_method') },
                        supplier: { header: 'supplier', label: Lang.t('label.supplier') },
                        supplier_address: { header: 'supplier_address', label: Lang.t('label.supplier_address') },
                        term: { header: 'term', label: Lang.t('label.term') },
                    },
                    extras: 'common'
                },
                damage: {
                    values: {
                        reason: { header: 'reason' },
                    },
                    labels: {
                        reason: { header: 'reason', label: Lang.t('label.reason') },
                    },
                    extras: 'common'
                },
                sales: {
                    values: {
                        customer: { header: 'customer' },
                        salesman: { header: 'salesman' },
                        term: { header: 'term' },
                        customer_address: { header: 'customer_address' },
                    },
                    labels: {
                        customer: { header: 'customer', label: Lang.t('label.customer') },
                        salesman: { header: 'salesman', label: Lang.t('label.salesman') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        customer_address: { header: 'customer_address', label: Lang.t('label.customer_address') },
                    },
                    extras: 'common'
                },
                sales_return: {
                    values: {
                        customer: { header: 'customer' },
                        salesman: { header: 'salesman' },
                        term: { header: 'term' },
                        customer_address: { header: 'customer_address' },
                    },
                    labels: {
                        customer: { header: 'customer', label: Lang.t('label.customer') },
                        salesman: { header: 'salesman', label: Lang.t('label.salesman') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        customer_address: { header: 'customer_address', label: Lang.t('label.customer_address') },
                    },
                    extras: 'common'
                },
                stock_request: {
                    values: {
                        request_to: { header: 'request_to' },
                        request_to_address: { header: 'request_to_address' },
                    },
                    labels: {
                        request_to: { header: 'request_to', label: Lang.t('label.request_to') },
                        request_to_address: { header: 'request_to_address', label: Lang.t('label.request_to_address') },
                    },
                    extras: 'common'
                },
                stock_delivery: {
                    values: {
                        to_branch: { header: 'to_branch' },
                        to_branch_address: { header: 'to_branch_address' },
                        deliver_to_location: { header: 'deliver_to_location' },
                        request_reference: { header: 'request_reference' }
                    },
                    labels: {
                        to_branch: { header: 'to_branch', label: Lang.t('label.to_branch') },
                        to_branch_address: { header: 'to_branch_address', label: Lang.t('label.to_branch_address') },
                        deliver_to_location: { header: 'deliver_to_location', label: Lang.t('label.deliver_to_location') },
                        request_reference: { header: 'request_reference', label: Lang.t('label.request_reference') }
                    },
                    extras: 'common'
                },
                stock_return: {
                    values: {
                        to_branch: { header: 'to_branch' },
                        to_branch_address: { header: 'to_branch_address' },
                        deliver_to_location: { header: 'deliver_to_location' },
                    },
                    labels: {
                        to_branch: { header: 'to_branch', label: Lang.t('label.to_branch') },
                        to_branch_address: { header: 'to_branch_address', label: Lang.t('label.to_branch_address') },
                        deliver_to_location: { header: 'deliver_to_location', label: Lang.t('label.deliver_to_location') },
                    },
                    extras: 'common'
                },
                purchase_inbound: {
                    values : {
                        payment_method: { header: 'payment_method' },
                        supplier: { header: 'supplier' },
                        supplier_address: { header: 'supplier_address' },
                        term: { header: 'term' },
                        dr_no: { header: 'dr_no' },
                        supplier_invoice_no: { header: 'supplier_invoice_no' },
                        supplier_box_count: { header: 'supplier_box_count' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        payment_method: { header: 'payment_method', label: Lang.t('label.payment_method') },
                        supplier: { header: 'supplier', label: Lang.t('label.supplier') },
                        supplier_address: { header: 'supplier_address', label: Lang.t('label.supplier_address') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        dr_no: { header: 'dr_no', label: Lang.t('label.dr_no') },
                        supplier_invoice_no: { header: 'supplier_invoice_no', label: Lang.t('label.supplier_invoice_no') },
                        supplier_box_count: { header: 'supplier_box_count', label: Lang.t('label.supplier_box_count') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                purchase_return_outbound: {
                    values: {
                        payment_method: { header: 'payment_method' },
                        supplier: { header: 'supplier' },
                        supplier_address: { header: 'supplier_address' },
                        term: { header: 'term' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        payment_method: { header: 'payment_method', label: Lang.t('label.payment_method') },
                        supplier: { header: 'supplier', label: Lang.t('label.supplier') },
                        supplier_address: { header: 'supplier_address', label: Lang.t('label.supplier_address') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                damage_outbound: {
                    values: {
                        reason: { header: 'reason' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        reason: { header: 'reason', label: Lang.t('label.reason') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                sales_outbound: {
                    values: {
                        customer: { header: 'customer' },
                        salesman: { header: 'salesman' },
                        term: { header: 'term' },
                        customer_address: { header: 'customer_address' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        customer: { header: 'customer', label: Lang.t('label.customer') },
                        salesman: { header: 'salesman', label: Lang.t('label.salesman') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        customer_address: { header: 'customer_address', label: Lang.t('label.customer_address') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                sales_return_inbound: {
                    values: {
                        customer: { header: 'customer' },
                        salesman: { header: 'salesman' },
                        term: { header: 'term' },
                        customer_address: { header: 'customer_address' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        customer: { header: 'customer', label: Lang.t('label.customer') },
                        salesman: { header: 'salesman', label: Lang.t('label.salesman') },
                        term: { header: 'term', label: Lang.t('label.term') },
                        customer_address: { header: 'customer_address', label: Lang.t('label.customer_address') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                stock_delivery_outbound: {
                    values: {
                        to_branch: { header: 'to_branch' },
                        to_branch_address: { header: 'to_branch_address' },
                        deliver_to_location: { header: 'deliver_to_location' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        to_branch: { header: 'to_branch', label: Lang.t('label.to_branch') },
                        to_branch_address: { header: 'to_branch_address', label: Lang.t('label.to_branch_address') },
                        deliver_to_location: { header: 'deliver_to_location', label: Lang.t('label.deliver_to_location') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                stock_delivery_inbound: {
                    values: {
                        delivery_from: { header: 'delivery_from' },
                        delivery_from_address: { header: 'delivery_from_address' },
                        delivery_from_location: { header: 'delivery_from_location' },
                        delivery_reference: { header: 'delivery_reference' }
                    },
                    labels: {
                        delivery_from: { header: 'delivery_from', label: Lang.t('label.delivery_from') },
                        delivery_from_address: { header: 'delivery_from_address', label: Lang.t('label.delivery_from_address') },
                        delivery_from_location: { header: 'delivery_from_location', label: Lang.t('label.delivery_from_location') },
                        delivery_reference: { header: 'delivery_reference', label: Lang.t('label.delivery_reference') }
                    },
                    extras: 'common'
                },
                stock_return_outbound: {
                    values: {
                        to_branch: { header: 'to_branch' },
                        to_branch_address: { header: 'to_branch_address' },
                        deliver_to_location: { header: 'deliver_to_location' },
                        invoice_reference: { header: 'invoice_reference' }
                    },
                    labels: {
                        to_branch: { header: 'to_branch', label: Lang.t('label.to_branch') },
                        to_branch_address: { header: 'to_branch_address', label: Lang.t('label.to_branch_address') },
                        deliver_to_location: { header: 'deliver_to_location', label: Lang.t('label.deliver_to_location') },
                        invoice_reference: { header: 'invoice_reference', label: Lang.t('label.invoice_reference') }
                    },
                    extras: 'common'
                },
                stock_return_inbound: {
                    values: {
                        delivery_from: { header: 'delivery_from' },
                        delivery_from_address: { header: 'delivery_from_address' },
                        delivery_from_location: { header: 'delivery_from_location' },
                        delivery_reference: { header: 'delivery_reference' }
                    },
                    labels: {
                        delivery_from: { header: 'delivery_from', label: Lang.t('label.delivery_from') },
                        delivery_from_address: { header: 'delivery_from_address', label: Lang.t('label.delivery_from_address') },
                        delivery_from_location: { header: 'delivery_from_location', label: Lang.t('label.delivery_from_location') },
                        delivery_reference: { header: 'delivery_reference', label: Lang.t('label.delivery_reference') }
                    }
                },
                inventory_adjust: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                counter: {
                    values : {
                        branch_address: { header: 'branch_address' },
                        branch_contact: { header: 'branch_contact' },
                        branch_name: { header: 'branch_name' },
                        created_by: { header: 'prepared_by' },
                        created_for: { header: 'for_branch' },
                        customer: { header: 'customer' },
                        remarks: { header: 'remarks' },
                        sheet_number: { header: 'sheet_no' },
                        transaction_date_time: { header: 'transaction_date_time' },
                        transaction_date: { header: 'transaction_date' },
                        transaction_time: { header: 'transaction_time' },
                    },
                    labels: {
                        created_by: { header: 'prepared_by', label: Lang.t('label.prepared_by') },
                        created_for: { header: 'for_branch', label: Lang.t('label.for_branch') },
                        customer: { header: 'customer', label: Lang.t('label.customer') },
                        remarks: { header: 'remarks', label: Lang.t('label.remarks') },
                        transaction_date_time: { header: 'transaction_date_time', label: Lang.t('label.transaction_date_time') },
                        transaction_date: { header: 'transaction_date', label: Lang.t('label.transaction_date') },
                        transaction_time: { header: 'transaction_time', label: Lang.t('label.transaction_time') },
                        static_text_1: { header: 'static_text_1', label: Lang.t('label.static_text_1') },
                        static_text_2: { header: 'static_text_2', label: Lang.t('label.static_text_2') },
                        static_text_3: { header: 'static_text_3', label: Lang.t('label.static_text_3') },
                        static_text_4: { header: 'static_text_4', label: Lang.t('label.static_text_4') },
                        static_text_5: { header: 'static_text_5', label: Lang.t('label.static_text_5') }
                    }
                }
            },
            content: {
                inventory_adjust: {
                    columns: {
                        unit_barcode: { header: 'barcode', label: Lang.t('label.barcode') },
                        name: { header: 'product_name', label: Lang.t('label.product_name') },
                        stock_no: { header: 'stock_no', label: Lang.t('label.stock_no') },
                        chinese_name: { header: 'chinese_name', label: Lang.t('label.chinese_name') },
                        qty: { header: 'qty', label: Lang.t('label.qty') },
                        unit_code: { header: 'unit_code', label: Lang.t('label.unit_code') },
                        unit_name: { header: 'unit_name', label: Lang.t('label.unit_name') },
                        unit_code_specs: { header: 'unit_code_specs', label: Lang.t('label.unit_code_specs') },
                        unit_qty: { header: 'unit_specs', label: Lang.t('label.unit_specs') },
                        pc_qty: { header: 'pc_qty', label: Lang.t('label.pc_qty') },
                        cost: { header: 'cost', label: Lang.t('label.cost') },
                        inventory: { header: 'inventory', label: Lang.t('label.inventory') },
                        amount: { header: 'amount', label: Lang.t('label.amount') },
                    },
                    decimals: {
                        qty: { label: 'qty', value: true },
                        unit_qty: { label: 'unit_specs', value: true },
                        unit_code_specs: { label: 'unit_code_specs', value: true },
                        pc_qty: { label: 'pc_qty', value: true },
                        cost: { label: 'cost', value: true },
                        inventory: { label: 'inventory', value: true },
                        amount: { label: 'amount', value: true },
                    },
                    extras: 'serials'
                },
                counter: {
                    columns: {
                        transaction_date: { header: 'transaction_date', label: Lang.t('label.transaction_date') },
                        sheet_number: { header: 'sheet_number', label: Lang.t('label.sheet_number') },
                        remaining: { header: 'remaining', label: Lang.t('label.remaining') },
                    },
                    decimals: {
                        remaining: { label: 'remaining', value: true },
                    }
                },
                purchase_inbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                purchase_return_outbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                damage_outbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                sales_outbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                sales_return_inbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                stock_delivery_outbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                stock_delivery_inbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                stock_return_outbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                stock_return_inbound: {
                    columns: {},
                    decimals: {},
                    extras: ['common', 'serials']
                },
                purchase: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                purchase_return: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                damage: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                sales: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                sales_return: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                stock_request: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                stock_delivery: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                },
                stock_return: {
                    columns: {},
                    decimals: {},
                    extras: 'common'
                }
            },
            footer: {
                inventory_adjust: {
                    values: {
                        net_total: { header: 'net_total' },
                        total_qty: { header: 'total_qty' },
                        total_lost_qty: { header: 'total_lost_qty' },
                        total_gain_qty: { header: 'total_gain_qty' },
                        total_gain_lost_qty: { header: 'total_gain_lost_qty' },
                        total_lost_amount_cost: { header: 'total_lost_amount_cost' },
                        total_gain_amount_cost: { header: 'total_gain_amount_cost' },
                        total_gain_lost_amount_cost: { header: 'total_gain_lost_amount_cost' },
                    },
                    labels: {
                        checked_by: { header: 'checked_by', label: Lang.t('label.checked_by') },
                        prepared_by: { header: 'prepared_by', label: Lang.t('label.prepared_by') },
                        total_qty: { header: 'total_qty', label: Lang.t('label.total_qty') },
                        total_lost_qty: { header: 'total_lost_qty', label: Lang.t('label.total_lost_qty') },
                        total_gain_qty: { header: 'total_gain_qty', label: Lang.t('label.total_gain_qty') },
                        total_gain_lost_qty: { header: 'total_gain_lost_qty', label: Lang.t('label.total_gain_lost_qty') },
                        total_lost_amount_cost: { header: 'total_lost_amount_cost', label: Lang.t('label.total_lost_amount_cost') },
                        total_gain_amount_cost: { header: 'total_gain_amount_cost', label: Lang.t('label.total_gain_amount_cost') },
                        total_gain_lost_amount_cost: { header: 'total_gain_lost_amount_cost', label: Lang.t('label.total_gain_lost_amount_cost') },
                    }
                },
                counter: {
                    values: {
                        total_remaining: { header: 'total_remaining' },
                    },
                    labels: {
                        total_remaining: { header: 'total_remaining', label: Lang.t('label.total_remaining') },
                        checked_by: { header: 'checked_by', label: Lang.t('label.checked_by') },
                        prepared_by: { header: 'prepared_by', label: Lang.t('label.prepared_by') },
                        delivered_by: { header: 'delivered_by', label: Lang.t('label.delivered_by') },
                        received_by: { header: 'received_by', label: Lang.t('label.received_by') },
                        static_text_1: { header: 'static_text_1', label: Lang.t('label.static_text_1') },
                        static_text_2: { header: 'static_text_2', label: Lang.t('label.static_text_2') },
                        static_text_3: { header: 'static_text_3', label: Lang.t('label.static_text_3') },
                        static_text_4: { header: 'static_text_4', label: Lang.t('label.static_text_4') },
                        static_text_5: { header: 'static_text_5', label: Lang.t('label.static_text_5') },
                    }
                },
                purchase_inbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                purchase_return_outbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                damage_outbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                sales_outbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                sales_return_inbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_delivery_outbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_delivery_inbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_return_outbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_return_inbound: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                purchase: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                purchase_return: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                damage: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                sales: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                sales_return: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_request: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_delivery: {
                    values: {},
                    labels: {},
                    extras: 'common'
                },
                stock_return: {
                    values: {},
                    labels: {},
                    extras: 'common'
                }
            },
            common: {
                header: {
                    values : {
                        branch_address: { header: 'branch_address' },
                        branch_contact: { header: 'branch_contact' },
                        branch_name: { header: 'branch_name' },
                        created_by: { header: 'prepared_by' },
                        created_for: { header: 'for_branch' },
                        for_location: { header: 'for_location' },
                        remarks: { header: 'remarks' },
                        requested_by: { header: 'request_by' },
                        sheet_number: { header: 'sheet_no' },
                        term: { header: 'term' },
                        transaction_date_time: { header: 'transaction_date_time' },
                        transaction_date: { header: 'transaction_date' },
                        transaction_time: { header: 'transaction_time' },
                    },
                    labels: {
                        created_by: { header: 'prepared_by', label: Lang.t('label.prepared_by') },
                        created_for: { header: 'for_branch', label: Lang.t('label.for_branch') },
                        for_location: { header: 'for_location', label: Lang.t('label.for_location') },
                        remarks: { header: 'remarks', label: Lang.t('label.remarks') },
                        requested_by: { header: 'request_by', label: Lang.t('label.request_by') },
                        transaction_date_time: { header: 'transaction_date_time', label: Lang.t('label.transaction_date_time') },
                        transaction_date: { header: 'transaction_date', label: Lang.t('label.transaction_date') },
                        transaction_time: { header: 'transaction_time', label: Lang.t('label.transaction_time') },
                        static_text_1: { header: 'static_text_1', label: Lang.t('label.static_text_1') },
                        static_text_2: { header: 'static_text_2', label: Lang.t('label.static_text_2') },
                        static_text_3: { header: 'static_text_3', label: Lang.t('label.static_text_3') },
                        static_text_4: { header: 'static_text_4', label: Lang.t('label.static_text_4') },
                        static_text_5: { header: 'static_text_5', label: Lang.t('label.static_text_5') }
                    }
                },
                content: {
                    columns: {
                        unit_barcode: { header: 'barcode', label: Lang.t('label.barcode') },
                        name: { header: 'product_name', label: Lang.t('label.product_name') },
                        stock_no: { header: 'stock_no', label: Lang.t('label.stock_no') },
                        chinese_name: { header: 'chinese_name', label: Lang.t('label.chinese_name') },
                        qty: { header: 'qty', label: Lang.t('label.qty') },
                        unit_code: { header: 'unit_code', label: Lang.t('label.unit_code') },
                        unit_name: { header: 'unit_name', label: Lang.t('label.unit_name') },
                        unit_code_specs: { header: 'unit_code_specs', label: Lang.t('label.unit_code_specs') },
                        unit_qty: { header: 'unit_specs', label: Lang.t('label.unit_specs') },
                        oprice: { header: 'o_price', label: Lang.t('label.o_price') },
                        price: { header: 'price', label: Lang.t('label.price') },
                        discount1: { header: 'discount_1', label: Lang.t('label.discount_1') },
                        discount2: { header: 'discount_2', label: Lang.t('label.discount_2') },
                        discount3: { header: 'discount_3', label: Lang.t('label.discount_3') },
                        discount4: { header: 'discount_4', label: Lang.t('label.discount_4') },
                        discounts: { header: 'discounts', label: Lang.t('label.discounts') },
                        amount: { header: 'amount', label: Lang.t('label.amount') },
                        remarks: { header: 'remarks', label: Lang.t('label.remarks') },
                    },
                    decimals: {
                        amount: { label: 'amount', value: true },
                        discount1: { label: 'discount_1', value: true },
                        discount2: { label: 'discount_2', value: true },
                        discount3: { label: 'discount_3', value: true },
                        discount4: { label: 'discount_4', value: true },
                        discounts: { label: 'discounts', value: true },
                        oprice: { label: 'o_price', value: true },
                        price: { label: 'price', value: true },
                        qty: { label: 'qty', value: true },
                        unit_qty: { label: 'unit_specs', value: true },
                        unit_code_specs: { label: 'unit_code_specs', value: true },
                    }
                },
                footer: {
                    values: {
                        net_total: { header: 'net_total' },
                        total_qty: { header: 'total_qty' }
                    },
                    labels: {
                        checked_by: { header: 'checked_by', label: Lang.t('label.checked_by') },
                        prepared_by: { header: 'prepared_by', label: Lang.t('label.prepared_by') },
                        total_qty: { header: 'total_qty', label: Lang.t('label.total_qty') },
                        net_total: { header: 'net_total', label: Lang.t('label.net_total') },
                        delivered_by: { header: 'delivered_by', label: Lang.t('label.delivered_by') },
                        received_by: { header: 'received_by', label: Lang.t('label.received_by') },
                        static_text_1: { header: 'static_text_1', label: Lang.t('label.static_text_1') },
                        static_text_2: { header: 'static_text_2', label: Lang.t('label.static_text_2') },
                        static_text_3: { header: 'static_text_3', label: Lang.t('label.static_text_3') },
                        static_text_4: { header: 'static_text_4', label: Lang.t('label.static_text_4') },
                        static_text_5: { header: 'static_text_5', label: Lang.t('label.static_text_5') }
                    }
                },
            },
            serials: {
                content: {
                    serials: {
                        enabled: false,
                        startAt: 1,
                        type: 'basic',
                        basic: {
                            cell: 3
                        },
                        advance: {
                            serial_number: { label: 'serial_number', enabled: true, width: 10},
                            expiration_date: { label: 'expiration_date', enabled: true, width: 10},
                            manufacturing_date: { label: 'manufacturing_date', enabled: true, width: 10},
                            admission_date: { label: 'admission_date', enabled: true, width: 10},
                            manufacturer_warranty_start: { label: 'manufacturer_warranty_start', enabled: true, width: 10},
                            manufacturer_warranty_end: { label: 'manufacturer_warranty_end', enabled: true, width: 10},
                            remarks: { label: 'remarks', enabled: true, width: 10},
                        }
                    }
                }
            }
        },
    }
}