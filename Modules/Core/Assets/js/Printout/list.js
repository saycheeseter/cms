import CoreMixins from '../Common/CoreMixins';
import PrintoutTemplateService from '../Services/PrintoutTemplateService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            }
        },

        filter: {
            visible: false,
            options: [
                { 
                    name: Lang.t('label.name'), 
                    columnName: 'name', 
                    type: 'string' 
                },
                { 
                    name: Lang.t('label.module'), 
                    columnName: 'module_id',
                    type: 'select',
                    selectOptions: [
                        { id: MODULE.PURCHASE, label: Lang.t('label.purchase_order') },
                        { id: MODULE.PURCHASE_RETURN, label: Lang.t('label.purchase_return') },
                        { id: MODULE.DAMAGE, label: Lang.t('label.damage') },
                        { id: MODULE.SALES, label: Lang.t('label.sales') },
                        { id: MODULE.SALES_RETURN, label: Lang.t('label.sales_return') },
                        { id: MODULE.STOCK_REQUEST, label: Lang.t('label.stock_request') },
                        { id: MODULE.STOCK_DELIVERY, label: Lang.t('label.stock_delivery') },
                        { id: MODULE.STOCK_RETURN, label: Lang.t('label.stock_return') },
                        { id: MODULE.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound') },
                        { id: MODULE.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound') },
                        { id: MODULE.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound') },
                        { id: MODULE.SALES_OUTBOUND, label: Lang.t('label.sales_outbound') },
                        { id: MODULE.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound') },
                        { id: MODULE.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound') },
                        { id: MODULE.STOCK_DELIVERY_INBOUND, label: Lang.t('label.stock_delivery_inbound') },
                        { id: MODULE.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound') },
                        { id: MODULE.STOCK_RETURN_INBOUND, label: Lang.t('label.stock_return_inbound') },
                        { id: MODULE.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust') },
                        { id: MODULE.COUNTER, label: Lang.t('label.counter') },
                    ],
                },
            ],
            data: []
        },

        message: new Notification,

        confirm: new Data({
            value: false,
            visible: false,
            id: 0,
            index: 0
        }),
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.filter.visible = false;
            this.processing = true;
            this.message.reset();

            PrintoutTemplateService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.filter.data
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.templates;
                that.datatable.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        create() {
            PrintoutTemplateService.create();
        },

        edit(id) {
            if(! permissions.view_detail && ! permissions.update) {
                return false;
            }
            
            PrintoutTemplateService.edit(id);
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            
            PrintoutTemplateService.destroy(this.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.confirm.reset();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.reset();
                });
        },

        confirmation(data, index) {
            this.confirm.visible = true;
            this.confirm.id = data.id;
            this.confirm.index = index;
        }
    },
});
