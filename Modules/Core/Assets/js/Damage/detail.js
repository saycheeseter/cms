import Invoice from '../Invoice/detail.js';
import Outbound from '../Common/OutboundMixins.js';
import DamageService from '../Services/DamageService';

let app = new Vue({
    el: '#app',

    mixins: [Invoice, Outbound],

    data: {
        reference: {
            reasons: reasons,
        },

        attachment: {
            module: MODULE.DAMAGE
        },

        service: new DamageService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.DAMAGE,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.DAMAGE
        }
    },

    created() {
        this.createInfo({
            reason_id: '',
        });
    },

    methods: {
        getExtraInfoForUpdateApproved() {
            return [
                'reason_id'
            ];
        }
    }
});
