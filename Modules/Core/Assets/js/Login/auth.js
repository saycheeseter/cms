
import CoreMixins from '../Common/CoreMixins';
import AuthService from '../Services/AuthService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        modal: {
            visible: false
        },
        credentials: new Data({
            username: '',
            password: ''
        }),
        branch: new Data({
            id: 0
        }),
        message: new Notification(['form', 'branch']),
        branches: []
    },

    mounted() {
        this.$refs.username.focus();
    },

    methods: {
        login() {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.clearRevokeMessage();
            that.processing = true;
            that.message.reset();

            AuthService.login(this.credentials.data())
                .then(function (response) {
                    that.branches = response.data.values.branches;
                    that.processing = false;

                    if (that.branches.length === 1) {
                        that.directlyAuthenticate();
                    } else {
                        that.branch.id = that.branches[0].id;
                        that.modal.visible = true;

                        that.$nextTick(() => {
                            that.$refs.authenticate.focus();
                        });
                    }
                })
                .catch(function (error) {
                    that.message.form.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },
        authenticate() {
            let that = this;

            if (that.processing) {
                return false;
            }        

            that.message.reset();
            that.processing = true;

            AuthService.authenticate(this.branch.data())
                .then(function(response){
                    that.message.branch.success = response.data.message;
                    window.location = '/dashboard';
                })
                .catch(function (error) {
                    that.message.branch.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        directlyAuthenticate() {
            this.branch.id = this.branches[0].id;
            this.authenticate();
        },

        clearRevokeMessage() {
            if (this.$refs.revokeMessage !== undefined) {
                this.$refs.revokeMessage.close();
            }
        }
    },
}); 