import Datatable from './Components/Datatable/Datatable.vue'
import TableHeader from './Components/Datatable/TableHeader.vue'
import TableInput from './Components/Datatable/TableInput.vue'
import DatatableMixins from './Components/Datatable/DatatableMixins.js'

window.Datatable = Datatable;
window.TableHeader = TableHeader;
window.TableInput = TableInput;
window.DatatableMixins = DatatableMixins;