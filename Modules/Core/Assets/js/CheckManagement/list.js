import CoreMixins from '../Common/CoreMixins';

import CollectionService from '../Services/CollectionService';
import PaymentService from '../Services/PaymentService';
import OtherPaymentService from '../Services/OtherPaymentService';
import OtherIncomeService from '../Services/OtherIncomeService';


export default {
    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        transfer_date: new Date(),
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withActionCell: false,
                perPage: 10,
                withSelect: {
                    value: 'id',
                }
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            confirm: {
                visible: false,
                value: false,
                id: 0,
                index: 0
            },
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_from',
                        type: 'select',
                        selectOptions: branches,
                        selected: true,
                        default: { value: Laravel.auth.branch },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    { name: Lang.t('label.transaction_date'), columnName: 'transaction_date', type:'date' },
                    {
                        name: Lang.t('label.bank'),
                        columnName: 'bank_account_id',
                        type: 'select',
                        selectOptions: bankAccounts
                    },
                    { name: Lang.t('label.check_no'), columnName: 'check_no', type:'string' },
                    { name: Lang.t('label.check_date'), columnName: 'check_date', type:'date' },
                    { name: Lang.t('label.remarks'), columnName: 'remarks', type:'string' },
                    { name: Lang.t('label.transfer_date'), columnName: 'transfer_date', type:'date' },
                    {
                        name: Lang.t('label.check_status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: CHECK_STATUS.PENDING, label: Lang.t('label.pending') },
                            { id: CHECK_STATUS.TRANSFERRED, label: Lang.t('label.transferred') },
                        ]
                    },
                ],
                data: []
            },
            columnSettings: {
                visible: false,
                columns: [
                    { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                    { alias: 'created_from', label: Lang.t('label.created_from'), default: true },
                    { alias: 'bank', label: Lang.t('label.bank'), default: true },
                    { alias: 'check_date', label: Lang.t('label.check_date'), default: true },
                    { alias: 'transfer_date', label: Lang.t('label.transfer_date'), default: true },
                    { alias: 'amount', label: Lang.t('label.amount'), default: true },
                    { alias: 'remarks', label: Lang.t('label.remarks'), default: true }
                ],
                selections: []
            },
            detail: {
                visible: false
            }
        },
        service: '',
        message: new Notification()
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.visible = false;
            this.processing = true;
            this.message.reset();

            this.service.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data
            })
            .then(function (response) {
                that.datatable.value.data = response.data.values.checks;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        transfer() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.confirm.visible = false;
            this.processing = true;
            this.message.reset();

            this.service.transfer({
                ids: that.datatable.selected,
                transfer_date: Moment(this.transfer_date).format('YYYY-MM-DD')
            })
            .then(function (response) {
                that.processing = false;
                that.paginate(that.datatable.value.pagination.current_page);
                that.message.info = response.data.message;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        redirect(type, id) {
            let service = '';

            if(type === CHECK_TRANSACTION_TYPE.PAYMENT) {
                service = new PaymentService;
            } else if (type === CHECK_TRANSACTION_TYPE.COLLECTION) {
                service = new CollectionService;
            } else if (type === CHECK_TRANSACTION_TYPE.OTHER_PAYMENT) {
                service = OtherPaymentService;
            } else if (type === CHECK_TRANSACTION_TYPE.OTHER_INCOME) {
                service = OtherIncomeService;
            }

            service.view(id);
        }
    },
}