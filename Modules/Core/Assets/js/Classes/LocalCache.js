export default class LocalCache {
    static set(key, value){
        if (!key || !value) {return;}

        if(typeof value === "object") {
            value = JSON.stringify(value);
        }

        localStorage.setItem(key, value);
    }

    static get(key) {
        let value = localStorage.getItem(key);

        if (!value) {return;}

        if (value[0] === "{") {
            value = JSON.parse(value);
        }
      
        return value;
    }

    static remove(key) {
        localStorage.removeItem(key);
    }
}