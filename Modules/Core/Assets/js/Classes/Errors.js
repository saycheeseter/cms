export default class Errors {
    constructor() {
        this.value = {}
    }

    has(field) {
        return this.value.hasOwnProperty(field);
    }

    any() {
        return Object.keys(this.value).length > 0;
    }

    get(field = null) {

        if (field === null) {
            return this.value;
        }

        if (this.value[field]) {

            let value = this.value[field];

            if (typeof this.value[field] !== 'string') {
                
                value = '';

                for (let key in this.value[field]) {
                    value += "\n" + this.value[field][key]; 
                }
            }

            return value;
        }
    }

    set(value) {
        this.value = value;
    }
    
    clear(field) {
        if (field) {
            delete this.value[field];
            return;
        }

        this.value = {};
    }
}