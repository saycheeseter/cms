import QueryString from 'qs';
import * as _ from "lodash";
import Arr from "./Arr";

export default class QueryStringLocation {
    constructor(mappings = {}) {
        this.mappings = mappings;
        this.queryString = window.location.search;
    }

    toString() {
        return this.queryString;
    }

    toArray() {
        return QueryString.parse(this.queryString, { ignoreQueryPrefix: true });
    }

    setDefaultOnFilters(existing) {
        let query = this.toArray();

        if (!query.hasOwnProperty('filters')) {
            return;
        }

        let filters = query.filters;
        let list = {};

        for (let i in filters) {
            let filter = filters[i];

            for (let property in this.mappings) {
                let mapping = this.mappings[property];
                let columns = [];
                let type = null;

                if (_.isString(mapping)) {
                    columns = Arr.wrap(mapping);
                } else if (mapping instanceof Object) {
                    if (mapping.hasOwnProperty('column')) {
                        columns = _.isString(mapping.column)
                            ? Arr.wrap(mapping.column)
                            : mapping.column;

                        type = mapping.type;
                    }
                } else {
                    columns = mapping;
                }

                if (!Arr.in(filter.column, columns)) {
                    continue;
                }

                if (! list.hasOwnProperty(property)) {
                    list[property] = [];
                }

                let value;

                if (Arr.in(type, ['date', 'datetime']) && this._isYearMonthFormat(filter.value)) {
                    if (Arr.in(filter.operator, ['=', '<>'])) {
                        let operator = {
                            start: filter.operator === '=' ? '>=' : '<',
                            end: filter.operator === '=' ? '<=' : '>'
                        };

                        list[property].push(
                            {
                                comparison: operator.start,
                                value: this._parse(Moment(filter.value).startOf('month').format(), type)
                            },
                            {
                                comparison: operator.end,
                                value: this._parse(Moment(filter.value).endOf('month').format(), type)
                            }
                        );

                        break;
                    }

                    value = this._parse(this._convertYearMonthToDateByOperator(filter.value, filter.operator));
                } else {
                    value = this._parse(filter.value, type);
                }

                list[property].push({
                    comparison: filter.operator,
                    value: value
                });

                break;
            }
        }

        for (let i = 0; i < existing.length; i++) {
            let item = existing[i];

            if (item.hasOwnProperty('selected')) {
                existing[i].selected = false;
            }

            if (!list.hasOwnProperty(item.columnName)) {
                continue;
            }

            existing[i]['default'] = list[item.columnName];
            existing[i].selected = true;
        }
    }

    _convertYearMonthToDateByOperator(value, operator) {
        let date = Moment(value);

        switch(operator) {
            case '<':
            case '>=':
                date = date.startOf('month');
                break;

            case '>':
            case '<=':
                date = date.endOf('month');
                break;
        }

        return date.format();
    }

    _isYearMonthFormat(value) {
        // Dirty fix as of now
        return value.split('-').length === 2;
    }

    _parse(value, format) {
        let result = '';

        switch (format) {
            case 'date':
                result = Moment(value).format('YYYY-MM-DD');
                break;

            case 'datetime':
                result = Moment(value).format();
                break;

            case 'number':
                result = Number(value);
                break;

            default:
                result = value;
                break;
        }

        return result;
    }
}