export default class Notification {
    constructor(properties = []) {
        this.source = properties;

        for (let i = 0; i < properties.length; i++) {
            this[properties[i]] = {
                success: '',
                info: '',
                error: new Errors
            }
        }

        if (properties.length === 0) {
            this.success = '';
            this.info = '';
            this.error = new Errors;
        }
    }

    reset (property = '') {
        if (property !== '') {
           this.clear(property);
           return this;
        }

        for (let i = 0; i < this.source.length; i++) {
            this.clear(this.source[i]);
        }

        if (this.source.length === 0) {
            this.success = '';
            this.info = '';
            this.error.set({});
        }

        return this;
    }

    clear(property) {
        this[property].success = '';
        this[property].info = '';
        this[property].error.set({});
    }
}