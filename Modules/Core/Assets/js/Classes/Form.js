export default class Form {
    constructor(data) {
        this.sourceData = data;

        for (let field in data) {
            this[field] = data[field]
        }

        this.errors = new Errors();
    }

    set(data) {
        for (let property in data) {
            this[property] = data[property];
        }

        return this;
    }

    data () {
        let data = {};

        for (let property in this.sourceData) {
            data[property] = this[property];
        }

        return data;
    }

    parse(format) {

        for (let property in format) {
            
            if (this[property] === '') {
                continue;
            }

            switch(format[property]) {
                case 'dateTime':
                    var date = new Date(this[property]);
                    this[property] = date.getFullYear() + "-" 
                        + ('0' + (date.getMonth() + 1)).slice(-2) + "-" 
                        + ('0' + (date.getDate())).slice(-2) + " " 
                        + date.getHours() + ":" 
                        + date.getMinutes() + ":" 
                        + date.getSeconds();
                    break;

                case 'date':
                    var date = new Date(this[property]);
                    this[property] = date.getFullYear() + "-" 
                        + ('0' + (date.getMonth() + 1)).slice(-2) + "-" 
                        + ('0' + (date.getDate())).slice(-2);
                    break;

                case 'string':
                    this[property] = String(this[property]);
                    break;

                case 'boolean':
                    this[property] = Boolean(this[property]);
                    break;

                case 'numeric':
                    let str = String(this[property]).replace(/\,/g,'');
                    this[property] = Number(str);
                    break;
            }
        }

        return this;
    }

    reset () {
        for (let property in this.sourceData) {
            this[property] = this.sourceData[property];
        }

        this.errors.clear();

        return this;
    }
}