import Obj from "./Obj";

export default class Data {
    constructor(data) {
        this.sourceData = Obj.copy(data);

        for (let field in data) {
            this[field] = data[field];
        }
    }

    set(data) {
        for (let property in data) {
            this[property] = data[property];
        }

        return this;
    }

    data () {
        let data = {};

        for (let property in this.sourceData) {
            data[property] = this[property];
        }

        return data;
    }

    reset () {
        let copy = Obj.copy(this.sourceData);

        for (let property in copy) {
            this[property] = copy[property];
        }

        return this;
    }

    remove (key) {
        if (!this.sourceData.hasOwnProperty(key) || this.hasOwnProperty(key)) {
            return false;
        }

        delete this.sourceData[key];
        delete this[key];
    }
}