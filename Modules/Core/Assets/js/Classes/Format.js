import Numeric from './Numeric.js';

let instance = null;

export default class Format {

    constructor() {
        if (instance) {
            return instance;
        }

        this.numeric = new Numeric;
    }

    static instance() {
        return new Format;
    }

    make(data, format) {

        var holder = this._parseHolder(data);

        for (var i = 0; i < holder.length; i++) {
            for (let property in format) {
                if (holder[i][property] == '' || holder[i][property] == null) {
                    continue;
                }

                switch(format[property]) {
                    case 'dateTime':
                        holder[i][property] = this._parseDateTime(holder[i][property]);
                        break;

                    case 'date':
                        holder[i][property] = this._parseDate(holder[i][property]);
                        break;

                    case 'string':
                        holder[i][property] = this._parseString(holder[i][property]);
                        break;

                    case 'boolean':
                        this._parseBoolean(holder[i][property]);
                        break;

                    case 'numeric':
                        holder[i][property] = this._parseNumeric(holder[i][property]);
                        break;
                }
            }
        }

        if (data instanceof Array) {
            return holder;
        }

        return holder[0];
    }

    _parseDateTime(data) {
        var date = new Date(data);

        return date.getFullYear() + "-" 
            + ('0' + (date.getMonth() + 1)).slice(-2) + "-" 
            + ('0' + (date.getDate())).slice(-2) + " " 
            + date.getHours() + ":" 
            + date.getMinutes() + ":" 
            + date.getSeconds();
    }

    _parseDate(data) {
        var date = new Date(data);

        return date.getFullYear() + "-" 
            + ('0' + (date.getMonth() + 1)).slice(-2) + "-" 
            + ('0' + (date.getDate())).slice(-2);
    }

    _parseString(data) {
        return data === null ? data : String(data);
    }

    _parseBoolean(data) {
        return Boolean(data);
    }

    _parseNumeric(data) {
        return this.numeric.parse(data);
    }

    _parseHolder(data) {
        var holder = [];

        if (data instanceof Array) {
            for (var i = 0; i < data.length; i++) {
                holder.push(Object.assign({}, data[i]));
            }
        } else {
            holder = [Object.assign({}, data)]
        }

        return holder;
    }
}
