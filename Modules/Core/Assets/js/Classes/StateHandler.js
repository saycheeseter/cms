export default class StateHandler {
    constructor() {
        this.STATES = {
            CREATED: 1,
            UPDATED: 2,
            DELETED: 3
        };

        this.state = {
            deleted: [

            ],
            updated: [

            ],
            created: [

            ]
        };
    }

    set(holder, data, page, index) {
        if(holder[page] == null) {
            holder[page] = [];
        }
        holder[page][index] = data;
        // console.log(holder[page][index]);
        return this;
    }

    setDeleted(data, page, index) {
        this.set(this.state.deleted, data, page, index);
        // console.log(this.state.deleted);
        return this;
    }

    setUpdated(data, page, index) {
        this.set(this.state.updated, data, page, index);

        return this;
    }

    setCreated(data, page, index) {
        this.set(this.state.created, data, page, index);
        // console.log(this.state.created);
        return this;
    }

    get(holder = null, page, index){
        if(holder == null) {
            return this.unify();
        } else {
            if(page == null || index == null) {
                return holder;
            } else {
                return holder[page][index];
            }
        }
    }

    getDeleted(page = null, index = null) {
        return this.get(this.state.deleted, page, index);
    }

    getUpdated(page = null, index = null) {
        return this.get(this.state.updated, page, index);
    }

    getCreated(page = null, index = null) {
        return this.get(this.state.created, page, index);
    }

    destroy(holder, page, index) {
        if(holder[page] != undefined) {
            if(holder[page][index] != undefined) {
                holder[page].splice(index, 1);
                if(holder[page].length = 0){
                    holder.splice(page, 1);
                }
            }
        }

        return this;
    }

    destroyDeleted(page, index) {
        this.destroy(this.state.deleted, page, index);

        return this;
    }

    destroyUpdated(page, index) {
        this.destroy(this.state.updated, page, index);

        return this;
    }

    destroyCreated(page, index) {
        this.destroy(this.state.created, page, index);

        return this;
    }

    unify() {
        var unifiedHolder = {};

        for(var holder in this.state) {
            unifiedHolder[holder] = [];
        }

        for(var holder in this.state){
            for(var index in this.state[holder]){
                for(var value in this.state[holder][index]){
                    // unifiedHolder.push({data: this.state[holder][index][value], event: holder});
                    unifiedHolder[holder].push(this.state[holder][index][value]);
                }
            }
        }

        return unifiedHolder;
    }

    clear() {
        for(var holder in this.state)
        {
            this.state[holder] = [];
        }
    }
}
