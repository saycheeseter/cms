export default class Toggle {
    constructor(toggles, active) {
        this.source = toggles;

        toggles.forEach(toggle => {
            this[toggle.name] = {
                active: toggle.name === active
            };

            for(let property in toggle) {
                if (property === 'name') {
                    continue;
                }

                this[toggle.name][property] = toggle[property];
            }
        });
    }

    active(name) {
        this.source.forEach(toggle => {
            this[toggle.name].active = name === toggle.name;
        });
    }

    get() {
        let data = {};

        this.source.forEach(toggle => {
            data[toggle.name] = this[toggle.name];
        });

        return data;
    }
}