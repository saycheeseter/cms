import Arr from './Arr';
import { cloneDeep } from 'lodash';

export default class Obj {
    static toString(obj, separator = "\n") {
        if (!obj instanceof Object) {
            return false;
        }

        if (obj.length < 1) {
            return false;
        }

        let text = '';
        let result = '';

        for(var property in obj) {
            text += (obj[property] instanceof Array) 
                ? "\n" + Arr.toString(obj[property])
                : "\n" + obj[property];
        }

        return text;
    }

    static toArray(obj) {
        if (!obj instanceof Object) {
            return false;
        }

        if (obj.length < 1) {
            return false;
        }

        let array = [];

        for(var property in obj) {
            if (obj[property] instanceof Array) {
                array = array.concat(obj[property])
            } else {
                array.push(obj[property]);
            }
        }

        return array;
    }

    static sortBy(subject, key) {
        let keys = Object.keys(subject).sort(function(a, b) {
            return (subject[a][key] > subject[b][key]) 
                ? 1 
                : ((subject[b][key] > subject[a][key]) 
                    ? -1 
                    : 0);
        });

        let data = {};

        for (var i = 0; i < keys.length; i++) {
            data[keys[i]] = subject[keys[i]];
        }

        return data;
    }

    static copy(obj) {
        return _.cloneDeep(obj);
    }

    static isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }

        return true;
    }

    static getLength(obj) {
        let size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }

        return size;
    }
}