import Comma from 'comma-number';

let instance = null;

export default class Numeric {

    constructor() {
        if (instance) {
            return instance;
        }

        this.precision = typeof Laravel === 'undefined'
            ? 2
            : Laravel.settings.monetary_precision; 
    }

    static instance() {
        return new Numeric;
    }

    parse(string) {
        let number = String(string).replace(/[^-\d.]/g, '');
        let value = isNaN(number) ? Number(0) : Number(number);

        return value; 
    }

    format(string, precision = null) {
        let number = this.parse(string, precision);

        if (precision === null) {
            precision = this.precision;
        }

        return Comma(number.toFixed(precision));
    }

    // isNumeric(n) {
    //     return !isNaN(parseFloat(n)) && isFinite(n);
    // }
}
