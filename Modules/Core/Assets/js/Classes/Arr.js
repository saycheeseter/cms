import Numeric from './Numeric.js';

export default class Arr {
    /**
     * Converts given data to string with the given separator
     * 
     * @param  Array arr
     * @param  string separator
     * @return string          
     */
    static toString(arr, separator = "\n") {
        if (!arr instanceof Array) {
            return false;
        }

        if (arr.length < 1) {
            return false;
        }
        
        return arr.join(separator);
    }

    /**
     * Extract the given elements within the given base array
     * 
     * @param  Array base     
     * @param  Array extracted
     * @return Array          
     */
    static extract(base, extracted) {
        let that = this;

        return base.filter(function(item) {
            return that.in(item, extracted);
        });
    }

    /**
     * Remove the given elements within the given base array
     *
     * @param  Array base
     * @param  Array extracted
     * @return Array
     */
    static remove(base, extracted) {
        let that = this;

        return base.filter(function(item) {
            return !that.in(item, extracted);
        });
    }

    /**
     * Get the unmatched elements of the 2 given array
     * 
     * @param  Array arr1     
     * @param  Array arr2
     * @return Array          
     */
    static difference(arr1, arr2) {
        var arr1Set = new Set(arr1);
        var arr2Set = new Set(arr2);
        
        var diff1 = arr1.filter(function(item) { 
            return !arr2Set.has(item); 
        });

        var diff2 = arr2.filter(function(item) { 
            return !arr1Set.has(item); 
        });

        return diff1.concat(diff2);
    }

    /**
     * Get the matching elements of the 2 given array
     * 
     * @param  Array arr1     
     * @param  Array arr2
     * @return Array          
     */
    static intersection(arr1, arr2) {
        let that = this;

        return arr1.filter(function(item) { 
            return that.in(item, arr2);
        });
    }

    /**
     * Check if the given element of array is present within the base array
     * 
     * @param  Array base     
     * @param  Array comparison
     * @return Boolean
     */
    static has(base, comparison) {
        var intersection = this.intersection(base, comparison);

        if (intersection.length !== comparison.length) {
            return false;
        }

        var difference = this.difference(comparison, intersection);

        return difference.length === 0;
    }

    /**
     * Merge the two given array without duplicate
     * 
     * @param  Array arr1     
     * @param  Array arr2
     * @return Array
     */
    static merge(arr1, arr2) {
        var intersection = this.intersection(arr1, arr2);
        var difference = this.difference(arr1, arr2);

        return intersection.concat(difference);
    }

    /**
     * Get the elements in the array based on the given key
     * 
     * @param  Array data     
     * @param  int key
     * @param  int except
     * @return Array
     */
    static pluck(data, key, except = null) {
        var plucked = [];

        for (var i = 0; i < data.length; i++) {
            if (i !== except) {
                plucked.push(data[i][key]);
            }
        }

        return plucked;
    }

    /**
     * Regroup the given array based on the given key
     * 
     * @param  Array data
     * @param  int count
     * @return Array
     */
    static groupBy(data, key) {
        let plucked = this.pluck(data, key);
        let groups = this.unique(plucked).sort();
        let set = {};
        let temp = [];

        for (var i = 0; i < groups.length; i++) {
            temp = [];

            for (var x = 0; x < data.length; x++) {
                if (data[x][key] == groups[i]) {
                    temp.push(data[x]);
                }
            }

            set[groups[i]] = temp;
        }

        return set;
    }

    /**
     * Replace the item in the subject array by the given index
     * 
     * @param  int index  
     * @param  Array subject
     * @param  Array data   
     * @return Array        
     */
    static replace(index, subject, data) {
        let sub = subject.slice(0);

        sub[index] = data;

        return sub;
    }

    /**
     * Check if the current element exists in the array
     * 
     * @param  string search
     * @param  array subject
     * @return bool
     */
    static in(search, subject) {
        if (subject.indexOf(search) !== -1) {
            return true;
        }

        return false;
    }

    /**
     * Return only the unique values of the given array
     * 
     * @param  array subject
     * @return array
     */
    static unique(subject) {
        return subject.filter((v, i, a) => a.indexOf(v) === i); 
    }

    /**
     * Sort array by the given key
     * @param  array subject
     * @param  string key
     * @return array
     */
    static sortBy(subject, key) {
        return subject.sort(function(a, b) {
            return (a[key] > b[key]) 
                ? 1 
                : ((b[key] > a[key]) 
                    ? -1 
                    : 0);
        });
    }

    /**
     * Add new data at the beginning of the array
     * 
     * @param  Array subject
     * @param  Object data
     * @return Array
     */
    static unshift(subject, data) {
        let sub = subject.slice(0);

        sub.unshift(data);
        
        return sub;
    }

    /**
     * Return the sum of all number values in the
     * given array
     * 
     * @param  array numbers
     * @return int
     */
    static sum(numbers) {
        return numbers.reduce((a, b) => 
            Numeric.instance().parse(a) + Numeric.instance().parse(b), 
        0);
    }

    static wrap(data) {
        return (data instanceof Array)
            ? data
            : [data];
    }

    static findBy(subject, pairs) {
        var sub = subject;
        var found = [];
        var detach = [];

        for (var pair in  pairs){
            for (var index in sub) {
                if (sub[index][pairs[pair][0]] === pairs[pair][1]){
                    found.push(sub[index]);
                    detach.push(index);
                }
            }
            sub = this.removeSet(sub, detach);
            detach = [];
        }

        return found;
    }

    static removeSet(subject, set = []) {
        for (let i = set.length -1; i >= 0; i--){
            subject.splice(set[i],1);
        }

        return subject;
    }
}