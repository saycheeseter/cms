import * as _ from "lodash";

export default class DateUnit {
    static enumMonths() {
        return [
            { id: 1 , label: Lang.t('label.jan') },
            { id: 2 , label: Lang.t('label.feb') },
            { id: 3 , label: Lang.t('label.mar') },
            { id: 4 , label: Lang.t('label.apr') },
            { id: 5 , label: Lang.t('label.may') },
            { id: 6 , label: Lang.t('label.jun') },
            { id: 7 , label: Lang.t('label.jul') },
            { id: 8 , label: Lang.t('label.aug') },
            { id: 9 , label: Lang.t('label.sep') },
            { id: 10 , label: Lang.t('label.oct') },
            { id: 11 , label: Lang.t('label.nov') },
            { id: 12 , label: Lang.t('label.dec') },
        ];
    }

    static months(type = 'string') {
        let value = [];

        switch (type) {
            case 'string':
                value =  [
                    Lang.t('label.jan'),
                    Lang.t('label.feb'),
                    Lang.t('label.mar'),
                    Lang.t('label.apr'),
                    Lang.t('label.may'),
                    Lang.t('label.jun'),
                    Lang.t('label.jul'),
                    Lang.t('label.aug'),
                    Lang.t('label.sep'),
                    Lang.t('label.oct'),
                    Lang.t('label.nov'),
                    Lang.t('label.dec'),
                ];
                break;

            case 'int':
                value = _.range(1, 13);
                break;
        }

        return value;
    }

    static monthLabel(value) {
        let months = this.enumMonths();

        for (let i = 0; i < months.length; i++) {
            if (months[i].id === value) {
                return months[i].label;
            }
        }

        return null;
    }

    static years(start = 1970) {
        let years = [];
        let current = (new Date).getFullYear();

        for (let i = 0; ((i + start)) <= current; i++) {
            years[i] = (current - i);
        }

        return years;
    }

    static enumYears(start = 1970) {
        let years = [];
        let current = (new Date).getFullYear();

        for (let i = 0; ((i + start)) <= current; i++) {
            years[i] = { id: (current - i), label: (current - i)};
        }

        return years;
    }
}