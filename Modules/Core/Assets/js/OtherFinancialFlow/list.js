import CoreMixins from '../Common/CoreMixins';
import QueryStringLocation from "../Classes/QueryStringLocation";

export default {
    mixins: [CoreMixins],

    components: {
        Datepicker,
    },

    data: {
        datatable: {
            list: {
                id: 'table-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            detail: {
                id: 'table-modal',
                settings: {
                    withPaging: true,
                    withPaginateCallBack: false,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },

        modal: {
            list: {
                confirm: new Data({
                    visible: false,
                    value: false,
                    id: 0,
                    index: 0
                })
            },
            detail: {
                visible: false,
                id: 'modal-detail',
                confirm: new Data({
                    visible: false,
                    value: false,
                    index: 0
                })
            },
        },
        record: {
            info: new Data({
                id: 0,
                reference_name: '',
                remarks: '',
                transaction_date:  Moment().format(),
                payment_method: 0,
                bank_account_id: null,
                check_number: '',
                check_date: ''
            }),
            detail: new Data({
                id: 0,
                amount: '',
                type: 0
            })
        },

        format: {
            info: {
                transaction_date: 'date',
                check_date: 'date'
            },
            detail: {
                amount: 'numeric'
            }
        },

        message: new Notification(['list', 'detail']),

        chosen: {
            payment_methods: paymentMethods,
            bank_accounts: bankAccounts,
            types: types
        },

        method: PAYMENT_METHOD,

        service: '',

        validation: {
            rules: {
                info: {
                    transaction_date: 'required',
                    payment_method: 'required',
                    reference_name: 'required'
                },
                detail: {
                    type: 'required',
                    amount: 'required'
                }
            },
            messages: {
                info: {
                    'transaction_date.required': Lang.t('validation.transaction_date_required'),
                    'payment_method.required': Lang.t('validation.payment_method_required'),
                },
                detail: {
                    'type.required': Lang.t('validation.type_required'),
                    'amount.required': Lang.t('validation.amount_required')
                }
            }
        }
    },

    created: function() {
        (new QueryStringLocation({
            created_from: 'created_for',
            transaction_date: {
                type: 'datetime',
                column: 'year_month'
            }
        }))
        .setDefaultOnFilters(this.modal.filter.options);
    },

    mounted: function() {
        if(id != null) {
            this.processing = false;
            this.show(id);
        } else {
            this.paginate();
        }
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            this.modal.filter.visible = false;

            this.service.paginate({
                'perPage': that.datatable.list.settings.perPage,
                'page': page,
                'filters': that.modal.filter.data
            })
            .then(function (response) {
                that.message.list.info = response.data.message;
                that.datatable.list.value.data = response.data.values.data;
                that.datatable.list.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.store(this.data)
                .then(function (response){
                    that.message.detail.success = response.data.message;
                    that.processing = false;

                    setTimeout(function(){
                        that.modal.detail.visible = false;
                        that.datatable.list.value.data.push(response.data.values.data);
                        that.computePagination(that.datatable.list.value.pagination, 'add');
                    }, 2000);
                })
                .catch(function(error) {
                    that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.update(this.data, that.record.info.id)
                .then(function (response){
                    that.message.detail.success = response.data.message;
                    that.processing = false;

                    setTimeout(function(){
                        that.modal.detail.visible = false;
                        that.paginate(that.datatable.list.value.pagination.current_page);
                    }, 2000);
                })
                .catch(function(error) {
                    that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.destroy(that.modal.list.confirm.id)
                .then(function (response) {
                    that.message.list.success = response.data.message;
                    that.processing = false;
                    that.modal.list.confirm.visible = false;

                    that.datatable.list.value.data.splice(that.modal.list.confirm.index, 1);
                    that.computePagination(that.datatable.list.value.pagination, 'delete');
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            this.processing = true;

            this.service.show(id)
                .then(function (response) {
                    that.record.info.set(response.data.values.data);
                    that.datatable.detail.value.data = response.data.values.details.data;
                    that.datatable.detail.value.pagination = response.data.values.details.meta.pagination;
                    that.processing = false;
                    that.modal.detail.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        confirm(data, index) {
            this.modal.list.confirm.visible = true;
            this.modal.list.confirm.id = data.id;
            this.modal.list.confirm.index = index;
        },

        validate() {
            if (this.record.info.id != 0 && !permissions.update) {
                return false;
            }

            if(!this.finalized('detail')) {
                this.message.detail.error.set([Lang.t('validation.finalize_all_rows')]);
                return false;
            }

            var validation = Validator.make(
                this.record.info.data(),
                this.validation.rules.info,
                this.validation.messages.info
            );

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();
            
            if (this.record.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        createDetail() {
            let data = Object.assign({}, this.record.detail.data());

            if (!this.isValidDetail(data)) {
                return false;
            }

            this.datatable.detail.value.data.push(data);
            this.record.detail.reset();
        },

        updateDetail(data, index) {
            if (!this.isValidDetail(data)) {
                return false;
            }

            this.changeState(index, 'destroy', 'detail');
        },

        isValidDetail(data) {
            let validation = Validator.make(
                Format.make(data, this.format.detail),
                this.validation.rules.detail,
                this.validation.messages.detail
            );

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();

            return true;
        },

        confirmDetail(data, index, dataIndex) {
            this.modal.detail.confirm.visible = true;
            this.modal.detail.confirm.index = dataIndex;
        },

        destroyDetail() {
            this.datatable.detail.value.data.splice(this.modal.detail.confirm.index, 1);
            this.modal.detail.confirm.visible = false;
        },
    },

    watch: {
        'modal.list.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.list.confirm.reset();
            }
        },
        'modal.detail.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.detail.confirm.reset();
            }
        },
        'modal.detail.visible': function(visible) {
            if (!visible) {
                this.record.info.reset();
                this.record.detail.reset();
                this.datatable.detail.value.data = [];
                this.message.reset();
            }
        },
        'record.info.payment_method': function(value) {
            if(Arr.in(value, [PAYMENT_METHOD.CHECK, PAYMENT_METHOD.BANK_DEPOSIT])) {
                if (this.record.info.bank_account_id === null) {
                    this.record.info.bank_account_id = 0;
                }
            } else {
                this.record.info.bank_account_id = null;
            }
        }
    },

    computed: {
        totalAmount: function () {
            let total = 0;
            let data = this.datatable.detail.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['amount']);
            }

            return Numeric.format(total);
        },
        data: function() {
            return {
                info: Format.make(this.record.info.data(), this.format.info),
                details: Format.make(this.datatable.detail.value.data, this.format.detail)
            }
        }
    }
}