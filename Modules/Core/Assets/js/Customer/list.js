import CoreMixins from '../Common/CoreMixins';
import CustomerService from '../Services/CustomerService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            list: {
                id: 'table-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            detail: {
                id: 'table-modal',
                settings: {
                    withPaging: true,
                    withPaginateCallBack: false,
                    perPage: 10,
                    withDelete: permissions.delete,
                    withEdit: permissions.update,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            }
        },
        modal: {
            list: {
                confirm: new Data({
                    visible: false,
                    id: 0,
                    index: 0
                })
            },
            detail: {
                visible: false,
                id: 'modal-detail',
                confirm: new Data({
                    visible: false,
                    index: 0
                })
            },
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string', optionBool: false},
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false}
                ],
                data: []
            },
            update: {
                visible: false,
                id: 'modal-update',
                confirm: new Data({
                    visible: false,
                    index: 0
                })
            },
        },
        tabs: new Toggle([
            { name: 'info', label: Lang.t('label.customer_info') },
            { name: 'branches', label: Lang.t('label.customer_branches') }
        ], 'info'),
        customer: {
            info: new Data({
                id: 0,
                name: '',
                code: '',
                memo: '',
                credit_limit: 0,
                pricing_type: SELLING_PRICE_BASED_ON.WHOLESALE,
                pricing_rate: 0,
                historical_change_revert: 1,
                historical_default_price: 0,
                salesman_id: 0,
                website: '',
                term: 0,
                status: 1,
            }),
            branch: new Data({
                id: 0,
                name: '',
                address: '',
                chinese_name: '',
                owner_name: '',
                contact: '',
                landline: '',
                fax: '',
                mobile: '',
                email: '',
                contact_person: '',
                tin: ''
            }),
            previous_pricing_type: '0',
        },
        state: new Toggle([
            { name: 'create' },
            { name: 'update', index: 0 }
        ], 'create'),
        message: new Notification(['list', 'detail']),
        chosen: {
            salesmen: salesmen,
            priceBasis: [
                { id: SELLING_PRICE_BASED_ON.HISTORICAL, label: Lang.t('label.historical_record') },
                { id: SELLING_PRICE_BASED_ON.WHOLESALE, label: Lang.t('label.wholesale_price') },
                { id: SELLING_PRICE_BASED_ON.RETAIL, label: Lang.t('label.retail_price') },
                { id: SELLING_PRICE_BASED_ON.PERCENT_PURCHASE, label: Lang.t('label.percent_purchase') },
                { id: SELLING_PRICE_BASED_ON.PERCENT_WHOLESALE, label: Lang.t('label.percent_wholesale') },
                { id: SELLING_PRICE_BASED_ON.PRICE_A, label: Lang.t('label.price_a') },
                { id: SELLING_PRICE_BASED_ON.PRICE_B, label: Lang.t('label.price_b') },
                { id: SELLING_PRICE_BASED_ON.PRICE_C, label: Lang.t('label.price_c') },
                { id: SELLING_PRICE_BASED_ON.PRICE_D, label: Lang.t('label.price_d') },
                { id: SELLING_PRICE_BASED_ON.PRICE_E, label: Lang.t('label.price_e') },
            ] 
        },
        format: {
            info: {
                credit_limit: 'numeric',
                pricing_rate: 'numeric',
                term: 'numeric'
            }
        },
        columnSettings: {
            customer: {
                visible: false,
                columns: [
                    { alias: 'name', label: Lang.t('label.name'), default: true },
                    { alias: 'salesman', label: Lang.t('label.salesman'), default: false },
                    { alias: 'term', label: Lang.t('label.term'), default: false },
                    { alias: 'website', label: Lang.t('label.company_website'), default: false },
                    { alias: 'memo', label: Lang.t('label.memo'), default: false },
                    { alias: 'pricingType', label: Lang.t('label.pricing_type'), default: false },
                    { alias: 'creditLimit', label: Lang.t('label.credit_limit'), default: false },
                    { alias: 'status', label: Lang.t('label.status'), default: false },
                ],
                selections: {},
                module: MODULE.CUSTOMER
            }
        }
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            CustomerService.paginate({
                perPage: that.datatable.list.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.list.info = response.data.message;
                that.datatable.list.value.data = response.data.values.customers;
                that.datatable.list.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.list.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            CustomerService.store({
                info: Format.make(that.customer.info.data(), that.format.info),
                details: that.datatable.detail.value.data
            })
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.datatable.list.value.data.push(response.data.values.customer);
                    that.computePagination(that.datatable.list.value.pagination, 'add');
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.update.confirm.visible = false;
            this.processing = true;
            this.message.reset();

            CustomerService.update({
                info: Format.make(that.customer.info.data(), that.format.info),
                details: that.datatable.detail.value.data
            }, that.customer.info.id)
            .then(function (response){
                that.message.detail.success = response.data.message;
                that.processing = false;

                setTimeout(function(){
                    that.modal.detail.visible = false;
                    that.paginate(that.datatable.list.value.pagination.current_page);
                }, 2000);
            })
            .catch(function(error) {
                that.message.detail.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            CustomerService.destroy(that.modal.list.confirm.id)
                .then(function (response) {
                    that.message.list.success = response.data.message;
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                    that.datatable.list.value.data.splice(that.modal.list.confirm.index, 1);
                    that.computePagination(that.datatable.list.value.pagination, 'delete');
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.list.confirm.visible = false;
                });
        },

        show(id) {
            if (this.processing || !permissions.view_detail) {
                return false;
            }

            let that = this;

            this.processing = true;

            CustomerService.show(id)
                .then(function (response) {
                    that.customer.info.set(response.data.values.customer);
                    that.datatable.detail.value.data = response.data.values.details;
                    that.customer.previous_pricing_type = response.data.values.customer.pricing_type;
                    that.processing = false;
                    that.modal.detail.visible = true;
                })
                .catch(function (error) {
                    that.message.list.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        confirm(data, index) {
            this.modal.list.confirm.visible = true;
            this.modal.list.confirm.id = data.id;
            this.modal.list.confirm.index = index;
        },

        validate() {
            if (this.customer.info.id != 0 && !permissions.update) {
                return false;
            }

            var validation = Validator.make(Format.make(this.customer.info.data(), this.format.info), {
                name: 'required',
                credit_limit: 'numeric',
                pricing_rate: 'numeric',
                term: 'numeric'
            }, {
                'name.required': Lang.t('label.customer_detail') + ' : ' + Lang.t('validation.name_required'),
                'credit_limit.numeric': Lang.t('label.advance_info') + ' : ' + Lang.t('validation.credit_limit_numeric'),
                'pricing_rate.numeric': Lang.t('label.advance_info') + ' : ' + Lang.t('validation.pricing_rate_numeric'),
                'term.numeric': Lang.t('label.advance_info') + ' : ' + Lang.t('validation.term_numeric')
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if (this.customer.info.id === 0) {
                this.store();
            } else {
                if(
                    this.customer.previous_pricing_type != SELLING_PRICE_BASED_ON.HISTORICAL
                    && this.customer.info.pricing_type == SELLING_PRICE_BASED_ON.HISTORICAL
                ){
                    this.modal.update.confirm.visible = true;
                } else {
                    this.update();
                }
            }
        },

        //Customer Branches Functions
        createBranch() {
            if (!this.isValidDetail()) {
                return false;
            }

            let data = Object.assign({}, this.customer.branch.data());

            this.datatable.detail.value.data.push(data);
            this.customer.branch.reset();
        },

        editBranch(branch) {
            this.state.update.index = branch.dataIndex;
            this.customer.branch.set(branch.data);
            this.state.active('update');
        },

        updateBranch() {
            if (!this.isValidDetail()) {
                return false;
            }

            let data = this.customer.branch.data();

            for (let property in data) {
                this.datatable.detail.value.data[this.state.update.index][property] = data[property];
            }

            this.customer.branch.reset();
            this.state.active('create');
        },

        confirmBranch(data, index, dataIndex) {
            this.modal.detail.confirm.visible = true;
            this.modal.detail.confirm.index = dataIndex;
        },

        destroyBranch() {
            this.datatable.detail.value.data.splice(this.modal.detail.confirm.index, 1);
            this.modal.detail.confirm.visible = false;
        },

        clearBranchFields() {
            this.state.active('create');
            this.customer.branch.reset();
        },

        initial() {
            if (this.datatable.detail.value.data.length === 0) {
                this.customer.branch.name = 'Main Office';
                this.datatable.detail.value.data.push(this.customer.branch.data());
                this.customer.branch.reset();
            }
        },

        isValidDetail() {
            let except = this.state.update.active ? this.state.update.index : -1;
            let names = Arr.pluck(this.datatable.detail.value.data, 'name', except).join(',');

            let rules = {
                name: 'required' + (names.length > 0 ? '|not_in:' + names : ''),
                email: this.customer.branch.email !== '' ? 'email' : ''
            };

            let validation = Validator.make(this.customer.branch.data(), rules, {
                'name.required': Lang.t('label.customer_branches') + ' : ' + Lang.t('validation.name_required'),
                'name.not_in': Lang.t('label.customer_branches') + ' : ' + Lang.t('validation.name_array_unique'),
                'email.email': Lang.t('label.customer_branches') + ' : ' + Lang.t('validation.email_invalid')
            });

            if (validation.fails()) {
                this.message.detail.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            this.message.reset();

            return true;
        }
    },

    computed: {
        showHistoricalOptions: function() {
            return Number(this.customer.info.pricing_type) === SELLING_PRICE_BASED_ON.HISTORICAL;
        },
        showPricingRate: function() {
            return [
                SELLING_PRICE_BASED_ON.PERCENT_PURCHASE, 
                SELLING_PRICE_BASED_ON.PERCENT_WHOLESALE
            ].indexOf(Number(this.customer.info.pricing_type)) !== -1
        },
    },

    watch: {
        'modal.detail.visible': function(visible) {
            if (visible) {
                this.initial();
            } else {
                this.tabs.active('info');
                this.customer.info.reset();
                this.customer.branch.reset();
                this.datatable.detail.value.data = [];
                this.message.reset();
                this.state.active('create');
            }
        },
        'modal.list.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.list.confirm.reset();
            }
        },
        'modal.detail.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.detail.confirm.reset();
            }
        },
        'customer.info.pricing_type': function(type) {
            if (Number(type) !== SELLING_PRICE_BASED_ON.HISTORICAL) {
                this.customer.info.historical_change_revert = 1;
                this.customer.info.historical_default_price = 0;
            }

            if ([
                SELLING_PRICE_BASED_ON.PERCENT_PURCHASE, 
                SELLING_PRICE_BASED_ON.PERCENT_WHOLESALE
            ].indexOf(Number(type)) === -1) {
                this.customer.info.pricing_rate = 0;
            }
        }
    },
});
