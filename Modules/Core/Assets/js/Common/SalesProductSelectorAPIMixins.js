import ProductService from "../Services/ProductService";

export default {
    methods: {
        getBarcodeScanApi(params){
            return ProductService.findByBarcodeWithSummaryAndCustomerPrice(params);
        },

        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                customer_id: this.info.customer_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                customer_type: this.info.customer_type,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getStockNoApi(params){
            return ProductService.findByStockNoWithSummaryAndCustomerPrice(params);
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                customer_id: this.info.customer_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                customer_type: this.info.customer_type,
                excluded: {
                    status: PRODUCT_STATUS.INACTIVE
                }
            }
        },

        getSelectProductsApi(params) {
            return ProductService.selectWithCustomerPrice(params);
        },

        getSelectProductsApiParams() {
            return {
                product_ids: this.datatable.product.checked,
                customer_id: this.info.customer_id,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                customer_type: this.info.customer_type
            }
        }
    }
}