import LanguageService from '../Services/LanguageService';
import UserMenuService from '../Services/UserMenuService';
import UserService from '../Services/UserService';

export default {
    data: {
        loading: true,
        processing: false,
        sidebar: {
            visible: true,
            toggle: 'opened',
            tabs: new Toggle([
                { name: 'mainMenu', label: Lang.t('label.main_menu') },
                { name: 'customMenu', label: Lang.t('label.custom_menu') },
            ], 'mainMenu')
        },
        locales: [
            { label: 'EN', value: 'en' },
            { label: 'CH', value: 'zh' },
        ],

        custom_menu: customMenu.list,

        modal: {
            changePassword: {
                visible: false,
                credentials: new Data({
                    old: '',
                    new: '',
                    new_confirmation: ''
                }),
                message: new Notification
            },
            helpDesk: {
                visible: false,
                url: ''
            }
        }
    },

    components: {
        Sidebar,
        ControlBar,
        ControlButton,
        ControlDropdown,
        Accordion,
        AccordionLink,
        AccordionGroup,
        Modal,
        Datatable,
        TableHeader,
        TableInput,
        TableCellData,
        FilterModal,
        Message,
        Confirm,
        ColumnSelect,
        Chosen,
        NumberInput,
        DialogBox,
        MyMenu,
    },

    mixins: [DatatableMixins, ChosenMixins],

    mounted() {
        this.loading = false;
        this.setActiveMenu();
        this.addVisibilityAttributeToCustomMenu();
    },

    methods: {
        addVisibilityAttributeToCustomMenu() {
            for (var i = 0; i < this.custom_menu.length; i++) {
                Vue.set(this.custom_menu[i], 'visible', true); 
            }
        },

        toggleCustomMenuSection(index) {
            this.custom_menu[index].visible = (this.custom_menu[index].visible) ? false : true;
        },

        setActiveMenu() {
            if(customMenu.active == ACTIVE_MENU.CUSTOM) {
                this.sidebar.tabs.customMenu.active = true;
                this.sidebar.tabs.mainMenu.active = false;
            } else {
                this.sidebar.tabs.customMenu.active = false;
                this.sidebar.tabs.mainMenu.active = true;
            }
        },

        setLanguage(lang) {
            let that = this;

            this.processing = true;

            LanguageService.switch(lang)
                .then((response) => {
                    this.processing = false;
                    location.reload();
                })
                .catch((error) => {
                    this.processing = false;
                    alert(Lang.t('error.language_switch_failed'));
                });
        },

        toggleSidebar() {
            if(this.sidebar.visible) {
                this.sidebar.visible = false;
                this.sidebar.toggle = 'closed';
            }
            else {
                this.sidebar.visible = true;
                this.sidebar.toggle = 'opened';
            }
        },

        setActiveMenuType($type) {
            this.processing = true;

            UserMenuService.set($type)
                .then((response) => {
                    this.processing = false;
                })
                .catch((error) => {
                    this.processing = false;
                });
        },

        changePassword() {
            let that = this;

            if (!this.isValidPasswordCredentials()) {
                return;
            }

            this.modal.changePassword.message.reset();
            this.processing = true;

            UserService.changePassword(this.modal.changePassword.credentials.data())
                .then(function (response){
                    that.modal.changePassword.visible = false;
                    that.processing = false;
                    that.modal.changePassword.message.success = response.data.message;
                    that.modal.changePassword.credentials.reset();
                })
                .catch(function(error){
                    that.processing = false;
                    that.modal.changePassword.message.error.set(Obj.toArray(error.response.data.errors));
                })
        },

        isValidPasswordCredentials() {
            let validation = Validator.make(
                this.modal.changePassword.credentials.data(),
                {
                    old: 'required',
                    new: 'required|min:6|confirmed'
                },
                {
                    'old.required': Lang.t('validation.old_password_required'),
                    'new.required': Lang.t('validation.new_password_required'),
                    'new.confirmed' : Lang.t('validation.confirm_password_incorrect'),
                    'new.min' : Lang.t('validation.new_password_min_length'),
                }
            );

            if (validation.fails()) {
                this.modal.changePassword.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        },

        openHelpDesk() {
            this.modal.helpDesk.url = Laravel.config.custom.url.helpDesk;
            this.modal.helpDesk.visible = true;
        }

    },

    watch: {
        'sidebar.tabs.customMenu.active': {
            handler: function(active, oldValue) {
                if(active) {
                   this.setActiveMenuType(ACTIVE_MENU.CUSTOM)
                }
            }
        },

        'sidebar.tabs.mainMenu.active': {
            handler: function(active, oldValue) {
                if(active) {
                    this.setActiveMenuType(ACTIVE_MENU.DEFAULT)
                } 
            },
        },
    }
}