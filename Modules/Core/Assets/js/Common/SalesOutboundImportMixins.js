import SalesOutboundService from "../Services/SalesOutboundService";
import { range, rangeRight, flatten, map, uniq, pullAll } from 'lodash';

export default {
    data: {
        modal: {
            salesOutbound: {
                visible: false,
                option: new Data({
                    type: 'transaction-date',
                    date: {
                        from: Moment().startOf('day').format(),
                        to: Moment().endOf('day').format(),
                    },
                    sheetNumber: {
                        prefix: Laravel.settings.sales_outbound_sheet_number_prefix,
                        from: '',
                        to: ''
                    },
                    statuses: []
                }),
                transactions: []
            }
        }
    },

    methods: {
        closeSalesOutboundImport() {
            this.modal.salesOutbound.visible = false;
            this.modal.salesOutbound.option.reset();
            this.modal.salesOutbound.transactions = [];
        },

        importFromSalesOutbound() {
            if (this.processing) {
                return false;
            }

            let params = {
                page: 1,
                per_page: 10,
            };

            let service = new SalesOutboundService;

            switch (this.modal.salesOutbound.option.type) {
                case 'transaction-date':
                    params = Object.assign({}, params, {
                        date_from: this.modal.salesOutbound.option.date.from,
                        date_to: this.modal.salesOutbound.option.date.to,
                        statuses: this.modal.salesOutbound.option.statuses,
                    });
                    break;

                case 'sheet-number':
                    this.modal.salesOutbound.option.sheetNumber.from = Numeric.parse(this.modal.salesOutbound.option.sheetNumber.from);
                    this.modal.salesOutbound.option.sheetNumber.to = Numeric.parse(this.modal.salesOutbound.option.sheetNumber.to);

                    if(!this.isValidSalesOutboundImportSheetRange()) {
                        return false;
                    }

                    let sheets = this.generateSalesOutboundImportSheetRange();

                    params = Object.assign({}, params, {
                        sheets: sheets,
                        statuses: this.modal.salesOutbound.option.statuses
                    });
                    break;
            }

            this.processing = true;

            this.iterateImportFromSalesOutbound(service, this.modal.salesOutbound.option.type, params, 1);
        },

        iterateImportFromSalesOutbound(service, type, params, page) {
            let that = this;
            let request;

            params.page = page;

            switch (type) {
                case 'transaction-date':
                    request = service.getByDateAndStatus(params);
                    break;

                case 'sheet-number':
                    request = service.getBySheetNumberAndStatus(params);
                    break;
            }

            request.then(function (response) {
                if (response.data.values.data.length > 0) {
                    that.modal.salesOutbound.transactions = that.modal.salesOutbound.transactions.concat(response.data.values.data);
                } else {
                    that.message.transaction.info = response.data.message;
                }

                if (page !== response.data.values.meta.pagination.total_pages) {
                    that.iterateImportFromSalesOutbound(service, type, params, (page + 1));
                } else {
                    that.selectProductsFromSalesOutboundImport();
                }
            })
            .catch(function (error) {
                that.closeSalesOutboundImport();
                that.processing = false;
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        selectProductsFromSalesOutboundImport() {
            let that = this;

            let details = map(map(this.modal.salesOutbound.transactions, 'details'), 'data');
            let selected = map(map(flatten(details), 'product'), 'id');
            let current = map(this.datatable.details.value.data, 'product_id');
            let unique = uniq(pullAll(selected, current));

            if (unique.length === 0) {
                this.setSalesOutboundImportData();
                this.closeSalesOutboundImport();
                this.processing = false;
                return;
            }

            this.datatable.product.checked = unique;

            let callback = this.beforeProductSelect();

            if (typeof callback !== 'undefined' && !callback) {
                return false;
            }

            let params = this.getSelectProductsApiParams();

            this.getSelectProductsApi(params)
                .then(function (response) {
                    let details = that.formatProductResponseToTransactionDetails(response.data.values.products);
                    that.datatable.product.checked = [];
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(details);
                    that.setSalesOutboundImportData();
                    that.closeSalesOutboundImport();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        setSalesOutboundImportData() {
            let items = flatten(map(map(this.modal.salesOutbound.transactions, 'details'), 'data'));

            for(let i in items) {
                let item = items[i];

                for(let x in this.datatable.details.value.data) {
                    let current = this.datatable.details.value.data[x];

                    if (current.product_id === item.product.id) {
                        if (current.unit_id !== item.unit.id) {
                            current.qty = Numeric.format(
                                (Numeric.parse(current.qty) * Numeric.parse(current.unit_qty))
                                + (Numeric.parse(item.qty) * Numeric.parse(item.unit_qty))
                            );

                            current.unit_id = Laravel.settings.default_unit;
                            current.unit_qty = 1;
                        } else {
                            current.qty = Numeric.format(Numeric.parse(current.qty) + Numeric.parse(item.qty));
                        }

                        this.setTotalQty(x);
                        break;
                    }
                }
            }
        },

        isValidSalesOutboundImportSheetRange() {
            if ((Numeric.parse(this.modal.salesOutbound.option.sheetNumber.to)
                    - Numeric.parse(this.modal.salesOutbound.option.sheetNumber.from))
                > 100
            ) {
                this.message.transaction.error.set([
                    Lang.t('validation.transaction_count_limited', { number: 100 })
                ]);

                this.closeSalesOutboundImport();
                return false;
            }

            return true;
        },

        generateSalesOutboundImportSheetRange() {
            let to = this.modal.salesOutbound.option.sheetNumber.to;
            let from = this.modal.salesOutbound.option.sheetNumber.from;

            let sheets = Numeric.parse(from) > Numeric.parse(to)
                ? rangeRight(from, (to - 1))
                : range(from, (to + 1));

            let vm = this;

            return map(sheets, function(sheet) {
                return vm.modal.salesOutbound.option.sheetNumber.prefix + '' + sheet;
            });
        }
    },
    computed: {
        salesOutboundByDate: function() {
            return this.modal.salesOutbound.option.type === 'transaction-date';
        },

        salesOutboundBySheetNumber: function() {
            return this.modal.salesOutbound.option.type === 'sheet-number';
        },

        enableSalesOutboundImport: function() {
            return this.modal.salesOutbound.option.type === 'sheet-number';
        },
    }
}
