import ProductService from '../Services/ProductService';

export default {
    data: {
        autoGroupProduct : {
            dialog: {
                message: ''
            },
            indices: [],
            components: [],
        },
    },

    methods: {
        onSuccessfulValidation() {
            if(this.withAutoGroupProduct()) {
                this.autoGroupProduct.dialog.message = Lang.t('info.is_with_automatic_group_products');
                return false;
            }

            return true
        },

        withAutoGroupProduct() {
            let details = this.datatable.details.value.data;

            for (let i = 0; i < details.length; i++) {
                if(GROUP_TYPE.AUTOMATIC_GROUP == details[i].group_type 
                    || GROUP_TYPE.AUTOMATIC_UNGROUP == details[i].group_type
                ) {
                    this.autoGroupProduct.indices.push(i);

                    let components  = details[i].components;
                    for (let x = 0; x < components.length; x++) {
                        this.autoGroupProduct.components.push({
                            qty: Numeric.parse(details[i].qty) * Numeric.parse(components[x].qty),
                            product_id: components[x].product_id
                        }) 
                    }
                }
            }

            return this.autoGroupProduct.components.length > 0;
        },

        breakToComponents() {
            
            let that = this;

            for (let i = this.autoGroupProduct.indices.length -1; i >= 0; i--) {
                this.datatable.details.value.data.splice(this.autoGroupProduct.indices[i],1);
            }

            this.processing = true;

            this.datatable.product.checked = Arr.pluck(this.autoGroupProduct.components, 'product_id');

            let params = this.getSelectProductsApiParams();

            this.getSelectProductsApi(params)
                .then(function (response) {
                    that.processComponentsQty(response.data.values.products);
                    that.resetAutoGroupComponents();
                    that.datatable.product.checked = [];
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.resetAutoGroupComponents();
                    that.datatable.product.checked = [];
                    that.processing = false;
                });
        },

        resetAutoGroupComponents() {
            this.autoGroupProduct.dialog.message = '';
            this.autoGroupProduct.indices = [];
            this.autoGroupProduct.components = [];
        },

        processComponentsQty(details) {
            let collection = [];

            for (let i=0; i < this.autoGroupProduct.components.length; i++) {
                let component = this.autoGroupProduct.components[i];

                for (let d = 0; d < details.length; d++) {
                    let detail = details[d];

                    if (component.product_id === detail.id) {
                        let data = Object.assign({}, detail, {
                            qty: component.qty,
                            unit_id: Laravel.settings.default_unit
                        });

                        collection.push(data);
                    }
                }
            }

            this.datatable.details.value.data = this.datatable.details.value.data.concat(
                this.formatProductResponseToTransactionDetails(collection)
            );
        },
    }
}