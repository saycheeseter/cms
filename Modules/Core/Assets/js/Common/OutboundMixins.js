import ProductService from '../Services/ProductService';

export default {
    methods: {
        checkInventoryForWarning(data) {
            if (this.disabled || Numeric.parse(data.qty) === 0) {
                return false;
            }

            let that = this;
            let product = data.product_id;

            ProductService.summary({
                product_id: product,
                branch_id: this.info.created_for,
                location_id: this.info.for_location
            })
            .then((response) => {
                that.$set(data, 'inventory_warning', (
                    Numeric.parse(response.data.values.product.inventory)
                    - Numeric.parse(data.total_qty) < 0
                ));
            })
            .catch((error) => {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        showAlternativeItems(product, index) {
            let that = this;

            if(this.processing) {
                return false;
            }

            this.processing = true;
            this.dialog.alternativeItems.index = index;

            ProductService.alternatives({
                product_id: product,
                branch_id: this.info.created_for,
                location_id: this.info.for_location
            })
            .then((response) => {
                that.dialog.alternativeItems.content = response.data.values.products;
                that.processing = false;
            })
            .catch((error) => {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        replaceItemWithAlternative(alternative) {
            let that = this;

            if (this.processing) {
                return false;
            }

            this.processing = true;
            this.datatable.product.checked = [alternative];

            let params = this.getSelectProductsApiParams();

            this.getSelectProductsApi(params)
                .then(function (response) {
                    let product = that.formatProductResponseToTransactionDetails(response.data.values.products)[0];

                    that.$set(
                        that.datatable.details.value.data,
                        that.dialog.alternativeItems.index,
                        product
                    );

                    that.dialog.alternativeItems.content = [];
                    that.datatable.product.checked = [];
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        checkTransactionForInventoryWarning() {
            if (this.disabled) {
                return false;
            }

            let that = this;
            let products = Arr.pluck(this.datatable.details.value.data, 'product_id');

            ProductService.summaries({
                product_ids: products,
                branch_id: this.info.created_for,
                location_id: this.info.for_location
            })
            .then((response) => {
                for (let i in that.datatable.details.value.data) {
                    let data = that.datatable.details.value.data[i];
                    let inventory = 0;

                    for (let ix in response.data.values.products) {
                        let product = response.data.values.products[ix];

                        if(product.id == data.product_id) {
                            inventory = product.inventory;
                            break;
                        }
                    }

                    that.$set(data, 'inventory_warning', Numeric.parse(inventory) - (
                        Numeric.parse(data.qty) * Numeric.parse(data.unit_qty)
                    ) < 0);
                }
            })
            .catch((error) => {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        onSuccessfulLoad(response) {
            this.checkTransactionForInventoryWarning();
        },

        onSuccessfulImportInvoice(response) {
            this.checkTransactionForInventoryWarning();
        },

        onSuccessfulBarcodeScan(response) {
            let index = this.datatable.details.value.data.length - 1;

            this.checkInventoryForWarning(this.datatable.details.value.data[index]);
        },

        onSuccessfulStockNoScan(response) {
            let index = this.datatable.details.value.data.length - 1;

            this.checkInventoryForWarning(this.datatable.details.value.data[index]);
        },

        onSuccessfulImportTransaction(response) {
            this.checkTransactionForInventoryWarning();
        }
    },
}
