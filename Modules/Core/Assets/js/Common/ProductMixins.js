import ProductService from '../Services/ProductService';

export default {
    data: {
        filter: {
            product: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch_inventory'),
                        columnName: 'branch_id',
                        type: 'select',
                        selectOptions: references.filters.product.branches,
                        selected: true,
                        default: { value: Laravel.auth.branch },
                        freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                    },
                    { name: Lang.t('label.stock_no'), columnName: 'stock_no', type: 'string', optionBool: false },
                    { name: Lang.t('label.unit_barcode'), columnName: 'barcode', type: 'string', optionBool: false },
                    { name: Lang.t('label.name'), columnName: 'name', type: 'string', optionBool: false },
                    { name: Lang.t('label.chinese_name'), columnName: 'chinese_name', type: 'string', optionBool: false },
                    {
                        name: Lang.t('label.status'),
                        columnName: 'status',
                        type: 'select',
                        selectOptions: [
                            { id: PRODUCT_STATUS.INACTIVE, label: Lang.t('label.inactive') },
                            { id: PRODUCT_STATUS.ACTIVE, label: Lang.t('label.active') },
                            { id: PRODUCT_STATUS.DISCONTINUED, label: Lang.t('label.discontinued') },
                        ],
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.suppliers,
                            headers: [Lang.t('label.code'), Lang.t('label.supplier_name')],
                            columns: ['code', 'full_name'],
                            returncolumn: 'full_name'
                        }
                    },
                    {
                        name: Lang.t('label.category'),
                        columnName: 'category',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.categories,
                            headers: [Lang.t('label.code'), Lang.t('label.category_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    {
                        name: Lang.t('label.brand'),
                        columnName: 'brand',
                        type: 'string',
                        optionBool: true,
                        tableOptions: {
                            data: references.filters.product.brands,
                            headers: [Lang.t('label.code'), Lang.t('label.brand_name')],
                            columns: ['code', 'name'],
                            returncolumn: 'name'
                        }
                    },
                    { 
                        name: Lang.t('label.date_created'),
                        columnName: 'created_at',
                        type: 'datetime',
                    },
                    { 
                        name: Lang.t('label.last_modified'),
                        columnName: 'updated_at',
                        type: 'datetime',
                    },
                    {
                        name: Lang.t('label.inventory_status'),
                        columnName: 'inventory',
                        type: 'select',
                        selectOptions: [
                            { id: INVENTORY_STATUS.NEGATIVE, label: Lang.t('label.negative_inventory') },
                            { id: INVENTORY_STATUS.NO_INVENTORY, label: Lang.t('label.no_inventory') },
                            { id: INVENTORY_STATUS.WITH_INVENTORY, label: Lang.t('label.with_inventory') }
                        ],
                    },
                    {
                        name: Lang.t('label.group_type'),
                        columnName: 'group_type',
                        type: 'select',
                        selectOptions: [
                            {id: GROUP_TYPE.NONE, label: Lang.t('label.none')},
                            {id: GROUP_TYPE.MANUAL_GROUP, label: Lang.t('label.manual_group')},
                            {id: GROUP_TYPE.MANUAL_UNGROUP, label: Lang.t('label.manual_ungroup')},
                            {id: GROUP_TYPE.AUTOMATIC_GROUP, label: Lang.t('label.automatic_group')},
                            {id: GROUP_TYPE.AUTOMATIC_UNGROUP, label: Lang.t('label.automatic_ungroup')},
                        ],
                    },
                ],
                express: {
                    stock_no: '',
                    barcode: '',
                    name: ''
                },
                forced: [],
                data: []
            }
        },

        columnSettings: {
            product: {
                visible: false,
                columns: [
                    { alias: 'barcode', label: Lang.t('label.unit_barcode'), default: true },
                    { alias: 'stock_no', label: Lang.t('label.stock_no'), default: true },
                    { alias: 'chinese_name', label: Lang.t('label.chinese_name'), default: true },
                    { alias: 'description', label: Lang.t('label.description'), default: true },
                    { alias: 'memo', label: Lang.t('label.memo'), default: false },
                    { alias: 'brand', label: Lang.t('label.brand'), default: false },
                    { alias: 'category', label: Lang.t('label.category'), default: true },
                    { alias: 'supplier', label: Lang.t('label.supplier'), default: true },
                    { alias: 'group_type_label', label: Lang.t('label.group_type'), default: false },
                    { alias: 'purchase_price', label: Lang.t('label.cost'), default: true },
                    { alias: 'wholesale_price', label: Lang.t('label.wholesale'), default: true },
                    { alias: 'selling_price', label: Lang.t('label.retail'), default: true },
                    { alias: 'price_a', label: Lang.t('label.price_a'), default: false },
                    { alias: 'price_b', label: Lang.t('label.price_b'), default: false },
                    { alias: 'price_c', label: Lang.t('label.price_c'), default: false },
                    { alias: 'price_d', label: Lang.t('label.price_d'), default: false },
                    { alias: 'price_e', label: Lang.t('label.price_e'), default: false },
                    { alias: 'inventory', label: Lang.t('label.inventory'), default: true }
                ],
                selections: {},
                module: {
                    list: {
                        id: MODULE.PRODUCT,
                        key: 'info'
                    }, 
                    selector: {
                        id: MODULE.PRODUCT,
                        key: 'selector'
                    }
                }
            }
        },

        datatable: {
            product: {
                id: 'product-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withActionCell: false,
                    withSelect: {
                        value: 'id'
                    }
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
                checked: []
            }
        },

        productPicker: {
            visible: false
        },

        message: {
            product: new Notification
        }
    },

    methods: {
        productSearch(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.filter.product.visible = false;
            this.message.product.reset();

            let filters = this.combineProductSearchFilters();

            ProductService.paginate({
                'perPage': that.datatable.product.settings.perPage,
                'page': page,
                'filters': filters,
            })
            .then(function (response) {
                that.datatable.product.value.data = response.data.values.products;
                that.datatable.product.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.product.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        combineProductSearchFilters() {
            let expressFilters = [];
            let filters = this.filter.product.data.concat(this.filter.product.forced);

            for(let key in this.filter.product.express) {
                if(this.filter.product.express[key] != '') {
                    expressFilters.push({
                        column: key,
                        operator: 'contains',
                        join: 'or',
                        value: this.filter.product.express[key]
                    });
                }
            }

            return filters.concat(expressFilters);
        },

        add(cont = true) {
            if(this.processing) {
                return false;
            }

            this.message.product.reset();

            if (this.datatable.product.checked.length === 0) {
                this.message.product.error.set([Lang.t('validation.details_min_one')]);
                return false;
            }

            this.processing = true;

            if(!cont) {
                this.productPicker.visible = false;
            }

            this.selectProducts();
        },

        selectProducts() {
            return false;
        }
    }
}
