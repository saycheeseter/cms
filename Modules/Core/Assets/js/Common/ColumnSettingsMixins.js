export default {
    
    methods: {
        
        getExportColumnSettings() {
            return {}
        },

        removeFromColumnSettings(columnSettings) {
            let settings = this.getExportColumnSettings();

            columnSettings = columnSettings.filter(function( obj ) {
                return (settings[obj.alias] !== undefined) ? settings[obj.alias] : true;
            });

            return columnSettings;
        },

        appendExportColumnSettingsParams(param) {
            let settings = this.getExportColumnSettings();

            for (let key in settings) {
                if (param.hasOwnProperty(key)) {
                    continue;
                }

                param[key] = settings[key];
            }

            return param;
        }
    }
}