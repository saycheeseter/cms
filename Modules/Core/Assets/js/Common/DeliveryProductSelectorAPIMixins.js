import ProductService from "../Services/ProductService";

export default {

    methods: {
        getBarcodeScanApi(params){
            switch (Laravel.settings.delivery_module_price_basis) {
                case 1:
                    return ProductService.findByBarcodeWithSummaryAndPurchasePrice(params);
                    break;

                case 2:
                    return ProductService.findByBarcodeWithSummaryAndWholesalePrice(params);
                    break;

                case 3:
                    return ProductService.findByBarcodeWithSummaryAndSellingPrice(params);
                    break;
            }
        },

        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: PRODUCT_STATUS.INACTIVE
                }
            };
        },

        getStockNoApi(params){
            switch (Laravel.settings.delivery_module_price_basis) {
                case 1:
                    return ProductService.findByStockNoWithSummaryAndPurchasePrice(params);
                    break;

                case 2:
                    return ProductService.findByStockNoWithSummaryAndWholesalePrice(params);
                    break;

                case 3:
                    return ProductService.findByStockNoWithSummaryAndSellingPrice(params);
                    break;
            }
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: PRODUCT_STATUS.INACTIVE
                }
            };
        },

        getSelectProductsApi(params) {
            switch (Laravel.settings.delivery_module_price_basis) {
                case 1:
                    return ProductService.selectWithSummaryAndPurchasePrice(params);
                    break;

                case 2:
                    return ProductService.selectWithSummaryAndWholesalePrice(params);
                    break;

                case 3:
                    return ProductService.selectWithSummaryAndSellingPrice(params);
                    break;
            }
        },

        getSelectProductsApiParams() {
            return {
                product_ids: this.datatable.product.checked,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
            };
        }
    }
}