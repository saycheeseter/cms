import StockRequestService from '../Services/StockRequestService';
import Invoice from '../Invoice/list.js';

export default {
    mixins: [Invoice],

    data: {
        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'request_to', label: Lang.t('label.request_to'), default: true },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'transaction_status', label: Lang.t('label.transaction_status'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.STOCK_REQUEST,
                key: 'info'
            }
        },
        service: new StockRequestService,
        cache: {
            module: MODULE.STOCK_REQUEST
        }
    },
};
