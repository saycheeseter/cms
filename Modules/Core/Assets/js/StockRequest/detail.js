import Invoice from '../Invoice/detail.js';
import StockRequestService from '../Services/StockRequestService';
import DeliveryProductSelectorAPIMixins from "../Common/DeliveryProductSelectorAPIMixins";

export default {
    mixins: [Invoice, DeliveryProductSelectorAPIMixins],

    data: {
        filter: {
            product: {
                forced: [
                    {
                        column: 'status',
                        join: 'and',
                        operator: '=',
                        value: PRODUCT_STATUS.ACTIVE
                    },
                ]
            }
        },

        requestedWarning : new Data({
            id: 'requested-warning',
            visible: false,
            messages: [],
            proceed: false,
            action: ''
        }),

        attachment: {
            module: MODULE.STOCK_REQUEST
        },

        service: new StockRequestService,

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_REQUEST,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.STOCK_REQUEST
        },
    },

    created() {
        this.createInfo({
            request_to: '',
        });
    },

    methods: {
        getBarcodeScanApiParams(source) {
            return {
                barcode: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            };
        },

        getStockNoApiParams(source) {
            return {
                stock_no: source,
                branch_id: this.info.created_for,
                location_id: this.info.for_location,
                excluded: {
                    group_type: [
                        GROUP_TYPE.AUTOMATIC_GROUP,
                        GROUP_TYPE.AUTOMATIC_UNGROUP
                    ],
                    status: [
                        PRODUCT_STATUS.INACTIVE,
                        PRODUCT_STATUS.DISCONTINUED
                    ]
                }
            };
        },

        proceed() {
            this.requestedWarning.visible = false;
            this.requestedWarning.proceed = true;

            switch(this.requestedWarning.action) {
                case 'save':
                    this.save();
                    break;

                case 'for-approval':
                    this.approval(APPROVAL_STATUS.FOR_APPROVAL);
                    break;
            }

            this.requestedWarning.reset();
        },

        save() {
            if (this.info.id === 0) {
                this.store(this.data);
            } else {
                this.update(this.data);
            }
        },

        process(reload = false) {
            this.reload = reload;
            if (
                (this.info.id != 0 && !permissions.update)
                || (!this.validate())
            ) {
                return false;
            }

            this.requestedWarning.action = 'save';
            this.checkAlreadyRequestedProducts();
        },

        approval(status) {
            if(this.processing || !this.validate()) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            
            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL: 
                    if (!this.requestedWarning.proceed) {
                        this.requestedWarning.action = 'for-approval';
                        this.checkAlreadyRequestedProducts();
                        return false;
                    }
                    
                    this.updateBeforeForApproval();
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        checkAlreadyRequestedProducts() {
            if (! this.isRequestedWarningActive()) {
                this.processing = false;
                this.proceed();
                return false;
            }

            let that = this;

            this.processing = true;
            this.cacheData();

            this.service.warning(this.data)
                .then(function (response) {
                    that.clearCache();
                    that.processing = false;
                    that.proceed();
                })
                .catch(function (error) {
                    that.requestedWarning.visible = true;    
                    that.requestedWarning.messages = Obj.toArray(error.response.data.errors);
                    that.processing = false;
                });
        },

        isRequestedWarningActive() {
            return Laravel.settings.request_warning_already_requested_not_delivered_toggle
                || Laravel.settings.request_warning_already_requested_delivered_toggle;
        },
    }
}