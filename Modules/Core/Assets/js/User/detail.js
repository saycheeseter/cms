import CoreMixins from '../Common/CoreMixins';
import UserService from '../Services/UserService';
import RoleService from '../Services/RoleService';
import attachment from '../Attachment/attachment';
import Timepicker2 from '../Components/Timepicker2/Timepicker2.vue';
import { isMoment } from 'moment';
import { range } from 'lodash';

let app = new Vue({
    el: '#app',

    components: {
        Timepicker2
    },

    mixins: [CoreMixins, attachment],

    data: {
        tabs: {
            content: new Toggle([
                { name: 'info', label: Lang.t('label.user_info') },
                { name: 'permissions', label: Lang.t('label.user_permissions') }
            ], 'info'),
            permissions: new Toggle(sections),
        },
        user: new Data({
            info : {
                id: 0,
                code: '',
                username: '',
                full_name: '',
                password: '',
                status: 1,
                telephone: '',
                mobile: '',
                position: '',
                email: '',
                address: '',
                memo: '',
                role_id: null,
                license_key: null
            },
            permissions: [],
            schedule: {
                mon: true,
                tue: true,
                wed: true,
                thu: true,
                fri: true,
                sat: true,
                sun: true,
                start: '00:00',
                end: '23:59'
            }
        }),
        selected: {
            branch: 0,
            code: []
        },
        message: new Notification,
        branches: branches,
        roles: roles,

        attachment: {
            type: 'image',
            module: MODULE.USER,
            single: true
        },

        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0
            }),
        },

        allBranches: {
            confirm: false,
            value: false
        }
    },

    mounted: function() {
        this.roles = [{
            id: null,
            label: Lang.t('label.none')
        }].concat(roles);

        this.setFirstSectionActive();

        this.load();
    },

    methods: {
        load() {
            let that = this;

            if(id == 'create'){
                return;
            }

            this.processing = true;
            this.message.reset();

            UserService.show(id)
                .then(function (response) {
                    that.user.info = response.data.values.user;
                    that.user.permissions = response.data.values.permissions;
                    that.evaluateAllBranchesCheckbox();
                    that.user.schedule = response.data.values.schedule;
                    that.setAttachmentProperties(id, response.data.values.attachment);
                    that.permissions();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        store() {
            let that = this;
            
            that.processing = true;
            that.message.reset();

            UserService.store(that.user.data())
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.attachFiles(response.data.values.user.id);
                    that.redirect(response.data.values.user.id);
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            let that = this;

            that.processing = true;
            that.message.reset();
            
            UserService.update(that.user.data(), id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.redirect(id);
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validate() {
            if (this.user.info.id != 0 && !userPermissions.update) {
                return false;
            } 
            
            let info = Object.assign({}, this.user.data().info);
            let permissions = Object.assign({}, {
                permissions: this.user.data().permissions
            });

            let data = Object.assign(info, permissions);

            let validation = Validator.make(data, {
                username: 'required',
                password: this.user.info.id === 0 
                    || (data.hasOwnProperty('password') && data.password !== '')
                        ? 'required|min:6' 
                        : '',
                full_name: 'required',
                email: this.user.info.email !== '' ? 'email' : '',
                permissions: 'min:1'
            }, {
                'username.required': Lang.t('validation.username_required'),
                'password.required': Lang.t('validation.password_required'),
                'password.min': Lang.t('validation.password_min_length'),
                'full_name.required': Lang.t('validation.full_name_required'),
                'email.email': Lang.t('validation.email_invalid'),
                'permissions.min': Lang.t('validation.permissions_min_length')
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            if(!this.isValidLoginRange()) {
                this.message.error.set([Lang.t('validation.invalid_time_range')]);
                return false;
            }

            if(this.user.info.id == 0) {
                this.store();
            } else {
                this.update();
            }
        },

        redirect(iden) {
            setTimeout(function() {
                UserService.edit(iden);
            }, 2000);
        },

        permissions() {
            let permissions = this.user.permissions;
            let exit = false;

            for (let i = 0; i < permissions.length; i++) {
                if (permissions[i].branch == this.selected.branch) {
                    this.selected.code = permissions[i].code;
                    exit = true;
                    break;
                }
            }

            if (exit) {
                return false;
            }

            this.selected.code = [];
        },

        toggleSectionPermissions(section) {
            let permissions = this.sectionPermissions(section);

            this.selected.code = (this.tabs.permissions[section].all)
                ? Arr.merge(this.selected.code, permissions)
                : Arr.remove(this.selected.code, permissions);
        },

        toggleModulePermissions(section, module) {
            let permissions = this.modulePermissions(section, module);

            this.selected.code = (this.tabs.permissions[section].modules[module].all)
                ? Arr.merge(this.selected.code, permissions)
                : Arr.remove(this.selected.code, permissions);
        },

        sectionPermissions(section) {
            let permissions = [];
            let modules = this.tabs.permissions[section].modules;

            for (let i = 0; i < modules.length; i++) {
                for (let x = 0; x < modules[i].permissions.length; x++) {
                    permissions.push(modules[i].permissions[x].code);
                }
            }

            return permissions;
        },

        modulePermissions(section, module) {
            let permissions = [];
            let modulePermissions = this.tabs.permissions[section].modules[module].permissions;

            for (let i = 0; i < modulePermissions.length; i++) {
                permissions.push(modulePermissions[i].code);
            }

            return permissions;
        },

        getRolePermissions() {
            this.modal.confirm.visible = false;
            if(this.user.info.role_id == null) {
                return false;
            }

            let that = this;

            that.processing = true;
            that.message.reset();

            RoleService.permissions(this.user.info.role_id)
                .then(function (response) {
                    that.user.permissions = response.data.values.permissions;
                    that.evaluateAllBranchesCheckbox();
                    that.permissions();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        confirmChangeRole() {
            if(this.user.info.role_id !== null) {
                this.modal.confirm.visible = true;
            }
        },

        cancel() {
            this.user.info.role_id = this.modal.confirm.id;
            this.modal.confirm.visible = false;
        },

        setFirstSectionActive() {
            let sections = Object.keys(this.tabs.permissions.get());
            this.tabs.permissions[sections[0]].active = true;
        },

        isValidLoginRange() {
            let start = Moment(this.user.schedule.start, 'HH:mm:ss');
            let end = Moment(this.user.schedule.end, 'HH:mm:ss');

            return !(start.diff(end) > 0);
        },

        range(start, end) {
            return range(start, end);
        },

        getLowerRangePermission(number) {
            let value;

            if (number <= 4) {
                value = 0;
            } else if ((number + 1) % 5 === 0) {
                value = number - 4;
            } else {
                value = (number + 1) - ((number + 1) % 5);
            }

            return value;
        },

        clearPermissions() {
            this.selected.code = [];
            this.user.permissions = [];
            this.allBranches.confirm = false;
        },

        closeAllBranchConfirm() {
            this.allBranches.confirm = false;
            this.allBranches.value = !this.allBranches.value;
        },

        evaluateAllBranchesCheckbox() {
            let equal = true;

            if(this.user.permissions.length !== this.branches.length) {
                equal = false;
            } else {
                for(let index in this.user.permissions) {
                    let row = this.user.permissions[index];
    
                    for(let i = Number(index) + 1; i < this.user.permissions.length; i++) {
                        let row2 = this.user.permissions[i];
    
                        if(Arr.difference(row.code, row2.code).length !== 0) {
                            equal = false;
                            break;
                        }
                    }

                    if (!equal) {
                        break;
                    }
                }
            }

            if(equal) {
                this.allBranches.value = true;
            }
        },

        onClickAllBranch() {
            if(this.user.permissions.length !== 0) {
                this.allBranches.confirm = true;
            } else {
                this.selected.code = [];
                this.user.permissions = [];
            }
        }
    },
    watch: {
        'selected.code': function(codes) {
            let permissions = this.user.permissions;
            let sections = this.tabs.permissions.get();
            let exit = false;

            // For auto checking section checkbox
            for(let section in sections) {
                let _permissions = this.sectionPermissions(section);
                
                this.tabs.permissions[section].all = (Arr.has(codes, _permissions));

                for(let i = 0; i < this.tabs.permissions[section].modules.length; i++) {
                    let _modulePermissions = this.modulePermissions(section, i);

                    this.tabs.permissions[section].modules[i].all = (Arr.has(codes, _modulePermissions))
                }
            }

            if(this.allBranches.value) {
                let existing = false;

                for(let index in this.branches) {
                    let branchId = this.branches[index].id;
                    for(let i=0; i < permissions.length; i++) {
                        if (permissions[i].branch === branchId) {
                            if (codes.length === 0) {
                                permissions.splice(i, 1);
                            } else {
                                permissions[i].code = codes;
                            }
                            
                            existing = true;
                            break;
                        }
                    }
                    
                    if(!existing && codes.length > 0) {
                        this.user.permissions.push({
                            branch: branchId,
                            code: codes
                        });
                    }

                    existing = false;
                }
            } else {
                // Push or removes a code in the user permission array
                for(let i=0; i < permissions.length; i++) {
                    if (permissions[i].branch === this.selected.branch) {
                        if (codes.length === 0) {
                            permissions.splice(i, 1);
                        } else {
                            permissions[i].code = codes;
                        }
                        
                        exit = true;
                        break;
                    }
                }

                if (exit) {
                    return false;
                }

                if(this.selected.branch !== null && codes.length > 0) {
                    // Push new array in user permission
                    this.user.permissions.push({
                        branch: this.selected.branch,
                        code: codes
                    });
                }
            }
        },
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        },
        'user.info.role_id': function(newValue, oldValue) {
            this.modal.confirm.id = oldValue;
        }
    }
});
