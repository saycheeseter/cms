import CoreMixins from '../Common/CoreMixins';
import UserService from '../Services/UserService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        message: new Notification,
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0,
                type: ''
            }),
            changeAdminPassword: new Data({
                visible: false,
                id: 0,
                password: ''
            }),
            filter: {
                visible: false,
                options: [
                    { name: Lang.t('label.code'), columnName: 'code', type: 'string' },
                    { name: Lang.t('label.full_name'), columnName: 'full_name', type: 'string' },
                    { name: Lang.t('label.username'), columnName: 'username', type: 'string' },
                    { name: Lang.t('label.position'), columnName: 'position', type: 'string' },
                ],
                data: []
            }
        },
        datatable: {
            value: {
                data: [],
                pagination: {}
            },
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
        },
        columnSettings: {
            user: {
                visible: false,
                columns: [
                    { alias: 'name', label: Lang.t('label.name'), default: true },
                    { alias: 'username', label: Lang.t('label.username'), default: false },
                    { alias: 'position', label: Lang.t('label.position'), default: false },
                    { alias: 'status', label: Lang.t('label.status'), default: false },
                    { alias: 'licenseKey', label: Lang.t('label.license_key'), default: false },
                    { alias: 'mobile', label: Lang.t('label.mobile'), default: false },
                    { alias: 'email', label: Lang.t('label.email'), default: false },
                    { alias: 'address', label: Lang.t('label.address'), default: false },
                    { alias: 'memo', label: Lang.t('label.memo'), default: false },
                    { alias: 'scheduleStart', label: Lang.t('label.schedule_start'), default: false },
                    { alias: 'scheduleEnd', label: Lang.t('label.schedule_end'), default: false },
                ],
                selections: {},
                module: MODULE.USER
            }
        }
    },

    mounted: function() {
        this.paginate();
    },

    methods: {

        paginate(page) {
            if (this.processing) {
                return false;
            }

            this.modal.filter.visible = false;

            let that = this;

            this.processing = true;
            this.message.reset();

            UserService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.users;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        destroy() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            UserService.destroy(this.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                });
        },

        create() {
            UserService.create();
        },

        view(id, type) {
            if(!permissions.view_detail && !permissions.update) {
                return false;
            }
            
            if(type === USER_TYPE.ADMIN) {
                this.modal.changeAdminPassword.id = id;
                this.modal.changeAdminPassword.visible = true;
            } else {
                UserService.edit(id);
            }
        },

        confirm(data, index) {
            if(data.type === USER_TYPE.ADMIN) {
                return false;
            }

            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        changeAdminPassword() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            UserService.changeAdminPassword(
                this.modal.changeAdminPassword.id,
                this.modal.changeAdminPassword.password
            )
            .then(function (response) {
                that.processing = false;
                that.closeChangeAdminPasswordForm();
                that.message.success = response.data.message;
            })
            .catch(function (error) {
                that.processing = false;
                that.closeChangeAdminPasswordForm();
                that.message.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        closeChangeAdminPasswordForm() {
            this.modal.changeAdminPassword.visible = false;
            this.modal.changeAdminPassword.reset();
        }
    },
    
    watch: {
        'modal.confirm.visible': function(visible) {
            if (!visible) {
                this.modal.confirm.reset();
            }
        }
    }
});
