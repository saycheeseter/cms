import InventoryFlow from '../InventoryFlow/list.js';

export default  {
    mixins: [InventoryFlow],

    data: {
        key: 'invoices',

        filter: {
            options: [
                {
                    name: Lang.t('label.branch'),
                    columnName: 'created_from',
                    type: 'select',
                    selectOptions: branches,
                    selected: true,
                    default: { value: Laravel.auth.branch },
                    freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                },
                {
                    name: Lang.t('label.for_location'),
                    columnName: 'for_location',
                    type: 'select',
                    selectOptions: locations
                },
                {
                    name: Lang.t('label.request_by'),
                    columnName: 'requested_by',
                    type: 'select',
                    selectOptions: users
                },
                { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                {
                    name: Lang.t('label.transaction_date_time'),
                    columnName: 'transaction_date',
                    type: 'datetime',
                    optionBool: false,
                    selected: true,
                    default: [
                        { comparison: '>=', value: Moment().startOf('day').format() },
                        { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                    ]
                },
                {
                    name: Lang.t('label.approval_status'),
                    columnName: 'approval_status',
                    type: 'select',
                    selectOptions: [
                        { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                        { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                        { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                        { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                    ]
                },
                {
                    name: Lang.t('label.transaction_status'),
                    columnName: 'transaction_status',
                    type: 'select',
                    selectOptions: [
                        { id: String(TRANSACTION_STATUS.COMPLETE), label: Lang.t('label.complete') },
                        { id: String(TRANSACTION_STATUS.INCOMPLETE), label: Lang.t('label.incomplete') },
                        { id: String(TRANSACTION_STATUS.EXCESS), label: Lang.t('label.excess') },
                        { id: String(TRANSACTION_STATUS.NO_DELIVERY), label: Lang.t('label.no_delivery') },
                    ]
                },
                {
                    name: Lang.t('label.deleted'),
                    columnName: 'deleted_at',
                    type: 'select',
                    selected: true,
                    selectOptions: [
                        { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                        { id: String(VOIDED.NO), label: Lang.t('label.no') },
                    ],
                    default: { value: String(VOIDED.NO) }
                },
            ]
        },
    },
}