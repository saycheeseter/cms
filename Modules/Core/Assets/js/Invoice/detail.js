import InventoryFlow from '../InventoryFlow/detail.js';
import BranchService from '../Services/BranchService';

export default {
    mixins: [InventoryFlow],

    data: {
        importing: { referenceNumber: '' },

        columnSettings: {
            details: {
                columns: [
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true},
                    { alias: 'stockNo', label: Lang.t('label.stock_no'), default: true},
                    { alias: 'chineseName', label: Lang.t('label.chinese_name') },
                    { alias: 'discount1', label: Lang.t('label.discount_1') },
                    { alias: 'discount2', label: Lang.t('label.discount_2') },
                    { alias: 'discount3', label: Lang.t('label.discount_3') },
                    { alias: 'discount4', label: Lang.t('label.discount_4') },
                    { alias: 'remarks', label: Lang.t('label.remarks') },
                ],
            }
        },

        format: {
            detail: {
                qty: 'numeric',
                unit_qty: 'numeric',
                oprice: 'numeric',
                price: 'numeric',
                cost: 'numeric',
                discount1: 'numeric',
                discount2: 'numeric',
                discount3: 'numeric',
                discount4: 'numeric',
            },
        },

        validation: {
            rules: {
                detail: {
                    qty: 'numeric',
                    price: 'numeric',
                    discount1: 'numeric',
                    discount2: 'numeric',
                    discount3: 'numeric',
                    discount4: 'numeric',
                },
            },
            messages: {
                detail: {
                    'qty.numeric': Lang.t('validation.qty_numeric'),
                    'price.numeric': Lang.t('validation.price_numeric'),
                    'discount1.numeric': Lang.t('validation.discount_numeric', { num: '1' }),
                    'discount2.numeric': Lang.t('validation.discount_numeric', { num: '2' }),
                    'discount3.numeric': Lang.t('validation.discount_numeric', { num: '3' }),
                    'discount4.numeric': Lang.t('validation.discount_numeric', { num: '4' })
                },
            }
        },

        buffer: {
            data: {
                created_for: ''
            }
        },

        key: 'invoice',
    },

    methods: {
        load() {
            if(this.processing || id === 'create') {
                this.loadCache();
                return false;
            }

            this.processing = true;

            let that = this;
            
            this.service.show(id, !this.forViewingOnly)
                .then(function (response){
                    that.processing = false;
                    that.info.set(response.data.values[that.key]);
                    that.datatable.details.value.data = that.formatAttributesToTransactionDetails(response.data.values.details.data);
                    that.setAttachmentProperties(that.info.id, response.data.values.attachments);
                    that.getLocations(that.info.for_location);

                    if (that.info.is_deleted) {
                        that.options.setup = SETUP.VIEW;
                    }

                    if (!that.forViewingOnly) {
                        that.visibility();
                    } else if (that.forViewingOnly) {
                        that.hideControls();
                    }

                    if(that.hasLocalCache(that.info.id)) {
                        that.cache.modal = true;
                    }

                    that.onSuccessfulLoad(response);
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getLocations(def = null) {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            BranchService.details(this.info.created_for)
                .then(function (response) {
                    that.reference.locations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.for_location = location;
                    that.action.reset();
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.locations = [];
                    that.processing = false;
                });
        },
        
        zeroDiscounts(index) {
            let row = this.datatable.details.value.data[index];
            
            row['discount1'] = Numeric.format(0);
            row['discount2'] = Numeric.format(0);
            row['discount3'] = Numeric.format(0);
            row['discount4'] = Numeric.format(0);
        },

        setDiscount(index) {
            let row = this.datatable.details.value.data[index];

            if (Numeric.parse(row['discount1']) > 0 
                || Numeric.parse(row['discount2']) > 0
                || Numeric.parse(row['discount3']) > 0
                || Numeric.parse(row['discount4']) > 0
            ) {
                row['price'] = Numeric.format(
                    Numeric.parse(row['oprice']) 
                    * (1 - Numeric.parse(row['discount1'])/100) 
                    * (1 - Numeric.parse(row['discount2'])/100) 
                    * (1 - Numeric.parse(row['discount3'])/100) 
                    * (1 - Numeric.parse(row['discount4'])/100)
                );
            }

            this.setAmount(index);
        },

        formatAttributesToTransactionDetails(details, override = {}, hook = null) {
            let collection = [];

            details = Arr.wrap(details);

            for(let i = 0; i < details.length; i++) {
                let detail = details[i];

                let formatted = {
                    id: detail.hasOwnProperty('id') ? detail.id : 0,
                    manage_type: detail.manage_type,
                    product_id: detail.hasOwnProperty('product_id') ? detail.product_id : 0,
                    barcode_list: detail.barcode_list,
                    barcode: detail.barcode,
                    name: detail.name,
                    stock_no: detail.stock_no,
                    chinese_name: detail.chinese_name,
                    units: detail.units,
                    unit_id: detail.unit_id,
                    unit_qty: detail.unit_qty,
                    qty: detail.hasOwnProperty('qty') ? detail.qty : Numeric.format(0),
                    price: detail.price,
                    cost: detail.cost,
                    old_unit: detail.hasOwnProperty('old_unit') ? detail.old_unit : true,
                    oprice: detail.hasOwnProperty('oprice') ? detail.oprice : detail.price,
                    discount1: detail.hasOwnProperty('discount1') ? detail.discount1 : Numeric.format(0),
                    discount2: detail.hasOwnProperty('discount2') ? detail.discount2 : Numeric.format(0),
                    discount3: detail.hasOwnProperty('discount3') ? detail.discount3 : Numeric.format(0),
                    discount4: detail.hasOwnProperty('discount4') ? detail.discount4 : Numeric.format(0),
                    remarks: detail.hasOwnProperty('remarks') ? detail.remarks : '',
                    group_type: detail.group_type,
                    components: detail.components,
                    manageable: detail.manageable,
                    product_type: detail.product_type,
                    transactionable_type: detail.hasOwnProperty('transactionable_type') ? detail.transactionable_type : null,
                    transactionable_id: detail.hasOwnProperty('transactionable_id') ? detail.transactionable_id : null,
                    references: detail.hasOwnProperty('references') ? detail.references : null,
                    remaining_qty: detail.hasOwnProperty('remaining_qty') ? detail.remaining_qty : 0
                };

                formatted['total_qty'] = Numeric.format(
                    Numeric.parse(formatted['unit_qty']) *
                    Numeric.parse(formatted['qty'])
                );

                formatted['amount'] = Numeric.format(
                    Numeric.parse(formatted['total_qty']) *
                    Numeric.parse(formatted['price'])
                );

                if (hook !== null) {
                    formatted = hook(formatted, detail);
                }

                collection.push(Object.assign({}, formatted, override));
            }

            return collection;
        },

        importTransaction() {
            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.import(this.importing.referenceNumber, this.info.id)
                .then(function (response) {
                    that.message.transaction.success = response.data.message;
                    that.datatable.details.value.data = that.datatable.details.value.data.concat(that.formatImportResponseToTransactionDetails(response.data.values.details.data));
                    that.processing = false;
                    that.importing.referenceNumber = '';
                    that.onSuccessfulImportTransaction(response);
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },
    },

    watch: {
        'action.value': function(action) {
            switch(action) {
                case 'created-for':
                    this.getLocations();
                    break;
            }
        },
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.created_for = oldValue;
        },
    },
}