import InventoryChain from '../InventoryChain/detail.js';
import StockReturnInboundService from '../Services/StockReturnInboundService';
import ProductService from '../Services/ProductService';
import StockReturnOutboundService from '../Services/StockReturnOutboundService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain],

    data: {
        service: {
            invoice: new StockReturnOutboundService,
            inventoryChain: new StockReturnInboundService
        },

        controls: {
            create: false
        },

        attachment: {
            module: MODULE.STOCK_RETURN_INBOUND
        },

        columnSettings: {
            details: {
                module: {
                    id: MODULE.STOCK_RETURN_INBOUND,
                    key: 'detail'
                }
            }
        },

        cache: {
            module: MODULE.STOCK_RETURN_INBOUND
        },

        selector: {
            invoice: {
                columnSettings: {
                    module: {
                        id: MODULE.STOCK_RETURN_OUTBOUND,
                        key: 'info'
                    }
                }
            }
        },
    },

    created() {
        this.createInfo({
            delivery_from: null,
            delivery_from_location: null,
            delivery_from_label: '-',
            delivery_from_location_label: '-',
            transaction_type: TRANSACTION_TYPE.FROM_INVOICE
        });
    },

    methods: {
        getInvoice(string) {
            let that = this;

            if (this.processing) {
                return false;
            }

            this.processing = true;

            return this.service.invoice.remaining({
                sheet_number: string,
                branch: this.info.created_for
            })
            .then(function (response) {
                that.reference.invoice = response.data.values.stock_return_outbounds;
                that.processing = false;
                that.action.reset();
            })
            .catch(function (response) {
                that.processing = false;
                that.action.reset();
            });
        },

        replaceAndRetrieveInvoice() {
            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            this.service.invoice.retrieve(this.info.reference_id)
                .then(function (response) {
                    that.message.transaction.success = response.data.message;
                    that.info.set(response.data.values.stock_return_outbound);
                    that.datatable.details.value.data = [];
                    that.datatable.details.value.data = that.formatImportInvoiceResponseToTransactionDetails(response.data.values.details.data);
                    that.processing = false;
                    that.importWarning.sameInvoice.visible = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        showSerials(data, row) {
            if (this.processing) {
                return false;
            }

            this.processing = true;
            this.datatable.serial.selected.value.data = data.serials;

            if (!this.disabled) {
                if (!this.itemManagement.serials.hasOwnProperty(data.reference_id)) {
                    this.getNewSerials(data.reference_id);
                } else {
                    this.datatable.serial.list.value.data = this.itemManagement.serials[data.reference_id];
                }
            } else {
                this.datatable.serial.selected.settings.withSelect.value = false;
            }

            this.processing = false;
            this.itemManagementSelection.serial.visible = true;
            this.itemManagementSelection.serial.row = row;
        },

        getNewSerials(id) {
            let that = this;

            this.service.invoice.serials(id)
                .then(function (response) {
                    let serials = that.filterSerials(response.data.values.serials);

                    that.itemManagement.serials[id] = serials;
                    that.datatable.serial.list.value.data = that.itemManagement.serials[id];
                    that.processing = false;
                })
                .catch(function (error) {
                    //that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        showBatches(data, row) {
            this.processing = true;
            this.datatable.batch.selected.value.data = data.batches;

            if (!this.disabled) {
                if (!this.itemManagement.batches.hasOwnProperty(data.reference_id)) {
                    this.getNewBatches(data.reference_id);
                } else {
                    this.datatable.batch.list.value.data = this.itemManagement.batches[data.reference_id];
                }
            } else {
                this.datatable.batch.selected.settings.withSelect.value = false;
            }

            this.processing = false;
            this.itemManagementSelection.batch.visible = true;
            this.itemManagementSelection.batch.row = row;
        },

        getNewBatches(id) {
            let that = this;

            this.service.invoice.batches(id)
                .then(function (response) {
                    let batches = that.filterBatches(response.data.values.batches);

                    that.setBatchGetQty(batches);

                    that.itemManagement.batches[id] = batches;
                    that.datatable.batch.list.value.data = that.itemManagement.batches[id];
                    that.processing = false;
                })
                .catch(function (error) {
                    //that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        removeItemManagementDataWarning() {
            return;
        },

        getInvoiceSelectorParams() {
            return this.selector.invoice.filter.data.concat([
                {
                    value: this.info.created_for,
                    operator: '=',
                    join: 'and',
                    column: 'deliver_to'
                },
                {
                    value: APPROVAL_STATUS.APPROVED,
                    operator: '=',
                    join: 'and',
                    column: 'approval_status'
                },
                {
                    value: VOIDED.NO,
                    operator: '=',
                    join: 'and',
                    column: 'deleted_at'
                },
                {
                    value: TRANSACTION_STATUS.COMPLETE,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },

                {
                    value: TRANSACTION_STATUS.EXCESS,
                    operator: '<>',
                    join: 'and',
                    column: 'transaction_status'
                },
            ]);
        },
    },
});
