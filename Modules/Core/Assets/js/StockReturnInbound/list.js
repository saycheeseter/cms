import StockReturnInboundService from '../Services/StockReturnInboundService';
import InventoryChain from '../InventoryChain/list.js';
import BranchService from '../Services/BranchService';

let app = new Vue({
    el: '#app',

    mixins: [InventoryChain],

    data: {
        columnSettings: {
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'reference', label: Lang.t('label.reference'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'delivery_from', label: Lang.t('label.delivery_from'), default: true  },
                { alias: 'amount', label: Lang.t('label.amount'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.STOCK_RETURN_INBOUND,
                key: 'info'
            }
        },
        service: new StockReturnInboundService,
        cache: {
            module: MODULE.STOCK_RETURN_INBOUND
        }
    },

    created: function() {
        this.filter.options.push({
            name: Lang.t('label.delivery_from'),
            columnName: 'delivery_from',
            type:'doubleSelect',
            secondaryColumn: 'delivery_from_location',
            selectOptions: branches,
            withAll: true,
            withCallback: true,
            callback: function(branch) {
                return BranchService.details(branch);
            }
        });
    }
});
