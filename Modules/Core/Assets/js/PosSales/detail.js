import CoreMixins from "../Common/CoreMixins";
import PosSalesService from "../Services/PosSalesService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            details: {
                id: 'detail-list',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withActionCell: false,
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                }
            },
        },
        info: {},
        columnSettings: {
            details: {
                visible: false,
                columns: [
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true },
                    { alias: 'stockNo', label: Lang.t('label.stock_no'), default: true },
                    { alias: 'chineseName', label: Lang.t('label.chinese_name') },
                    { alias: 'discount1', label: Lang.t('label.discount_1') },
                    { alias: 'discount2', label: Lang.t('label.discount_2') },
                    { alias: 'discount3', label: Lang.t('label.discount_3') },
                    { alias: 'discount4', label: Lang.t('label.discount_4') },
                    { alias: 'remarks', label: Lang.t('label.remarks') },
                    { alias: 'vat', label: Lang.t('label.vat'), default: true },
                    { alias: 'senior', label: Lang.t('label.senior'), default: true },
                ],
                selections: {}
            }
        },
        message: new Notification(),
    },

    mounted() {
        this.load();
    },

    methods: {
        load() {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            PosSalesService.show(id)
                .then(function (response){
                    that.info = (response.data.values.info);
                    that.datatable.details.value.data = response.data.values.details.data;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },
    },

    computed: {
        totalQty: function () {
            let total = 0;
            let data = this.datatable.details.value.data;

            for(let index in data) {
                total += Numeric.parse(data[index]['qty']);
            }

            return Numeric.format(total);
        },
    }
});