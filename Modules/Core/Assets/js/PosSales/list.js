import CoreMixins from "../Common/CoreMixins";
import PosSalesService from "../Services/PosSalesService";
import QueryStringLocation from "../Classes/QueryStringLocation";

let app = new Vue({
    el: '#app',
    mixins: [CoreMixins],
    data: {
        modal: {
            filter: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type: 'select',
                        selectOptions: branches,
                        limit: 1
                    },
                    {
                        name: Lang.t('label.transaction_date_time'),
                        columnName: 'transaction_date',
                        type: 'datetime',
                        optionBool: false,
                        limit: 2
                    },
                    // {
                    //     name: Lang.t('label.transaction_number'),
                    //     columnName: 'transaction_number',
                    //     type: 'string'
                    // },
                    // {
                    //     name: Lang.t('label.or_si_no'),
                    //     columnName: 'or_number',
                    //     type: 'string'
                    // },
                    // {
                    //     name: Lang.t('label.terminal_no'),
                    //     columnName: 'terminal_number',
                    //     type: 'number'
                    // },
                    // {
                    //     name: Lang.t('label.cashier'),
                    //     columnName: 'cashier',
                    //     type: 'select',
                    //     selectOptions: users,
                    //     limit: 1,
                    // },
                    // {
                    //     name: Lang.t('label.salesman'),
                    //     columnName: 'salesman_id',
                    //     type: 'select',
                    //     selectOptions: users,
                    //     limit: 1,
                    // },
                    // {
                    //     name: Lang.t('label.customer_type'),
                    //     columnName: 'customer_type',
                    //     type: 'select',
                    //     selectOptions: [
                    //         { id: CUSTOMER.REGULAR, label: Lang.t('label.regular') },
                    //         { id: CUSTOMER.WALK_IN, label: Lang.t('label.walk_in_customer') },
                    //     ],
                    //     limit: Obj.getLength(CUSTOMER)
                    // },
                    // {
                    //     name: Lang.t('label.customer'),
                    //     columnName: 'customer',
                    //     type: 'string',
                    //     optionBool: true,
                    //     tableOptions: {
                    //         data: customers,
                    //         headers: [Lang.t('label.code'), Lang.t('label.customer')],
                    //         columns: ['code', 'name'],
                    //         returncolumn: 'name'
                    //     }
                    // },
                    // {
                    //     name: Lang.t('label.member'),
                    //     columnName: 'member',
                    //     type: 'string',
                    //     optionBool: true,
                    //     tableOptions: {
                    //         data: members,
                    //         headers: [Lang.t('label.barcode'), Lang.t('label.member')],
                    //         columns: ['barcode', 'full_name'],
                    //         returncolumn: 'full_name'
                    //     }
                    // },
                    // {
                    //     name: Lang.t('label.senior_name'),
                    //     columnName: 'senior_name',
                    //     type: 'string'
                    // },
                    // {
                    //     name: Lang.t('label.senior_number'),
                    //     columnName: 'senior_number',
                    //     type: 'string'
                    // },
                    // {
                    //     name: Lang.t('label.nonvat'),
                    //     columnName: 'is_non_vat',
                    //     type: 'select',
                    //     selectOptions: [
                    //         { id: VOIDED.YES, label: Lang.t('label.yes') },
                    //         { id: VOIDED.NO, label: Lang.t('label.no') },
                    //     ],
                    //     limit: 1
                    // },
                    // {
                    //     name: Lang.t('label.is_wholesale'),
                    //     columnName: 'is_wholesale',
                    //     type: 'select',
                    //     selectOptions: [
                    //         { id: VOIDED.YES, label: Lang.t('label.yes') },
                    //         { id: VOIDED.NO, label: Lang.t('label.no') },
                    //     ],
                    //     limit: 1
                    // },
                ],
                data: []
            }
        },
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                perPage: 10,
                withActionCell: false,
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        columnSettings: {
            id: 'column-settings',
            visible: false,
            columns: [
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'terminal_number', label: Lang.t('label.terminal_number'), default: true },
                { alias: 'cashier', label: Lang.t('label.cashier'), default: true },
                { alias: 'salesman', label: Lang.t('label.salesman'), default: true },
                { alias: 'customer_type', label: Lang.t('label.customer_type'), default: true },
                { alias: 'customer', label: Lang.t('label.customer'), default: true },
                { alias: 'member', label: Lang.t('label.member'), default: true },
                { alias: 'senior_name', label: Lang.t('label.senior'), default: true },
                { alias: 'senior_number', label: Lang.t('label.senior_number'), default: true },
                { alias: 'total_amount', label: Lang.t('label.total_amount'), default: true },
                { alias: 'is_non_vat', label: Lang.t('label.nonvat'), default: false },
                { alias: 'wholesale', label: Lang.t('label.wholesale'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                { alias: 'voided', label: Lang.t('label.voided'), default: true },
            ],
            selections: []
        },
        message: new Notification,
    },

    mounted:function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.modal.filter.visible = false;
            this.message.reset();
            this.processing = true;

            PosSalesService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters : that.modal.filter.data
            }).then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.data;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            }).catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        show(id) {
            PosSalesService.view(id);
        }
    },

    created() {
        (new QueryStringLocation({
            created_for: 'created_for',
            transaction_date: 'transaction_date'
        })).setDefaultOnFilters(this.modal.filter.options);
    },
});
