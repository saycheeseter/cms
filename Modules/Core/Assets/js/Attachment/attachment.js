import AttachmentService from '../Services/AttachmentService';
import ImageCompressor from '@xkeshi/image-compressor';

export default {
    
    data: {
        attachment: {
            form: new FormData(),
            files: {
                attached: [],
                temporary: [],
                forUpload: []
            },
            type: 'file',
            element: 'attachments[]',
            module: '',
            entity_id: 0,
            modal: {
                visible: false,
                link: ''
            },
            validation : {
                max: 2000000,
                invalid_type: ['application/x-msdownload'],
                valid_image_type: ['image/jpeg', 'image/png', 'image/bmp']
            },
            single: false, 
            compressor: {
                instance: new ImageCompressor(),
                option : {
                    width: 1024,
                    height: 768,
                }
            },
        },
    },

    methods: {
        filesChange(attachments) {
            this.attachment.fails = [];

            for (var i = 0; i < attachments.length; i++) {
                let attachment = attachments[i];
                
                if(this.validFile(attachment)) {
                    if(this.attachment.validation.valid_image_type.indexOf(attachment.type) != -1) {
                        this.compressImageAndAttach(attachment);
                    } else {
                        this.addAttachment(attachment)
                    }
                }
            }
            
            document.getElementById('upload-photo').value = null;
        },

        compressImageAndAttach(attachment) {
            this.processing = true;
            let that = this; 

            this.attachment.compressor.instance.compress(attachment, this.attachment.compressor.option)
                .then((result) => {
                    that.processing = false;
                    that.addAttachment(new File([result], result.name));
                }).catch((error) => {
                    that.processing = false;
                    that.message.error.set([error.message + ':' + attachment.name]);
                })
        },

        addAttachment(attachment) {
            if(this.attachment.entity_id != 0) {
                this.attachment.files.forUpload.push(attachment);
                this.attachFiles(this.attachment.entity_id)
            } else {
                this.temporaryFiles(attachment);
            }
        },

        temporaryFiles(attachment) {
            this.processing = true;

            let temporary = new FormData();
            temporary.append(this.attachment.element, attachment);

            let that = this;

            AttachmentService.temporary(temporary, that.attachment.type)
                .then(function (response) {
                    that.processing = false;
                    that.attachment.files.forUpload.push(attachment);
                    that.attachment.files.temporary.push(response.data.values.attachment);
                    if(that.attachment.single) {
                        that.attachment.files.attached.push(response.data.values.attachment);
                    }
                })
                .catch(function (error) {
                    that.processing = false;
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        attachFiles(id = 0) {
            if(this.attachment.files.forUpload.length == 0) {
                return false;
            }

            this.attachment.form.delete(this.attachment.element);
            
            for (let i = 0; i < this.attachment.files.forUpload.length; i++) {
                this.attachment.form.append(this.attachment.element, this.attachment.files.forUpload[i]);
            }

            let that = this;

            if(this.attachment.single) {
                this.updateFiles(id);
                return;
            }

            AttachmentService.attach(this.attachment.form, id, this.attachment.module, this.attachment.type)
                .then(function (response) {
                    that.attachment.files.attached = response.data.values.attachments;
                    that.message.success = response.data.values.message;
                    that.attachment.files.forUpload = [];
                    that.attachment.files.temporary = [];
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        updateFiles(id) {
            let that = this;

            AttachmentService.update(this.attachment.form, id, this.attachment.module, this.attachment.type)
                .then(function (response) {
                    that.attachment.files.attached = response.data.values.attachments;
                    that.message.success = response.data.values.message;
                    that.attachment.files.forUpload = [];
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                });
        },

        detachFiles(data, index) {
            let that = this;
            
            AttachmentService.detach(id, this.attachment.module, data['key'], data['status'])
                .then(function (response) {
                    that.message.success = response.data.values.message;
                    if(data.status == 'temporary') {
                        that.attachment.files.forUpload.splice(index, 1);
                        that.attachment.files.temporary.splice(index, 1);
                    } else {
                        that.attachment.files.attached.splice(index, 1);
                    }
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(response.data.errors));
                });
        },

        preview(path) {
            this.attachment.modal.visible = true;
            this.attachment.modal.link = path;
        },

        setAttachmentProperties(id, files) {
            this.attachment.files.attached = files;
            this.attachment.entity_id = id;
        },

        validFile(attachment) {
            let errors = [];
            let filename = attachment.name;

            if (attachment.size > this.attachment.validation.max) {
                errors.push(Lang.t('validation.file_over_allowed_size') + ':' + filename);
            }

            if(this.attachment.validation.invalid_type.indexOf(attachment.type) != -1) {
                errors.push(Lang.t('validation.invalid_file_type') + ':' + filename);
            }

            if(errors.length > 0) {
                this.message.error.set(errors)
                return false;
            }

            return true;
        },

        orientation (image) {
            let width = image[0];
            let height = image[1];

            if(height > width) {
                return 'img-border portrait';
            }

            return 'img-border';
        }
    },
}