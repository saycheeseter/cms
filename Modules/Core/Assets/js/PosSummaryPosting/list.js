import CoreMixins from '../Common/CoreMixins';
import PosSummaryPostingService from "../Services/PosSummaryPostingService";

let app = new Vue({
    el: '#app',

    components: {
        DateTimePicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: false,
                withEdit: false,
                withActionCell: false,
                withPaginateCallBack: true
            },
            value: {
                data: [],
                pagination: {}
            }
        },
        posting: {
            form: {
                visible: false,
                id: null,
                index: null,
                data: new Data({
                    date: Moment().format()
                }),
            }
        },
        message: new Notification,
    },

    mounted: function() {
        this.paginate(1);
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            PosSummaryPostingService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.terminals.data;
                that.datatable.value.pagination = response.data.values.terminals.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        showPostingForm(id, index) {
            this.posting.form.visible = true;
            this.posting.form.id = id;
            this.posting.form.index = index;
            this.posting.form.data.date = Moment().format();
        },

        closePostingForm() {
            this.posting.form.visible = false;
            this.posting.form.id = null;
            this.posting.form.index = null;
        },

        generateSummary() {
            if (this.processing || !this.isValidDateRange()) {
                return false;
            }

            this.processing = true;

            let that = this;

            PosSummaryPostingService.generateSummary(this.posting.form.id, {
                date: this.posting.form.data.date
            })
            .then(function (response) {
                that.processing = false;
                that.message.success = response.data.message;
                that.datatable.value.data[that.posting.form.index].last_post_date = response.data.values.terminal.last_post_date;
                that.closePostingForm();
            })
            .catch(function (error) {
                that.closePostingForm();
                that.processing = false;
                that.message.error.set(Obj.toArray(error.response.data.errors));
            });
        },

        isValidDateRange() {
            let dateFrom = Moment(this.datatable.value.data[this.posting.form.index].last_post_date);
            let dateTo = Moment(this.posting.form.data.date);

            if (dateFrom >= dateTo) {
                this.message.error.set([
                    Lang.t('validation.invalid_date_range')
                ]);

                this.closePostingForm();

                return false;
            }

            return true;
        }
    }
});
