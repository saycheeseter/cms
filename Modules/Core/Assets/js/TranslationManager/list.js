import CoreMixins from '../Common/CoreMixins';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10
            },
            value: {
                data: [],
                pagination: {}
            }
        }
    },

    methods: {
        paginate() {},
        store() {},
        update() {},
        confirm() {},
        destroy() {
            return false;
        },
        validate() {}
    }
});