import CoreMixins from '../Common/CoreMixins';
import BankTransactionService from '../Services/BankTransactionService';

let app = new Vue({
    el: '#app',

    components: {
        Datepicker
    },

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'datatable-id',
            settings: {
                withPaging : true,
                withPaginateCallBack: true,
                perPage: 10,
                withDelete: permissions.delete,
                withEdit: permissions.update
            },
            value: {
                data: [],
                pagination: {}
            },
            selected: [],
        },
        modal: {
            confirm: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            filter: {
                visible: false,
                options: [
                    { 
                        name: Lang.t('label.from_bank'), 
                        columnName: 'from_bank', 
                        type: 'select', 
                        selectOptions: bankAccounts
                    },
                    { 
                        name: Lang.t('label.to_bank'), 
                        columnName: 'to_bank', 
                        type: 'select', 
                        selectOptions: bankAccounts
                    },
                    { 
                        name: Lang.t('label.branch'), 
                        columnName: 'branch_id', 
                        type: 'select', 
                        selectOptions: branches
                    },
                    {
                        name: Lang.t('label.type'), 
                        columnName: 'type', 
                        type: 'select', 
                        selectOptions: [
                            { id: String(BANK_TRANSACTION_TYPE.DEPOSIT), label: Lang.t('label.deposit') },
                            { id: String(BANK_TRANSACTION_TYPE.WITHDRAWAL), label: Lang.t('label.withdrawal') },
                            { id: String(BANK_TRANSACTION_TYPE.TRANSFER), label: Lang.t('label.transfer') }
                        ]
                    },
                    {
                        name: Lang.t('label.date'),
                        columnName: 'transaction_date',
                        type:'dateRange',
                        default: [
                            {
                                value: {
                                    start: Moment().startOf('day').format(),
                                    end: Moment().add(1, 'day').startOf('day').format()
                                }
                            }
                        ],
                    },
                ],
                data: {},
            }
        },
        columnSettings: {
            id: 'selector-column-settings',
            visible: false,
            columns: [
                { alias: 'type', label: Lang.t('label.type'), default: true },
                { alias: 'date', label: Lang.t('label.date'), default: true },
                { alias: 'from_bank', label: Lang.t('label.from_bank'), default: true },
                { alias: 'to_bank', label: Lang.t('label.to_bank'), default: true },
                { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
            ],
            selections: []
        },

        message: new Notification,

        fields: new Data({
            type: String(BANK_TRANSACTION_TYPE.DEPOSIT),
            transaction_date: new Date(),
            amount: '0.00',
            from_bank: null,
            to_bank: '',
            remarks: ''
        }),

        references: {
            branches: branches,
            bankAccounts: bankAccounts,
        },

        transactionType: [
            { id: String(BANK_TRANSACTION_TYPE.DEPOSIT), label: Lang.t('label.deposit') },
            { id: String(BANK_TRANSACTION_TYPE.WITHDRAWAL), label: Lang.t('label.withdrawal') },
            { id: String(BANK_TRANSACTION_TYPE.TRANSFER), label: Lang.t('label.transfer') }
        ],

        format: {
            amount: 'numeric',
            transaction_date: 'date'
        },

        permissions: permissions
    },

    mounted: function(){
        this.paginate();
    },

    methods: {
        paginate(page = 1) {
            let that = this;

            if (that.processing) {
                return false;
            }

            that.modal.filter.visible = false;
            that.processing = true;
            that.message.reset();

            BankTransactionService.paginate({
                perPage: that.datatable.settings.perPage,
                page: page,
                filters: that.modal.filter.data,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.bank_transactions;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        store() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            BankTransactionService.store(Format.make(that.fields.data(), that.format))
                .then(function (response) {
                    that.datatable.value.data.push(response.data.values.bank_transaction);
                    that.message.success = response.data.message;
                    that.processing = false;
                    that.computePagination(that.datatable.value.pagination, 'add');
                    //that.toggleFocus();
                    that.fields.reset();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        update(data, index) {
            let that = this;

            if(that.processing){
                return false
            }

            that.message.reset();
            that.processing = true;

            BankTransactionService.update(Format.make(data, that.format), data.id)
                .then(function (response) {
                    that.changeState(index, 'destroy');
                    // that.toggleFocus();
                    that.message.success = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        destroy(data) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            BankTransactionService.destroy(that.modal.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.modal.confirm.index, 1);
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.modal.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.modal.confirm.visible = false;
                })
        },

        confirm(data, index) {
            this.modal.confirm.visible = true
            this.modal.confirm.id = data.id;
            this.modal.confirm.index = index;
        },

        process(data, index) {
            data = index == null ? this.fields.data() : data;

            if (!this.isValid(data)) {
                return false;
            }

            if (index == null) {
                this.store();
            } else {
                this.update(data, index);
            }
        },

        isValid(data) {
            if(data.from_bank == data.to_bank) {
                this.message.error.set([Lang.t('validation.bank_mismatch')]);
                return false;
            }

            if(Numeric.parse(data.amount) <= 0) {
                this.message.error.set([Lang.t('validation.amount_cannot_be_zero')]);
                return false;
            }

            var validation = Validator.make(Format.make(data, this.format), {
                amount: 'required',
            }, {
                'amount.required': Lang.t('validation.amount_required')
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        },

        isNotDeposit(type) {
            return type != BANK_TRANSACTION_TYPE.DEPOSIT;
        },

        isNotWithdrawal(type) {
            return type != BANK_TRANSACTION_TYPE.WITHDRAWAL;
        }
    },

    watch: {
        'modal.confirm.visible': function(visible){
            if (!visible) {
                this.modal.confirm.reset();
            }
        },
        'fields.type': function(value) {
            let account = this.references.bankAccounts.length > 0
                ? this.references.bankAccounts[0].id
                : null;

            switch(value) {
                case String(BANK_TRANSACTION_TYPE.DEPOSIT): 
                    this.fields.from_bank = null;
                    this.fields.to_bank = account;
                    break;

                case String(BANK_TRANSACTION_TYPE.WITHDRAWAL):
                    this.fields.from_bank = account;
                    this.fields.to_bank = null;
                    break;

                case String(BANK_TRANSACTION_TYPE.TRANSFER):
                    this.fields.from_bank = account;
                    this.fields.to_bank = account;
                    break;
            }
        }
    }
});