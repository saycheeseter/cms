import CoreMixins from '../Common/CoreMixins';
import BranchService from '../Services/BranchService';
import ProductService from '../Services/ProductService';
import PurchaseInbound from '../Services/PurchaseInboundService';
import StockDeliveryInbound from '../Services/StockDeliveryInboundService';
import StockReturnInbound from '../Services/StockReturnInboundService';
import SalesReturnInbound from '../Services/SalesReturnInboundService';
import PurchaseReturnOutbound from '../Services/PurchaseReturnOutboundService';
import StockDeliveryOutbound from '../Services/StockDeliveryOutboundService';
import StockReturnOutbound from '../Services/StockReturnOutboundService';
import SalesOutbound from '../Services/SalesOutboundService';
import DamageOutbound from '../Services/DamageOutboundService';
import AdjustIncrease from '../Services/AdjustIncreaseService';
import AdjustDecrease from '../Services/AdjustDecreaseService';
import ConversionIncrease from '../Services/ConversionIncreaseService';
import ConversionDecrease from '../Services/ConversionDecreaseService';
import AutoConversionIncrease from '../Services/AutoConversionIncreaseService';
import AutoConversionDecrease from '../Services/AutoConversionDecreaseService';
import SerialNumberService from "../Services/SerialNumberService";
import PosSalesService from "../Services/PosSalesService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        Datepicker
    },

    data: {
        tabs: new Toggle([
            { name: 'transactionSummary', label: Lang.t('label.transaction_summary') },
            { name: 'inventoryStockCard', label: Lang.t('label.inventory_stock_card') },
            { name: 'serialNumbers', label: Lang.t('label.serial_numbers') }
        ], 'transactionSummary'),

        transaction: {
            summary: new Data({
                beginning: Numeric.format(),
                purchase_inbound: Numeric.format(),
                stock_delivery_inbound: Numeric.format(),
                stock_return_inbound: Numeric.format(),
                sales_return_inbound: Numeric.format(),
                adjustment_increase: Numeric.format(),
                product_conversion_increase: Numeric.format(),
                auto_product_conversion_increase: Numeric.format(),
                purchase_return_outbound: Numeric.format(),
                stock_delivery_outbound: Numeric.format(),
                stock_return_outbound: Numeric.format(),
                sales_outbound: Numeric.format(),
                damage_outbound: Numeric.format(),
                adjustment_decrease: Numeric.format(),
                product_conversion_decrease: Numeric.format(),
                auto_product_conversion_decrease: Numeric.format(),
                pos_sales: Numeric.format()
            }),
            datatable: {
                id: 'transaction-summary',
                settings: {
                    withPaging : true,
                    withPaginateCallBack: true,
                    perPage: 10,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {}
                },
            },
            label: ''
        },

        stockCard: {
            datatable: {
                id: 'inventory-log',
                settings: {
                    withPaging : true,
                    withPaginateCallBack: true,
                    perPage: 10,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {}
                },
            },
            columnSettings: {
                visible: false,
                columns: [
                    { alias: 'sequenceNo', label: Lang.t('label.sequence_no'), default: true },
                    { alias: 'cost', label: Lang.t('label.cost'), default: true },
                    { alias: 'preInventory', label: Lang.t('label.pre_inventory'), default: true },
                    { alias: 'preAverageCost', label: Lang.t('label.pre_average_cost'), default: true },
                    { alias: 'preAmount', label: Lang.t('label.pre_amount'), default: true },
                    { alias: 'postInventory', label: Lang.t('label.post_inventory'), default: true },
                    { alias: 'postAverageCost', label: Lang.t('label.post_average_cost'), default: true },
                    { alias: 'postAmount', label: Lang.t('label.post_amount'), default: true },
                ],
                selections: {},
                module: MODULE.STOCK_CARD
            }
        },

        serialNumbers: {
            datatable: {
                id: 'serial-numbers',
                settings: {
                    withPaging : true,
                    withPaginateCallBack: true,
                    perPage: 10,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {}
                },
            }
        },

        filter: {
            transactionSummary: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type:'doubleSelect',
                        secondaryColumn: 'for_location',
                        selectOptions: branches,
                        withAll: true,
                        limit: 1,
                        withCallback: true,
                        selected: true,
                        callback: function(branch) {
                            return BranchService.details(branch);
                        },
                        default: { value: { head: Laravel.auth.branch } }
                    },
                    {
                        name: Lang.t('label.transaction_date'),
                        columnName: 'transaction_date',
                        type:'dateRange',
                        default: [
                            {
                                value: {
                                    start: Moment().startOf('day').format(),
                                    end: Moment().add(1, 'day').startOf('day').format()
                                }
                            }
                        ],
                        limit: 1,
                        selected: true,
                    },
                ],
                data: [],
            },
            stockCard: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type:'doubleSelect',
                        secondaryColumn: 'for_location',
                        selectOptions: branches,
                        withAll: true,
                        limit: 1,
                        withCallback: true,
                        selected: true,
                        callback: function(branch) {
                            return BranchService.details(branch);
                        },
                        default: { value: { head: Laravel.auth.branch } }
                    },
                    {
                        name: Lang.t('label.transaction_date'),
                        columnName: 'transaction_date',
                        type:'dateRange',
                        default: [
                            {
                                value: {
                                    start: Moment().startOf('day').format(),
                                    end: Moment().add(1, 'day').startOf('day').format()
                                }
                            }
                        ],
                        limit: 1,
                        selected: true
                    },
                ],
                data: [],
            },
            serialNumber: {
                visible: false,
                options: [
                    {
                        name: Lang.t('label.branch'),
                        columnName: 'created_for',
                        type:'doubleSelect',
                        secondaryColumn: 'for_location',
                        selectOptions: branches,
                        withAll: true,
                        limit: 1,
                        withCallback: true,
                        selected: true,
                        callback: function(branch) {
                            return BranchService.details(branch);
                        },
                        default: { value: { head: Laravel.auth.branch } }
                    },
                    {
                        name: Lang.t('label.supplier'),
                        columnName: 'supplier_id',
                        type: 'select',
                        selectOptions: suppliers,
                    },
                ],
                data: [],
            }
        },

        message: new Notification,

        service: {
            purchase_inbound: new PurchaseInbound,
            stock_delivery_inbound: new StockDeliveryInbound,
            stock_return_inbound: new StockReturnInbound,
            sales_return_inbound: new SalesReturnInbound,
            purchase_return_outbound: new PurchaseReturnOutbound,
            stock_delivery_outbound: new StockDeliveryOutbound,
            stock_return_outbound: new StockReturnOutbound,
            sales_outbound: new SalesOutbound,
            damage_outbound: new DamageOutbound,
            adjust_increase: new AdjustIncrease,
            adjust_decrease: new AdjustDecrease,
            product_conversion_increase: new ConversionIncrease,
            product_conversion_decrease: new ConversionDecrease,
            auto_product_conversion_increase: new AutoConversionIncrease,
            auto_product_conversion_decrease: new AutoConversionDecrease,
            pos_sales: PosSalesService,
            active: null
        }
    },

    mounted() {
        this.search();
    },

    methods: {
        search() {
            let that = this;

            this.filter.visible = false;
            this.processing = true;

            axios.all([
                ProductService.transactionSummary({
                    filters: this.filter.transactionSummary.data,
                    product_id: id
                }),
                ProductService.stockCard({
                    perPage: this.stockCard.datatable.settings.perPage,
                    page: 1,
                    filters: this.filter.stockCard.data,
                    product_id: id
                }),
                ProductService.availableSerials({
                    perPage: this.serialNumbers.datatable.settings.perPage,
                    page: 1,
                    product_id: id,
                    filters: this.filter.serialNumber.data
                })
            ])
            .then(axios.spread((transactions, stockCard, serialNumbers) => {
                that.clearTransactions();
                that.service.active = null;
                that.transaction.summary.reset();
                that.transaction.summary.set(transactions.data.values.summary);

                that.stockCard.datatable.value.data = stockCard.data.values.stock_card.data;
                that.stockCard.datatable.value.pagination = stockCard.data.values.stock_card.meta.pagination;

                that.serialNumbers.datatable.value.data = serialNumbers.data.values.serials.data;
                that.serialNumbers.datatable.value.pagination = serialNumbers.data.values.serials.meta.pagination;

                that.processing = false;
            }));
        },

        getStockCard(page) {
            let that = this;

            this.clearStockCard();
            this.filter.stockCard.visible = false;
            this.processing = true;

            ProductService.stockCard({
                perPage: this.stockCard.datatable.settings.perPage,
                page: page,
                filters: this.filter.stockCard.data,
                product_id: id
            })
            .then(function (response) {
                that.stockCard.datatable.value.data = response.data.values.stock_card.data;
                that.stockCard.datatable.value.pagination = response.data.values.stock_card.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        setTransactionType(service, label) {
            this.service.active = service;
            this.transaction.label = label;
            this.getTransactions(1);
        },

        getTransactionSummary() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.clearTransactions();
            this.service.active = null;
            this.transaction.summary.reset();
            this.filter.transactionSummary.visible = false;
            this.processing = true;

            ProductService.transactionSummary({
                filters: this.filter.transactionSummary.data,
                product_id: id
            })
            .then(function (response) {
                that.transaction.summary.set(response.data.values.summary);
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        getTransactions(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.clearTransactions();
            this.processing = true;

            this.service.active.transactions({
                perPage: this.transaction.datatable.settings.perPage,
                page: page,
                filters: this.filter.transactionSummary.data,
                product_id: id
            })
            .then(function (response) {
                that.transaction.datatable.value.data = response.data.values.data;
                that.transaction.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        showTransaction(data) {
            if (Arr.in(this.service.active.constructor.name, [
                    this.service.auto_product_conversion_increase.constructor.name,
                    this.service.auto_product_conversion_decrease.constructor.name])
            ) {
                this.service.active.edit(data.transaction_id, data.module);
            } else if (this.service.active.constructor.name === this.service.pos_sales.constructor.name) {
                this.service.active.view(data.transaction_id);
            } else {
                this.service.active.edit(data.transaction_id);
            }
        },

        clearTransactions() {
            this.transaction.datatable.value.data = [];
            this.transaction.datatable.value.pagination = {};
        },

        clearStockCard() {
            this.stockCard.datatable.value.data = [];
            this.stockCard.datatable.value.pagination = {};
        },

        getAvailableSerials(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();
            this.filter.serialNumber.visible = false;

            ProductService.availableSerials({
                perPage: this.serialNumbers.datatable.settings.perPage,
                page: page,
                product_id: id,
                filters: this.filter.serialNumber.data
            })
            .then(function (response) {
                that.processing = false;
                that.message.info = response.data.message;
                that.serialNumbers.datatable.value.data = response.data.values.serials.data;
                that.serialNumbers.datatable.value.pagination = response.data.values.serials.meta.pagination;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        exportSerial() {
            SerialNumberService.export({
                filters: this.filter.serialNumber.data,
                id: id
            });
        }
    },

    computed: {
        inventory: function() {
            return Numeric.format(
                Numeric.parse(this.transaction.summary.beginning)
                + Numeric.parse(this.transaction.summary.purchase_inbound)
                + Numeric.parse(this.transaction.summary.stock_delivery_inbound)
                + Numeric.parse(this.transaction.summary.stock_return_inbound)
                + Numeric.parse(this.transaction.summary.sales_return_inbound)
                + Numeric.parse(this.transaction.summary.adjustment_increase)
                + Numeric.parse(this.transaction.summary.product_conversion_increase)
                + Numeric.parse(this.transaction.summary.auto_product_conversion_increase)
                - Numeric.parse(this.transaction.summary.purchase_return_outbound)
                - Numeric.parse(this.transaction.summary.stock_delivery_outbound)
                - Numeric.parse(this.transaction.summary.stock_return_outbound)
                - Numeric.parse(this.transaction.summary.sales_outbound)
                - Numeric.parse(this.transaction.summary.damage_outbound)
                - Numeric.parse(this.transaction.summary.adjustment_decrease)
                - Numeric.parse(this.transaction.summary.product_conversion_decrease)
                - Numeric.parse(this.transaction.summary.auto_product_conversion_decrease)
                - Numeric.parse(this.transaction.summary.pos_sales)
            );
        }
    }
});
