import CoreMixins from '../Common/CoreMixins';
import ProductService from '../Services/ProductService';
import ProductMixins from '../Common/ProductMixins';
import ImportExcel from '../ImportExcel/import-excel';
import ColumnSettingsMixins from '../Common/ColumnSettingsMixins.js';
import AttachmentService from "../Services/AttachmentService";

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins, ColumnSettingsMixins, ImportExcel],

    components: {
        Carousel
    },

    data: {
        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index : 0
        }),

        message: new Notification(),

        datatable: {
            id: 'tablelist',
            settings: {
                withPaging : true,
                perPage: 10,
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            }
        },

        modal: {
            logs: {
                visible: false,
                content: []
            },

            images: {
                visible: false,
                content: []
            }
        },
    },

    mounted: function() {
        this.columnSettings.product.columns = this.removeFromColumnSettings(this.columnSettings.product.columns);
        this.paginate(1);
    },

    methods: {
        paginate(page) {
            this.filter.product.visible = false;

            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.datatable.value.data = [];
            this.message.reset();

            let filters = this.combineProductSearchFilters();

            ProductService.paginate({
                perPage: this.datatable.settings.perPage,
                page: page,
                filters: filters,
            })
            .then(function (response) {
                that.message.info = response.data.message;
                that.datatable.value.data = response.data.values.products;
                that.datatable.value.pagination = response.data.values.meta.pagination;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        previewImages(id) {
            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            AttachmentService.getByProduct(id)
                .then(function (response) {
                    that.message.info = response.data.message;
                    that.modal.images.content = response.data.values.data;
                    that.processing = false;

                    if (that.modal.images.content.length > 0) {
                        that.modal.images.visible = true;
                    }
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        destroy() {
            if(this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            ProductService.destroy(this.confirm.id)
                .then(function (response) {
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.processing = false;
                    that.computePagination(that.datatable.value.pagination, 'delete');
                    that.confirm.visible = false;
                    
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.confirm.visible = false;
                    that.processing = false;
                })
        },

        edit(id) {
            if(!permissions.view_detail && !permissions.update) {
                return false;
            }

            ProductService.edit(id);
        },

        create() {
            ProductService.create();
        },

        report(id) {
            ProductService.report(id);
        },

        confirmation(data, index) {
            this.confirm.visible = true;
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        getExportColumnSettings() {
            return {
                cost: permissions.show_cost
            }
        },

        onSuccessfulImport() {
            this.paginate();
        },

        importExcelService() {
            return ProductService;
        },

        importExcelMessage() {
            return this.message;
        },

        excel() {
            let filters = this.combineProductSearchFilters();

            let params = {
                filters: filters,
                settings: Object.assign({}, this.columnSettings.product.selections)
            };

            ProductService.export(params);
        },
    },

    watch: {
        'confirm.visible': function(visible) {
            if (!visible) {
                this.confirm.reset();
            }
        }
    },
});
