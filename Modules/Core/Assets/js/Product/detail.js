import CoreMixins from '../Common/CoreMixins';
import ProductMixins from '../Common/ProductMixins';
import ProductService from '../Services/ProductService';
import attachment from '../Attachment/attachment';
import Datepicker from 'vuejs-datepicker';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins, attachment],

    components: {
        Datepicker
    },

    data: {
        tables: {
            units: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
                fields: new Data({
                    uom_id: null,
                    qty: 0,
                    length: 0,
                    width: 0,
                    height: 0,
                    weight: 0,
                })
            },
            barcodes: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
                fields: new Data({
                    uom_id: pc,
                    code: '',
                    memo: ''
                })
            },
            prices: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
            },
            branches: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
            },
            locations: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withActionCell: false
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
            },
            alternatives: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
            },
            components: {
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallBack: false,
                    withDelete: true
                },
                value: {
                    data: [],
                    pagination: {
                        count: 1,
                        current_page: 1 ,
                        links: [],
                        per_page: 10,
                        total: 1,
                        total_pages: 1
                    }
                },
            },
        },

        tabs: new Toggle([
            { name: 'advance', label: Lang.t('label.advance_info') },
            { name: 'unit', label: Lang.t('label.unit') },
            { name: 'barcode', label: Lang.t('label.barcode') },
            { name: 'price', label: Lang.t('label.price') },
            { name: 'branch', label: Lang.t('label.branch') },
            { name: 'location', label: Lang.t('label.location') },
            { name: 'alternativeItems', label: Lang.t('label.alternative_items') },
            { name: 'grouping', label: Lang.t('label.product_grouping') },
            { name: 'image', label: Lang.t('label.product_images') },
        ], 'advance'),

        //General Info
        info: new Data({
            id: 0,
            stock_no: '',
            name: '',
            supplier_id: '',
            memo: '',
            manageable: true,
            status: 1,
            senior: 0,
            manage_type: PRODUCT_MANAGE_TYPE.NONE,
            is_used: 0,
            chinese_name: '',
            supplier_sku: '',
            brand_id: '',
            description: '',
            category_id: '',
            created: '-',
            modified: '-',
            group_type: GROUP_TYPE.NONE,
            type: PRODUCT_TYPE.REGULAR,
            taxes: [],
            default_unit: pc
        }),

        format: {
            product: {
                manage_type: 'numeric',
            },
            unit: {
                qty: 'numeric',
                length: 'numeric',
                width: 'numeric',
                height: 'numeric',
                weight: 'numeric'
            },
            price: {
                purchase_price: 'numeric',
                wholesale_price: 'numeric',
                selling_price: 'numeric',
                price_a: 'numeric',
                price_b: 'numeric',
                price_c: 'numeric',
                price_d: 'numeric',
                price_e: 'numeric'
            },
            location: {
                min: 'numeric',
                max: 'numeric'
            },
            component: {
                qty: 'numeric'
            }
        },

        references: {
            uoms: {
                initial: uoms,
                current: []
            },
            branches: branches,
            locations: locations,
            suppliers: suppliers,
            taxes: taxes,
            brands: brands,
            categories: categories,
            groupType: [
                {id: GROUP_TYPE.NONE, label: Lang.t('label.none')},
                {id: GROUP_TYPE.MANUAL_GROUP, label: Lang.t('label.manual_group')},
                {id: GROUP_TYPE.MANUAL_UNGROUP, label: Lang.t('label.manual_ungroup')},
                {id: GROUP_TYPE.AUTOMATIC_GROUP, label: Lang.t('label.automatic_group')},
                {id: GROUP_TYPE.AUTOMATIC_UNGROUP, label: Lang.t('label.automatic_ungroup')},
            ],
            type: PRODUCT_TYPE
        },

        message: new Notification(),

        confirm: {
            deleteUnit: new Data({
                visible: false,
                index: 0
            }),
            deleteBarcode: new Data({
                visible: false,
                index: 0
            }),
            deleteAlternativeItem: new Data({
                visible: false,
                index: 0
            }),
            deleteComponent: new Data({
                visible: false,
                index: 0
            }),
            changeGroupType: new Data({
                visible: false,
                id: 0
            }),
        },

        warning : {
            priceAbnormality: {
                visible: false,
                proceed: false,
                messages: []
            }
        },

        attachment: {
            type: 'image',
            module: MODULE.PRODUCT
        },

        cost: {
            to: Moment().startOf('day').format(),
            from: Moment().add(1, 'day').startOf('day').format()
        }
    },

    mounted: function() {
        this.setCurrentUnits();

        if (this.isNew) {
            this.setDefaults();
        } else {
            this.load();
        }
    },

    methods: {
        /**
         * Load currently selected product
         */
        load() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            ProductService.load(id)
                .then(function (response){
                    that.info.set(response.data.values.product);
                    that.info.taxes = response.data.values.taxes;
                    that.tables.units.value.data = response.data.values.units;
                    that.tables.prices.value.data = response.data.values.prices;
                    that.tables.barcodes.value.data = response.data.values.barcodes;
                    that.tables.branches.value.data= response.data.values.branches;
                    that.tables.locations.value.data = response.data.values.locations;
                    that.tables.components.value.data = response.data.values.components;
                    that.setAttachmentProperties(id, response.data.values.attachments);
                    that.tables.alternatives.value.data = response.data.values.alternatives;
                    that.processing = false;
                })
                .catch(function(error){
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        /**
         * Trigger permission validation, or settings validation and redirect the process to store
         * or update
         */
        process(reload = false) {
            if((id === 'create' && !permissions.create)
                || (id !== 'create' && !permissions.update)
                || this.isPricesAbnormal()
                || !this.doesPieceHaveBarcode()
            ) {
                return false;
            }

            if (id === 'create') {
                this.store(reload);
            } else {
                this.update(reload);
            }
        },

        /**
         * Send data to server for saving
         */
        store(reload) {
            let that = this;

            this.processing = true;

            this.message.reset();
            
            ProductService.store(this.data)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.info.id = response.data.values.product.id;
                    that.attachFiles(response.data.values.product.id);
                    that.redirect(reload);
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        /**
         * Send data to server for update
         */
        update(reload) {
            let that = this;

            this.processing = true;

            this.message.reset();

            ProductService.update(this.data, id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.redirect(reload);
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        /**
         * Validates current encoded unit and push it to the array of units, 
         * 
         * @param  Object data
         */
        storeUnit(data) {
            this.message.reset();

            if (!this.isUnitValid(data)) {
                return false;
            }

            this.tables.units.value.data.push(data);
            this.tables.units.fields.reset();
        },

        /**
         * Validates current encoded unit and change the icon of the table to delete
         * 
         * @param  Object data
         * @param  int index
         */
        updateUnit(data, index, dataIndex) {
            this.message.reset();

            if (!this.isUnitValid(data, dataIndex)) {
                return false;
            }

            this.changeState(index, 'destroy', 'units');
        },

        /**
         * Delete current unit and remove all the corresponding associated barcode to it
         */
        deleteUnit() {
            let index = this.confirm.deleteUnit.index;

            this.removeRelatedBarcode(this.tables.units.value.data[index].uom_id);
            this.removeRelatedDefaultUnit(this.tables.units.value.data[index].uom_id);
            this.tables.units.value.data.splice(index, 1);
            this.confirm.deleteUnit.visible = false;
        },

        /**
         * Place all product unit validation here
         * 
         * @param  Object  data
         * @param  int|null except
         * @return Boolean
         */
        isUnitValid(data, except) {
            if (data.uom_id === null) {
                this.message.error.set([Lang.t('validation.unit_required')]);
                return false;
            }

            if(this.isUnitAlreadyExists(data.uom_id, except)) {
                this.message.error.set([Lang.t('validation.unit_already_exists')]);
                return false;
            }

            return true;
        },

        /**
         * Clear prices of the row if delete button is clicked
         * 
         * @param  Object data
         * @param  int index
         */
        clearPrice(data, index) {
            for(let property in this.tables.prices.value.data[index]) {
                if(property != 'branch_id'){
                    this.tables.units.value.data[index][property] = Numeric.format(0);
                }
            }
        },

        /**
         * Validates current encoded barcode and push it to the array of barcodes, 
         * 
         * @param  Object data
         */
        storeBarcode(data) {
            this.message.reset();

            if (!this.isBarcodeValid(data)) {
                return false;
            }

            this.tables.barcodes.value.data.push(data);
            this.tables.barcodes.fields.reset();
        },

        /**
         * Validates current encoded barcode and change the icon of the table to delete
         * 
         * @param  Object data
         * @param  int index
         */
        updateBarcode(data, index, dataIndex) {
            this.message.reset();

            if (!this.isBarcodeValid(data, dataIndex)) {
                return false;
            }

            this.changeState(index, 'destroy', 'barcodes');
        },

        /**
         * Delete current barcode
         */
        deleteBarcode() {
            this.tables.barcodes.value.data.splice(this.confirm.deleteBarcode.index, 1);
            this.confirm.deleteBarcode.visible = false;
        },

        /**
         * Delete current alternative item
         */
        deleteAlternativeItem() {
            this.tables.alternatives.value.data.splice(this.confirm.deleteAlternativeItem.index, 1);
            this.confirm.deleteAlternativeItem.visible = false;
        },

        /**
         * Delete current component
         */
        deleteComponent() {
            this.tables.components.value.data.splice(this.confirm.deleteComponent.index, 1);
            this.confirm.deleteComponent.visible = false;
        },

        /**
         * Place all product unit barcode validation here
         * 
         * @param  Object  data
         * @param  int|null except
         * @return Boolean
         */
        isBarcodeValid(data, except) {
            if(this.isUnitBarcodeAlreadyExists(data.code, except) && data.code != '') {
                this.message.error.set([Lang.t('validation.barcode_unique')]);
                return false;
            }

            return true;
        },

        setDefaults() {
            this.setPriceAndBranch();
            this.setLocation();
            this.setInitialUnit();
            this.setInitialBarcode();
            this.setFirstTax();
        },

        /**
         * Set initial rows for branch price and branch info
         */
        setPriceAndBranch() {
            for(let key in this.references.branches) {
                let branch = this.references.branches[key];
                
                let price = {
                    branch_id: branch.id,
                    branch_name: branch.label,
                    copy_from: branch.id === Laravel.settings.main_branch
                        ? null
                        : Laravel.settings.main_branch,
                    purchase_price: Numeric.format(0),
                    wholesale_price: Numeric.format(0),
                    selling_price: Numeric.format(0),
                    price_a: Numeric.format(0),
                    price_b: Numeric.format(0),
                    price_c: Numeric.format(0),
                    price_d: Numeric.format(0),
                    price_e: Numeric.format(0)
                };
                
                this.tables.prices.value.data.push(price);

                let info = {
                    branch_id: branch.id,
                    branch_name: branch.label,
                    status: PRODUCT_STATUS.ACTIVE
                };
                
                this.tables.branches.value.data.push(info);
            }
        },

        /**
         * Set intial row for locations
         */
        setLocation() {
            for(let key in this.references.locations) {
                let location = this.references.locations[key];

                let info = {
                    branch_name: location.branch,
                    branch_detail_id: location.id,
                    branch_detail_name: location.name,
                    min: Numeric.format(0),
                    max: Numeric.format(0)
                };
                
                this.tables.locations.value.data.push(info);
            }
        },

        /**
         * Add default unit for product
         */
        setInitialUnit() {
            this.tables.units.value.data.push({
                uom_id: pc,
                qty: Numeric.format(1),
                length: Numeric.format(0),
                width : Numeric.format(0),
                height: Numeric.format(0),
                weight: Numeric.format(0),
            });
        },

        /**
         * Add default barcode for product
         */
        setInitialBarcode() {
            this.tables.barcodes.value.data.push({
                uom_id: pc,
                code: '',
                memo: ''
            });
        },

        setFirstTax() {
            this.info.taxes.push(this.references.taxes[0].id);
        },

        /**
         * Checks whether the given unit id exists in the array of encoded units
         * 
         * @param  int  unit
         * @param  int|null  except 
         * @return Boolean
         */
        isUnitAlreadyExists(unit, except = null) {
            let units = Arr.pluck(this.tables.units.value.data, 'uom_id', except);

            return Arr.in(unit, units);
        },

        /**
         * Checks whether the given barcode exists in the array of encoded barcodes
         *
         * @param  int  barcode
         * @param  int|null  except 
         * @return Boolean
         */
        isUnitBarcodeAlreadyExists(barcode, except = null) {
            let barcodes = Arr.pluck(this.tables.barcodes.value.data, 'code', except);

            return Arr.in(barcode, barcodes);
        },

        /**
         * Confirms unit deletion and do not allow if the unit to be deleted is the default
         * 
         * @param  Object data
         * @param  int index
         */
        confirmDeleteUnit(data, index, dataIndex) {
            // Do not allow to delete default unit
            if (Number(data.uom_id) === Number(pc)) {
                return false;
            }

            this.confirm.deleteUnit.visible = true;
            this.confirm.deleteUnit.index = dataIndex;
        },

        /**
         * Confirms barcode deletion
         * 
         * @param  Object data
         * @param  int index
         */
        confirmDeleteBarcode(data, index, dataIndex) {
            this.confirm.deleteBarcode.visible = true;
            this.confirm.deleteBarcode.index = dataIndex;
        },

        /**
         * Confirms alternative item deletion
         *
         * @param  Object data
         * @param  int index
         */
        confirmDeleteAlternativeItem(data, index, dataIndex) {
            this.confirm.deleteAlternativeItem.visible = true;
            this.confirm.deleteAlternativeItem.index = dataIndex;
        },

        /**
         * Confirms component deletion
         *
         * @param  Object data
         * @param  int index
         */
        confirmDeleteComponent(data, index, dataIndex) {
            this.confirm.deleteComponent.visible = true;
            this.confirm.deleteComponent.index = dataIndex;
        },

        /**
         * Checks whether the give unit is the default
         * 
         * @param  int  id
         * @return Boolean
         */
        isDefaultUnit(id) {
            return Number(id) === Number(pc);
        },

        /**
         * Check whether to show other prices of each branch based on the settings
         * 
         * @param  int branch
         * @return Boolean
         */
        showPricesOfOtherBranches(branch) {
            if(Laravel.settings.product_view_prices == VIEW_PRICES.BRANCH_ONLY
                && branch != Laravel.auth.branch 
            ) {
                return false;
            }

            return true;
        },

        /**
         * Hides the warning modal and trigger the process method
         */
        proceedOnPriceWarning() {
            this.warning.priceAbnormality.visible = false;
            this.warning.priceAbnormality.proceed = true;
            this.process();
        },

        /**
         * Checks whether the purchase price of each product is greather than the selling or wholesale price
         * 
         * @return Boolean
         */
        isPricesAbnormal() {
            let warnings = [];

            if(this.warning.priceAbnormality.proceed || !Laravel.settings.product_show_price_abnormality_warning) {
                return false;
            }

            for(let i = 0; i < this.tables.prices.value.data.length; i++) {
                let price = this.tables.prices.value.data[i];
                let purchasePrice = Numeric.parse(price.purchase_price);
                let sellingPrice = Numeric.parse(price.selling_price);
                let wholesalePrice = Numeric.parse(price.wholesale_price);

                if(purchasePrice > sellingPrice || purchasePrice > wholesalePrice) {
                    warnings.push(price.branch_name);
                }
            }

            if(warnings.length > 0){
                this.warning.priceAbnormality.visible = true;
                this.warning.priceAbnormality.messages = warnings;
                return true;
            }

            return false;
        },

        getUnitName(id) {
            for(let index in uoms) {
                let unit = uoms[index];

                if (Number(unit.id) === Number(id)) {
                    return unit.label;
                }
            }

            return '';
        },

        setCurrentUnits() {
            let uoms = [];

            for(let index in this.references.uoms.initial) {
                let uom = this.references.uoms.initial[index];

                if (Number(uom.id) === Number(pc)) {
                    continue;
                }

                uoms.push(uom);
            }

            this.references.uoms.current = uoms;
        },

        removeRelatedBarcode(unit) {
            for (var i = this.tables.barcodes.value.data.length - 1; i >= 0; i--) {
                let barcode = this.tables.barcodes.value.data[i];

                if (Number(barcode.uom_id) === Number(unit)) {
                    this.tables.barcodes.value.data.splice(i, 1);
                }
            }
        },

        selectProducts() {
            let that = this;
            let ids = [];

            if(this.tabs.alternativeItems.active) {
                ids = this.getNotExistingSelectedIds(this.tables.alternatives.value.data, 'id');
            } else if(this.tabs.grouping.active) {
                ids = this.getNotExistingSelectedIds(this.tables.components.value.data, 'component_id');
            }

            if (ids.length === 0) {
                this.processing = false;
                return false;
            }

            ProductService.findManyBasic(ids)
                .then(function (response) {
                    that.datatable.product.checked = [];

                    if(that.tabs.alternativeItems.active) {
                        that.tables.alternatives.value.data = that.tables.alternatives.value.data.concat(response.data.values.products);
                    } else if(that.tabs.grouping.active) {
                        that.tables.components.value.data = that.tables.components.value.data.concat(
                            that.formatAttributesToComponents(response.data.values.products)
                        );
                    }

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        getNotExistingSelectedIds(container, key) {
            let existing = Arr.pluck(container, key);
            let selected = this.datatable.product.checked;

            existing.push(id);

            return Arr.remove(selected, existing);
        },

        formatAttributesToComponents(data) {
            let result = [];

            for(let index in data) {
                let row = data[index];

                result.push({
                    component_id: row.id,
                    stock_no: row.stock_no,
                    name: row.name,
                    qty: 0
                });
            }

            return result;
        },

        notifyGroupType(){
            if(this.tables.components.value.data.length > 0) {
                this.confirm.changeGroupType.visible = true;
            } else {
                this.filter.product.forced = this.forceGroupTypeFilters();
            }
        },

        changeGroupType() {
            this.confirm.changeGroupType.visible = false;
            this.tables.components.value.data = [];
            this.filter.product.forced = this.forceGroupTypeFilters();
        },

        forceGroupTypeFilters() {
            return [
                {
                    column: 'type',
                    join: 'and',
                    operator: '<>',
                    value: PRODUCT_TYPE.GROUP
                },
                {
                    column: 'status',
                    join: 'and',
                    operator: '=',
                    value: PRODUCT_STATUS.ACTIVE
                },
                {
                    column: 'manage_type',
                    join: 'and',
                    operator: '=',
                    value: PRODUCT_MANAGE_TYPE.NONE
                },
            ];
        },

        forceAlternativeFilters() {
            return [
                {
                    column: 'status',
                    join: 'and',
                    operator: '=',
                    value: PRODUCT_STATUS.ACTIVE
                },
                {
                    column: 'manage_type',
                    join: 'and',
                    operator: '=',
                    value: PRODUCT_MANAGE_TYPE.NONE
                },
                {
                    column: 'group_type',
                    join: 'and',
                    operator: '<>',
                    value: GROUP_TYPE.AUTOMATIC_GROUP
                },
                {
                    column: 'group_type',
                    join: 'and',
                    operator: '<>',
                    value: GROUP_TYPE.AUTOMATIC_UNGROUP
                },
            ];
        },

        revertGroupType() {
            this.info.group_type = this.confirm.changeGroupType.id;
            this.confirm.changeGroupType.visible = false;
        },

        doesPieceHaveBarcode() {
            let units = Arr.pluck(this.tables.barcodes.value.data, 'uom_id');

            if (!Arr.in(pc, units)) {
                this.message.error.set([Lang.t('validation.piece_should_have_barcode')]);
                return false;
            }

            return true;
        },

        removeRelatedDefaultUnit(uom) {
            if (uom !== this.info.default_unit) {
                return false;
            }

            this.info.default_unit = pc;
        },

        getOtherBranches(excluded) {
            let branches = [
                { id: null, label: Lang.t('label.none') }
            ];

            for(let key in this.references.branches) {
                let branch = this.references.branches[key];

                if (branch.id === excluded) {
                    continue;
                }

                branches.push({ id: branch.id, label: branch.label });
            }

            return branches;
        },

        copyPrices() {

            for (let i in this.tables.prices.value.data) {
                let price = this.tables.prices.value.data[i];

                if (price.copy_from === null) {
                    continue;
                }

                let base = this.getPriceByBranch(price.copy_from);

                price.purchase_price = base.purchase_price;
                price.wholesale_price = base.wholesale_price;
                price.selling_price = base.selling_price;
                price.price_a = base.price_a;
                price.price_b = base.price_b;
                price.price_c = base.price_c;
                price.price_d = base.price_d;
                price.price_e = base.price_e;
            }
        },

        getPriceByBranch(branch) {
            let base = {};

            for (let i in this.tables.prices.value.data) {
                let price = this.tables.prices.value.data[i];

                if (price.branch_id === branch) {
                    base = price;
                    break;
                }
            }

            return base;
        },

        redirect(reload = true) {
            if (reload) {
                ProductService.redirect(this.info.id);
            } else {
                ProductService.create(true);
            }
        },

        applyPurchasePriceToTransactions(data) {
            if (this.processing || !this.isValidDateRange(data.branch_id)) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            let branch = data.branch_id;
            let cost = Numeric.parse(data.purchase_price);

            ProductService.applyPurchasePriceToTransactions({
                product_id: this.info.id,
                branch_id: branch,
                to: Moment(this.cost.to).format('YYYY-MM-DD'),
                from: Moment(this.cost.from).format('YYYY-MM-DD'),
                cost: cost
            })
            .then(function (response){
                that.$refs['purchaseprice' + branch].away();
                that.message.success = response.data.message;
                that.processing = false;
            })
            .catch(function(error){
                that.$refs['purchaseprice' + branch].away();
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        isValidDateRange(branchId){
            if(Moment(this.cost.from) < Moment(this.cost.to)) {
                this.$refs['purchaseprice' + branchId].away();
                this.message.error.set({ error: Lang.t('error.date_range_incorrect') });
                return false;
            }

            return true;
        },

        isMainBranch(branch) {
            return branch === Laravel.settings.main_branch;
        }
    },

    computed: {
        data: function() {
            let info = this.info.data();
            let taxes = info.taxes;

            delete info.taxes;

            return {
                product: Format.make(this.info.data(), this.format.info),
                units: Format.make(this.tables.units.value.data, this.format.unit),
                prices: Format.make(this.tables.prices.value.data, this.format.price),
                barcodes: this.tables.barcodes.value.data,
                branches: this.tables.branches.value.data,
                locations: Format.make(this.tables.locations.value.data, this.format.location),
                taxes: taxes,
                alternatives: Arr.pluck(this.tables.alternatives.value.data, 'id'),
                components: Format.make(this.tables.components.value.data, this.format.component)
            };
        },

        units: function() {
            let units = [];

            for(let index in this.tables.units.value.data) {
                let unit = this.tables.units.value.data[index];

                units.push({
                    id: unit.uom_id,
                    label: this.getUnitName(unit.uom_id)
                });
            }

            return units;
        },

        isLoggedAsMainBranch: function() {
            return Laravel.settings.main_branch === Laravel.auth.branch;
        },

        isNew: function() {
            return id === 'create';
        },

        isComponent: function() {
            return this.info.type === PRODUCT_TYPE.COMPONENT;
        }
    },

    watch: {
        'confirm.deleteUnit.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteUnit.reset();
            }
        },

        'confirm.deleteBarcode.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteBarcode.reset();
            }
        },
        'confirm.deleteAlternativeItem.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteAlternativeItem.reset();
            }
        },
        'confirm.deleteComponent.visible': function(visible) {
            if(!visible) {
                this.confirm.deleteComponent.reset();
            }
        },
        'confirm.changeGroupType.visible': function(visible) {
            if(!visible) {
                this.confirm.changeGroupType.reset();
            }
        },
        'info.group_type': function(newValue, oldValue) {
            this.confirm.changeGroupType.id = oldValue;
        },
        'productPicker.visible': function(visible) {
            if(visible) {
                this.productSearch();
            }
        },
        'tabs.grouping.active': function(value) {
            if(value) {
                this.filter.product.forced = this.forceGroupTypeFilters();
            } else {
                this.filter.product.forced = [];
            }
        },
        'tabs.alternativeItems.active': function(value) {
            if(value) {
                this.filter.product.forced = this.forceAlternativeFilters();
            } else {
                this.filter.product.forced = [];
            }
        }
    },
});
