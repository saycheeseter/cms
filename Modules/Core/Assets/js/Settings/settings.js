import CoreMixins from '../Common/CoreMixins';
import TreeList from '../Components/TreeList/TreeList.vue';
import BranchSettingsService from '../Services/BranchSettingsService.js';
import GenericSettingsService from '../Services/GenericSettingsService.js';
import UDFService from '../Services/UDFService.js';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        TreeList
    },

    data: {
        tableId: 'tablelist',

        tabs: new Toggle([
            { name: 'generic', label: Lang.t('label.generic_settings') },
            { name: 'branch', label: Lang.t('label.branch_settings') },
            { name: 'udf', label: Lang.t('label.user_defined_fields') }
        ], 'generic'),

        sections: {
            branch: [
                { id: 1, label: Lang.t('label.purchase_inbound'), level: 1, parent: null, hasChild: false, active: true },
                { id: 2, label: Lang.t('label.payment'), level: 1, parent: null, hasChild: false, active: false },
                { id: 3, label: Lang.t('label.collection'), level: 1, parent: null, hasChild: false, active: false }
            ],

            generic: [
                { id: 1, label: Lang.t('label.general'), level: 1, parent: null, hasChild: false, active: true },
                { id: 2, label: Lang.t('label.product'), level: 1, parent: null, hasChild: false, active: false },
                { id: 3, label: Lang.t('label.sheet_number_prefix'), level: 1, parent: null, hasChild: false, active: false },
                { id: 4, label: Lang.t('label.purchase_and_purchase_inbound'), level: 1, parent: null, hasChild: true, active: false },
                { id: 5, label: Lang.t('label.purchase_inbound'), level: 2, parent: 4, hasChild: false, active: false },
                { id: 6, label: Lang.t('label.sales_modules'), level: 1, parent: null, hasChild: true, active: false },
                { id: 10, label: Lang.t('label.sales_outbound'), level: 2, parent: 6, hasChild: false, active: false },
                { id: 7, label: Lang.t('label.delivery_modules'), level: 1, parent: null, hasChild: true, active: false },
                { id: 8, label: Lang.t('label.stock_request'), level: 2, parent: 7, hasChild: false, active: false },
                { id: 9, label: Lang.t('label.stock_delivery_outbound'), level: 2, parent: 7, hasChild: false, active: false },
            ]
        },

        settings: {
            generic : {},
            branch : {},
            defaults: {
                generic : {},
                branch : {},
            },
        },

        udf: {
            module: null,
            options: [
                { id: MODULE.PURCHASE, label: Lang.t('label.purchase_order') },
                { id: MODULE.PURCHASE_RETURN, label: Lang.t('label.purchase_return') },
                { id: MODULE.DAMAGE, label: Lang.t('label.damage') },
                { id: MODULE.SALES, label: Lang.t('label.sales') },
                { id: MODULE.SALES_RETURN, label: Lang.t('label.sales_return') },
                { id: MODULE.STOCK_REQUEST, label: Lang.t('label.stock_request') },
                { id: MODULE.STOCK_DELIVERY, label: Lang.t('label.stock_delivery') },
                { id: MODULE.STOCK_RETURN, label: Lang.t('label.stock_return') },
                { id: MODULE.PURCHASE_INBOUND, label: Lang.t('label.purchase_inbound') },
                { id: MODULE.PURCHASE_RETURN_OUTBOUND, label: Lang.t('label.purchase_return_outbound') },
                { id: MODULE.DAMAGE_OUTBOUND, label: Lang.t('label.damage_outbound') },
                { id: MODULE.SALES_OUTBOUND, label: Lang.t('label.sales_outbound') },
                { id: MODULE.SALES_RETURN_INBOUND, label: Lang.t('label.sales_return_inbound') },
                { id: MODULE.STOCK_DELIVERY_OUTBOUND, label: Lang.t('label.stock_delivery_outbound') },
                { id: MODULE.STOCK_DELIVERY_INBOUND, label: Lang.t('label.stock_delivery_inbound') },
                { id: MODULE.STOCK_RETURN_OUTBOUND, label: Lang.t('label.stock_return_outbound') },
                { id: MODULE.STOCK_RETURN_INBOUND, label: Lang.t('label.stock_return_inbound') },
                { id: MODULE.INVENTORY_ADJUST, label: Lang.t('label.inventory_adjust') },
            ],
            table: {
                id: 'table-discount-head',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withPaginateCallback: true
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
            fields: new Data({
                label: '',
                type: UDF_TYPE.STR,
                visible: true
            }),
            types: [
                { id: UDF_TYPE.STR, label: Lang.t('label.string') },
                { id: UDF_TYPE.INTR, label: Lang.t('label.number') },
                { id: UDF_TYPE.DECIMAL, label: Lang.t('label.decimal') },
                { id: UDF_TYPE.DATE, label: Lang.t('label.date') },
                { id: UDF_TYPE.DATETIME, label: Lang.t('label.datetime') },
            ]
        },

        message: new Notification,
        confirm: {
            deleteUDF: new Data({
                visible: false,
                id: 0,
                index: 0
            }),
        },
    },

    mounted: function() {
        this.fetchSettings();
    },

    methods: {

        fetchSettings() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();
            
            BranchSettingsService.fetch()
                .then(function (response){
                    that.processing = false;
                    that.settings.branch = response.data.values.settings;
                    that.settings.defaults.branch = Object.assign({}, response.data.values.settings);
                })
                .catch(function(error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });

            GenericSettingsService.fetch()
                .then(function (response){
                    that.processing = false;
                    that.settings.generic = response.data.values.settings;
                    that.settings.defaults.generic = Object.assign({}, response.data.values.settings);
                })
                .catch(function(error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        changeUDFs() {
            this.udf.table.value.data = [];
            this.paginateUDFs();
        },

        paginateUDFs(page = 1) {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;

            UDFService.paginate({
                perPage: this.udf.table.settings.perPage,
                page: page,
                module: this.udf.module
            })
            .then(function (response){
                that.processing = false;
                that.udf.table.value.data = response.data.values.udfs;
                that.udf.table.value.pagination = response.data.values.meta.pagination;
            })
            .catch(function(error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        updateBranchSettings() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            BranchSettingsService.update(this.settings.branch)
                .then(function (response){
                    that.message.success = response.data.message;
                    that.processing = false;
                })
                .catch(function(error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        updateGenericSettings() {
            if (this.processing) {
                return false;
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            GenericSettingsService.update(this.settings.generic)
                .then(function (response){
                    that.message.success = response.data.message;
                    that.processing = false;
                })
                .catch(function(error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        storeUDF(data) {
            if(this.processing){
                return false
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            UDFService.store(data)
                .then(function (response) {
                    that.udf.table.value.data.push(response.data.values.udf);
                    that.message.success = response.data.message;
                    that.processing = false;
                    that.computePagination(that.udf.table.value.pagination, 'add');
                    that.udf.fields.reset();
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        updateUDF(data, index) {
            if(this.processing){
                return false
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            UDFService.update(data, data.id)
                .then(function (response) {
                    that.changeState(index, 'destroy');
                    that.message.success = response.data.message;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        saveUDF(data, index) {
            data = Object.assign(data, {
                module_id: this.udf.module
            });

            if (!this.isUDFValid(data)) {
                return false;
            }

            if (data.hasOwnProperty('id')) {
                this.updateUDF(data, index)
            } else {
                this.storeUDF(data);
            }
        },

        isUDFValid(data) {
            var validation = Validator.make(data, {
                label: 'required',
                module_id: 'required',
                visible: 'required',
                type: 'required'
            }, {
                'label.required': Lang.t('validation.label_required'),
                'module_id.required': Lang.t('validation.module_required'),
                'visible.required': Lang.t('validation.visible_required'),
                'type.required': Lang.t('validation.type_required'),
            });

            if (validation.fails()) {
                this.message.error.set(Obj.toArray(validation.getErrors()));
                return false;
            }

            return true;
        },

        deleteUDF() {
            if(this.processing){
                return false
            }

            let that = this;

            this.processing = true;
            this.message.reset();

            UDFService.destroy(this.confirm.deleteUDF.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.udf.table.value.data.splice(that.confirm.deleteUDF.index, 1);
                    that.paginateUDFs(that.udf.table.value.pagination.current_page);
                    that.confirm.deleteUDF.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.deleteUDF.visible = false;
                })
        },

        confirmDeleteUDF(data, index) {
            this.confirm.deleteUDF.visible = true
            this.confirm.deleteUDF.id = data.id;
            this.confirm.deleteUDF.index = index;
        },

        forceDefaultSheetNumberPrefix(attribute, event) {
            this.settings.generic[attribute] = this.settings.generic[attribute].replace(/\s+/g, '');

            if (this.settings.generic[attribute] === '') {
                this.settings.generic[attribute] = String(event.target.getAttribute('defaults'));
            }
        }
    },

    watch: {
        'confirm.deleteUDF.visible': function(visible) {
            if (!visible) {
                this.confirm.deleteUDF.reset();
            }
        },
    },
});