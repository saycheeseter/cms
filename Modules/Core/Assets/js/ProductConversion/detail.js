import CoreMixins from '../Common/CoreMixins';
import ProductMixins from '../Common/ProductMixins';
import ProductService from '../Services/ProductService';
import ProductConversionService from '../Services/ProductConversionService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, ProductMixins],

    components: {
        DateTimePicker
    },

    data: {
        info: new Data({
            id: 0,
            sheet_number: '-',
            transaction_date: Moment().format(),
            created_for: Laravel.auth.branch,
            product_id: 0,
            qty: 1,
            product_name: '',
            stock_number: '',
            barcode: '',
            approval_status: APPROVAL_STATUS.NEW,
            approval_status_label: Lang.t('label.new'),
            for_location: '',
            group_type: '',
            group_type_label: '',
            is_deleted: false,
            cost: 0
        }),

        columnSettings: {
            details: {
                visible: false,
                columns: [
                    { alias: 'stock_number', label: Lang.t('label.stock_no'), default: true },
                    { alias: 'barcode', label: Lang.t('label.barcode'), default: true },
                    { alias: 'required_qty', label: Lang.t('label.required_qty'), default: true },
                    { alias: 'allocated_qty', label: Lang.t('label.allocated_qty'), default: true },
                ],
                selections: {},
                module: {
                    id: MODULE.PRODUCT_CONVERSION,
                    key: 'detail'
                }
            }
        },

        filter: {
            product: {
                forced: [
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.NONE
                    },
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_GROUP
                    },
                    {
                        column: 'group_type',
                        join: 'and',
                        operator: '<>',
                        value: GROUP_TYPE.AUTOMATIC_UNGROUP
                    },
                    {
                        column: 'status',
                        join: 'and',
                        operator: '<>',
                        value: PRODUCT_STATUS.INACTIVE
                    },
                ]
            }
        },

        references: {
            branches: branches,
            locations: locations,
        },

        datatable: {
            detail: {
                id: 'datatable-id',
                settings: {
                    withPaging : true,
                    perPage: 10,
                    withActionCell: false,
                },
                value: {
                    data: [],
                    pagination: {}
                }
            },
        },

        controls: {
            draft: true,
            forApproval: false,
            approveDecline: false,
            revert: false,
            create: true,
        },

        dialog: {
            process: {
                message: ''
            },
            directApproval: {
                message: '',
                visible: false
            }
        },

        format: {
            info: {
                product_id: 'numeric',
                qty: 'numeric',
                cost: 'numeric'
            },
            details: {
                required_qty: 'numeric',
                cost: 'numeric'
            }
        },

        message: new Notification(['transaction']),

        service: new ProductConversionService,
    },

    created: function() {
        delete this.datatable.product.settings.withSelect;
    },

    mounted: function(){
        this.load();
    },

    methods: {
        visibility() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.DRAFT:
                    this.controls.forApproval = true;
                    break;
                    
                case APPROVAL_STATUS.FOR_APPROVAL:
                    this.controls.draft = false;
                    this.controls.approveDecline = true;
                    break;

                case APPROVAL_STATUS.APPROVED: 
                    this.controls.draft = false;

                    if (Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.MANUAL) {
                        this.controls.revert = true;
                    }
                    break;

                case APPROVAL_STATUS.DECLINED:
                    this.controls.draft = false;
                    this.controls.revert = true;
                    break;
            }
        },

        load() {
            if(this.processing || id === 'create') {
                return false;
            }

            this.processing = true;

            let that = this;

            this.service.show(id)
                .then(function (response){
                    that.info.set(response.data.values.record);
                    that.getLocations(that.info.for_location);
                    that.datatable.detail.value.data = that.formatAttributesTransactionDetails(response.data.values.details.data);

                    if (that.info.is_deleted) {
                        that.hideControls();
                    } else {
                        that.visibility();
                    }

                    that.processing = false;
                })
                .catch(function(error){
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        process(reload = false) {
            this.reload = reload;

            if ((this.info.id === 0 && ! permissions.create)
                || (this.info.id !== 0 && ! permissions.update)
            ) {
                return false;
            }

            if (this.info.id === 0) {
                this.store();
            } else {
                this.update();
            }
        },

        store() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.processing = true;
            this.message.reset();

            this.service.store(this.data)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }

                    that.info.id = response.data.values.record.id;
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        update() {
            if (this.processing) {
                return false;
            }

            let that = this;
            
            this.message.reset();
            
            this.service.update(this.data, that.info.id)
                .then(function (response) {
                    if(permissions.approve) {
                        that.showDirectApprovalDialog(response.data.message);
                    } else {
                        that.dialog.process.message = response.data.message;
                    }

                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        approval(status) {
            if(this.processing) {
                return false;
            }

            this.processing = true;
            
            let that = this;

            this.message.reset();
            
            switch(status) {
                case APPROVAL_STATUS.FOR_APPROVAL: 
                    this.updateBeforeForApproval();
                    break;

                default:
                    this.approvalRequest(status);
                    break;
            }
        },

        updateBeforeForApproval() {
            let that = this;
            let update = this.service.update(this.data, that.info.id);

            axios.all([update])
                .then(axios.spread(function (update){
                    that.approvalRequest(APPROVAL_STATUS.FOR_APPROVAL);
                }))
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        approvalRequest(status) {
            let that = this;

            this.service.approval({
                transactions: that.info.id,
                status: status
            })
            .then(function (response) {
                that.dialog.process.message = response.data.message;
                that.processing = false;
            })
            .catch(function (error) {
                that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            });
        },

        selectRow(data, target = "") {
            this.productPicker.visible = false;

            this.info.product_id = data.id;
            this.info.product_name = data.name;
            this.info.stock_number = data.stock_no;
            this.info.barcode = data.barcode;
            this.info.group_type = data.group_type;
            this.info.group_type_label = data.group_type_label;
            this.info.cost = data.purchase_price;

            let that = this;

            this.processing = true;

            ProductService.components(this.info.product_id)
                .then(function (response) {
                    that.datatable.detail.value.data = that.formatComponentResponseToTransactionDetails(response.data.values.components);
                    that.computeAllocated();
                    that.processing = false;
                })
                .catch(function (error) {
                    that.message.transaction.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
        },

        redirect() {
            if(this.info.id != 0 && !this.reload) {
                this.service.redirect(this.info.id);
            } else {
                this.service.create(this.reload);
            }
        },

        computeAllocated() {
            for(let index in this.datatable.detail.value.data) {
                let row = this.datatable.detail.value.data[index];

                let qty = 0;
                if(this.info.qty != null && this.info.qty != 0) {
                    qty = this.info.qty;
                }

                this.datatable.detail.value.data[index]['allocated_qty'] = Numeric.parse(qty) * Numeric.parse(row.required_qty);
            }
        },

        formatComponentResponseToTransactionDetails(details) {
            return this.formatAttributesTransactionDetails(details, function (formatted, detail) {
                return Object.assign({}, formatted, {
                    id: 0,
                    component_id: detail.id
                }); 
            });
        },

        formatAttributesTransactionDetails(details, hook = null) {
            let collection = [];

            details = Arr.wrap(details);

            for(let i = 0; i < details.length; i++) {
                let detail = details[i];

                let formatted = {
                    id: detail.hasOwnProperty('id') ? detail.id : 0,
                    component_id: detail.hasOwnProperty('component_id') ? detail.component_id : 0,
                    stock_number: detail.stock_number,
                    name: detail.name,
                    required_qty: detail.required_qty,
                    barcode: detail.barcode,
                    cost: detail.cost
                };

                if (hook != null) {
                    formatted = hook(formatted, detail);
                }

                collection.push(Object.assign({}, formatted));
            }

            return collection;
        },

        getLocations(def = null) {
            if(this.processing) {
                return false;
            }

            this.processing = true;

            let that = this;

            BranchService.details(this.info.created_for)
                .then(function (response) {
                    that.reference.locations = response.data.values.details;

                    let location = def === null
                        ? response.data.values.details[0].id
                        : def;

                    that.info.for_location = location;
                    that.action.reset();
                    that.processing = false;
                })
                .catch(function (response) {
                    that.reference.locations = [];
                    that.processing = false;
                });
        },

        hideControls() {
            this.controls.forApproval = false;
            this.controls.draft = false;
            this.controls.approveDecline = false;
            this.controls.revert = false;
        },

        directApproval() {
            this.approveTransaction();
            this.hideDirectApprovalDialog();
        },

        showDirectApprovalDialog(message) {
            this.dialog.directApproval.message = message;
            this.dialog.directApproval.visible = true;

            this.$nextTick(() => {
                this.$refs.saveOnly.focus();
            });
        },

        hideDirectApprovalDialog() {
            this.dialog.directApproval.message = '';
            this.dialog.directApproval.visible = false;
        },

        forApprovalTransaction() {
            this.approval(APPROVAL_STATUS.FOR_APPROVAL);
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },

        revertTransaction() {
            this.approval(APPROVAL_STATUS.DRAFT);
        }
    },

    computed: {
        data: function () {
            return {
                info: Format.make(this.info.data(), this.format.info),
                details: Format.make(this.datatable.detail.value.data, this.format.details)
            }
        },
        approvalClass: function() {
            switch(Number(this.info.approval_status)){
                case APPROVAL_STATUS.NEW:
                    return 'new';
                    break;

                case APPROVAL_STATUS.DRAFT:
                    return 'draft';
                    break;

                case APPROVAL_STATUS.FOR_APPROVAL:
                    return 'for-approval';
                    break;

                case APPROVAL_STATUS.APPROVED:
                    return 'approved';
                    break;
                    
                case APPROVAL_STATUS.DECLINED:
                    return 'declined';
                    break;
            }
        },
        disabled: function () {
            return this.info.is_deleted
                ? true
                : [APPROVAL_STATUS.NEW, APPROVAL_STATUS.DRAFT].indexOf(Number(this.info.approval_status)) === -1;
        },

        newTransaction: function() {
            return this.info.approval_status === APPROVAL_STATUS.NEW;
        },

        datepickerFormat: function() {
            let options = {
                format: 'yyyy-MM-dd'
            };

            if (Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.MANUAL) {
                options.disabled = {
                    to: new Date(Moment(Laravel.settings.transaction_summary_computation_last_post_date).add(1, 'days').format('YYYY-MM-DD'))
                }
            }

            return options;
        },

        disabledDate: function () {
            return (this.disabled || Laravel.settings.transaction_summary_computation_mode === TRANSACTION_SUMMARY_COMPUTATION.AUTOMATIC);
        }
    },

    watch: {
        'productPicker.visible': function(visible) {
            if(visible) {
                this.productSearch();
            }
        },
        'info.qty': function(value) {
            if(Numeric.parse(value) === 0) {
                this.info.qty = 1;
            }

            this.computeAllocated();
        },
    }
});