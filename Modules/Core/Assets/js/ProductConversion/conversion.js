import CoreMixins from '../Common/CoreMixins';
import Datepicker from 'vuejs-datepicker';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    components: {
        Datepicker
    },

    data: {
        selector: {
            transaction: {
                visible: false,
                datatable: {
                    id: 'datatable-id',
                    settings: {
                        withPaging : true,
                        perPage: 10
                    },
                    value: {
                        data: [],
                        pagination: {}
                    },
                    selected: [],
                },
                columnSettings: {
                    id: 'selector-column-settings',
                    visible: false,
                    options: []
                }
            }
        },

        productPicker: {
            visible: false
        },

        datatable: {
            product: {
                id: 'datatable-id',
                settings: {
                    withPaging : true,
                    perPage: 10
                },
                value: {
                    data: [],
                    pagination: {}
                },
                checked: []
            }
        },

        columnSettings: {
            product: {
                visible: false,
                columns: [],
                selections: []
            }
        },

        filter: {
            product: {
                visible: false,
                options: [],
                data: []
            }
        },

        message: new Notification,
    },

    methods: {

        add(cont = false) {
            return false;
        },

        get() {
            return false;
        },

        productSearch() {
            return false;
        }
    }
});