import CoreMixins from '../Common/CoreMixins.js';
import ProductConversionService from '../Services/ProductConversionService';

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins],

    data: {
        datatable: {
            id: 'table-list',
            settings: {
                withPaging : true,
                perPage: 10,
                withSelect: {
                    value: 'id'
                },
                withDelete: permissions.delete,
            },
            value: {
                data: [],
                pagination: {}
            },
            selected:[]
        },
        
        processing: false,

        confirm: new Data({
            visible: false,
            value: false,
            id: 0,
            index: 0
        }),

        filter: {
            visible: false,
            options: [
                {
                    name: Lang.t('label.branch'),
                    columnName: 'created_for',
                    type: 'select',
                    selectOptions: branches,
                    selected: true,
                    default: { value: String(Laravel.auth.branch) },
                    freezing: (Laravel.settings.main_branch != Laravel.auth.branch)
                },
                {
                    name: Lang.t('label.for_location'),
                    columnName: 'for_location',
                    type: 'select',
                    selectOptions: locations
                },
                { name: Lang.t('label.sheet_no'), columnName: 'sheet_number', type: 'string', optionBool: false },
                { name: Lang.t('label.remarks'), columnName: 'remarks', type: 'string', optionBool: false },
                {
                    name: Lang.t('label.transaction_date_time'),
                    columnName: 'transaction_date',
                    type: 'datetime',
                    optionBool: false,
                    selected: true,
                    default: [
                        { comparison: '>=', value: Moment().startOf('day').format() },
                        { comparison: '<', value: Moment().add(1, 'day').startOf('day').format() },
                    ]
                },
                {
                    name: Lang.t('label.approval_status'),
                    columnName: 'approval_status',
                    type: 'select',
                    selectOptions: [
                        { id: String(APPROVAL_STATUS.DRAFT), label: Lang.t('label.draft') },
                        { id: String(APPROVAL_STATUS.FOR_APPROVAL), label: Lang.t('label.for_approval') },
                        { id: String(APPROVAL_STATUS.APPROVED), label: Lang.t('label.approved') },
                        { id: String(APPROVAL_STATUS.DECLINED), label: Lang.t('label.declined') },
                    ]
                },
                {
                    name: Lang.t('label.deleted'),
                    columnName: 'deleted_at',
                    type: 'select',
                    selected: true,
                    selectOptions: [
                        { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                        { id: String(VOIDED.NO), label: Lang.t('label.no') },
                    ],
                    default: { value: String(VOIDED.NO) }
                },
                {
                    name: Lang.t('label.group_type'),
                    columnName: 'group_type',
                    type: 'select',
                    selectOptions: [
                        { id: String(GROUP_TYPE.MANUAL_GROUP), label: Lang.t('label.manual_group') },
                        { id: String(GROUP_TYPE.MANUAL_UNGROUP), label: Lang.t('label.manual_ungroup') }
                    ]
                },
            ],
            data: []
        },

        columnSettings: {
            id: 'column-settings',
            visible: false,
            selections: {},
            columns: [
                { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                { alias: 'created_from', label: Lang.t('label.created_from') },
                { alias: 'created_for', label: Lang.t('label.created_for'), default: true },
                { alias: 'for_location', label: Lang.t('label.for_location'), default: true },
                { alias: 'product', label: Lang.t('label.product'), default: true },
                { alias: 'qty', label: Lang.t('label.qty'), default: true },
                { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                { alias: 'created_date', label: Lang.t('label.created_date') },
                { alias: 'audited_by', label: Lang.t('label.audit_by') },
                { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                { alias: 'deleted', label: Lang.t('label.deleted') },
            ],
            module: {
                id: MODULE.PRODUCT_CONVERSION,
                key: 'info'
            }
        },

        service: new ProductConversionService,
        message: new Notification
    },

    mounted: function() {
        this.paginate();
    },

    methods: {
        paginate(page) {
            if (this.processing) {
                return false;
            }

            let that = this;

            that.filter.visible = false;
            that.processing = true;
            that.message.reset();

            Promises.push(
                this.service.paginate({
                    'perPage': that.datatable.settings.perPage,
                    'page': page,
                    'filters': that.filter.data,
                })
                .then(function (response) {
                    that.processing = false;
                    that.message.info = response.data.message;
                    that.datatable.value.data = response.data.values['records'];
                    that.datatable.value.pagination = response.data.values.meta.pagination;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                })
            );
        },

        destroy() {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            this.service.destroy(that.confirm.id)
                .then(function (response) {
                    that.processing = false;
                    that.message.success = response.data.message;
                    that.datatable.value.data.splice(that.confirm.index, 1);
                    that.paginate(that.datatable.value.pagination.current_page);
                    that.confirm.visible = false;
                })
                .catch(function (error) {
                    that.message.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                    that.confirm.visible = false;
                })
        },

        approval(status) {
            let that = this;

            if(that.processing){
                return false
            }

            that.processing = true;
            that.message.reset();

            if(that.datatable.selected.length == 0){
                that.message.error.set([Lang.t('validation.please_select_one_transaction')]);
                that.processing = false;
                return false
            }

            this.service.approval({
                transactions: that.datatable.selected,
                status: status
            })
            .then(function (response) {
                that.processing = false;
                that.message.success = response.data.message;
                that.paginate(that.datatable.value.pagination.current_page);
                that.datatable.selected = [];
             })
            .catch(function (error) {
                that.message.error.set(Obj.toArray(error.response.data.errors));
                that.processing = false;
            })
        },

        excel() {
            let params = {
                filters: this.filter.data,
                settings: this.columnSettings.selections
            };

            this.service.export(params);
        },
        
        create() {
            this.service.create();
        },

        show(id) {
            if(! permissions.view_detail && ! permissions.update) {
                return false;
            }

            this.service.edit(id);
        },

        confirmation(data, index) {
            this.confirm.visible = true
            this.confirm.id = data.id;
            this.confirm.index = index;
        },

        approveTransaction() {
            this.approval(APPROVAL_STATUS.APPROVED);
        },

        declineTransaction() {
            this.approval(APPROVAL_STATUS.DECLINED);
        },
    },

    watch: {
        'confirm.visible': function(visible) {
            if (!visible) {
                this.confirm.reset();
            }
        },
    }
});