import CoreMixins from '../Common/CoreMixins';
import SectionMixins from '../UserMenu/SectionMixins';
import UserMenuService from '../Services/UserMenuService';
import Sortable from 'sortablejs';

Vue.directive('sortable', {
    inserted: function (el, binding) {
        let sortable = new Sortable(el, binding.value || {});
    }
});

let app = new Vue({
    el: '#app',

    mixins: [CoreMixins, SectionMixins],

    components: {
        MyMenu
    },

    data: {
        modal : {
            addSection: {
                visible: false,
            },
            addFolder: {
                visible: false,
            },
        },

        menu: [],
        menu_type: MENU_TYPE,
        confirm: {
            section: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
            folder: new Data({
                visible: false,
                value: false,
                id: 0,
                index: 0
            }),
        },
        message: new Notification(['head', 'modal']),

        customMenuPane : {
            addMenu: {
                value: {
                    attributes: {},
                    children: [],
                    label: '',
                    type: '',
                    checker: {}
                },
                values: []
            },

            selected: {
                section: {
                    label: '',
                    index: null
                },
                modules: [],
                location: '',
                folder: []
            },

            forSort: [],
        },

        existingMenuPane : {
            selected: {
                modules: []
            }
        },
    },

    created: function(){
        MyMenuDispatcher.$on('selectForRemoval', this.updateForRemovals);
        MyMenuDispatcher.$on('setTargetFolder', this.setTargetFolder);
        MyMenuDispatcher.$on('removeFolderFromMenu', this.confirmRemoveFolderFromMenu);
        MyMenuDispatcher.$on('movePositionOfMenu', this.movePositionOfMenu);
    },

    mounted: function() {
        this.loadCustomMenu();
        this.setDefaults();
    },

    methods: {
        loadCustomMenu(){
            for (let i = 0; i < menu.length; i++) {
                let obj = Object.assign(menu[i], {key: menu[i].label});
                this.menu.push(obj);
            }
        },

        sortSection ({oldIndex, newIndex}) {
            const movedItem = this.menu.splice(oldIndex, 1)[0]
            this.menu.splice(newIndex, 0, movedItem)
        },

        movePositionOfMenu(emitValue) {
            let indices = emitValue.location.split("|");
            let operator = emitValue.operator;

            let holder = this.menu[indices[0]];

            //loop through target index based on the folder selected
            for (let i = 1; i < indices.length  - 1 ; i++) {
                holder = holder.children[indices[i]];
            }

            let oldIndex = indices[indices.length - 1];
            let newIndex = oldIndex;
            if(operator === '-') {
                newIndex =  Numeric.parse(oldIndex) - 1;
            }

            if(operator === '+') {
                newIndex = Numeric.parse(oldIndex) + 1;
            }

            const movedItem = holder.children.splice(oldIndex, 1)[0]
            holder.children.splice(newIndex, 0, movedItem);
        },

        sortMenu({oldIndex, newIndex}) {
            let holder = this.menu[0].children;
            const movedItem = holder.splice(oldIndex, 1)[0];
            holder.splice(newIndex, 0, movedItem);
        },

        setTargetFolder(location) {
            this.customMenuPane.selected.folder = [];
            if(location !== '') {
                this.customMenuPane.selected.folder = String(location).split("|");
            }
        },

        selectModule(sectionKey, moduleKey) {
            let module =  this.sections[sectionKey].modules[moduleKey];

            if(module.selected) {
                this.sections[sectionKey].modules[moduleKey].selected = false;

                for (let i = 0; i < this.existingMenuPane.selected.modules.length; i++) {
                    if(module.label === this.existingMenuPane.selected.modules[i].label) {
                        this.existingMenuPane.selected.modules.splice(i, 1);
                    }
                }

            } else {
                this.sections[sectionKey].modules[moduleKey].selected = true;
                this.existingMenuPane.selected.modules.push(module);
            }
        },

        setDefaults() {
            for(let sectionKey in this.sections) {
                for(let moduleKey in this.sections[sectionKey].modules) {
                    this.sections[sectionKey].modules[moduleKey] = {
                        label: Lang.t('label.' + moduleKey),
                        sprite: moduleKey.replace(/_/g, ''),
                        url: this.sections[sectionKey].modules[moduleKey].hasOwnProperty('url')
                            ? this.sections[sectionKey].modules[moduleKey].url
                            : moduleKey.replace(/_/g, '-'),
                        active_path: this.sections[sectionKey].modules[moduleKey].hasOwnProperty('active_path')
                            ? this.sections[sectionKey].modules[moduleKey].active_path
                            : moduleKey.replace(/_/g, '-'),
                        selected: false,
                        visible: true,
                        checker: this.sections[sectionKey].modules[moduleKey].hasOwnProperty('checker')
                            ? this.sections[sectionKey].modules[moduleKey].checker
                            : { type: 0, value: moduleKey },
                    };
                }
            }

            let reports = {};

            for (let i = 0; i < customReports.length; i++) {
                let reportsPerModule = customReports[i];

                for(let module in reportsPerModule){
                    for (let x = 0; x < reportsPerModule[module].length; x++) {
                        let report = reportsPerModule[module][x];

                        Vue.set(reports, report.slug, {
                            label: report.name,
                            sprite: 'report',
                            url: 'reports/custom/'+  report.slug,
                            active_path: 'reports/custom/' + report.slug,
                            selected: false,
                            visible: true,
                            checker: {
                                type: 1,
                                value: 'view_' + report.slug.replace(/-/g, '_')
                            }
                        });
                    }
                }
            }


            Vue.set(this.sections, 'custom_report', {
                label: Lang.t('label.custom_report'),
                modules: reports
            });

            this.removeModulesFromExistingMenu();
        },

        addSectionOnMenu() {

            if(!this.validateAddMenu()) {
                return false;
            }

            this.customMenuPane.addMenu.value.type = MENU_TYPE.SECTION;
            this.menu.push( Object.assign(this.customMenuPane.addMenu.value, {key: this.customMenuPane.addMenu.value.label}));
            this.modal.addSection.visible = false;
            this.resetAddMenuValue();
        },

        addFolderOnMenu() {

            if(!this.validateAddMenu()) {
                return false;
            }
            //assign section value on holder
            let holder = this.menu[this.customMenuPane.selected.section.index];
            let that = this;

            //loop through target index based on the folder selected
            for (let i = 1; i < this.customMenuPane.selected.folder.length; i++) {
                holder = holder.children.find( function(record) {
                    if(record.label === that.customMenuPane.selected.folder[i]) {
                        return record
                    }
                });
            }

            this.customMenuPane.addMenu.value.type = MENU_TYPE.FOLDER;
            //push value on the last children based on the selected indices
            holder.children.push(this.customMenuPane.addMenu.value);

            this.modal.addFolder.visible = false;
            this.resetSelectedSectionValue();
            this.resetAddMenuValue();
        },

        addModuleOnMenu() {

            if(this.customMenuPane.selected.section.index === null &&
                this.customMenuPane.selected.folder.length === 0
            ) {
                this.message.head.error.set([Lang.t('validation.no_target_folder_section')]);
                return;
            }

            //loop through the selected modules on the existing menus pane
            for (let i = 0; i < this.existingMenuPane.selected.modules.length; i++) {
                let module = this.existingMenuPane.selected.modules[i];

                //push on the addmenu values
                //format strings based on specification
                this.customMenuPane.addMenu.values.push({
                    children: [],
                    label: module.label,
                    type: MENU_TYPE.MODULE,
                    attributes: {
                        sprite: module.sprite.replace(/_/g, ''),
                        url: module.url,
                        active_path: module.active_path,
                        checker: module.checker
                    },
                });
            }

            let holder;

            if(this.customMenuPane.selected.section.index !== null) {
                holder = this.menu[this.customMenuPane.selected.section.index];
            } else {
                let that = this;

                holder = this.menu.find( function(record) {
                    if(record.key === that.customMenuPane.selected.folder[0]) {
                        return record
                    }
                });

                for (let i = 1; i < this.customMenuPane.selected.folder.length; i++) {
                    holder = holder.children.find( function(record) {
                        if(record.label === that.customMenuPane.selected.folder[i]) {
                            return record
                        }
                    });
                }
            }

            //push values on the custom menu pane
            for (let i = 0; i < this.customMenuPane.addMenu.values.length; i++) {
                holder.children.push(this.customMenuPane.addMenu.values[i]);
            }

            //set all modules selected to false
            for(let sectionKey in this.sections){
                for(let moduleKey in this.sections[sectionKey].modules) {
                    this.sections[sectionKey].modules[moduleKey].selected  = false;
                }
            }

            this.customMenuPane.addMenu.values = [];
            this.existingMenuPane.selected.modules = [];
            this.resetSelectedSectionValue();
            this.removeModulesFromExistingMenu();
        },

        removeSectionFromMenu() {
            let modules = this.getMenuPerType(this.menu[this.confirm.section.id].children, MENU_TYPE.MODULE);

            if(modules.length > 0) {
                for(let sectionKey in this.sections){
                    for(let moduleKey in this.sections[sectionKey].modules) {
                        if(modules.indexOf(this.sections[sectionKey].modules[moduleKey].label) !== -1) {
                            this.sections[sectionKey].modules[moduleKey].visible = true;
                        }
                    }
                }
            }

            this.menu.splice(this.confirm.section.id, 1);
            this.confirm.section.visible = false;
        },

        removeModuleFromMenu() {
            for (let i = 0; i < this.customMenuPane.selected.modules.length; i++) {
                if(!this.processing) {
                    let target = this.customMenuPane.selected.modules[i].location.split("|");
                    let label = this.customMenuPane.selected.modules[i].label;

                    let holder = this.menu[target[0]];
                    this.remove(target);

                    for(let sectionKey in this.sections){
                        for(let moduleKey in this.sections[sectionKey].modules) {
                            if(this.sections[sectionKey].modules[moduleKey].label === label) {
                                this.sections[sectionKey].modules[moduleKey].visible = true;
                            }
                        }
                    }

                    this.processing = false;
                }
            }

            this.customMenuPane.selected.modules = [];
            this.toggleMyMenuSelected();
        },

        confirmRemoveFolderFromMenu(location) {
            this.confirm.folder.visible = true;
            this.confirm.folder.id = location
        },

        removeFolderFromMenu() {

            let target = this.confirm.folder.id.split("|");

            let modules = this.remove(target);

            if(modules.length > 0) {
                for(let sectionKey in this.sections){
                    for(let moduleKey in this.sections[sectionKey].modules) {
                        if(modules.indexOf(this.sections[sectionKey].modules[moduleKey].label) !== -1) {
                            this.sections[sectionKey].modules[moduleKey].visible = true;
                        }
                    }
                }
            }
            this.confirm.folder.visible = false;
        },

        remove(target) {

            let holder = this.menu.find( function(record) {
                if(record.key === target[0]) {
                    return record
                }
            });

            for (let a = 1; a < target.length - 1; a++) {
                holder = holder.children.find( function(record) {
                    if(record.label === target[a]) {
                        return record
                    }
                });
            }

            let that = this;
            let modules = [];

            holder = holder.children.find( function(record, index) {
                if(record !== undefined) {
                    if(record.label === target[target.length - 1]) {
                        modules = that.getMenuPerType(holder.children[index].children, MENU_TYPE.MODULE)
                        holder.children.splice(index, 1);
                    }
                }
            });

            return modules;
        },

        toggleMyMenuSelected() {
            for (let i = 0; i < this.$children.length; i++) {
                let componentName = this.$children[i].$options._componentTag;

                if(componentName === 'my-menu'){
                    this.$children[i].removeSelect();
                }
            }
        },

        updateForRemovals(emitValue) {
            if(emitValue.selected) {
                this.customMenuPane.selected.modules.push({
                    location: emitValue.location,
                    label: emitValue.label
                });
            } else {
                this.customMenuPane.selected.modules.splice(
                    this.customMenuPane.selected.modules.indexOf(emitValue.location),
                    1
                );
            }
        },

        getMenuPerType(menus, type, holder = []) {

            for (let i = 0; i < menus.length; i++) {
                let menu =  menus[i];

                if(menu.type === type) {
                    holder.push(menu.label);
                }

                if(menu.children.length > 0) {
                    this.getMenuPerType(menu.children, type, holder);
                }
            }

            return holder;
        },

        /**
         * set visibilty of the modules that are on the custom menu to false
         */
        removeModulesFromExistingMenu() {
            let modules = this.getMenuPerType(this.menu, MENU_TYPE.MODULE);

            if(modules.length > 0) {
                for(let sectionKey in this.sections){
                    for(let moduleKey in this.sections[sectionKey].modules) {
                        if(modules.indexOf(this.sections[sectionKey].modules[moduleKey].label) !== -1) {
                            this.sections[sectionKey].modules[moduleKey].visible = false;
                        }
                    }
                }
            }
        },

        save() {
            if(this.processing) {
                return false;
            }

            if(!this.validateFormat()) {
                return false;
            }

            this.processing = true;

            let that = this;

            UserMenuService.save({format: this.menu})
                .then(function (response){
                    that.message.head.success = response.data.message;
                    that.processing = false;
                    location.reload();
                })
                .catch(function(error){
                    that.message.head.error.set(Obj.toArray(error.response.data.errors));
                    that.processing = false;
                });
        },

        validateFormat() {
            let errors = [];

            for (let i = 0; i < this.menu.length; i++) {
                if(this.menu[i].label === '') {
                    errors.push(Lang.t('validation.section_folder_label_required'))
                }
            }

            if(errors.length > 0) {
                this.message.head.error.set(errors);
                return false;
            }

            return true
        },

        selectSection(section, index) {
            if(this.customMenuPane.selected.section.label === section) {
                this.resetSelectedSectionValue();
            } else {
                this.customMenuPane.selected.section.label = section;
                this.customMenuPane.selected.section.index = index;
            }
        },

        isSectionSelected(section) {
            if(this.customMenuPane.selected.section.label === section) {
                return 'selected';
            }
        },

        resetAddMenuValue() {
            this.customMenuPane.addMenu.value = {
                attributes: {},
                children: [],
                label: '',
                type: '',
                checker: {}
            }
        },

        resetSelectedSectionValue() {
            this.customMenuPane.selected.section = {
                label: '',
                index: null
            };
        },

        validateAddMenu() {
            let errors = [];

            if(this.customMenuPane.addMenu.value.label.trim() === '') {
                errors.push(Lang.t('validation.section_folder_label_required'))
            }

            let folders = this.getMenuPerType(this.menu, MENU_TYPE.FOLDER);
            let sections = this.getMenuPerType(this.menu, MENU_TYPE.SECTION);

            if(folders.indexOf(this.customMenuPane.addMenu.value.label) !== -1
                || sections.indexOf(this.customMenuPane.addMenu.value.label) !== -1
            ) {
                errors.push(Lang.t('validation.duplicate_section_folder_label'))
            }

            if(errors.length > 0) {
                this.message.modal.error.set(errors);
                return false;
            }

            return true;
        },
    }
});