export default {
    data: {
        sections : {
            data : {
                label: Lang.t('label.data'),
                modules : {
                    product : {},
                    brand : {},
                    branch : {},
                    category : {},
                    supplier : {},
                    customer : {},
                    member : {},
                    member_rate : {},
                    user : {},
                    role : {},
                    bank : {},
                    bank_account : {},
                    uom : {},
                    reason : {},
                    payment_type : {},
                    income_type : {},
                }
            },
            purchase_other_income : {
                label: Lang.t('label.poi'),
                modules : {
                    purchase_order : { checker: { type: 0, value: 'purchase' }},
                    purchase_inbound: {},
                    purchase_return: {},
                    purchase_return_outbound: {},
                    payment: {},
                    other_payment: {},
                    issued_check: {},
                    damage: {},
                    damage_outbound: {},
                }
            },
            sales_other_income : {
                label: Lang.t('label.soi'),
                modules : {
                    sales: {},
                    sales_outbound: {},
                    sales_return: {},
                    sales_return_inbound: {},
                    counter: {},
                    collection: {},
                    other_income: {},
                    received_check: {}
                }
            },
            stock_transferring : {
                label: Lang.t('label.stock_transferring'),
                modules : {
                    stock_request_to: { checker: { type: 0, value: 'stock_request' }},
                    stock_request_from: { checker: { type: 0, value: 'stock_request|viewFrom' }},
                    stock_delivery: {},
                    stock_delivery_outbound_to: { checker: { type: 0, value: 'stock_delivery_outbound' }},
                    stock_delivery_outbound_from: { checker: { type: 0, value: 'stock_delivery_outbound|viewFrom' }},
                    stock_delivery_inbound: {},
                    stock_return: {},
                    stock_return_outbound: {},
                    stock_return_inbound: {},
                }
            },
            others: {
                label: Lang.t('label.others'),
                modules: {
                    inventory_adjust: {},
                    gift_check: {},
                    printout_template: {},
                    excel_template: {},
                    product_conversion: {},
                    custom_report_builder: {
                        url: 'reports/builder',
                        active_path: 'reports/builder',
                        checker: { type: 0, value: 'report_builder' }
                    },
                    product_discount: {},
                    pos_summary_posting: { checker: { type: 2, value: true }}
                }
            },
            data_collector: {
                label: Lang.t('label.data_collector'),
                modules: {
                    data_collector: { checker: { type: 2, value: true }},
                }
            },
            reports : {
                label: Lang.t('label.reports'),
                modules : {
                    customer_product_price: {
                        url: 'reports/customer/product-price',
                        active_path: 'reports/customer/product-price',
                        checker: { type: 1, value: 'view_customer_product_price' },
                    },
                    customer_balance: {
                        url: 'reports/customer/balance',
                        active_path: 'reports/customer/balance',
                        checker: { type: 1, value: 'view_customer_balance' }
                    },
                    supplier_product_price: {
                        url: 'reports/supplier/product-price',
                        active_path: 'reports/supplier/product-price',
                        checker: { type: 1, value: 'view_supplier_product_price' }
                    },
                    supplier_balance: {
                        url: 'reports/supplier/balance',
                        active_path: 'reports/supplier/balance',
                        checker: { type: 1, value: 'view_supplier_balance' }
                    },
                    audit_trail: {
                        url: 'reports/audit-trail',
                        active_path: 'reports/audit-trail',
                        checker: { type: 1, value: 'view_audit_trail' }
                    },
                    product_inventory_value: {
                        url: 'reports/product/inventory-value',
                        active_path: 'reports/product/inventory-value',
                        checker: { type: 1, value: 'view_product_inventory_value' }
                    },
                    product_inventory_warning: {
                        url: 'reports/product/inventory-warning',
                        active_path: 'reports/product/inventory-warning',
                        checker: { type: 1, value: 'view_product_inventory_warning' }
                    },
                    supplier_monthly_sales_ranking: {
                        url: 'reports/supplier/monthly-sales-ranking',
                        active_path: 'reports/supplier/monthly-sales-ranking',
                        checker: { type: 1, value: 'view_supplier_monthly_sales_ranking' }
                    },
                    category_monthly_sales_ranking: {
                        url: 'reports/category/monthly-sales-ranking',
                        active_path: 'reports/category/monthly-sales-ranking',
                        checker: { type: 1, value: 'view_category_monthly_sales_ranking' }
                    },
                    brand_monthly_sales_ranking: {
                        url: 'reports/brand/monthly-sales-ranking',
                        active_path: 'reports/brand/monthly-sales-ranking',
                        checker: { type: 1, value: 'view_brand_monthly_sales_ranking' }
                    },
                    product_monthly_sales_ranking: {
                        url: 'reports/product/monthly-sales-ranking',
                        active_path: 'reports/product/monthly-sales-ranking',
                        checker: { type: 1, value: 'view_product_monthly_sales_ranking' }
                    },
                    product_serial_transaction_report: {
                        url: 'reports/product/serial-transaction',
                        active_path: 'reports/product/serial-transaction',
                        checker: { type: 1, value: 'view_product_serial_transaction_report' }
                    },
                    product_periodic_sales_report: {
                        url: 'reports/product/periodic-sales',
                        active_path: 'reports/product/periodic-sales',
                        checker: { type: 1, value: 'view_product_periodic_sales' }
                    },
                    cash_transaction: {
                        url: 'reports/cash-transaction',
                        active_path: 'reports/cash-transaction',
                        checker: { type: 1, value: 'view_cash_transaction' }
                    },
                    product_branch_inventory_report: {
                        url: 'reports/product/branch-inventory',
                        active_path: 'reports/product/branch-inventory',
                        checker: { type: 1, value: 'view_product_branch_inventory' }
                    },
                    salesman_report: {
                        url: 'reports/salesman/report',
                        active_path: 'reports/salesman/report',
                        checker: { type: 1, value: 'view_salesman_report' }
                    },
                    bank_transaction: {
                        url: 'reports/bank-transaction',
                        active_path: 'reports/bank-transaction',
                        checker: { type: 1, value: 'view_bank_transaction' }
                    },
                    income_statement: {
                        url: 'reports/income-statement',
                        active_path: 'reports/income-statement',
                        checker: { type: 1, value: 'view_income_statement' }
                    },
                    pos_sales_report: {
                        url: 'reports/pos-sales',
                        active_path: 'reports/pos-sales',
                        checker: { type: 1, value: 'view_pos_sales_report' }
                    },
                    product_transaction_list_report: {
                        url: 'reports/product/transaction-list',
                        active_path: 'reports/product/transaction-list',
                        checker: { type: 1, value: 'view_product_transaction_list' }
                    },
                    negative_profit_product_report: {
                        url: 'reports/product/negative-profit',
                        active_path: 'reports/product/negative-profit',
                        checker: { type: 1, value: 'view_negative_profit_product' }
                    },
                    member_report: {
                        url: 'reports/member/report',
                        active_path: 'reports/member/report',
                        checker: { type: 1, value: 'view_member_report' }
                    },
                    payment_due_warning: {
                        url: 'reports/payment/due-warning',
                        active_path: 'reports/payment/due-warning',
                        checker: { type: 1, value: 'view_payment_due_warning' }
                    },
                    collection_due_warning: {
                        url: 'reports/collection/due-warning',
                        active_path: 'reports/collection/due-warning',
                        checker: { type: 1, value: 'view_collection_due_warning' }
                    },
                    discount_summary_report: {
                        url: 'reports/pos/discounted-summary',
                        active_path: 'reports/pos/discounted-summary',
                        checker: { type: 1, value: 'view_pos_discounted_summary' }
                    },
                    senior_transaction_report: {
                        url: 'reports/senior/transaction-report',
                        active_path: 'reports/senior/transaction-report',
                        checker: { type: 1, value: 'view_senior_transaction' }
                    }
                }
            }
        }
    }
}