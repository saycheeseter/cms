import FinancialFlow from '../FinancialFlow/list.js';
import CollectionService from '../Services/CollectionService';

let app = new Vue({
    el: '#app',

    mixins: [FinancialFlow],

    data: {
        modal: {
            columnSettings: {
                columns: [
                    { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                    { alias: 'date', label: Lang.t('label.date'), default: true },
                    { alias: 'location', label: Lang.t('label.location'), default: true },
                    { alias: 'customer', label: Lang.t('label.customer'), default: true },
                    { alias: 'customer_type', label: Lang.t('label.customer_type'), default: true },
                    { alias: 'amount', label: Lang.t('label.amount'), default: true },
                    { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                    { alias: 'deleted', label: Lang.t('label.deleted'), default: true },
                ],
                module: {
                    id: MODULE.COLLECTION,
                    key: 'info'
                }
            },
            filter: {
                options: Arr.merge(FinancialFlow.data.modal.filter.options, [
                    {
                        name: Lang.t('label.deleted'),
                        columnName: 'collection.deleted_at',
                        type: 'select',
                        selected: true,
                        selectOptions: [
                            { id: String(VOIDED.YES), label: Lang.t('label.yes') },
                            { id: String(VOIDED.NO), label: Lang.t('label.no') },
                        ],
                        default: { value: String(VOIDED.NO) }
                    },
                    {
                        name: Lang.t('label.customer'),
                        columnName: 'customer.name',
                        type: 'string', 
                        optionBool: true,
                        tableOptions: {
                            data: customers,
                            headers: ['id', Lang.t('label.customer')], 
                            columns: ['id', 'name'], 
                            returncolumn: 'name' 
                        }
                    },
                    {
                        name: Lang.t('label.customer_type'),
                        columnName: 'customer_type',
                        type: 'select',
                        selectOptions: [
                            {id: CUSTOMER.WALK_IN, label: Lang.t('label.walk_in_customer')},
                            {id: CUSTOMER.REGULAR, label: Lang.t('label.regular_customer')}
                        ]
                    },
                ])
            }
        },
        service: new CollectionService
    },
});