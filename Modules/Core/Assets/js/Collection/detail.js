import FinancialFlow from '../FinancialFlow/detail.js';
import SalesOutboundService from '../Services/SalesOutboundService';
import CollectionService from '../Services/CollectionService';
import CustomerService from '../Services/CustomerService';
import SalesReturnInboundService from "../Services/SalesReturnInboundService";

let app = new Vue({
    el: '#app',

    mixins: [FinancialFlow],

    data: {
        selector: {
            transaction: {
                columnSettings: {
                    columns: [
                        { alias: 'transaction_date', label: Lang.t('label.transaction_date'), default: true },
                        { alias: 'created_for', label: Lang.t('label.created_for') },
                        { alias: 'for_location', label: Lang.t('label.for_location') },
                        { alias: 'remarks', label: Lang.t('label.remarks'), default: true },
                        { alias: 'customer', label: Lang.t('label.customer') },
                        { alias: 'customer_detail', label: Lang.t('label.customer_detail') },
                        { alias: 'salesman', label: Lang.t('label.salesman') },
                        { alias: 'requested_by', label: Lang.t('label.request_by'), default: true },
                        { alias: 'created_by', label: Lang.t('label.created_by'), default: true },
                        { alias: 'created_date', label: Lang.t('label.created_date') },
                        { alias: 'audited_by', label: Lang.t('label.audit_by') },
                        { alias: 'audited_date', label: Lang.t('label.audit_date'), default: true},
                        { alias: 'deleted', label: Lang.t('label.deleted') },
                        { alias: 'approval_status', label: Lang.t('label.approval_status'), default: true },
                        { alias: 'reference', label: Lang.t('label.reference'), },
                        { alias: 'amount', label: Lang.t('label.amount'), default: true },
                        { alias: 'remaining_amount', label: Lang.t('label.remaining_amount'), default: true },
                    ],
                    module: {
                        id: MODULE.COLLECTION,
                        key: 'transaction'
                    }
                }
            }
        },

        format: {
            references: {
                collectible: 'numeric'
            }
        },

        info: new Data({
            sheet_number: '-',
            id: 0,
            created_for: Laravel.auth.branch,
            customer: null,
            entity_id: null,
            transaction_date: '',
            approval_status: APPROVAL_STATUS.NEW,
            remarks: '',
            customer_type: CUSTOMER.REGULAR,
            is_deleted: false
        }),

        references: {
            selectedCustomerType: customers,
        },

        service: {
            payment: new CollectionService,
            balance: CustomerService,
        },

        keys: {
            //balance: 'customer',
            amount: 'collectible'
        },

        attachment: {
            module: MODULE.COLLECTION
        },
    },

    methods: {
        showSalesOutboundTransactionSelector() {
            this.service.transaction = new SalesOutboundService();
            this.selector.transaction.type = types.sales_outbound;
            this.showTransactionSelector();
        },

        showSalesReturnInboundTransactionSelector() {
            this.service.transaction = new SalesReturnInboundService();
            this.selector.transaction.type = types.sales_return_inbound;
            this.showTransactionSelector();
        },

        getTransactionSelectorApi(settings) {
            return this.service.transaction.transactionsWithBalance(Object.assign({},
                {
                    branch_id: this.info.created_for,
                    customer: this.info.customer,
                    customer_type: this.info.customer_type
                },
                settings)
            );
        },

        validateRequiredEntity() {
            if(this.info.customer === null) {
                this.message.transaction.error.set([Lang.t('validation.customer_required')]);
                return false;
            }

            return true;
        },

        setCustomerSelection() {
            if(this.isRegularCustomer) {
                this.references.selectedCustomerType = customers;
            } else if(this.isWalkInCustomer) {
                this.references.selectedCustomerType = walkInCustomers;
            }

            this.action.reset();
        },

        removeTransactionsAndPayments() {
            this.datatable.transactions.value.data = [];
            this.datatable.payments.value.data = [];
            this.confirm.warning.visible = false;
            this.setCustomerSelection();
        },

        afterSuccessfulLoad(response) {
            this.setCustomerSelection();
            //
            // if (this.isRegularCustomer) {
            //     this.getBalance();
            // }
        },

        isSalesOutbound(type) {
            return type === types.sales_outbound;
        },
    },

    watch: {
        'info.created_for': function(newValue, oldValue) {
            this.buffer.data.created_for = oldValue;
            this.buffer.data.customer = this.info.customer;
            this.buffer.data.customer_type = this.info.customer_type;
        },
        'info.customer': function(newValue, oldValue) {
            this.buffer.data.customer = oldValue;
            this.buffer.data.created_for = this.info.created_for;
            this.buffer.data.customer_type = this.info.customer_type;
            this.info.entity_id = newValue;
        },
        'info.customer_type': function(newValue, oldValue) {
            this.buffer.data.customer_type = oldValue;
            this.buffer.data.customer = this.info.customer;
            this.buffer.data.created_for = this.info.created_for;
        },
        'action.value': function(newValue) {
            switch(newValue) {
                // case 'customer':
                //     if(this.isRegularCustomer) {
                //         this.getBalance();
                //     }
                //     break;
                    
                case 'customer_type':
                    this.setCustomerSelection();
                    break;
            }
        }
    },

    computed: {
        isRegularCustomer: function () {
            return this.info.customer_type === CUSTOMER.REGULAR;
        },

        isWalkInCustomer: function () {
            return this.info.customer_type === CUSTOMER.WALK_IN;
        },

        regularCustomer: function () {
            return CUSTOMER.REGULAR;
        },

        walkInCustomer: function () {
            return CUSTOMER.WALK_IN;
        },

        isApprovedOrDeclined: function () {
            return Arr.in(this.info.approval_status, [
                APPROVAL_STATUS.APPROVED,
                APPROVAL_STATUS.DECLINED
            ]);
        }
    }
});