import IssuedCheckService from '../Services/IssuedCheckService';
import CheckManagement from '../CheckManagement/list.js';

let app = new Vue({
    el: '#app',

    mixins: [CheckManagement],

    data: {
        service: new IssuedCheckService,
        modal: {
            columnSettings: {
                module: MODULE.ISSUED_CHECK
            }
        }
    },
});