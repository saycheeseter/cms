<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use DB;
use Exception;

class CreateBranchPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:create-branch-permission {user} {branch} {permission=0} {--r|range= : Permission range separated by hyphen}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert new user branch permission value (by value or range).';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $user = (int) $this->argument('user');
            $branch = (int) $this->argument('branch');
            $permission = (int) $this->argument('permission');
            $range = $this->option('range');

            if (empty($range) && $permission === 0) {
                throw new Exception('No permission provided.');
            }

            $values = [];

            if ($permission !== 0) {
                $values[] = [
                    'branch_id' => $branch,
                    'user_id' => $user,
                    'permission_id' => $permission
                ];
            } else if (!empty($range)) {
                $range = explode('-', $range);

                for ($i = (int) $range[0]; $i <= (int) $range[1]; $i++) {
                    $values[] = [
                        'branch_id' => $branch,
                        'user_id' => $user,
                        'permission_id' => (int) $i
                    ];
                }
            }

            DB::table('branch_permission_user')->insert($values);
            $this->info('Done creating branch permission');
        } catch (Exception $e) {
            $this->error('Something went wrong. Message: '.$e->getMessage());
        }
    }
}