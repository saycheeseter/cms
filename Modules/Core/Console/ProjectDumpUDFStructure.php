<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\UserDefinedField;
use Storage;

class ProjectDumpUDFStructure extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:dump-udf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump the list of tables with udfs in a json file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = 'udf.json';

        $udfs = UserDefinedField::all();

        $data = [];

        if ($udfs->count() === 0) {
            $this->error('No UDF found.');
        } else {
            foreach ($udfs as $udf) {
                if (!$udf->visible) {
                    continue;
                }

                $data[$udf->table] = array_merge(
                    $data[$udf->table] ?? [],
                    [
                        $udf->alias => is_array($udf->field_type )
                            ? $udf->field_type[0]
                            : $udf->field_type
                    ]
                );
            }
        }

        Storage::disk('local')->put($filename, json_encode($data));

        $this->info('Successfully compiled the UDF structure to storage/app/'. $filename);

        return true;
    }
}