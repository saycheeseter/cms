<?php

namespace Modules\Core\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Auth;
use App;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Services\Contracts\InventoryAdjustServiceInterface;

class ProjectRevertInvAdjust extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:revert-inv-adjust';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a revert Inventory Adjustment based on the given transaction sheet number';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        Auth::loginUsingId(1);
        Auth::loginBranchUsingId(1);

        $service = App::make(InventoryAdjustServiceInterface::class);
        $count = $this->ask('How many sheets to revert');
        $sheets = [];

        for ($i = 0; $i < $count; $i++) {
            $sheets[] = $this->ask(sprintf('Sheet Number[%s]', ($i + 1)));
        }

        $transactions = InventoryAdjust::with(['details'])
            ->whereIn('sheet_number', $sheets)
            ->get();

        foreach ($transactions as $transaction) {
            $data = [
                'info' => [
                    'created_for' => $transaction->created_for,
                    'for_location' => $transaction->for_location,
                    'requested_by' => Auth::user()->id,
                    'transaction_date' => Carbon::now()->toDateTimeString(),
                    'remarks' => 'Revert Beginning Inventory'
                ],
                'details' => []
            ];

            foreach ($transaction->details as $detail) {
                $data['details'][] = [
                    'id' => 0,
                    'product_id' => $detail->product_id,
                    'unit_id' => $detail->unit_id,
                    'unit_qty' => $detail->unit_qty,
                    'qty' => 0,
                    'pc_qty' => 0,
                    'inventory' => $detail->total_qty,
                    'price' => $detail->price,
                    'remarks' => '',
                    'cost' => $detail->cost,
                ];
            }

            $service->store($data);

            $this->line(sprintf('Transaction Sheet Number %s successfully generated a new revert transaction.', $transaction->sheet_number));
        }
    }
}