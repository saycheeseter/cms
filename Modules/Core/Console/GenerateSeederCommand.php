<?php


namespace Modules\Core\Console;

use App;
use File;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Str;
use Exception;

class GenerateSeederCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'seed:make {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new migratable seeder class';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'seed:make {name : The name of the seeder.}
        {--env= : The environment the seeder should be created for.}
        {--path= : The location where the seeder file should be created.}';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        try {
            $this->putSeederInDirectories();
            App::make(Composer::class)->dumpAutoloads();
        } catch (Exception $e) {
            $this->error('Unable to generate seeder file: '.$e->getMessage());
        }
    }

    /**
     * Construct filename with format of timestamp_SeederName.php.
     */
    private function createFilename()
    {
        return sprintf("%s_%s",
            Carbon::now()->format('Y_m_d_His'),
            $this->argument('name').
            '.php'
        );
    }

    /**
     * Fetch Seeder Stub File (Seeder Template).
     */
    private function getSeederTemplate()
    {
        return str_replace('{{SEEDER_NAME}}',
            $this->argument('name'),
            File::get(__DIR__."/stubs/Seeder.stub"));
    }

    /**
     * Save the new seeder file to seeders directories.
     */
    private function putSeederInDirectories()
    {
        $seedTemplate = $this->getSeederTemplate();
        $filename = $this->createFilename();
        $directories = File::directories(base_path()."/database/Seeders");

        foreach ($directories as $directory) {
            File::put($directory.'/'.$filename, $seedTemplate);

            $this->info(sprintf('%s created successfully in %s.', $filename, $directory));
        }
    }
}