<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;

class GeneratePHPUnitBat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dusk:copy-bat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy phpunit.bat from root to vendor folder (For Windows OS)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = base_path('phpunit.bat');
        $dest = base_path('vendor/bin/phpunit.bat');

        if (!File::copy($file, $dest)) {
            $this->error("Couldn't copy file");
            exit;
        }
    }
}