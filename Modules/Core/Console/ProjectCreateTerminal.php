<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\PosSummaryPosting;
use Modules\Core\Entities\Terminal;
use Auth;

class ProjectCreateTerminal extends Command
{
    protected $name = 'project:create-terminal';

    protected $description = 'Create new terminal and posting data.';

    public function handle()
    {
        try {
            $terminal = $this->ask('Terminal Number: ', '1');
            $branch = $this->ask('Branch ID: ', 1);

            Auth::loginUsingId(1);

            Terminal::create([
                'number' => $terminal,
                'branch_id' => $branch
            ]);

            PosSummaryPosting::create([
                'terminal_number' => $terminal,
                'branch_id' => $branch
            ]);

            $this->line('Terminal successfully created.');
        } catch (\Exception $e) {
            $this->error('Something went wrong. Exception Message: '.$e->getMessage());
        }
    }
}