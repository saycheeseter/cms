<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use DB;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\TransactionSummary;
use App;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use Modules\Core\Traits\HasCurrentCostAsMAC;
use Modules\Core\Traits\HasInventoryAndManagementProcess;

class ProjectRecomputeStockCard extends Command
{
    protected $signature = 'project:recompute-stock-card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recompute Stock Card data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $that = $this;

        DB::transaction(function() use ($that) {
            $stockCards = DB::table('product_stock_card')
                ->orderBy('id', 'ASC')
                ->get();

            DB::table('product_stock_card')->delete();

            DB::table('product_branch_summary')->update([
                'current_cost' => 0
            ]);

            DB::table('product_monthly_transactions')->update([
                'stock_delivery_inbound_total_ma_cost'           => 0,
                'stock_return_inbound_total_ma_cost'             => 0,
                'sales_return_inbound_total_ma_cost'             => 0,
                'purchase_return_outbound_total_ma_cost'         => 0,
                'stock_delivery_outbound_total_ma_cost'          => 0,
                'stock_return_outbound_total_ma_cost'            => 0,
                'sales_outbound_total_ma_cost'                   => 0,
                'damage_outbound_total_ma_cost'                  => 0,
                'adjustment_increase_total_ma_cost'              => 0,
                'adjustment_decrease_total_ma_cost'              => 0,
                'product_conversion_increase_total_ma_cost'      => 0,
                'product_conversion_decrease_total_ma_cost'      => 0,
                'auto_product_conversion_increase_total_ma_cost' => 0,
                'auto_product_conversion_decrease_total_ma_cost' => 0,
                'pos_sales_total_ma_cost'                        => 0,
            ]);

            $type = 0;
            $id = 0;
            $last = $stockCards->last();

            foreach ($stockCards as $stockCard) {
                if ($type === 0 && $id === 0) {
                    $type = $stockCard->reference_type;
                    $id = $stockCard->reference_id;
                }

                if ($stockCard->reference_type !== $type || $stockCard->reference_id !== $id) {
                    $this->computeStockCardAndPMT($type, $id);

                    $type = $stockCard->reference_type;
                    $id = $stockCard->reference_id;
                }

                if ($stockCard->id === $last->id) {
                    $this->computeStockCardAndPMT($stockCard->reference_type, $stockCard->reference_id);
                }
            }

            $this->line('Stock Card and PMT successfully recomputed.');
        }, 5);
    }

    private function getClassModelByType($type)
    {
        $class = '';

        switch ($type) {
            case TransactionSummary::PURCHASE_INBOUND:
                $class = PurchaseInbound::class;
                break;

            case TransactionSummary::STOCK_DELIVERY_INBOUND:
                $class = StockDeliveryInbound::class;
                break;

            case TransactionSummary::STOCK_RETURN_INBOUND:
                $class = StockReturnInbound::class;
                break;

            case TransactionSummary::SALES_RETURN_INBOUND:
                $class = SalesReturnInbound::class;
                break;

            case TransactionSummary::PURCHASE_RETURN_OUTBOUND:
                $class = PurchaseReturnOutbound::class;
                break;

            case TransactionSummary::STOCK_DELIVERY_OUTBOUND:
                $class = StockDeliveryOutbound::class;
                break;

            case TransactionSummary::STOCK_RETURN_OUTBOUND:
                $class = StockReturnOutbound::class;
                break;

            case TransactionSummary::SALES_OUTBOUND:
                $class = SalesOutbound::class;
                break;

            case TransactionSummary::DAMAGE_OUTBOUND:
                $class = DamageOutbound::class;
                break;

            case TransactionSummary::INVENTORY_ADJUST:
                $class = InventoryAdjust::class;
                break;

            case TransactionSummary::POS_SALES:
                $class = PosSales::class;
                break;

            case TransactionSummary::PRODUCT_CONVERSION:
                $class = ProductConversion::class;
                break;

            case TransactionSummary::AUTO_PRODUCT_CONVERSION:
                $class = AutoProductConversion::class;
                break;
        }

        return $class;
    }

    private function computeStockCardAndPMT($type, $id)
    {
        $fields = ['total_ma_cost'];

        $transactionMovingAverageCostService = App::make(TransactionMovingAverageCostServiceInterface::class);
        $productStockCardService = App::make(ProductStockCardServiceInterface::class);
        $productMonthlyTransactionService = App::make(ProductMonthlyTransactionServiceInterface::class);

        $class = $this->getClassModelByType($type);

        $model = App::make($class)->find($id);

        $traits = class_uses_recursive($class);

        if (in_array(HasCurrentCostAsMAC::class, $traits)) {
            $model = $transactionMovingAverageCostService->updateTransactionDetails($model);
        }

        if (in_array(HasInventoryAndManagementProcess::class, $traits)) {
            $productStockCardService->create($model);
            $productMonthlyTransactionService->generateSpecificFieldsFromTransaction($model, $fields);
        }

        if ($model instanceof AutoProductConversion) {
            $model = $transactionMovingAverageCostService->updateAutoConversion($model);
            $productStockCardService->createFromProductConversion($model);
            $productMonthlyTransactionService->generateSpecificFieldsFromAutoConversion($model, $fields);
        }

        if ($model instanceof ProductConversion) {
            $model = $transactionMovingAverageCostService->updateManualConversion($model);
            $productStockCardService->createFromProductConversion($model);
            $productMonthlyTransactionService->generateSpecificFieldsFromConversion($model, $fields);
        }

        if ($model instanceof InventoryAdjust) {
            $productStockCardService->create($model);
            $productMonthlyTransactionService->generateSpecificFieldsFromInventoryAdjust($model, $fields);
        }
    }
}