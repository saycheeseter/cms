<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\Admin;
use App;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface;
use Auth;

class ProjectSetAdminLicenseKey extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:set-admin-key';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set License Key for Admin account.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Auth::loginUsingId(1);
        Auth::loginBranchUsingId(1);

        $finished = false;

        $service = App::make(LicenseApiServiceInterface::class);

        while (!$finished) {
            $key = $this->ask('License key for Admin account');

            if (!$service->isValidKey($key)) {
                $this->error('Invalid License Key.');
                continue;
            }

            $admin = Admin::first();

            $admin->license_key = $key;

            $admin->save();

            App::make(LicenseApiServiceInterface::class)->activate($admin);

            $finished = true;

            $this->line('License Key successfully activated for Admin account.');
        }
    }
}
