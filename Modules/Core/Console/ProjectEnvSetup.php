<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectEnvSetup extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:set-env';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set needed .env variables for production.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = $this->ask('Environtment setup: ', 'production');
        $this->call('env:set', ['key' => 'APP_ENV', 'value' => $env]);
        $this->call('env:set', ['key' => 'MIX_APP_ENV', 'value' => $env]);

        if ($env === 'production') {
            $this->call('env:set', ['key' => 'APP_DEBUG', 'value' => false]);
        }

        $connection = $this->ask('Database Connection: ', 'sqlsrv');
        $this->call('env:set', ['key' => 'DB_CONNECTION', 'value' => $connection]);

        $host = $this->ask('Database Host: ', '127.0.0.1');
        $this->call('env:set', ['key' => 'DB_HOST', 'value' => $host]);

        $port = $this->ask('Database Port: ', '3306');
        $this->call('env:set', ['key' => 'DB_PORT', 'value' => $port]);

        $database = $this->ask('Database Name: ');
        $this->call('env:set', ['key' => 'DB_DATABASE', 'value' => $database]);

        $username = $this->ask('Database User: ');
        $this->call('env:set', ['key' => 'DB_USERNAME', 'value' => $username]);

        $password = $this->ask('Database Password: ', false);
        $this->call('env:set', ['key' => 'DB_PASSWORD', 'value' => !$password ? '' : $password]);

        $clientId = $this->ask('Client ID: ');
        $this->call('env:set', ['key' => 'CLIENT_ID', 'value' => $clientId]);

        $oauthClientId = $this->ask('OAuth Client ID: ');
        $this->call('env:set', ['key' => 'OAUTH_CLIENT_ID', 'value' => $oauthClientId]);

        $oauthClientSecret = $this->ask('OAuth Client Secret: ');
        $this->call('env:set', ['key' => 'OAUTH_CLIENT_SECRET', 'value' => $oauthClientSecret]);
    }
}
