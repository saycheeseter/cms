<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Extensions\Log\AttachmentLogger;
use Storage;
use Carbon\Carbon;
use Log;

class CleanAttachmentTempDirectory extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:clean-attachment-temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove floating attachments from the temporary directory.';

    private $logger;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->logger = new AttachmentLogger;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::allFiles('attachments/temporary');
        
        foreach ($files as $file) {
            if($this->hourDifference($file) >= 1) {
                $this->attachmentLog(
                    Storage::disk('public')->delete($file), 
                    $file
                );
            }
        }
    }

    private function hourDifference($file)
    {
        return Carbon::now()->diffInHours(
            Carbon::createFromTimestamp(Storage::lastModified($file))
        );
    }

    private function attachmentLog($removed, $file)
    {   
        $status = 'removed';

        if(!$removed) {
            $status = 'failed';
        }

        $this->logger->addInfo(sprintf('%s [%s]', $file, $status));
    }

}
