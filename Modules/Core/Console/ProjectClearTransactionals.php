<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\ProductBatch;
use Modules\Core\Entities\ProductBatchOwner;
use Modules\Core\Entities\ProductBranchSummary;
use Modules\Core\Entities\ProductCustomer;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Entities\ProductSerialNumber;
use Modules\Core\Entities\ProductSupplier;
use Modules\Core\Entities\ProductTransactionSummary;
use Modules\Core\Entities\ProductStockCard;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\CollectionDetail;
use Modules\Core\Entities\Counter;
use Modules\Core\Entities\CounterDetail;
use Modules\Core\Entities\Damage;
use Modules\Core\Entities\DamageDetail;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\DamageOutboundDetail;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\InventoryAdjustDetail;
use Modules\Core\Entities\Payment;
use Modules\Core\Entities\PaymentDetail;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\ProductConversionDetail;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\PurchaseDetail;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseInboundDetail;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\PurchaseReturnDetail;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\PurchaseReturnOutboundDetail;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\SalesDetail;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesOutboundDetail;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\SalesReturnDetail;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\SalesReturnInboundDetail;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\StockDeliveryDetail;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryInboundDetail;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockDeliveryOutboundDetail;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\StockRequestDetail;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockReturnDetail;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnInboundDetail;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Entities\StockReturnOutboundDetail;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\AutoProductConversionDetail;
use Modules\Core\Entities\IssuedCheck;
use Modules\Core\Entities\ReceivedCheck;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Entities\OtherPaymentDetail;
use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherIncomeDetail;
use Modules\Core\Entities\PaymentReference;
use Modules\Core\Entities\CollectionReference;
use DB;
use App;
use Exception;

class ProjectClearTransactionals extends Command
{
    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'project:clear-transactionals';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Clear transactions and related data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);

        $confirm = $this->confirm('Are you sure you want to clear all the transactions and summary tables?');

        if (!$confirm) {
            return false;
        }

        try {
            DB::table('transaction_batch')->delete();
            DB::table('transaction_serial_number')->delete();

            $transactions = [
                PaymentReference::class,
                CollectionReference::class,
                PaymentDetail::class,
                Payment::class,
                PurchaseInboundDetail::class,
                PurchaseInbound::class,
                PurchaseDetail::class,
                Purchase::class,
                PurchaseReturnOutboundDetail::class,
                PurchaseReturnOutbound::class,
                PurchaseReturnDetail::class,
                PurchaseReturn::class,
                DamageOutboundDetail::class,
                DamageOutbound::class,
                DamageDetail::class,
                Damage::class,
                CollectionDetail::class,
                Collection::class,
                CounterDetail::class,
                Counter::class,
                SalesOutboundDetail::class,
                SalesOutbound::class,
                SalesDetail::class,
                Sales::class,
                SalesReturnInboundDetail::class,
                SalesReturnInbound::class,
                SalesReturnDetail::class,
                SalesReturn::class,
                StockRequestDetail::class,
                StockRequest::class,
                StockDeliveryInboundDetail::class,
                StockDeliveryInbound::class,
                StockDeliveryOutboundDetail::class,
                StockDeliveryOutbound::class,
                StockReturnDetail::class,
                StockDeliveryDetail::class,
                StockDelivery::class,
                StockReturnInboundDetail::class,
                StockReturnInbound::class,
                StockReturnOutboundDetail::class,
                StockReturnOutbound::class,
                StockReturn::class,
                InventoryAdjustDetail::class,
                InventoryAdjust::class,
                ProductConversionDetail::class,
                ProductConversion::class,
                IssuedCheck::class,
                ReceivedCheck::class,
                ProductTransactionSummary::class,
                ProductMonthlyTransactions::class,
                ProductStockCard::class,
                ProductSupplier::class,
                ProductCustomer::class,
                AutoProductConversionDetail::class,
                AutoProductConversion::class,
                OtherPaymentDetail::class,
                OtherPayment::class,
                OtherIncomeDetail::class,
                OtherIncome::class,
                ProductSerialNumber::class,
                ProductBatchOwner::class,
                ProductBatch::class,
            ];

            foreach ($transactions as $key => $transaction) {
                App::make($transaction)->getQuery()->delete();
            }

            ProductBranchSummary::getQuery()->update([
                'reserved_qty' => 0,
                'requested_qty' => 0,
                'qty' => 0,
                'current_cost' => 0
            ]);

            $this->line('Transactions successfully cleared.');
        } catch (Exception $e) {
            $this->error('Something went wrong. Message: '.$e->getMessage());
        }
    }
}