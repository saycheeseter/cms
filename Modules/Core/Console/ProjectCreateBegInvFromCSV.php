<?php

namespace Modules\Core\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Excel;
use Auth;
use App;
use Exception;
use Modules\Core\Entities\Product;
use Modules\Core\Enums\ProductManageType;
use Modules\Core\Services\Contracts\InventoryAdjustServiceInterface;

class ProjectCreateBegInvFromCSV extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:create-beg-inv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Inventory Adjustment based on what branch and csv data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        Auth::loginUsingId(1);
        Auth::loginBranchUsingId(1);

        $path = $this->ask('Indicate the path for the excel/csv file.');
        $branch = $this->ask('Which branch to create?');
        $location = $this->ask('Which location to place?');
        $field = $this->ask('What field does the first column will be based upon searching for the product?');

        $transaction = [
            'info' => [
                'created_for' => $branch,
                'for_location' => $location,
                'requested_by' => Auth::user()->id,
                'transaction_date' => Carbon::now()->toDateTimeString(),
                'remarks' => 'Beginning Inventory'
            ],
            'details' => []
        ];

        Excel::filter('chunk')
            ->load($path)
            ->chunk(500, function($results) use (&$transaction, $field, $branch) {
                $transaction['details'] = [];

                foreach ($results as $index => $row) {
                    if ($row->inv == 0) {
                        continue;
                    }

                    $product = Product::with(['prices' =>
                        function ($query) use ($branch) {
                            $query->where('branch_id', $branch);
                        }
                    ])
                    ->where($field, $row->field)
                    ->where('manage_type', ProductManageType::NONE)
                    ->first();

                    if (!$product) {
                        continue;
                    }

                    $transaction['details'][] = [
                        'id'         => 0,
                        'product_id' => $product->id,
                        'unit_id'    => 1,
                        'unit_qty'   => 1,
                        'qty'        => $row->inv,
                        'pc_qty'     => 0,
                        'inventory'  => 0,
                        'price'      => $product->prices->first()->purchase_price,
                        'cost'       => $product->prices->first()->purchase_price,
                        'remarks'    => '',
                    ];
                }

                try {
                    App::make(InventoryAdjustServiceInterface::class)->store($transaction);

                    $this->line('Inventory Adjustment successfully created.');
                } catch (Exception $e) {
                    $this->error('Error trying to create Inventory Adjustment: '.$e->getMessage());
                }
            }, false);
    }
}