<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\ProductStockCard;
use Modules\Core\Enums\TransactionSummary;
use DB;

class ProjectClearPosTransactions extends Command
{
    protected $name = 'project:clear-pos-transactions';

    protected $description = 'Clear created POS Transactions and its summary data.';

    public function handle()
    {
        $continue = $this->confirm('Are you sure you want to clear all the POS Transactions?');

        if (!$continue) {
            return;
        }

        $that = $this;

        // Auto group condition still not implemented. Will later support this
        // Also add later support for recomputing stock card when POS Stock Card is deleted
        DB::transaction(function() use ($that) {
            $cards = ProductStockCard::where('reference_type', TransactionSummary::POS_SALES)->cursor();

            foreach ($cards as $card) {
                $that->updateInventoryFromStockCard($card);
            }

            $that->clearPOSColumnInProductTransactionSummary();
            $that->clearPOSColumnInProductMonthlyTransactions();
            $that->clearPOSStockCardData();
            $that->clearPOSData();
            $that->resetTerminalPosting();
            $this->line('POS Data successfully cleared.');
        }, 5);
    }

    private function updateInventoryFromStockCard(ProductStockCard $model)
    {
        DB::table('product_branch_summary')
            ->where('product_id', $model->product_id)
            ->where('branch_id', $model->created_for)
            ->where('branch_detail_id', $model->for_location)
            ->update([
                'qty' => \DB::raw('qty - '.$model->qty->innerValue())
            ]);
    }

    private function clearPOSColumnInProductTransactionSummary()
    {
        DB::table('product_transaction_summary')
            ->update([
                'pos_sales' => 0
            ]);
    }

    private function clearPOSColumnInProductMonthlyTransactions()
    {
        DB::table('product_monthly_transactions')
            ->update([
                'pos_sales_total_qty'     => 0,
                'pos_sales_total_amount'  => 0,
                'pos_sales_total_ma_cost' => 0,
                'pos_sales_total_cost'    => 0,
            ]);
    }

    private function clearPOSStockCardData()
    {
        DB::table('product_stock_card')
            ->where('reference_type', TransactionSummary::POS_SALES)
            ->delete();
    }

    private function clearPOSData()
    {
        DB::table('pos_card_payment')->delete();
        DB::table('pos_collection')->delete();
        DB::table('pos_detail_components')->delete();
        DB::table('pos_sales_detail')->delete();
        DB::table('pos_sales')->delete();
    }

    private function resetTerminalPosting()
    {
        DB::table('pos_summary_postings')->update(['last_post_date' => null]);
    }
}