<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use DB;
use Modules\Core\Entities\TransactionSerialNumber;
use Modules\Core\Entities\TransactionSerialNumberLogs;
use Modules\Core\Enums\ApprovalStatus;
use App;
use Illuminate\Database\Eloquent\Relations\Relation;

class ProjectRecomputeSerialLogs extends Command
{
    protected $signature = 'project:recompute-serial-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recompute transaction_serial_number_logs table based from transaction_serial_number table.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function() {
            TransactionSerialNumberLogs::getQuery()->delete();

            $itemSerials = TransactionSerialNumber::with(['seriable.transaction'])
                ->orderBy('id', 'ASC')
                ->cursor();

            foreach ($itemSerials as $itemSerial) {
                if (!$itemSerial->seriable) {
                    continue;
                }

                $transaction = $itemSerial->seriable->transaction;

                if ($transaction->approval_status !== ApprovalStatus::APPROVED) {
                    continue;
                }

                TransactionSerialNumberLogs::insert([
                    'serial_id' => $itemSerial->serial_id,
                    'transaction_id' => $transaction->id,
                    'transaction_type' => array_search(get_class($transaction), Relation::morphMap()),
                    'approval_date' => $transaction->audited_date
                ]);

                $this->line('Transaction '.$transaction->sheet_number.' successfully logged.');
            }
        });
    }
}