<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;

class InitializeProject extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute needed command upon fresh installation of the project.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->assertEnvExist();

        $this->call('key:generate');
        $this->call('vendor:publish', [
            '--provider' => 'Nwidart\Modules\LaravelModulesServiceProvider'
        ]);
        $this->call('config:clear');
        $this->info('Command successfully executed.');
    }

    private function assertEnvExist()
    {
        if (!file_exists(base_path().'/.env')) {
            $this->error('Please set a .env file.');
            exit;
        }
    }
}
