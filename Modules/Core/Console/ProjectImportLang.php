<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use File;
use Lang;

class ProjectImportLang extends Command
{
    protected $signature = 'project:import-lang {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Language file.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $path = $this->argument('path');

        if(!file_exists($path)){
            $this->line('File does not exists, please try again');
            return false;
        }

        if(!in_array(File::extension($path), ['xls', 'xlsx'])) {
            $this->line('Incorrect file format, please try again');
            return false;
        }

        $localization = $this->choice('Enter Localization to be updated', ['en', 'zh'], 0);

        $buildData = $this->loadDataFromExcel($path);
        $langFiles = $this->getAllLangFiles();
        $langArray = [];

        foreach($langFiles as $filename){
            $langArray[$filename] = Lang::get('core::'.$filename, [], $localization);
        }
        
        $langArray = $this->replaceLang($langArray, $buildData);

        foreach($langArray as $fileName => $data) {
            $path = base_path('Modules\\Core\\Resources\\lang\\'.$localization.'\\'.$fileName.'.php');
            
            unlink($path);
            
            $write = "<?php\n\nreturn [\n";

            foreach($data as $key => $value) {
                $write = $write.'    \''.$key.'\' => \''.addslashes($value).'\','.PHP_EOL;
            }

            $write = $write.'];';

            File::put($path, $write);
        }

        $this->line($localization.' succesfully imported.');
    }

    private function loadDataFromExcel($path) 
    {
        $buildData = [];

        $data = Excel::load($path, function($reader) use (&$buildData) {
            foreach($reader->all() as $index => $sheet) {
                $currentSheet = $sheet->getTitle();
                $buildData[$currentSheet] = [];

                foreach($sheet as $sheetIndex => $cellCollection) {
                    $buildData[$currentSheet][$cellCollection->key] = $cellCollection->value;
                }
            }
        });

        return $buildData;
    }

    private function getAllLangFiles()
    {
        $filenames = [];
        $filesInFolder = File::files(base_path('Modules\Core\Resources\lang\en'));

        foreach($filesInFolder as $path) {
            $filenames[] = pathinfo($path)['filename'];
        }

        return $filenames;
    }

    private function replaceLang($original, $changes)
    {
        foreach($changes as $langFile => $rows) {
            foreach($rows as $rowKey => $row) {
                if(array_key_exists($rowKey, $original[$langFile])) {
                    $original[$langFile][$rowKey] = $row;
                }
            }
        }

        return $original;
    }
}