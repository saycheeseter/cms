<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use DB;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Enums\TransactionSummary;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;
use App;
use Auth;

class ProjectZeroTransactionPosted extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:clear-trans-post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all summary data related to manual posting and populate all existing transactions for manual posting.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Auth::loginUsingId(1);
        Auth::loginBranchUsingId(1);

        $pendings = TransactionSummaryPosting::all();

        if ($pendings->count() > 0) {
            $this->error('Cannot clear posted transactions. Please post the remaining pending transactions.');
            return;
        }

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $that = $this;

        DB::transaction(function() use ($that) {
            $stockCards = DB::table('product_stock_card')
                ->orderBy('id', 'ASC')
                ->get();

            DB::table('product_stock_card')->delete();

            DB::table('product_branch_summary')->update([
                'current_cost' => 0
            ]);

            DB::table('product_monthly_transactions')->delete();
            DB::table('product_transaction_summary')->delete();
            DB::table('product_customer')->delete();
            DB::table('product_supplier')->delete();

            $type = 0;
            $id = 0;
            $last = $stockCards->last();

            foreach ($stockCards as $stockCard) {
                if ($type === 0 && $id === 0) {
                    $type = $stockCard->reference_type;
                    $id = $stockCard->reference_id;
                }

                if ($stockCard->reference_type !== $type || $stockCard->reference_id !== $id) {
                    $that->createPostingLog($type, $id);

                    $type = $stockCard->reference_type;
                    $id = $stockCard->reference_id;
                }

                if ($stockCard->id === $last->id) {
                    $that->createPostingLog($type, $id);
                }
            }

            DB::table('generic_settings')
                ->where('key', 'transaction.summary.computation.last.post.date')
                ->update(['value' => '2000-01-01']);

            $this->line('Summary data cleared and transaction for posting has been successfully populated.');
        });
    }


    private function getClassModelByType($type)
    {
        $class = '';

        switch ($type) {
            case TransactionSummary::PURCHASE_INBOUND:
                $class = PurchaseInbound::class;
                break;

            case TransactionSummary::STOCK_DELIVERY_INBOUND:
                $class = StockDeliveryInbound::class;
                break;

            case TransactionSummary::STOCK_RETURN_INBOUND:
                $class = StockReturnInbound::class;
                break;

            case TransactionSummary::SALES_RETURN_INBOUND:
                $class = SalesReturnInbound::class;
                break;

            case TransactionSummary::PURCHASE_RETURN_OUTBOUND:
                $class = PurchaseReturnOutbound::class;
                break;

            case TransactionSummary::STOCK_DELIVERY_OUTBOUND:
                $class = StockDeliveryOutbound::class;
                break;

            case TransactionSummary::STOCK_RETURN_OUTBOUND:
                $class = StockReturnOutbound::class;
                break;

            case TransactionSummary::SALES_OUTBOUND:
                $class = SalesOutbound::class;
                break;

            case TransactionSummary::DAMAGE_OUTBOUND:
                $class = DamageOutbound::class;
                break;

            case TransactionSummary::INVENTORY_ADJUST:
                $class = InventoryAdjust::class;
                break;

            case TransactionSummary::POS_SALES:
                $class = PosSales::class;
                break;

            case TransactionSummary::PRODUCT_CONVERSION:
                $class = ProductConversion::class;
                break;

            case TransactionSummary::AUTO_PRODUCT_CONVERSION:
                $class = AutoProductConversion::class;
                break;
        }

        return $class;
    }

    private function createPostingLog($type, $id)
    {
        $class = $this->getClassModelByType($type);

        $model = App::make($class)->find($id);

        App::make(TransactionSummaryPostingServiceInterface::class)->insert($model);
    }
}
