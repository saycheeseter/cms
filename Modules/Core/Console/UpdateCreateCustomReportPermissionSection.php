<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Modules\Core\Entities\PermissionSection;

class UpdateCreateCustomReportPermissionSection extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:update-create-custom-report-permission-section';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates custom report permission section or updates custom report permission section sequence number';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        //insert or get custom report section instance
        $customReportSection =  PermissionSection::firstOrCreate(
            ['alias' => 'custom_report'], 
            [
                'name' => 'Custom Report',
                'alias' => 'custom_report',
                'translation' => 'label.custom.report',
                'sequence_number' => 1
            ]
        );
        
        //get last section based on sequence number
        $lastSectionSeqNo = PermissionSection::orderBy('sequence_number', 'desc')
            ->first()
            ->sequence_number;
        
        //update sequence number of custom report 
        $customReportSection->sequence_number = $lastSectionSeqNo + 1;
        $customReportSection->save();
    }
}
