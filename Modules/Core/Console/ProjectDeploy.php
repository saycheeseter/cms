<?php

namespace Modules\Core\Console;

use Exception;
use Illuminate\Console\Command;

class ProjectDeploy extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run required commands for automated deployment.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        try {
            $this->call('down');
            $this->call('clear-compiled');
            $this->call('view:clear');
            $this->call('config:clear');
            $this->call('route:clear');
            $this->call('cache:clear');
            $this->call('module:migrate', ['--force' => true]);
            $this->call('migrate', ['--force' => true]);
            $this->call('seed', ['--force' => true]);
            $this->call('project:update-create-custom-report-permission-section');
            $this->call('project:dump-udf');
            $this->call('queue:restart');
            $this->call('route:cache');
            $this->call('config:cache');
            $this->call('optimize');
            $this->call('env:sync', ['--no-interaction' => true]);
            $this->call('up');
            return 0;
        } catch (Exception $e) {
            $this->error('Error encountered while deploying: '.$e->getMessage());
            return 1;
        }
    }
}
