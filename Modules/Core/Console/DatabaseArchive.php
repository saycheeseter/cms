<?php

namespace Modules\Core\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use App;
use Modules\Core\Entities\UserDefinedField;
use Modules\Core\Services\Contracts\UserDefinedFieldMigrationServiceInterface;
use Exception;
use Schema;

class DatabaseArchive extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:database-archive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archive current database by year and clear all inventory transactions.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '2G');
        ini_set('max_execution_time', 0);

        $continue = $this->confirm('Are you sure you want to archive the current database? This will backup the tables and wipe all the tables listed in the config file.');

        if (!$continue) {
            $this->line('Archive processed cancelled.');
            return false;
        }

        $start = Carbon::now();

        $this->createAndSetArchiveSchema();
        $this->runMigrations();
        $this->initializeJsonFile();

        try {
            $this->log('Setting system to maintenance mode.');
            $this->call('down');
            $this->transaction('archive');
            $this->transaction('production');
            $this->runUDFMigrations();
            $this->copyDataFromSourceToArchive();
            $this->cleanSourceData();
            $this->onSuccessfulArchive();
            $this->updateJsonFile();
            $this->commit('production');
            $this->commit('archive');
            $this->log('Finished archiving the database!');
            $this->duration($start);
            $this->call('up');
        } catch (Exception $e) {
            $this->error(sprintf("ERROR. Archiving Failed! %s", $e->getMessage()));
            $this->duration($start);
            $this->rollback('archive');
            $this->rollback('production');
            $this->call('up');
        }
    }

    /**
     * Create a new schema with a name based on the config
     * Ask for database username and password which needs to have a permission to create schema
     */
    private function createAndSetArchiveSchema()
    {
        $driver = config('database.connections.production.driver');

        $username = $this->ask('Database username');
        $password = $this->secret('Database password');

        // Ask for db creator username and password for more security
        config([
            'database.connections.archive.username' => $username,
            'database.connections.archive.password' => $password,
            'database.connections.production.username' => $username,
            'database.connections.production.password' => $password,
        ]);

        $schema = sprintf("%s_%s", config('archive.database_prefix'), Carbon::now()->year);

        $this->log(sprintf("Creating %s database schema.", $schema));

        switch ($driver) {
            case 'sqlsrv':
                DB::unprepared("IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = '$schema') CREATE DATABASE $schema");
                break;

            case 'mysql':
                DB::unprepared("CREATE DATABASE IF NOT EXISTS $schema");
                break;
        }

        config(['database.connections.archive.database' => $schema]);

        $this->log(sprintf("Successfully created %s database schema.", $schema));
    }

    /**
     * Run database migrations to the archive schema
     */
    private function runMigrations()
    {
        $this->log('Migrating new schema structure.');

        config(['database.default' => 'archive']);

        $this->call('module:migrate', ['--force' => true]);
        $this->call('migrate', ['--force' => true]);
    }

    /**
     * Run auto generated columns of udf module to the archive database
     */
    private function runUDFMigrations()
    {
        config(['database.default' => 'production']);

        $sourceUDFs = UserDefinedField::all();

        config(['database.default' => 'archive']);

        $archiveUDFs = UserDefinedField::all();

        // Check whether the udfs of the archive schema doesn't exists in
        // the production schema, and if not, drop those udf columns
        if (($archiveDiff = $archiveUDFs->diff($sourceUDFs))->count() > 0) {
            $this->dropExistingUDFMigrations($archiveDiff);
        }

        // Check whether the udfs of the product schema doesn't exists in
        // the archive schema, and if not, migrate those udf columns
        if (($sourceDiff = $sourceUDFs->diff($archiveUDFs))->count() > 0) {
            $this->createUDFMigrations($sourceDiff);
        }
    }

    /**
     * Execute drop column migration based on the specified udfs
     *
     * @param $udfs
     */
    private function dropExistingUDFMigrations($udfs)
    {
        $this->executeUDFMigration($udfs, 'dropColumn');
    }

    /**
     * Execute column migration based on the specified udfs
     *
     * @param $udfs
     */
    private function createUDFMigrations($udfs)
    {
        $this->executeUDFMigration($udfs, 'migrate');
    }

    /**
     * Migrate or drop a column based on the udfs
     *
     * @param $udfs
     * @param $method
     */
    private function executeUDFMigration($udfs, $method)
    {
        $service = App::make(UserDefinedFieldMigrationServiceInterface::class);

        foreach ($udfs as $udf) {
            $message = $method === 'migrate'
                ? sprintf("Migrating: %s column to %s.", $udf->alias, $udf->table)
                : sprintf("Dropping: %s column in %s.", $udf->alias, $udf->table);

            $this->log($message);

            $service->$method($udf);

            $message = $method === 'migrate'
                ? sprintf("Migrated: %s column to %s.", $udf->alias, $udf->table)
                : sprintf("Dropped: %s column in %s.", $udf->alias, $udf->table);

            $this->log($message);
        }
    }

    /**
     * Initialize default values of json archive which will be used in the
     * archive process
     */
    private function initializeJsonFile()
    {
        if (is_null(archive())) {
            archive($archive = [
                'last_update' => null
            ]);
        }
    }

    /**
     * Copy the data of the production schema to the archive schema
     */
    private function copyDataFromSourceToArchive()
    {
        $this->log('Beginning to copy data from the source to archive schema.');

        // Do a delete - insert process
        $this->cleanArchiveTables();
        $this->copyToArchive($this->getTableList());
    }

    /**
     * Delete all the data of tables in the archive schema by disabling the foreign key
     * constraint
     */
    private function cleanArchiveTables()
    {
        config(['database.default' => 'archive']);

        // Exclude the tables that are listed in the clean list of the config
        $tables = array_diff($this->getTableList(), config('archive.clean'));

        foreach ($tables as $table) {
            $this->deleteArchiveTableWithDisabledForeignConstraint($table);
        }
    }

    /**
     * Execute a delete statement
     * Foreign key constraint is disabled before executing the delete statement
     * and afterwhich, enable it again
     *
     * @param $table
     */
    private function deleteArchiveTableWithDisabledForeignConstraint($table)
    {
        $statement = '';

        switch (config('database.connections.archive.driver')) {
            case 'mysql':
                $statement = sprintf('DELETE FROM %s;', $table);
                break;

            case 'sqlsrv':
                $statement = sprintf('DELETE FROM [%s];', $table);
                break;
        }

        $statement = sprintf("%s%s%s",
            $this->disableArchiveForeignKeyConstraint(),
            $statement,
            $this->enableArchiveForeignKeyConstraint()
        );

        DB::unprepared($statement);
    }

    /**
     * Return the disable foreign key constraint statement
     *
     * @return string
     */
    private function disableArchiveForeignKeyConstraint()
    {
        config(['database.default' => 'archive']);

        switch (config('database.connections.archive.driver')) {
            case 'mysql':
                return "SET FOREIGN_KEY_CHECKS=0;";
                break;

            case 'sqlsrv':
                $statement = '';

                foreach ($this->getTableList() as $table) {
                    $statement .= sprintf("ALTER TABLE [%s] NOCHECK CONSTRAINT ALL;", $table);
                }

                return $statement;
                break;
        }
    }

    /**
     * Return the enable foreign key constraint statement
     *
     * @return string
     */
    private function enableArchiveForeignKeyConstraint()
    {
        config(['database.default' => 'archive']);

        switch (config('database.connections.archive.driver')) {
            case 'mysql':
                return "SET FOREIGN_KEY_CHECKS=1;";
                break;

            case 'sqlsrv':
                $statement = '';

                foreach ($this->getTableList() as $table) {
                    $statement .= sprintf("ALTER TABLE [%s] CHECK CONSTRAINT ALL;", $table);
                }

                return $statement;
                break;
        }
    }

    /**
     * Copies the data by batch from production to archive schema
     *
     * @param $tables
     */
    private function copyToArchive($tables)
    {
        foreach ($tables as $table) {
            $skip = 0;
            $finished = false;

            do {
                $data = $this->getTableDataFromSource($table, $skip);

                if (count($data) === 0) {
                    $finished = true;
                } else {
                    $this->copy($table, $data);
                }

                unset($data);
            } while (! $finished);

            $this->log(sprintf('Finished copying data of table %s to archive.', $table));
        }
    }

    /**
     * Get the data by batch of the specified table in the production schema
     *
     * @param $table
     * @param $skip
     * @return array
     */
    private function getTableDataFromSource($table, &$skip)
    {
        config(['database.default' => 'production']);

        $batch = config('archive.batch');

        $data = object_to_array(DB::table($table)->skip($skip)->take($batch)->get());

        $skip += $batch;

        return $data;
    }

    /**
     * Insert the specified data and table in the archive schema
     *
     * @param $table
     * @param $data
     */
    private function copy($table, $data)
    {
        config(['database.default' => 'archive']);

        $fields = array_diff(array_keys($data[0]), ['row_num']);

        $query = $this->buildInsertStatement($table, $fields, $data);

        // Append disable/enable foreign key constraint to prevent
        // sql error for foreign keys
        $query = sprintf("%s%s%s",
            $this->disableArchiveForeignKeyConstraint(),
            $query,
            $this->enableArchiveForeignKeyConstraint()
        );

        // Add turn off/on auto increment statement
        // if the specified table has a autoincrement column
        if ($this->isTableHasAutoIncrement($table)) {
            $query = sprintf("%s%s%s",
                $this->turnOffAutoIncrement($table),
                $query,
                $this->turnOnAutoIncrement($table)
            );
        }

        DB::unprepared($query);

        unset($bindings);
        unset($query);
    }

    /**
     * Return the turn off auto increment statement
     *
     * @param $table
     * @return string
     */
    private function turnOffAutoIncrement($table)
    {
        switch (config('database.connections.archive.driver')) {
            case 'sqlsrv':
                return "SET IDENTITY_INSERT [$table] ON;";
                break;
        }
    }

    /**
     * Return the turn off auto increment statement
     *
     * @param $table
     * @return string
     */
    private function turnOnAutoIncrement($table)
    {
        switch (config('database.connections.archive.driver')) {
            case 'sqlsrv':
                return "SET IDENTITY_INSERT [$table] OFF;";
                break;
        }
    }

    /**
     * Build the insert statement by the specified table name, fields and
     * data
     *
     * @param $table
     * @param $fields
     * @param $data
     * @return string
     */
    private function buildInsertStatement($table, $fields, $data)
    {
        $values = '';

        $pdo = DB::connection()->getPDO();

        foreach ($data as $key => $record) {
            $values .= ',(';
            $temp  = '';

            foreach ($record as $column => $val) {
                if ($column === 'row_num') {
                    continue;
                }

                if (is_string($val)) {
                    $val = $pdo->quote($val);
                } else if (is_null($val)) {
                    $val = "null";
                }

                $temp .= ",$val";
            }

            $values .= substr($temp, 1).')';
        }

        switch (config('database.connections.archive.driver')) {
            case 'sqlsrv':
                return sprintf("INSERT INTO [%s] (%s) VALUES%s",
                    $table,
                    "[".implode('],[', $fields)."]",
                    substr($values, 1)
                );
                break;
        }
    }

    /**
     * Check whether the specified table has an autoincrement column
     *
     * @param $table
     * @return bool
     */
    private function isTableHasAutoIncrement($table)
    {
        $driver = config('database.connections.archive.driver');

        switch ($driver) {
            case 'sqlsrv':
                $statement = sprintf("
                  SELECT is_identity 
                  FROM 
                    sys.columns 
                  WHERE 
                    object_id = object_id('%s') 
                    AND is_identity = 1
                ", $table);
                break;

            case 'mysql':
                $statement = sprintf("
                    SELECT *
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE TABLE_NAME = '%s'
                        AND TABLE_SCHEMA = '%s'
                        AND EXTRA like '%%auto_increment%%'
                ", $table, config('database.connections.archive.database'));
                break;
        }

        return count(DB::select($statement)) > 0;
    }

    /**
     * Execute the delete statement on the tables in the production schema
     * that are specified in the config file
     */
    private function cleanSourceData()
    {
        config(['database.default' => 'production']);

        $tables = config('archive.clean');

        $this->log('Starting to clean the tables listed in config.');

        foreach ($tables as $table) {
            DB::table($table)->delete();

            $this->log(sprintf("Successfully cleaned the data in table %s.", $table));
        }

        $this->log('Successfully cleaned all the tables.');
    }

    /**
     * Get the list of tables excluding the ones in the archive ignore config
     *
     * @return array
     */
    private function getTableList()
    {
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();

        return array_diff($tables, config('archive.ignore'));
    }

    /**
     * Update the values of the archive json file
     */
    private function updateJsonFile()
    {
        $archive = archive();

        $archive['last_update'] = Carbon::now()->toDateTimeString();

        archive($archive);
    }

    /**
     * Execute begin transaction on the specified connection
     *
     * @param $connection
     */
    private function transaction($connection)
    {
        DB::connection($connection)->beginTransaction();
    }

    /**
     * Execute commit transaction on the specified connection
     *
     * @param $connection
     */
    private function commit($connection)
    {
        config(['database.default' => $connection]);

        DB::commit();
    }

    /**
     * Execute rollback transaction on the specified connection
     *
     * @param $connection
     */
    private function rollback($connection)
    {
        config(['database.default' => $connection]);

        DB::rollback();
    }

    /**
     * Print a message with a datetime
     * @param $message
     */
    private function log($message)
    {
        $this->line(sprintf("[%s] %s", Carbon::now()->toDateTimeString(), $message));
    }

    /**
     * Callback after all archive commands/statements have been successfully
     * executed. If there's a scenario where you need to rollback if a code in this
     * callback fails, just throw an exception.
     */
    private function onSuccessfulArchive()
    {

    }

    /**
     * Print total duration based on the provided start value and
     * display it in mins / hrs
     * @param $start
     */
    private function duration(Carbon $start)
    {
        $this->line(sprintf("Duration: %s min(s) | %s hr(s)",
            Carbon::now()->diffInMinutes($start),
            Carbon::now()->diffInHours($start)
        ));
    }
}
