<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Lang;
use File;

class ProjectExportLang extends Command
{
    protected $signature = 'project:export-lang {directory?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Language file.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $directory = !is_null($this->argument('directory'))
            ? $this->argument('directory')
            : base_path('storage\\exports\\');

        $localization = $this->choice('Enter Localization', ['en', 'zh'], 0);

        $langFiles = $this->getAllLangFiles();
        $langArray = [];

        foreach($langFiles as $filename) {
            $langArray[$filename] = $this->transformLangToArrayForExcelFormat(Lang::get('core::'.$filename, [], $localization));
        }

        Excel::create($localization, function($excel) use ($langArray, $localization) {
            $excel->setTitle($localization);

            foreach($langArray as $key => $value) {
                $excel->sheet($key, function($sheet) use ($value) {
                    $sheet->fromArray($value, null, 'A1', false, false);
                });
            }
        })->store('xlsx', $directory);

        $this->line('Exported '.$localization.'.xlsx to '.$directory);
    }

    private function getAllLangFiles()
    {
        $filenames = [];
        $filesInFolder = File::files(base_path('Modules\Core\Resources\lang\en'));

        foreach($filesInFolder as $path) {
            $filenames[] = pathinfo($path)['filename'];
        }

        return $filenames;
    }

    private function transformLangToArrayForExcelFormat($array) 
    {
        $tempArray = [['key', 'value']];

        foreach($array as $key => $value) {
            $tempArray[] = [$key, $value];
        }

        return $tempArray;
    }   
}