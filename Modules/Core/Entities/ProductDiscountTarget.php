<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\ProductDiscountTargetPresenter;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class ProductDiscountTarget extends Base implements AuditableInterface
{   
    use Auditable;
    
    protected $table = 'product_discount_target';

    public $timestamps = false;

    protected $fillable = [
        'product_discount_id',
        'target_id',
        'target_type'
    ];

    public function discount()
    {
        return $this->belongsTo(ProductDiscount::class, 'product_discount_id', 'id')->withTrashed();
    }

    public function target()
    {
        return $this->morphTo();
    }

    public function presenter()
    {
        return new ProductDiscountTargetPresenter($this);
    }

    public function generateTags(): array
    {
        return [
            sprintf("Product Discount Code: %s", $this->discount->code),
        ];
    }
}
