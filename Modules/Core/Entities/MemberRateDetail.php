<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberRateDetail extends DataModule 
{
    use SoftDeletes;

    protected $table = 'member_rate_detail';

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'member_rate_head_id' => 'int',
        'discount' => 'decimal',
        'amount_from' => 'decimal',
        'amount_to' => 'decimal',
        'amount_per_point' => 'decimal',
        'date_from' => 'datetime',
        'date_to' => 'datetime'
    ];

    public function head()
    {
        return $this->belongsTo(MemberRate::class, 'member_rate_head_id')->withTrashed();
    }

    public function setDateFromAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['date_from'] = Carbon::createFromFormat('Y-m-d', $value)
                ->startOfDay()
                ->equalTo(Carbon::today())
                    ? Carbon::now()
                    : Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        } else {
            $this->attributes['date_from'] = $value;
        }
    }

    public function setDateToAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['date_to'] = Carbon::createFromFormat('Y-m-d', $value)->modify('23:59:59.999');
        } else {
            $this->attributes['date_to'] = $value;
        }
    }

    public function generateTags(): array
    {
        return [
            sprintf("Head: %s", $this->head->name),
        ];
    }
}