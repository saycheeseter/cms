<?php

namespace Modules\Core\Entities;

class DamageOutboundDetail extends InventoryChainDetail
{
    protected $table = 'damage_outbound_detail';

    public function transaction()
    {
        return $this->belongsTo(DamageOutbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(DamageDetail::class, 'reference_id')->withTrashed();
    }
}
