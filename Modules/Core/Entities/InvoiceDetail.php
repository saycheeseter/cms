<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\ComputedDetailAttributes;
use Modules\Core\Presenters\InvoiceDetailPresenter;
use Modules\Core\Traits\HasRemainingQtyAttribute;
use Modules\Core\Traits\HasTransactionTotalAmountComputation;

abstract class InvoiceDetail extends InventoryFlowDetail
{
    use ComputedDetailAttributes,
        HasRemainingQtyAttribute,
        HasTransactionTotalAmountComputation;

    protected $attributes = [
        'remaining_qty' => 0
    ];

    protected $guarded = [
        'id',
        'remaining_qty'
    ];

    protected $casts = [
        'transaction_id' => 'int',
        'unit_id' => 'int',
        'unit_qty' => 'decimal',
        'qty' => 'decimal',
        'oprice' => 'decimal',
        'price' => 'decimal',
        'discount1' => 'decimal',
        'discount2' => 'decimal',
        'discount3' => 'decimal',
        'discount4' => 'decimal',
        'remaining_qty' => 'decimal',
        'product_id' => 'int',
        'remarks' => 'string',
        'ma_cost' => 'decimal',
        'cost'    => 'decimal'
    ];

    public function presenter()
    {
        return new InvoiceDetailPresenter($this);
    }

    public function getTotalQtyAttribute()
    {
        return $this->qty->mul($this->unit_qty, setting('monetary.precision'));
    }

    public function getAmountAttribute()
    {
        return $this->price->mul($this->total_qty, setting('monetary.precision'));
    }
}
