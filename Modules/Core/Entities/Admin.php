<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Enums\UserType;

class Admin extends User
{
    protected $attributes = [
        'type' => UserType::ADMIN
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('type', UserType::ADMIN);
        });
    }
}