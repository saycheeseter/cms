<?php

namespace Modules\Core\Entities;

use Modules\Core\Extensions\Relations\MorphPivot;

class TransactionComponent extends MorphPivot
{
    protected $casts = [
        'qty' => 'decimal',
        'group_type' => 'int'
    ];
}