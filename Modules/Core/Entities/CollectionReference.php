<?php 

namespace Modules\Core\Entities;

class CollectionReference extends FinancialFlowReference 
{
    protected $table = 'collection_reference';

    public function transaction()
    {
        return $this->belongsTo(Collection::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->morphTo();
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'collectible' => 'decimal'
        ]);
    }
}