<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;

class Reason extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::REASON
    ];

    protected static function type()
    {
        return SystemCodeType::REASON;
    }
}
