<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\AuditableToProduct;

class ProductBranchDetail extends DataModule
{
    use AuditableToProduct;

    protected $table = 'product_branch_detail';

    public $timestamps = false;

    protected $touches = ['product'];

    protected $guarded = [
        'product_id',
    ];

    protected $casts = [
        'min' => 'decimal',
        'max' => 'decimal',
        'branch_detail_id' => 'int',
        'product_id' => 'int'
    ];

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'branch_detail_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relationships()
    {
        return [
            'location' => [
                'class' => BranchDetail::class,
                'relation' => 'location'
            ]
        ];
    }

    public function generateTags(): array
    {
        return [
            'Product Branch Detail',
        ];
    }
}
