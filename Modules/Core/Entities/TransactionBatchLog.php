<?php

namespace Modules\Core\Entities;

class TransactionBatchLog extends Base
{
    public function transaction()
    {
        return $this->morphTo();
    }

    public function batch()
    {
        return $this->belongsTo(ProductBatch::class, 'batch_id');
    }
}