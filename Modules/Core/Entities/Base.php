<?php 

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\HasRelationships;
use Modules\Core\Traits\FormatsNumber;
use Modules\Core\Traits\HasAttributes;
use Modules\Core\Traits\HasUDF;
use Modules\Core\Traits\HasMorphMap;

class Base extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.u';

    use HasRelationships, 
        FormatsNumber,
        HasAttributes,
        HasUDF,
        HasMorphMap;

    public function getExcludedColumnsAttribute()
    {
        return [
            'id',
            'created_from',
            'created_by',
            'created_for',
            'deleted_at',
            'created_at',
            'updated_at',
            'supplier_id',
            'deliver_to',
            'for_location',
            'transaction_id',
            'payment_method',
            'udfs',
            'modified_by',
            'audited_by',
            'requested_by',
            'reference_id',
            'customer_id',
            'customer_detail_id',
            'salesman_id',
            'deliver_to',
            'deliver_to_location',
            'delivery_from_location',
            'delivery_from',
            'historical_change_revert',
            'historical_default_price',
            'product_type'
        ];
    }
}