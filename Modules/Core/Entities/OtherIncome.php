<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\OtherPaymentCheckManagement;

class OtherIncome extends OtherFinancialFlow implements OtherPaymentCheckManagement
{
    protected $table = 'other_income';
    
    public function details()
    {
        return $this->hasMany(OtherIncomeDetail::class, 'transaction_id');
    }

    public function getCheckTransactionableTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(self::class);
    }

    public function check()
    {
        return $this->morphOne(ReceivedCheck::class, 'transactionable');
    }
}
