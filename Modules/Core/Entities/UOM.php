<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;

class UOM extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::UOM
    ];

    protected static function type()
    {
        return SystemCodeType::UOM;
    }
}
