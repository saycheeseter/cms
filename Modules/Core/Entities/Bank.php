<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Extensions\Database\Eloquent\Builder;

class Bank extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::BANK
    ];

    protected static function type()
    {
        return SystemCodeType::BANK;
    }
}
