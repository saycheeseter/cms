<?php

namespace Modules\Core\Entities;

class OtherPaymentDetail extends OtherFinancialFlowDetail
{
    protected $table = 'other_payment_detail';
    
    public function transaction()
    {
        return $this->belongsTo(OtherPayment::class, 'transaction_id', 'id')->withTrashed();
    }

    public function type()
    {
        return $this->belongsTo(IncomeType::class, 'type')->withTrashed();
    }
}
