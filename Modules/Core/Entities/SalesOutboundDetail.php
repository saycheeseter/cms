<?php

namespace Modules\Core\Entities;

class SalesOutboundDetail extends InventoryChainDetail
{
    protected $table = 'sales_outbound_detail';

    public function transaction()
    {
        return $this->belongsTo(SalesOutbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(SalesDetail::class, 'reference_id')->withTrashed();
    }

    public function autoConversion()
    {
        return $this->morphOne(AutoProductConversion::class, 'transaction');
    }
}
