<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\TransactionReference;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

use Modules\Core\Entities\Contracts\BranchModuleSeries;
use Modules\Core\Traits\HasBranchModuleSeries;
use Modules\Core\Traits\HasTransactionSheetNumber;

class Counter extends Base implements
    TransactionReference,
    AuditableInterface,
    BranchModuleSeries
{
    use SoftDeletes,
        Auditable,
        HasBranchModuleSeries,
        HasTransactionSheetNumber,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'counter';

    protected $attributes = [
        'total_amount' => 0
    ];

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
    ];

    protected $casts = [
        'created_for' => 'int',
        'customer_id' => 'int',
        'transaction_date' => 'datetime',
        'sheet_number' => 'string',
        'remarks' => 'string',
        'total_amount' => 'decimal'
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(CounterDetail::class, 'transaction_id');
    }

    public function getSeriesColumnAttribute()
    {
        return 'sheet_number';
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('counter.sheet.number.prefix');
    }
}