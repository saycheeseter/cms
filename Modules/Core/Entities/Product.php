<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Presenters\ProductPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\CustomizableReport;
use Bnb\Laravel\Attachments\HasAttachment;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;

class Product extends DataModule implements CustomizableReport, ModuleSeries
{
    use SoftDeletes,
        HasAttachment,
        HasModuleSeries,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'product';

    protected $attributes = [
        'is_used' => 0
    ];

    protected $guarded = [
        'id',
        'created_from',
        'deleted_at',
        'created_by',
        'modified_by',
        'updated_at'
    ];

    protected $casts = [
        'stock_no'        => 'string',
        'supplier_sku'    => 'string',
        'name'            => 'string',
        'description'     => 'string',
        'chinese_name'    => 'string',
        'status'          => 'int',
        'senior'          => 'int',
        'memo'            => 'string',
        'supplier_id'     => 'int',
        'brand_id'        => 'int',
        'category_id'     => 'int',
        'manage_type'     => 'int',
        'is_used'         => 'bool',
        'group_type'      => 'int',
        'manageable'      => 'int',
        'type'            => 'int',
        'default_unit'    => 'int'
    ];

    public function prices()
    {
        return $this->hasMany(BranchPrice::class, 'product_id')->withPrimaryKey(['branch_id', 'product_id']);
    }

    public function units()
    {
        return $this->hasMany(ProductUnit::class)->withPrimaryKey(['product_id', 'uom_id']);
    }

    public function barcodes()
    {
        return $this->hasMany(ProductBarcode::class, 'product_id')->withPrimaryKey(['product_id', 'uom_id', 'code']);
    }

    public function summaries()
    {
        return $this->hasMany(ProductBranchSummary::class)->withPrimaryKey(['product_id', 'branch_id', 'branch_detail_id']);
    }

    public function taxes()
    {
        return $this->belongsToMany(Tax::class, 'product_tax', 'product_id', 'tax_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id')->withTrashed();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withTrashed();
    }

    public function branches()
    {
         return $this->hasMany(ProductBranchInfo::class)->withPrimaryKey(['product_id', 'branch_id']);
    }

    public function locations()
    {
         return $this->hasMany(ProductBranchDetail::class)->withPrimaryKey(['product_id', 'branch_detail_id']);
    }

    public function suppliers()
    {
        return $this->hasMany(ProductSupplier::class)->withPrimaryKey(['product_id', 'supplier_id']);;
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }

    public function customers()
    {
        return $this->hasMany(ProductCustomer::class);
    }

    public function serials()
    {
        return $this->hasMany(ProductSerialNumber::class);
    }

    public function alternatives()
    {
        return $this->belongsToMany(Product::class, 'product_alternative', 'product_id', 'alternative_id');
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountDetail::class, 'entity');
    }

    public function components()
    {
        return $this->belongsToMany(Product::class, 'product_component', 'group_id', 'component_id')
            ->withPivot('qty')
            ->using(ProductComponent::class);
    }
    
    public function groups()
    {
        return $this->belongsToMany(Product::class, 'product_component', 'component_id', 'group_id')
            ->withPivot('qty')
            ->using(ProductComponent::class);
    }

    public function presenter()
    {
        return new ProductPresenter($this);
    }

    public function getSeriesColumnAttribute()
    {
        return 'stock_no';
    }

    public function relationships()
    {
        return [
            'barcode' => [
                'class' => ProductBarcode::class,
                'relation' => 'barcodes',
                'uniques' => ['product_id', 'code']
            ],
            'branch_price' => [
                'class' => BranchPrice::class,
                'relation' => 'prices',
                'uniques' => ['product_id', 'branch_id']
            ],
            'branch_summary' => [
                'class' => ProductBranchSummary::class,
                'relation' => 'summaries',
                'uniques' => ['product_id', 'branch_id', 'branch_detail_id']
            ],
            'unit' => [
                'class' => ProductUnit::class,
                'relation' => 'units',
                'uniques' => ['product_id', 'uom_id']
            ],
            'brand' => [
                'class' => Brand::class,
                'relation' => 'brand'
            ],
            'category' => [
                'class' => Category::class,
                'relation' => 'category'
            ],
            'supplier' => [
                'class' => Supplier::class,
                'relation' => 'supplier'
            ],
            'created_by' => [
                'class' => User::class,
                'relation' => 'creator'
            ],
            'modifier' => [
                'class' => User::class,
                'relation' => 'modifier'
            ],
            'branch_detail' => [
                'class' => ProductBranchDetail::class,
                'relation' => 'locations',
                'uniques' => ['product_id', 'branch_detail_id']
            ],
            'branch_info' => [
                'class' => ProductBranchInfo::class,
                'relation' => 'branches',
                'uniques' => ['product_id', 'branch_id']
            ],
            'tax' => [
                'class' => Tax::class,
                'relation' => 'taxes',
                'uniques' => ['product_id', 'tax_id']
            ],
            'suppliers' => [
                'class' => ProductSupplier::class,
                'relation' => 'suppliers',
                'uniques' => ['product_id', 'supplier_id']
            ]
        ];
    }
}