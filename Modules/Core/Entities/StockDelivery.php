<?php 

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionRemainingReference;
use Modules\Core\Presenters\DeliveryOutboundPresenter;
use Modules\Core\Traits\HasReferenceRemainingQtyProcess;

class StockDelivery extends Invoice implements TransactionRemainingReference
{
    use HasReferenceRemainingQtyProcess;

    protected $table = 'stock_delivery';

    public function details()
    {
        return $this->hasMany(StockDeliveryDetail::class, 'transaction_id');
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function deliverTo()
    {
        return $this->belongsTo(Branch::class, 'deliver_to')->withTrashed();
    }

    public function deliverToLocation()
    {
        return $this->belongsTo(BranchDetail::class, 'deliver_to_location')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockRequest::class, 'reference_id')->withTrashed();
    }

    public function presenter()
    {
        return new DeliveryOutboundPresenter($this);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.delivery.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => StockDeliveryDetail::class,
                    'relation' => 'details'
                ],
                'deliver_to' => [
                    'class' => Branch::class,
                    'relation' => 'deliverTo'
                ],
                'deliver_to_location' => [
                    'class' => BranchDetail::class,
                    'relation' => 'deliverToLocation'
                ],
                'reference' => [
                    'class' => StockRequest::class,
                    'break' => true,
                    'relation' => 'reference'
                ],
            ]
        );
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'deliver_to' => 'int',
            'deliver_to_location' => 'int',
            'reference_id' => 'int',
            'transaction_type' => 'int',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(StockDeliveryOutbound::class, 'reference_id');
    }
}