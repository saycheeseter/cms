<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\ConversionMovingAverageCost;
use Modules\Core\Entities\Contracts\GenerateConversionProductMonthlySummary;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;
use Modules\Core\Presenters\ProductConversionPresenter;
use Modules\Core\Entities\Contracts\TransactionReference;
use Modules\Core\Entities\Contracts\BranchModuleSeries;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasBranchModuleSeries;
use Modules\Core\Traits\HasCustomInventoryProcessForConversion;
use Modules\Core\Traits\HasTransactionSheetNumber;
use Modules\Core\Traits\UserAudit;
use Modules\Core\Traits\GenerateConversionProductMonthlySummary as HasConversionProductMonthlySummary;

class ProductConversion extends Base implements
    TransactionReference,
    TransactionMovingAverageCost,
    ConversionMovingAverageCost,
    BranchModuleSeries,
    GenerateConversionProductMonthlySummary
{
    use SoftDeletes,
        HasBranchModuleSeries,
        HasTransactionSheetNumber,
        HasConversionProductMonthlySummary,
        BranchAudit,
        HasCustomInventoryProcessForConversion,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $attributes = [
        'approval_status' => ApprovalStatus::DRAFT
    ];

    protected $table = 'product_conversion';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'approval_status' => 'int',
        'created_from' => 'int',
        'created_by' => 'int',
        'created_for' => 'int',
        'for_location' => 'int',
        'audited_by' => 'int',
        'audited_date' => 'datetime',
        'transaction_date' => 'datetime',
        'product_id' => 'int',
        'qty' => 'decimal',
        'group_type' => 'int',
        'ma_cost' => 'decimal',
        'sheet_number' => 'string',
        'amount' => 'decimal',
        'cost' => 'decimal'
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function auditor()
    {
        return $this->belongsTo(User::class, 'audited_by')->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(ProductConversionDetail::class, 'transaction_id');
    }

    public function posting()
    {
        return $this->morphOne(TransactionSummaryPosting::class, 'transaction');
    }

    public function presenter()
    {
        return new ProductConversionPresenter($this);
    }

    public function getAmountAttribute()
    {
        return $this->ma_cost->mul($this->qty, setting('monetary.precision'));
    }

    public function getTotalCostAttribute()
    {
        return $this->cost->mul($this->qty, setting('monetary.precision'));
    }

    public function getSeriesColumnAttribute()
    {
        return 'sheet_number';
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('product.conversion.sheet.number.prefix');
    }

    public function getIsDeletedAttribute()
    {
        return $this->trashed();
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->for_location;
    }

    public function getProductMonthlyTransactionProductIdAttribute()
    {
        return $this->product_id;
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction_date;
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $key = $this->process_flow;

        return [
            $key.'_total_qty' => 'qty',
            $key.'_total_cost' => 'total_cost',
            $key.'_total_ma_cost' => 'amount'
        ];
    }

    public function getProcessFlowAttribute()
    {
        return $this->group_type === GroupType::MANUAL_GROUP
            ? 'product_conversion_increase'
            : 'product_conversion_decrease';
    }
}