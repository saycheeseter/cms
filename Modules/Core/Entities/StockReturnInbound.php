<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\DeliveryInboundPresenter;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Lang;
use Modules\Core\Traits\HasProductBatchDeliveryInboundReferenceRemainingComputation;

class StockReturnInbound extends InventoryChain
{
    use HasProductBatchDeliveryInboundReferenceRemainingComputation;

    protected $table = 'stock_return_inbound';

    public function details()
    {
        return $this->hasMany(StockReturnInboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(StockReturnOutbound::class, 'reference_id')->withTrashed();
    }

    public function deliveryFrom()
    {
        return $this->belongsTo(Branch::class, 'delivery_from')->withTrashed();
    }

    public function deliveryFromLocation()
    {
        return $this->belongsTo(BranchDetail::class, 'delivery_from_location')->withTrashed();
    }

    public function presenter()
    {
        return new DeliveryInboundPresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->positive_multiplier;
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::AVAILABLE;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::STOCK_RETURN_INBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.stock.return.inbound');
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getBatchBaseOwnerQtyAttribute()
    {
        return 'pending_qty';
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.return.inbound.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'delivery_from' => 'int',
            'delivery_from_location' => 'int'
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'stock_return_inbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'stock_return_inbound';
    }
}
