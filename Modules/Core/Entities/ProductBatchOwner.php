<?php

namespace Modules\Core\Entities;

class ProductBatchOwner extends Base
{   
    public $table = 'product_batch_owner';

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'owner_id' => 'int',
        'batch_id' => 'int',
        'current_qty' => 'decimal',
        'pending_qty' => 'decimal',
    ];

    protected $fillable = [
        'owner_id',
        'owner_type',
        'batch_id',
        'current_qty',
        'pending_qty'
    ];

    public function batch()
    {
        return $this->belongsTo(ProductBatch::class, 'batch_id', 'id');
    }

    public function ownable()
    {
        return $this->morphTo();
    }
}
