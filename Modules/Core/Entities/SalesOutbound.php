<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Enums\Customer as CustomerEnum;
use Modules\Core\Presenters\SalesIOPresenter;
use Lang;
use Modules\Core\Traits\HasAutoProductConversionTransaction;
use Modules\Core\Traits\HasCurrentCostAsMAC;
use Modules\Core\Traits\HasCustomerHistoricalTransaction;
use Modules\Core\Traits\HasPayableRemainingAmount;

class SalesOutbound extends InventoryChain implements TransactionMovingAverageCost
{
    use HasPayableRemainingAmount,
        HasAutoProductConversionTransaction,
        HasCustomerHistoricalTransaction,
        HasCurrentCostAsMAC;

    protected $table = 'sales_outbound';

    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = $value;

        $this->attributes['due_date'] = Carbon::parse($value)->addDays($this->term);
    }

    public function setTermAttribute($value)
    {
        $this->attributes['term'] = $value;

        if (!empty($this->transaction_date)) {
            $this->attributes['due_date'] = Carbon::parse($this->transaction_date)->addDays($value);
        }
    }

    public function details()
    {
        return $this->hasMany(SalesOutboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(Sales::class, 'reference_id')->withTrashed();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class, 'customer_detail_id')->withTrashed();
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function collections()
    {
        return $this->morphMany(CollectionReference::class, 'reference');
    }

    public function presenter()
    {
        return new SalesIOPresenter($this);
    }

    public function getCollectedAmountAttribute()
    {
        return $this->total_amount->sub($this->remaining_amount, 6);
    }

    public function getMultiplierAttribute()
    {
        return $this->negative_multiplier;
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('sales.outbound.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => SalesOutboundDetail::class,
                    'relation' => 'details'
                ],
                'customer' => [
                    'class' => Customer::class,
                    'relation' => 'customer'
                ],
                'salesman' => [
                    'class' => User::class,
                    'relation' => 'salesman'
                ],
                'reference' => [
                    'class' => Sales::class,
                    'break' => true,
                    'relation' => 'reference'
                ],
            ]
        );
    }

    public function getSerialOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(Customer::class);
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return $this->customer_type === CustomerEnum::WALK_IN
            ? null
            : $this->customer_id;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::SALES_OUTBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.sales.outbound');
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(Customer::class);
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->customer_type === CustomerEnum::WALK_IN
            ? null
            : $this->customer_id;
    }

    public function getCustomerValueAttribute()
    {
        return $this->customer_type == CustomerEnum::WALK_IN
            ? $this->customer_walk_in_name
            : $this->customer_id;
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'remaining_amount' => 'decimal',
            'customer_id' => 'int',
            'customer_detail_id' => 'int',
            'salesman_id' => 'int',
            'term' => 'int',
            'customer_type' => 'int',
            'customer_walk_in_name' => 'string',
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'sales_outbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'sales_outbound';
    }
}
