<?php 

namespace Modules\Core\Entities;

abstract class BalanceFlow extends Base
{
    protected $primaryKey = '';

    public $incrementing = false;

    protected $guarded = [
        'id',
        'deleted_at',
        'created_at',
        'modified_at'
    ];

    protected $casts = [
        'amount' => 'decimal',
        'transaction_date' => 'datetime',
        'beginning' => 'decimal',
        'balance' => 'decimal',
    ];

    public function transactionable()
    {
        return $this->morphTo();
    }
}