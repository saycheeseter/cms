<?php

namespace Modules\Core\Entities;

class TransactionSerialNumberLogs extends Base
{
    public function transaction()
    {
        return $this->morphTo();
    }

    public function serial()
    {
        return $this->belongsTo(ProductSerialNumber::class);
    }
}