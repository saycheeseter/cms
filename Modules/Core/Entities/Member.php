<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;
use Modules\Core\Presenters\MemberPresenter;

class Member extends DataModule implements ModuleSeries
{
    use SoftDeletes,
        HasModuleSeries,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'member';
    
    protected $attributes = [
        'gender' => 0,
        'status' => 0
    ];

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'deleted_at',
        'created_at',
        'modified_at'
    ];

    protected $casts = [
        'card_id' => 'string',
        'barcode' => 'string',
        'full_name' => 'string',
        'mobile' => 'string',
        'telephone' => 'string',
        'address' => 'string',
        'email' => 'string',
        'gender' => 'int',
        'birth_date' => 'date',
        'registration_date' => 'date',
        'expiration_date' => 'date',
        'status' => 'int',
        'memo' => 'string',
        'rate_head_id' => 'int'
    ];

    public function rate()
    {
        return $this->belongsTo(MemberRate::class, 'rate_head_id')->withTrashed();
    }

    public function points()
    {
        return $this->hasMany(MemberPointTransaction::class, 'member_id');
    }

    public function getTotalPointsAttribute()
    {   
        $amount = Decimal::create(0, setting('monetary.precision'));
        
        foreach($this->points as $point) {
            $amount = $amount->add($point->amount);
        }

        return $amount;
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountTarget::class, 'target');
    }

    public function presenter()
    {
        return new MemberPresenter($this);
    }

    public function getSeriesColumnAttribute()
    {
        return 'card_id';
    }
}
