<?php

namespace Modules\Core\Entities;

class GenericSetting extends Setting
{
    protected $table = 'generic_settings';

    protected $fillable = [
        'key',
        'value',
        'comments',
        'type'
    ];

    protected $casts = [
        'key' => 'string',
        'comments' => 'string',
        'type' => 'string',
    ];
}
