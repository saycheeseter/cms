<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class PosCardPayment extends Base 
{
    use SoftDeletes;

    protected $table = 'pos_card_payment';

    protected $touches = ['collection'];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'card_number' => 'string',
        'full_name' => 'string',
        'type' => 'int',
        'amount' => 'decimal',
        'expiration_date' => 'string',
        'approval_code' => 'string'
    ];

    public function collection()
    {
        return $this->belongsTo(PosCollection::class, 'collection_id', 'id')->withTrashed();
    }
}