<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use DateTime;

class ProductSupplier extends ProductEntityLog
{
    protected $table = 'product_supplier';

    protected $attributes = [
        'default' => 0
    ];

    protected $casts = [
        'min_qty' => 'decimal',
        'latest_unit_qty' => 'decimal',
        'latest_price' => 'decimal',
        'lowest_price' => 'decimal',
        'highest_price' => 'decimal',
        'latest_fields_updated_date' => 'datetime',
        'latest_unit_id' => 'int',
        'supplier_id' => 'int',
        'product_id' => 'int',
        'remarks' => 'string',
        'order_lead_time' => 'int',
        'default' => 'int'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }

    public function setLatestFieldsUpdatedDateAttribute($value)
    {
        $this->attributes['latest_fields_updated_date'] = $value instanceof DateTime
            ? Carbon::instance($value)
            : Carbon::parse($value);
    }
}
