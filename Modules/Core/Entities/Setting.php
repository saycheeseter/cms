<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;

class Setting extends DataModule
{
    public $timestamps = false;

    public function getValueAttribute($value)
    {
        return $this->castValueAttribute($value);
    }

    public function setValueAttribute($value)
    {
        $value = $this->castValueAttribute($value);

        if ($this->type === 'boolean') {
            $value = $value ? 'true' : 'false';
        }

        $this->attributes['value'] = $value;
    }

    public function getReadableKeyAttribute()
    {
        return ucwords(str_replace('.', ' ', $this->key));
    }

    public function generateTags(): array
    {
        return [
            $this->readable_key,
        ];
    }

    protected function castValueAttribute($value)
    {
        switch ($this->type) {
            case 'boolean':
                return filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;

            case 'int':
                return (int) $value;
                break;

            case 'string':
                return (string) $value;
                break;

            case 'date':
                return Carbon::parse($value)->toDateString();
                break;

            case 'datetime':
                return Carbon::parse($value)->toDateTimeString();
                break;
        }
    }
}
