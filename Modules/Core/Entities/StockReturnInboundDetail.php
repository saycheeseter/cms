<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\HasPriceAsMovingAverageCost;

class StockReturnInboundDetail extends InventoryChainDetail
{
    use HasPriceAsMovingAverageCost;

    protected $table = 'stock_return_inbound_detail';

    public function transaction()
    {
        return $this->belongsTo(StockReturnInbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockReturnOutboundDetail::class, 'reference_id')->withTrashed();
    }
}
