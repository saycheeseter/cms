<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Presenters\ProductDiscountPresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class ProductDiscount extends Base implements AuditableInterface
{   
    use SoftDeletes,
        Auditable,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }
    
    protected $table = 'product_discount';
    
    protected $guarded = [
        'id',
        'deleted_at',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'code'            => 'string',
        'name'            => 'string',
        'valid_from'      => 'datetime',
        'valid_to'        => 'datetime',
        'status'          => 'int',
        'mon'             => 'int',
        'tue'             => 'int',
        'wed'             => 'int',
        'thur'            => 'int',
        'fri'             => 'int',
        'sat'             => 'int',
        'sun'             => 'int',
        'target_type'     => 'int',
        'discount_scheme' => 'int',
        'select_type'     => 'int',
        'created_for'     => 'int',
        'created_from' => 'int',
        'created_by' => 'int',
        'modified_by' => 'int',
    ];

    public function details()
    {
        return $this->hasMany(ProductDiscountDetail::class, 'product_discount_id');
    }

    public function targets()
    {
        return $this->hasMany(ProductDiscountTarget::class, 'product_discount_id');
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function setValidFromAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['valid_from'] = Carbon::createFromFormat('Y-m-d', $value)
                ->startOfDay()
                ->equalTo(Carbon::today())
                    ? Carbon::now()
                    : Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        } else {
            $this->attributes['valid_from'] = $value;
        }
    }

    public function setValidToAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['valid_to'] = Carbon::createFromFormat('Y-m-d', $value)->modify('23.59.59.999');
        } else {
            $this->attributes['valid_to'] = $value;
        }
    }

    public function presenter()
    {
        return new ProductDiscountPresenter($this);
    }
}
