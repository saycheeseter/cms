<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Presenters\DataCollectorTransactionPresenter;
use Modules\Core\Traits\HasBranchModuleSeries;
use Modules\Core\Entities\Contracts\BranchModuleSeries;

class DataCollectorTransaction extends Base implements BranchModuleSeries
{
    use SoftDeletes, HasBranchModuleSeries;

    protected $table = 'data_collector_transaction';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'branch_id' => 'integer',
        'reference_no' => 'string',
        'filesize' => 'integer',
        'client_id' => 'integer',
        'transaction_type' => 'integer',
        'source' => 'integer',
        'row_count' => 'integer',
        'import_date' => 'date',
        'imported_by' => 'integer'
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function importer()
    {
        return $this->belongsTo(User::class, 'imported_by');
    }

    public function details()
    {
        return $this->hasMany(DataCollectorTransactionDetail::class, 'transaction_id');
    }

    public function presenter()
    {
        return new DataCollectorTransactionPresenter($this);
    }

    public function getSeriesColumnAttribute()
    {
        return 'reference_no';
    }
}