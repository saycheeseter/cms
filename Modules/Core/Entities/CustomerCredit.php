<?php

namespace Modules\Core\Entities;

class CustomerCredit extends Base
{
    protected $primaryKey = 'customer_id';

    public $incrementing = false;

    protected $casts = [
        'due' => 'decimal',
        'customer_id' => 'int'
    ];

    public $attributes = [
        'due' => 0
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}