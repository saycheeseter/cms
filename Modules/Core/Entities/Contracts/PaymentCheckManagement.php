<?php

namespace Modules\Core\Entities\Contracts;

interface PaymentCheckManagement
{
    public function details();
    public function getCheckTransactionableTypeAssignmentAttribute();
}