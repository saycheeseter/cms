<?php

namespace Modules\Core\Entities\Contracts;

interface GenerateAutoConversionDetailProductMonthlySummary
{
    public function getProductMonthlyTransactionProductIdAttribute();
    public function getProductMonthlyTransactionAttributeMapping();
}