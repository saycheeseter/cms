<?php

namespace Modules\Core\Entities\Contracts;

interface CustomizableReport
{
    public function relationships();
    public function getExcludedColumnsAttribute();
    public function getTable();
}