<?php

namespace Modules\Core\Entities\Contracts;

interface GenerateConversionProductMonthlySummary
{
    public function getProductMonthlyTransactionCreatedForAttribute();
    public function getProductMonthlyTransactionForLocationAttribute();
    public function getProductMonthlyTransactionProductIdAttribute();
    public function getProductMonthlyTransactionTransactionDateAttribute();
    public function getProductMonthlyTransactionAttributeMapping();
}