<?php

namespace Modules\Core\Entities\Contracts;

interface TransactionRemainingReference
{
    public function details();
    public function reference();
}