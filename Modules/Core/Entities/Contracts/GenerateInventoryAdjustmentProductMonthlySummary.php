<?php

namespace Modules\Core\Entities\Contracts;

interface GenerateInventoryAdjustmentProductMonthlySummary
{
    public function getProductMonthlyTransactionCreatedForAttribute();
    public function getProductMonthlyTransactionForLocationAttribute();
    public function getProductMonthlyTransactionProductIdColumn();
    public function getProductMonthlyTransactionTransactionDateAttribute();
}