<?php

namespace Modules\Core\Entities\Contracts;

interface ConversionMovingAverageCost
{
    public function product();
    public function details();
}