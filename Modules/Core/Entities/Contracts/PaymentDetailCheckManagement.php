<?php

namespace Modules\Core\Entities\Contracts;

interface PaymentDetailCheckManagement
{
    public function check();
}