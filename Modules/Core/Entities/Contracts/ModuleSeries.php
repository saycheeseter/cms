<?php

namespace Modules\Core\Entities\Contracts;

interface ModuleSeries
{
    public function getSeriesTypeAttribute();
    public function getSeriesColumnAttribute();
}