<?php

namespace Modules\Core\Entities\Contracts;

interface BatchTransaction
{
    public function details();
    public function getBatchBaseOwnerTypeAssignmentAttribute();
    public function getBatchBaseOwnerIdAssignmentAttribute();
    public function getBatchBaseOwnerQtyAttribute();
    public function getBatchOwnerIdAssignmentAttribute();
    public function getBatchOwnerTypeAssignmentAttribute();
    public function getBatchOwnerQtyAttribute();
}