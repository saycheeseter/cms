<?php

namespace Modules\Core\Entities\Contracts;

interface BranchModuleSeries
{
    public function getSeriesTypeAttribute();
    public function getSeriesColumnAttribute();
    public function getSeriesBranchAttribute();
}