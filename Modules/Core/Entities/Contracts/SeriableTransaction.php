<?php

namespace Modules\Core\Entities\Contracts;

interface SeriableTransaction
{
    public function details();
    public function getSerialOwnerTypeAssignmentAttribute();
    public function getSerialOwnerIdAssignmentAttribute();
    public function getSerialStatusAssignmentAttribute();
    public function getSerialTransactionAssignmentAttribute();
    public function getSerialTransactionAssignmentLabelAttribute();
}