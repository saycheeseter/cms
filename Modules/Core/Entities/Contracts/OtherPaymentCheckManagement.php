<?php

namespace Modules\Core\Entities\Contracts;

interface OtherPaymentCheckManagement
{
    public function details();
    public function getCheckTransactionableTypeAssignmentAttribute();
}