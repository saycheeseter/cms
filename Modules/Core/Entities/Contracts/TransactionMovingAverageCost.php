<?php

namespace Modules\Core\Entities\Contracts;

interface TransactionMovingAverageCost
{
    public function details();
}