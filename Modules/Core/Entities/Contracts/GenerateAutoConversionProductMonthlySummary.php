<?php

namespace Modules\Core\Entities\Contracts;

interface GenerateAutoConversionProductMonthlySummary
{
    public function getProductMonthlyTransactionCreatedForAttribute();
    public function getProductMonthlyTransactionForLocationAttribute();
    public function getProductMonthlyTransactionTransactionDateAttribute();
    public function getProductMonthlyTransactionProductIdAttribute();
    public function getProductMonthlyTransactionAttributeMapping();
}