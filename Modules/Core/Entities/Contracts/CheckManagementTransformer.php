<?php

namespace Modules\Core\Entities\Contracts;

interface CheckManagementTransformer
{
    public function getRedirectIdAttribute();
}