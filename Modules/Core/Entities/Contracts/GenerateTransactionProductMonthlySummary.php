<?php

namespace Modules\Core\Entities\Contracts;

interface GenerateTransactionProductMonthlySummary
{
    public function getProductMonthlyTransactionCreatedForAttribute();
    public function getProductMonthlyTransactionForLocationAttribute();
    public function getProductMonthlyTransactionProductIdColumn();
    public function getProductMonthlyTransactionTransactionDateAttribute();
    public function getProductMonthlyTransactionAttributeMapping();
}