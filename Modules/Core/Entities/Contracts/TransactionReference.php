<?php

namespace Modules\Core\Entities\Contracts;

interface TransactionReference
{
    public function getSheetNumberPrefixAttribute();
    public function setSheetNumberAttribute($value);
}