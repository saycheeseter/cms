<?php

namespace Modules\Core\Entities\Contracts;

interface TransactionSummary
{
    public function details();
    public function getTransactionSummaryFieldAttribute();
}