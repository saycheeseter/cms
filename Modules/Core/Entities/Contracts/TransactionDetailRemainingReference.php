<?php

namespace Modules\Core\Entities\Contracts;

interface TransactionDetailRemainingReference
{
    public function reference();
}