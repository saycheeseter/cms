<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\TransactionSummaryPostingPresenter;

class TransactionSummaryPosting extends Base
{
    public $timestamps = false;

    protected $casts = [
        'transaction_id' => 'int',
        'transaction_type' => 'string',
        'transaction_date' => 'date'
    ];

    public function transaction()
    {
        return $this->morphTo();
    }

    public function presenter()
    {
        return new TransactionSummaryPostingPresenter($this);
    }
}