<?php 

namespace Modules\Core\Entities;

class Terminal extends Base
{
    public $timestamps = false;

    protected $table = 'terminal';

    protected $casts = [
        'number' => 'int',
        'branch_id' => 'int'
    ];

    protected $fillable = [
        'number',
        'branch_id'
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }
}