<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class PosCollection extends Base 
{
    use SoftDeletes;

    protected $table = 'pos_collection';

    protected $touches = ['transaction'];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'amount' => 'decimal'
    ];

    public function transaction()
    {
        return $this->belongsTo(PosSales::class, 'transaction_id', 'id')->withTrashed();
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method')->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(BankAccount::class, 'bank_id')->withTrashed();
    }

    public function cardPayments()
    {
        return $this->hasMany(PosCardPayment::class, 'collection_id');
    }
}