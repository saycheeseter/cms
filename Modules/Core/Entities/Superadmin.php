<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Enums\UserType;

class Superadmin extends User
{
    protected $attributes = [
        'type' => UserType::SUPERADMIN
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('type', UserType::SUPERADMIN);
        });
    }
}