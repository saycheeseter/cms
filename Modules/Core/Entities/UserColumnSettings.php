<?php

namespace Modules\Core\Entities;

class UserColumnSettings extends Base
{
    public $timestamps = false;

    protected $table = 'user_column_settings';

    protected $casts = [
        'user_id' => 'int',
        'module_id' => 'int',
        'settings' => 'object'
    ];

    protected $guarded = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }
}