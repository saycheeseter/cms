<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\AuditableToProduct;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\HasModuleSeries;

class ProductBarcode extends DataModule implements ModuleSeries
{
    use AuditableToProduct, HasModuleSeries;

    protected $table = 'product_barcode';

    protected $touches = ['product'];

    public $timestamps = false;

    public $incrementing = false;

    protected $casts = [
        'code' => 'string',
        'memo' => 'string',
        'uom_id' => 'int'
    ];

    protected $guarded = [
        'product_id',
    ];

    public function uom()
    {
        return $this->belongsTo(UOM::class, 'uom_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relationships()
    {
        return [
            'uom' => [
                'class' => UOM::class,
                'relation' => 'uom'
            ]
        ];
    }

    public function generateTags(): array
    {
        return [
            'Product Barcode',
        ];
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}
