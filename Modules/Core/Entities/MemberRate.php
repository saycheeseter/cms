<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Presenters\MemberRatePresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;

class MemberRate extends DataModule
{
    use SoftDeletes,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'member_rate';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'name' => 'string',
        'memo' => 'string',
        'status' => 'int',
    ];

    public function details()
    {
        return $this->hasMany('Modules\Core\Entities\MemberRateDetail', 'member_rate_head_id');
    }

    public function members()
    {
        return $this->hasMany('Modules\Core\Entities\Member', 'rate_head_id');
    }

    public function presenter()
    {
        return new MemberRatePresenter($this);
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountTarget::class, 'target');
    }
}
