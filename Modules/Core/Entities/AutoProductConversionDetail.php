<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\GenerateAutoConversionDetailProductMonthlySummary;
use Modules\Core\Traits\HasAutoConversionDetailCustomProcess;

class AutoProductConversionDetail extends Base implements GenerateAutoConversionDetailProductMonthlySummary
{
    use HasAutoConversionDetailCustomProcess;

    protected $table = 'auto_product_conversion_detail';

    protected $attributes = [
        'ma_cost' => 0
    ];

    protected $casts = [
        'transaction_id' => 'int',
        'product_id' => 'int',
        'required_qty' => 'decimal',
        'ma_cost' => 'decimal',
        'amount' => 'decimal',
        'total_qty' => 'decimal',
        'cost' => 'decimal'
    ];

    protected $guarded = [
        'id'
    ];

    public $timestamps = false;

    public function transaction()
    {
        return $this->belongsTo(AutoProductConversion::class, 'transaction_id', 'id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function getTotalQtyAttribute()
    {
        return $this->required_qty->mul($this->transaction->qty);
    }

    public function getAmountAttribute()
    {
        return $this->ma_cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getTotalCostAttribute()
    {
        return $this->cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getProductMonthlyTransactionProductIdAttribute()
    {
        return $this->product_id;
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $key = $this->process_flow;

        return [
            $key.'_total_qty' => 'total_qty',
            $key.'_total_cost' => 'total_cost',
            $key.'_total_ma_cost' => 'amount'
        ];
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return $this->process_flow;
    }

    public function getProcessFlowAttribute()
    {
        $flow = '';

        switch ($this->transaction->transaction_type) {
            case $this->getMorphMapKey(SalesOutboundDetail::class):
            case $this->getMorphMapKey(PosSalesDetail::class):
                $flow = 'auto_product_conversion_decrease';
                break;

            case $this->getMorphMapKey(SalesReturnInboundDetail::class):
                $flow = 'auto_product_conversion_increase';
                break;
        }

        return $flow;
    }
}