<?php 

namespace Modules\Core\Entities;

use Modules\Core\Presenters\CollectionPresenter;
use Modules\Core\Enums\Customer as CustomerEnum;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Traits\HasCollectionProcess;

class Collection extends FinancialFlow
{
    use HasCollectionProcess;

    protected $table = 'collection';

    public function details()
    {
        return $this->hasMany(CollectionDetail::class, 'transaction_id');
    }

    public function references()
    {
        return $this->hasMany(CollectionReference::class, 'transaction_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function flow()
    {
        return $this->morphMany(CustomerBalanceFlow::class, 'transactionable');
    }

    public function getTotalCollectibleAttribute()
    {
        $total = Decimal::create(0, setting('monetary.precision'));

        $references = $this->references;

        foreach ($references as $reference) {
            $total = $total->add($reference->collectible);
        }

        return $total;
    }

    public function getTotalExcessAmountAttribute()
    {
        return $this->total_amount->sub($this->total_collectible);
    }

    public function getCustomerValueAttribute()
    {
        return $this->customer_type == CustomerEnum::WALK_IN
            ? $this->customer_walk_in_name
            : $this->customer_id;
    }

    public function presenter()
    {
        return new CollectionPresenter($this);
    }

    public function getCheckTransactionableTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(CollectionDetail::class);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('collection.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'customer_type' => 'int',
            'customer_id' => 'int',
            'customer_walk_in_name' => 'string'
        ]);
    }
}