<?php 

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\PaymentDetailCheckManagement;
use Modules\Core\Traits\HasPaymentDetailProcess;

class PaymentDetail extends FinancialFlowDetail implements PaymentDetailCheckManagement
{
    use HasPaymentDetailProcess;

    protected $table = 'payment_detail';

    public function transaction()
    {
        return $this->belongsTo(Payment::class, 'transaction_id', 'id')->withTrashed();
    }

    public function check()
    {
        return $this->morphOne(IssuedCheck::class, 'transactionable')->withTrashed();
    }
}