<?php

namespace Modules\Core\Entities;

use Auth;
use Illuminate\Database\Eloquent\Relations\Relation;

class UserMenu extends Base
{
    protected $table = 'user_menu';

    protected $guarded = [
        'id',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'user_id' => 'int',
        'format' => 'object',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getViewableModuleAttribute()
    {
        $format = $this->format;

        $this->removeNotAllowedModules($format);

        return $format;
    }

    private function removeNotAllowedModules(&$format)
    {   
        foreach ($format as $key => $value) {
            if($value->attributes) {
                if(!$this->isViewable($value->attributes)) {
                    unset($format[$key]);
                }
            }

            $this->removeNotAllowedModules($value->children);
        }
    }

    private function isViewable($attributes)
    {
        $type = $attributes->checker->type;
        $value = $attributes->checker->value;
        $result = false;

        switch ($type) {
            case 0:
                $result = $this->isViewableByPolicy($value);
                break;

            case 1:
                $result = $this->isViewableByPermission($value);
                break;

            case 2:
            default:
                $result = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
        }

        return $result;
    }

    private function isViewableByPolicy($expression)
    {
        $expression = explode('|', $expression);
        $alias = $expression[0];
        $ability = $expression[1] ?? 'view';

        $class = Relation::getMorphedModel($alias);

        return Auth::user()->can($ability, $class);
    }

    private function isViewableByPermission($value)
    {
        $permissions = array_wrap($value);

        return Auth::user()->isEitherGrantedTo($permissions);
    }
}