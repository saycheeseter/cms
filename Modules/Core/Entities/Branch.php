<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Presenters\BranchPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Traits\HasBranchInitialDataProcess;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;

class Branch extends DataModule implements ModuleSeries
{
    use SoftDeletes,
        HasModuleSeries,
        HasBranchInitialDataProcess,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }
    
    protected $table = 'branch';
    
    protected $guarded = [
        'id',
        'deleted_at',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'address' => 'string',
        'contact' => 'string',
        'business_name' => 'string',
    ];

    public function details()
    {
        return $this->hasMany(BranchDetail::class);
    }

    public function presenter()
    {
        return new BranchPresenter($this);
    }

    public function serials()
    {
        return $this->morphMany(ProductSerialNumber::class, 'ownable');
    }

    public function batches()
    {
        return $this->morphMany(ProductBatchOwner::class, 'ownable');
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}
