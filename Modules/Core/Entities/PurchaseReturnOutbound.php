<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Presenters\PurchasePresenter;
use Modules\Core\Enums\SerialTransaction;
use Lang;
use Modules\Core\Traits\HasCurrentCostAsMAC;
use Modules\Core\Traits\HasPayableRemainingAmount;

class PurchaseReturnOutbound extends InventoryChain implements TransactionMovingAverageCost
{
    use HasCurrentCostAsMAC,
        HasPayableRemainingAmount;

    protected $table = 'purchase_return_outbound';

    public function details()
    {
        return $this->hasMany(PurchaseReturnOutboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(PurchaseReturn::class, 'reference_id')->withTrashed();
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }
    
    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method')->withTrashed();
    }

    public function payments()
    {
        return $this->morphMany(PaymentReference::class, 'reference');
    }

    public function getPaidAmountAttribute()
    {
        return $this->total_amount->sub($this->remaining_amount);
    }

    public function presenter()
    {
        return new PurchasePresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->negative_multiplier;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::PURCHASE_RETURN_OUTBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.purchase.return.outbound');
    }

    public function getSerialOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(Supplier::class);
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return $this->supplier_id;
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->supplier_id;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(Supplier::class);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('purchase.return.outbound.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'supplier_id' => 'int',
            'payment_method' => 'int',
            'remaining_amount' => 'decimal',
            'term' => 'int',
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'purchase_return_outbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'purchase_return_outbound';
    }
}
