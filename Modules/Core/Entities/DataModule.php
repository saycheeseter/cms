<?php

namespace Modules\Core\Entities;

use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class DataModule extends Base implements AuditableInterface
{
    use Auditable;
}