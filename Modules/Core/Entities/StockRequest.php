<?php

namespace Modules\Core\Entities;

class StockRequest extends Invoice
{
    protected $table = 'stock_request';

    public function details()
    {
        return $this->hasMany(StockRequestDetail::class, 'transaction_id');
    }

    public function to()
    {
        return $this->belongsTo(Branch::class, 'request_to')->withTrashed();
    }

    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'transactionable');
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.request.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => StockRequestDetail::class,
                    'relation' => 'details'
                ],
                'request_to' => [
                    'class' => Branch::class,
                    'relation' => 'to'
                ],
            ]
        );
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'request_to' => 'int',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(StockDelivery::class, 'reference_id');
    }
}
