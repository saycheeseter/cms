<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\SalesPresenter;

class Sales extends Invoice
{
    protected $table = 'sales';

    public function details()
    {
        return $this->hasMany(SalesDetail::class, 'transaction_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class, 'customer_detail_id')->withTrashed();
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function presenter()
    {
        return new SalesPresenter($this);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('sales.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => SalesDetail::class,
                    'relation' => 'details'
                ],
                'customer_regular' => [
                    'class' => Customer::class,
                    'relation' => 'customer'
                ],
                'salesman' => [
                    'class' => User::class,
                    'relation' => 'salesman'
                ],
            ]
        );
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'customer_id' => 'int',
            'customer_detail_id' => 'int',
            'salesman_id' => 'int',
            'term' => 'int',
            'customer_type' => 'int',
            'customer_walk_in_name' => 'string',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(SalesOutbound::class, 'reference_id');
    }
}
