<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\GenerateConversionProductMonthlySummary;
use Modules\Core\Enums\GroupType;
use Modules\Core\Traits\HasCustomConversionDetailProcess;

class ProductConversionDetail extends Base implements GenerateConversionProductMonthlySummary
{
    use HasCustomConversionDetailProcess;

    protected $table = 'product_conversion_detail';

    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'transaction_id' => 'int',
        'component_id' => 'int',
        'required_qty' => 'decimal',
        'ma_cost' => 'decimal',
        'amount' => 'decimal',
        'total_qty' => 'decimal',
        'cost' => 'decimal'
    ];

    public function transaction()
    {
        return $this->belongsTo(ProductConversion::class, 'transaction_id', 'id')->withTrashed();
    }

    public function component()
    {
        return $this->belongsTo(Product::class, 'component_id')->withTrashed();
    }

    public function getTotalQtyAttribute()
    {
        return $this->required_qty->mul($this->transaction->qty);
    }

    public function getAmountAttribute()
    {
        return $this->ma_cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getTotalCostAttribute()
    {
        return $this->cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->transaction->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->transaction->for_location;
    }

    public function getProductMonthlyTransactionProductIdAttribute()
    {
        return $this->component_id;
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction->transaction_date;
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $key = $this->process_flow;

        return [
            $key.'_total_qty' => 'total_qty',
            $key.'_total_cost' => 'total_cost',
            $key.'_total_ma_cost' => 'amount'
        ];
    }

    public function getProcessFlowAttribute()
    {
        return $this->transaction->group_type === GroupType::MANUAL_GROUP
            ? 'product_conversion_decrease'
            : 'product_conversion_increase';
    }
}