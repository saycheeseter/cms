<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Presenters\DeliveryOutboundPresenter;
use Modules\Core\Traits\ComputeQuantityTransaction;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Lang;
use Modules\Core\Traits\HasCurrentCostAsMAC;

class StockDeliveryOutbound extends InventoryChain implements TransactionMovingAverageCost
{
    use ComputeQuantityTransaction,
        HasCurrentCostAsMAC;
    
    protected $table = 'stock_delivery_outbound';

    protected $attributes = [
        'transaction_status' => TransactionStatus::NO_DELIVERY,
        'approval_status' => ApprovalStatus::DRAFT,
        'total_amount' => 0
    ];

    public function details()
    {
        return $this->hasMany(StockDeliveryOutboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(StockDelivery::class, 'reference_id')->withTrashed();
    }

    public function deliverTo()
    {
        return $this->belongsTo(Branch::class, 'deliver_to')->withTrashed();
    }

    public function deliverToLocation()
    {
        return $this->belongsTo(BranchDetail::class, 'deliver_to_location')->withTrashed();
    }

    public function presenter()
    {
        return new DeliveryOutboundPresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->negative_multiplier;
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.delivery.outbound.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => StockDeliveryOutboundDetail::class,
                    'relation' => 'details'
                ],
                'deliver_to' => [
                    'class' => Branch::class,
                    'relation' => 'deliverTo'
                ],
                'deliver_to_location' => [
                    'class' => BranchDetail::class,
                    'relation' => 'deliverToLocation'
                ],
                'reference' => [
                    'class' => StockDelivery::class,
                    'break' => true,
                    'relation' => 'reference'
                ],
            ]
        );
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::ON_TRANSIT;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::STOCK_DELIVERY_OUTBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.stock.delivery.outbound');
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return $this->deliver_to_location;
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->deliver_to_location;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getBatchOwnerQtyAttribute()
    {
        return 'pending_qty';
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'transaction_status' => 'int',
            'deliver_to' => 'int',
            'deliver_to_location' => 'int'
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'stock_delivery_outbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'stock_delivery_outbound';
    }

    public function imports()
    {
        return $this->hasMany(StockDeliveryInbound::class, 'reference_id');
    }
}
