<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\BatchStatus;

class ProductBatch extends Base
{   
    public $table = 'product_batch';

    protected $attributes = [
        'status' => BatchStatus::NOT_AVAILABLE,
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'product_id' => 'int',
        'supplier_id' => 'int',
        'status' => 'int',
        'name' => 'string',
        'qty' => 'decimal',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id')->withTrashed();
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'id')->withTrashed();
    }

    public function owners()
    {
        return $this->hasMany(ProductBatchOwner::class, 'batch_id');
    }

    public function transactionLogs()
    {
        return $this->hasMany(TransactionBatchLog::class, 'batch_id');
    }
}
