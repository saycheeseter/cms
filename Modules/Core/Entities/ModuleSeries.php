<?php

namespace Modules\Core\Entities;

class ModuleSeries extends Base
{
    protected $table = 'module_series';

    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'type'   => 'string',
        'column' => 'string',
        'series' => 'string',
        'limit'  => 'string'
    ];
}