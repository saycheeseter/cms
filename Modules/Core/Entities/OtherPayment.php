<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\OtherPaymentCheckManagement;

class OtherPayment extends OtherFinancialFlow implements OtherPaymentCheckManagement
{
    protected $table = 'other_payment';
    
    public function details()
    {
        return $this->hasMany(OtherPaymentDetail::class, 'transaction_id');
    }

    public function getCheckTransactionableTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(self::class);
    }

    public function check()
    {
        return $this->morphOne(IssuedCheck::class, 'transactionable');
    }
}
