<?php

namespace Modules\Core\Entities;

class SalesDetail extends InvoiceDetail 
{
    protected $table = 'sales_detail';

    public function transaction()
    {
        return $this->belongsTo(Sales::class, 'transaction_id', 'id')->withTrashed();
    }
}