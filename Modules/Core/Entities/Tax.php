<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Base
{
    use SoftDeletes;

    protected $table = 'tax';

    protected $guarded = [
        'id',
        'deleted_at',
        'created_by',
        'modified_by'
    ];
}
