<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class BankTransaction extends Base
{
    use SoftDeletes;

    protected $table = 'bank_transaction';

    protected $fillable = [
        'branch_id',
        'transaction_date',
        'type',
        'amount',
        'from_bank',
        'to_bank',
        'remarks'
    ];

    protected $attributes = [
        'amount' => 0
    ];

    protected $casts = [
        'branch_id' => 'int',
        'transaction_date' => 'datetime',
        'type' => 'int',
        'amount' => 'decimal',
        'from_bank' => 'int',
        'to_bank' => 'int',
        'remarks' => 'string'
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }

    public function from()
    {
        return $this->belongsTo(BankAccount::class, 'from_bank')->withTrashed();
    }

    public function to()
    {
        return $this->belongsTo(BankAccount::class, 'to_bank')->withTrashed();
    }
}