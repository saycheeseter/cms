<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\GenerateAutoConversionProductMonthlySummary;
use Modules\Core\Presenters\AutoProductConversionPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\ConversionMovingAverageCost;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasCustomAutoConversionProcess;
use Modules\Core\Traits\UserAudit;

class AutoProductConversion extends Base implements
    TransactionMovingAverageCost,
    ConversionMovingAverageCost,
    GenerateAutoConversionProductMonthlySummary
{
    use SoftDeletes,
        HasCustomAutoConversionProcess,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $attributes = [
        'ma_cost' => 0
    ];

    protected $table = 'auto_product_conversion';

    protected $casts = [
        'created_from' => 'int',
        'created_by' => 'int',
        'created_for' => 'int',
        'for_location' => 'int',
        'transaction_id' => 'int',
        'transaction_type' => 'string',
        'transaction_date' => 'datetime',
        'product_id' => 'int',
        'qty' => 'decimal',
        'group_type' => 'int',
        'ma_cost' => 'decimal',
        'amount' => 'decimal',
        'cost' => 'decimal'
    ];

    protected $guarded = [
        'id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(AutoProductConversionDetail::class, 'transaction_id');
    }

    public function posting()
    {
        return $this->morphOne(TransactionSummaryPosting::class, 'transaction');
    }

    public function getAmountAttribute()
    {
        return $this->ma_cost->mul($this->qty, setting('monetary.precision'));
    }

    public function transaction()
    {
        return $this->morphTo();
    }

    public function presenter()
    {
        return new AutoProductConversionPresenter($this);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return $this->process_flow;
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->for_location;
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction_date;
    }

    public function getTotalCostAttribute()
    {
        return $this->cost->mul($this->qty, setting('monetary.precision'));
    }

    public function getTotalMaCostAttribute()
    {
        return $this->ma_cost->mul($this->qty, setting('monetary.precision'));
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $key = $this->process_flow;

        return [
            $key.'_total_qty' => 'qty',
            $key.'_total_cost' => 'total_cost',
            $key.'_total_ma_cost' => 'total_ma_cost'
        ];
    }

    public function getProductMonthlyTransactionProductIdAttribute()
    {
        return $this->product_id;
    }

    public function getProcessFlowAttribute()
    {
        $flow = '';

        switch ($this->transaction_type) {
            case $this->getMorphMapKey(SalesOutboundDetail::class):
            case $this->getMorphMapKey(PosSalesDetail::class):
                $flow = 'auto_product_conversion_increase';
                break;

            case $this->getMorphMapKey(SalesReturnInboundDetail::class):
                $flow = 'auto_product_conversion_decrease';
                break;
        }

        return $flow;
    }
}
