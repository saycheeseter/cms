<?php

namespace Modules\Core\Entities;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Enums\UserType;
use Modules\Core\Presenters\UserPresenter;
use Bnb\Laravel\Attachments\HasAttachment;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\UserResolver;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\HasModuleSeries;
use Carbon\Carbon;
use Hash;
use Auth;

class User extends Authenticable implements UserResolver, AuditableInterface, ModuleSeries
{
    use SoftDeletes,
        HasApiTokens,
        HasAttachment,
        Auditable,
        HasModuleSeries,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'user';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'deleted_at',
        'created_at',
        'modified_at'
    ];

    protected $casts = [
        'code' => 'string',
        'username' => 'string',
        'password' => 'string',
        'full_name' => 'string',
        'position' => 'string',
        'address' => 'string',
        'telephone' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'memo' => 'string',
        'type' => 'int',
        'status' => 'int',
        'role_id' => 'int',
        'license_key' => 'string'
    ];

    protected $attributes = [
        'type' => UserType::DEFAULT
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function setLicenseKeyAttribute($value)
    {
        $value = trim($value);

        $this->attributes['license_key'] = empty($value)
            ? null
            : $value;
    }

    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_permission_user', 'user_id', 'branch_id');
    }

    public function permissions()
    {   
        return $this->belongsToMany(Permission::class, 'branch_permission_user', 'user_id', 'permission_id')->withPivot('branch_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function schedule()
    {
        return $this->hasOne(UserLoginSchedule::class);
    }

    public function menu()
    {   
        return $this->hasOne(UserMenu::class);
    }

    public function loginAudits()
    {
        return $this->hasMany(UserLoginAudit::class, 'user_id');
    }

    public function getLastLoginAuditAttribute()
    {
        return $this->loginAudits()->orderBy('created_at', 'DESC')->take(1)->first();
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }

    public function hasAccessTo(string $alias)
    {
        if ($this->isEitherAdminOrSuperadmin()) {
            return true;
        }

        return in_array(
            $alias,
            Auth::permissions()->pluck('alias')->toArray()
        );
    }

    public function isWithinLoginSchedule()
    {
        if ($this->isEitherAdminOrSuperadmin()) {
            return true;
        }

        $schedule = $this->schedule;

        $now = Carbon::now();
        $day = strtolower($now->shortEnglishDayOfWeek);

        $start = Carbon::parse('today '.$schedule->start);
        $end = Carbon::parse('today '.$schedule->end);

        return $schedule->$day == 1 && ($now->greaterThanOrEqualTo($start) && $now->lessThanOrEqualTo($end));
    }

    public function isEitherGrantedTo(array $aliases)
    {   
        foreach ($aliases as $alias) {
            if($this->hasAccessTo($alias)){
                return true;
            }
        }

        return false;
    }

    public function isSuperadmin()
    {
        return $this->type === UserType::SUPERADMIN;
    }

    public function isEitherAdminOrSuperadmin()
    {
        return in_array($this->type, [UserType::SUPERADMIN, UserType::ADMIN]);
    }

    public function hasAssignedBranch()
    {
        if ($this->isEitherAdminOrSuperadmin()) {
            return true;
        }

        return $this->permissions->pluck('alias')->search('login') !== false;
    }

    public function assignedBranches()
    {
        if ($this->isEitherAdminOrSuperadmin()) {
            return Branch::all();
        }

        return $this
            ->branches()
            ->withPivot('permission_id')
            ->join(
                'permission',
                'permission.id',
                'branch_permission_user.permission_id'
            )
            ->where('permission.alias', 'login')
            ->get();
    }

    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function presenter()
    {
        return new UserPresenter($this);
    }

    public static function resolveId()
    {
        return Auth::user() ? Auth::user()->id : null;
    }
}