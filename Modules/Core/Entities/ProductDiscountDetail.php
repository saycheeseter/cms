<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\ProductDiscountDetailPresenter;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class ProductDiscountDetail extends Base implements AuditableInterface
{   
    use Auditable;

    protected $table = 'product_discount_detail';

    public $timestamps = false;
    
    protected $casts = [
        'discount_type'  => 'int',
        'entity_id'    => 'int',
        'qty'            => 'decimal',
        'discount_value' => 'decimal',
    ];

    protected $fillable = [
        'product_discount_id',
        'entity_type',
        'entity_id',
        'discount_type',
        'discount_value',
        'qty'
    ];

    public function discount()
    {
        return $this->belongsTo(ProductDiscount::class, 'product_discount_id', 'id')->withTrashed();
    }

    public function entity()
    {
        return $this->morphTo();
    }

    public function presenter()
    {
        return new ProductDiscountDetailPresenter($this);
    }

    public function generateTags(): array
    {
        return [
            sprintf("Product Discount Code: %s", $this->discount->code),
        ];
    }
}
