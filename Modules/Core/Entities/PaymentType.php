<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;

class PaymentType extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::PAYMENT_TYPE
    ];

    protected static function type()
    {
        return SystemCodeType::PAYMENT_TYPE;
    }
}
