<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Enums\GiftCheckStatus;
use Modules\Core\Presenters\GiftCheckPresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class GiftCheck extends Base implements AuditableInterface
{
    use SoftDeletes,
        Auditable,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'gift_check';

    protected $attributes = [
        'status' => GiftCheckStatus::AVAILABLE
    ];

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'generate_rand'
    ];

    protected $casts = [
        'amount' => 'decimal',
        'date_from' => 'datetime',
        'date_to' => 'datetime',
        'redeemed_date' => 'datetime',
        'code' => 'string',
        'check_number' => 'string',
        'redeemed_by' => 'int',
        'created_by' => 'int',
        'modified_by' => 'int',
        'created_from' => 'int',
        'status' => 'int'
    ];

    public function redeemer()
    {
        return $this->belongsTo(User::class, 'redeemed_by')->withTrashed();
    }

    public function presenter()
    {
        return new GiftCheckPresenter($this);
    }

    public function setDateFromAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['date_from'] = Carbon::createFromFormat('Y-m-d', $value)
                ->startOfDay()
                ->equalTo(Carbon::today())
                    ? Carbon::now()
                    : Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        } else {
            $this->attributes['date_from'] = $value;
        }
    }

    public function setDateToAttribute($value)
    {
        if ($this->isStandardDateFormat($value)) {
            $this->attributes['date_to'] = Carbon::createFromFormat('Y-m-d', $value)->modify('23:59:59.999');
        } else {
            $this->attributes['date_to'] = $value;
        }
    }
}
