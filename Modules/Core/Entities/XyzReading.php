<?php

namespace Modules\Core\Entities;

class XyzReading extends Base
{
    protected $table = 'xyz_reading';

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'date' => 'datetime',
        'old_grand_total' => 'decimal',
        'new_grand_total' => 'decimal',
        'total_discount_amount' => 'decimal',
        'vat_returns_amount' => 'decimal',
        'nonvat_returns_amount' => 'decimal',
        'senior_returns_amount' => 'decimal',
        'cash_sales_amount' => 'decimal',
        'credit_sales_amount' => 'decimal',
        'debit_sales_amount' => 'decimal',
        'bank_sales_amount' => 'decimal',
        'gift_sales_amount' => 'decimal',
        'senior_sales_amount' => 'decimal',
        'vatable_sales_amount' => 'decimal',
        'nonvat_sales_amount' => 'decimal',
        'total_qty_sold' => 'decimal',
        'total_qty_void' => 'decimal',
        'void_transaction_amount' => 'decimal',
        'success_transaction_count' => 'decimal',
        'return_qty_count' => 'decimal',
        'member_points_amount' => 'decimal',
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }
}