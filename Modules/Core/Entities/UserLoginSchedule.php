<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\UserLoginSchedulePresenter;

class UserLoginSchedule extends Base
{
    protected $table = 'user_login_schedule';

    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'user_id' => 'int',
        'mon'     => 'int',
        'tue'     => 'int',
        'wed'     => 'int',
        'thu'     => 'int',
        'fri'     => 'int',
        'sat'     => 'int',
        'sun'     => 'int'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function presenter()
    {
        return new UserLoginSchedulePresenter($this);
    }
}