<?php

namespace Modules\Core\Entities;
use OwenIt\Auditing\Contracts\Audit as AuditInterface;
use OwenIt\Auditing\Audit as AuditTrait;
use Modules\Core\Presenters\AuditTrailPresenter;

class Audit extends Base implements AuditInterface
{
    use AuditTrait;

    protected $guarded = [];

    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    public function audited()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function presenter()
    {
        return new AuditTrailPresenter($this);
    }
}
