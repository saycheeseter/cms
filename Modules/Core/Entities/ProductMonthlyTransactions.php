<?php


namespace Modules\Core\Entities;

class ProductMonthlyTransactions extends Base
{
    protected $table = 'product_monthly_transactions';

    protected $attributes = [
        'purchase_inbound_total_qty'                     => 0,
        'purchase_inbound_total_amount'                  => 0,
        'purchase_inbound_total_cost'                    => 0,
        'stock_delivery_inbound_total_qty'               => 0,
        'stock_delivery_inbound_total_amount'            => 0,
        'stock_delivery_inbound_total_cost'              => 0,
        'stock_return_inbound_total_qty'                 => 0,
        'stock_return_inbound_total_amount'              => 0,
        'stock_return_inbound_total_cost'                => 0,
        'sales_return_inbound_total_qty'                 => 0,
        'sales_return_inbound_total_amount'              => 0,
        'sales_return_inbound_total_cost'                => 0,
        'purchase_return_outbound_total_qty'             => 0,
        'purchase_return_outbound_total_amount'          => 0,
        'purchase_return_outbound_total_cost'            => 0,
        'stock_delivery_outbound_total_qty'              => 0,
        'stock_delivery_outbound_total_amount'           => 0,
        'stock_delivery_outbound_total_cost'             => 0,
        'stock_return_outbound_total_qty'                => 0,
        'stock_return_outbound_total_amount'             => 0,
        'stock_return_outbound_total_cost'               => 0,
        'sales_outbound_total_qty'                       => 0,
        'sales_outbound_total_amount'                    => 0,
        'sales_outbound_total_cost'                      => 0,
        'damage_outbound_total_qty'                      => 0,
        'damage_outbound_total_amount'                   => 0,
        'damage_outbound_total_cost'                     => 0,
        'adjustment_increase_total_qty'                  => 0,
        'adjustment_increase_total_amount'               => 0,
        'adjustment_increase_total_cost'                 => 0,
        'adjustment_decrease_total_qty'                  => 0,
        'adjustment_decrease_total_amount'               => 0,
        'adjustment_decrease_total_cost'                 => 0,
        'product_conversion_increase_total_qty'          => 0,
        'product_conversion_increase_total_cost'         => 0,
        'product_conversion_decrease_total_qty'          => 0,
        'product_conversion_decrease_total_cost'         => 0,
        'auto_product_conversion_increase_total_qty'     => 0,
        'auto_product_conversion_increase_total_cost'    => 0,
        'auto_product_conversion_decrease_total_qty'     => 0,
        'auto_product_conversion_decrease_total_cost'    => 0,
        'purchase_inbound_total_ma_cost'                 => 0,
        'stock_delivery_inbound_total_ma_cost'           => 0,
        'stock_return_inbound_total_ma_cost'             => 0,
        'sales_return_inbound_total_ma_cost'             => 0,
        'purchase_return_outbound_total_ma_cost'         => 0,
        'stock_delivery_outbound_total_ma_cost'          => 0,
        'stock_return_outbound_total_ma_cost'            => 0,
        'sales_outbound_total_ma_cost'                   => 0,
        'damage_outbound_total_ma_cost'                  => 0,
        'adjustment_increase_total_ma_cost'              => 0,
        'adjustment_decrease_total_ma_cost'              => 0,
        'product_conversion_increase_total_ma_cost'      => 0,
        'product_conversion_decrease_total_ma_cost'      => 0,
        'auto_product_conversion_increase_total_ma_cost' => 0,
        'auto_product_conversion_decrease_total_ma_cost' => 0,
        'pos_sales_total_qty'                            => 0,
        'pos_sales_total_amount'                         => 0,
        'pos_sales_total_ma_cost'                        => 0,
        'pos_sales_total_cost'                           => 0,
    ];

    protected $casts = [
        'purchase_inbound_total_qty'                     => 'decimal',
        'purchase_inbound_total_amount'                  => 'decimal',
        'purchase_inbound_total_cost'                    => 'decimal',
        'stock_delivery_inbound_total_qty'               => 'decimal',
        'stock_delivery_inbound_total_amount'            => 'decimal',
        'stock_delivery_inbound_total_cost'              => 'decimal',
        'stock_return_inbound_total_qty'                 => 'decimal',
        'stock_return_inbound_total_amount'              => 'decimal',
        'stock_return_inbound_total_cost'                => 'decimal',
        'sales_return_inbound_total_qty'                 => 'decimal',
        'sales_return_inbound_total_amount'              => 'decimal',
        'sales_return_inbound_total_cost'                => 'decimal',
        'purchase_return_outbound_total_qty'             => 'decimal',
        'purchase_return_outbound_total_amount'          => 'decimal',
        'purchase_return_outbound_total_cost'            => 'decimal',
        'stock_delivery_outbound_total_qty'              => 'decimal',
        'stock_delivery_outbound_total_amount'           => 'decimal',
        'stock_delivery_outbound_total_cost'             => 'decimal',
        'stock_return_outbound_total_qty'                => 'decimal',
        'stock_return_outbound_total_amount'             => 'decimal',
        'stock_return_outbound_total_cost'               => 'decimal',
        'sales_outbound_total_qty'                       => 'decimal',
        'sales_outbound_total_amount'                    => 'decimal',
        'sales_outbound_total_cost'                      => 'decimal',
        'damage_outbound_total_qty'                      => 'decimal',
        'damage_outbound_total_amount'                   => 'decimal',
        'damage_outbound_total_cost'                     => 'decimal',
        'adjustment_increase_total_qty'                  => 'decimal',
        'adjustment_increase_total_amount'               => 'decimal',
        'adjustment_increase_total_cost'                 => 'decimal',
        'adjustment_decrease_total_qty'                  => 'decimal',
        'adjustment_decrease_total_amount'               => 'decimal',
        'adjustment_decrease_total_cost'                 => 'decimal',
        'product_conversion_increase_total_qty'          => 'decimal',
        'product_conversion_increase_total_cost'         => 'decimal',
        'product_conversion_decrease_total_qty'          => 'decimal',
        'product_conversion_decrease_total_cost'         => 'decimal',
        'auto_product_conversion_increase_total_qty'     => 'decimal',
        'auto_product_conversion_increase_total_cost'    => 'decimal',
        'auto_product_conversion_decrease_total_qty'     => 'decimal',
        'auto_product_conversion_decrease_total_cost'    => 'decimal',
        'product_id'                                     => 'int',
        'created_for'                                    => 'int',
        'for_location'                                   => 'int',
        'transaction_date'                               => 'date',
        'purchase_inbound_total_ma_cost'                 => 'decimal',
        'stock_delivery_inbound_total_ma_cost'           => 'decimal',
        'stock_return_inbound_total_ma_cost'             => 'decimal',
        'sales_return_inbound_total_ma_cost'             => 'decimal',
        'purchase_return_outbound_total_ma_cost'         => 'decimal',
        'stock_delivery_outbound_total_ma_cost'          => 'decimal',
        'stock_return_outbound_total_ma_cost'            => 'decimal',
        'sales_outbound_total_ma_cost'                   => 'decimal',
        'damage_outbound_total_ma_cost'                  => 'decimal',
        'adjustment_increase_total_ma_cost'              => 'decimal',
        'adjustment_decrease_total_ma_cost'              => 'decimal',
        'product_conversion_increase_total_ma_cost'      => 'decimal',
        'product_conversion_decrease_total_ma_cost'      => 'decimal',
        'auto_product_conversion_increase_total_ma_cost' => 'decimal',
        'auto_product_conversion_decrease_total_ma_cost' => 'decimal',
        'pos_sales_total_qty'                            => 'decimal',
        'pos_sales_total_amount'                         => 'decimal',
        'pos_sales_total_ma_cost'                        => 'decimal',
        'pos_sales_total_cost'                           => 'decimal',
    ];

    protected $fillable = [
        'created_for',
        'for_location',
        'transaction_date',
        'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(BranchDetail::class, 'created_for')->withTrashed();
    }
}