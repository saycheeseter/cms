<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\AuditableToProduct;

class ProductUnit extends DataModule
{
    use AuditableToProduct;

    protected $table = 'product_unit';

    public $timestamps = false;

    protected $touches = ['product'];

    protected $guarded = [
        'product_id',
    ];

    protected $casts = [
        'qty'        => 'decimal',
        'length'     => 'decimal',
        'width'      => 'decimal',
        'height'     => 'decimal',
        'weight'     => 'decimal',
        'default'    => 'int',
        'uom_id'     => 'int',
        'product_id' => 'int'
    ];

    public function uom()
    {
        return $this->belongsTo(UOM::class, 'uom_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relationships()
    {
        return [
            'uom' => [
                'class' => UOM::class,
                'relation' => 'uom'
            ]
        ];
    }

    public function generateTags(): array
    {
        return [
            'Product Unit',
        ];
    }
}
