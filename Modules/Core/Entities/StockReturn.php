<?php 

namespace Modules\Core\Entities;

use Modules\Core\Presenters\DeliveryOutboundPresenter;

class StockReturn extends Invoice
{
    protected $table = 'stock_return';

    public function details()
    {
        return $this->hasMany(StockReturnDetail::class, 'transaction_id');
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function deliverTo()
    {
        return $this->belongsTo(Branch::class, 'deliver_to')->withTrashed();
    }

    public function deliverToLocation()
    {
        return $this->belongsTo(BranchDetail::class, 'deliver_to_location')->withTrashed();
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.return.sheet.number.prefix');
    }

    public function presenter()
    {
        return new DeliveryOutboundPresenter($this);
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'deliver_to' => 'int',
            'deliver_to_location' => 'int',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(StockReturnOutbound::class, 'reference_id');
    }
}