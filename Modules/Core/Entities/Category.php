<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;

class Category extends DataModule implements ModuleSeries
{
    use SoftDeletes, HasModuleSeries, UserAudit {
        UserAudit::runSoftDelete insteadof SoftDeletes;
    }

    protected $table = 'category';
    
    protected $with = array('children');
    
    protected $fillable = [
        'parent_id',
        'code',
        'name'
    ];

    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'parent_id' => 'int'
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id')->withTrashed();
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountDetail::class, 'entity');
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}
