<?php 

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\PaymentDetailCheckManagement;
use Modules\Core\Traits\HasCollectionDetailProcess;

class CollectionDetail extends FinancialFlowDetail implements PaymentDetailCheckManagement
{
    use HasCollectionDetailProcess;

    protected $table = 'collection_detail';

    public function transaction()
    {
        return $this->belongsTo(Collection::class, 'transaction_id', 'id')->withTrashed();
    }

    public function check()
    {
        return $this->morphOne(ReceivedCheck::class, 'transactionable');
    }
}