<?php

namespace Modules\Core\Entities;

class OtherIncomeDetail extends OtherFinancialFlowDetail
{
    protected $table = 'other_income_detail';
    
    public function transaction()
    {
        return $this->belongsTo(OtherIncome::class, 'transaction_id', 'id')->withTrashed();
    }

    public function type()
    {
        return $this->belongsTo(PaymentType::class, 'type')->withTrashed();
    }
}
