<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Enums\Customer as CustomerEnum;
use Modules\Core\Presenters\SalesIOPresenter;
use Lang;
use Modules\Core\Traits\HasAutoProductConversionTransaction;
use Modules\Core\Traits\HasCurrentCostAsMAC;
use Modules\Core\Traits\HasPayableRemainingAmount;

class SalesReturnInbound extends InventoryChain implements TransactionMovingAverageCost
{
    use HasAutoProductConversionTransaction,
        HasCurrentCostAsMAC,
        HasPayableRemainingAmount;

    protected $table = 'sales_return_inbound';

    public function details()
    {
        return $this->hasMany(SalesReturnInboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(SalesReturn::class, 'reference_id')->withTrashed();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class, 'customer_detail_id')->withTrashed();
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function presenter()
    {
        return new SalesIOPresenter($this);
    }

    public function getCollectedAmountAttribute()
    {
        return $this->total_amount->sub($this->remaining_amount);
    }

    public function getMultiplierAttribute()
    {
        return $this->positive_multiplier;
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::AVAILABLE;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::SALES_RETURN_INBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.sales.return.inbound');
    }

    public function getBatchBaseOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(Customer::class);
    }

    public function getBatchBaseOwnerIdAssignmentAttribute()
    {
        return $this->customer_type === CustomerEnum::WALK_IN
            ? null
            : $this->customer_id;
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('sales.return.inbound.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'customer_id' => 'int',
            'customer_detail_id' => 'int',
            'salesman_id' => 'int',
            'term' => 'int',
            'customer_type' => 'int',
            'customer_walk_in_name' => 'string',
            'remaining_amount' => 'decimal'
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'sales_return_inbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'sales_return_inbound';
    }

    public function collections()
    {
        return $this->morphMany(CollectionReference::class, 'reference');
    }
}
