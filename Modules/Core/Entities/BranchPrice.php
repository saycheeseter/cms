<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\AuditableToProduct;

class BranchPrice extends DataModule
{
    use AuditableToProduct;

    protected $table = 'branch_price';

    public $timestamps = false;

    protected $touches = ['product'];

    protected $fillable = [
        'branch_id',
        'product_id',
        'purchase_price',
        'selling_price',
        'wholesale_price',
        'price_a',
        'price_b',
        'price_c',
        'price_d',
        'price_e',
        'editable',
        'copy_from'
    ];

     protected $casts = [
        'purchase_price'  => 'decimal',
        'selling_price'   => 'decimal',
        'wholesale_price' => 'decimal',
        'price_a'         => 'decimal',
        'price_b'         => 'decimal',
        'price_c'         => 'decimal',
        'price_d'         => 'decimal',
        'price_e'         => 'decimal',
        'editable'        => 'int',
        'branch_id'       => 'int',
        'product_id'      => 'int',
        'copy_from'       => 'int',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relationships()
    {
        return [
            'branch' => [
                'class' => Branch::class,
                'relation' => 'branch'
            ],
        ];
    }

    public function generateTags(): array
    {
        return [
            'Branch Price',
        ];
    }
}