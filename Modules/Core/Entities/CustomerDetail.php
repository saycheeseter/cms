<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerDetail extends DataModule
{
    use SoftDeletes;

    protected $table = 'customer_detail';

    protected $touches = ['group'];

    protected $guarded = [
        'id',
        'head_id'
    ];

    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'chinese_name' => 'string',
        'owner_name' => 'string',
        'contact' => 'string',
        'landline' => 'string',
        'fax' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'contact_person' => 'string',
        'tin' => 'string',
        'head_id' => 'int'
    ];

    public function group()
    {
        return $this->belongsTo(Customer::class, 'head_id', 'id')->withTrashed();
    }

    public function generateTags(): array
    {
        return [
            sprintf("Customer: %s", $this->group->name),
        ];
    }
}