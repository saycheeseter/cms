<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Enums\MemberPointType;
use Modules\Core\Traits\BranchAudit;

class MemberPointTransaction extends Base
{
    use SoftDeletes, BranchAudit;

    protected $table = 'member_point_transaction';
    
    protected $guarded = [
        'id',
        'deleted_at',
    ];

    protected $casts = [
        'amount' => 'decimal',
        'transaction_date' => 'datetime'
    ];

    public function setAmountAttribute($value)
    {     
        switch ($this->attributes['reference_type']) {
            case MemberPointType::SALES_USE_POINTS :
                $this->attributes['amount'] =  $value * -1;
                break;
            
            default:
                $this->attributes['amount'] =  $value;
                break;
        }
    }
}
