<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Traits\HasModuleSeries;

abstract class SystemCode extends DataModule implements ModuleSeries
{
    use SoftDeletes, HasModuleSeries;

    protected $table = 'system_code';

    protected $fillable = [
        'code',
        'name',
        'remarks'
    ];

    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'remarks' => 'string',
        'deletable' => 'int',
    ];

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('type_id', static::type());
        });
    }

    protected abstract static function type();
}
