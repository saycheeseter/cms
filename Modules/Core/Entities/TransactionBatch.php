<?php

namespace Modules\Core\Entities;

use Modules\Core\Extensions\Relations\MorphPivot;

class TransactionBatch extends MorphPivot
{
    protected $casts = [
        'qty' => 'decimal',
        'remaining_qty' => 'decimal'
    ];
}
