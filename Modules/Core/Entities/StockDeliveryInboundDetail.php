<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\HasPriceAsMovingAverageCost;

class StockDeliveryInboundDetail extends InventoryChainDetail
{
    use HasPriceAsMovingAverageCost;

    protected $table = 'stock_delivery_inbound_detail';

    public function transaction()
    {
        return $this->belongsTo(StockDeliveryInbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockDeliveryOutboundDetail::class, 'reference_id')->withTrashed();
    }
}