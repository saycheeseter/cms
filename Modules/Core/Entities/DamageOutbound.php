<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Presenters\DamageOutboundPresenter;
use Modules\Core\Enums\SerialTransaction;
use Lang;
use Modules\Core\Traits\HasCurrentCostAsMAC;

class DamageOutbound extends InventoryChain implements TransactionMovingAverageCost
{
    use HasCurrentCostAsMAC;

    protected $table = 'damage_outbound';

    public function details()
    {
        return $this->hasMany(DamageOutboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(Damage::class, 'reference_id')->withTrashed();
    }

    public function reason()
    {
        return $this->belongsTo(Reason::class, 'reason_id')->withTrashed();
    }

    public function presenter()
    {
        return new DamageOutboundPresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->negative_multiplier;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::DAMAGE_OUTBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.damage.outbound');
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return null;
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return null;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('damage.outbound.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'reason_id' => 'int'
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'damage_outbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'damage_outbound';
    }
}
