<?php

namespace Modules\Core\Entities;

class Damage extends Invoice
{
    protected $table = 'damage';

    public function details()
    {
        return $this->hasMany(DamageDetail::class, 'transaction_id');
    }

    public function reason()
    {
        return $this->belongsTo(Reason::class, 'reason_id')->withTrashed();
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('damage.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'reason_id' => 'int'
        ]);
    }

    public function imports()
    {
        return $this->hasMany(DamageOutbound::class, 'reference_id');
    }
}
