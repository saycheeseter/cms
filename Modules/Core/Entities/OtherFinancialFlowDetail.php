<?php

namespace Modules\Core\Entities;

use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

abstract class OtherFinancialFlowDetail extends Base implements AuditableInterface
{
    use Auditable;

    protected $timeStamps = false;

    protected $casts = [
        'type' => 'int',
        'transaction_id' => 'int',
        'amount' => 'decimal',
        'transaction_id' => 'int'
    ];

    protected $guarded = [
        'id'
    ];

    public function generateTags(): array
    {
        return [
            sprintf("Reference Name: %s", $this->transaction->reference_name),
        ];
    }

    abstract function transaction();
    abstract function type();
}
