<?php

namespace Modules\Core\Entities;

class DamageDetail extends InvoiceDetail
{
    protected $table = 'damage_detail';

    public function transaction()
    {
        return $this->belongsTo(Damage::class, 'transaction_id', 'id')->withTrashed();
    }
}
