<?php

namespace Modules\Core\Entities;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Traits\HasAdjustmentDetailCustomProcess;
use Modules\Core\Traits\HasPriceAsMovingAverageCost;

class InventoryAdjustDetail extends InventoryFlowDetail
{
    use HasPriceAsMovingAverageCost,
        HasAdjustmentDetailCustomProcess;

    protected $table = 'inventory_adjust_detail';
    
    protected $casts = [
        'transaction_id' => 'int',
        'unit_id' => 'int',
        'unit_qty' => 'decimal',
        'qty' => 'decimal',
        'price' => 'decimal',
        'cost' => 'decimal',
        'product_id' => 'int',
        'remarks' => 'string',
        'pc_qty' => 'decimal',
        'inventory' => 'decimal',
        'total_qty' => 'decimal',
        'added_inventory_value' => 'decimal',
        'ma_cost' => 'decimal'
    ];

    public function transaction()
    {
        return $this->belongsTo(InventoryAdjust::class, 'transaction_id')->withTrashed();
    }

    public function serials()
    {
        return $this->morphToMany(ProductSerialNumber::class, 'seriable', 'transaction_serial_number', 'seriable_id', 'serial_id');
    }
    
    public function getTotalQtyAttribute()
    {
        return $this->qty
            ->mul($this->unit_qty, setting('monetary.precision'))
            ->add($this->pc_qty, setting('monetary.precision'));
    }

    public function getAmountAttribute()
    {
        return $this->price->mul($this->diff_qty, setting('monetary.precision'));
    }

    public function getDiffQtyAttribute()
    {
        return $this->total_qty->sub($this->inventory);
    }

    public function getAddedInventoryValueAttribute()
    {
        return $this->diff_qty;
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return $this->process_flow;
    }

    public function getTotalMaCostAmountAttribute()
    {
        return $this->ma_cost->mul($this->diff_qty, setting('monetary.precision'));
    }

    public function getTotalCostAmountAttribute()
    {
        return $this->cost->mul($this->diff_qty, setting('monetary.precision'));
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $key = $this->process_flow;

        return [
            $key.'_total_qty' => 'diff_qty',
            $key.'_total_amount' => 'amount',
            $key.'_total_cost' => 'total_cost_amount',
            $key.'_total_ma_cost' => 'total_ma_cost_amount'
        ];
    }

    public function getProcessFlowAttribute()
    {
        $zero = Decimal::create(0, setting('monetary.precision'));

        return $this->added_inventory_value->isLessThan($zero)
            ? 'adjustment_decrease'
            : 'adjustment_increase';
    }
}
