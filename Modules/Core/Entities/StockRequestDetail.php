<?php

namespace Modules\Core\Entities;

class StockRequestDetail extends InvoiceDetail
{
    protected $table = 'stock_request_detail';

    public function transaction()
    {
        return $this->belongsTo(StockRequest::class, 'transaction_id', 'id')->withTrashed();
    }

    public function purchases()
    {
        return $this->morphMany(PurchaseDetail::class, 'transactionable');
    }
}
