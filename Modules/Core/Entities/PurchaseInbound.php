<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Modules\Core\Presenters\PurchasePresenter;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Lang;
use Modules\Core\Traits\HasPayableRemainingAmount;
use Modules\Core\Traits\HasPurchaseInboundProductBatchStatusUpdate;
use Modules\Core\Traits\HasPurchaseInboundSettingsProcess;
use Modules\Core\Traits\HasSupplierHistoricalTransaction;

class PurchaseInbound extends InventoryChain
{
    use HasPayableRemainingAmount,
        HasSupplierHistoricalTransaction,
        HasPurchaseInboundProductBatchStatusUpdate,
        HasPurchaseInboundSettingsProcess;

    protected $table = 'purchase_inbound';

    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = $value;

        $this->attributes['due_date'] = Carbon::parse($value)->addDays($this->term);
    }

    public function setTermAttribute($value)
    {
        $this->attributes['term'] = $value;

        if (!empty($this->transaction_date)) {
            $this->attributes['due_date'] = Carbon::parse($this->transaction_date)->addDays($value);
        }
    }

    public function details()
    {
        return $this->hasMany(PurchaseInboundDetail::class, 'transaction_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }

    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(Purchase::class, 'reference_id')->withTrashed();
    }

    public function payments()
    {
        return $this->morphMany(PaymentReference::class, 'reference');
    }

    public function getPaidAmountAttribute()
    {
        return $this->total_amount->sub($this->remaining_amount);
    }

    public function presenter()
    {
        return new PurchasePresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->positive_multiplier;
    }

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('purchase.inbound.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => PurchaseInboundDetail::class,
                    'relation' => 'details'
                ],
                'supplier' => [
                    'class' => Supplier::class,
                    'relation' => 'supplier'
                ],
                'payment_method' => [
                    'class' => PaymentMethod::class,
                    'relation' => 'payment'
                ],
                'reference' => [
                    'class' => Purchase::class,
                    'break' => true,
                    'relation' => 'reference'
                ],
            ]
        );
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::AVAILABLE;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::PURCHASE_INBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.purchase.inbound');
    }

    public function getBatchBaseOwnerTypeAssignmentAttribute()
    {
        return null;
    }

    public function getBatchBaseOwnerIdAssignmentAttribute()
    {
        return null;
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'supplier_id' => 'int',
            'payment_method' => 'int',
            'term' => 'int',
            'supplier_invoice_no' => 'string',
            'supplier_box_count' => 'string',
            'remaining_amount' => 'decimal',
            'transactionable_id' => 'int',
            'due_date' => 'datetime',
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'purchase_inbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'purchase_inbound';
    }
}