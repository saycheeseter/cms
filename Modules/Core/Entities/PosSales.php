<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;
use Modules\Core\Entities\Contracts\SeriableTransaction;
use Modules\Core\Entities\Contracts\TransactionMovingAverageCost;
use Modules\Core\Entities\Contracts\TransactionSummary;
use Modules\Core\Presenters\PosSalesPresenter;

class PosSales extends Base implements
    SeriableTransaction,
    TransactionMovingAverageCost,
    TransactionSummary,
    GenerateTransactionProductMonthlySummary
{
    use SoftDeletes;

    protected $table = 'pos_sales';

    protected $attributes = [
        'total_amount' => 0,
        'voided' => 0
    ];

    protected $casts = [
        'total_amount' => 'decimal',
        'transaction_date' => 'datetime',
        'voided' => 'int'
    ];

    protected $guarded = [
        'id',
    ];

    public function cashier()
    {
        return $this->belongsTo(User::class, 'cashier_id')->withTrashed();
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class, 'customer_detail_id')->withTrashed();
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id')->withTrashed();
    }

    public function details()
    {
        return $this->hasMany(PosSalesDetail::class, 'transaction_id');
    }

    public function collections()
    {
        return $this->hasMany(PosCollection::class, 'transaction_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function getMultiplierAttribute()
    {
        return Decimal::create(-1, setting('monetary.precision'));
    }

    public function getSerialOwnerTypeAssignmentAttribute()
    {
        return false;
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return false;
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return false;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return false;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.pos.sales');
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'pos_sales';
    }

    public function presenter()
    {
        return new PosSalesPresenter($this);
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->for_location;
    }

    public function getProductMonthlyTransactionProductIdColumn()
    {
        return 'product_id';
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction_date;
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        return [
            'pos_sales_total_qty' => 'total_qty',
            'pos_sales_total_amount' => 'amount',
            'pos_sales_total_cost' => 'total_cost_amount',
            'pos_sales_total_ma_cost' => 'total_ma_cost_amount'
        ];
    }
}