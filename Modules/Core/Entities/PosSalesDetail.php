<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Presenters\PosSalesDetailPresenter;

class PosSalesDetail extends Base 
{
    use SoftDeletes;
    
    protected $table = 'pos_sales_detail';

    protected $touches = ['transaction'];

    protected $casts = [
        'unit_qty' => 'decimal',
        'qty' => 'decimal',
        'oprice' => 'decimal',
        'price' => 'decimal',
        'discount1' => 'decimal',
        'discount2' => 'decimal',
        'discount3' => 'decimal',
        'discount4' => 'decimal',
        'vat' => 'decimal',
        'cost' => 'decimal',
        'ma_cost' => 'decimal',
        'group_type' => 'int',
        'product_type' => 'int'
    ];

    protected $guarded = [
        'id',
    ];

    public function transaction()
    {
        return $this->belongsTo(PosSales::class, 'transaction_id', 'id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function unit()
    {
        return $this->belongsTo(UOM::class, 'unit_id')->withTrashed();
    }

    public function getTotalQtyAttribute()
    {
        return $this->qty->mul($this->unit_qty, setting('monetary.precision'));
    }

    public function getAmountAttribute()
    {
        return $this->price->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getAddedInventoryValueAttribute()
    {
        return $this->total_qty;
    }

    public function serials()
    {
        return $this->morphToMany(ProductSerialNumber::class, 'seriable', 'transaction_serial_number', 'seriable_id', 'serial_id');
    }

    public function components()
    {
        return $this->belongsToMany(Product::class, 'pos_detail_components', 'pos_sales_detail_id', 'product_id')
            ->withPivot('qty', 'group_type')
            ->using(PosComponent::class);
    }

    public function barcodes()
    {
        return $this->hasMany(ProductBarcode::class, 'product_id', 'product_id');
    }

    public function autoConversion()
    {
        return $this->morphOne(AutoProductConversion::class, 'transaction');
    }

    public function presenter()
    {
        return new PosSalesDetailPresenter($this);
    }

    public function getTotalCostAmountAttribute()
    {
        return $this->cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getTotalMaCostAmountAttribute()
    {
        return $this->ma_cost->mul($this->total_qty, setting('monetary.precision'));
    }
}