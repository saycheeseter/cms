<?php

namespace Modules\Core\Entities;

class PurchaseReturn extends Invoice
{
    protected $table = 'purchase_return';

    public function details()
    {
        return $this->hasMany(PurchaseReturnDetail::class, 'transaction_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }
    
    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method')->withTrashed();
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('purchase.return.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'supplier_id' => 'int',
            'payment_method' => 'int',
            'term' => 'int',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(PurchaseReturnOutbound::class, 'reference_id');
    }
}
