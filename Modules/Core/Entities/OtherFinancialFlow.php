<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

abstract class OtherFinancialFlow extends Base implements AuditableInterface
{
    use SoftDeletes,
        Auditable,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }
    
    protected $casts = [
        'reference_name' => 'string',
        'remarks' => 'string',
        'transaction_date' => 'datetime',
        'payment_method' => 'int',
        'check_date' => 'datetime',
        'check_number' => 'string',
        'bank_account_id' => 'int',
    ];

    protected $guarded = [
        'id',
    ];

    public function method()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method', 'id')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function getTotalAmountAttribute()
    {
        $amount = Decimal::create(0, setting('monetary.precision'));

        foreach($this->details as $detail) {
            $amount = $amount->add($detail->amount);
        }

        return $amount;
    }

    abstract function details();
}