<?php

namespace Modules\Core\Entities;

class PermissionSection extends Base
{
    protected $table = 'permission_section';

    protected $guarded = [
        'id'
    ];

    public $timestamps = false;
    
    public function modules()
    {
        return $this->hasMany(PermissionModule::class, 'section_id');
    }
}
