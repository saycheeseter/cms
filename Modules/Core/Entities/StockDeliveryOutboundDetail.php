<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\ComputedDetailAttributes;
use Modules\Core\Traits\HasRemainingQtyAttribute;

class StockDeliveryOutboundDetail extends InventoryChainDetail
{
    use ComputedDetailAttributes, HasRemainingQtyAttribute;
    
    protected $table = 'stock_delivery_outbound_detail';

    public function transaction()
    {
        return $this->belongsTo(StockDeliveryOutbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockDeliveryDetail::class, 'reference_id')->withTrashed();
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'remaining_qty' => 'decimal'
        ]);
    }
}
