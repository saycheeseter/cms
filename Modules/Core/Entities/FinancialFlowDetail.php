<?php 

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

abstract class FinancialFlowDetail extends Base implements AuditableInterface
{
    use SoftDeletes, Auditable;

    protected $touches = ['transaction'];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'transaction_id' => 'int',
        'payment_method' => 'int',
        'amount' => 'decimal',
        'bank_account_id' => 'int',
        'check_no' => 'string',
        'check_date' => 'date'
    ];

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method')->withTrashed();
    }

    public function bank()
    {
        return $this->belongsTo(BankAccount::class, 'bank_id')->withTrashed();
    }

    public function generateTags(): array
    {
        return [
            sprintf("Sheet No: %s", $this->transaction->sheet_number),
        ];
    }
}