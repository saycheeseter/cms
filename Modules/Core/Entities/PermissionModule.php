<?php

namespace Modules\Core\Entities;

class PermissionModule extends Base
{
    protected $table = 'permission_module';

    protected $guarded = [
        'id'
    ];

    public $timestamps = false;
    
    public function permissions()
    {
        return $this->hasMany(Permission::class, 'module_id');
    }
}
