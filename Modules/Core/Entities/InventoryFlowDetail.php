<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\InventoryFlowDetailPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Traits\DetachesComponentsUponDeletion;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;
use App;

abstract class InventoryFlowDetail extends Base implements AuditableInterface
{
    use SoftDeletes,
        Auditable,
        DetachesComponentsUponDeletion;

    protected $touches = ['transaction'];

    protected $guarded = [
        'id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function unit()
    {
        return $this->belongsTo(UOM::class, 'unit_id')->withTrashed();
    }

    public function units()
    {
        return $this->hasMany(ProductUnit::class, 'product_id', 'product_id');
    }

    public function barcodes()
    {
        return $this->hasMany(ProductBarcode::class, 'product_id', 'product_id');
    }

    public function components()
    {
        return $this->morphToMany(Product::class, 'invoice_detail', 'invoice_component', 'invoice_detail_id', 'product_id')
            ->withPivot('qty', 'group_type')
            ->using(TransactionComponent::class);
    }

    public function presenter()
    {
        return new InventoryFlowDetailPresenter($this);
    }

    public function relationships()
    {   
        $head = $this->transaction()->getRelated()->getTable();

        return [
            'product' => [
                'class' => Product::class,
                'relation' => 'product',
                'exclusions' => ['branch_price', 'unit', 'branch_detail', 'branch_info', 'tax', 'barcode', 'branch_summary']
            ],
            'unit' => [
                'class' => UOM::class,
                'relation' => 'unit'
            ],
            'barcode' => [
                'class' => ProductBarcode::class,
                'relation' => 'barcodes',
                'joins' => [
                    ['local' => 'product_id', 'foreign' => 'product_id'],
                    ['local' => 'unit_id', 'foreign' => 'uom_id'],
                ]
            ],
            'product_branch_summary' => [
                'class' => ProductBranchSummary::class,
                'relation' => 'product',
                'joins' => [
                    ['local' => 'product_id', 'foreign' => 'product_id'],
                    ['table' => $head, 'local' => 'created_from', 'foreign' => 'branch_id'],
                    ['table' => $head, 'local' => 'for_location', 'foreign' => 'branch_detail_id']
                ]
            ],
            'product_branch_price' => [
                'class' => BranchPrice::class,
                'relation' => 'product',
                'joins' => [
                    ['local' => 'product_id', 'foreign' => 'product_id'],
                    ['table' => $head, 'local' => 'created_from', 'foreign' => 'branch_id']
                ]
            ]
        ];
    }

    public function generateTags(): array
    {
        return [
            sprintf("Sheet No: %s", $this->transaction->sheet_number),
        ];
    }

    abstract function getTotalQtyAttribute();
    abstract function getAmountAttribute();
    abstract function transaction();
}
