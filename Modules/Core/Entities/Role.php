<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\HasModuleSeries;

class Role extends Base implements ModuleSeries
{
    use SoftDeletes, HasModuleSeries;

    protected $table = 'role';

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'code' => 'string',
        'name' => 'string'
    ];

    public function branches()
    {
        return $this->hasMany(BranchRole::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}