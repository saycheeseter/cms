<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductMonthlySales extends Base
{
    protected $casts = [
        'stock_no' => 'string',
        'name' => 'string',
        'chinese_name' => 'string',
        'description' => 'string',
        'supplier' => 'string',
        'category' => 'string',
        'brand' => 'string',
        'total_quantity' => 'decimal',
        'total_amount' => 'decimal',
        'total_cost' => 'decimal',
        'total_profit' => 'decimal'
    ];
}