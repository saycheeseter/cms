<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class CollectionDueWarningSummary extends Base
{
    protected $casts = [
        'total_count' => 'decimal',
        'total_amount' => 'decimal',
        'total_remaining' => 'decimal'
    ];
}