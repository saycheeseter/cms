<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductTransactionList extends Base
{
    protected $casts = [
        'barcode' => 'string',
        'product' => 'string',
        'chinese_name' => 'string',
        'beginning' => 'decimal',
        'purchase_inbound' => 'decimal',
        'purchase_return_outbound' => 'decimal',
        'sales_outbound' => 'decimal',
        'sales_return_inbound' => 'decimal',
        'stock_delivery_inbound' => 'decimal',
        'stock_delivery_outbound' => 'decimal',
        'stock_return_inbound' => 'decimal',
        'adjustment_increase' => 'decimal',
        'stock_return_outbound' => 'decimal',
        'damage_outbound' => 'decimal',
        'adjustment_decrease' => 'decimal',
        'product_conversion_increase' => 'decimal',
        'product_conversion_decrease' => 'decimal',
        'auto_product_conversion_increase' => 'decimal',
        'auto_product_conversion_decrease' => 'decimal',
        'pos_sales' => 'decimal',
        'inventory' => 'decimal'
    ];
}