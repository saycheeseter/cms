<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductInventoryWarning extends Base
{
    protected $casts = [
        'reserved_qty' => 'decimal',
        'requested_qty' => 'decimal',
        'projected_inventory' => 'decimal',
        'min' => 'decimal',
        'max' => 'decimal',
        'inventory' => 'decimal',
        'product_name' => 'string',
    ];
}