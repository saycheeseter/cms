<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class BankTransactionSummary extends Base
{
    protected $casts = [
        'total' => 'decimal',
    ];
}