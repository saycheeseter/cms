<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class BrandMonthlySales extends Base
{
    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'remarks' => 'string',
        'total_qty' => 'decimal',
        'total_amount' => 'decimal',
        'total_profit' => 'decimal'
    ];
}