<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class CategoryMonthlySalesRanking extends Base
{
    protected $casts = [
        'category_code' => 'string',
        'category_name' => 'string',
        'total_qty' => 'decimal',
        'total_amount' => 'decimal',
        'total_profit' => 'decimal'
    ];
}