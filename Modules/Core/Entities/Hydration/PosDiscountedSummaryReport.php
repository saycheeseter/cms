<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class PosDiscountedSummaryReport extends Base
{
    protected $casts = [
        'terminal_number' => 'string',
        'transaction_date' => 'datetime',
        'or_number' => 'string',
        'cashier_name' => 'string',
        'category_name' => 'string',
        'product_barcode' => 'string',
        'product_name' => 'string',
        'total_qty' => 'decimal',
        'oprice' => 'decimal',
        'price' => 'decimal',
        'discounted_price' => 'decimal',
        'amount' => 'decimal',
        'discounted_amount' => 'decimal',
        'percentage_discount' => 'decimal',
    ];
}