<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class CollectionDueWarning extends Base
{
    protected $casts = [
        'id'                => 'int',
        'transaction_date'  => 'datetime',
        'customer'          => 'string',
        'sheet_number'      => 'string',
        'total_amount'      => 'decimal',
        'remaining_amount'  => 'decimal',
        'term'              => 'int',
        'due_date'          => 'datetime',
        'remarks'           => 'string'
    ];
}