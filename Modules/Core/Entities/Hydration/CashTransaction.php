<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class CashTransaction extends Base
{
    protected $casts = [
        'type' => 'string',
        'reference_id' => 'int',
        'transaction_date' => 'date',
        'particular' => 'string',
        'in' => 'decimal',
        'out' => 'decimal',
        'remarks' => 'string'
    ];
}