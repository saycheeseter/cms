<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class PosSalesTerminalReport extends Base
{
    protected $casts = [
        'transaction_date' => 'date'
    ];
}