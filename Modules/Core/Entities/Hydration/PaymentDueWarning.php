<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class PaymentDueWarning extends Base
{
    protected $casts = [
        'id'                => 'int',
        'transaction_date'  => 'datetime',
        'supplier_full_name'=> 'string',
        'sheet_number'      => 'string',
        'total_amount'      => 'decimal',
        'remaining_amount'  => 'decimal',
        'term'              => 'int',
        'remarks'           => 'string',
        'due_date'          => 'datetime'
    ];
}