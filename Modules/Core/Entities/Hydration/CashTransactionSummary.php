<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class CashTransactionSummary extends Base
{
    protected $casts = [
        'total' => 'decimal',
    ];
}