<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductInventoryValue extends Base
{
    protected $casts = [
        'barcode' => 'string',
        'name' => 'string',
        'chinese_name' => 'string',
        'supplier' => 'string',
        'inventory' => 'decimal',
        'branch' => 'string',
        'location' => 'string',
        'purchase_price'  => 'decimal',
        'purchase_inventory_value' => 'decimal',
        'current_cost_inventory_value' => 'decimal',
        'current_cost' => 'decimal',
    ];
}