<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class SalesmanReportTransactionsProducts extends Base
{
    protected $casts = [
        'product_barcode'    => 'string',
        'product_name'       => 'string',
        'total_qty'          => 'decimal',
        'minimum_price'      => 'decimal',
        'average_price'      => 'decimal',
        'maximum_price'      => 'decimal',
        'total_amount'       => 'decimal'
    ];
}