<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductPeriodicSales extends Base
{
    protected $casts = [
        'name' => 'string',
        'chinese_name' => 'string',
        'barcode' => 'string',
        'inventory' => 'decimal'
    ];
}