<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class IncomeStatement extends Base
{
    protected $casts = [
        'gross_sales' => 'decimal',
        'return_amount' => 'decimal',
        'cost_of_goods_sold' => 'decimal',
        'ma_cost_of_goods_sold' => 'decimal',
        'damage' => 'decimal',
        'adjustment_discrepancy' => 'decimal',
        'other_payment' => 'decimal',
        'other_income' => 'decimal',
    ];
}