<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;
use Modules\Core\Presenters\ProductPresenter;

class ProductPriceSummary extends Base
{
    protected $casts = [
        'id' => 'int',
        'stock_no' => 'string',
        'supplier_sku' => 'string',
        'description' => 'string',
        'name' => 'string',
        'chinese_name' => 'string',
        'memo' => 'string',
        'barcode' => 'string',
        'category' => 'string',
        'brand' => 'string',
        'supplier' => 'string',
        'purchase_price' => 'decimal',
        'selling_price' => 'decimal',
        'wholesale_price' => 'decimal',
        'price_a' => 'decimal',
        'price_b' => 'decimal',
        'price_c' => 'decimal',
        'price_d' => 'decimal',
        'price_e' => 'decimal',
        'inventory' => 'decimal',
        'group_type' => 'int',
    ];

    public function presenter()
    {
        return new ProductPresenter($this);
    }
}