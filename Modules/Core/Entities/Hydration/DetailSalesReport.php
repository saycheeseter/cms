<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;
use Modules\Core\Presenters\DetailSalesReportPresenter;

class DetailSalesReport extends Base
{
    protected $casts = [
        'created_from_name' => 'string',
        'created_for_name' => 'string',
        'for_location_name' => 'string',
        'transaction_date' => 'datetime',
        'sheet_number' => 'string',
        'reference_id' => 'string',
        'approval_status' => 'integer',
        'customer_type' => 'integer',
        'customer_name' => 'string',
        'customer_walk_in_name' => 'string',
        'total_amount' => 'decimal',
        'remaining_amount' => 'decimal',
        'remarks' => 'string',
        'salesman_name' => 'string',
        'term' => 'integer',
        'requested_by_name' => 'string',
        'created_by_name' => 'string',
        'created_at' => 'datetime',
        'audited_by_name' => 'string',
        'audited_date' => 'datetime',
        'deleted_at' => 'datetime',
        'stock_no' => 'string',
        'chinese_name' => 'string',
        'product_name' => 'string',
        'qty' => 'decimal',
        'unit_name' => 'string',
        'unit_qty' => 'decimal',
        'oprice' => 'decimal',
        'price' => 'decimal',
        'discount1' => 'decimal',
        'discount2' => 'decimal',
        'discount3' => 'decimal',
        'discount4' => 'decimal',
        'total_qty' => 'decimal',
        'detail_total_amount' => 'decimal',
        'detail_remarks' => 'string',
        'product_barcode' => 'string',
        'product_serial' => 'string',
        'product_batch' => 'string'
    ];

    public function presenter()
    {
        return new DetailSalesReportPresenter($this);
    }
}