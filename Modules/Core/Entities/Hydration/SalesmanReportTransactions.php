<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class SalesmanReportTransactions extends Base
{
    protected $casts = [
        'branch_name'        => 'string',
        'transaction_date'   => 'datetime',
        'sheet_number'       => 'string',
        'customer_name'      => 'string',
        'salesman_full_name' => 'string',
        'total_amount'       => 'decimal',
        'product_barcode'    => 'string',
        'product_name'       => 'string',
        'transaction_total_amount' => 'decimal'
    ];
}