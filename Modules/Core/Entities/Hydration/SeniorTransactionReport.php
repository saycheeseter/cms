<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class SeniorTransactionReport extends Base
{
    protected $casts = [
        'transaction_date' => 'date',
        'or_number' => 'string',
        'senior_number' => 'string',
        'senior_name' => 'string',
        'product_name' => 'string',
        'qty' => 'decimal',
        'price' => 'decimal',
    ];
}