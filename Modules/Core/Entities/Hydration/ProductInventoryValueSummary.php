<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductInventoryValueSummary extends Base
{
    protected $casts = [
        'positive_inventory'      => 'decimal',
        'negative_inventory'      => 'decimal',
        'positive_ma_cost_amount' => 'decimal',
        'negative_ma_cost_amount' => 'decimal',
        'positive_cost_amount'    => 'decimal',
        'negative_cost_amount'    => 'decimal'
    ];
}