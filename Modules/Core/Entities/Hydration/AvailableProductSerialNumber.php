<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class AvailableProductSerialNumber extends Base
{
    protected $casts = [
        'branch' => 'string',
        'supplier_name' => 'string',
        'location' => 'string',
        'serial_number' => 'string',
        'expiration_date' => 'date',
        'manufacturing_date' => 'date',
        'admission_date' => 'date',
        'manufacturer_warranty_start' => 'date',
        'manufacturer_warranty_end' => 'date',
        'remarks' => 'string',
    ];
}