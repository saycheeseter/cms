<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class DetailSalesReportSummary extends Base
{
    protected $casts = [
        'total_remaining_amount' => 'decimal',
        'total_qty' => 'decimal',
        'total_amount' => 'decimal',
    ];
}