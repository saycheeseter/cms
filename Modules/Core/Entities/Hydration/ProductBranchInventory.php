<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class ProductBranchInventory extends Base
{
    protected $casts = [
        'name' => 'string',
        'barcode' => 'string',
    ];
}