<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class SupplierMonthlySalesRanking extends Base
{
    protected $casts = [
        'supplier_code' => 'string',
        'supplier_full_name' => 'string',
        'total_qty' => 'decimal',
        'total_amount' => 'decimal',
        'total_profit' => 'decimal'
    ];
}