<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class SalesOutboundNegativeProfitProduct extends Base
{
    protected $casts = [
        'barcode' => 'string',
        'product' => 'string',
        'sheet_number' => 'string',
        'qty' => 'decimal',
        'cost' => 'decimal',
        'price' => 'decimal',
        'loss' => 'decimal'
    ];
}