<?php

namespace Modules\Core\Entities\Hydration;

use Modules\Core\Entities\Base;

class BankTransactionReport extends Base
{
    protected $casts = [
        'type' => 'string',
        'reference_id' => 'int',
        'check_date' => 'date',
        'particular' => 'string',
        'in' => 'decimal',
        'out' => 'decimal'
    ];
}