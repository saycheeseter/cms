<?php

namespace Modules\Core\Entities;

class PurchaseDetail extends InvoiceDetail 
{
    protected $table = 'purchase_detail';

    public function transaction()
    {
        return $this->belongsTo(Purchase::class, 'transaction_id', 'id')->withTrashed();
    }

    public function transactionable()
    {
        return $this->morphTo();
    }
}