<?php

namespace Modules\Core\Entities;

class ProductUnitBarcode
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
       return $this->product->hasMany(ProductBarcode::class);
    }

    public function byUnit(UOM $uom)
    {
        return $this->all()->where('uom_id', '=', $uom->id);
    }

    public function __get($name)
    {
        if (method_exists(self::class, $name)) {
            return call_user_func(array(self::class, $name))->get();
        }
    }
}
