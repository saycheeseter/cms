<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;
use Modules\Core\Presenters\SupplierPresenter;

class Supplier extends DataModule implements ModuleSeries
{
    use SoftDeletes,
        HasModuleSeries,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $attributes = [
        'term' => 0,
        'status' => 0
    ];
    
    protected $table = 'supplier';
    
    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'deleted_at',
        'created_at',
        'modified_at'
    ];

    protected $casts = [
        'code' => 'string',
        'full_name' => 'string',
        'chinese_name' => 'string',
        'owner_name' => 'string',
        'contact' => 'string',
        'address' => 'string',
        'landline' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'mobile' => 'string',
        'email' => 'string',
        'website' => 'string',
        'contact_person' => 'string',
        'memo' => 'string',
        'term' => 'int',
        'status' => 'int'
    ];

    public function serials()
    {
        return $this->morphMany(ProductSerialNumber::class, 'ownable');
    }

    public function batches()
    {
        return $this->morphMany(ProductBatchOwner::class, 'ownable');
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountDetail::class, 'entity');
    }

    public function products()
    {
        return $this->hasMany(ProductSupplier::class, 'supplier_id')->withPrimaryKey(['supplier_id', 'product_id']);
    }
    
    public function presenter()
    {
        return new SupplierPresenter($this);
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}
