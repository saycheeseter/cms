<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Builder;

class BranchSetting extends Setting
{
    protected $table = 'branch_settings';

    protected $fillable = [
        'branch_id',
        'key',
        'value',
        'comments',
        'type'
    ];

    protected $casts = [
        'key' => 'string',
        'comments' => 'string',
        'type' => 'string',
        'branch_id' => 'int'
    ];
}
