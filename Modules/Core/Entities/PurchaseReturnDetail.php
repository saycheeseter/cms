<?php

namespace Modules\Core\Entities;

class PurchaseReturnDetail extends InvoiceDetail 
{
    protected $table = 'purchase_return_detail';

    public function transaction()
    {
        return $this->belongsTo(PurchaseReturn::class, 'transaction_id', 'id')->withTrashed();
    }
}