<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Traits\BranchAudit;

class BankAccount extends DataModule
{
    use SoftDeletes, BranchAudit;

    protected $table = 'bank_account';

    protected $fillable = [
        'bank_id',
        'account_name',
        'account_number',
        'opening_balance'
    ];

    protected $attributes = [
        'opening_balance' => 0
    ];

    protected $casts = [
        'bank_id' => 'int',
        'opening_balance' => 'decimal',
        'account_name' => 'string',
        'account_number' => 'string',
    ];
}
