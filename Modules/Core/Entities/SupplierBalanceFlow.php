<?php 

namespace Modules\Core\Entities;

use Modules\Core\Presenters\SupplierBalanceFlowPresenter;

class SupplierBalanceFlow extends BalanceFlow
{
    protected $table = 'supplier_balance_flow';

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }

    public function presenter()
    {
        return new SupplierBalanceFlowPresenter($this);
    }
}