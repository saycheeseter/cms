<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class DataCollectorTransactionDetail extends Base
{
    use SoftDeletes;

    protected $table = 'data_collector_transaction_detail';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'transaction_id' => 'integer',
        'product_id' => 'integer',
        'qty' => 'decimal',
        'price' => 'decimal',
        'unit_id' => 'integer',
        'unit_qty' => 'decimal'
    ];

    public function transaction()
    {
        return $this->belongsTo(DataCollectorTransaction::class, 'transaction_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}