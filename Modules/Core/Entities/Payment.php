<?php 

namespace Modules\Core\Entities;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Traits\HasPaymentProcess;

class Payment extends FinancialFlow
{
    use HasPaymentProcess;

    protected $table = 'payment';

    public function details()
    {
        return $this->hasMany(PaymentDetail::class, 'transaction_id');
    }

    public function references()
    {
        return $this->hasMany(PaymentReference::class, 'transaction_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id')->withTrashed();
    }
    
    public function flow()
    {
        return $this->morphMany(SupplierBalanceFlow::class, 'transactionable');
    }

    public function getTotalPayableAttribute()
    {
        $total = Decimal::create(0, setting('monetary.precision'));

        $references = $this->references;

        foreach ($references as $reference) {
            $total = $total->add($reference->payable);
        }

        return $total;
    }

    public function getTotalExcessAmountAttribute()
    {
        return $this->total_amount->sub($this->total_payable);
    }

    public function getCheckTransactionableTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(PaymentDetail::class);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('payment.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'supplier_id' => 'int',
        ]);
    }
}