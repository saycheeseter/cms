<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\TransactionReference;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Presenters\TransactionPresenter;
use Modules\Core\Entities\Contracts\CustomizableReport;
use Litipk\BigNumbers\Decimal;
use Bnb\Laravel\Attachments\HasAttachment;
use Modules\Core\Traits\ApprovalAudit;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;
use Modules\Core\Entities\Contracts\BranchModuleSeries;
use Modules\Core\Traits\HasBranchModuleSeries;
use Modules\Core\Traits\HasTransactionSheetNumber;

abstract class InventoryFlow extends Base implements
    TransactionReference,
    CustomizableReport,
    AuditableInterface,
    BranchModuleSeries
{
    use SoftDeletes,
        HasAttachment,
        Auditable,
        HasBranchModuleSeries,
        HasTransactionSheetNumber,
        ApprovalAudit,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $attributes = [
        'approval_status' => ApprovalStatus::DRAFT,
        'total_amount' => 0
    ];

    protected $casts = [
        'audited_by' => 'int',
        'requested_by' => 'int',
        'created_for' => 'int',
        'sheet_number' => 'string',
        'remarks' => 'string',
        'transaction_date' => 'datetime',
        'audited_date' => 'datetime',
        'approval_status' => 'int',
        'total_amount' => 'decimal',
        'for_location' => 'int',
    ];

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function auditor()
    {
        return $this->belongsTo(User::class, 'audited_by')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function requested()
    {
        return $this->belongsTo(User::class, 'requested_by')->withTrashed();
    }

    public function presenter()
    {
        return new TransactionPresenter($this);
    }

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function getTotalQtyAttribute()
    {
        $total_qty = Decimal::create(0, setting('monetary.precision'));

        foreach($this->details as $detail) {
            $total_qty = $total_qty->add($detail->total_qty);
        }

        return $total_qty;
    }

    public function getIsDeletedAttribute()
    {
        return $this->trashed();
    }

    public function relationships()
    {
        return [
            'created_by' => [
                'class' => User::class,
                'relation' => 'creator'
            ],
            'modified_by' => [
                'class' => User::class,
                'relation' => 'modifier'
            ],
            'audited_by' => [
                'class' => User::class,
                'relation' => 'auditor'
            ],
            'requested_by' => [
                'class' => User::class,
                'relation' => 'requested'
            ],
            'created_for' => [
                'class' => Branch::class,
                'relation' => 'for'
            ],
            'created_from' => [
                'class' => Branch::class,
                'relation' => 'from'
            ],
        ];
    }

    public function getSeriesColumnAttribute()
    {
        return 'sheet_number';
    }

    abstract function details();
}
