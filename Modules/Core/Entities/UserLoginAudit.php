<?php

namespace Modules\Core\Entities;

class UserLoginAudit extends Base
{
    public $table = 'user_login_audit';

    protected $fillable = [
        'ip_address',
        'user_agent'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}