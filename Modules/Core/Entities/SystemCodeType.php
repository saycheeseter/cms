<?php

namespace Modules\Core\Entities;

class SystemCodeType extends Base
{
    protected $table = 'system_code_type';

    protected $fillable = [
        'name'
    ];

    public $timestamps  = false;
}
