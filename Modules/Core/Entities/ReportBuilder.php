<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Base;
use Modules\Core\Presenters\ReportBuilderPresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\GenerateNewPermissionFromReportBuilder;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class ReportBuilder extends Base implements AuditableInterface
{   
    use Auditable,
        UserAudit,
        BranchAudit,
        GenerateNewPermissionFromReportBuilder;
    
    protected $table = 'report_builder';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'updated_at'
    ];

    protected $casts = [
        'group_id' => 'int',
        'name' => 'string',
        'slug' => 'string',
        'created_from' => 'int',
        'format' => 'object',
    ];

    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function userModified()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function presenter()
    {
        return new ReportBuilderPresenter($this);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value, '-');
        $this->attributes['name'] = $value;
    }

    public function getExcelFormatAttribute()
    {   
        return $this->format->excel;
    }
}