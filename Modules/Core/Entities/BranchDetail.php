<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Traits\HasBranchDetailInitialDataProcess;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;

class BranchDetail extends DataModule implements ModuleSeries
{
    use SoftDeletes,
        HasModuleSeries,
        HasBranchDetailInitialDataProcess,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'branch_detail';

    protected $touches = ['branch'];

    protected $guarded = [
        'id',
        'deleted_at',
        'created_by',
        'modified_by'
    ];

    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'address' => 'string',
        'contact' => 'string',
        'business_name' => 'string',
    ];
    
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }

    public function generateTags(): array
    {
        return [
            sprintf("Head: %s", $this->branch->name),
        ];
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }
}
