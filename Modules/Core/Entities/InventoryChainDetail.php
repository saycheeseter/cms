<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionDetailRemainingReference;
use Modules\Core\Presenters\InventoryChainDetailPresenter;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Traits\DetachesItemManagementUponDeletion;
use Modules\Core\Traits\HasTransactionTotalAmountComputation;

abstract class InventoryChainDetail extends InventoryFlowDetail implements TransactionDetailRemainingReference
{
    use DetachesItemManagementUponDeletion,
        HasTransactionTotalAmountComputation;

    protected $casts = [
        'transaction_id' => 'int',
        'unit_id' => 'int',
        'unit_qty' => 'decimal',
        'qty' => 'decimal',
        'oprice' => 'decimal',
        'price' => 'decimal',
        'cost' => 'decimal',
        'discount1' => 'decimal',
        'discount2' => 'decimal',
        'discount3' => 'decimal',
        'discount4' => 'decimal',
        'remaining_qty' => 'decimal',
        'product_id' => 'int',
        'remarks' => 'string',
        'added_inventory_value' => 'decimal',
        'refererence_id' => 'int',
        'ma_cost' => 'decimal',
    ];

    public function presenter()
    {
        return new InventoryChainDetailPresenter($this);
    }

    public function components()
    {
        return $this->morphToMany(Product::class, 'inventory_chain_detail', 'inventory_chain_component', 'inventory_chain_detail_id', 'product_id')
            ->withPivot('qty', 'group_type')
            ->using(TransactionComponent::class);
    }

    public function getTotalQtyAttribute()
    {
        return $this->qty->mul($this->unit_qty, setting('monetary.precision'));
    }

    public function getAmountAttribute()
    {
        return $this->price->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getSpareQtyAttribute()
    {
        return ($this->transaction_status == ApprovalStatus::APPROVED)
            ? $this->reference->remaining_qty
            : $this->reference->remaining_qty->sub($this->total_qty, setting('monetary.precision'));
    }

    public function getAddedInventoryValueAttribute()
    {
        return $this->total_qty;
    }

    public function serials()
    {
        return $this->morphToMany(ProductSerialNumber::class, 'seriable', 'transaction_serial_number', 'seriable_id', 'serial_id');
    }
    
    public function batches()
    {
        return $this->morphToMany(ProductBatch::class, 'transactionable', 'transaction_batch', 'transactionable_id', 'batch_id')
            ->withPivot('qty', 'remaining_qty')
            ->using(TransactionBatch::class);
    }

    public function getTotalMaCostAmountAttribute()
    {
        return $this->ma_cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function getTotalCostAmountAttribute()
    {
        return $this->cost->mul($this->total_qty, setting('monetary.precision'));
    }

    public function autoConversion()
    {
        return $this->morphOne(AutoProductConversion::class, 'transaction');
    }

    abstract function reference();
}
