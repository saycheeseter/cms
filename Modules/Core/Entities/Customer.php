<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\HasCustomerSummaryDataGeneration;
use Modules\Core\Traits\HasModuleSeries;
use Modules\Core\Traits\UserAudit;
use Modules\Core\Presenters\CustomerPresenter;

class Customer extends DataModule implements ModuleSeries
{
    use SoftDeletes,
        HasModuleSeries,
        BranchAudit,
        HasCustomerSummaryDataGeneration,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $table = 'customer';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by'
    ];

    protected $attributes = [
        'credit_limit' => 0,
        'pricing_type' => 0,
        'historical_change_revert' => 1,
        'historical_default_price' => 0,
        'pricing_rate' => 0,
        'salesman_id' => 0,
        'term' => 0,
        'status' => 0
    ];

    protected $casts = [
        'credit_limit' => 'decimal',
        'pricing_rate' => 'decimal',
        'name' => 'string',
        'code' => 'string',
        'memo' => 'string',
        'pricing_type' => 'int',
        'historical_change_revert' => 'int',
        'historical_default_price' => 'int',
        'website' => 'string',
        'term' => 'int',
        'status' => 'int',
        'salesman_id' => 'int',
    ];

    public function details()
    {
        return $this->hasMany(CustomerDetail::class, 'head_id');
    }

    public function sales()
    {
        return $this->hasMany(SalesOutbound::class);
    }

    public function relationships()
    {
        return [
            'detail' => [
                'class' => CustomerDetail::class,
                'relation' => 'details'
            ],
        ];
    }

    public function serials()
    {
        return $this->morphMany(ProductSerialNumber::class, 'ownable');
    }

    public function batches()
    {
        return $this->morphMany(ProductBatchOwner::class, 'ownable');
    }

    public function discounts()
    {
        return $this->morphMany(ProductDiscountTarget::class, 'target');
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function getSeriesColumnAttribute()
    {
        return 'code';
    }

    public function credit()
    {
        return $this->hasOne(CustomerCredit::class, 'customer_id');
    }

    public function presenter()
    {
        return new CustomerPresenter($this);
    }
}