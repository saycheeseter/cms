<?php

namespace Modules\Core\Entities;

use Modules\Core\Extensions\Relations\Pivot;

class PosComponent extends Pivot
{
    protected $casts = [
        'qty' => 'decimal',
        'group_type' => 'int'
    ];
}