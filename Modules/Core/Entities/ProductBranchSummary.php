<?php

namespace Modules\Core\Entities;

class ProductBranchSummary extends DataModule
{
    protected $table = 'product_branch_summary';

    public $timestamps = false;

    protected $primaryKey = 'product_id';

    protected $touches = ['product'];

    protected $fillable = [
        'branch_id',
        'branch_detail_id',
        'product_id',
        'qty',
    ];

    protected $casts = [
        'qty' => 'decimal',
        'reserved_qty' => 'decimal',
        'requested_qty' => 'decimal',
        'inventory' => 'decimal',
        'inventory_value' => 'decimal',
        'current_cost' => 'decimal',
        'branch_id' => 'int',
        'product_id' => 'int',
        'branch_detail_id' => 'int',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }
}
