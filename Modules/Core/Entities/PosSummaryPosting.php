<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\PosSummaryPostingStatus;
use Modules\Core\Presenters\PosSummaryPostingPresenter;
use Modules\Core\Traits\UserAudit;

class PosSummaryPosting extends Base
{
    use UserAudit;

    protected $attributes = [
        'status' => PosSummaryPostingStatus::IDLE
    ];

    protected $casts = [
        'created_by' => 'int',
        'modified_by' => 'int',
        'branch_id' => 'int',
        'terminal_number' => 'int',
        'status' => 'int',
        'last_post_date' => 'datetime',
    ];

    protected $fillable = [
        'branch_id',
        'terminal_number',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function presenter()
    {
        return new PosSummaryPostingPresenter($this);
    }
}