<?php

namespace Modules\Core\Entities;

class CashDenomination extends Base
{
    protected $table = 'cash_denomination';

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'create_date' => 'datetime'
    ];
}
