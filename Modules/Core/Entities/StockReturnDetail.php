<?php 

namespace Modules\Core\Entities;

class StockReturnDetail extends InvoiceDetail
{
    protected $table = 'stock_return_detail';

    public function transaction()
    {
        return $this->belongsTo(StockReturn::class, 'transaction_id', 'id')->withTrashed();
    }
}