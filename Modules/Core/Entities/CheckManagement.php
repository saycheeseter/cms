<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\CheckManagementTransformer;
use Modules\Core\Presenters\CheckManagementPresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;
use Modules\Core\Enums\CheckStatus;

abstract class CheckManagement extends Base implements
    AuditableInterface,
    CheckManagementTransformer
{
    use SoftDeletes,
        Auditable,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
    ];

    protected $casts = [
        'created_from' => 'int',
        'transactionable_id' => 'int',
        'transaction_date' => 'date',
        'bank_account_id' => 'int',
        'check_no' => 'string',
        'check_date' => 'date',
        'transfer_date' => 'date',
        'amount' => 'decimal',
        'remarks' => 'string',
        'status' => 'int'
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function bank()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id')->withTrashed();
    }

    public function getRedirectIdAttribute()
    {
        if($this->transactionable_type == $this->getMorphMapKey(PaymentDetail::class) 
            || $this->transactionable_type == $this->getMorphMapKey(CollectionDetail::class)
        ) {
            return $this->transactionable->transaction->id;
        }

        return $this->transactionable_id;
    }

    public function presenter()
    {
        return new CheckManagementPresenter($this);
    }
}