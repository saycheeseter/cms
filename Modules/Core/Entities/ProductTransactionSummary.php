<?php

namespace Modules\Core\Entities;

use Litipk\BigNumbers\Decimal;

class ProductTransactionSummary extends Base
{
    protected $table = 'product_transaction_summary';

    protected $guarded = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'purchase_inbound' => 'decimal',
        'stock_delivery_inbound' => 'decimal',
        'stock_return_inbound' => 'decimal',
        'sales_return_inbound' => 'decimal',
        'adjustment_increase' => 'decimal',
        'purchase_return_outbound' => 'decimal',
        'stock_delivery_outbound' => 'decimal',
        'stock_return_outbound' => 'decimal',
        'sales_outbound' => 'decimal',
        'damage_outbound' => 'decimal',
        'adjustment_decrease' => 'decimal',
        'beginning' => 'decimal',
        'product_conversion_increase' => 'decimal',
        'product_conversion_decrease' => 'decimal',
        'auto_product_conversion_increase' => 'decimal',
        'auto_product_conversion_decrease' => 'decimal',
        'pos_sales' => 'decimal',
    ];

    protected $attributes = [
        'purchase_inbound' => 0,
        'stock_delivery_inbound' => 0,
        'stock_return_inbound' => 0,
        'sales_return_inbound' => 0,
        'adjustment_increase' => 0,
        'purchase_return_outbound' => 0,
        'stock_delivery_outbound' => 0,
        'stock_return_outbound' => 0,
        'sales_outbound' => 0,
        'damage_outbound' => 0,
        'adjustment_decrease' => 0,
        'product_conversion_increase' => 0,
        'product_conversion_decrease' => 0,
        'auto_product_conversion_increase' => 0,
        'auto_product_conversion_decrease' => 0,
        'pos_sales' => 0,
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function location()
    {
         return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function getTotalAttribute()
    {
        $total = Decimal::create(0, setting('monetary.decision'));

        $total = $total->add($this->purchase_inbound)
            ->add($this->stock_delivery_inbound)
            ->add($this->stock_return_inbound)
            ->add($this->sales_return_inbound)
            ->add($this->adjustment_increase)
            ->add($this->product_conversion_increase)
            ->add($this->auto_product_conversion_increase)
            ->sub($this->purchase_return_outbound)
            ->sub($this->stock_delivery_outbound)
            ->sub($this->stock_return_outbound)
            ->sub($this->sales_outbound)
            ->sub($this->damage_outbound)
            ->sub($this->adjustment_decrease)
            ->sub($this->product_conversion_decrease)
            ->sub($this->auto_product_conversion_decrease);

        return $total;
    }
}