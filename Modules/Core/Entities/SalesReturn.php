<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\SalesPresenter;

class SalesReturn extends Invoice
{
    protected $table = 'sales_return';

    public function details()
    {
        return $this->hasMany(SalesReturnDetail::class, 'transaction_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class, 'customer_detail_id')->withTrashed();
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id')->withTrashed();
    }

    public function presenter()
    {
        return new SalesPresenter($this);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('sales.return.sheet.number.prefix');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'customer_id' => 'int',
            'customer_detail_id' => 'int',
            'salesman_id' => 'int',
            'term' => 'int',
            'customer_type' => 'int',
            'customer_walk_in_name' => 'string',
        ]);
    }

    public function imports()
    {
        return $this->hasMany(SalesReturnInbound::class, 'reference_id');
    }
}
