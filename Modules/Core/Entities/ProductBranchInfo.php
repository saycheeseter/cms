<?php 

namespace Modules\Core\Entities;

use Modules\Core\Traits\AuditableToProduct;

class ProductBranchInfo extends DataModule
{
    use AuditableToProduct;

    protected $table = 'product_branch_info';

    public $timestamps = false;

    protected $guarded = [
        'product_id',
    ];

    protected $touches = ['product'];

    protected $casts = [
        'status' => 'int',
        'product_id' => 'int',
        'branch_id' => 'int'
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relationships()
    {
        return [
            'branch' => [
                'class' => Branch::class,
                'relation' => 'branch'
            ]
        ];
    }

    public function generateTags(): array
    {
        return [
            'Product Branch Info',
        ];
    }
}