<?php

namespace Modules\Core\Entities;

abstract class ProductEntityLog extends Base
{
    protected $casts = [
        'latest_unit_qty' => 'decimal',
        'highest_price' => 'decimal',
        'lowest_price' => 'decimal',
        'latest_price' => 'decimal',
        'latest_fields_updated_date' => 'datetime',
    ];

    protected $guarded = [
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function unit()
    {
        return $this->belongsTo(UOM::class, 'latest_unit_id')->withTrashed();
    }
}
