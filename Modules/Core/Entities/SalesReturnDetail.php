<?php

namespace Modules\Core\Entities;

class SalesReturnDetail extends InvoiceDetail 
{
    protected $table = 'sales_return_detail';

    public function transaction()
    {
        return $this->belongsTo(SalesReturn::class, 'transaction_id', 'id')->withTrashed();
    }
}