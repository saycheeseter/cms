<?php

namespace Modules\Core\Entities;

class PurchaseReturnOutboundDetail extends InventoryChainDetail
{
    protected $table = 'purchase_return_outbound_detail';

    public function transaction()
    {
        return $this->belongsTo(PurchaseReturnOutbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(PurchaseReturnDetail::class, 'reference_id')->withTrashed();
    }
}
