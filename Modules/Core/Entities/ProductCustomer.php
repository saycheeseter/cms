<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use DateTime;

class ProductCustomer extends ProductEntityLog
{
    protected $table = 'product_customer';

    protected $casts = [
        'latest_unit_qty' => 'decimal',
        'latest_price' => 'decimal',
        'lowest_price' => 'decimal',
        'highest_price' => 'decimal',
        'latest_fields_updated_date' => 'datetime',
        'latest_unit_id' => 'int',
        'customer_id' => 'int',
        'product_id' => 'int',
        'remarks' => 'string',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function setLatestFieldsUpdatedDateAttribute($value)
    {
        $this->attributes['latest_fields_updated_date'] = $value instanceof DateTime
            ? Carbon::instance($value)
            : Carbon::parse($value);
    }
}
