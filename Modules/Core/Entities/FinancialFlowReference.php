<?php 

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

abstract class FinancialFlowReference extends Base 
{
    use SoftDeletes;

    protected $touches = ['transaction'];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'transaction_id' => 'int',
        'reference_id' => 'int',
        'reference_type' => 'string',
    ];
}