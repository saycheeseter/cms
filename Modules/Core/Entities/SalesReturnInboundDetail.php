<?php

namespace Modules\Core\Entities;

class SalesReturnInboundDetail extends InventoryChainDetail
{
    protected $table = 'sales_return_inbound_detail';

    public function transaction()
    {
        return $this->belongsTo(SalesReturnInbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(SalesReturnDetail::class, 'reference_id')->withTrashed();
    }

    public function autoConversion()
    {
        return $this->morphOne(AutoProductConversion::class, 'transaction');
    }
}
