<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\ComputedDetailAttributes;
use Modules\Core\Traits\HasRemainingQtyAttribute;

class StockReturnOutboundDetail extends InventoryChainDetail
{
    use ComputedDetailAttributes, HasRemainingQtyAttribute;
    
    protected $table = 'stock_return_outbound_detail';
    
    public function transaction()
    {
        return $this->belongsTo(StockReturnOutbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockReturnDetail::class, 'reference_id')->withTrashed();
    }
}
