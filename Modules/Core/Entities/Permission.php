<?php

namespace Modules\Core\Entities;

class Permission extends Base
{
    protected $table = 'permission';
    
    protected $guarded = [
        'id'
    ];

    protected $hidden = ['pivot'];
    
    public $timestamps  = false;

    protected $touches = ['users'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'branch_permission_user', 'permission_id', 'user_id')->withPivot('branch_id');
    }
}