<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Base;

class Config extends Base
{
    protected $table = 'config';
    
    protected $fillable = [];

    public $timestamps  = false;
}
