<?php

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;
use Modules\Core\Entities\Contracts\TransactionRemainingReference;
use Modules\Core\Entities\Contracts\TransactionSummary;
use Modules\Core\Presenters\InventoryChainPresenter;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Entities\Contracts\SeriableTransaction;
use Modules\Core\Entities\Contracts\BatchTransaction;
use Modules\Core\Traits\GenerateTransactionProductMonthlySummary as GenerateTransactionProductMonthlySummaryTrait;
use Modules\Core\Traits\HasBatchLogs;
use Modules\Core\Traits\HasSerialNumberLogs;
use Modules\Core\Traits\HasInventoryAndManagementProcess;
use Modules\Core\Traits\HasReferenceRemainingQtyProcess;

abstract class InventoryChain extends InventoryFlow implements
    SeriableTransaction,
    BatchTransaction,
    TransactionRemainingReference,
    TransactionSummary,
    GenerateTransactionProductMonthlySummary
{
    use GenerateTransactionProductMonthlySummaryTrait,
        HasSerialNumberLogs,
        HasBatchLogs,
        HasInventoryAndManagementProcess,
        HasReferenceRemainingQtyProcess;

    protected $casts = [
        'audited_by' => 'int',
        'requested_by' => 'int',
        'created_for' => 'int',
        'sheet_number' => 'string',
        'remarks' => 'string',
        'transaction_date' => 'datetime',
        'audited_date' => 'datetime',
        'approval_status' => 'int',
        'transaction_status' => 'int',
        'total_amount' => 'decimal',
        'for_location' => 'int',
        'reference_id' => 'int',
        'transaction_type' => 'int',
        'created_from' => 'int',
        'created_by' => 'int',
    ];

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function presenter()
    {
        return new InventoryChainPresenter($this);
    }

    public function getPositiveMultiplierAttribute()
    {
        return Decimal::create(1, setting('monetary.precision'));
    }

    public function getNegativeMultiplierAttribute()
    {
        return Decimal::create(-1, setting('monetary.precision'));
    }
    
    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [
                "for_location" => [
                    'class' => BranchDetail::class,
                    'relation' => 'location'
                ],
            ]
        );
    }

    public function getSerialOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::NOT_AVAILABLE;
    }
    
    public function getBatchBaseOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getBatchBaseOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getBatchBaseOwnerQtyAttribute()
    {
        return 'current_qty';
    }

    public function getBatchOwnerQtyAttribute()
    {
        return 'current_qty';
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->for_location;
    }

    public function getProductMonthlyTransactionProductIdColumn()
    {
        return 'product_id';
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction_date;
    }

    public function getProductMonthlyTransactionAttributeMapping()
    {
        $prefix = $this->getProductMonthlyTransactionAttributeMappingPrefix();

        return [
            $prefix.'_total_qty' => 'total_qty',
            $prefix.'_total_amount' => 'amount',
            $prefix.'_total_cost' => 'total_cost_amount',
            $prefix.'_total_ma_cost' => 'total_ma_cost_amount'
        ];
    }

    public function posting()
    {
        return $this->morphOne(TransactionSummaryPosting::class, 'transaction');
    }

    abstract public function reference();
    abstract public function getMultiplierAttribute();
    abstract public function getProductMonthlyTransactionAttributeMappingPrefix();
}
