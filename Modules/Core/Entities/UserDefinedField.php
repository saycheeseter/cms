<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Enums\UserDefinedFieldType;
use Carbon\Carbon;
use App;
use Modules\Core\Traits\MigratesColumnFromUDF;

class UserDefinedField extends Base
{
    use MigratesColumnFromUDF;

    protected $table = 'user_defined_fields';

    public $timestamps = false;
    
    protected $guarded = [
        'id',
        'alias'
    ];

    protected $casts = [
        'visible' => 'bool',
        'type' => 'int',
        'module_id' => 'int',
        'alias' => 'string',
        'label' => 'string'
    ];

    public function setLabelAttribute($value)
    {
        $this->attributes['label'] = $value;

        if (empty($this->attributes['alias'])) {
            $this->attributes['alias'] = str_slug('udf_'.$value.'_'.Carbon::now()->toDateString(), '_');
        }
    }

    public function getTableAttribute()
    {
        $model = '';

        foreach (ModuleDefinedFields::getEnumValues() as $key => $module) {
            if ($module->getValue() === $this->module_id) {
                $model = $module->getModel();
                break;
            }
        }
        
        return App::make($model)->getTable();
    }

    public function getFieldTypeAttribute()
    {
        switch ($this->type) {
            case UserDefinedFieldType::STR:
                return 'string';
                break;
            
            case UserDefinedFieldType::INTR:
                return 'integer';
                break;

            case UserDefinedFieldType::DECIMAL:
                return array('decimal', 23, 6);
                break;

            case UserDefinedFieldType::DATE:
                return 'date';
                break;

            case UserDefinedFieldType::DATETIME:
                return 'datetime';
                break;
        }
    }
}
