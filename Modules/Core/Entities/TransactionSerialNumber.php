<?php

namespace Modules\Core\Entities;

use Modules\Core\Extensions\Relations\MorphPivot;

class TransactionSerialNumber extends MorphPivot
{
    protected $table = 'transaction_serial_number';

    public function seriable()
    {
        return $this->morphTo();
    }
}