<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\HasCounterTotalAmountComputationProcess;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class CounterDetail extends Base implements AuditableInterface
{   
    use Auditable,
        HasCounterTotalAmountComputationProcess;

    protected $table = 'counter_detail';

    protected $touches = ['transaction'];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'reference_id' => 'int',
        'remaining_amount' => 'decimal',
        'transaction_id' => 'int'
    ];

    public function transaction()
    {
        return $this->belongsTo(Counter::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(SalesOutbound::class, 'reference_id')->withTrashed();
    }

    public function generateTags(): array
    {
        return [
            sprintf("Sheet No: %s", $this->transaction->sheet_number),
        ];
    }
}