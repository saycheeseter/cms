<?php

namespace Modules\Core\Entities;

class BranchModuleSeries extends Base
{
    protected $table = 'branch_module_series';

    public $timestamps = false;

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'branch_id' => 'int',
        'type'      => 'string',
        'column'    => 'string',
        'series'    => 'string',
        'limit'     => 'string',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withTrashed();
    }
}