<?php

namespace Modules\Core\Entities;

use Modules\Core\Extensions\Relations\Pivot;

class ProductComponent extends Pivot
{
    public $timestamps = false;
    
    protected $casts = [
        'qty' => 'decimal'
    ];
}