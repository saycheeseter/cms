<?php

namespace Modules\Core\Entities;

class BranchRole extends Base
{
    public $timestamps = false;

    protected $table = 'branch_role';

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'role_id' => 'integer',
        'branch_id' => 'integer'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'branch_permission_role', 'branch_role_id', 'permission_id');
    }
}