<?php

namespace Modules\Core\Entities;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Contracts\GenerateInventoryAdjustmentProductMonthlySummary;
use Modules\Core\Entities\Contracts\SeriableTransaction;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Presenters\InventoryAdjustPresenter;
use Modules\Core\Traits\GenerateInventoryAdjustProductMonthlySummary;
use Lang;
use Modules\Core\Traits\HasSerialNumberLogs;
use Modules\Core\Traits\HasAdjustmentCustomProcess;

class InventoryAdjust extends InventoryFlow implements
    GenerateInventoryAdjustmentProductMonthlySummary,
    SeriableTransaction
{
    use GenerateInventoryAdjustProductMonthlySummary,
        HasSerialNumberLogs,
        HasAdjustmentCustomProcess;

    protected $table = 'inventory_adjust';

    protected $attributes = [
        'approval_status' => ApprovalStatus::DRAFT,
        'total_gain_amount' => 0,
        'total_lost_amount' => 0
    ];

    public function details()
    {
        return $this->hasMany(InventoryAdjustDetail::class, 'transaction_id');
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function posting()
    {
        return $this->morphOne(TransactionSummaryPosting::class, 'transaction');
    }

    public function getTotalAmountAttribute()
    {
        return $this->total_gain_amount->sub($this->total_lost_amount, setting('monetary.precision'));
    }

    public function getTotalLostQtyAttribute()
    {
        $zero = Decimal::create(0, setting('monetary.precision'));
        $total = $zero;

        foreach($this->details as $detail) {
            if ($detail->diff_qty->isLessThan($zero)) {
                $total = $total->add($detail->diff_qty->abs());
            }
        }

        return $total;
    }

    public function getTotalGainQtyAttribute()
    {
        $zero = Decimal::create(0, setting('monetary.precision'));
        $total = $zero;

        foreach($this->details as $detail) {
            if ($detail->diff_qty->isGreaterThan($zero)) {
                $total = $total->add($detail->diff_qty);
            }
        }

        return $total;
    }

    public function getTotalGainLostQtyAttribute()
    {
        return $this->total_gain_qty->sub($this->total_lost_qty, setting('monetary.precision'));
    }

    public function getMultiplierAttribute()
    {
        return Decimal::create(1, setting('monetary.precision'));
    }

    public function presenter()
    {
        return new InventoryAdjustPresenter($this);
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('inventory.adjust.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => InventoryAdjustDetail::class,
                    'relation' => 'details'
                ],
                'for_location' => [
                    'class' => BranchDetail::class,
                    'relation' => 'location'
                ],
            ]
        );
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'total_gain_amount' => 'decimal',
            'total_lost_amount' => 'decimal',
        ]);
    }

    public function getProductMonthlyTransactionCreatedForAttribute()
    {
        return $this->created_for;
    }

    public function getProductMonthlyTransactionForLocationAttribute()
    {
        return $this->for_location;
    }

    public function getProductMonthlyTransactionProductIdColumn()
    {
        return 'product_id';
    }

    public function getProductMonthlyTransactionTransactionDateAttribute()
    {
        return $this->transaction_date;
    }

    public function getSerialOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getSerialOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::AVAILABLE;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::INVENTORY_ADJUST_INCREASE;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.inventory.adjust');
    }
}