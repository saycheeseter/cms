<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\ProductStockCardPresenter;
use Modules\Core\Traits\UpdatesCurrentCost;

class ProductStockCard extends Base
{
    use UpdatesCurrentCost;

    protected $table = 'product_stock_card';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'qty' => 'decimal',
        'price' => 'decimal',
        'amount' => 'decimal',
        'cost' => 'decimal',
        'pre_inventory' => 'decimal',
        'pre_avg_cost' => 'decimal',
        'pre_amount' => 'decimal',
        'post_inventory' => 'decimal',
        'post_avg_cost' => 'decimal',
        'post_amount' => 'decimal',
        'sequence_no' => 'int',
        'transaction_date' => 'datetime'
    ];

    protected $dates = [
        'transaction_date',
    ];

    public function previous()
    {
        return $this->belongsTo(ProductStockCard::class, 'previous_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function location()
    {
         return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }

    public function presenter()
    {
        return new ProductStockCardPresenter($this);
    }
}