<?php 

namespace Modules\Core\Entities;

use Modules\Core\Entities\Contracts\TransactionDetailRemainingReference;
use Modules\Core\Enums\ApprovalStatus;

class StockDeliveryDetail extends InvoiceDetail implements TransactionDetailRemainingReference
{
    protected $table = 'stock_delivery_detail';

    public function transaction()
    {
        return $this->belongsTo(StockDelivery::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(StockRequestDetail::class, 'reference_id')->withTrashed();
    }

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function getSpareQtyAttribute()
    {
        return ($this->transaction_status == ApprovalStatus::APPROVED)
            ? $this->reference->remaining_qty
            : $this->reference->remaining_qty->sub($this->total_qty, setting('monetary.precision'));
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'reference_id' => 'int',
        ]);
    }
}
