<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;

class PaymentMethod extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::PAYMENT_METHOD
    ];

    protected static function type()
    {
        return SystemCodeType::PAYMENT_METHOD;
    }
}
