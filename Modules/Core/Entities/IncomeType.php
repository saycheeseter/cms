<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;

class IncomeType extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::INCOME_TYPE
    ];

    protected static function type()
    {
        return SystemCodeType::INCOME_TYPE;
    }
}
