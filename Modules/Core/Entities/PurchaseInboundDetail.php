<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\HasPriceAsMovingAverageCost;

class PurchaseInboundDetail extends InventoryChainDetail
{
    use HasPriceAsMovingAverageCost;

    protected $table = 'purchase_inbound_detail';

    public function transaction()
    {
        return $this->belongsTo(PurchaseInbound::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->belongsTo(PurchaseDetail::class, 'reference_id')->withTrashed();
    }

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function deliveries()
    {
        return $this->morphMany(StockDeliveryDetail::class, 'transactionable');
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'transactionable_id' => 'int'
        ]);
    }
}
