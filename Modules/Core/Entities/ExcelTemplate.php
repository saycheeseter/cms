<?php

namespace Modules\Core\Entities;

use Modules\Core\Presenters\ExcelTemplatePresenter;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class ExcelTemplate extends Base implements AuditableInterface
{
    use Auditable, UserAudit, BranchAudit;
    
    protected $table = 'excel_template';

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'format' => 'object',
        'module' => 'int'
    ];
        
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function userModified()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function presenter()
    {
        return new ExcelTemplatePresenter($this);
    }
}