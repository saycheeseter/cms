<?php

namespace Modules\Core\Entities;

use Modules\Core\Traits\ComputeQuantityTransaction;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Presenters\InvoicePresenter;
use Modules\Core\Traits\HasSummaryPendingQuantityComputation;

abstract class Invoice extends InventoryFlow
{
    use ComputeQuantityTransaction,
        HasSummaryPendingQuantityComputation;

    protected $attributes = [
        'approval_status' => ApprovalStatus::DRAFT,
        'transaction_status' => TransactionStatus::NO_DELIVERY,
        'total_amount' => 0
    ];
    
    protected $casts = [
        'audited_by' => 'int',
        'requested_by' => 'int',
        'created_for' => 'int',
        'sheet_number' => 'string',
        'remarks' => 'string',
        'transaction_date' => 'datetime',
        'audited_date' => 'datetime',
        'approval_status' => 'int',
        'transaction_status' => 'int',
        'total_amount' => 'decimal',
        'for_location' => 'int',
    ];

    public function presenter()
    {
        return new InvoicePresenter($this);
    }

    public function location()
    {
        return $this->belongsTo(BranchDetail::class, 'for_location')->withTrashed();
    }
    
    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [
                "for_location" => [
                    'class' => BranchDetail::class,
                    'relation' => 'location'
                ],
            ]
        );
    }

    abstract public function imports();
}
