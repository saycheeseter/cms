<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Extensions\Database\Eloquent\Builder;

class Brand extends SystemCode
{
    protected $attributes = [
        'type_id' => SystemCodeType::BRAND
    ];

    public function discounts()
    {
        return $this->morphMany(ProductDiscountDetail::class, 'entity');
    }

    protected static function type()
    {
        return SystemCodeType::BRAND;
    }
}
