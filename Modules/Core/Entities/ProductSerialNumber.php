<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SerialStatus;

class ProductSerialNumber extends Base
{   
    public $table = 'product_serial_number';

    protected $attributes = [
        'status' => SerialStatus::NOT_AVAILABLE,
        'transaction' => 0,
    ];

    protected $guarded = [
        'id',
        'status',
        'transaction',
        'created_at',
        'updated_at',
        'owner_type',
        'owner_id'
    ];

    protected $casts = [
        'product_id' => 'int',
        'supplier_id' => 'int',
        'status' => 'int',
        'transaction' => 'int',
        'owner_type' => 'string',
        'owner_id' => 'int',
        'serial_number' => 'string',
        'expiration_date' => 'date',
        'manufacturing_date' => 'date',
        'admission_date' => 'date',
        'manufacturer_warranty_start' => 'date',
        'manufacturer_warranty_end' => 'date',
        'remarks' => 'string',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id')->withTrashed();
    }

    public function transactionDetails($relationship)
    {
        return $this->morphedByMany($relationship, 'seriable', 'transaction_serial_number', 'serial_id');
    }
}