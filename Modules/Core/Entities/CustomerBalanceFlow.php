<?php 

namespace Modules\Core\Entities;

use Modules\Core\Presenters\CustomerBalanceFlowPresenter;

class CustomerBalanceFlow extends BalanceFlow
{
    protected $table = 'customer_balance_flow';

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withTrashed();
    }

    public function presenter()
    {
        return new CustomerBalanceFlowPresenter($this);
    }
}