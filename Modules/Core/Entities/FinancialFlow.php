<?php 

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\Contracts\PaymentCheckManagement;
use Modules\Core\Entities\Contracts\TransactionReference;
use Modules\Core\Presenters\TransactionPresenter;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\PaymentMethod;
use Litipk\BigNumbers\Decimal;
use Bnb\Laravel\Attachments\HasAttachment;
use Modules\Core\Traits\BranchAudit;
use Modules\Core\Traits\UserAudit;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

use Modules\Core\Entities\Contracts\BranchModuleSeries;
use Modules\Core\Traits\HasBranchModuleSeries;
use Modules\Core\Traits\HasTransactionSheetNumber;

abstract class FinancialFlow extends Base implements
    TransactionReference,
    AuditableInterface,
    PaymentCheckManagement,
    BranchModuleSeries
{
    use SoftDeletes,
        HasAttachment,
        Auditable,
        HasBranchModuleSeries,
        HasTransactionSheetNumber,
        BranchAudit,
        UserAudit {
            UserAudit::runSoftDelete insteadof SoftDeletes;
        }

    protected $attributes = [
        'approval_status' => ApprovalStatus::DRAFT,
        'total_amount' => 0
    ];

    protected $guarded = [
        'id',
        'created_from',
        'created_by',
        'modified_by',
    ];

    protected $casts = [
        'created_from' => 'int',
        'created_by' => 'int',
        'created_for' => 'int',
        'modified_by' => 'int',
        'audited_by' => 'int',
        'audited_date' => 'datetime',
        'approval_status' => 'int',
        'sheet_number' => 'string',
        'remarks' => 'string',
        'total_amount' => 'decimal',
        'transaction_date' => 'datetime'
    ];

    public function from()
    {
        return $this->belongsTo(Branch::class, 'created_from')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function for()
    {
        return $this->belongsTo(Branch::class, 'created_for')->withTrashed();
    }

    public function modifier()
    {
        return $this->belongsTo(User::class, 'modified_by')->withTrashed();
    }

    public function auditor()
    {
        return $this->belongsTo(User::class, 'audited_by')->withTrashed();
    }

    public function presenter()
    {
        return new TransactionPresenter($this);
    }

    public function getTotalFromBalanceAttribute()
    {
        $total = Decimal::create(0, setting('monetary.precision'));

        foreach ($this->details as $detail) {
            if((int) $detail->payment_method == PaymentMethod::FROM_BALANCE) {
                $total = $total->add($detail->amount);
            }
        }

        return $total;
    }

    public function getSeriesColumnAttribute()
    {
        return 'sheet_number';
    }

    public function getIsDeletedAttribute()
    {
        return $this->trashed();
    }

    public abstract function getTotalExcessAmountAttribute();
}