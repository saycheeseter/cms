<?php

namespace Modules\Core\Entities;

use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Presenters\DeliveryInboundPresenter;
use Lang;
use Modules\Core\Traits\HasProductBatchDeliveryInboundReferenceRemainingComputation;

class StockDeliveryInbound extends InventoryChain
{
    use HasProductBatchDeliveryInboundReferenceRemainingComputation;

    protected $table = 'stock_delivery_inbound';

    public function details()
    {
        return $this->hasMany(StockDeliveryInboundDetail::class, 'transaction_id');
    }

    public function reference()
    {
        return $this->belongsTo(StockDeliveryOutbound::class, 'reference_id')->withTrashed();
    }

    public function deliveryFrom()
    {
        return $this->belongsTo(Branch::class, 'delivery_from')->withTrashed();
    }

    public function deliveryFromLocation()
    {
        return $this->belongsTo(BranchDetail::class, 'delivery_from_location')->withTrashed();
    }

    public function presenter()
    {
        return new DeliveryInboundPresenter($this);
    }

    public function getMultiplierAttribute()
    {
        return $this->positive_multiplier;
    }

    public function getSheetNumberPrefixAttribute()
    {
        return setting('stock.delivery.inbound.sheet.number.prefix');
    }

    public function relationships()
    {
        return array_merge(
            parent::relationships(), 
            [   
                'detail' => [
                    'class' => StockDeliveryInboundDetail::class,
                    'relation' => 'details'
                ],
                'delivery_from_location' => [
                    'class' => BranchDetail::class,
                    'relation' => 'deliveryFromLocation'
                ],
                'delivery_from' => [
                    'class' => Branch::class,
                    'relation' => 'deliveryFrom'
                ],
                'reference' => [
                    'class' => StockDeliveryOutbound::class,
                    'break' => true,
                    'relation' => 'reference'
                ],
            ]
        );
    }

    public function getSerialStatusAssignmentAttribute()
    {
        return SerialStatus::AVAILABLE;
    }

    public function getSerialTransactionAssignmentAttribute()
    {
        return SerialTransaction::STOCK_DELIVERY_INBOUND;
    }

    public function getSerialTransactionAssignmentLabelAttribute()
    {
        return Lang::get('core::label.stock.delivery.inbound');
    }

    public function getBatchOwnerIdAssignmentAttribute()
    {
        return $this->for_location;
    }

    public function getBatchOwnerTypeAssignmentAttribute()
    {
        return $this->getMorphMapKey(BranchDetail::class);
    }

    public function getBatchBaseOwnerQtyAttribute()
    {
        return 'pending_qty';
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'delivery_from' => 'int',
            'delivery_from_location' => 'int'
        ]);
    }

    public function getTransactionSummaryFieldAttribute()
    {
        return 'stock_delivery_inbound';
    }

    public function getProductMonthlyTransactionAttributeMappingPrefix()
    {
        return 'stock_delivery_inbound';
    }
}
