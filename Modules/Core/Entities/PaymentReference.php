<?php 

namespace Modules\Core\Entities;

class PaymentReference extends FinancialFlowReference 
{
    protected $table = 'payment_reference';

    public function transaction()
    {
        return $this->belongsTo(Payment::class, 'transaction_id', 'id')->withTrashed();
    }

    public function reference()
    {
        return $this->morphTo();
    }

    public function getCasts()
    {
        return array_merge(parent::getCasts(), [
            'payable' => 'decimal'
        ]);
    }
}