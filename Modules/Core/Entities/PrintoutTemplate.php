<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Presenters\PrintoutTemplatePresenter;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Modules\Core\Extensions\Auditing\Auditable;

class PrintoutTemplate extends Base implements AuditableInterface
{
    use SoftDeletes, Auditable;

    protected $table = 'printout_template';

    protected $fillable = [
        'name',
        'module_id',
        'format',
    ];

    protected $casts = [
        'module_id' => 'int',
        'name' => 'string',
        'format' => 'object',
    ];

    public function presenter()
    {
        return new PrintoutTemplatePresenter($this);
    }
}
