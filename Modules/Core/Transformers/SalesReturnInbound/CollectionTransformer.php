<?php

namespace Modules\Core\Transformers\SalesReturnInbound;

use Illuminate\Database\Eloquent\Relations\Relation;
use League\Fractal\TransformerAbstract;
use Lang;
use Modules\Core\Entities\SalesReturnInbound;

/**
 * Class CollectionTransformer
 * @package namespace Modules\Core\Transformers\SalesReturnInbound;
 */
class CollectionTransformer extends TransformerAbstract
{
    /**
     * Transform the SalesReturnInbound entity
     * @param SalesReturnInbound $model
     *
     * @return array
     */
    public function transform(SalesReturnInbound $model)
    {
        return [
            'reference_id'       => $model->id,
            'reference_type'     => array_search(get_class($model), Relation::morphMap()),
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'term'               => $model->term,
            'dr_no'              => '',
            'remarks'            => $model->remarks,
            'amount'             => $model->number()->total_amount,
            'collected_amount'   => $model->number()->collected_amount,
            'remaining'          => $model->number()->remaining_amount,
            'collectible'        => ''
        ];
    }
}