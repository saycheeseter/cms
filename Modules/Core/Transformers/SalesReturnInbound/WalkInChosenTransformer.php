<?php

namespace Modules\Core\Transformers\SalesReturnInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SalesReturnInbound;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\SalesOutbound;
 */
class WalkInChosenTransformer extends TransformerAbstract
{

    /**
     * Transform the SalesOutbound entity
     * @param SalesOutbound $model
     *
     * @return array
     */
    public function transform(SalesReturnInbound $model)
    {
        return [
            'id'    => $model->customer_walk_in_name,
            'label' => $model->customer_walk_in_name
        ];
    }
}
