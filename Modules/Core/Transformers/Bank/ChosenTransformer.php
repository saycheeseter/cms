<?php 

namespace Modules\Core\Transformers\Bank;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Bank;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Bank;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Bank entity
     * @param Bank $model
     *
     * @return array
     */
    public function transform(Bank $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}