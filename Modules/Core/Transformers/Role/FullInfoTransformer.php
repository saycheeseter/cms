<?php

namespace Modules\Core\Transformers\Role;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Role;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Role;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Role entity
     * @param Role $model
     *
     * @return array
     */
    public function transform(Role $model)
    {
        $permissions = array();

        foreach($model->branches as $key => $value){
            $permissions[] = [
                'branch' => $value['branch_id'],
                'code' => $value->permissions->pluck('id')->toArray()
            ];
        }

        return [
            'id'          => $model->id,
            'code'        => $model->code,
            'name'        => $model->name,
            'permissions' => $permissions
        ];
    }
}