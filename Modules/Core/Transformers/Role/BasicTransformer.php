<?php

namespace Modules\Core\Transformers\Role;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Role;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Role;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Role entity
     * @param Role $model
     *
     * @return array
     */
    public function transform(Role $model)
    {
        return [
            'id'   => $model->id,
            'code' => $model->code,
            'name' => $model->name
        ];
    }
}