<?php

namespace Modules\Core\Transformers\Role;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Role;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Role;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Role entity
     * @param Role $model
     *
     * @return array
     */
    public function transform(Role $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
