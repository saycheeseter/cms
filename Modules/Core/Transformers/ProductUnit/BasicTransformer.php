<?php

namespace Modules\Core\Transformers\ProductUnit;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductUnit;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductUnit;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductUnit entity
     * @param ProductUnit $model
     *
     * @return array
     */
    public function transform(ProductUnit $model)
    {
        return [
            'uom_id'        => $model->uom_id,
            'qty'           => $model->number()->qty,
            'length'        => $model->number()->length,
            'width'         => $model->number()->width,
            'height'        => $model->number()->height,
            'weight'        => $model->number()->weight,
        ];
    }
}
