<?php

namespace Modules\Core\Transformers\MemberRateDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\MemberRateDetail;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\MemberRateDetail;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the MemberRateDetail entity
     * @param MemberRateDetail $model
     *
     * @return array
     */
    public function transform(MemberRateDetail $model)
    {
        return [
            'id'                    => $model->id,
            'member_rate_head_id'   => $model->member_rate_head_id,
            'amount_from'           => $model->number()->amount_from,
            'amount_to'             => $model->number()->amount_to,
            'date_from'             => $model->date_from->toDateString(),
            'date_to'               => $model->date_to->toDateString(),
            'amount_per_point'      => $model->number()->amount_per_point,
            'discount'              => $model->number()->discount
        ];
    }
}