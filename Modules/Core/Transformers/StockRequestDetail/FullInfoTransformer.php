<?php

namespace Modules\Core\Transformers\StockRequestDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockRequestDetail;
use Modules\Core\Transformers\Product\TransactionComponentsTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\StockRequestDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the StockRequestDetail entity
     * 
     * @param StockRequestDetail $model
     *
     * @return array
     */
    public function transform(StockRequestDetail $model)
    {
        return [
            'id'            => $model->id,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcode_list,
            'barcode'       => $model->presenter()->unit_barcode,
            'stock_no'      => $model->product->stock_no,
            'name'          => $model->product->name, 
            'chinese_name'  => $model->product->chinese_name,
            'units'         => $model->presenter()->unit_list,
            'unit_id'       => $model->unit_id,
            'unit_qty'      => $model->number()->unit_qty,
            'qty'           => $model->number()->qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'cost'          => $model->number()->cost,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'remarks'       => $model->remarks,
            'remaining_qty' => $model->number()->remaining_qty,
            'references'    => $this->transformPurchases($model->purchases),
            'components'    => Fractal::collection($model->components, new TransactionComponentsTransformer)->getArray()['data'],
            'group_type'    => $model->group_type,
            'manageable'    => $model->manageable,
            'product_type'  => $model->product_type
        ];
    }

    private function transformPurchases($purchases)
    {
        $data = [];

        foreach ($purchases as $key => $purchase) {
            $data[] = [
                'id'           => $purchase->transaction->id,
                'sheet_number' => $purchase->transaction->sheet_number
            ];
        }

        return $data;
    }

}
