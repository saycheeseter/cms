<?php

namespace Modules\Core\Transformers\Tax;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Tax;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Tax;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Tax entity
     * @param Tax $model
     *
     * @return array
     */
    public function transform(Tax $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
