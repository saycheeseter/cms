<?php

namespace Modules\Core\Transformers\ProductTax;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Tax;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductUnit;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductUnit entity
     * @param ProductUnit $model
     *
     * @return array
     */
    public function transform(Tax $model)
    {
        return $model->id;
    }
}
