<?php

namespace Modules\Core\Transformers\ProductConversionDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversionDetail;

/**
 * Class TransactionTransformer
 * @package namespace Modules\Core\Transformers\ProductConversionDetail;
 */
class TransactionTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductConversionDetail entity
     * @param ProductConversionDetail $model
     *
     * @return array
     */
    public function transform(ProductConversionDetail $model)
    {
        return [
            'transaction_id'   => $model->transaction->id,
            'transaction_date' => $model->transaction->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->transaction->sheet_number,
            'particulars'      => $model->transaction->presenter()->particular,
            'qty'              => $model->number()->total_qty,
            'price'            => $model->number()->ma_cost,
            'amount'           => $model->number()->amount,
            'prepared_by'      => $model->transaction->creator->full_name,
            'remarks'          => $model->transaction->presenter()->group_type_label
        ];
    }
}