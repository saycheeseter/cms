<?php

namespace Modules\Core\Transformers\ProductConversionDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversionDetail;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ProductConversionDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductConversionDetail entity
     * @param ProductConversionDetail $model
     *
     * @return array
     */
    public function transform(ProductConversionDetail $model)
    {
        return [
            'id'           => $model->id,
            'component_id' => $model->component_id,
            'required_qty' => $model->number()->required_qty,
            'name'         => $model->component->name,
            'barcode'      => $model->component->barcodes->implode('code', ', '),
            'stock_number' => $model->component->stock_no,
            'cost'         => $model->number()->cost
        ];
    }
}
