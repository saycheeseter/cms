<?php

namespace Modules\Core\Transformers\PurchaseReturnOutbound;

use Illuminate\Database\Eloquent\Relations\Relation;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Lang;

/**
 * Class PaymentTransformer
 * @package namespace Modules\Core\Transformers\PurchaseReturnOutbound;
 */
class PaymentTransformer extends TransformerAbstract
{
    /**
     * Transform the PurchaseReturnOutbound entity
     * @param PurchaseReturnOutbound $model
     *
     * @return array
     */
    public function transform(PurchaseReturnOutbound $model)
    {
        return [
            'reference_id'       => $model->id,
            'reference_type'     => array_search(get_class($model), Relation::morphMap()),
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'term'               => $model->term,
            'dr_no'              => '',
            'remarks'            => $model->remarks,
            'amount'             => $model->number()->total_amount,
            'paid_amount'        => $model->number()->paid_amount,
            'remaining'          => $model->number()->remaining_amount,
            'payable'            => ''
        ];
    }
}