<?php

namespace Modules\Core\Transformers\PurchaseReturnOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\PurchaseReturnOutbound;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the PurchaseReturnOutbound entity
     * @param PurchaseReturnOutbound $model
     *
     * @return array
     */
    public function transform(PurchaseReturnOutbound $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'payment_method'        => $model->payment->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'supplier'              => $model->supplier->full_name,
            'supplier_address'      => $model->supplier->address,
            'term'                  => $model->term,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'invoice_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
