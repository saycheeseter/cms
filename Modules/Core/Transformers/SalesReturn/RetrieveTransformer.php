<?php

namespace Modules\Core\Transformers\SalesReturn;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SalesReturn;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\SalesReturn;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the SalesReturn entity
     * @param SalesReturn $model
     *
     * @return array
     */
    public function transform(SalesReturn $model)
    {
        return [
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'term'                  => $model->term,
            'for_location'          => $model->for_location,
            'created_for'           => $model->created_for,
        ];
    }
}
