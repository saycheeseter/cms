<?php

namespace Modules\Core\Transformers\BranchPrice;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BranchPrice;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BranchPrice;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the BranchPrice entity
     * @param BranchPrice $model
     *
     * @return array
     */
    public function transform(BranchPrice $model)
    {
        return [
            'branch_id'         => $model->branch_id,
            'branch_name'       => $model->branch->name,
            'copy_from'         => $model->copy_from,
            'purchase_price'    => $model->number()->purchase_price,
            'selling_price'     => $model->number()->selling_price,
            'wholesale_price'   => $model->number()->wholesale_price,
            'price_a'           => $model->number()->price_a,
            'price_b'           => $model->number()->price_b,
            'price_c'           => $model->number()->price_c,
            'price_d'           => $model->number()->price_d,
            'price_e'           => $model->number()->price_e,
            'editable'          => $model->editable
        ];
    }
}
