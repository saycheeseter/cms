<?php

namespace Modules\Core\Transformers\CounterDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CounterDetail;

class PrintTransformer extends TransformerAbstract
{
    public function transform(CounterDetail $model)
    {
        return [
            'transaction_date' => $model->reference->transaction_date->toDateString(),
            'sheet_number'     => $model->reference->sheet_number,
            'remaining'        => $model->remaining_amount,
        ];
    }
}