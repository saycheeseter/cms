<?php

namespace Modules\Core\Transformers\CounterDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CounterDetail;
use Lang;

/**
 * Class DetailFullInfoTransfomer
 * @package namespace Modules\Core\Transformers\CounterDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the CounterDetail entity
     * @param CounterDetail $model
     *
     * @return array
     */
    public function transform(CounterDetail $model)
    {
        return [
            'id'               => $model->id,
            'transaction_date' => $model->reference->transaction_date->toDateString(),
            'sheet_number'     => $model->reference->sheet_number,
            'remaining_amount' => $model->number()->remaining_amount,
            'reference_id'     => $model->reference_id
        ];
    }
}