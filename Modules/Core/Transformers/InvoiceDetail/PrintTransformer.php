<?php

namespace Modules\Core\Transformers\InvoiceDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InvoiceDetail;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\InvoiceDetail;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InvoiceDetail entity
     * 
     * @param InvoiceDetail $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InvoiceDetail::class)) {
            return [];
        }

        return [
            'unit_barcode'                 => $model->presenter()->unit_barcode,
            'name'                         => $model->product->name,
            'stock_no'                     => $model->product->stock_no,
            'chinese_name'                 => $model->product->chinese_name,
            'qty'                          => $model->qty,
            'unit_code'                    => $model->unit->code,
            'unit_name'                    => $model->unit->name,
            'decimal_unit_code_specs'      => $model->presenter()->decimal_unit_code_and_specs,
            'whole_number_unit_code_specs' => $model->presenter()->whole_number_unit_code_and_specs,
            'unit_qty'                     => $model->unit_qty,
            'oprice'                       => $model->oprice,
            'price'                        => $model->price,
            'discount1'                    => $model->discount1,
            'discount2'                    => $model->discount2,
            'discount3'                    => $model->discount3,
            'discount4'                    => $model->discount4,
            'decimal_discounts'            => $model->presenter()->decimal_discounts,
            'whole_number_discounts'       => $model->presenter()->whole_number_discounts,
            'amount'                       => $model->amount,
            'remarks'                      => $model->remarks
        ];
    }
}
