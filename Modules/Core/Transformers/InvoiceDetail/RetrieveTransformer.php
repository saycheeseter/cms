<?php

namespace Modules\Core\Transformers\InvoiceDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InvoiceDetail;
use Modules\Core\Transformers\Product\TransactionComponentsTransformer;
use Fractal;
/**
 * Class RetrieveTransformer
 *
 * @package namespace Modules\Core\Transformers\Damage;
 */
class RetrieveTransformer extends TransformerAbstract
{
    /**
     * Transform the InvoiceDetail entity
     *
     * @param InvoiceDetail $model
     *
     * @return array
     */
    public function transform(InvoiceDetail $model)
    {
        return [
            'id'            => $model->id,
            'manage_type'   => $model->product->manage_type,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcode_list,
            'barcode'       => $model->presenter()->computed_unit_barcode,
            'stock_no'      => $model->product->stock_no,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'units'         => $model->presenter()->unit_list,
            'unit_id'       => $model->computed_unit_id,
            'unit_qty'      => $model->number()->computed_unit_qty,
            'qty'           => $model->number()->computed_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'cost'          => $model->number()->cost,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'remarks'       => $model->remarks,
            'remaining_qty' => $model->number()->remaining_qty,
            'components'    => Fractal::collection($model->components, new TransactionComponentsTransformer)->getArray()['data'],
            'group_type'    => $model->group_type,
            'manageable'    => $model->manageable,
            'product_type'  => $model->product_type
        ];
    }
}
