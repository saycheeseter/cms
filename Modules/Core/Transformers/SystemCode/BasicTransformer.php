<?php

namespace Modules\Core\Transformers\SystemCode;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SystemCode;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\SystemCode;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the SystemCode entity
     * @param SystemCode $model
     *
     * @return array
     */
    public function transform(SystemCode $model)
    {
        return [
            'id'         => $model->id,
            'code'       => $model->code,
            'name'       => $model->name,
            'remarks'    => $model->remarks,
            'deletable'  => $model->deletable
        ];
    }
}
