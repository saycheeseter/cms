<?php

namespace Modules\Core\Transformers\SystemCode;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SystemCode;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\SystemCode;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the SystemCode entity
     * @param SystemCode $model
     *
     * @return array
     */
    public function transform(SystemCode $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name
        ];
    }
}
