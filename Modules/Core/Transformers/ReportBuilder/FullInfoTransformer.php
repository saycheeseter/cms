<?php

namespace Modules\Core\Transformers\ReportBuilder;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ReportBuilder;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ReportBuilder;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the ReportBuilder entity
     * @param ReportBuilder $model
     *
     * @return array
     */
    public function transform(ReportBuilder $model)
    {
        return [
            'id'       => $model->id,
            'name'     => $model->name,
            'group_id' => $model->group_id,
            'format'   => $model->format
        ];
    }
}
