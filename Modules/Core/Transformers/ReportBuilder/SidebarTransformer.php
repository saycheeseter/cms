<?php

namespace Modules\Core\Transformers\ReportBuilder;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ReportBuilder;
use Lang;

/**
 * Class SidebarTransformer
 * @package namespace Modules\Core\Transformers\ReportBuilder;
 */
class SidebarTransformer extends TransformerAbstract
{

    /**
     * Transform the ReportBuilder entity
     * @param ReportBuilder $model
     *
     * @return array
     */
    public function transform($model)
    {
        $list = [];
        
        foreach ($model as $key => $value) {
            $list[$value->presenter()->moduleGroup][] = [
                'name' => $value->name,
                'slug' => $value->slug
            ];
        }

        return $list;
    }
}
