<?php

namespace Modules\Core\Transformers\ReportBuilder;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ReportBuilder;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ReportBuilder;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ReportBuilder entity
     * @param ReportBuilder $model
     *
     * @return array
     */
    public function transform(ReportBuilder $model)
    {
        return [
            'id'           => $model->id,
            'name'         => $model->name,
            'module_group' => $model->presenter()->moduleGroup,
            'created_by'   => is_null($model->userCreated) ? '' : $model->userCreated->full_name,
            'created_at'   => $model->created_at->toDateTimeString(),
            'modified_by'  => is_null($model->userModified) ? '' : $model->userModified->full_name,
            'updated_at'   => ($model->updated_at) ? $model->updated_at->toDateTimeString() : ''
        ];
    }
}
