<?php

namespace Modules\Core\Transformers\UserMenu;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\UserMenu;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\UserMenu;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the UserMenu entity
     * @param UserMenu $model
     *
     * @return array
     */
    public function transform($model)
    {
        return [
            'format' => $model->format ?? [],
            'viewable_format' => $model->viewable_module ?? []
        ];
    }
}
