<?php

namespace Modules\Core\Transformers\Counter;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Counter;

class FooterPrintTransformer extends TransformerAbstract
{
    public function transform(Counter $model)
    {
        return [
            'total_remaining' => $model->number()->total_amount
        ];
    }
}