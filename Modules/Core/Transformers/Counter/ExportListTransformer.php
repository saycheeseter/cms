<?php

namespace Modules\Core\Transformers\Counter;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Counter;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Counter;
 */
class ExportListTransformer extends TransformerAbstract
{

    /**
     * Transform the Counter entity
     * @param Counter $model
     *
     * @return array
     */
    public function transform(Counter $model)
    {
        return [
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->sheet_number,
            'created_from'     => $model->from->name,
            'created_for'      => $model->for->name,
            'customer'         => $model->customer->name,
            'total_amount'     => $model->total_amount,
            'remarks'          => $model->remarks,
            'created_by'       => $model->creator->full_name,
        ];
    }
}
