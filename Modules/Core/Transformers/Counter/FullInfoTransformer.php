<?php

namespace Modules\Core\Transformers\Counter;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Counter;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Counter;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Counter entity
     * @param Counter $model
     *
     * @return array
     */
    public function transform(Counter $model)
    {
        return [
            'id'               => $model->id,
            'created_for'      => $model->created_for,
            'customer_id'      => $model->customer_id,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->sheet_number,
            'remarks'          => $model->remarks,
            'total_amount'     => $model->number()->total_amount
        ];
    }
}