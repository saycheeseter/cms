<?php

namespace Modules\Core\Transformers\Counter;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Counter;
use Auth;

class HeaderPrintTransformer extends TransformerAbstract
{
    public function transform(Counter $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'remarks'               => $model->remarks,
            'sheet_number'          => $model->sheet_number,
            'customer'              => $model->customer->name,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
        ];
    }
}