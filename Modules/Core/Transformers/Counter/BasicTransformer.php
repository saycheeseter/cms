<?php

namespace Modules\Core\Transformers\Counter;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Counter;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Counter;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Counter entity
     * @param Counter $model
     *
     * @return array
     */
    public function transform(Counter $model)
    {
        return [
            'id'               => $model->id,
            'created_from'     => $model->from->name,
            'created_for'      => $model->for->name,
            'created_by'       => $model->creator->full_name,
            'customer'         => $model->customer->name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->sheet_number,
            'remarks'          => $model->remarks,
            'total_amount'     => $model->number()->total_amount
        ];
    }
}