<?php

namespace Modules\Core\Transformers\OtherPayment;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherPayment;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\OtherPayment;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the OtherPayment entity
     * @param OtherPayment $model
     *
     * @return array
     */
    public function transform(OtherPayment $model)
    {
        return [
            'id'               => $model->id,
            'reference_name'           => $model->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks ?? '',
            'payment_method'   => $model->payment_method,
            'created_by'       => $model->created_by,
            'created_from'     => $model->created_from,
            'bank_account_id'          => $model->bank_account_id,
            'check_date'       => isset($model->check_date) ? $model->check_date->toDateTimeString() : null,
            'check_number'     => $model->check_number
        ];
    }
}
