<?php

namespace Modules\Core\Transformers\OtherPayment;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherPayment;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\OtherPayment;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the OtherPayment entity
     * @param OtherPayment $model
     *
     * @return array
     */
    public function transform(OtherPayment $model)
    {
        return [
            'id'               => $model->id,
            'reference_name'   => $model->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks ?? '',
            'method'           => $model->method->name,
            'created_by'       => $model->creator->full_name,
            'created_from'     => $model->from->name,
            'total_amount'     => $model->number()->total_amount
        ];
    }
}
