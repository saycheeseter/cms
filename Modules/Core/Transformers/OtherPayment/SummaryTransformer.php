<?php

namespace Modules\Core\Transformers\OtherPayment;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherPayment;

/**
 * Class SummaryTransformer
 * @package namespace Modules\Core\Transformers\OtherPayment;
 */
class SummaryTransformer extends TransformerAbstract
{
    public function transform($model)
    {
        return [
            'other_payment' => $model->other_payment
        ];
    }
}
