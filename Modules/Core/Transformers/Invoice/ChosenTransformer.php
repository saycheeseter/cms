<?php

namespace Modules\Core\Transformers\Invoice;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Invoice;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Invoice;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the invoice entity
     * @param invoice $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, Invoice::class)) {
            return [];
        }
        
        return [
            'id'    => $model->id,
            'label' => $model->sheet_number,
        ];
    }
}
