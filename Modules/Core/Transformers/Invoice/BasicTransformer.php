<?php

namespace Modules\Core\Transformers\Invoice;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Invoice;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\Invoice;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the Invoice entity
     * 
     * @param Invoice $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, Invoice::class)) {
            return [];
        }

        return [
            'id'               => $model->id,
            'sheet_number'     => $model->sheet_number,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks,
        ];
    }
}
