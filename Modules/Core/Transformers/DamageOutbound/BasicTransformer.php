<?php

namespace Modules\Core\Transformers\DamageOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\DamageOutbound;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\DamageOutbound;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the DamageOutbound entity
     * @param DamageOutbound $model
     *
     * @return array
     */
    public function transform(DamageOutbound $model)
    {
        $fields = [
            'id'                    => $model->id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'created_by'            => $model->creator->full_name,
            'created_from'          => $model->from->name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'reason'                => $model->reason->name,
            'created_date'          => $model->created_at->toDateTimeString(),
            'audited_by'            => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'          => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'amount'                => $model->number()->total_amount,
            'reference'             => [
                'id'           => $model->reference_id,
                'sheet_number' => $model->reference->sheet_number ?? ''
            ],
            'deleted'               => $model->presenter()->deleted,
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
