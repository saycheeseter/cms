<?php

namespace Modules\Core\Transformers\DamageOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\DamageOutbound;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\DamageOutbound;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the DamageOutbound entity
     * @param DamageOutbound $model
     *
     * @return array
     */
    public function transform(DamageOutbound $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'reason'                => $model->reason->name,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'invoice_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
