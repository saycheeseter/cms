<?php

namespace Modules\Core\Transformers\Payment;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Payment;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Payment;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Payment entity
     * @param Payment $model
     *
     * @return array
     */
    public function transform(Payment $model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'created_for'           => $model->created_for,
            'supplier_id'           => $model->supplier_id,
            'entity_id'             => $model->supplier_id,
            'remarks'               => $model->remarks,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'is_deleted'            => $model->is_deleted,
        ];
    }
}