<?php

namespace Modules\Core\Transformers\Payment;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Payment;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Payment;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Payment entity
     * @param Payment $model
     *
     * @return array
     */
    public function transform(Payment $model)
    {
        return [
            'id'                    => $model->id,
            'location'              => $model->for->name,
            'date'                  => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'supplier'              => $model->supplier->full_name,
            'amount'                => $model->number()->total_amount,
            'remarks'               => $model->remarks,
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'deleted'               => $model->presenter()->deleted
        ];
    }
}