<?php

namespace Modules\Core\Transformers\StockReturnInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockReturnInbound;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\StockReturnInbound;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the StockReturnInbound entity
     * @param StockReturnInbound $model
     *
     * @return array
     */
    public function transform(StockReturnInbound $model)
    {
        $fields = [
            'id'                           => $model->id,
            'sheet_number'                 => $model->sheet_number,
            'transaction_date'             => $model->transaction_date->toDateTimeString(),
            'delivery_from'                => $model->delivery_from ?? null,
            'delivery_from_location'       => $model->delivery_from_location ?? null,
            'delivery_from_label'          => $model->deliveryFrom->name ?? '',
            'delivery_from_location_label' => $model->deliveryFromLocation->name ?? '',
            'remarks'                      => $model->remarks,
            'requested_by'                 => $model->requested_by,
            'created_date'                 => $model->created_at->toDateTimeString(),
            'modified_date'                => $model->updated_at->toDateTimeString(),
            'approval_status_label'        => $model->presenter()->approval,
            'approval_status'              => $model->approval_status,
            'created_by'                   => $model->creator->full_name,
            'created_for'                  => $model->created_for,
            'for_location'                 => $model->for_location,
            'reference_id'                 => $model->reference_id ?? null,
            'modified_by'                  => $model->modifier->full_name ?? '',
            'is_deleted'                   => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
