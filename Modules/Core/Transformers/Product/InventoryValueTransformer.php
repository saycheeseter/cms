<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductInventoryValue;

/**
 * Class InventoryValueTransformer
 * @package namespace Modules\Core\Transformers\ProductBranchSummary;
 */
class InventoryValueTransformer extends TransformerAbstract
{

    /**
     * @param ProductInventoryValue $model
     * @return array
     */
    public function transform(ProductInventoryValue $model)
    {
        return [
            'barcode'                      => $model->barcode,
            'name'                         => $model->name,
            'chinese_name'                 => $model->chinese_name ?? '',
            'supplier_name'                => $model->supplier_name ?? '',
            'inventory'                    => $model->number()->inventory,
            'branch'                       => $model->branch,
            'location'                     => $model->location,
            'purchase_price'               => $model->number()->purchase_price,
            'purchase_inventory_value'     => $model->number()->purchase_inventory_value,
            'current_cost_inventory_value' => $model->number()->current_cost_inventory_value,
            'current_cost'                 => $model->number()->current_cost
        ];
    }
}
