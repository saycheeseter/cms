<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;
use Lang;

/**
 * Class AlternativeTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class AlternativeTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'           => $model->id,
            'barcode'      => $model->barcodes->implode('code', ', '),
            'name'         => $model->name,
            'chinese_name' => $model->chinese_name,
            'inventory'    => $model->summaries->first()->number()->qty
        ];
    }
}
