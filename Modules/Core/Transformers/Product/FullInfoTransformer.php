<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\User;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'           => $model->id,
            'stock_no'     => $model->stock_no,
            'supplier_sku' => $model->supplier_sku,
            'name'         => $model->name,
            'description'  => $model->description ?? '',
            'chinese_name' => $model->chinese_name ?? '',
            'supplier_id'  => $model->supplier_id,
            'brand_id'     => $model->brand_id,
            'category_id'  => $model->category_id,
            'manage_type'  => $model->manage_type,
            'senior'       => $model->senior,
            'status'       => $model->status,
            'memo'         => $model->memo ?? '',
            'group_type'   => $model->group_type,
            'type'         => $model->type,
            'manageable'   => $model->manageable,
            'type_label'   => $model->presenter()->type_label,
            'created'      => $model->presenter()->created_by_and_date,
            'modified'     => $model->presenter()->modified_by_and_date,
            'is_used'      => $model->is_used,
            'default_unit' => $model->default_unit
        ];
    }
}
