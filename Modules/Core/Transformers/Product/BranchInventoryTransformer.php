<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductBranchInventory;
use Litipk\BigNumbers\Decimal;
use Lang;

/**
 * Class BranchInventoryTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class BranchInventoryTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param ProductBranchInventory $model
     *
     * @return array
     */
    public function transform(ProductBranchInventory $model)
    {
        $excluded = ['barcode', 'name'];

        $attributes = $model->getAttributes();

        $data = [
            'barcode' => $model->barcode,
            'name'    => $model->name
        ];

        foreach ($attributes as $key => $value) {
            if(!in_array($key, $excluded)) {
                $data[$key] = number_format(
                    Decimal::create(
                        app('NumberFormatter')->parse($value),
                        setting('monetary.precision')
                    )->innerValue(),
                    setting('monetary.precision')
                );
            }
        }

        return $data;
    }
}