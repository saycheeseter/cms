<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;

use Modules\Core\Entities\Product;

/**
 * Class TransactionComponentsTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class TransactionComponentsTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'product_id' => $model->id,
            'qty' => $model->pivot->number()->qty,
            'group_type' => $model->pivot->group_type
        ];
    }
}