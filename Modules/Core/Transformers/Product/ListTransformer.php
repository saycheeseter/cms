<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductPriceSummary;
use Modules\Core\Entities\Product;

/**
 * Class ListTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ListTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param ProductPriceSummary $model
     *
     * @return array
     */
    public function transform(ProductPriceSummary $model)
    {
        return [
            'id'               => $model->id,
            'stock_no'         => $model->stock_no,
            'supplier_sku'     => $model->supplier_sku,
            'description'      => $model->description ?? '',
            'name'             => $model->name,
            'chinese_name'     => $model->chinese_name ?? '',
            'memo'             => $model->memo ?? '',
            'barcode'          => $model->barcode,
            'category'         => $model->category_name ?? '',
            'brand'            => $model->brand_name ?? '',
            'supplier'         => $model->supplier_name ?? '',
            'purchase_price'   => $model->number()->purchase_price,
            'selling_price'    => $model->number()->selling_price,
            'wholesale_price'  => $model->number()->wholesale_price,
            'price_a'          => $model->number()->price_a,
            'price_b'          => $model->number()->price_b,
            'price_c'          => $model->number()->price_c,
            'price_d'          => $model->number()->price_d,
            'price_e'          => $model->number()->price_e,
            'inventory'        => $model->number()->inventory,
            'group_type_label' => $model->presenter()->group_type_label,
            'group_type'       => $model->group_type
        ];
    }
}
