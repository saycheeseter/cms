<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\Brand;
use Modules\Core\Entities\Category;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'                    => $model->id,
            'stock_no'              => $model->stock_no,
            'supplier_sku'          => $model->supplier_sku,
            'description'           => $model->description ?? '',
            'name'                  => $model->name,
            'chinese_name'          => $model->chinese_name ?? '',
            'memo'                  => $model->memo ?? '',
            'barcode'               => $model->barcodes->implode('code', ', ')
        ];
    }
}
