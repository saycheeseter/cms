<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;

/**
 * Class ConversionTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ConversionTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'           => $model->id,
            'name'         => $model->name,
            'barcode'      => $model->barcodes->implode('code', ', '),
            'stock_number' => $model->stock_no,
            'required_qty' => $model->pivot->number()->qty
        ];
    }
}