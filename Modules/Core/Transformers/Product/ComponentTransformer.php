<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;

/**
 * Class ComponentTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ComponentTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'component_id' => $model->id,
            'stock_no'     => $model->stock_no,
            'name'         => $model->name,
            'qty'          => $model->pivot->number()->qty
        ];
    }
}