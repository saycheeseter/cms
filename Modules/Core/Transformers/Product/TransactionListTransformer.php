<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductTransactionList;
use Litipk\BigNumbers\Decimal;
use Lang;

/**
 * Class TransactionListTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class TransactionListTransformer extends TransformerAbstract
{
    /**
     * Transform the Product entity
     * @param ProductTransactionList $model
     *
     * @return array
     */
    public function transform(ProductTransactionList $model)
    {
        return [
            'barcode'                          => $model->barcode,
            'product'                          => $model->name,
            'chinese_name'                     => $model->chinese_name,
            'beginning'                        => $model->number()->beginning,
            'purchase_inbound'                 => $model->number()->purchase_inbound,
            'purchase_return_outbound'         => $model->number()->purchase_return_outbound,
            'sales_outbound'                   => $model->number()->sales_outbound,
            'sales_return_inbound'             => $model->number()->sales_return_inbound,
            'stock_delivery_inbound'           => $model->number()->stock_delivery_inbound,
            'stock_delivery_outbound'          => $model->number()->stock_delivery_outbound,
            'stock_return_inbound'             => $model->number()->stock_return_inbound,
            'adjustment_increase'              => $model->number()->adjustment_increase,
            'stock_return_outbound'            => $model->number()->stock_return_outbound,
            'damage_outbound'                  => $model->number()->damage_outbound,
            'adjustment_decrease'              => $model->number()->adjustment_decrease,
            'product_conversion_increase'      => $model->number()->product_conversion_increase,
            'product_conversion_decrease'      => $model->number()->product_conversion_decrease,
            'auto_product_conversion_increase' => $model->number()->auto_product_conversion_increase,
            'auto_product_conversion_decrease' => $model->number()->auto_product_conversion_decrease,
            'pos_sales'                        => $model->number()->pos_sales,
            'inventory'                        => $model->number()->inventory
        ];
    }
}