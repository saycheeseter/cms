<?php

namespace Modules\Core\Transformers\Product;

use Modules\Core\Entities\Customer;
use Modules\Core\Enums\BarcodeScanUnit;
use Modules\Core\Enums\CustomerPricingType;
use Modules\Core\Enums\CustomerPriceSettings;
use Litipk\BigNumbers\Decimal;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\Product;
 */
class CustomerPriceTransformer extends SummaryAndPriceTransformer
{
    protected $customer;

    public function __construct()
    {
        parent::__construct();

        $this->customer = Customer::find($this->request->get('customer_id'));
    }

    protected function getPriceBySettings($model)
    {
        $precision = setting('monetary.precision');

        $setting = setting('soi.regular.customers.price.basis');

        switch ($setting) {
            case CustomerPriceSettings::WHOLESALE:
                $price = $model->prices->first()->number()->wholesale_price;
                break;

            case CustomerPriceSettings::RETAIL:
                $price = $model->prices->first()->number()->selling_price;
                break;

            case CustomerPriceSettings::CUSTOM:

                switch ($this->customer->pricing_type) {
                    case CustomerPricingType::HISTORICAL:
                        $price = $this->getHistoricalPrice($model);
                        break;

                    case CustomerPricingType::WHOLESALE:
                        $price = $model->prices->first()->number()->wholesale_price;
                        break;

                    case CustomerPricingType::RETAIL:
                        $price = $model->prices->first()->number()->selling_price;
                        break;

                    case CustomerPricingType::PERCENT_PURCHASE:
                        $price = $this->getPercentagePrice($model->prices->first()->purchase_price);
                        $price = number_format($price->innerValue(), $precision);
                        break;

                    case CustomerPricingType::PERCENT_WHOLESALE:
                        $price = $this->getPercentagePrice($model->prices->first()->wholesale_price);
                        $price = number_format($price->innerValue(), $precision);
                        break;

                    case CustomerPricingType::PRICE_A:
                        $price = $model->prices->first()->number()->price_a;
                        break;

                    case CustomerPricingType::PRICE_B:
                        $price = $model->prices->first()->number()->price_b;
                        break;

                    case CustomerPricingType::PRICE_C:
                        $price = $model->prices->first()->number()->price_c;
                        break;

                    case CustomerPricingType::PRICE_D:
                        $price = $model->prices->first()->number()->price_d;
                        break;

                    case CustomerPricingType::PRICE_E:
                        $price = $model->prices->first()->number()->price_e;
                        break;
                }

                break;
        }

        return $price;
    }

    protected function getPercentagePrice($price)
    {
        $precision = setting('monetary.precision');

        return $this->customer->pricing_rate
            ->add(Decimal::create(100, $precision))
            ->div(Decimal::create(100, $precision))
            ->mul($price, $precision);
    }

    protected function getHistoricalPrice($model)
    {
        $precision = setting('monetary.precision');

        $defaultPrice = (int) $this->customer->historical_default_price === 0
            ? $model->prices->first()->number()->wholesale_price
            : $model->prices->first()->number()->selling_price;

        $latestPrice = is_null($model->customers->first())
            ? 0
            : $model->customers->first()->number()->latest_price;

        // If no historical record is found, set price as the default price
        if (is_null($model->customers->first())) {
            $price = $defaultPrice;
        } else {
            // If revert price to 0 feature is not set
            // get the latest price
            if ((int) $this->customer->historical_change_revert === 0) {
                $price = $latestPrice;
            } else {
                // If revert price to 0 feature is set
                // Check if latest price is same as default price
                // If not the same, revert to 0
                $price = $latestPrice !== $defaultPrice
                    ? number_format(0, $precision)
                    : $latestPrice;
            }
        }

        return $price;
    }

    protected function getUnitBySettings($model)
    {
        if ($this->isHistoricalUnit($model)) {
            return $model->customers->first()->latest_unit_id;
        } else if ($this->request->has('barcode') && setting('barcode.scan.default.unit') === BarcodeScanUnit::BARCODE_UNIT) {
            return $this->getUnitByBarcode($model);
        } else {
            return $model->default_unit;
        }
    }

    protected function getUnitQtyBySettings($model)
    {
        if ($this->isHistoricalUnit($model)) {
            return $model->customers->first()->number()->latest_unit_qty;
        } else {
            $unit = $this->getUnitBySettings($model);

            return array_first($model->presenter()->unit_list, function ($value, $key) use($unit) {
                return $value['id'] === $unit;
            })['attribute'];
        }
    }

    protected function isHistoricalUnit($model)
    {
        return setting('soi.unit.shown.base.on.history') && ! is_null($model->customers->first());
    }
}
