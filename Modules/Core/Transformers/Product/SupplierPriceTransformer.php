<?php

namespace Modules\Core\Transformers\Product;
use Modules\Core\Enums\BarcodeScanUnit;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\Product;
 */
class SupplierPriceTransformer extends SummaryAndPriceTransformer
{
    protected function getPriceBySettings($model)
    {
        return $this->isHistoricalPrice($model)
            ? $model->suppliers->first()->number()->latest_price
            : $model->prices->first()->number()->purchase_price;
    }

    protected function getUnitBySettings($model)
    {
        if ($this->isHistoricalUnit($model)) {
            return $model->suppliers->first()->latest_unit_id;
        } else if ($this->request->has('barcode') && setting('barcode.scan.default.unit') === BarcodeScanUnit::BARCODE_UNIT) {
            return $this->getUnitByBarcode($model);
        } else {
            return $model->default_unit;
        }
    }

    protected function getUnitQtyBySettings($model)
    {
        if ($this->isHistoricalUnit($model)) {
            return $model->suppliers->first()->number()->latest_unit_qty;
        } else {
            $unit = $this->getUnitBySettings($model);

            return array_first($model->presenter()->unit_list, function ($value, $key) use($unit) {
                return $value['id'] === $unit;
            })['attribute'];
        }
    }

    protected function isHistoricalUnit($model)
    {
        return setting('poi.unit.based.on.history') && ! is_null($model->suppliers->first());
    }

    protected function isHistoricalPrice($model)
    {
        return setting('poi.price.based.on.history') && ! is_null($model->suppliers->first());
    }
}
