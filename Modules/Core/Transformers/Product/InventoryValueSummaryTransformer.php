<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductInventoryValueSummary;

/**
 * Class SummaryTransformer
 */
class InventoryValueSummaryTransformer extends TransformerAbstract
{
    /**
     * @param ProductInventoryValueSummary $model
     * @return array
     */
    public function transform(ProductInventoryValueSummary $model)
    {
        return [
            'positive_inventory'      => $model->number()->positive_inventory,
            'negative_inventory'      => $model->number()->negative_inventory,
            'positive_ma_cost_amount' => $model->number()->positive_ma_cost_amount,
            'negative_ma_cost_amount' => $model->number()->negative_ma_cost_amount,
            'positive_cost_amount'    => $model->number()->positive_cost_amount,
            'negative_cost_amount'    => $model->number()->negative_cost_amount
        ];
    }
}
