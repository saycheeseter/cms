<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;
use Lang;

/**
 * Class InventoryTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class SummaryTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'        => $model->id,
            'inventory' => $model->summaries->first()->number()->qty
        ];
    }
}
