<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductPeriodicSales;
use Litipk\BigNumbers\Decimal;
use Lang;

/**
 * Class PeriodicSalesTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class PeriodicSalesTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param ProductPeriodicSales $model
     *
     * @return array
     */
    public function transform(ProductPeriodicSales $model)
    {
        $fields = ['total_qty', 'total_amount'];

        $attributes = $model->getAttributes();

        $data = [
            'name' => $model->name,
            'chinese_name' => $model->chinese_name,
            'barcode' => $model->barcode,
            'inventory' => $model->number()->inventory
        ];

        foreach ($attributes as $key => $value) {
            foreach ($fields as $field) {
                if(strpos($key, $field) !== false) {
                    $data[$key] = number_format(
                        Decimal::create(app('NumberFormatter')->parse($value), setting('monetary.precision'))->innerValue(),
                        setting('monetary.precision')
                    );
                    break;
                }
            }
        }

        return $data;
    }
}