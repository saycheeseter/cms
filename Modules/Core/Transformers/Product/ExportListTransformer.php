<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductPriceSummary;
use Modules\Core\Entities\Product;

/**
 * Class ListTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ExportListTransformer extends TransformerAbstract
{

    /**
     * Transform the Product entity
     * @param ProductPriceSummary $model
     *
     * @return array
     */
    public function transform(ProductPriceSummary $model)
    {
        return [
            'barcode'          => $model->barcode,
            'stock_no'         => $model->stock_no,
            'name'             => $model->name,
            'chinese_name'     => $model->chinese_name ?? '',
            'description'      => $model->description ?? '',
            'memo'             => $model->memo ?? '',
            'brand'            => $model->brand_name ?? '',
            'category'         => $model->category_name ?? '',
            'supplier'         => $model->supplier_name ?? '',
            'group_type_label' => $model->presenter()->group_type_label,
            'purchase_price'   => $model->purchase_price,
            'selling_price'    => $model->selling_price,
            'wholesale_price'  => $model->wholesale_price,
            'price_a'          => $model->price_a,
            'price_b'          => $model->price_b,
            'price_c'          => $model->price_c,
            'price_d'          => $model->price_d,
            'price_e'          => $model->price_e,
            'inventory'        => $model->inventory,
        ];
    }
}

