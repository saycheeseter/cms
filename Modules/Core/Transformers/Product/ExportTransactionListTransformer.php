<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductTransactionList;
use Litipk\BigNumbers\Decimal;
use Lang;

/**
 * Class TransactionListTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ExportTransactionListTransformer extends TransformerAbstract
{
    /**
     * Transform the Product entity
     * @param ProductTransactionList $model
     *
     * @return array
     */
    public function transform(ProductTransactionList $model)
    {
        return [
            'barcode'                          => $model->barcode,
            'product'                          => $model->name,
            'chinese_name'                     => $model->chinese_name,
            'beginning'                        => $model->beginning,
            'purchase_inbound'                 => $model->purchase_inbound,
            'purchase_return_outbound'         => $model->purchase_return_outbound,
            'sales_outbound'                   => $model->sales_outbound,
            'sales_return_inbound'             => $model->sales_return_inbound,
            'stock_delivery_inbound'           => $model->stock_delivery_inbound,
            'stock_delivery_outbound'          => $model->stock_delivery_outbound,
            'stock_return_inbound'             => $model->stock_return_inbound,
            'adjustment_increase'              => $model->adjustment_increase,
            'stock_return_outbound'            => $model->stock_return_outbound,
            'damage_outbound'                  => $model->damage_outbound,
            'adjustment_decrease'              => $model->adjustment_decrease,
            'product_conversion_increase'      => $model->product_conversion_increase,
            'product_conversion_decrease'      => $model->product_conversion_decrease,
            'auto_product_conversion_increase' => $model->auto_product_conversion_increase,
            'auto_product_conversion_decrease' => $model->auto_product_conversion_decrease,
            'pos_sales'                        => $model->pos_sales,
            'inventory'                        => $model->inventory
        ];
    }
}