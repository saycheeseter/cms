<?php

namespace Modules\Core\Transformers\Product;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Product;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Product entity
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
