<?php

namespace Modules\Core\Transformers\Product;

use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;
use Modules\Core\Enums\BarcodeScanUnit;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\Manageable;
use Modules\Core\Enums\BranchPrice;
use Illuminate\Http\Request;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\Product;
 */
class SummaryAndPriceTransformer extends TransformerAbstract
{
    protected $request;

    public function __construct()
    {
        $this->request = resolve(Request::class);
    }

    /**
     * Transform the Product entity
     * 
     * @param Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        $price = $this->getPriceBySettings($model);

        $unit = $this->getUnitBySettings($model);

        $unitQty = $this->getUnitQtyBySettings($model);

        return [
            'id'           => $model->id,
            'manage_type'  => $model->manage_type,
            'barcode_list' => $model->presenter()->barcode_list,
            'name'         => $model->name,
            'stock_no'     => $model->stock_no,
            'chinese_name' => $model->chinese_name,
            'units'        => $model->presenter()->unit_list,
            'unit_id'      => $unit,
            'unit_qty'     => $unitQty,
            'inventory'    => $model->summaries->first()->number()->qty,
            'price'        => $price,
            'cost'         => $model->prices->first()->number()->purchase_price,
            'group_type'   => $model->group_type,
            'components'   => $this->transformComponents($model->components, $model->group_type) ?? [],
            'manageable'   => $model->manageable,
            'product_type' => $model->type
        ];
    }

    protected function getUnitByBarcode($model)
    {
        $that = $this;

        return $model->barcodes
            ->filter(function ($item, $key) use ($that) {
                return Str::lower($item->code) === Str::lower($that->request->get('barcode'));
            })
            ->first()
            ->uom_id;
    }

    protected function getUnitBySettings($model)
    {
        if ($this->request->has('barcode') && setting('barcode.scan.default.unit') === BarcodeScanUnit::BARCODE_UNIT) {
            return $this->getUnitByBarcode($model);
        } else {
            return $model->default_unit;
        }
    }

    protected function getUnitQtyBySettings($model)
    {
        $unit = $this->getUnitBySettings($model);

        return array_first($model->presenter()->unit_list, function ($value, $key) use($unit) {
            return $value['id'] === $unit;
        })['attribute'];
    }

    protected function getPriceBySettings($model)
    {
        $price = 0;

        switch ($this->request->get('price')) {
            case BranchPrice::PURCHASE:
                $price = $model->prices->first()->number()->purchase_price;
                break;

            case BranchPrice::WHOLESALE:
                $price = $model->prices->first()->number()->wholesale_price;
                break;

            case BranchPrice::SELLING:
                $price = $model->prices->first()->number()->selling_price;
                break;

            case BranchPrice::A:
                $price = $model->prices->first()->number()->price_a;
                break;

            case BranchPrice::B:
                $price = $model->prices->first()->number()->price_b;
                break;

            case BranchPrice::C:
                $price = $model->prices->first()->number()->price_c;
                break;

            case BranchPrice::D:
                $price = $model->prices->first()->number()->price_d;
                break;

            case BranchPrice::E:
                $price = $model->prices->first()->number()->price_e;
                break;
        }

        return $price;
    }

    protected function transformComponents($components, $type)
    {
        $data = [];

        foreach ($components as $key => $component) {
            $data[] = [
                'product_id' => $component->id,
                'qty' => $component->pivot->number()->qty,
                'group_type' => $type
            ];
        }

        return $data;
    }
}
