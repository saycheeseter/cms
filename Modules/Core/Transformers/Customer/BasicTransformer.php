<?php 

namespace Modules\Core\Transformers\Customer;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Customer;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Branch;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Branch entity
     * @param Customer $model
     *
     * @return array
     */
    public function transform(Customer $model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'name'         => $model->name,
            'salesman'     => $model->salesman->full_name,
            'term'         => $model->term,
            'website'      => $model->website,
            'memo'         => $model->memo,
            'pricing_type' => $model->presenter()->pricing_type,
            'status'       => $model->presenter()->status,
            'credit_limit' => $model->number()->credit_limit
        ];
    }
}