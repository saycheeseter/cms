<?php

namespace Modules\Core\Transformers\Customer;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Customer;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Customer;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Customer entity
     * @param Customer $model
     *
     * @return array
     */
    public function transform(Customer $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
