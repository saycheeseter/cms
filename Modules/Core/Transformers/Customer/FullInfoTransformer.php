<?php 
    
namespace Modules\Core\Transformers\Customer;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Customer;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Customer;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Customer entity
     * @param Customer $model
     *
     * @return array
     */
    public function transform(Customer $model)
    {
        return [
            'id'                        => $model->id,
            'name'                      => $model->name,
            'code'                      => $model->code,
            'memo'                      => $model->memo,
            'credit_limit'              => $model->number()->credit_limit,
            'pricing_type'              => $model->pricing_type,
            'historical_change_revert'  => $model->historical_change_revert,
            'historical_default_price'  => $model->historical_default_price,
            'pricing_rate'              => $model->number()->pricing_rate,
            'salesman_id'               => $model->salesman_id,
            'website'                   => $model->website,
            'term'                      => $model->term,
            'status'                    => $model->status,
            'due'                       => $model->credit ? $model->credit->number()->due : 0
        ];
    }
}