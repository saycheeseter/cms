<?php

namespace Modules\Core\Transformers\Terminal;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Terminal;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Terminal;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the Terminal entity
     * @param Terminal $model
     *
     * @return array
     */
    public function transform(Terminal $model)
    {
        return [
            'id'        => $model->id,
            'number'    => $model->number,
            'branch_id' => $model->branch_id
        ];
    }
}