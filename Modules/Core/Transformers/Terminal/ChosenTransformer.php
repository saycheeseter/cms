<?php

namespace Modules\Core\Transformers\Terminal;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Terminal;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Terminal;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Terminal entity
     * @param Terminal $model
     *
     * @return array
     */
    public function transform(Terminal $model)
    {
        return [
            'id'    => $model->id,
            'number' => $model->number,
        ];
    }
}