<?php

namespace Modules\Core\Transformers\BankTransaction;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BankTransaction;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BankTransaction;
 */
class BasicTransformer extends TransformerAbstract
{
    public function transform(BankTransaction $model)
    {
        return [
            'id'               => $model->id,
            'transaction_date' => $model->transaction_date->toDateString(),
            'type'             => $model->type,
            'amount'           => $model->number()->amount,
            'from_bank'        => $model->from_bank,
            'to_bank'          => $model->to_bank,
            'remarks'          => $model->remarks
        ];
    }
}
