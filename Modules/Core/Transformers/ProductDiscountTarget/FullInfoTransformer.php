<?php

namespace Modules\Core\Transformers\ProductDiscountTarget;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductDiscountTarget;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductDiscountTarget;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductDiscountTarget entity
     * @param ProductDiscountTarget $model
     *
     * @return array
     */
    public function transform(ProductDiscountTarget $model)
    {
        return [
            'target_id'         => $model->target_id,
            'target_type'       => $model->discount->target_type,
            'target_type_label' => $model->presenter()->target_type_label,
            'target_name'       => $model->presenter()->target_name
        ];
    }
}
