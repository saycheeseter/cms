<?php

namespace Modules\Core\Transformers\InventoryAdjust;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryAdjust;
use Lang;

/**
 * Class FooterPrintTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryAdjust;
 */
class FooterPrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryAdjust entity
     *
     * @param InventoryAdjust $model
     *
     * @return array
     */
    public function transform(InventoryAdjust $model)
    {
        return [
            'total_qty'                         => $model->number()->total_qty,
            'total_lost_qty'                    => $model->number()->total_lost_qty,
            'total_gain_qty'                    => $model->number()->total_gain_qty,
            'total_gain_lost_qty'               => $model->number()->total_gain_lost_qty,
            'total_lost_amount_cost'            => $model->number()->total_lost_amount,
            'total_gain_amount_cost'            => $model->number()->total_gain_amount,
            'total_gain_lost_amount_cost'       => $model->number()->total_amount,
        ];
    }
}
