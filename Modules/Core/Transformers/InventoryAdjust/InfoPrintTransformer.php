<?php

namespace Modules\Core\Transformers\InventoryAdjust;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryAdjust;
use Lang;
use Auth;

/**
 * Class InfoPrintTransformer
 * @package namespace Modules\Core\Transformers\InventoryAdjust;
 */
class InfoPrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryAdjust entity
     * @param InventoryAdjust $model
     *
     * @return array
     */
    public function transform(InventoryAdjust $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
        ];
    }
}
