<?php

namespace Modules\Core\Transformers\InventoryAdjust;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryAdjust;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\InventoryAdjust;
 */
class ExportListTransformer extends TransformerAbstract
{

    /**
     * Transform the InventoryAdjust entity
     * @param InventoryAdjust $model
     *
     * @return array
     */
    public function transform(InventoryAdjust $model)
    {
        $fields = [
            'approval_status'  => $model->presenter()->approval,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->sheet_number,
            'created_from'     => $model->from->name,
            'created_for'      => $model->for->name,
            'for_location'     => $model->location->name,
            'amount'           => $model->total_amount,
            'lost_amount'      => $model->total_lost_amount,
            'gain_amount'      => $model->total_gain_amount,
            'remarks'          => $model->remarks,
            'requested_by'     => $model->requested->full_name,
            'created_by'       => $model->creator->full_name,
            'created_date'     => $model->created_at->toDateTimeString(),
            'audited_by'       => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'     => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'deleted'          => $model->presenter()->deleted,
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
