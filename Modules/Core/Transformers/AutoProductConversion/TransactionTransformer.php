<?php

namespace Modules\Core\Transformers\AutoProductConversion;

use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Traits\HasMorphMap;
use League\Fractal\TransformerAbstract;

/**
 * Class TransactionTransformer
 * @package namespace Modules\Core\Transformers\AutoProductConversion;
 */
class TransactionTransformer extends TransformerAbstract
{
    use HasMorphMap;

    /**
     * Transform the AutoProductConversion entity
     * @param AutoProductConversion $model
     *
     * @return array
     */
    public function transform(AutoProductConversion $model)
    {
        $reference = $model->transaction->transaction;

        return [
            'transaction_id'   => $reference->id,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->presenter()->reference_number,
            'particulars'      => $model->presenter()->transaction_label,
            'qty'              => $model->number()->qty,
            'price'            => $model->number()->ma_cost,
            'amount'           => $model->number()->amount,
            'prepared_by'      => $model->creator->full_name,
            'remarks'          => $model->presenter()->group_type_label,
            'module'           => $model->presenter()->module
        ];
    }
}