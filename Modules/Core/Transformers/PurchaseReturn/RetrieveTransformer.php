<?php

namespace Modules\Core\Transformers\PurchaseReturn;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseReturn;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\PurchaseReturn;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the PurchaseReturn entity
     * @param PurchaseReturn $model
     *
     * @return array
     */
    public function transform(PurchaseReturn $model)
    {
        return [
            'supplier_id'    => $model->supplier_id,
            'payment_method' => $model->payment_method,
            'term'           => $model->term, 
            'remarks'        => $model->remarks,
            'requested_by'   => $model->requested_by,
            'for_location'   => $model->for_location,
            'created_for'    => $model->created_for,
        ];
    }
}
