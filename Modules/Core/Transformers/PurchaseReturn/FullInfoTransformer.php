<?php

namespace Modules\Core\Transformers\PurchaseReturn;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseReturn;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PurchaseReturn;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PurchaseReturn entity
     * @param PurchaseReturn $model
     *
     * @return array
     */
    public function transform(PurchaseReturn $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'supplier_id'           => $model->supplier_id,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'payment_method'        => $model->payment_method,
            'term'                  => $model->term,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
