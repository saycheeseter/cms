<?php

namespace Modules\Core\Transformers\StockDeliveryOutboundDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBatch;
use Lang;

/**
 * Class ProductBatchTransformer
 * @package namespace Modules\Core\Transformers\StockDeliveryOutboundDetail;
 */
class ProductBatchTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductBatch entity
     * @param ProductBatch $model
     *
     * @return array
     */
    public function transform(ProductBatch $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'qty'           => $model->pivot->number()->remaining_qty,
            'remaining_qty' => $model->pivot->number()->remaining_qty,
        ];
    }
}
