<?php

namespace Modules\Core\Transformers\InventoryChainDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryChainDetail;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\InventoryChainDetail;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryChainDetail entity
     * 
     * @param InventoryChainDetail $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryChainDetail::class)) {
            return [];
        }

        return [
            'unit_barcode'                 => $model->presenter()->unit_barcode,
            'name'                         => $model->product->name,
            'stock_no'                     => $model->product->stock_no,
            'chinese_name'                 => $model->product->chinese_name,
            'qty'                          => $model->qty,
            'unit_code'                    => $model->unit->code,
            'unit_name'                    => $model->unit->name,
            'decimal_unit_code_specs'      => $model->presenter()->decimal_unit_code_and_specs,
            'whole_number_unit_code_specs' => $model->presenter()->whole_number_unit_code_and_specs,
            'unit_qty'                     => $model->unit_qty,
            'oprice'                       => $model->oprice,
            'price'                        => $model->price,
            'discount1'                    => $model->discount1,
            'discount2'                    => $model->discount2,
            'discount3'                    => $model->discount3,
            'discount4'                    => $model->discount4,
            'decimal_discounts'            => $model->presenter()->decimal_discounts,
            'whole_number_discounts'       => $model->presenter()->whole_number_discounts,
            'amount'                       => $model->amount,
            'remarks'                      => $model->remarks,
            'serials'                      => Fractal::collection($model->serials, new SerialTransformer)->getArray()['data']
        ];
    }
}
