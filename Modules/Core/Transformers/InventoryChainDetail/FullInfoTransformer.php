<?php

namespace Modules\Core\Transformers\InventoryChainDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryChainDetail;
use Modules\Core\Transformers\Product\TransactionComponentsTransformer;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialTransformer;
use Modules\Core\Transformers\ProductBatch\TransactionBatchTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryChainDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryChainDetail entity
     *
     * @param InventoryChainDetail $model
     *
     * @return array
     */
    public function transform(InventoryChainDetail $model)
    {
        $precision = setting('monetary.precision');

        return [
            'id'            => $model->id,
            'manage_type'   => $model->product->manage_type,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcode_list,
            'barcode'       => $model->presenter()->unit_barcode,
            'stock_no'      => $model->product->stock_no,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'units'         => $model->presenter()->unit_list,
            'unit_id'       => $model->unit_id,
            'serials'       => Fractal::collection($model->serials, new SerialTransformer)->getArray()['data'],
            'batches'       => Fractal::collection($model->batches, new TransactionBatchTransformer)->getArray()['data'],
            'unit_qty'      => $model->number()->unit_qty,
            'qty'           => $model->number()->qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'cost'          => $model->number()->cost,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'remarks'       => $model->remarks,
            'reference_id'  => $model->reference_id ?? null,
            'invoice_qty'   => is_null($model->reference) ? number_format(0, $precision) : $model->reference->number()->total_qty,
            'remaining_qty' => is_null($model->reference) ? number_format(0, $precision) : $model->reference->number()->remaining_qty,
            'spare_qty'     => is_null($model->reference)
                ? number_format(0, $precision)
                : $model->number()->spare_qty,
            'components'    => Fractal::collection($model->components, new TransactionComponentsTransformer)->getArray()['data'],
            'group_type'    => $model->group_type,
            'manageable'    => $model->manageable,
            'product_type'  => $model->product_type
        ];
    }
}
