<?php

namespace Modules\Core\Transformers\InventoryChainDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryChainDetail;
use Modules\Core\Transformers\Product\TransactionComponentsTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryChainDetail;
 */
class ImportTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryChainDetail entity
     *
     * @param InventoryChainDetail $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryChainDetail::class)) {
            return [];
        }

        return [
            'manage_type'  => $model->product->manage_type,
            'product_id'   => $model->product_id,
            'barcode_list' => $model->presenter()->barcode_list,
            'barcode'      => $model->presenter()->unit_barcode,
            'stock_no'     => $model->product->stock_no,
            'name'         => $model->product->name,
            'chinese_name' => $model->product->chinese_name,
            'units'        => $model->presenter()->unit_list,
            'unit_id'      => $model->unit_id,
            'unit_qty'     => $model->number()->unit_qty,
            'qty'          => $model->number()->qty,
            'oprice'       => $model->number()->oprice,
            'price'        => $model->number()->price,
            'cost'         => $model->number()->cost,
            'discount1'    => $model->number()->discount1,
            'discount2'    => $model->number()->discount2,
            'discount3'    => $model->number()->discount3,
            'discount4'    => $model->number()->discount4,
            'amount'       => $model->number()->amount,
            'remarks'      => $model->remarks,
            'components'   => Fractal::collection($model->components, new TransactionComponentsTransformer)->getArray()['data'],
            'group_type'   => $model->group_type,
            'manageable'   => $model->manageable,
            'product_type' => $model->product_type
        ];
    }
}
