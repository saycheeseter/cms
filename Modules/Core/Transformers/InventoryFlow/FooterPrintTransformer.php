<?php

namespace Modules\Core\Transformers\InventoryFlow;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryFlow;
use Lang;

/**
 * Class FooterPrintTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryFlow;
 */
class FooterPrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryFlow entity
     *
     * @param InventoryFlow $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryFlow::class)) {
            return [];
        }

        return [
            'net_total'                 => $model->number()->total_amount,
            'total_qty'                 => $model->number()->total_qty,
        ];
    }
}
