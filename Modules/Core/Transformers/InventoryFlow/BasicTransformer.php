<?php

namespace Modules\Core\Transformers\InventoryFlow;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryFlow;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryFlow;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryFlow entity
     *
     * @param InventoryFlow $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryFlow::class)) {
            return [];
        }

        return [
            'id'            => (string) $model->id,
            'sheet_number' => $model->sheet_number,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'  => $model->remarks,
        ];
    }
}
