<?php

namespace Modules\Core\Transformers\ProductSupplier;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductSupplier;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductSupplier;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductSupplier entity
     * @param ProductSupplier $model
     *
     * @return array
     */
    public function transform(ProductSupplier $model)
    {
        return [
            'supplier'                   => $model->supplier->full_name,
            'product'                    => $model->product->name,
            'latest_unit'                => $model->unit->name,
            'latest_unit_qty'            => $model->number()->latest_unit_qty,
            'latest_price'               => $model->number()->latest_price,
            'latest_fields_updated_date' => $model->latest_fields_updated_date->toDateString(),
            'lowest_price'               => $model->number()->lowest_price,
            'highest_price'              => $model->number()->highest_price,
            'min_qty'                    => $model->number()->min_qty,
            'remarks'                    => $model->remarks,
        ];
    }
}
