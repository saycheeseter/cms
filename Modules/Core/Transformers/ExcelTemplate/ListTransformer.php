<?php

namespace Modules\Core\Transformers\ExcelTemplate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ExcelTemplate;
use Lang;

/**
 * Class ListTransformer
 * @package namespace Modules\Core\Transformers\ExcelTemplate;
 */
class ListTransformer extends TransformerAbstract
{

    /**
     * Transform the ExcelTemplate entity
     * @param ExcelTemplate $model
     *
     * @return array
     */
    public function transform(ExcelTemplate $model)
    {
        return [
            'id'          => (string) $model->id,
            'name'        => $model->name,
            'module'      => $model->presenter()->module,
            'created_by'  => $model->userCreated->full_name,
            'created_at'  => $model->created_at->toDateTimeString(),
            'modified_by' => is_null($model->userModified) ? '' : $model->userModified->full_name,
            'updated_at'  => $model->updated_at->toDateTimeString()
        ];
    }
}
