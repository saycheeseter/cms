<?php

namespace Modules\Core\Transformers\ExcelTemplate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ExcelTemplate;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ExcelTemplate;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the ExcelTemplate entity
     * @param ExcelTemplate $model
     *
     * @return array
     */
    public function transform(ExcelTemplate $model)
    {
        return [
            'id'            => (string) $model->id,
            'name'            => $model->name,
            'module'            => $model->module,
            'format'            => $model->format
        ];
    }
}
