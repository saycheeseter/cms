<?php

namespace Modules\Core\Transformers\Pos;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\PosDiscountedSummaryReport;

class DiscountedSummaryReportTransformer extends TransformerAbstract
{
    public function transform(PosDiscountedSummaryReport $model)
    {
        return [
            'terminal_number'     => $model->terminal_number,
            'transaction_date'    => $model->transaction_date->toDateTimeString(),
            'or_number'           => $model->or_number,
            'cashier_name'        => $model->cashier_name,
            'category_name'       => $model->category_name,
            'product_barcode'     => $model->product_barcode,
            'product_name'        => $model->product_name,
            'total_qty'           => $model->number()->total_qty,
            'oprice'              => $model->number()->oprice,
            'price'               => $model->number()->price,
            'discounted_price'    => $model->number()->discounted_price,
            'amount'              => $model->number()->amount,
            'discounted_amount'   => $model->number()->discounted_amount,
            'percentage_discount' => $model->number()->percentage_discount,
        ];
    }
}