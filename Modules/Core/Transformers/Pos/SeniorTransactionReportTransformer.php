<?php

namespace Modules\Core\Transformers\Pos;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\SeniorTransactionReport;
use Litipk\BigNumbers\Decimal;

class SeniorTransactionReportTransformer extends TransformerAbstract
{

	public function transform(SeniorTransactionReport $model)
	{
		return [
			'transaction_date' => $model->transaction_date->toDateString(),
			'or_number' => $model->or_number,
			'senior_number' => $model->senior_number,
			'senior_name' => $model->senior_name,
			'product_name' => $model->product_name,
			'qty' => $model->number()->qty,
			'price' => $model->number()->price,
		];
	}

}