<?php

namespace Modules\Core\Transformers\Pos;

use League\Fractal\TransformerAbstract;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\Hydration\PosSalesTerminalReport;
use Lang;

/**
 * Class SalesReportTransformer
 * @package namespace Modules\Core\Transformers\PosSales;
 */
class TerminalReportTransformer extends TransformerAbstract
{
    /**
     * Transform the PosSalesReport entity
     * @param PosSalesTerminalReport $model
     *
     * @return array
     */
    public function transform(PosSalesTerminalReport $model)
    {
        $data = [
            'transaction_date' => is_null($model->transaction_date)
                ? Lang::get('core::label.total.sales')
                : $model->transaction_date->toDateString()
        ];

        $attributes =  $model->getAttributes();

        foreach ($attributes as $key => $value) {
            if(strpos($key, 'terminal_') !== false){
                $data[$key] = number_format(
                    Decimal::create(
                        app('NumberFormatter')->parse($value),
                        setting('monetary.precision')
                    )->innerValue(),
                    setting('monetary.precision')
                );
            }
        }

        return $data;
    }
}
