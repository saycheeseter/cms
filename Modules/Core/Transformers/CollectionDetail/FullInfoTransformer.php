<?php

namespace Modules\Core\Transformers\CollectionDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CollectionDetail;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\CollectionDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the CollectionDetail entity
     * @param CollectionDetail $model
     *
     * @return array
     */
    public function transform(CollectionDetail $model)
    {
        return [
            'id'              => $model->id,
            'payment_method'  => $model->payment_method,
            'bank_account_id' => $model->bank_account_id,
            'check_no'        => $model->check_no,
            'check_date'      => $model->check_date ? $model->check_date->toDateString() : null,
            'amount'          => $model->number()->amount
        ];
    }
}