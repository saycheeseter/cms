<?php

namespace Modules\Core\Transformers\StockRequest;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockRequest;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\StockRequest;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the StockRequest entity
     * @param StockRequest $model
     *
     * @return array
     */
    public function transform(StockRequest $model)
    {
        return [
            'created_for'  => $model->created_for,
            'for_location' => $model->for_location
        ];
    }
}
