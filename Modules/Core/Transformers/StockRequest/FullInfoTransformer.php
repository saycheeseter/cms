<?php

namespace Modules\Core\Transformers\StockRequest;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockRequest;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\StockRequest;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the StockRequest entity
     * @param StockRequest $model
     *
     * @return array
     */
    public function transform(StockRequest $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'request_to'            => $model->request_to,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
