<?php

namespace Modules\Core\Transformers\StockRequest;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockRequest;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\StockRequest;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the StockRequest entity
     * @param StockRequest $model
     *
     * @return array
     */
    public function transform(StockRequest $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'request_to'            => $model->to->name,
            'request_to_address'    => $model->to->address,
        ];
    }
}
