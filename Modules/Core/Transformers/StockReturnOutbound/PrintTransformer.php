<?php

namespace Modules\Core\Transformers\StockReturnOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockReturnOutbound;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\StockReturnOutbound;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the StockReturnOutbound entity
     * @param StockReturnOutbound $model
     *
     * @return array
     */
    public function transform(StockReturnOutbound $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'to_branch'             => $model->deliverTo->name,
            'to_branch_address'     => $model->deliverTo->address,
            'deliver_to_location'   => $model->deliverToLocation->name,
            'invoice_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
