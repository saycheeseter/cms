<?php

namespace Modules\Core\Transformers\StockReturnOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockReturnOutbound;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\StockReturnOutbound;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the StockReturnOutbound entity
     * @param StockReturnOutbound $model
     *
     * @return array
     */
    public function transform(StockReturnOutbound $model)
    {
        return [
            //deliver_to_location of outbound
            //will be the for_location value of the inbound when retrieved
            //reason why use for_location key on deliver_to_location
            'for_location'                 => $model->deliver_to_location,
            'requested_by'                 => $model->requested_by,
            'delivery_from'                => $model->created_for,
            'delivery_from_location'       => $model->for_location,
            'delivery_from_label'          => $model->for->name,
            'delivery_from_location_label' => $model->location->name,
            'remarks'                      => $model->remarks,
        ];
    }
}
