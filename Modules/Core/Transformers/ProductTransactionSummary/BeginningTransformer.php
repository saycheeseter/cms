<?php

namespace Modules\Core\Transformers\ProductTransactionSummary;

use League\Fractal\TransformerAbstract;

/**
 * Class BeginningTransformer
 * 
 * @package namespace Modules\Core\Transformers\ProductTransactionSummary;
 */
class BeginningTransformer extends TransformerAbstract
{
    private $default = [
        'beginning' => '0.00'
    ];

    public function transform($model)
    {
        if(is_null($model)) {
            return $this->default;
        }

        return ['beginning' => $model->number()->beginning];
    }
}
