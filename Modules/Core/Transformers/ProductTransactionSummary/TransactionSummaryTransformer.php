<?php

namespace Modules\Core\Transformers\ProductTransactionSummary;

use League\Fractal\TransformerAbstract;

/**
 * Class TransactionSummaryTransformer
 * @package namespace Modules\Core\Transformers\ProductTransactionSummary;
 */
class TransactionSummaryTransformer extends TransformerAbstract
{
    private $default = [
        'purchase_inbound' => '0.00',
        'stock_delivery_inbound' => '0.00',
        'stock_return_inbound' => '0.00',
        'sales_return_inbound' => '0.00',
        'adjustment_increase' => '0.00',
        'purchase_return_outbound' => '0.00',
        'stock_delivery_outbound' => '0.00',
        'stock_return_outbound' => '0.00',
        'sales_outbound' => '0.00',
        'damage_outbound' => '0.00',
        'adjustment_decrease' => '0.00',
        'product_conversion_increase' => '0.00',
        'product_conversion_decrease' => '0.00',
        'auto_product_conversion_increase' => '0.00',
        'auto_product_conversion_decrease' => '0.00',
        'pos_sales' => '0.00'
    ];

    public function transform($model)
    {
        if(is_null($model)) {
            return $this->default;
        }

        return [
            'purchase_inbound'                 => $model->number()->purchase_inbound,
            'stock_delivery_inbound'           => $model->number()->stock_delivery_inbound,
            'stock_return_inbound'             => $model->number()->stock_return_inbound,
            'sales_return_inbound'             => $model->number()->sales_return_inbound,
            'adjustment_increase'              => $model->number()->adjustment_increase,
            'purchase_return_outbound'         => $model->number()->purchase_return_outbound,
            'stock_delivery_outbound'          => $model->number()->stock_delivery_outbound,
            'stock_return_outbound'            => $model->number()->stock_return_outbound,
            'sales_outbound'                   => $model->number()->sales_outbound,
            'damage_outbound'                  => $model->number()->damage_outbound,
            'adjustment_decrease'              => $model->number()->adjustment_decrease,
            'product_conversion_increase'      => $model->number()->product_conversion_increase,
            'product_conversion_decrease'      => $model->number()->product_conversion_decrease,
            'auto_product_conversion_increase' => $model->number()->auto_product_conversion_increase,
            'auto_product_conversion_decrease' => $model->number()->auto_product_conversion_decrease,
            'pos_sales'                        => $model->number()->pos_sales
        ];
    }
}
