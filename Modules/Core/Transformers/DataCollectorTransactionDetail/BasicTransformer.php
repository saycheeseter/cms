<?php

namespace Modules\Core\Transformers\DataCollectorTransactionDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\DataCollectorTransactionDetail;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\DataCollectorTransactionDetail;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the DataCollectorTransactionDetail entity
     * @param DataCollectorTransactionDetail $model
     *
     * @return array
     */
    public function transform(DataCollectorTransactionDetail $model)
    {
        return [
            'id'             => $model->id,
            'transaction_id' => $model->transaction_id,
            'product_id'     => $model->product_id,
            'qty'            => $model->number()->qty,
            'price'          => $model->number()->price,
            'unit_id'        => $model->unit_id,
            'unit_qty'       => $model->number()->unit_qty
        ];
    }
}