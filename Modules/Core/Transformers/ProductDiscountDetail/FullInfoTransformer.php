<?php

namespace Modules\Core\Transformers\ProductDiscountDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductDiscountDetail;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductDiscountDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductDiscountDetail entity
     * @param ProductDiscountDetail $model
     *
     * @return array
     */
    public function transform(ProductDiscountDetail $model)
    {
        return [
            'entity_id'         => $model->entity_id,
            'entity_type'       => $model->discount->select_type,
            'entity_type_label' => $model->presenter()->entity_type_label,
            'name'              => $model->presenter()->entity_name,
            'discount_type'     => $model->discount_type,
            'discount_value'    => $model->number()->discount_value,
            'qty'               => $model->number()->qty,
        ];
    }
}
