<?php

namespace Modules\Core\Transformers\PurchaseInbound;

use Illuminate\Database\Eloquent\Relations\Relation;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseInbound;
use Lang;

/**
 * Class PaymentTransformer
 * @package namespace Modules\Core\Transformers\PurchaseInbound;
 */
class PaymentTransformer extends TransformerAbstract
{
    /**
     * Transform the PurchaseInbound entity
     * @param PurchaseInbound $model
     *
     * @return array
     */
    public function transform(PurchaseInbound $model)
    {
        return [
            'reference_id'       => $model->id,
            'reference_type'     => array_search(get_class($model), Relation::morphMap()),
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'term'               => $model->term,
            'dr_no'              => $model->dr_no,
            'remarks'            => $model->remarks,
            'amount'             => $model->number()->total_amount,
            'paid_amount'        => $model->number()->paid_amount,
            'remaining'          => $model->number()->remaining_amount,
            'payable'            => ''
        ];
    }
}