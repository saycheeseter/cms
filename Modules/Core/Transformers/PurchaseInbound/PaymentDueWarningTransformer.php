<?php

namespace Modules\Core\Transformers\PurchaseInbound;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\PaymentDueWarning;

class PaymentDueWarningTransformer extends TransformerAbstract
{
    public function transform(PaymentDueWarning $model)
    {
        $now = Carbon::now();

        return [
            'id'               => $model->id,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'supplier'         => $model->supplier_full_name,
            'sheet_number'     => $model->sheet_number,
            'amount'           => $model->number()->total_amount,
            'remaining'        => $model->number()->remaining_amount,
            'term'             => $model->term,
            'days_overdue'     => $now->gt($model->due_date) 
                ? Carbon::now()->diffInDays($model->due_date)
                : 0,
            'due_date'         => $model->due_date->toDateTimeString(),
            'memo'             => $model->remarks
        ];
    }
}