<?php

namespace Modules\Core\Transformers\PurchaseInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseInbound;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PurchaseInbound;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PurchaseInbound entity
     * @param PurchaseInbound $model
     *
     * @return array
     */
    public function transform(PurchaseInbound $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'supplier_id'           => $model->supplier_id,
            'payment_method'        => $model->payment_method,
            'term'                  => $model->term,
            'remarks'               => $model->remarks,
            'dr_no'                 => $model->dr_no,
            'supplier_invoice_no'   => $model->supplier_invoice_no,
            'supplier_box_count'    => $model->supplier_box_count,
            'requested_by'          => $model->requested_by,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'reference_id'          => $model->reference_id != null ? $model->reference_id : null,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'transactionable_type'  => $model->transactionable_type ?? null,
            'transactionable_id'    => $model->transactionable_id ?? null,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
