<?php

namespace Modules\Core\Transformers\PurchaseInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\PaymentDueWarningSummary;

class PaymentDueWarningSummaryTransformer extends TransformerAbstract
{
    public function transform(PaymentDueWarningSummary $model)
    {
        return [
            'total_count' => $model->number()->total_count,
            'total_amount' => $model->number()->total_amount,
            'total_remaining' => $model->number()->total_remaining
        ];
    }
}