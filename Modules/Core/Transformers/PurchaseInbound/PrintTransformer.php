<?php

namespace Modules\Core\Transformers\PurchaseInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseInbound;
use Lang;
use Auth;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PurchaseInbound;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the PurchaseInbound entity
     * @param PurchaseInbound $model
     *
     * @return array
     */
    public function transform(PurchaseInbound $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'payment_method'        => $model->payment->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'supplier'              => $model->supplier->full_name,
            'supplier_address'      => $model->supplier->address,
            'term'                  => $model->term,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'dr_no'                 => $model->dr_no,
            'supplier_invoice_no'   => $model->supplier_invoice_no,
            'supplier_box_count'    => $model->supplier_box_count,
            'invoice_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
