<?php

namespace Modules\Core\Transformers\PaymentReference;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PaymentReference;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PaymentReference;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PaymentReference entity
     * @param PaymentReference $model
     *
     * @return array
     */
    public function transform(PaymentReference $model)
    {
        $transaction = $model->reference;

        return [
            'id'               => $model->id,
            'reference_id'     => $transaction->id,
            'reference_type'   => $model->reference_type,
            'transaction_date' => $transaction->transaction_date->toDateTimeString(),
            'sheet_number'     => $transaction->sheet_number,
            'term'             => $transaction->term,
            'dr_no'            => $transaction->dr_no,
            'remarks'          => $transaction->remarks,
            'amount'           => $transaction->number()->total_amount,
            'paid_amount'      => $transaction->number()->paid_amount,
            'payable'          => $model->number()->payable,
            'remaining'        => $transaction->number()->remaining_amount,
        ];
    }
}