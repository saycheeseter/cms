<?php

namespace Modules\Core\Transformers\InventoryAdjustDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryAdjustDetail;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryAdjustDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryAdjustDetail entity
     *
     * @param InventoryAdjustDetail $model
     *
     * @return array
     */
    public function transform(InventoryAdjustDetail $model)
    {
        return [
            'id'           => $model->id,
            'manage_type'  => $model->product->manage_type,
            'product_id'   => $model->product_id,
            'barcode_list' => $model->presenter()->barcode_list,
            'barcode'      => $model->presenter()->unit_barcode,
            'stock_no'     => $model->product->stock_no,
            'name'         => $model->product->name,
            'chinese_name' => $model->product->chinese_name,
            'units'        => $model->presenter()->unit_list,
            'unit_id'      => $model->unit_id,
            'unit_qty'     => $model->number()->unit_qty,
            'qty'          => $model->number()->qty,
            'pc_qty'       => $model->number()->pc_qty,
            'price'        => $model->number()->price,
            'cost'         => $model->number()->cost,
            'inventory'    => $model->number()->inventory,
            'remarks'      => $model->remarks,
            'serials'      => Fractal::collection($model->serials, new SerialTransformer)->getArray()['data'],
        ];
    }
}
