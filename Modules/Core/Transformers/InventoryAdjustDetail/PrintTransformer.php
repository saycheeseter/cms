<?php

namespace Modules\Core\Transformers\InventoryAdjustDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryAdjustDetail;
use Modules\Core\Transformers\ProductSerialNumber\BasicTransformer as SerialTransformer;
use Fractal;

/**
 * Class FullInfoTransformer
 * 
 * @package namespace Modules\Core\Transformers\InventoryAdjustDetail;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryAdjustDetail entity
     * 
     * @param InventoryAdjustDetail $model
     *
     * @return array
     */
    public function transform(InventoryAdjustDetail $model)
    {
        return [
            'unit_barcode'                 => $model->presenter()->unit_barcode,
            'name'                         => $model->product->name,
            'stock_no'                     => $model->product->stock_no,
            'chinese_name'                 => $model->product->chinese_name,
            'qty'                          => $model->qty,
            'unit_code'                    => $model->unit->code,
            'unit_name'                    => $model->unit->name,
            'decimal_unit_code_specs'      => $model->presenter()->decimal_unit_code_and_specs,
            'whole_number_unit_code_specs' => $model->presenter()->whole_number_unit_code_and_specs,
            'unit_qty'                     => $model->unit_qty,
            'pc_qty'                       => $model->pc_qty,
            'cost'                         => $model->price,
            'inventory'                    => $model->inventory,
            'amount'                       => $model->amount,
            'serials'                      => Fractal::collection($model->serials, new SerialTransformer)->getArray()['data']
        ];
    }
}
