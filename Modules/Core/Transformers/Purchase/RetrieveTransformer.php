<?php

namespace Modules\Core\Transformers\Purchase;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Purchase;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\Purchase;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the Purchase entity
     * @param Purchase $model
     *
     * @return array
     */
    public function transform(Purchase $model)
    {
        return [
            'supplier_id'          => $model->supplier_id,
            'payment_method'       => $model->payment_method,
            'term'                 => $model->term, 
            'remarks'              => $model->remarks,
            'requested_by'         => $model->requested_by,
            'created_for'          => $model->created_for,
            'for_location'         => $model->for_location,
            'transactionable_type' => $model->transactionable_type ?? null,
            'transactionable_id'   => $model->transactionable_id ?? null,
        ];
    }
}
