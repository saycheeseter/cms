<?php

namespace Modules\Core\Transformers\Purchase;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Purchase;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Purchase;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Purchase entity
     * @param Purchase $model
     *
     * @return array
     */
    public function transform(Purchase $model)
    {
        $fields = [
            'id'                           => $model->id,
            'sheet_number'                 => $model->sheet_number,
            'transaction_date'             => $model->transaction_date->toDateTimeString(),
            'supplier_id'                  => $model->supplier_id,
            'remarks'                      => $model->remarks,
            'requested_by'                 => $model->requested_by,
            'payment_method'               => $model->payment_method,
            'deliver_until'                => $model->deliver_until->toDateString(),
            'term'                         => $model->term,
            'created_for'                  => $model->created_for,
            'for_location'                 => $model->for_location,
            'deliver_to'                   => $model->deliver_to,
            'created_date'                 => $model->created_at->toDateTimeString(),
            'modified_date'                => $model->updated_at->toDateTimeString(),
            'approval_status'              => $model->approval_status,
            'approval_status_label'        => $model->presenter()->approval,
            'created_by'                   => $model->creator->full_name,
            'modified_by'                  => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'transactionable_type'         => $model->transactionable_type,
            'transactionable_id'           => $model->transactionable_id,
            'transactionable_sheet_number' => !is_null($model->transactionable) ? $model->transactionable->sheet_number : null,
            'is_deleted'                   => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
