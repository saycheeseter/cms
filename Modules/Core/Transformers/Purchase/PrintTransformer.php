<?php

namespace Modules\Core\Transformers\Purchase;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Purchase;
use Lang;
use Auth;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Purchase;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the Purchase entity
     * @param Purchase $model
     *
     * @return array
     */
    public function transform(Purchase $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'deliver_to'            => $model->deliverTo->name,
            'deliver_until'         => $model->deliver_until->toDateString(),
            'for_location'          => $model->location->name,
            'payment_method'        => $model->payment->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'supplier'              => $model->supplier->full_name,
            'supplier_address'      => $model->supplier->address,
            'term'                  => $model->term,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
        ];
    }
}
