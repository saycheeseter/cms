<?php

namespace Modules\Core\Transformers\Category;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Category;

/**
 * Class TreeTransformer
 * @package namespace Modules\Core\Transformers\Category;
 */
class TreeTransformer extends TransformerAbstract
{
    /**
     * Transform the Category entity
     * @param Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        $transformable = [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id,
            'children'      => array()
        ];

        foreach ($model->children as $key => $child) {
            $transformable['children'][] = $this->transform($child);
        }

        return $transformable;
    }
}
