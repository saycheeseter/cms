<?php

namespace Modules\Core\Transformers\Category;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Category;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Category;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the Category entity
     * @param Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id
        ];
    }
}
