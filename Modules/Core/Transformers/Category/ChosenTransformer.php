<?php

namespace Modules\Core\Transformers\Category;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Category;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Category;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Category entity
     * @param Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
