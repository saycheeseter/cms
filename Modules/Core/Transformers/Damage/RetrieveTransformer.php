<?php

namespace Modules\Core\Transformers\Damage;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Damage;
/**
 * Class RetrieveInvoiceTransformer
 *
 * @package namespace Modules\Core\Transformers\StockRequest;
 */
class RetrieveTransformer extends TransformerAbstract
{
    /**
     * Transform the Damage entity
     *
     * @param Damage $model
     *
     * @return array
     */
    public function transform(Damage $model)
    {
        return [
            'reason_id'    => $model->reason_id,
            'requested_by' => $model->requested_by,
            'remarks'      => $model->remarks,
            'for_location' => $model->for_location,
            'created_for'  => $model->created_for,
        ];
    }
}
