<?php

namespace Modules\Core\Transformers\Damage;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Damage;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Damage;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Damage entity
     * @param Damage $model
     *
     * @return array
     */
    public function transform(Damage $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'reason_id'             => $model->reason_id,
            'remarks'               => $model->remarks,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'requested_by'          => $model->requested_by,
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
