<?php

namespace Modules\Core\Transformers\Damage;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Damage;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\Damage;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the Damage entity
     * @param Damage $model
     *
     * @return array
     */
    public function transform(Damage $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'reason'                => $model->reason->name,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
        ];
    }
}
