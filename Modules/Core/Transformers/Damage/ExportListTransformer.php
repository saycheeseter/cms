<?php

namespace Modules\Core\Transformers\Damage;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Damage;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Damage;
 */
class ExportListTransformer extends TransformerAbstract
{

    /**
     * Transform the Damage entity
     * @param Damage $model
     *
     * @return array
     */
    public function transform(Damage $model)
    {
        $fields = [
            'approval_status'    => $model->presenter()->approval,
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'created_for'        => $model->for->name,
            'created_from'       => $model->from->name,
            'for_location'       => $model->location->name,
            'reason'             => $model->reason->name,
            'amount'             => $model->total_amount,
            'transaction_status' => $model->presenter()->transaction,
            'remarks'            => $model->remarks,
            'requested_by'       => $model->requested->full_name,
            'created_by'         => $model->creator->full_name,
            'created_date'       => $model->created_at->toDateTimeString(),
            'audited_by'         => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'       => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'deleted'            => $model->presenter()->deleted,
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
