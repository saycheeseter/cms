<?php

namespace Modules\Core\Transformers\ProductBatch;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBatch;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductBatch;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductBatch entity
     * @param ProductBatch $model
     *
     * @return array
     */
    public function transform(ProductBatch $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'qty'           => $model->owners->first()->number()->current_qty,
            'remaining_qty' => $model->owners->first()->number()->current_qty,
        ];
    }
}
