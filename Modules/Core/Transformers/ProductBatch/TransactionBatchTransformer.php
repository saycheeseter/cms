<?php

namespace Modules\Core\Transformers\ProductBatch;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBatch;
use Lang;

/**
 * Class TransactionBatchTransformer
 * @package namespace Modules\Core\Transformers\ProductBatch;
 */
class TransactionBatchTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductBatch entity
     * @param ProductBatch $model
     *
     * @return array
     */
    public function transform(ProductBatch $model)
    {
        return [
            'id'   => $model->id,
            'name' => $model->name,
            'qty'  => $model->pivot->number()->qty,
        ];
    }
}
