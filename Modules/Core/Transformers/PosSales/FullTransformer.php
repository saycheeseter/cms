<?php

namespace Modules\Core\Transformers\PosSales;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PosSales;

class FullTransformer extends TransformerAbstract
{
    public function transform(PosSales $model)
    {
        return [
            'id'                  => $model->id,
            'created_for'         => $model->for->name,
            'for_location'        => $model->location->name,
            'transaction_date'    => $model->transaction_date->toDateTimeString(),
            'transaction_number'  => $model->transaction_number,
            'or_number'           => $model->or_number,
            'terminal_number'     => $model->terminal_number,
            'cashier'             => $model->cashier->full_name ?? '-',
            'salesman'            => $model->salesman->full_name ?? '-',
            'customer_type_label' => $model->presenter()->customer_type_label,
            'customer'            => $model->presenter()->customer,
            'member'              => $model->member->full_name ?? '-',
            'senior_name'         => $model->senior_name ?? '-',
            'senior_number'       => $model->senior_number ?? '-',
            'total_amount'        => $model->number()->total_amount,
            'remarks'             => $model->remarks,
            'voided'              => [
                'value' => $model->voided,
                'label' => $model->presenter()->voided
            ]
        ];
    }

}