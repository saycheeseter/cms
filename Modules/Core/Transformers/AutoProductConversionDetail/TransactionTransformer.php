<?php

namespace Modules\Core\Transformers\AutoProductConversionDetail;

use Modules\Core\Entities\AutoProductConversionDetail;
use Modules\Core\Traits\HasMorphMap;
use League\Fractal\TransformerAbstract;

/**
 * Class TransactionTransformer
 * @package namespace Modules\Core\Transformers\AutoProductConversionDetail;
 */
class TransactionTransformer extends TransformerAbstract
{
    use HasMorphMap;

    /**
     * Transform the AutoProductConversionDetail entity
     * @param AutoProductConversionDetail $model
     *
     * @return array
     */
    public function transform(AutoProductConversionDetail $model)
    {
        $reference = $model->transaction->transaction->transaction;

        return [
            'transaction_id'   => $reference->id,
            'transaction_date' => $model->transaction->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->transaction->presenter()->reference_number,
            'particulars'      => $model->transaction->presenter()->transaction_label,
            'qty'              => $model->number()->total_qty,
            'price'            => $model->number()->ma_cost,
            'amount'           => $model->number()->amount,
            'prepared_by'      => $model->transaction->creator->full_name,
            'remarks'          => $model->transaction->presenter()->group_type_label,
            'module'           => $model->transaction->presenter()->module
        ];
    }
}