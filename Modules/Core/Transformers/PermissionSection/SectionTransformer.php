<?php

namespace Modules\Core\Transformers\PermissionSection;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PermissionSection;
use Lang;

/**
 * Class SectionTransformer
 * @package namespace Modules\Core\Transformers\PermissionSection;
 */
class SectionTransformer extends TransformerAbstract
{

    /**
     * Transform the PermissionSection entity
     * @param PermissionSection $model
     *
     * @return array
     */
    public function transform(PermissionSection $model)
    {
        return [
            'name' => $model->alias,
            'all' => false,
            'label' => (is_null($model->translation))
                ? $model->name
                : Lang::get(sprintf('core::%s', $model->translation)),
            'modules' => $this->modules($model)
        ];
    }

    /**
     * Populate all modules inside a section
     * 
     * @param  PermissionSection $model
     * @return array
     */
    private function modules(PermissionSection $model)
    {
        $modules = array();
        
        $collection = $model->modules;

        foreach ($collection->sortBy('sequence_number') as $keyModule => $module) {
            $permissions = $this->permissions($module);

            $modules[] = array(
                'label' => (is_null($module->translation)) 
                    ? $module->name 
                    : Lang::get(sprintf('core::%s', $module->translation)),
                'permissions' => $permissions
            );
        }

        return $modules;
    }

    /**
     * Populate all permissions inside a module
     * 
     * @param  Collection $module
     * @return array
     */
    private function permissions($module)
    {
        $permissions = array();

        foreach ($module->permissions->sortBy('sequence') as $keyPermission => $permission) {
            $permissions[] = array(
                'code' => $permission->id,
                'label' => (is_null($permission->translation))
                    ? $permission->name
                    : Lang::get(sprintf('core::%s', $permission->translation)),
            );
        }

        return $permissions;
    }
}
