<?php

namespace Modules\Core\Transformers\Member;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Member;

/**
 * Class ExportReportTransformer
 * @package namespace Modules\Core\Transformers\Member;
 */
class ExportReportTransformer extends TransformerAbstract
{

    /**
     * Transform the Member entity
     * @param Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'full_name'    => $model->full_name,
            'rate'         => $model->rate->name,
            'card_id'      => $model->card_id,
            'barcode'      => $model->barcode,
            'date_created' => $model->created_at->toDateString(),
            'points'       => $model->total_points
        ];
    }
}
