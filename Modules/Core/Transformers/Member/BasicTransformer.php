<?php

namespace Modules\Core\Transformers\Member;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Member;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Member;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Member entity
     * @param Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'id'                => $model->id,
            'full_name'         => $model->full_name,
            'member_type'       => $model->rate->name,
            'card_id'           => $model->card_id,
            'barcode'           => $model->barcode,
            'mobile'            => $model->mobile,
            'telephone'         => $model->telephone,
            'birth_date'        => $model->birth_date->toDateString(),
            'gender'            => $model->presenter()->gender,
            'email'             => $model->email,
            'address'           => $model->address,
            'memo'              => $model->memo,
            'registration_date' => $model->registration_date->toDateString(),
            'expiration_date'   => $model->expiration_date->toDateString(),
            'status'            => $model->presenter()->status,
        ];
    }
}
