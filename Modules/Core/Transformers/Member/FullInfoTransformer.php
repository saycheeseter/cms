<?php

namespace Modules\Core\Transformers\Member;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Member;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Member;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Member entity
     * @param Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'id'                    => $model->id,
            'rate_head_id'          => $model->rate_head_id,
            'card_id'               => $model->card_id,
            'barcode'               => $model->barcode,
            'full_name'             => $model->full_name,
            'mobile'                => $model->mobile,
            'telephone'             => $model->telephone,
            'address'               => $model->address,
            'email'                 => $model->email,
            'gender'                => $model->gender,
            'birth_date'            => $model->birth_date->toDateString(),
            'registration_date'     => $model->registration_date->toDateString(),
            'expiration_date'       => $model->expiration_date->toDateString(),
            'status'                => $model->status,
            'memo'                  => $model->memo
        ];
    }
}
