<?php

namespace Modules\Core\Transformers\Member;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Member;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Member;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Member entity
     * @param Member $model
     *
     * @return array
     */
    public function transform(Member $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->full_name,
        ];
    }
}