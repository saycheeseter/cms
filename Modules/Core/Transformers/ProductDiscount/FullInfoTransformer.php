<?php

namespace Modules\Core\Transformers\ProductDiscount;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductDiscount;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductDiscount;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductDiscount entity
     * @param ProductDiscount $model
     *
     * @return array
     */
    public function transform(ProductDiscount $model)
    {
        return [
            'id'              => $model->id,
            'code'            => $model->code,
            'name'            => $model->name,
            'valid_from'      => $model->valid_from->toDateTimeString(),
            'valid_to'        => $model->valid_to->toDateTimeString(),
            'status'          => $model->status,
            'mon'             => $model->mon,
            'tue'             => $model->tue,
            'wed'             => $model->wed,
            'thur'            => $model->thur,
            'fri'             => $model->fri,
            'sat'             => $model->sat,
            'sun'             => $model->sun,
            'target_type'     => $model->target_type,
            'discount_scheme' => $model->discount_scheme,
            'select_type'     => $model->select_type,
            'created_for'     => $model->created_for
        ];
    }
}
