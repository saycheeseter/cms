<?php

namespace Modules\Core\Transformers\ProductDiscount;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductDiscount;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductDiscount;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductDiscount entity
     * @param ProductDiscount $model
     *
     * @return array
     */
    public function transform(ProductDiscount $model)
    {
        return [
            'id' => $model->id,
            'code' => $model->code,
            'name' => $model->name,
            'valid_from' => $model->valid_from->toDateTimeString(),
            'valid_to' => $model->valid_to->toDateTimeString(),
            'target_type' => $model->presenter()->target_type,
            'discount_scheme' => $model->presenter()->discount_scheme,
            'select_type' => $model->presenter()->select_type,
            'status' => $model->presenter()->status,
            'created_for' => $model->for->name
        ];
    }
}
