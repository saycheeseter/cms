<?php

namespace Modules\Core\Transformers\Collection;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Collection;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Collection;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Collection entity
     * @param Collection $model
     *
     * @return array
     */
    public function transform(Collection $model)
    {
        return [
            'id'                    => $model->id,
            'location'              => $model->for->name,
            'date'                  => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'customer'              => $model->presenter()->customer(),
            'customer_type'         => $model->presenter()->customer_type(),
            'amount'                => $model->number()->total_amount,
            'remarks'               => $model->remarks,
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'deleted'               => $model->presenter()->deleted
        ];
    }
}