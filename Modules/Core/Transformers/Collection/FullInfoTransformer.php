<?php

namespace Modules\Core\Transformers\Collection;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Collection;

use Modules\Core\Enums\Customer;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Collection;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Collection entity
     * @param Collection $model
     *
     * @return array
     */
    public function transform(Collection $model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'created_for'           => $model->created_for,
            'customer_type'         => $model->customer_type,
            'customer'              => $model->customer_value,
            'entity_id'             => $model->customer_value,
            'remarks'               => $model->remarks,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'is_deleted'            => $model->is_deleted,
        ];
    }
}