<?php

namespace Modules\Core\Transformers\Sales;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Sales;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\Sales;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the Sales entity
     * @param Sales $model
     *
     * @return array
     */
    public function transform(Sales $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'customer'              => $model->presenter()->customer,
            'customer_address'      => $model->presenter()->customer_address,
            'salesman'              => $model->salesman->full_name,
            'term'                  => $model->term,
        ];
    }
}
