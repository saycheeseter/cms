<?php

namespace Modules\Core\Transformers\Sales;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Sales;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Sales;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Sales entity
     * @param Sales $model
     *
     * @return array
     */
    public function transform(Sales $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id) 
                ? $model->customer_id 
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id) 
                ? $model->customer_detail_id 
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'term'                  => $model->term,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
