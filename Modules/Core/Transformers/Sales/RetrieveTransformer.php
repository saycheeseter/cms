<?php

namespace Modules\Core\Transformers\Sales;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Sales;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\Sales;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the Sales entity
     * @param Sales $model
     *
     * @return array
     */
    public function transform(Sales $model)
    {
        return [
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'term'                  => $model->term,
            'for_location'          => $model->for_location,
            'created_for'           => $model->created_for,
        ];
    }
}
