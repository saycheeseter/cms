<?php

namespace Modules\Core\Transformers\Sales;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Sales;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Sales;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Sales entity
     * @param Sales $model
     *
     * @return array
     */
    public function transform(Sales $model)
    {
        $fields = [
            'id'                    => $model->id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'created_from'          => $model->from->name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'salesman'              => $model->salesman->full_name,
            'customer'              => $model->presenter()->customer,
            'remarks'               => $model->remarks,
            'term'                  => $model->term,
            'created_by'            => $model->creator->full_name,
            'created_date'          => $model->created_at->toDateTimeString(),
            'audited_by'            => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'          => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_status'    => $model->presenter()->transaction,
            'amount'                => $model->number()->total_amount,
            'deleted'               => $model->presenter()->deleted,
            'requested_by'          => $model->requested->full_name,
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
