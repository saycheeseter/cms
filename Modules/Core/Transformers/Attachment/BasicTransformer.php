<?php

namespace Modules\Core\Transformers\Attachment;

use League\Fractal\TransformerAbstract;
/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Attachment;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Attachment entity
     * @param Attachment $model
     *
     * @return array
     */
    public function transform($model)
    {   
        $path = sprintf('storage/%s', $model->filepath);

        return [
            'link'     => asset($path),
            'key'      => $model->key,
            'original' => $model->filename,
            'status'   => 'saved',
            'attribute' => getimagesize($path)
        ];
    }
}
