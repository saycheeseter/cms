<?php

namespace Modules\Core\Transformers\ProductBranchDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBranchDetail;

class BasicTransformer extends TransformerAbstract
{
    public function transform(ProductBranchDetail $model)
    {
        return [
            'branch_name'        => $model->location->branch->name ?? '',
            'branch_detail_id'   => $model->branch_detail_id,
            'branch_detail_name' => $model->location->name ?? '',
            'min'                => $model->number()->min,
            'max'                => $model->number()->max
        ];
    }
}
