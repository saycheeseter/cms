<?php

namespace Modules\Core\Transformers\BankTransactionReport;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\BankTransactionSummary;

/**
 * Class BeginningBalanceTransformer
 * @package namespace Modules\Core\Transformers\BankTransactionReport;
 */
class BeginningBalanceTransformer extends TransformerAbstract
{

    public function transform(BankTransactionSummary $model)
    {
        return [
            'total'  => $model->number()->total,
        ];
    }
}
