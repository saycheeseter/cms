<?php

namespace Modules\Core\Transformers\BankTransactionReport;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\BankTransactionReport;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BankTransactionReport;
 */
class BasicTransformer extends TransformerAbstract
{
    public function transform(BankTransactionReport $model)
    {
        return [
            'type'         => $model->type,
            'reference_id' => $model->reference_id,
            'date'         => $model->check_date->toDateString(),
            'particular'   => $model->particular,
            'in'           => $model->number()->in,
            'out'          => $model->number()->out,
        ];
    }
}
