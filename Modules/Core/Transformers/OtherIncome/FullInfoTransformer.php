<?php

namespace Modules\Core\Transformers\OtherIncome;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherIncome;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\OtherIncome;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the OtherIncome entity
     * @param OtherIncome $model
     *
     * @return array
     */
    public function transform(OtherIncome $model)
    {
        return [
            'id'               => $model->id,
            'reference_name'   => $model->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks ?? '',
            'payment_method'   => $model->payment_method,
            'created_by'       => $model->created_by,
            'created_from'     => $model->created_from,
            'bank_account_id'  => $model->bank_account_id,
            'check_date'       => $model->check_date ? $model->check_date->toDateTimeString() : null,
            'check_number'     => $model->check_number
        ];
    }
}
