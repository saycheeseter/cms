<?php

namespace Modules\Core\Transformers\ProductSerialNumber;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\AvailableProductSerialNumber;

class AvailableSerialTransformer extends TransformerAbstract
{
    /**
     * Transform the AvailableProductSerialNumber entity
     * @param AvailableProductSerialNumber $model
     *
     * @return array
     */
    public function transform(AvailableProductSerialNumber $model)
    {
        return [
            'branch'                      => $model->branch,
            'supplier_name'               => $model->supplier_name,
            'location'                    => $model->location,
            'serial_number'               => $model->serial_number,
            'expiration_date'             => $model->expiration_date ? $model->expiration_date->toDateString() : null,
            'manufacturing_date'          => $model->manufacturing_date ? $model->manufacturing_date->toDateString() : null,
            'admission_date'              => $model->admission_date ? $model->admission_date->toDateString() : null,
            'manufacturer_warranty_start' => $model->manufacturer_warranty_start ? $model->manufacturer_warranty_start->toDateString() : null,
            'manufacturer_warranty_end'   => $model->manufacturer_warranty_end ? $model->manufacturer_warranty_end->toDateString() : null,
            'remarks'                     => $model->remarks,
        ];
    }
}