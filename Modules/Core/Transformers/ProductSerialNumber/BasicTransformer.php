<?php

namespace Modules\Core\Transformers\ProductSerialNumber;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductSerialNumber;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ProductSerialNumber;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductSerialNumber entity
     * @param ProductSerialNumber $model
     *
     * @return array
     */
    public function transform(ProductSerialNumber $model)
    {
        return [
            'id'                          => $model->id,
            'serial_number'               => $model->serial_number,
            'expiration_date'             => $model->expiration_date ? $model->expiration_date->toDateString() : null,
            'manufacturing_date'          => $model->manufacturing_date ? $model->manufacturing_date->toDateString() : null,
            'admission_date'              => $model->admission_date ? $model->admission_date->toDateString() : null,
            'manufacturer_warranty_start' => $model->manufacturer_warranty_start ? $model->manufacturer_warranty_start->toDateString() : null,
            'manufacturer_warranty_end'   => $model->manufacturer_warranty_end ? $model->manufacturer_warranty_end->toDateString() : null,
            'remarks'                     => $model->remarks,
        ];
    }
}
