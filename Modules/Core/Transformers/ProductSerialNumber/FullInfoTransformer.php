<?php

namespace Modules\Core\Transformers\ProductSerialNumber;

use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductSerialNumber;
use Modules\Core\Transformers\Product\FullInfoTransformer as ProductFullInfoTransformer;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ProductSerialNumber;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductSerialNumber entity
     * @param ProductSerialNumber $model
     *
     * @return array
     */
    public function transform(ProductSerialNumber $model)
    {
        return [
            'id'                          => $model->id,
            'serial_number'               => $model->serial_number,
            'expiration_date'             => $model->expiration_date ? $model->expiration_date->toDateString() : null,
            'manufacturing_date'          => $model->manufacturing_date ? $model->manufacturing_date->toDateString() : null,
            'admission_date'              => $model->admission_date ? $model->admission_date->toDateString() : null,
            'manufacturer_warranty_start' => $model->manufacturer_warranty_start ? $model->manufacturer_warranty_start->toDateString() : null,
            'manufacturer_warranty_end'   => $model->manufacturer_warranty_end ? $model->manufacturer_warranty_end->toDateString() : null,
            'remarks'                     => $model->remarks,
            'owner_type'                  => $model->owner_type,
            'owner_id'                    => $model->owner_id,
            'status'                      => $model->status,
            'transaction'                 => $model->transaction,
            'product'                     => array_merge(
                Fractal::item($model->product, new ProductFullInfoTransformer)->getArray(),
                ['barcodes' => $model->product->presenter()->barcodes]
            )
        ];
    }
}
