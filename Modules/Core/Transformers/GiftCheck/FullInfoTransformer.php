<?php

namespace Modules\Core\Transformers\GiftCheck;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\GiftCheck;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\GiftCheck;
 */
class FullInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the  entity
     * @param  $model
     *
     * @return array
     */
    public function transform(GiftCheck $model)
    {
        return [
            'id'               => $model->id,
            'code'             => $model->code,
            'check_number'     => $model->check_number,
            'date_from'        => $model->date_from->toDateString(),
            'date_to'          => $model->date_to->toDateString(),
            'amount'           => $model->number()->amount,
            'status'           => $model->status,
            'status_label'     => $model->presenter()->status,
            'redeemed_by'      => $model->redeemer->full_name ?? '-',
            'redeemed_date'    => is_null($model->redeemed_date) ? '-' : $model->redeemed_date->toDateTimeString(),
            'reference_number' => $model->reference_number ?? '-',
        ];
    }
}
