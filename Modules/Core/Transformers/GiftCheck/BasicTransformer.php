<?php

namespace Modules\Core\Transformers\GiftCheck;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\GiftCheck;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\GiftCheck;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the GiftCheck entity
     * @param GiftCheck $model
     *
     * @return array
     */
    public function transform(GiftCheck $model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'check_number' => $model->check_number,
            'date_from'    => $model->date_from->toDateString(),
            'date_to'      => $model->date_to->toDateString(),
            'amount'       => $model->number()->amount,
            'status'       => $model->presenter()->status
        ];
    }
}
