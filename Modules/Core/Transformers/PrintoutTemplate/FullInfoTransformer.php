<?php

namespace Modules\Core\Transformers\PrintoutTemplate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PrintoutTemplate;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PrintoutTemplate;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PrintoutTemplate entity
     * @param PrintoutTemplate $model
     *
     * @return array
     */
    public function transform(PrintoutTemplate $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'module_id'     => $model->module_id,
            'format'        => $model->format,
        ];
    }
}
