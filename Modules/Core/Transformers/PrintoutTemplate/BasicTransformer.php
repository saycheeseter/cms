<?php

namespace Modules\Core\Transformers\PrintoutTemplate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PrintoutTemplate;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\PrintoutTemplate;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the PrintoutTemplate entity
     * @param PrintoutTemplate $model
     *
     * @return array
     */
    public function transform(PrintoutTemplate $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'module'        => $model->presenter()->module,
        ];
    }
}
