<?php

namespace Modules\Core\Transformers\IncomeStatement;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\IncomeStatement;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\IncomeStatement;
 */
class BasicTransformer extends TransformerAbstract
{
    public function transform(IncomeStatement $model)
    {
        return [
            'gross_sales'            => $model->number()->gross_sales,
            'return_amount'          => $model->number()->return_amount,
            'cost_of_goods_sold'     => $model->number()->cost_of_goods_sold,
            'ma_cost_of_goods_sold'  => $model->number()->ma_cost_of_goods_sold,
            'damage'                 => $model->number()->damage,
            'adjustment_discrepancy' => $model->number()->adjustment_discrepancy,
            'other_payment'          => $model->number()->other_payment,
            'other_income'           => $model->number()->other_income,
        ];
    }
}