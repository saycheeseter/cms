<?php

namespace Modules\Core\Transformers\User;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\User;
use Lang;

/**
 * Class PermissionTransformer
 * @package namespace Modules\Core\Transformers\User;
 */
class PermissionTransformer extends TransformerAbstract
{

    /**
     * Transform the User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        $attributes = array();

        $branches = $model->branches()->distinct()->get();

        foreach ($branches as $key => $branch) {
            $permissions = $this->filterPermissionsByBranch($model->permissions, $branch->id);
            
            $attributes[] = array(
                'branch' => $branch->id,
                'code'   => $permissions
            );
        }

        return $attributes;
    }

    private function filterPermissionsByBranch($permissions, $branchId)
    {
        $filtered = $permissions->filter(function ($value, $key) use ($branchId) {
            return $value->pivot->branch_id == $branchId;
        });

        return array_pluck($filtered->toArray(), 'id');
    }
}
