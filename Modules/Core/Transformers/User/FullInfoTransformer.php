<?php

namespace Modules\Core\Transformers\User;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\User;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\User;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'full_name'     => $model->full_name,
            'username'      => $model->username,
            'status'        => $model->status,
            'position'      => $model->position ?? '',
            'address'       => $model->address ?? '',
            'telephone'     => $model->telephone ?? '',
            'mobile'        => $model->mobile ?? '',
            'email'         => $model->email ?? '',
            'memo'          => $model->memo ?? '',
            'role_id'       => $model->role_id,
            'license_key'   => $model->license_key
        ];
    }
}
