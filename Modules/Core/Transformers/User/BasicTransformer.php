<?php

namespace Modules\Core\Transformers\User;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\User;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\User;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'username'       => $model->username,
            'position'       => $model->position,
            'status'         => $model->presenter()->status,
            'type'           => $model->type,
            'license_key'    => $model->license_key,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'address'        => $model->address,
            'memo'           => $model->memo,
            'schedule_start' => is_null($model->schedule) ? '-' : $model->schedule->presenter()->start,
            'schedule_end'   => is_null($model->schedule) ? '-' : $model->schedule->presenter()->end
        ];
    } 
}
