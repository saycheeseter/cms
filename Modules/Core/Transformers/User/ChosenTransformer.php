<?php 

namespace Modules\Core\Transformers\User;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\User;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Supplier;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Supplier entity
     * @param Supplier $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->full_name,
        ];
    }
}