<?php

namespace Modules\Core\Transformers\PurchaseInboundDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PurchaseInboundDetail;
use Modules\Core\Transformers\Product\TransactionComponentsTransformer;
use Fractal;

/**
 * Class ImportTransformer
 *
 * @package namespace Modules\Core\Transformers\PurchaseInboundDetail;
 */
class ImportTransformer extends TransformerAbstract
{
    /**
     * Transform the PurchaseInboundDetail entity
     *
     * @param PurchaseInboundDetail $model
     *
     * @return array
     */
    public function transform(PurchaseInboundDetail $model)
    {
        $precision = setting('monetary.precision');

        return [
            'id'                   => $model->id,
            'manage_type'          => $model->product->manage_type,
            'product_id'           => $model->product_id,
            'barcode_list'         => $model->presenter()->barcode_list,
            'barcode'              => $model->presenter()->unit_barcode,
            'stock_no'             => $model->product->stock_no,
            'name'                 => $model->product->name,
            'chinese_name'         => $model->product->chinese_name,
            'units'                => $model->presenter()->unit_list,
            'unit_id'              => $model->unit_id,
            'unit_qty'             => $model->number()->unit_qty,
            'qty'                  => $model->number()->qty,
            'oprice'               => $model->number()->oprice,
            'price'                => $model->number()->price,
            'discount1'            => $model->number()->discount1,
            'discount2'            => $model->number()->discount2,
            'discount3'            => $model->number()->discount3,
            'discount4'            => $model->number()->discount4,
            'remarks'              => $model->remarks,
            'reference_id'         => $model->reference_id ?? null,
            'transactionable_type' => $model->transactionable_type ?? null,
            'transactionable_id'   => $model->transactionable_id ?? null,
            'transactionable'       => is_null($model->transactionable)
                ? []
                : [
                    'total_qty' => $model->transactionable->number()->total_qty,
                    'remaining_qty' => $model->transactionable->number()->remaining_qty ?? 0,
                ],
            'components'    => Fractal::collection($model->components, new TransactionComponentsTransformer)->getArray()['data'],
            'group_type'    => $model->group_type,
            'manageable'    => $model->manageable,
            'product_type'  => $model->product_type
        ];
    }
}
