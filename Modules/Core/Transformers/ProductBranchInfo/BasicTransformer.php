<?php 

namespace Modules\Core\Transformers\ProductBranchInfo;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBranchInfo;

class BasicTransformer extends TransformerAbstract
{
    public function transform(ProductBranchInfo $model)
    {
        return [
            'branch_id'         => $model->branch_id,
            'branch_name'       => $model->branch->name ?? '',
            'status'            => $model->status
        ];
    }
}