<?php 

namespace Modules\Core\Transformers\StockReturn;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockReturn;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\StockReturn;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the StockReturn entity
     * @param StockReturn $model
     *
     * @return array
     */
    public function transform(StockReturn $model)
    {
        return [
            'created_for'         => $model->created_for,
            'for_location'        => $model->for_location,
            'deliver_to'          => $model->deliver_to,
            'deliver_to_location' => $model->deliver_to_location,
            'requested_by'        => $model->requested_by,
            'remarks'             => $model->remarks,
        ];
    }
}