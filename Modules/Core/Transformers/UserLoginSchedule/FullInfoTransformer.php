<?php

namespace Modules\Core\Transformers\UserLoginSchedule;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\UserLoginSchedule;
use Lang;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\User;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the User entity
     * @param User $model
     *
     * @return array
     */
    public function transform(UserLoginSchedule $model)
    {
        return [
            'mon'   => $model->mon,
            'tue'   => $model->tue,
            'wed'   => $model->wed,
            'thu'   => $model->thu,
            'fri'   => $model->fri,
            'sat'   => $model->sat,
            'sun'   => $model->sun,
            'start' => $model->start,
            'end'   => $model->end,
        ];
    }
}
