<?php

namespace Modules\Core\Transformers\OtherPaymentDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherPaymentDetail;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\OtherPaymentDetail;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the OtherPaymentDetail entity
     * @param OtherPaymentDetail $model
     *
     * @return array
     */
    public function transform(OtherPaymentDetail $model)
    {
        return [
            'id'     => $model->id,
            'transaction_id'   => $model->transaction_id,
            'amount'   => $model->number()->amount,
            'type'   => $model->type,
        ];
    }
}
