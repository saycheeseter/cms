<?php

namespace Modules\Core\Transformers\CashTransaction;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\CashTransactionSummary;

/**
 * Class BeginningBalanceTransformer
 * @package namespace Modules\Core\Transformers\CashTransaction;
 */
class BeginningBalanceTransformer extends TransformerAbstract
{

    public function transform(CashTransactionSummary $model)
    {
        return [
            'total'  => $model->number()->total,
        ];
    }
}
