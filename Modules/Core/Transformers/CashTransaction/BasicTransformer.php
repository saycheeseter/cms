<?php

namespace Modules\Core\Transformers\CashTransaction;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\CashTransaction;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\CashTransaction;
 */
class BasicTransformer extends TransformerAbstract
{

    public function transform(CashTransaction $model)
    {
        return [
            'type'         => $model->type,
            'reference_id' => $model->reference_id,
            'date'         => $model->transaction_date->toDateString(),
            'particular'   => $model->particular,
            'in'           => $model->number()->in,
            'out'          => $model->number()->out,
            'remarks'      => $model->remarks
        ];
    }
}
