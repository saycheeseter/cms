<?php

namespace Modules\Core\Transformers\ProductBarcode;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductBarcode;
use Modules\Core\Entities\UOM;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductBarcode;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductBarcode entity
     * @param ProductBarcode $model
     *
     * @return array
     */
    public function transform(ProductBarcode $model)
    {
        return [
            'uom_id'     => $model->uom_id,
            'code'       => $model->code,
            'memo'       => $model->memo ?? ''
        ];
    }
}
