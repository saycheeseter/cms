<?php

namespace Modules\Core\Transformers\InventoryChain;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Contracts\SeriableTransaction;

/**
 * Class TransactionTransformer
 */
class SerialTransactionTransformer extends TransformerAbstract
{
    public function transform(SeriableTransaction $model)
    {
        return [
            'id'                 => $model->id,
            'created_for'        => $model->for->name,
            'for_location'       => $model->location->name,
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'particulars'        => $model->presenter()->particular,
            'amount'             => $model->number()->total_amount,
            'prepared_by'        => $model->creator->full_name,
            'remarks'            => $model->remarks,
            'serial_transaction' => [
                'type'  => $model->serial_transaction_assignment,
                'label' => $model->serial_transaction_assignment_label
            ],
        ];
    }
}
