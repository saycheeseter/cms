<?php

namespace Modules\Core\Transformers\InventoryChain;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryChain;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\InventoryChain;
 */
class BasicTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryChain entity
     *
     * @param InventoryChain $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryChain::class)) {
            return [];
        }

        return [
            'id'               => $model->id,
            'sheet_number'     => $model->sheet_number,
            'transaction_type' => $model->transaction_type,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks,
        ];
    }
}
