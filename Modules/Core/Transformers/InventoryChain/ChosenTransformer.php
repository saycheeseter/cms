<?php

namespace Modules\Core\Transformers\InventoryChain;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\InventoryChain;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\InventoryChain;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the InventoryChain entity
     * @param InventoryChain $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (!is_subclass_of($model, InventoryChain::class)) {
            return [];
        }

        return [
            'id'    => $model->id,
            'label' => $model->sheet_number,
        ];
    }
}
