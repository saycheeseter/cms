<?php 

namespace Modules\Core\Transformers\StockDelivery;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDelivery;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\StockDelivery;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the StockDelivery entity
     * @param StockDelivery $model
     *
     * @return array
     */
    public function transform(StockDelivery $model)
    {
        return [
            'created_for'         => $model->created_for,
            'for_location'        => $model->for_location,
            'deliver_to'          => $model->deliver_to,
            'deliver_to_location' => $model->deliver_to_location,
            'requested_by'        => $model->requested_by,
            'remarks'             => $model->remarks,
        ];
    }
}