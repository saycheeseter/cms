<?php

namespace Modules\Core\Transformers\StockDelivery;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDelivery;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\StockDelivery;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the StockDelivery entity
     * @param StockDelivery $model
     *
     * @return array
     */
    public function transform(StockDelivery $model)
    {
        return [
            'branch_address'        => Auth::branch()->address,
            'branch_contact'        => Auth::branch()->contact,
            'branch_name'           => Auth::branch()->name,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'sheet_number'          => $model->sheet_number,
            'transaction_date_time' => $model->transaction_date->toDateTimeString(),
            'transaction_date'      => $model->transaction_date->toDateString(),
            'transaction_time'      => $model->transaction_date->toTimeString(),
            'to_branch'             => $model->deliverTo->name,
            'to_branch_address'     => $model->deliverTo->address,
            'deliver_to_location'   => $model->deliverToLocation->name,
            'request_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
