<?php

namespace Modules\Core\Transformers\StockDelivery;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDelivery;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\StockDelivery;
 */

class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the StockDelivery entity
     * @param StockDelivery $model
     *
     * @return array
     */
    public function transform(StockDelivery $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'deliver_to'            => $model->deliver_to,
            'deliver_to_location'   => $model->deliver_to_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'reference_id'          => is_null($model->reference_id) 
                ? null
                : $model->reference_id,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}