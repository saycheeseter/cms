<?php

namespace Modules\Core\Transformers\StockDeliveryOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDeliveryOutbound;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\StockDeliveryOutbound;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the StockDeliveryOutbound entity
     * @param StockDeliveryOutbound $model
     *
     * @return array
     */
    public function transform(StockDeliveryOutbound $model)
    {
        $fields = [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'deliver_to'            => $model->deliver_to,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'deliver_to_location'   => $model->deliver_to_location,
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'reference_id'          => $model->reference_id != null ? $model->reference_id : null,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'is_deleted'            => $model->is_deleted
        ];

        $fields += $model->udf()->all();

        return $fields;
    }
}
