<?php

namespace Modules\Core\Transformers\StockDeliveryOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDeliveryOutbound;
use Lang;

/**
 * Class RetrieveTransformer
 * @package namespace Modules\Core\Transformers\StockDeliveryOutbound;
 */
class RetrieveTransformer extends TransformerAbstract
{

    /**
     * Transform the StockDeliveryOutbound entity
     * @param StockDeliveryOutbound $model
     *
     * @return array
     */
    public function transform(StockDeliveryOutbound $model)
    {
        return [
            'for_location'                 => $model->deliver_to_location,
            'requested_by'                 => $model->requested_by,
            'delivery_from'                => $model->created_for,
            'delivery_from_location'       => $model->for_location,
            'delivery_from_label'          => $model->for->name,
            'delivery_from_location_label' => $model->location->name,
            'remarks'                      => $model->remarks,
        ];
    }
}
