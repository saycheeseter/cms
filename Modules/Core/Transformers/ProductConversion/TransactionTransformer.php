<?php

namespace Modules\Core\Transformers\ProductConversion;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversion;

/**
 * Class TransactionTransformer
 * @package namespace Modules\Core\Transformers\ProductConversion;
 */
class TransactionTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductConversion entity
     * @param ProductConversion $model
     *
     * @return array
     */
    public function transform(ProductConversion $model)
    {
        return [
            'transaction_id'   => $model->id,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->sheet_number,
            'particulars'      => $model->presenter()->particular,
            'qty'              => $model->number()->qty,
            'price'            => $model->number()->ma_cost,
            'amount'           => $model->number()->amount,
            'prepared_by'      => $model->creator->full_name,
            'remarks'          => $model->presenter()->group_type_label
        ];
    }
}