<?php

namespace Modules\Core\Transformers\ProductConversion;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversion;

/**
 * Class FullInfoTransformer
 *
 * @package namespace Modules\Core\Transformers\ProductConversion;
 */
class StoreTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductConversion entity
     *
     * @param ProductConversion $model
     *
     * @return array
     */
    public function transform(ProductConversion $model)
    {
        return [
            'id'            => $model->id,
            'sheet_number' => $model->sheet_number,
            'transaction_date' => $model->transaction_date->toDateTimeString()
        ];
    }
}
