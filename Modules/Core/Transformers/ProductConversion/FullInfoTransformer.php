<?php

namespace Modules\Core\Transformers\ProductConversion;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversion;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\ProductConversion;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductConversion entity
     * @param ProductConversion $model
     *
     * @return array
     */
    public function transform(ProductConversion $model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'product_id'            => $model->product_id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'qty'                   => $model->number()->qty,
            'product_name'          => $model->product->name,
            'stock_number'          => $model->product->stock_no,
            'barcode'               => $model->product->barcodes->implode('code', ', '),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'group_type'            => $model->group_type,
            'group_type_label'      => $model->presenter()->group_type_label,
            'is_deleted'            => $model->is_deleted,
            'cost'                  => $model->number()->cost
        ];
    }
}
