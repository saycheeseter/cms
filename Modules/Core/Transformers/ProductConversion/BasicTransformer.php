<?php

namespace Modules\Core\Transformers\ProductConversion;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductConversion;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductConversion;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductConversion entity
     * @param ProductConversion $model
     *
     * @return array
     */
    public function transform(ProductConversion $model)
    {
        return [
            'id'                    => $model->id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'created_from'          => $model->from->name,
            'created_for'           => $model->for->name,
            'product'               => $model->product->name,
            'qty'                   => $model->number()->qty,
            'created_by'            => $model->creator->full_name,
            'for_location'          => $model->location->name,
            'created_date'          => $model->created_at->toDateTimeString(),
            'audited_by'            => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'          => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'deleted'               => $model->presenter()->deleted,
        ];
    }
}
