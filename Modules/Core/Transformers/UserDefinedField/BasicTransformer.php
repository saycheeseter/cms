<?php

namespace Modules\Core\Transformers\UserDefinedField;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\UserDefinedField;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\UserDefinedField;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the UserDefinedField entity
     * @param UserDefinedField $model
     *
     * @return array
     */
    public function transform(UserDefinedField $model)
    {
        return [
            'id'            => $model->id,
            'alias'         => $model->alias,
            'label'         => $model->label,
            'type'          => $model->type,
            'visible'       => $model->visible,
        ];
    }
}
