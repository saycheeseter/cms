<?php

namespace Modules\Core\Transformers\ProductInventoryWarning;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Product;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductInventoryWarning;
 */
class FullInfoTransformer extends TransformerAbstract
{
    public function transform($model)
    {
        return [
            'product_name'        => $model->product_name,
            'min'                 => $model->number()->min,
            'max'                 => $model->number()->max,
            'inventory'           => $model->number()->inventory,
            'reserved_qty'        => $model->number()->reserved_qty,
            'requested_qty'       => $model->number()->requested_qty,
            'projected_inventory' => $model->number()->projected_inventory,
        ];
    }
}
