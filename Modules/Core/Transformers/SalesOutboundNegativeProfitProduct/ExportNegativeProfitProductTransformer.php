<?php

namespace Modules\Core\Transformers\SalesOutboundNegativeProfitProduct;

use League\Fractal\TransformerAbstract;
use Lang;
use Modules\Core\Entities\Hydration\SalesOutboundNegativeProfitProduct;

/**
 * Class ExportNegativeProfitProductTransformer
 * @package namespace Modules\Core\Transformers\SalesOutboundNegativeProfitProduct;
 */
class ExportNegativeProfitProductTransformer extends TransformerAbstract
{
    /**
     * Transform the SalesOutboundNegativeProfitProduct entity
     *
     * @param SalesOutboundNegativeProfitProduct $model
     * @return array
     */
    public function transform(SalesOutboundNegativeProfitProduct $model)
    {
        return [
            'barcode'      => $model->barcode,
            'product'      => $model->product,
            'sheet_number' => $model->sheet_number,
            'qty'          => $model->qty,
            'cost'         => $model->cost,
            'price'        => $model->price,
            'loss'         => $model->loss
        ];
    }
}
