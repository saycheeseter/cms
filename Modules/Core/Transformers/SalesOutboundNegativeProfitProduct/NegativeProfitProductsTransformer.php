<?php

namespace Modules\Core\Transformers\SalesOutboundNegativeProfitProduct;

use League\Fractal\TransformerAbstract;
use Lang;
use Modules\Core\Entities\Hydration\SalesOutboundNegativeProfitProduct;

/**
 * Class NegativeProfitProductsTransformer
 * @package namespace Modules\Core\Transformers\SalesOutboundNegativeProfitProduct;
 */
class NegativeProfitProductsTransformer extends TransformerAbstract
{

    /**
     * Transform the SalesOutboundNegativeProfitProduct entity
     *
     * @param  $model
     * @return array
     */
    public function transform(SalesOutboundNegativeProfitProduct $model)
    {
        return [
            'barcode'      => $model->barcode,
            'product'      => $model->product,
            'sheet_number' => $model->sheet_number,
            'qty'          => $model->number()->qty,
            'cost'         => $model->number()->cost,
            'price'        => $model->number()->price,
            'loss'         => $model->number()->loss
        ];
    }
}
