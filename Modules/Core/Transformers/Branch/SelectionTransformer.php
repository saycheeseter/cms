<?php

namespace Modules\Core\Transformers\Branch;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Branch;

/**
 * Class SelectionTransformer
 * @package namespace Modules\Core\Transformers\Branch;
 */
class SelectionTransformer extends TransformerAbstract
{

    /**
     * Transform the Branch entity
     * @param Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        return [
            'id'   => $model->id,
            'name' => $model->code.' - '.$model->name
        ];
    }
}
