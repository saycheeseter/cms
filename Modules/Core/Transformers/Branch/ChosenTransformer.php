<?php

namespace Modules\Core\Transformers\Branch;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Branch;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Branch;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Branch entity
     * @param Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
