<?php

namespace Modules\Core\Transformers\Branch;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Branch;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Branch;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Branch entity
     * @param Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }
}
