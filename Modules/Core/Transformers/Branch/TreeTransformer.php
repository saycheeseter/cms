<?php

namespace Modules\Core\Transformers\Branch;

use Modules\Core\Transformers\BranchDetail\SelectionTransformer;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Branch;
use Fractal;

/**
 * Class TreeTransformer
 * @package namespace Modules\Core\Transformers\Branch;
 */
class TreeTransformer extends TransformerAbstract
{
    /**
     * Transform the Branch entity
     * @param Branch $model
     *
     * @return array
     */
    public function transform(Branch $model)
    {
        return [
            'id'         => $model->id,
            'name'       => $model->code.' - '.$model->name,
            'details'    => $this->details($model->details)
        ];
    }

    private function details($collection)
    {
        return Fractal::collection($collection, new SelectionTransformer)->getArray()['data'];
    }
}
