<?php

namespace Modules\Core\Transformers\AuditTrail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Audit;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Audit;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Audit entity
     * @param Audit $model
     *
     * @return array
     */
    public function transform(Audit $model)
    {
        return [
            'user'                => $model->audited->full_name,
            'action'              => $model->presenter()->action,
            'timestamp'           => $model->updated_at->toDateTimeString(),
            'module'              => $model->presenter()->module,
            'audit_trail_details' => $model->presenter()->message
        ];
    }
}
