<?php

namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\CollectionDueWarningSummary;

class CollectionDueWarningSummaryTransformer extends TransformerAbstract
{
    public function transform(CollectionDueWarningSummary $model)
    {
        return [
            'total_count' => $model->number()->total_count,
            'total_amount' => $model->number()->total_amount,
            'total_remaining' => $model->number()->total_remaining
        ];
    }
}