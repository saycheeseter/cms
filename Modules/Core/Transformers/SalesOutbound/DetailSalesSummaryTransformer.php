<?php

namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\DetailSalesReportSummary;

class DetailSalesSummaryTransformer extends TransformerAbstract
{
    public function transform(DetailSalesReportSummary $model)
    {
        return [
            'total_remaining_amount' => $model->number()->total_remaining_amount,
            'total_qty' => $model->number()->total_qty,
            'total_amount' => $model->number()->total_amount
        ];
    }
}