<?php

namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\SalesmanReportTransactionsProducts;

class SalesmanTransactionsProductsTransformer extends TransformerAbstract
{
    public function transform(SalesmanReportTransactionsProducts $model)
    {
        return [
            'product_barcode'    => $model->product_barcode,
            'product_name'       => $model->product_name,
            'total_qty'          => $model->number()->total_qty,
            'minimum_price'      => $model->number()->minimum_price,
            'average_price'      => $model->number()->average_price,
            'maximum_price'      => $model->number()->maximum_price,
            'total_amount'       => $model->number()->total_amount
        ];
    }
}