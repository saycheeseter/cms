<?php

namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\DetailSalesReport;

class DetailSalesExportTransformer extends TransformerAbstract
{
    public function transform(DetailSalesReport $model)
    {
        return [
            'created_from_name' => $model->created_from_name,
            'created_for_name' => $model->created_for_name,
            'for_location_name' => $model->for_location_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'sheet_number' => $model->sheet_number,
            'reference_id' => $model->reference_id,
            'approval' => $model->presenter()->approval,
            'customer_type' => $model->presenter()->customer_type,
            'customer_name' => $model->customer_name ?? $model->customer_walk_in_name,
            'total_amount' => $model->total_amount,
            'remaining_amount' => $model->remaining_amount,
            'remarks' => $model->remarks,
            'salesman_name' => $model->salesman_name,
            'term' => $model->term,
            'requested_by_name' => $model->requested_by_name,
            'created_by_name' => $model->created_by_name,
            'created_at' => $model->created_at->toDateTimeString(),
            'audited_by_name' => $model->audited_by_name,
            'audited_date' => is_null($model->audited_date) ? '' : $model->audited_date->toDateTimeString(),
            'deleted' => $model->presenter()->deleted,
            'product_barcode' => $model->product_barcode,
            'stock_no' => $model->stock_no,
            'chinese_name' => $model->chinese_name,
            'product_name' => $model->product_name,
            'qty' => $model->qty,
            'unit_name' => $model->unit_name,
            'unit_qty' => $model->unit_qty,
            'total_qty' => $model->total_qty,
            'product_serial' => $model->product_serial,
            'product_batch' => $model->product_batch,
            'oprice' => $model->oprice,
            'price' => $model->price,
            'discount1' => $model->discount1,
            'discount2' => $model->discount2,
            'discount3' => $model->discount3,
            'discount4' => $model->discount4,
            'detail_total_amount' => $model->detail_total_amount,
            'detail_remarks' => $model->detail_remarks,
        ];
    }
}