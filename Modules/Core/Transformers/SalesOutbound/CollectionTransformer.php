<?php

namespace Modules\Core\Transformers\SalesOutbound;

use Illuminate\Database\Eloquent\Relations\Relation;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SalesOutbound;
use Lang;

/**
 * Class CollectionTransformer
 * @package namespace Modules\Core\Transformers\SalesOutbound;
 */
class CollectionTransformer extends TransformerAbstract
{
    /**
     * Transform the SalesOutbound entity
     * @param SalesOutbound $model
     *
     * @return array
     */
    public function transform(SalesOutbound $model)
    {
        return [
            'reference_id'       => $model->id,
            'reference_type'     => array_search(get_class($model), Relation::morphMap()),
            'transaction_date'   => $model->transaction_date->toDateTimeString(),
            'sheet_number'       => $model->sheet_number,
            'term'               => $model->term,
            'dr_no'              => '',
            'remarks'            => $model->remarks,
            'amount'             => $model->number()->total_amount,
            'collected_amount'   => $model->number()->collected_amount,
            'remaining'          => $model->number()->remaining_amount,
            'collectible'        => ''
        ];
    }
}