<?php
namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\SalesmanReportTransactions;

class SalesmanTransactionsTransformer extends TransformerAbstract
{
    public function transform(SalesmanReportTransactions $model)
    {
        return [
            'branch_name'              => $model->branch_name,
            'transaction_date'         => $model->transaction_date->toDateTimeString(),
            'sheet_number'             => $model->sheet_number,
            'customer_name'            => $model->customer_name,
            'salesman_full_name'       => $model->salesman_full_name,
            'transaction_total_amount' => $model->number()->transaction_total_amount
        ];
    }
}