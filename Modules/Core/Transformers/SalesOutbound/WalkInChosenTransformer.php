<?php

namespace Modules\Core\Transformers\SalesOutbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\SalesOutbound;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\SalesOutbound;
 */
class WalkInChosenTransformer extends TransformerAbstract
{

    /**
     * Transform the SalesOutbound entity
     * @param SalesOutbound $model
     *
     * @return array
     */
    public function transform(SalesOutbound $model)
    {
        return [
            'id'    => $model->customer_walk_in_name,
            'label' => $model->customer_walk_in_name
        ];
    }
}
