<?php 

namespace Modules\Core\Transformers\CustomerDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CustomerDetail;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\CustomerDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the CustomerDetail entity
     * @param CustomerDetail $model
     *
     * @return array
     */
    public function transform(CustomerDetail $model)
    {
        return [
            'id'             => $model->id,
            'name'           => $model->name,
            'address'        => $model->address,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'contact_person' => $model->contact_person,
            'tin'            => $model->tin
        ];
    }
}