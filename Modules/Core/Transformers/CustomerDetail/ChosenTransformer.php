<?php

namespace Modules\Core\Transformers\CustomerDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CustomerDetail;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\CustomerDetail;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the CustomerDetail entity
     * @param CustomerDetail $model
     *
     * @return array
     */
    public function transform(CustomerDetail $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
