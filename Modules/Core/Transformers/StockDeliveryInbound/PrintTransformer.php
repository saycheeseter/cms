<?php

namespace Modules\Core\Transformers\StockDeliveryInbound;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\StockDeliveryInbound;
use Lang;
use Auth;

/**
 * Class PrintTransformer
 * @package namespace Modules\Core\Transformers\StockDeliveryInbound;
 */
class PrintTransformer extends TransformerAbstract
{
    /**
     * Transform the StockDeliveryInbound entity
     * @param StockDeliveryInbound $model
     *
     * @return array
     */
    public function transform(StockDeliveryInbound $model)
    {
        return [
            'branch_address'         => Auth::branch()->address,
            'branch_contact'         => Auth::branch()->contact,
            'branch_name'            => Auth::branch()->name,
            'created_by'             => $model->creator->full_name,
            'created_for'            => $model->for->name,
            'for_location'           => $model->location->name,
            'remarks'                => $model->remarks,
            'requested_by'           => $model->requested->full_name,
            'sheet_number'           => $model->sheet_number,
            'transaction_date_time'  => $model->transaction_date->toDateTimeString(),
            'transaction_date'       => $model->transaction_date->toDateString(),
            'transaction_time'       => $model->transaction_date->toTimeString(),
            'delivery_from'          => $model->deliveryFrom->name,
            'delivery_from_address'  => $model->deliveryFrom->address,
            'delivery_from_location' => $model->deliveryFromLocation->name,
            'delivery_reference'     => $model->presenter()->invoice_reference,
        ];
    }
}
