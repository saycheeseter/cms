<?php

namespace Modules\Core\Transformers\BranchDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BranchDetail;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\BranchDetail;
 */
class ChosenTransformer extends TransformerAbstract
{

    /**
     * Transform the BranchDetail entity
     * @param BranchDetail $model
     *
     * @return array
     */
    public function transform(BranchDetail $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
