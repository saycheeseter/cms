<?php

namespace Modules\Core\Transformers\BranchDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BranchDetail;

/**
 * Class SelectionTransformer
 * @package namespace Modules\Core\Transformers\BranchDetail;
 */
class SelectionTransformer extends TransformerAbstract
{

    /**
     * Transform the BranchDetail entity
     * @param BranchDetail $model
     *
     * @return array
     */
    public function transform(BranchDetail $model)
    {
        return [
            'id'   => $model->id,
            'name' => $model->code.' - '.$model->name
        ];
    }
}
