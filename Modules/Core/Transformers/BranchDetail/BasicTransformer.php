<?php

namespace Modules\Core\Transformers\BranchDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BranchDetail;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BranchDetail;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the BranchDetail entity
     * @param BranchDetail $model
     *
     * @return array
     */
    public function transform(BranchDetail $model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }
}
