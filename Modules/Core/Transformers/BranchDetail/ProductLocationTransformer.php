<?php

namespace Modules\Core\Transformers\BranchDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BranchDetail;

/**
 * Class ProductLocationTransformer
 * @package namespace Modules\Core\Transformers\BranchDetail;
 */
class ProductLocationTransformer extends TransformerAbstract
{

    /**
     * Transform the BranchDetail entity
     * @param BranchDetail $model
     *
     * @return array
     */
    public function transform(BranchDetail $model)
    {
        return [
            'id'     => $model->id,
            'name'   => $model->name,
            'branch' => $model->branch->name ?? ''
        ];
    }
}
