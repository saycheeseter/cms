<?php 

namespace Modules\Core\Transformers\CustomReport\Product;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{
    /**
     * Transform the PurchaseInbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->barcode($data),
            $this->branchSummary($data),
            $this->branchPrice($data),
            $this->branchDetail($data),
            $this->branchInfo($data),
            $this->productTax($data),
            $this->unit($data)
        );
    }

    private function info($data) 
    {
        return array_merge(
            $this->product($data),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }

    private function barcode($data) 
    {
        return array_merge(
            array(
                'product_barcode_memo' => $data->product_barcode_memo,
                'product_barcode_code' => $data->product_barcode_code,
            ),
            $this->systemCode($data, 'product_barcode_uom_')
        );
    }

    private function branchSummary($data)
    {
        return array(
            'product_branch_summary_reserved_qty' => number_format($data->product_branch_summary_reserved_qty, $this->precision),
            'product_branch_summary_requested_qty' => number_format($data->product_branch_summary_requested_qty, $this->precision),
            'product_branch_summary_qty' => number_format($data->product_branch_summary_qty, $this->precision)
        );
    }

    private function branchPrice($data) 
    {
        return array(
            'product_branch_price_purchase_price' => number_format($data->branch_price_purchase_price, $this->precision),
            'product_branch_price_selling_price' => number_format($data->branch_price_selling_price, $this->precision),
            'product_branch_price_wholesale_price' => number_format($data->branch_price_wholesale_price, $this->precision),
            'product_branch_price_price_a' => number_format($data->branch_price_price_a, $this->precision),
            'product_branch_price_price_b' => number_format($data->branch_price_price_b, $this->precision),
            'product_branch_price_price_c' => number_format($data->branch_price_price_c, $this->precision),
            'product_branch_price_price_d' => number_format($data->branch_price_price_d, $this->precision),
            'product_branch_price_price_e' => number_format($data->branch_price_price_e, $this->precision)
        );
    }

    private function branchDetail($data) 
    {
        return array_merge(
            array(
                'product_branch_detail_min' => number_format($data->product_branch_detail_min, $this->precision),
                'product_branch_detail_max' => number_format($data->product_branch_detail_max, $this->precision),
            ),
            $this->location($data, 'product_branch_detail_location_')
        );
    }

    private function branchInfo($data) 
    {
        return array_merge(
            array(
                'product_branch_info_status' => $data->product_branch_info_status,
            ),
            $this->branch($data, 'product_branch_info_branch_')
        );
    }

    private function productTax($data) 
    {
        return $this->tax($data, 'product_tax_');
    }

    private function unit($data) 
    {
        return array_merge(
            array(
                'product_unit_qty' => number_format($data->product_unit_qty, $this->precision),
                'product_unit_length' => number_format($data->product_unit_length, $this->precision),
                'product_unit_width' => number_format($data->product_unit_width, $this->precision),
                'product_unit_height' => number_format($data->product_unit_height, $this->precision),
                'product_unit_weight' => number_format($data->product_unit_weight, $this->precision),
                'product_unit_default' => number_format($data->product_unit_default, $this->precision),
            ),
            $this->systemCode($data, 'product_unit_uom_')
        );
    }

}