<?php

namespace Modules\Core\Transformers\CustomReport\InventoryAdjust;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{
    /**
     * Transform the PurchaseInbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data) 
    {
        return array_merge(
            array(
                'inventory_adjust_audited_date' => $data->inventory_adjust_audited_date,
                'inventory_adjust_transaction_date' => $data->inventory_adjust_transaction_date,
                'inventory_adjust_approval_status' => $data->inventory_adjust_approval_status,
                'inventory_adjust_sheet_number' => $data->inventory_adjust_sheet_number,
                'inventory_adjust_remarks' => $data->inventory_adjust_remarks,
                'inventory_adjust_total_gain_amount' => number_format($data->inventory_adjust_total_gain_amount, $this->precision),
                'inventory_adjust_total_lost_amount' => number_format($data->inventory_adjust_total_lost_amount, $this->precision)
            ),
            $this->location($data, 'inventory_adjust_for_location_'),
            $this->user($data, 'inventory_adjust_requested_by_'),
            $this->systemCode($data, 'inventory_adjust_detail_unit_'),
            $this->role($data, 'inventory_adjust_requested_by_role_')
        );
    }

    public function detail($data) 
    {
        return array_merge(
            array(
                'inventory_adjust_detail_unit_qty' => number_format($data->inventory_adjust_detail_unit_qty, $this->precision),
                'inventory_adjust_detail_pc_qty' => number_format($data->inventory_adjust_detail_pc_qty, $this->precision),
                'inventory_adjust_detail_inventory' => number_format($data->inventory_adjust_detail_inventory, $this->precision),
                'inventory_adjust_detail_price' => number_format($data->inventory_adjust_detail_price, $this->precision),
                'inventory_adjust_detail_remarks' => $data->inventory_adjust_detail_remarks,
                'inventory_adjust_detail_qty' => number_format($data->inventory_adjust_detail_qty, $this->precision)
            ),
            $this->product($data, 'product_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}