<?php

namespace Modules\Core\Transformers\CustomReport\StockDelivery;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{
    /**
     * Transform the StockDeliveryOutbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {
        return array_merge(
            array(
                'stock_delivery_reference_sheet_number' => $data->stock_delivery_reference_sheet_number,
                'stock_delivery_sheet_number' => $data->stock_delivery_sheet_number,
                'stock_delivery_remarks' => $data->stock_delivery_remarks,
                'stock_delivery_transaction_date' => $data->stock_delivery_transaction_date,
                'stock_delivery_audited_date' => $data->stock_delivery_audited_date,
                'stock_delivery_approval_status' => $data->stock_delivery_approval_status,
                'stock_delivery_transaction_status' => $data->stock_delivery_transaction_status,
                'stock_delivery_transaction_type' => $data->stock_delivery_transaction_type,
                'stock_delivery_total_amount' => number_format($data->stock_delivery_total_amount, $this->precision)
            ),
            $this->user($data, 'stock_delivery_requested_by_'),
            $this->role($data, 'stock_delivery_requested_by_role_'),
            $this->branch($data, 'stock_delivery_created_for_'),
            $this->location($data, 'stock_delivery_for_location_'),
            $this->branch($data, 'stock_delivery_deliver_to_'),
            $this->location($data, 'stock_delivery_deliver_to_location_')
        );
    }

    public function detail($data)
    {
        return array_merge(
            array(
                'stock_delivery_detail_unit_qty' => number_format($data->stock_delivery_detail_unit_qty, $this->precision),
                'stock_delivery_detail_qty' => number_format($data->stock_delivery_detail_qty, $this->precision),
                'stock_delivery_detail_oprice' => number_format($data->stock_delivery_detail_oprice, $this->precision),
                'stock_delivery_detail_price' => number_format($data->stock_delivery_detail_price, $this->precision),
                'stock_delivery_detail_discount1' => number_format($data->stock_delivery_detail_discount1, $this->precision),
                'stock_delivery_detail_discount2' => number_format($data->stock_delivery_detail_discount2, $this->precision),
                'stock_delivery_detail_discount3' => number_format($data->stock_delivery_detail_discount3, $this->precision),
                'stock_delivery_detail_discount4' => number_format($data->stock_delivery_detail_discount4, $this->precision),
                'stock_delivery_detail_remarks' => $data->stock_delivery_detail_remarks,
                'stock_delivery_detail_remaining_qty' => number_format($data->stock_delivery_detail_remaining_qty, $this->precision)
            ),
            $this->product($data),
            $this->systemCode($data, 'stock_delivery_detail_unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}