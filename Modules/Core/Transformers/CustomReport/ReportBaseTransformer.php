<?php

namespace Modules\Core\Transformers\CustomReport;

use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class ReportBaseTransformer
 */
class ReportBaseTransformer extends TransformerAbstract
{

    protected $precision;

    public function __construct()
    {
        $this->precision  = setting('monetary.precision');
    }

    public function product($data)
    {
        return array_merge(
            array(
                'product_stock_no' => $data->product_stock_no,
                'product_supplier_sku' => $data->product_supplier_sku,
                'product_name' => $data->product_name,
                'product_description' => $data->product_description,
                'product_chinese_name' => $data->product_chinese_name,
                'product_senior' => $data->product_senior,
                'product_memo' => $data->product_memo,
                'product_status' => $data->product_status
            ),
            $this->systemCode($data, 'brand_'),
            $this->category($data),
            $this->supplier($data),
            $this->inventory($data)
        );
    }

    public function systemCode($data, $alias = "system_code_")
    {
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'}, 
            sprintf('%sname', $alias) => $data->{$alias.'name'}, 
            sprintf('%sremarks', $alias) => $data->{$alias.'remarks'}
        ];
    }

    public function user($data, $alias)
    {   
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'},
            sprintf('%susername', $alias) => $data->{$alias.'username'},
            sprintf('%sfull_name', $alias) => $data->{$alias.'full_name'},
            sprintf('%sposition', $alias) => $data->{$alias.'position'},
            sprintf('%saddress', $alias) => $data->{$alias.'address'},
            sprintf('%stelephone', $alias) => $data->{$alias.'telephone'},
            sprintf('%smobile', $alias) => $data->{$alias.'mobile'},
            sprintf('%semail', $alias) => $data->{$alias.'email'},
            sprintf('%smemo', $alias) => $data->{$alias.'memo'},
            sprintf('%stype', $alias) => $data->{$alias.'type'},
            sprintf('%srole_id', $alias) => $data->{$alias.'role_id'},
            sprintf('%sstatus', $alias) => $data->{$alias.'status'}
        ];
    }

    public function category($data, $alias = "category_")
    {   
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'}, 
            sprintf('%sname', $alias) => $data->{$alias.'name'}, 
        ];
    }

    public function role($data, $alias = "role_")
    {   
        return  [
            sprintf('%sname', $alias) => $data->{$alias.'name'}, 
        ];
    }

    public function branch($data, $alias = "branch_")
    {
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'},
            sprintf('%sname', $alias) => $data->{$alias.'name'},
            sprintf('%saddress', $alias) => $data->{$alias.'address'},
            sprintf('%scontact', $alias) => $data->{$alias.'contact'},
            sprintf('%sbusiness_name', $alias) => $data->{$alias.'business_name'},
        ];
    }
    
    public function location($data, $alias = "branch_detail_")
    {
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'},
            sprintf('%sname', $alias) => $data->{$alias.'name'},
            sprintf('%saddress', $alias) => $data->{$alias.'address'},
            sprintf('%scontact', $alias) => $data->{$alias.'contact'},
            sprintf('%sbusiness_name', $alias) => $data->{$alias.'business_name'}
        ];
    }

    public function inventory($data, $alias = "product_branch_summary")
    {
        return  [
            sprintf('%sqty', $alias) => number_format($data->{$alias.'qty'}, $this->precision),
            sprintf('%sreserved_qty', $alias) => number_format($data->{$alias.'reserved_qty'}, $this->precision),
            sprintf('%srequested_qty', $alias) => number_format($data->{$alias.'requested_qty'}, $this->precision)
        ];
    }

    public function supplier($data, $alias = "supplier_")
    {
        return  [
            sprintf('%scode', $alias) => $data->{$alias.'code'},
            sprintf('%sfull_name', $alias) => $data->{$alias.'full_name'},
            sprintf('%schinese_name', $alias) => $data->{$alias.'chinese_name'},
            sprintf('%sowner_name', $alias) => $data->{$alias.'owner_name'},
            sprintf('%scontact', $alias) => $data->{$alias.'contact'},
            sprintf('%saddress', $alias) => $data->{$alias.'address'},
            sprintf('%slandline', $alias) => $data->{$alias.'landline'},
            sprintf('%stelephone', $alias) => $data->{$alias.'telephone'},
            sprintf('%scontact', $alias) => $data->{$alias.'contact'},
            sprintf('%sfax', $alias) => $data->{$alias.'fax'},
            sprintf('%smobile', $alias) => $data->{$alias.'mobile'},
            sprintf('%semail', $alias) => $data->{$alias.'email'},
            sprintf('%swebsite', $alias) => $data->{$alias.'website'},
            sprintf('%sterm', $alias) => $data->{$alias.'term'},
            sprintf('%sstatus', $alias) => $data->{$alias.'status'},
            sprintf('%scontact_person', $alias) => $data->{$alias.'contact_person'},
            sprintf('%smemo', $alias) => $data->{$alias.'memo'}
        ];
    }
    
    public function tax($data, $alias = 'tax_')
    {
        return [
            sprintf('%sname', $alias) => $data->{$alias.'name'},
            sprintf('%spercentage', $alias) => number_format($data->{$alias.'percentage'}, $this->precision),
            sprintf('%sis_inclusive', $alias) => $data->{$alias.'is_inclusive'}
        ];
    }

    public function customer($data, $alias = "customer_")
    {   
        return  [
            sprintf('%sname', $alias) => $data->{$alias.'name'},
            sprintf('%scode', $alias) => $data->{$alias.'code'},
            sprintf('%smemo', $alias) => $data->{$alias.'memo'},
            sprintf('%scredit_limit', $alias) => number_format($data->{$alias.'credit_limit'}, $this->precision),
            sprintf('%spricing_type', $alias) => $data->{$alias.'pricing_type'},
            sprintf('%swebsite', $alias) => $data->{$alias.'website'},
            sprintf('%sterm', $alias) => $data->{$alias.'term'},
            sprintf('%sstatus', $alias) => $data->{$alias.'status'},
            sprintf('%sdetail_name', $alias) => $data->{$alias.'detail_name'},
            sprintf('%sdetail_address', $alias) => $data->{$alias.'detail_address'},
            sprintf('%sdetail_chinese_name', $alias) => $data->{$alias.'detail_chinese_name'},
            sprintf('%sdetail_owner_name', $alias) => $data->{$alias.'detail_owner_name'},
            sprintf('%sdetail_contact', $alias) => $data->{$alias.'detail_contact'},
            sprintf('%sdetail_landline', $alias) => $data->{$alias.'detail_landline'},
            sprintf('%sdetail_fax', $alias) => $data->{$alias.'detail_fax'},
            sprintf('%sdetail_mobile', $alias) => $data->{$alias.'detail_mobile'},
            sprintf('%sdetail_email', $alias) => $data->{$alias.'detail_email'},
            sprintf('%sdetail_contact_person', $alias) => $data->{$alias.'detail_contact_person'},
            sprintf('%sdetail_tin', $alias) => $data->{$alias.'detail_tin'}
        ];
    }
}
