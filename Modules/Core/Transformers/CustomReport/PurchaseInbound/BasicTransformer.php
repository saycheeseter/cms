<?php

namespace Modules\Core\Transformers\CustomReport\PurchaseInbound;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{

    /**
     * Transform the PurchaseInbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data) {
        $precision  = setting('monetary.precision');

        return array_merge(
            array(
                'purchase_inbound_reference_sheet_number' => $data->purchase_inbound_reference_sheet_number,
                'purchase_inbound_term' => $data->purchase_inbound_term,
                'purchase_inbound_dr_no' => $data->purchase_inbound_dr_no,
                'purchase_inbound_supplier_invoice_no' => $data->purchase_inbound_supplier_invoice_no,
                'purchase_inbound_supplier_box_count' => $data->purchase_inbound_supplier_box_count,
                'purchase_inbound_audited_date' => $data->purchase_inbound_audited_date,
                'purchase_inbound_transaction_date' => $data->purchase_inbound_transaction_date,
                'purchase_inbound_transaction_type' => $data->purchase_inbound_transaction_type,
                'purchase_inbound_approval_status' => $data->purchase_inbound_approval_status,
                'purchase_inbound_total_amount' => number_format($data->purchase_inbound_total_amount, $precision),
                'purchase_inbound_remaining_amount' => number_format($data->purchase_inbound_remaining_amount, $precision),
                'purchase_inbound_sheet_number' => $data->purchase_inbound_sheet_number,
                'purchase_inbound_remarks' => $data->purchase_inbound_remarks,
                'purchase_inbound_total_qty' => number_format($data->purchase_inbound_total_qty, $precision)
            ),
            $this->supplier($data, 'purchase_inbound_supplier_'),
            $this->systemCode($data, 'purchase_inbound_payment_method_'),
            $this->branch($data, 'purchase_inbound_for_location_'),
            $this->user($data, 'purchase_inbound_requested_by_'),
            $this->role($data, 'purchase_inbound_requested_by_role_')
        );
    }

    public function detail($data) {
        $precision  = setting('monetary.precision');
        
         return array_merge(
            array(
            'purchase_inbound_detail_product' => $data->purchase_inbound_detail_product,
            'purchase_inbound_detail_unit_qty' => number_format($data->purchase_inbound_detail_unit_qty, $precision),
            'purchase_inbound_detail_qty' => number_format($data->purchase_inbound_detail_qty, $precision),
            'purchase_inbound_detail_oprice' => number_format($data->purchase_inbound_detail_oprice, $precision),
            'purchase_inbound_detail_price' => number_format($data->purchase_inbound_detail_price, $precision),
            'purchase_inbound_detail_discount1' => number_format($data->purchase_inbound_detail_discount1, $precision),
            'purchase_inbound_detail_discount2' => number_format($data->purchase_inbound_detail_discount2, $precision),
            'purchase_inbound_detail_discount3' => number_format($data->purchase_inbound_detail_discount3, $precision),
            'purchase_inbound_detail_discount4' => number_format($data->purchase_inbound_detail_discount4, $precision),
            'purchase_inbound_detail_remarks' => $data->purchase_inbound_detail_remarks,
            ),
            $this->product($data),
            $this->systemCode($data, 'unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}
