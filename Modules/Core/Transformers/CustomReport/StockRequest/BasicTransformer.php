<?php

namespace Modules\Core\Transformers\CustomReport\StockRequest;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{
    /**
     * Transform the SalesOutbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {
        return array_merge(
            array(
                'stock_request_sheet_number' => $data->stock_request_sheet_number,
                'stock_request_remarks' => $data->stock_request_remarks,
                'stock_request_transaction_date' => $data->stock_request_transaction_date,
                'stock_request_audited_date' => $data->stock_request_audited_date,
                'stock_request_approval_status' => $data->stock_request_approval_status,
                'stock_request_transaction_status' => $data->stock_request_transaction_status,
                'stock_request_total_amount' => number_format($data->stock_request_total_amount, $this->precision)
            ),
            $this->user($data, 'stock_request_requested_by_'),
            $this->role($data, 'stock_request_requested_by_role_'),
            $this->branch($data, 'stock_request_created_for_'),
            $this->branch($data, 'stock_request_for_location_'),
            $this->branch($data, 'stock_request_request_to_'),
            $this->location($data, 'stock_request_for_location_')
        );
    }

    public function detail($data)
    {
        return array_merge(
            array(
                'stock_request_detail_unit_qty' => number_format($data->stock_request_detail_unit_qty, $this->precision),
                'stock_request_detail_oprice' => number_format($data->stock_request_detail_oprice, $this->precision),
                'stock_request_detail_price' => number_format($data->stock_request_detail_price, $this->precision),
                'stock_request_detail_discount1' => number_format($data->stock_request_detail_discount1, $this->precision),
                'stock_request_detail_discount2' => number_format($data->stock_request_detail_discount2, $this->precision),
                'stock_request_detail_discount3' => number_format($data->stock_request_detail_discount3, $this->precision),
                'stock_request_detail_discount4' => number_format($data->stock_request_detail_discount4, $this->precision),
                'stock_request_detail_remarks' => $data->stock_request_detail_remarks,
                'stock_request_detail_qty' => number_format($data->stock_request_detail_qty, $this->precision),
                'stock_request_detail_remaining_qty' => number_format($data->stock_request_detail_remaining_qty, $this->precision)
            ),
            $this->product($data),
            $this->systemCode($data, 'stock_request_detail_unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}