<?php

namespace Modules\Core\Transformers\CustomReport\Purchase;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{

    /**
     * Transform the Purchase Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {
        return array_merge(
            array(
                'purchase_term' => $data->purchase_term,
                'purchase_audited_date' => $data->purchase_audited_date,
                'purchase_transaction_date' => $data->purchase_transaction_date,
                'purchase_approval_status' => $data->purchase_approval_status,
                'purchase_transaction_status' => $data->purchase_transaction_status,
                'purchase_total_amount' => number_format($data->purchase_total_amount, $this->precision),
                'purchase_sheet_number' => $data->purchase_sheet_number,
                'purchase_remarks' => $data->purchase_remarks,
                'purchase_total_remaining_qty' => number_format($data->purchase_total_remaining_qty, $this->precision),
                'purchase_total_qty' => number_format($data->purchase_total_qty, $this->precision)
            ),
            $this->supplier($data, 'purchase_supplier_'),
            $this->systemCode($data, 'purchase_payment_method_'),
            $this->branch($data, 'purchase_created_for_'),
            $this->location($data, 'purchase_for_location_'),
            $this->user($data, 'purchase_requested_by_'),
            $this->role($data, 'purchase_requested_by_role_')
        );
    }

    public function detail($data)
    {   
         return array_merge(
            array(
                'purchase_detail_product' => $data->purchase_detail_product,
                'purchase_detail_unit_qty' => number_format($data->purchase_detail_unit_qty, $this->precision),
                'purchase_detail_qty' => number_format($data->purchase_detail_qty, $this->precision),
                'purchase_detail_oprice' => number_format($data->purchase_detail_oprice, $this->precision),
                'purchase_detail_price' => number_format($data->purchase_detail_price, $this->precision),
                'purchase_detail_discount1' => number_format($data->purchase_detail_discount1, $this->precision),
                'purchase_detail_discount2' => number_format($data->purchase_detail_discount2, $this->precision),
                'purchase_detail_discount3' => number_format($data->purchase_detail_discount3, $this->precision),
                'purchase_detail_discount4' => number_format($data->purchase_detail_discount4, $this->precision),
                'purchase_detail_remarks' => $data->purchase_detail_remarks,
            ),
            $this->product($data),
            $this->systemCode($data, 'unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}
