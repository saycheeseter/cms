<?php

namespace Modules\Core\Transformers\CustomReport;

use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;
use Carbon\Carbon;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\Status;
use Modules\Core\Enums\UserType;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{
    protected $alias;

    /**
     * Transform the Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        $return = [];
        
        foreach ($data as $key => $value) {
            $return[$key] = $this->convertibles($key, $value);
        }

        return $return;
    }

    public function convertibles($key, $value)
    {
        $stringKey = (string)$key;

        if($this->endsWith($stringKey, 'transaction_type')) {
            return $value == TransactionType::DIRECT ? Lang::get('core::label.direct') : Lang::get('core::label.from.invoice');
        } else if($this->endsWith($stringKey, 'customer_type')) {
            return $value == Customer::WALK_IN ? Lang::get('core::label.walk.in.customer') : Lang::get('core::label.regular.customer');
        } else if($this->endsWith($stringKey, 'approval_status')) {
            switch ($value) {
                case ApprovalStatus::DRAFT:
                    return $value = Lang::get('core::label.draft');
                    break;
                case ApprovalStatus::FOR_APPROVAL:
                    return $value = Lang::get('core::label.for.approval');
                    break;
                case ApprovalStatus::APPROVED:
                    return $value = Lang::get('core::label.approved');
                    break;
                case ApprovalStatus::DECLINED:
                    return $value = Lang::get('core::label.declined');
                    break;
            }
        } else if($this->endsWith($stringKey, 'transaction_status')) {
            switch ($value) {
                case TransactionStatus::COMPLETE:
                    return $value = Lang::get('core::label.complete');
                    break;
                case TransactionStatus::INCOMPLETE:
                    return $value = Lang::get('core::label.incomplete');
                    break;
                case TransactionStatus::EXCESS:
                    return $value = Lang::get('core::label.excess');
                    break;
                case TransactionStatus::NO_DELIVERY:
                    return $value = Lang::get('core::label.no.delivery');
                    break;
            }
        } else if($this->endsWith($stringKey, 'status')) {
            return $value == Status::ACTIVE ? Lang::get('core::label.active') : Lang::get('core::label.inactive');
        } else if($this->endsWith($stringKey, 'type')) {
            return $value == UserType::SUPERADMIN ? Lang::get('core::label.superadmin') : Lang::get('core::label.default');
        } else if(
            $this->endsEitherWith($stringKey, 
                ['amount', 'qty', 'price', 'discount1', 'discount2', 'discount3', 'discount4', 'discount5',]
            )
        ) {
            return $value = number_format($value, setting('monetary.precision'));
        } else if($this->endsWith($stringKey, 'date')) {
            return $value = Carbon::parse($value)->format('Y-m-d H:i:s');
        } else {
            return $value;
        }
    }

    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 || 
            (substr($haystack, -$length) === $needle);
    }

    private function endsEitherWith($haystack, $needles)
    {   
        $bool = false;

        foreach ($needles as $key => $needle) {
            $length = strlen($needle);
            if($length === 0 || (substr($haystack, -$length) === $needle)) {
                $bool = true;
                break;
            }
        }

        return $bool;
    }
}
