<?php

namespace Modules\Core\Transformers\CustomReport\SalesOutbound;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{

    /**
     * Transform the SalesOutbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {
        
        $precision  = setting('monetary.precision');

        return array_merge(
            array(
                'sales_outbound_reference_sheet_number' => $data->sales_outbound_reference_sheet_number,
                'sales_outbound_term' => $data->sales_outbound_term,
                'sales_outbound_audited_date' => $data->sales_outbound_audited_date,
                'sales_outbound_transaction_date' => $data->sales_outbound_transaction_date,
                'sales_outbound_transaction_type' => $data->sales_outbound_transaction_type,
                'sales_outbound_approval_status' => $data->sales_outbound_approval_status,
                'sales_outbound_total_amount' => number_format($data->sales_outbound_total_amount, $precision),
                'sales_outbound_remaining_amount' => number_format($data->sales_outbound_remaining_amount, $precision),
                'sales_outbound_sheet_number' => $data->sales_outbound_sheet_number,
                'sales_outbound_remarks' => $data->sales_outbound_remarks,
                'sales_outbound_customer_type' => $data->sales_outbound_customer_type,
                'sales_outbound_customer_walk_in_name' => $data->sales_outbound_customer_walk_in_name,
                'sales_outbound_total_qty' => number_format($data->sales_outbound_total_qty, $precision)
            ),
            $this->customer($data, 'sales_outbound_customer_'),
            $this->user($data, 'sales_outbound_salesman_'),
            $this->role($data, 'sales_outbound_salesman_role_'),
            $this->branch($data, 'sales_outbound_for_location_'),
            $this->user($data, 'sales_outbound_requested_by_'),
            $this->role($data, 'sales_outbound_requested_by_role_')
        );
    }

    public function detail($data)
    {
        $precision  = setting('monetary.precision');
        
         return array_merge(
            array(
                'sales_outbound_detail_product' => $data->sales_outbound_detail_product,
                'sales_outbound_detail_unit_qty' => number_format($data->sales_outbound_detail_unit_qty, $precision),
                'sales_outbound_detail_qty' => number_format($data->sales_outbound_detail_qty, $precision),
                'sales_outbound_detail_oprice' => number_format($data->sales_outbound_detail_oprice, $precision),
                'sales_outbound_detail_price' => number_format($data->sales_outbound_detail_price, $precision),
                'sales_outbound_detail_discount1' => number_format($data->sales_outbound_detail_discount1, $precision),
                'sales_outbound_detail_discount2' => number_format($data->sales_outbound_detail_discount2, $precision),
                'sales_outbound_detail_discount3' => number_format($data->sales_outbound_detail_discount3, $precision),
                'sales_outbound_detail_discount4' => number_format($data->sales_outbound_detail_discount4, $precision),
                'sales_outbound_detail_remarks' => $data->sales_outbound_detail_remarks,
            ),
            $this->product($data),
            $this->systemCode($data, 'sales_outbound_detail_unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}
