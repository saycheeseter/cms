<?php

namespace Modules\Core\Transformers\CustomReport\Sales;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{

    /**
     * Transform the SalesOutbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {

        return array_merge(
            array(
                'sales_transaction_status' => $data->sales_transaction_status,
                'sales_term' => $data->sales_term,
                'sales_audited_date' => $data->sales_audited_date,
                'sales_transaction_date' => $data->sales_transaction_date,
                'sales_approval_status' => $data->sales_approval_status,
                'sales_total_amount' => number_format($data->sales_total_amount, $this->precision),
                'sales_remaining_amount' => number_format($data->sales_remaining_amount, $this->precision),
                'sales_sheet_number' => $data->sales_sheet_number,
                'sales_remarks' => $data->sales_remarks,
                'sales_customer_type' => $data->sales_customer_type,
                'sales_customer_walk_in_name' => $data->sales_customer_walk_in_name,
                'sales_total_qty' => number_format($data->sales_total_qty, $this->precision)
            ),
            $this->customer($data, 'sales_customer_'),
            $this->user($data, 'sales_salesman_'),
            $this->role($data, 'sales_salesman_role_'),
            $this->branch($data, 'sales_for_location_'),
            $this->user($data, 'sales_requested_by_'),
            $this->role($data, 'sales_requested_by_role_')
        );
    }

    public function detail($data)
    {
    
         return array_merge(
            array(
                'sales_detail_product' => $data->sales_detail_product,
                'sales_detail_unit_qty' => number_format($data->sales_detail_unit_qty, $this->precision),
                'sales_detail_qty' => number_format($data->sales_detail_qty, $this->precision),
                'sales_detail_oprice' => number_format($data->sales_detail_oprice, $this->precision),
                'sales_detail_price' => number_format($data->sales_detail_price, $this->precision),
                'sales_detail_discount1' => number_format($data->sales_detail_discount1, $this->precision),
                'sales_detail_discount2' => number_format($data->sales_detail_discount2, $this->precision),
                'sales_detail_discount3' => number_format($data->sales_detail_discount3, $this->precision),
                'sales_detail_discount4' => number_format($data->sales_detail_discount4, $this->precision),
                'sales_detail_remarks' => $data->sales_detail_remarks,
            ),
            $this->product($data),
            $this->systemCode($data, 'sales_detail_unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}
