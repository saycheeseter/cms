<?php

namespace Modules\Core\Transformers\CustomReport\StockDeliveryOutbound;

use Modules\Core\Transformers\CustomReport\ReportBaseTransformer;
use League\Fractal\TransformerAbstract;
use Lang;
use Litipk\BigNumbers\Decimal;

/**
 * Class BasicTransformer
 */
class BasicTransformer extends ReportBaseTransformer
{

    /**
     * Transform the StockDeliveryOutbound Report Builder data
     * @param $data
     *
     * @return array
     */
    public function transform($data)
    {   
        return array_merge(
            $this->info($data),
            $this->detail($data)
        );
    }

    public function info($data)
    {
        return array_merge(
            array(
                'stock_delivery_outbound_reference_sheet_number' => $data->stock_delivery_outbound_reference_sheet_number,
                'stock_delivery_outbound_audited_date' => $data->stock_delivery_outbound_audited_date,
                'stock_delivery_outbound_transaction_date' => $data->stock_delivery_outbound_transaction_date,
                'stock_delivery_outbound_transaction_type' => $data->stock_delivery_outbound_transaction_type,
                'stock_delivery_outbound_approval_status' => $data->stock_delivery_outbound_approval_status,
                'stock_delivery_outbound_transaction_status' => $data->stock_delivery_outbound_transaction_status,
                'stock_delivery_outbound_total_amount' => number_format($data->stock_delivery_outbound_total_amount, $this->precision),
                'stock_delivery_outbound_sheet_number' => $data->stock_delivery_outbound_sheet_number,
                'stock_delivery_outbound_remarks' => $data->stock_delivery_outbound_remarks,
                'stock_delivery_outbound_total_qty' => number_format($data->stock_delivery_outbound_total_qty, $this->precision)
            ),
            $this->systemCode($data, 'stock_delivery_outbound_payment_method_'),
            $this->branch($data, 'stock_delivery_outbound_created_for_'),
            $this->location($data, 'stock_delivery_outbound_for_location_'),
            $this->branch($data, 'stock_delivery_outbound_deliver_to_'),
            $this->location($data, 'stock_delivery_outbound_deliver_to_location_'),
            $this->user($data, 'stock_delivery_outbound_requested_by_'),
            $this->role($data, 'stock_delivery_outbound_requested_by_role_')
        );
    }

    public function detail($data)
    {   
        return array_merge(
            array(
                'stock_delivery_outbound_detail_product' => $data->stock_delivery_outbound_detail_product,
                'stock_delivery_outbound_detail_unit_qty' => number_format($data->stock_delivery_outbound_detail_unit_qty, $this->precision),
                'stock_delivery_outbound_detail_qty' => number_format($data->stock_delivery_outbound_detail_qty, $this->precision),
                'stock_delivery_outbound_detail_oprice' => number_format($data->stock_delivery_outbound_detail_oprice, $this->precision),
                'stock_delivery_outbound_detail_price' => number_format($data->stock_delivery_outbound_detail_price, $this->precision),
                'stock_delivery_outbound_detail_discount1' => number_format($data->stock_delivery_outbound_detail_discount1, $this->precision),
                'stock_delivery_outbound_detail_discount2' => number_format($data->stock_delivery_outbound_detail_discount2, $this->precision),
                'stock_delivery_outbound_detail_discount3' => number_format($data->stock_delivery_outbound_detail_discount3, $this->precision),
                'stock_delivery_outbound_detail_discount4' => number_format($data->stock_delivery_outbound_detail_discount4, $this->precision),
                'stock_delivery_outbound_detail_remarks' => $data->stock_delivery_outbound_detail_remarks,
            ),
            $this->product($data),
            $this->systemCode($data, 'stock_delivery_outbound_detail_unit_'),
            $this->user($data, 'user_'),
            $this->role($data, 'user_role_')
        );
    }
}
