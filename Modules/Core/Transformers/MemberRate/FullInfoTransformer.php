<?php

namespace Modules\Core\Transformers\MemberRate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\MemberRate;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\MemberRate;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the MemberRate entity
     * @param MemberRate $model
     *
     * @return array
     */
    public function transform(MemberRate $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'memo'          => $model->memo ?? '',
            'status'        => $model->status
        ];
    }
}
