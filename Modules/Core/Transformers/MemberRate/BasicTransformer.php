<?php

namespace Modules\Core\Transformers\MemberRate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\MemberRate;
use Modules\Core\Enums\Status;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\MemberRate;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the MemberRate entity
     * @param MemberRate $model
     *
     * @return array
     */
    public function transform(MemberRate $model)
    {
        return [
            'id'     => $model->id,
            'name'   => $model->name,
            'memo'   => $model->memo ?? '',
            'status' => $model->presenter()->status
        ];
    }
}
