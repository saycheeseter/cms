<?php 

namespace Modules\Core\Transformers\MemberRate;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\MemberRate;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\MemberRate;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the MemberRate entity
     * @param MemberRate $model
     *
     * @return array
     */
    public function transform(MemberRate $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}