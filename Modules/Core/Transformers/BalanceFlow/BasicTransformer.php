<?php

namespace Modules\Core\Transformers\BalanceFlow;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BalanceFlow;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BalanceFlow;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the BalanceFlow entity
     * @param BalanceFlow $model
     *
     * @return array
     */
    public function transform($model)
    {
        return [
            'date'        => (string) $model->transaction_date,
            'particular'  => $model->presenter()->particulars,
            'in'          => $model->presenter()->in,
            'out'         => $model->presenter()->out,
        ];
    }
}
