<?php

namespace Modules\Core\Transformers\BalanceFlow;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BalanceFlow;

/**
 * Class BalanceTransformer
 * @package namespace Modules\Core\Transformers\BalanceFlow;
 */
class BalanceTransformer extends TransformerAbstract
{
    protected $defaults = [
        'amount' => 0
    ];

    /**
     * Transform the BalanceFlow entity
     * @param BalanceFlow $model
     *
     * @return array
     */
    public function transform(BalanceFlow $model)
    {
        if (is_null($model)) {
            return $this->defaults;
        }

        return [
            'amount'  => $model->number()->balance,
        ];
    }
}
