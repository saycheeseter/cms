<?php

namespace Modules\Core\Transformers\BalanceFlow;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BalanceFlow;

/**
 * Class BeginningBalanceTransformer
 * @package namespace Modules\Core\Transformers\BalanceFlow;
 */
class BeginningBalanceTransformer extends TransformerAbstract
{
    /**
     * Transform the BalanceFlow entity
     * @param BalanceFlow $model
     *
     * @return array
     */
    public function transform($model)
    {
        if (is_null($model)) {
            return [
                'amount' => number_format(0, setting('monetary.precision'))
            ];
        }

        return [
            'amount'  => $model->number()->beginning,
        ];
    }
}
