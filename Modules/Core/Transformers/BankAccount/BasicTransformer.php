<?php

namespace Modules\Core\Transformers\BankAccount;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BankAccount;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\BankAccount;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the BankAccount entity
     * @param BankAccount $model
     *
     * @return array
     */
    public function transform(BankAccount $model)
    {
        return [
            'id'                => $model->id,
            'bank_id'           => $model->bank_id,
            'account_name'      => $model->account_name,
            'account_number'    => $model->account_number,
            'opening_balance'   => $model->number()->opening_balance,
        ];
    }
}
