<?php

namespace Modules\Core\Transformers\BankAccount;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\BankAccount;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\BankAccount;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the BankAccount entity
     * @param BankAccount $model
     *
     * @return array
     */
    public function transform(BankAccount $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->account_name
        ];
    }
}
