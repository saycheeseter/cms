<?php

namespace Modules\Core\Transformers\MonthlySalesRanking;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\BrandMonthlySales;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductMonthlySales;
 */
class BrandTransformer extends TransformerAbstract
{
    /**
     * Transform the Brand entity
     * @param BrandMonthlySales $model
     *
     * @return array
     */
    public function transform(BrandMonthlySales $model)
    {
        return [
            'code'         => $model->code,
            'name'         => $model->name,
            'remarks'      => $model->remarks,
            'total_qty'    => $model->number()->total_qty,
            'total_amount' => $model->number()->total_amount,
            'total_profit' => $model->number()->total_profit
        ];
    }
}
