<?php


namespace Modules\Core\Transformers\MonthlySalesRanking;


use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\CategoryMonthlySalesRanking;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(CategoryMonthlySalesRanking $model)
    {
        return [
            'category_code'      => $model->category_code,
            'category_name'      => $model->category_name,
            'total_qty'          => $model->number()->total_qty,
            'total_amount'       => $model->number()->total_amount,
            'total_profit'       => $model->number()->total_profit
        ];
    }
}