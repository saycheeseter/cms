<?php

namespace Modules\Core\Transformers\MonthlySalesRanking;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\ProductMonthlySales;

/**
 * Class ProductTransformer
 * @package namespace Modules\Core\Transformers\MonthlySalesRanking;
 */
class ProductTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductMonthlySales entity
     * @param ProductMonthlySales $model
     *
     * @return array
     */
    public function transform(ProductMonthlySales $model)
    {
        return [
            'stock_no'       => $model->stock_no,
            'name'           => $model->name,
            'chinese_name'   => $model->chinese_name ?? '',
            'description'    => $model->description ?? '',
            'supplier'       => $model->supplier,
            'category'       => $model->category,
            'brand'          => $model->brand,
            'total_quantity' => $model->number()->total_quantity,
            'total_amount'   => $model->number()->total_amount,
            'total_cost'     => $model->number()->total_cost,
            'total_profit'   => $model->number()->total_profit
        ];
    }
}
