<?php

namespace Modules\Core\Transformers\MonthlySalesRanking;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Hydration\SupplierMonthlySalesRanking;

class SupplierTransformer extends TransformerAbstract
{
    public function transform(SupplierMonthlySalesRanking $model)
    {
        return [
            'supplier_code'      => $model->supplier_code,
            'supplier_full_name' => $model->supplier_full_name,
            'total_qty'          => $model->number()->total_qty,
            'total_amount'       => $model->number()->total_amount,
            'total_profit'       => $model->number()->total_profit
        ];
    }
}