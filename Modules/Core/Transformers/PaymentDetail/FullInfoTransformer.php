<?php

namespace Modules\Core\Transformers\PaymentDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PaymentDetail;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PaymentDetail;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PaymentDetail entity
     * @param PaymentDetail $model
     *
     * @return array
     */
    public function transform(PaymentDetail $model)
    {
        return [
            'id'              => $model->id,
            'payment_method'  => $model->payment_method,
            'bank_account_id' => $model->bank_account_id,
            'check_no'        => $model->check_no,
            'check_date'      => $model->check_date ? $model->check_date->toDateString() : null,
            'amount'          => $model->number()->amount
        ];
    }
}