<?php

namespace Modules\Core\Transformers\CheckManagement;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Contracts\CheckManagementTransformer;
use Modules\Core\Presenters\TransactionDetailPresenter;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\CheckManagement;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the CheckManagementTransformer entity
     * @param CheckManagementTransformer $model
     *
     * @return array
     */
    public function transform(CheckManagementTransformer $model)
    {
        return [
            'id' => $model->id,
            'created_from' => $model->from->name,
            'transaction_date' => $model->transaction_date->toDateString(),
            'bank' => $model->bank->account_name,
            'check_no' => $model->check_no,
            'check_date' => $model->check_date->toDateString(),
            'transfer_date' => is_null($model->transfer_date) ? '' : $model->transfer_date->toDateString(),
            'amount' => $model->number()->amount,
            'remarks' => $model->remarks,
            'type' => $model->transactionable_type,
            'transaction_id' => $model->redirect_id,
            'status' => $model->presenter()->check_status
        ];
    }
}
