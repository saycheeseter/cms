<?php

namespace Modules\Core\Transformers\OtherIncomeDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\OtherIncomeDetail;
use Lang;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\OtherIncomeDetail;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the OtherIncomeDetail entity
     * @param OtherIncomeDetail $model
     *
     * @return array
     */
    public function transform(OtherIncomeDetail $model)
    {
        return [
            'id'     => $model->id,
            'transaction_id'   => $model->transaction_id,
            'amount'   => $model->number()->amount,
            'type'   => $model->type,
        ];
    }
}
