<?php

namespace Modules\Core\Transformers\ProductStockCard;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductStockCard;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductStockCard;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductStockCard entity
     * 
     * @param ProductStockCard $model
     * @return array
     */
    public function transform(ProductStockCard $model)
    {
        return [
            'audited_date'     => $model->created_at->toDateTimeString(),
            'created_for'      => $model->branch->name,
            'for_location'     => $model->location->name,
            'product_id'       => $model->product->name,
            'sequence_no'      => $model->sequence_no,
            'reference_type'   => $model->presenter()->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'qty'              => $model->number()->qty,
            'price'            => $model->number()->price,
            'amount'           => $model->number()->amount,
            'cost'             => $model->number()->cost,
            'pre_inventory'    => $model->number()->pre_inventory,
            'pre_avg_cost'     => $model->number()->pre_avg_cost,
            'pre_amount'       => $model->number()->pre_amount,
            'post_inventory'   => $model->number()->post_inventory,
            'post_avg_cost'    => $model->number()->post_avg_cost,
            'post_amount'      => $model->number()->post_amount
        ];
    }
}
