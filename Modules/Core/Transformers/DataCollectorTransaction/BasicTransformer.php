<?php

namespace Modules\Core\Transformers\DataCollectorTransaction;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\DataCollectorTransaction;
use Modules\Core\Transformers\DataCollectorTransactionDetail\BasicTransformer as DetailTransformer;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\DataCollectorTransaction;
 */
class BasicTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'details'
    ];

    /**
     * Transform the DataCollectorTransaction entity
     * @param DataCollectorTransaction $model
     *
     * @return array
     */
    public function transform(DataCollectorTransaction $model)
    {
        return [
            'id'               => $model->id,
            'branch'           => $model->branch->name,
            'reference_no'     => $model->reference_no,
            'filesize'         => $model->filesize,
            'client_id'        => $model->client_id,
            'transaction_type' => $model->presenter()->transaction_label,
            'source'           => $model->presenter()->source_label,
            'row_count'        => $model->row_count,
            'import_date'      => $model->import_date->toDateString(),
            'imported_by'      => $model->importer->full_name
        ];
    }

    public function includeDetails(DataCollectorTransaction $model)
    {
        return $this->collection($model->details, new DetailTransformer);
    }
}
