<?php

namespace Modules\Core\Transformers\PosSummaryPosting;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PosSummaryPosting;
use Modules\Core\Entities\PrintoutTemplate;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\PrintoutTemplate;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the PrintoutTemplate entity
     * @param PrintoutTemplate $model
     *
     * @return array
     */
    public function transform(PosSummaryPosting $model)
    {
        return [
            'id'              => $model->id,
            'branch'          => [
                'id'   => $model->branch_id,
                'name' => $model->branch->name
            ],
            'terminal_number' => $model->terminal_number,
            'status'          => [
                'value' => $model->status,
                'label' => $model->presenter()->status
            ],
            'last_post_date'  => !is_null($model->last_post_date)
                ? $model->last_post_date->toDateTimeString()
                : ''
        ];
    }
}
