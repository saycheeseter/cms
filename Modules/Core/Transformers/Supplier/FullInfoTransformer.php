<?php

namespace Modules\Core\Transformers\Supplier;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Supplier;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\Supplier;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the Supplier entity
     * @param Supplier $model
     *
     * @return array
     */
    public function transform(Supplier $model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'contact'        => $model->contact,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'telephone'      => $model->telephone,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'term'           => $model->term,
            'status'         => $model->status,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'owner_name'     => $model->owner_name
        ];
    }
}
