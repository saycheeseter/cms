<?php

namespace Modules\Core\Transformers\Supplier;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Supplier;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\Supplier;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the Supplier entity
     * @param Supplier $model
     *
     * @return array
     */
    public function transform(Supplier $model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'contact'        => $model->contact,
            'owner_name'     => $model->owner_name,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'term'           => $model->term,
            'status'         => $model->presenter()->status,
        ];
    }
}
