<?php

namespace Modules\Core\Transformers\Supplier;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Supplier;

/**
 * Class ChosenTransformer
 * @package namespace Modules\Core\Transformers\Supplier;
 */
class ChosenTransformer extends TransformerAbstract
{
    /**
     * Transform the Supplier entity
     * @param Supplier $model
     *
     * @return array
     */
    public function transform(Supplier $model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->full_name,
        ];
    }
}
