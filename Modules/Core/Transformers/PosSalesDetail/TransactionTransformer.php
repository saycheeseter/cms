<?php

namespace Modules\Core\Transformers\PosSalesDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PosSalesDetail;

/**
 * Class TransactionTransformer
 * @package namespace Modules\Core\Transformers\PosSalesDetail;
 */
class TransactionTransformer extends TransformerAbstract
{
    /**
     * Transform the PosSalesDetail entity
     * @param PosSalesDetail $model
     *
     * @return array
     */
    public function transform(PosSalesDetail $model)
    {
        return [
            'transaction_id'   => $model->transaction->id,
            'transaction_date' => $model->transaction->transaction_date->toDateTimeString(),
            'sheet_number'     => $model->transaction->or_number,
            'particulars'      => $model->transaction->presenter()->particular,
            'qty'              => $model->presenter()->abs_added_inventory_value,
            'price'            => $model->number()->price,
            'amount'           => $model->presenter()->abs_amount,
            'prepared_by'      => $model->transaction->cashier->full_name,
            'remarks'          => $model->transaction->remarks
        ];
    }
}
