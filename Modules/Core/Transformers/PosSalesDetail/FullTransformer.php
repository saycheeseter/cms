<?php

namespace Modules\Core\Transformers\PosSalesDetail;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\PosSalesDetail;

class FullTransformer extends TransformerAbstract
{
    public function transform(PosSalesDetail $model)
    {
        return [
            'id'            => $model->id,
            'barcode'       => $model->presenter()->unit_barcode,
            'stock_no'      => $model->product->stock_no,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'qty'           => $model->number()->qty,
            'uom'           => $model->unit->name,
            'unit_qty'      => $model->number()->unit_qty,
            'total_qty'     => $model->number()->total_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'remarks'       => $model->remarks,
            'vat'           => $model->number()->vat,
            'is_senior'     => $model->presenter()->is_senior
        ];
    }
}