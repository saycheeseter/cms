<?php

namespace Modules\Core\Transformers\ProductCustomer;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\ProductCustomer;

/**
 * Class BasicTransformer
 * @package namespace Modules\Core\Transformers\ProductCustomer;
 */
class BasicTransformer extends TransformerAbstract
{

    /**
     * Transform the ProductCustomer entity
     * @param ProductCustomer $model
     *
     * @return array
     */
    public function transform(ProductCustomer $model)
    {
        return [
            'customer'                   => $model->customer->name,
            'product'                    => $model->product->name,
            'latest_unit'                => $model->unit->name,
            'latest_unit_qty'            => $model->number()->latest_unit_qty,
            'latest_price'               => $model->number()->latest_price,
            'latest_fields_updated_date' => $model->latest_fields_updated_date->toDateString(),
            'lowest_price'               => $model->number()->lowest_price,
            'highest_price'              => $model->number()->highest_price,
            'remarks'                    => $model->remarks,
        ];
    }
}
