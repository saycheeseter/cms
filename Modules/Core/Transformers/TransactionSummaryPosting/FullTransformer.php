<?php

namespace Modules\Core\Transformers\TransactionSummaryPosting;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\TransactionSummaryPosting;

class FullTransformer extends TransformerAbstract
{
    public function transform(TransactionSummaryPosting $model)
    {
        return [
            'sheet_number'     => $model->transaction->sheet_number ?? '',
            'transaction_id'   => $model->transaction_id,
            'transaction_type' => [
                'value' => $model->transaction_type,
                'label' => $model->presenter()->transaction_type
            ],
            'transaction_date' => $model->transaction_date->toDateString()
        ];
    }
}