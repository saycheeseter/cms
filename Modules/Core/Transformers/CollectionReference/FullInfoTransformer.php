<?php

namespace Modules\Core\Transformers\CollectionReference;

use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\CollectionReference;

/**
 * Class FullInfoTransformer
 * @package namespace Modules\Core\Transformers\CollectionReference;
 */
class FullInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the CollectionReference entity
     * @param CollectionReference $model
     *
     * @return array
     */
    public function transform(CollectionReference $model)
    {
        $transaction = $model->reference;

        return [
            'id'               => $model->id,
            'reference_id'     => $transaction->id,
            'reference_type'   => $model->reference_type,
            'transaction_date' => $transaction->transaction_date->toDateTimeString(),
            'sheet_number'     => $transaction->sheet_number,
            'term'             => $transaction->term,
            'dr_no'            => $transaction->dr_no,
            'remarks'          => $transaction->remarks,
            'amount'           => $transaction->number()->total_amount,
            'collected_amount' => $transaction->number()->collected_amount,
            'collectible'      => $model->number()->collectible,
            'remaining'        => $transaction->number()->remaining_amount,
        ];
    }
}