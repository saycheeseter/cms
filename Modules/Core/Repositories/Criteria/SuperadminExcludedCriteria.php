<?php

namespace Modules\Core\Repositories\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Modules\Core\Enums\UserType;
use Auth;

/**
 * Class SuperadminExcludedCriteria
 * @package namespace Modules\Core\Repositories\Criteria;
 */
class SuperadminExcludedCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $exclude = Auth::user()->type == UserType::SUPERADMIN
            ? [UserType::SUPERADMIN]
            : [UserType::SUPERADMIN, UserType::ADMIN];

        return $model->whereNotIn('type', $exclude);
    }
}
