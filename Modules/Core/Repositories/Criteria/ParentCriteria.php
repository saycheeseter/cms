<?php

namespace Modules\Core\Repositories\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class ParentCriteria
 * @package namespace Modules\Core\Repositories\Criteria;
 */
class ParentCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('parent_id')) {
            $model = $model->where('parent_id', $this->request->get('parent_id'));
        }

        return $model;
    }
}
