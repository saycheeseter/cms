<?php

namespace Modules\Core\Repositories\Criteria;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class RequestCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $fieldsSearchable = $repository->getFieldsSearchable();
        $search = $this->request->get('search');
        $orderBy = $this->request->get('orderBy');
        $sortedBy = $this->request->get('sortedBy', 'asc');
        $with = $this->request->get('with');
        $withTrashed = filter_var($this->request->get('withTrashed', false), FILTER_VALIDATE_BOOLEAN);

        if ($search && count($fieldsSearchable) > 0) {
            $model = $this->buildWhereConditions($model, $fieldsSearchable, $search);
        }

        if ($orderBy && !empty($orderBy)) {
            $model = $this->buildOrderByFields($model, $orderBy, $sortedBy);
        }

        if ($with) {
            $model = $this->buildEagerLoad($model, $with);
        }

        if ($withTrashed) {
            $model = $model->withTrashed();
        }

        return $model;
    }

    /**
     * Build the where condition of the model based on the search expression and
     * the indicated searchable fields in the repository
     *
     * @param $model
     * @param $fieldsSearchable
     * @param $search
     * @return mixed
     */
    private function buildWhereConditions($model, $fieldsSearchable, $search)
    {
        return $model->where(function ($query) use ($fieldsSearchable, $search) {
            $expressions = explode(';', $search);

            foreach ($expressions as $expression) {
                extract($this->getConditionExpression($expression));

                // If fields are not listed in the fieldSearchable property, skip the current expression
                if (!in_array((!is_null($relation) ? ($relation.'.') : '').$field, $fieldsSearchable)) {
                    continue;
                }

                $table = $query->getModel()->getTable();

                $method = Str::camel(($condition === 'or'
                    ? 'or_'
                    : ''
                ).'where');

                // Relations can also be queried as long as the field name is written as
                // e.g product.name:foo
                if ($relation) {
                    $query->{$method.'Has'}($relation, function($query) use($field, $operator, $value) {
                        $query->where($field, $operator, $value);
                    });
                } else {
                    $query->{$method}($table.'.'.$field, $operator, $value);
                }
            }
        });
    }

    /**
     * Build the order by field indicated.
     * Format can be
     * e.g.
     *      &orderBy=name;id
     *      &orderBy=product.name;category.id
     *      &orderBy=product.name|custom_id;category.id
     *
     * @param $model
     * @param $orderBy
     * @param $sortedBy
     * @return mixed
     */
    private function buildOrderByFields($model, $orderBy, $sortedBy)
    {
        $expressions = explode(';', $orderBy);

        $table = $model->getModel()->getTable();

        foreach ($expressions as $expression) {
            $expr = explode('|', $expression);
            $relationAndField = explode('.', $expr[0]);
            $join = null;
            $order = null;

            // Check if the expression is a dotted notation
            // If not dotted, the code will assume that orderBy field is pertaining to the
            // format (name;id)
            if (count($relationAndField) === 1) {
                $order = $relationAndField[0];
            } else {
                $relationship = $relationAndField[0];
                $relatedTable = $model
                    ->getModel()
                    ->$relationship()
                    ->getRelated()
                    ->getTable();

                // This code assumes that the format given is either
                // (product.id;category.id) or
                // (product.name|custom_id;category.id)
                $join = [
                    'relation' => $relatedTable,
                    'field' => count($expr) === 1
                        ? $table.'_id'
                        : $expr[1]
                ];

                $order = $relatedTable.'.'.$relationAndField[1];
            }

            if ($join) {
                $model = $model->leftJoin($join['relation'], $join['field'], '=', $table.'.id');
            }

            $model = $model->orderBy($order, $sortedBy);
        }

        return $model;
    }

    /**
     * Get the corresponding field, relation, value, operator and condition based on the given
     * expression in the search query string
     *
     * @param $expression
     * @return array
     */
    private function getConditionExpression($expression)
    {
        $expr = [
            'field' => '',
            'relation' => null,
            'value' => '',
            'operator' => '=',
            'condition' => 'and'
        ];

        $units = explode('|', $expression);

        foreach ($units as $unit) {
            $breakdown = explode('=', $unit);
            switch ($breakdown[0]) {
                case 'optr':
                    if (!in_array(strtolower($breakdown[1]), ['>', '<', '<>', '=', 'like'])) {
                        continue;
                    }

                    $expr['operator'] = strtolower($breakdown[1]);

                    // Add wild card search on the string if current operator is like
                    // Even if the value is empty, it will still be outputed as %%
                    if ($expr['operator'] === 'like') {
                        $expr['value'] = '%'.$expr['value'].'%';
                    }
                    break;

                case 'cond':
                    if (!in_array(strtolower($breakdown[1]), ['and', 'or'])) {
                        continue;
                    }

                    $expr['condition'] = strtolower($breakdown[1]);
                    break;

                default:
                    $expr['field'] = $breakdown[0];

                    // If current value is %%, which indicates that the operator was written first in the
                    // search query string, insert the value in between the wild card
                    // If there are no value yet in the key 'value', append it
                    $expr['value'] = $expr['value'] === '%%'
                        ? '%'.$breakdown[1].'%'
                        : $breakdown[1];
                    break;
            }
        }

        $relation = explode('.', $expr['field']);

        if (count($relation) > 1) {
            $expr['relation'] = $relation[0];
            $expr['field'] = $relation[1];
        }

        $expr['value'] = Str::lower($expr['value']) === 'null'
            ? null
            : $expr['value'];

        return $expr;
    }

    private function buildEagerLoad($model, $with)
    {
        $relationships = explode(';', $with);

        $eagerLoaded = [];

        foreach ($relationships as $relationship) {
            $rel = explode('|', $relationship);

            if (count($rel) === 1) {
                $eagerLoaded[] = $rel[0];
            } else if ($rel[1] === 'withTrashed'){
                $eagerLoaded[$rel[0]] = function($query) {
                    $query->withTrashed();
                };
            }
        }

        return $model->with($eagerLoaded);
    }
}