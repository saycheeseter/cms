<?php

namespace Modules\Core\Repositories\Criteria;

use Modules\Core\Traits\FilterRebuild;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Core\Enums\Voided;
use App;

/**
 * Class ColumnFilterCriteria
 * @package namespace Modules\Core\Repositories\Criteria;
 */
class ColumnFilterCriteria implements CriteriaInterface
{
    use FilterRebuild;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $this->model = App::make($repository->model());

        $join = '';
        
        if($this->request->has('filters') && ! empty($this->request->input('filters'))) {
            foreach ($this->request->input('filters') as $filter) {
                $method = $join == 'or' ? 'orWhere' : 'where';
                $filter = (object) ($filter);

                $filter = $this->rebuild($filter);

                if (!$this->isSearchable($filter->column, $repository)) {
                    continue;
                }

                $filter->value = $filter->value ?? '';

                // Special condition for tables with soft delete property
                if (Str::contains($filter->column, 'deleted_at') && $this->isSoftDeletable()) {
                    $filter->operator = (int) $filter->value === Voided::YES ? '<>' : '=';
                    $filter->value = null;
                }

                $model = call_user_func_array([$model, $method], array(
                    $filter->column,
                    $filter->operator,
                    $filter->value
                ));

                $join = Str::lower($filter->join) ?? '';
            }
        }

        return $model;
    }

    protected function isSearchable($column, $repository)
    {
        return in_array(Str::lower($column), $repository->getFieldsSearchable());
    }

    protected function isSoftDeletable()
    {
        return method_exists($this->model, 'getDeletedAtColumn');
    }
}