<?php 

namespace Modules\Core\Repositories\Builder\Contracts;

interface CustomReportBuilderInterface 
{
    public function execute();
}