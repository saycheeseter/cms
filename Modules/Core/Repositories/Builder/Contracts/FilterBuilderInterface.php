<?php 

namespace Modules\Core\Repositories\Builder\Contracts;

interface FilterBuilderInterface 
{
    public function where();
    public function having();
}