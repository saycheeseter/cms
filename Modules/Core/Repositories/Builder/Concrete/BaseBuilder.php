<?php 

namespace Modules\Core\Repositories\Builder\Concrete;

use Modules\Core\Repositories\Builder\Contracts\FilterBuilderInterface;
use Modules\Core\Traits\FilterRebuild;
use Exception;

class BaseBuilder implements FilterBuilderInterface
{
    use FilterRebuild;

    protected $builder;
    protected $attributes;

    public function __construct($builder, $attributes)
    {
        if (!$builder instanceof \Illuminate\Database\Eloquent\Builder) {
            throw new Exception("Class ".get_class($builder)." must be an instance of Illuminate\\Database\\Eloquent\\Builder");
        }

        $this->builder = $builder;
        $this->attributes = $attributes;
    }

    public function where()
    {
        return true;
    }

    public function having()
    {
        return true;
    }
}