<?php

namespace Modules\Core\Repositories\Builder\Concrete;

use Illuminate\Database\Query\Builder;
use Modules\Core\Repositories\Builder\Contracts\FilterBuilderInterface;
use Modules\Core\Traits\FilterRebuild;
use Illuminate\Support\Str;

/**
 * Class CustomReportSearchBuilder
 * @package namespace Modules\Core\Repositories\Builder\Concrete;
 */
class CustomReportSearchBuilder implements FilterBuilderInterface
{   
    use FilterRebuild;

    protected $builder;
    protected $attributes;

    public function __construct(Builder $builder, $attributes)
    {
        $this->builder = $builder;
        $this->attributes = $attributes;
    }

    public function where()
    {
        $join = null;

        foreach ($this->attributes as $key => $value) {
            $curAttribute = $this->rebuild((object) ($value));
            $this->builder = $this->whereCondition($curAttribute, $curAttribute->column, $join);
        }

        return $this->builder;
    }
    
    private function whereCondition($curAttribute, $column, $overrideOperator = null)
    {
        if((Str::lower($curAttribute->join) == 'and' && $overrideOperator == null) || $overrideOperator == 'and') {
            $method = 'where';
        } else if((Str::lower($curAttribute->join) == 'or' && $overrideOperator == null) || $overrideOperator == 'or') {
            $method = 'orWhere';
        }

        return call_user_func_array(array($this->builder, $method), array(
            $column,
            $curAttribute->operator,
            $curAttribute->value
        ));
    }

    public function having()
    {
        return true;
    }
}
