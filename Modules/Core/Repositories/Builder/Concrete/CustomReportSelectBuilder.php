<?php

namespace Modules\Core\Repositories\Builder\Concrete;

use Modules\Core\Enums\TransactionType;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\Status;
use Modules\Core\Enums\UserType;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\CustomerPricingType;

use Lang;
use DB;
use Auth;
/**
 * Class CustomReportSelectBuilder
 * @package namespace Modules\Core\Repositories\Builder\Concrete;
 */
class CustomReportSelectBuilder
{   
    protected $builder;

    public function __construct($builder)
    {
        if (!$builder instanceof \Illuminate\Database\Eloquent\Builder) {
            throw new Exception("Class ".get_class($builder)." must be an instance of Illuminate\\Database\\Eloquent\\Builder");
        }

        $this->builder = $builder;
    }

    public function product($foreignkey, $alias = '')
    {   
        $table = 'product';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                    ->addSelect(
                        $this->columns([
                            'stock_no',
                            'name',
                            'supplier_sku',
                            'chinese_name',
                            'memo',
                            'senior',
                            'description'], $column)
                    )->addSelect(DB::raw(sprintf("CASE %s.status
                                WHEN %s THEN '%s'
                                WHEN %s THEN '%s'
                                WHEN %s THEN '%s'
                            END as %s_status", 
                            $table,
                            ProductStatus::INACTIVE,
                            Lang::get('core::label.inactive'),
                            ProductStatus::ACTIVE,
                            Lang::get('core::label.active'),
                            ProductStatus::DISCONTINUED,
                            Lang::get('core::label.discontinued'),
                            $column
                            ))
                    );

        $this->systemCode('product.brand_id', 'brand');
        $this->supplier('product.supplier_id');
        $this->category('product.category_id');
        $this->branch('product.created_from', 'product_location');

        return $this;
    }

    public function systemCode($foreignkey, $alias = '')
    {   
        $table = 'system_code';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns(['code', 'name', 'remarks'], $column)
                );
                
        return $this;
    }

    public function reference($table, $foreignkey, $alias = '')
    {
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns(['sheet_number'], $column)
                );
                
        return $this;
    }

    public function role($foreignkey, $alias = '')
    {   
        $table = 'role';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns(['name'], $column)
                );
                
        return $this;
    }

    public function branch($foreignkey, $alias = '')
    {   
        $table = 'branch';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns([
                        'name',
                        'code',
                        'address',
                        'contact',
                        'business_name'
                    ], $column)
                );

        return $this;
    }

    public function location($foreignkey, $alias = '')
    {   
        $table = 'branch_detail';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }
        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns([
                        'name',
                        'code',
                        'address',
                        'contact',
                        'business_name'
                    ], $column)
                );
        return $this;
    }

    public function category($foreignkey, $alias = '')
    {   
        $table = 'category';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns(['name', 'code'], $column)
                );           

        return $this;
    }

    public function supplier($foreignkey, $alias = '')
    {   
        $table = 'supplier';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
            ->addSelect(
                $this->columns([
                    'full_name',
                    'code',
                    'chinese_name',
                    'owner_name',
                    'contact',
                    'address',
                    'landline',
                    'telephone',
                    'fax',
                    'mobile',
                    'email',
                    'website',
                    'term',
                    'contact_person',
                    'memo'
                ], $column)
            );

        $this->status($column);

        return $this;
    }

    public function customer($foreignkey, $alias = '')
    {   
        $table = 'customer';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }
        
        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
            ->addSelect(
                $this->columns([
                    'name',
                    'code',
                    'memo',
                    'credit_limit',
                    'website',
                    'term'
                ], $column)
            );

        $this->status($column);
        $this->customerPricingType($column);
        $this->customerDetail(sprintf('%s.id', $column), sprintf('%s_detail', $column));

        return $this;
    }

    public function user($foreignkey, $alias = '')
    {   
        $table = 'user';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
            ->addSelect(
                $this->columns([
                    'code',
                    'username',
                    'full_name',
                    'position',
                    'address',
                    'telephone',
                    'mobile',
                    'email',
                    'memo',
                    'role_id'
                ], $column)
            )->addSelect(
                DB::raw(sprintf("CASE %s.type
                        WHEN %s THEN 'superadmin'
                        WHEN %s THEN 'default'
                    END as %s_type", 
                    $column,
                    UserType::SUPERADMIN,
                    UserType::DEFAULT,
                    $column
                ))
            );

        $this->status($column);

        return $this;
    }

    public function transactionType($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.transaction_type
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                        END as %s_transaction_type", 
                        $table,
                        TransactionType::DIRECT,
                        Lang::get('core::label.direct'),
                        TransactionType::FROM_INVOICE,
                        Lang::get('core::label.from.invoice'),
                        $table
            ))
        );

        return $this;
    }

    public function transactionStatus($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.transaction_status
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                        END as %s_transaction_status", 
                        $table,
                        TransactionStatus::COMPLETE,
                        Lang::get('core::label.complete'),
                        TransactionStatus::INCOMPLETE,
                        Lang::get('core::label.incomplete'),
                        TransactionStatus::EXCESS,
                        Lang::get('core::label.excess'),
                        TransactionStatus::NO_DELIVERY,
                        Lang::get('core::label.no.delivery'),
                        $table
            ))
        );

        return $this;
    }

    public function approvalStatus($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.approval_status
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                            WHEN %s THEN '%s'
                        END as %s_approval_status", 
                        $table,
                        ApprovalStatus::DRAFT,
                        Lang::get('core::label.draft'),
                        ApprovalStatus::FOR_APPROVAL,
                        Lang::get('core::label.for.approval'),
                        ApprovalStatus::APPROVED,
                        Lang::get('core::label.approved'),
                        ApprovalStatus::DECLINED,
                        Lang::get('core::label.declined'),
                        $table
            ))
        );

        return $this;
    }

    public function customerType($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.customer_type
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                END as %s_customer_type", 
                $table,
                Customer::WALK_IN,
                Lang::get('core::label.walk.in.customer'),
                Customer::REGULAR,
                Lang::get('core::label.regular.customer'),
                $table
            ))
        );

        return $this;
    }

    public function status($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.status
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                END as %s_status", 
                $table,
                Status::INACTIVE,
                Lang::get('core::label.inactive'),
                Status::ACTIVE,
                Lang::get('core::label.active'),
                $table
            ))
        );

    }

    public function customerDetail($foreignkey, $alias = '')
    {   
        $table = 'customer_detail';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.head_id', $column))
            ->addSelect(
                $this->columns([
                    'name',
                    'address',
                    'chinese_name',
                    'owner_name',
                    'contact',
                    'landline',
                    'fax',
                    'mobile',
                    'email',
                    'contact_person',
                    'tin'
                ], $column)
            );

        return $this;
    }

    public function inventory($foreignkey, $alias = '')
    {
        $table = 'product_branch_summary';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, function($join) use ($foreignkey, $column){
            $join->on($foreignkey, sprintf('%s.product_id', $column))
                  ->where(sprintf('%s.branch_id', $column), '=', Auth::branch()->id);
        })->addSelect(
            DB::raw(sprintf('SUM(COALESCE(%s.qty, 0) ) AS %s_qty', $column, $column)),
            DB::raw(sprintf('SUM(COALESCE(%s.reserved_qty, 0) ) AS %s_reserved_qty', $column, $column)),
            DB::raw(sprintf('SUM(COALESCE(%s.requested_qty, 0) ) AS %s_requested_qty', $column, $column))
        );

        return $this;
    }

    public function sumTransactionNumerics($table)
    {
        
        $this->builder->addSelect(
            DB::raw(sprintf('SUM(COALESCE(%s.total_amount, 0) ) AS %s_total_amount', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.unit_qty, 0) ) AS %s_detail_unit_qty', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.qty, 0) ) AS %s_detail_qty', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.price, 0) ) AS %s_detail_price', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.oprice, 0) ) AS %s_detail_oprice', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.discount1, 0) ) AS %s_detail_discount1', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.discount2, 0) ) AS %s_detail_discount2', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.discount3, 0) ) AS %s_detail_discount3', $table, $table)),
            DB::raw(sprintf('SUM(COALESCE(%s_detail.discount4, 0) ) AS %s_detail_discount4', $table, $table))
        );

        return $this;
    }

    public function tax($foreignkey, $alias = '')
    {   
        $table = 'tax';
        $column = $table;
        if($alias != ''){
            $table = sprintf('%s as %s', $table, $alias);
            $column = $alias;
        }

        $this->builder->leftJoin($table, $foreignkey, sprintf('%s.id', $column))
                ->addSelect(
                    $this->columns(['name', 'percentage'], $column)
                );
        
        $this->yesNo($column, 'is_inclusive');

        return $this;
    }

    public function yesNo($table, $column)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.%s
                    WHEN 0 THEN '%s'
                    WHEN 1 THEN '%s'
                END as %s_%s", 
                $table,
                $column,
                Lang::get('core::label.no'),
                Lang::get('core::label.yes'),
                $table,
                $column
            ))
        );
    }

    public function customerPricingType($table)
    {
        $this->builder->addSelect(
            DB::raw(sprintf("CASE %s.pricing_type
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                    WHEN %s THEN '%s'
                END as %s_pricing_type", 
                $table,
                CustomerPricingType::HISTORICAL,
                Lang::get('core::label.historical.record'),
                CustomerPricingType::WHOLESALE,
                Lang::get('core::label.wholesale.price'),
                CustomerPricingType::RETAIL,
                Lang::get('core::label.retail.price'),
                CustomerPricingType::PERCENT_PURCHASE,
                Lang::get('core::label.percent.purchase'),
                CustomerPricingType::PERCENT_WHOLESALE,
                Lang::get('core::label.percent.wholesale'),
                CustomerPricingType::PRICE_A,
                Lang::get('core::label.percent.a'),
                CustomerPricingType::PRICE_B,
                Lang::get('core::label.percent.b'),
                CustomerPricingType::PRICE_C,
                Lang::get('core::label.percent.c'),
                CustomerPricingType::PRICE_D,
                Lang::get('core::label.percent.d'),
                CustomerPricingType::PRICE_E,
                Lang::get('core::label.percent.e'),
                $table
            ))
        );
    }

    public function columns($fields, $alias)
    {
        $columns = [];

        foreach ($fields as $field) {
            $columns[] = sprintf('%s.%s as %s_%s', $alias, $field, $alias, $field); 
        }

        return $columns;
    }
}