<?php

namespace Modules\Core\Repositories\Builder\Concrete;

use Modules\Core\Entities\Contracts\CustomizableReport;
use Modules\Core\Repositories\Builder\Concrete\CustomReportSearchBuilder;
use  Modules\Core\Repositories\Builder\Contracts\CustomReportBuilderInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

use App;
use Schema;
use DB;

class CustomReportBuilder  implements CustomReportBuilderInterface 
{
    private $model;
    private $query;
    private $filters;
    private $format;
    private $columns = array();
    private $originalColumns = array();
    private $decimals = array();
    private $totals = array();
    private $table;

    public function __construct(CustomizableReport $model, $format, $filters = [])
    {   
        $this->model = $model;
        $this->format = $format;
        $this->filters = $filters;
        $this->totals = $format->total;
        $this->query = $model->getQuery();
        $this->table = $model->getTable();
    }

    /**
     * execute necessary builds for custom report
     * @return query builder instance
     */
    public function execute()
    {
        $this->buildReportJoins()
            ->buildReportColumns()
            ->buildReportFilters()
            ->buildReportGroupBy()
            ->buildReportOrderBy();

        return $this->query;
    }

    /**
     * build report joins
     * @return $this
     */
    private function buildReportJoins()
    {
        $this->generateReportJoins($this->model, $this->table);

        return $this;
    }

    /**
     * build report columns
     * @return $this
     */
    private function buildReportColumns()
    {
        $this->generateReportColumns();

        $this->query = $this->query->select($this->columns);

        //building of with formula and decimal columns after the first select
        //this selects will be added using addSelect query builder method
        $this->buildReportColumnsWithFormula();
        $this->buildReportColumnsWithDecimal();
        
        return $this;
    }

    /**
     * build columns based on selected from format on report builder table
     */
    private function generateReportColumns()
    {   
        foreach ($this->format->columns as $key => $column) {
            if(is_null($column->formula)) {
                (!$this->isNumericColumn($column->column_name))
                    ? $this->columns[] = sprintf('%s.%s AS %s', $column->table_alias, $column->column_name, $key)
                    : $this->decimals[$key] = $column;
            }
        }

        foreach ($this->columns as $key => $column) {
            $exploded = explode(" AS ", $column);
            $this->originalColumns[$exploded[1]] =  $exploded[0];
        }

        return $this;
    }

    /**
     * build report columns with decimals
     */
    private function buildReportColumnsWithDecimal()
    {
        foreach ($this->decimals as $key => $decimal) {
            $select = (in_array($key, $this->totals))
                    ? sprintf('SUM(%s.%s) AS %s', $decimal->table_alias, $decimal->column_name, $key)
                    : sprintf('%s.%s AS %s', $decimal->table_alias, $decimal->column_name, $key);

            $this->originalColumns[$key] =  sprintf('%s.%s', $decimal->table_alias, $decimal->column_name);
            $this->query = $this->query->addSelect(DB::raw($select));
        }
    }

    /**
     * build report columns with formula
     */
    private function buildReportColumnsWithFormula()
    {
        foreach ($this->format->columns as $key => $value) {
            if(!is_null($value->formula)) {

                $select = (in_array($key, $this->totals))
                    ? sprintf('SUM(%s) as %s', $value->formula->computation, $key)
                    : sprintf('%s as %s', $value->formula->computation, $key);

                $this->query = $this->query->addSelect(DB::raw($select));
            }
        }
    }

    /**
     * Recursive generator of table joins.
     * @param  $parent parent class of relationship
     * @param  string  $alias current alias
     * @param  $relation relation of parent to child
     * @param  $reference     reference table
     * @param  string  $previousAlias previous alias
     * @param  boolean $break check if there is a key of break on the relationship
     */
    private function generateReportJoins($parent, $alias = '', $relation = null, $reference = null, $previousAlias = '', $break = false, $excluded = [], $joins = [])
    {
        if (!is_null($relation) && !is_null($reference)) {
            $this->appendTableJoins($reference, $alias, $relation, $parent, $previousAlias, $joins);
        }

        if(method_exists($parent, 'relationships') && (!$break)) {
            foreach ($parent->relationships() as $key => $value) {
                if(!in_array($key, $excluded)) {
                    $newAlias = sprintf('%s_%s', $alias, $key);
                    $child = App::make($value['class']);
                    $relation = $value['relation'];
                    $break = $value['break' ] ?? false;
                    $joins = $value['joins' ] ?? [];

                    $this->generateReportJoins($child, $newAlias, $relation, $parent, $alias, $break, $value['exclusions'] ?? [], $joins);
                }
            }
        }
    }


    /**
     * append alias and tables per join
     * @param  $parent 
     * @param  $alias
     * @param  $relation
     * @param  $child
     * @param  $previousAlias
     */
    private function appendTableJoins($parent, $alias = '', $relation = null, $child = null, $previousAlias = '', $joins = [])
    {
        $keys = $this->getRelationshipKeys($parent->$relation());
        
        if(is_null($keys['pivot'])) {
            if(count($joins) == 0) {
                $this->query = $this->query->leftJoin(
                    sprintf('%s as %s', $child->getTable(), $alias), 
                    sprintf('%s.%s', $previousAlias, $keys['foreign']),
                    sprintf('%s.%s', $alias, $keys['local'])
                );
            } else {
                $this->query = $this->query->leftJoin(
                    sprintf('%s as %s', $child->getTable(), $alias), 
                    function($join) use ($joins, $alias, $previousAlias) {
                        foreach ($joins as $value) {
                            if(isset($value['table'])) {
                                $previousAlias = $value['table'];
                            }

                            $join->on(
                                sprintf('%s.%s', $previousAlias, $value['local']), 
                                '=', 
                                sprintf('%s.%s', $alias, $value['foreign'])
                            );
                        }
                    }
                );
            }
        } else {
            list($pivotTable, $pivotColumn) = explode('.', $keys['pivot']);

            $aliasedPivotTable = sprintf("%s_%s", $alias, $pivotTable);
            
            $this->query = $this->query->leftJoin(
                sprintf('%s as %s', $pivotTable, $aliasedPivotTable), 
                sprintf('%s.%s', $previousAlias, $keys['foreign']),
                sprintf('%s.%s', $aliasedPivotTable, $keys['local'])
            )->leftJoin(
                sprintf('%s as %s', $child->getTable(), $alias), 
                sprintf('%s.%s', $alias, $keys['foreign']),
                sprintf('%s.%s', $aliasedPivotTable, $pivotColumn)
            );
        }
    }

    /**
     * get keys of relationships
     * @param string $relation
     * @return array
     */
    private function getRelationshipKeys($relation)
    {   
        $pivot = null;

        if($relation instanceof BelongsTo) {
            $foreign =  $relation->getForeignKey();
            $local = $relation->getOwnerKey();
        } else if($relation instanceof BelongsToMany){
            $foreign = explode('.', $relation->getQualifiedParentKeyName())[1];
            $local = explode('.', $relation->getQualifiedForeignPivotKeyName())[1];
            $pivot = $relation->getQualifiedRelatedPivotKeyName();
        } else {
            $foreign =  explode('.', $relation->getQualifiedParentKeyName())[1];
            $local = $relation->getForeignKeyName();
        }

        return [
            'foreign' => $foreign,
            'local' => $local,
            'pivot' => $pivot
        ];
    }

    /**
     * apply order by on query
     * @param  object $format
     * @param  instance $query
     * @return $query
     */
    private function buildReportOrderBy()
    {   
        foreach ($this->format->order_by as $key => $value) {
            $this->query = $this->query->orderBy($this->originalColumns[$value->key], $value->order);
        }

        return $this;
    }

    /**
     * apply group by on query
     * @param  object $format
     * @param  instance $query
     * @return $query
     */
    private function buildReportGroupBy()
    {   
        foreach ($this->originalColumns as $key => $value) {
            if(!in_array($key, $this->totals)) {
                $this->query = $this->query->groupBy($value);
            }
        }

        foreach ($this->format->columns as $key => $value) {
            if(!is_null($value->formula) && !in_array($key, $this->totals)) {
                $columns = explode(",", $value->formula->columns);
                for ($x=0; $x < count($columns); $x++) { 
                    $this->query = $this->query->groupBy($columns[$x]);
                }
            }
        }
        

        return $this;
    }
    
    /**
     * apply filters for custom reports
     * @param  array $filters
     * @param  instance $query
     * @return $query
     */
    private function buildReportFilters()
    {   
        if(count($this->filters) > 0) {
            $searchBuilder = new CustomReportSearchBuilder($this->query, $this->filters);
            $this->query = $searchBuilder->where();
        }

        return $this;
    }

    /**
     * checker if column name is numeric
     * @param  string  $haystack 
     * @return boolean 
     */
    private function isNumericColumn($haystack)
    {   
        $bool = false;

        $needles = ['amount', 'qty', 'price', 'discount1', 'discount2', 'discount3', 'discount4', 'discount5'];

        foreach ($needles as $key => $needle) {
            $length = strlen($needle);
            if($length === 0 || (substr($haystack, -$length) === $needle)) {
                $bool = true;
                break;
            }
        }

        return $bool;
    }
}