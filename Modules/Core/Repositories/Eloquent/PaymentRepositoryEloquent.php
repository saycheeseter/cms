<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PaymentRepository;
use Modules\Core\Entities\Payment;

use DB;

class PaymentRepositoryEloquent extends FinancialFlowRepositoryEloquent implements PaymentRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'payment.deleted_at',
        'created_for',
        'supplier.full_name'
    ];

    public function model()
    {
        return Payment::class;
    }

    public function search($limit = null)
    {
        $this->withTrashed()
            ->with([
                'details',
                'references',
                'from',
                'creator',
                'for',
                'modifier',
                'auditor',
                'supplier'
            ]);

        $this->model = $this->model->join('supplier', 'payment.supplier_id', '=', 'supplier.id')->select(DB::raw('payment.*'));

        return $this->paginate($limit);
    }
}