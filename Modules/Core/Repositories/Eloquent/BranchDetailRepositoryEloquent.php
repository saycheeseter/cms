<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\BranchDetailRepository;
use Modules\Core\Entities\BranchDetail;

/**
 * Class BranchDetailRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BranchDetailRepositoryEloquent extends RepositoryEloquent implements BranchDetailRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'business_name'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchDetail::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
