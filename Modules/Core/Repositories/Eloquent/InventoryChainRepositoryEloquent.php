<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Enums\ApprovalStatus;

/**
 * Class InventoryChainRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
abstract class InventoryChainRepositoryEloquent extends RepositoryEloquent
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_from',
        'created_for',
        'for_location',
        'requested_by'
    ];

    /**
     * Find transaction by the given reference number
     *
     * @param  string $reference
     * @param  int $id
     * @return Model
     */
    public function findByReference($reference, $id = 0)
    {
        $builder = $this->model->where('sheet_number', $reference);

        if ($id !== 0) {
            $builder = $builder->where('id', '<>', $id);
        }

        $model = $builder->get()->first();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Get all detail transactions by transction filter and product_id
     *
     * @param  array|int $ids
     * @param  int $limit
     * @return Model
     */
    public function findTransactionsByProduct($ids, $limit = null)
    {
        $that = $this;
        $model = $this->model->details()->getRelated();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $base = $this->model->getTable();
        $related = $this->model->details()->getRelated()->getTable();

        $results = $model->with(['transaction', 'transaction.creator'])
            ->select($related.'.*')
            ->whereIn('product_id', $ids)
            ->whereHas('transaction', function ($query) use ($that) {
                $query = $that->getCriteria()[0]->apply($query, $that);
                $query->where('approval_status', '=', ApprovalStatus::APPROVED);
            })
            ->join($base, $base.'.id', $related.'.transaction_id')
            ->orderBy($base.'.transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();

        return $results;
    }

    public function findBySheetNumber($sheetNumber)
    {
        $result = $this->model->where('sheet_number', $sheetNumber)->first();

        $this->resetModel();

        return $this->parserResult($result);
    }

    public function findByTransactionDateRangeAndStatuses($dateFrom, $dateTo, $statuses = [], $perPage = 10)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model
            ->with(['details'])
            ->where('transaction_date', '>=', $dateFrom)
            ->where('transaction_date', '<=', $dateTo);

        if ($statuses) {
            $model = $model->whereIn('approval_status', $statuses);
        }

        $results = $model->paginate($perPage);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function findBySheetNumberRangeAndStatuses($sheets, $statuses = [], $perPage = 10)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model->with(['details']);

        if ($sheets) {
            $model = $model->whereIn('sheet_number', $sheets);
        }

        if ($statuses) {
            $model = $model->whereIn('approval_status', $statuses);
        }

        $results = $model->paginate($perPage);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function whereApproved()
    {
        $this->model = $this->model->where('approval_status', ApprovalStatus::APPROVED);

        return $this;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
