<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\UserColumnSettings;
use Modules\Core\Repositories\Contracts\UserColumnSettingsRepository;
use Auth;

/**
 * Class UserColumnSettingsRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class UserColumnSettingsRepositoryEloquent extends RepositoryEloquent implements UserColumnSettingsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserColumnSettings::class;
    }

    public function getByModule($module)
    {
        $data = [
            'user_id' => Auth::user()->id,
            'module_id' => $module
        ];

        $model = $this->findWhere($data)->first();

        return $model->settings ?? (object)[];
    }
}