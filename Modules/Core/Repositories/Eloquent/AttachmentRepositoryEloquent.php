<?php

namespace Modules\Core\Repositories\Eloquent;

use Bnb\Laravel\Attachments\Attachment;
use Modules\Core\Entities\Product;
use Modules\Core\Repositories\Contracts\AttachmentRepository;

class AttachmentRepositoryEloquent extends RepositoryEloquent implements AttachmentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Attachment::class;
    }

    public function getByType($type, $id)
    {
        switch ($type) {
            case 'product':
                $type = $this->getMorphMapKey(Product::class);
                break;

            default:
                $type = null;
                break;
        }

        if (is_null($type)) {
            return collect([]);
        }

        $result = $this->model
            ->where('model_type', $type)
            ->where('model_id', $id)
            ->get();

        $this->resetModel();

        return $result;
    }
}