<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PaymentMethodRepository;
use Modules\Core\Repositories\Criteria\PaymentMethodOnlyCriteria;
use Modules\Core\Entities\PaymentMethod;
use Modules\Core\Enums\PaymentMethod as PaymentMethodEnum;

/**
 * Class PaymentMethodRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PaymentMethodRepositoryEloquent extends SystemCodeRepositoryEloquent implements PaymentMethodRepository
{
    public function model()
    {
        return PaymentMethod::class;
    }

    public function getDefaultMethods()
    {
        $builder = $this->model->whereIn('id', PaymentMethodEnum::default());

        $this->resetModel();

        return $builder->get();
    }

    public function getPaymentMethodWalkIn()
    {
        $builder = $this->model->whereIn('id', PaymentMethodEnum::walkIn());
        
        $this->resetModel();

        return $builder->get();
    }
}
