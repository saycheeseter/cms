<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ReportBuilderRepository;
use Modules\Core\Entities\ReportBuilder;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

/**
 * Class ReportBuilderRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ReportBuilderRepositoryEloquent extends RepositoryEloquent implements ReportBuilderRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'group_id'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReportBuilder::class;
    }

    /**
     * Find transaction by the given slug
     *
     * @param  string $slug
     * @return Model
     */
    public function findBySlug($slug)
    {
        $builder = $this->model->where('slug', $slug);

        $model = $builder->get()->first();

        $this->resetModel();

        return $this->parserResult($model);
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
