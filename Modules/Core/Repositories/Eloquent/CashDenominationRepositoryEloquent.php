<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\CashDenominationRepository;
use Modules\Core\Entities\CashDenomination;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class CashDenominationRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class CashDenominationRepositoryEloquent extends RepositoryEloquent implements CashDenominationRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'terminal_number',
        'created_by',
        'updated_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CashDenomination::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
