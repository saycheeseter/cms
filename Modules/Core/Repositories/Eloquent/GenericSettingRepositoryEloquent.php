<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\GenericSettingRepository;
use Modules\Core\Repositories\Criteria\SettingsOwnBranchCriteria;
use Modules\Core\Entities\GenericSetting;

/**
 * Class GenericSettingRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class GenericSettingRepositoryEloquent extends RepositoryEloquent implements GenericSettingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GenericSetting::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        
    }
}
