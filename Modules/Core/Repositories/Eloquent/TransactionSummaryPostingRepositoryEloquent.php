<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Repositories\Contracts\TransactionSummaryPostingRepository;

class TransactionSummaryPostingRepositoryEloquent extends RepositoryEloquent implements TransactionSummaryPostingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TransactionSummaryPosting::class;
    }
}