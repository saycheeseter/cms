<?php

namespace Modules\Core\Repositories\Eloquent;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use Modules\Core\Traits\FilterRebuild;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Modules\Core\Repositories\Builder\Contracts\CustomReportBuilderInterface;
use Modules\Core\Traits\HasMorphMap;
use Modules\Core\Entities\Base;
use Closure;
use App;
use DB;

abstract class RepositoryEloquent extends BaseRepository
{
    use HasMorphMap,
        FilterRebuild;
    
    protected $constraints = [];
    protected $hasManyRelationships = [];

    /**
     * Find data by id (Override findOrFail)
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Find a model by its primary key or throw an exception.
     *
     * @param  mixed  $id
     * @param  array  $columns
     * @return mixed
     */
    public function findOrFail($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->findOrFail($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Overrides default with behaviour to add eager load constraints
     * 
     * @param  array $relations
     * @return $this;
     */
    public function with($relations)
    {   
        // Custom relationship with constraint
        $with = array();
        
        foreach ($relations as $key => $value) {
            $relation = is_numeric($key) ? $value : $key;

            // Check if value is a closure or a normal string value
            // Check if value is a custom relationship constraint
            if (!in_array($relation, $this->constraints)) {
                if ($value instanceof Closure) {
                    $with[$relation] = $value;
                } else {
                    $with[] = $value;
                }
            } else {
                $closure = $this->relationshipConstraints($relation, $value);
                $with = array_merge($with, $closure);
            }
        }

        $this->model = $this->model->with($with);

        return $this;
    }

    /**
     * Find multiple models by their primary keys.
     *
     * @param array $ids
     * @param array $columns
     *
     * @return mixed
     */
    public function findMany($ids, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->findMany($ids, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }
    
    /**
     * Get soft deleted models
     * @return $this
     */
    public function withTrashed()
    {
        $this->model = $this->model->withTrashed();

        return $this;
    }

    /**
     * Custom relationship constraint
     * 
     * @param  string $constraint
     * @param  array|string $value     
     * @return array            
     */
    protected function relationshipConstraints($constraint, $value)
    {
        return array();
    }

    /**
     * Fetch custom report data
     *
     * @param $format
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getCustomReportData($format, $filters = [])
    {   
        $builder = App::makeWith(CustomReportBuilderInterface::class, [
            'model'   => $this->model,
            'format'  => $format,
            'filters' => $filters
        ]);
        
        $request = App::make('request');

        if(!($request->has('per_page') && $request->has('page')) ) {
            return $builder->execute()->get();
        } else {
            return $this->createPaginationFromQuery($builder->execute());
        }
    }

    /**
     * Generate a LengthAwarePaginator class based from the count of Builder class
     *
     * @param Builder $query
     * @return LengthAwarePaginator
     */
    protected function createPaginationFromQuery(Builder $query)
    {
        $request = App::make('request');

        $perPage = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        
        $count = $query->getCountForPagination();

        $data = $query->forPage($page, $perPage)->get();

        return new LengthAwarePaginator($data, $count, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }

    /**
     * Hydrate items in the paginator collection to the specified class
     *
     * @param LengthAwarePaginator $paginator
     * @param Base $class
     * @return LengthAwarePaginator
     */
    protected function hydratePaginationItems(LengthAwarePaginator $paginator, Base $class)
    {
        $items = [];

        foreach ($paginator->items() as $key => $item) {
            $items[] = (get_class($item) === 'stdClass')
                ? (array) $item
                : $item->toArray();
        }

        return $paginator->setCollection($class::hydrate($items)); 
    }

    /**
     * Set conditions on the given builder based on the filters passed, alias and
     * the filter that are only needed to set on the builder class
     *
     * @param Builder $builder
     * @param array $filters
     * @param string $aliases
     * @param array $only
     * @return mixed
     * @internal param array|string $alias
     */
    protected function setConditionsOnBuilder(Builder $builder, $filters = [], $aliases = '', $only = [], $callbacks = null)
    {
        if(is_null($filters)) {
            return $builder;
        }

        $only = array_wrap($only);

        $that = $this;

        $builder->where(function ($query) use ($filters, $aliases, $only, $callbacks, $that) {
            $join = '';

            foreach ($filters as $key => $filter) {
                if (! in_array($filter['column'], $only)) {
                    continue;
                }

                $alias = is_array($aliases)
                    ? $aliases[$filter['column']] ?? $filter['column']
                    : $aliases;

                $alias = !empty($alias) ? $alias.'.' : '';

                $filter = $this->rebuild((object) ($filter));

                $method = $join == 'or' ? 'orWhere' : 'where';

                if (!is_null($callbacks) && isset($callbacks[$filter->column])) {
                    $callbacks[$filter->column]($query, $filter, $method, $alias);
                } else {
                    $query->$method($alias.$filter->column, $filter->operator, $filter->value);
                }

                $join = Str::lower($filter->join) ?? '';
            }
        });

        return $builder;
    }

    protected function createSubQuery($builder)
    {
        return DB::table(DB::raw("(".$builder->toSql().") as inner_query"))
            ->mergeBindings($builder)
            ->select('*');
    }

    protected function getFiltersByKey($filters, $key)
    {
        if(is_null($filters)) {
            return [];
        }

        return array_where($filters, function($item, $index) use ($key) {
            return $item['column'] === $key;
        });
    }

    protected function setFieldSearchable($fields)
    {
        $fields = array_wrap($fields);
        
        $this->fieldSearchable = array_merge($this->fieldSearchable, $fields);
        
        return $this;
    }
}