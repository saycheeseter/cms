<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Core\Entities\MemberRate;

/**
 * Class MemberRateRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class MemberRateRepositoryEloquent extends RepositoryEloquent implements MemberRateRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'memo',
        'updated_at',
        'deleted_at'
    ];

    public function model()
    {
        return MemberRate::class; 
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}