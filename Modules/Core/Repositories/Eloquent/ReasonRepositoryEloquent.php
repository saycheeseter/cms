<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ReasonRepository;
use Modules\Core\Entities\Reason;

/**
 * Class ReasonRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ReasonRepositoryEloquent extends SystemCodeRepositoryEloquent implements ReasonRepository
{
    public function model()
    {
        return Reason::class;
    }
}
