<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\SystemCodeTypeRepository;
use Modules\Core\Entities\SystemCodeType;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class SystemCodeTypeRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SystemCodeTypeRepositoryEloquent extends RepositoryEloquent implements SystemCodeTypeRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SystemCodeType::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
