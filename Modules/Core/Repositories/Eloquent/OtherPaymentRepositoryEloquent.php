<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\OtherPaymentRepository;
use Modules\Core\Entities\OtherPayment;
use Carbon\Carbon;
use DB;

/**
 * Class OtherPaymentRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class OtherPaymentRepositoryEloquent extends OtherFinancialFlowRepositoryEloquent implements OtherPaymentRepository
{
    public function model()
    {
        return OtherPayment::class; 
    }
}