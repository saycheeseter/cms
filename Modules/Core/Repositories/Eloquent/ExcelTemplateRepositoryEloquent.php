<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ExcelTemplateRepository;
use Modules\Core\Entities\ExcelTemplate;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

/**
 * Class ExcelTemplateRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ExcelTemplateRepositoryEloquent extends RepositoryEloquent implements ExcelTemplateRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'module',
        'name'
    ];

    /**
     * Find template by given module id
     *
     * @param  int $id
     * @return Model
     */
    public function findByModule($id = 0)
    {
        $model = $this->model->where('module', $id)->get();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExcelTemplate::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
