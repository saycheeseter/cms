<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Enums\Customer;
use Modules\Core\Repositories\Contracts\SalesReturnInboundRepository;
use Modules\Core\Entities\SalesReturnInbound;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\Manageable;

/**
 * Class DamageRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SalesReturnInboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements SalesReturnInboundRepository
{
    public function model()
    {
        return SalesReturnInbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['salesman_id', 'customer_type', 'customer_id', 'customer_detail_id']);
    }

    public function transactionsWithBalance($branchId, $customer, $customerType, $limit = null)
    {
        $builder = $this
            ->model
            ->with([
                'customer',
                'salesman',
                'for',
                'creator',
                'auditor',
                'from',
                'requested',
                'location',
                'reference'
            ])
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('customer_type', $customerType)
            ->where('remaining_amount', '>', 0);

        if(!setting('collection.display.transactions.of.other.branches')) {
            $builder = $builder->where('created_for', $branchId);
        }

        if($customerType == Customer::WALK_IN) {
            $builder = $builder->where('customer_walk_in_name', $customer);
        } else if($customerType == Customer::REGULAR) {
            $builder = $builder->where('customer_id', $customer);
        }

        $results = $builder->paginate($limit);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function getWalkInCustomers()
    {
        $result = $this
            ->model
            ->where('customer_type', Customer::WALK_IN)
            ->where('remaining_amount', '>', 0)
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->get();

        $this->resetModel();

        return $this->parserResult($result);
    }
}
