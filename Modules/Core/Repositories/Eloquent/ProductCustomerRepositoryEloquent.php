<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\ProductCustomerRepository;
use Modules\Core\Entities\ProductCustomer;

use App;

/**
 * Class ProductCustomerRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductCustomerRepositoryEloquent extends RepositoryEloquent implements ProductCustomerRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer.name',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductCustomer::class;
    }

    /**
     * Search data based on products and apply filter criteria implementation
     * 
     * @return Collection
     */
    public function search($limit = null)
    {
        $this->model = $this->model
            ->with([
                'product',
                'unit',
                'customer',
            ])
            ->select('product_customer.*')
            ->join('customer', 'product_customer.customer_id', '=', 'customer.id');

        if (app('request')->has('products')) {
            $this->model = $this->model->whereIn('product_id', app('request')->get('products'));
        }

        return $this->paginate($limit);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
