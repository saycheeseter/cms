<?php

namespace Modules\Core\Repositories\Eloquent;

use DB;
use Modules\Core\Entities\Hydration\PosDiscountedSummaryReport;
use Modules\Core\Entities\Hydration\PosSalesTerminalReport;
use Modules\Core\Entities\Hydration\SeniorTransactionReport;
use Modules\Core\Entities\PosSales;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\Voided;
use Modules\Core\Repositories\Contracts\PosSalesRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class PosSalesRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PosSalesRepositoryEloquent extends RepositoryEloquent implements PosSalesRepository
{
    protected $fieldSearchable = [
        'created_for',
        'transaction_date'
    ];

    public function model()
    {
        return PosSales::class;
    }

    public function getTerminalReport($filters = [], $terminals = [])
    {
        $columns = [
            DB::raw('CAST(transaction_date AS DATE) AS transaction_date'),
        ];

        if (!empty($terminals)) {
            foreach($terminals as $terminal) {
                $columns[] = DB::raw('SUM(CASE WHEN terminal_number = ' . $terminal . ' THEN total_amount ELSE 0 END) AS terminal_' . $terminal);
            }
        }

        $builder = $this->model
            ->getQuery()
            ->where('voided', Voided::NO)
            ->select($columns)
            ->where('voided', Voided::NO)
            ->groupBy(
                DB::raw('CAST(transaction_date AS DATE) WITH ROLLUP')
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            'pos_sales',
            [
                'created_for',
                'transaction_date',
            ]
        );

        if (!empty($terminals)) {
            $builder->whereIn('terminal_number', $terminals);
        }

        $builder = $this->createSubQuery($builder)->orderBy('transaction_date', 'DESC');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new PosSalesTerminalReport());

        $this->resetModel();

        return $paginator;
    }

    public function getDiscountedSummaryReport($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('pos_sales_detail', 'pos_sales_detail.transaction_id', '=', 'pos_sales.id')
            ->leftJoin('user', 'user.id', '=', 'pos_sales.salesman_id')
            ->leftJoin('product', 'pos_sales_detail.product_id', '=', 'product.id')
            ->leftJoin('product_barcode', 'product_barcode.product_id', '=', 'product.id')
            ->leftJoin('category', 'category.id', '=', 'product.category_id')
            ->select(
                'pos_sales.terminal_number',
                'pos_sales.transaction_date',
                'pos_sales.or_number',
                'user.full_name as cashier_name',
                'category.name as category_name',
                DB::raw('dbo.GROUP_CONCAT(product_barcode.code) as product_barcode'),
                'product.name as product_name',
                DB::raw('COALESCE(pos_sales_detail.qty * pos_sales_detail.unit_qty, 0) as total_qty'),
                'pos_sales_detail.oprice',
                'pos_sales_detail.price',
                DB::raw('COALESCE(pos_sales_detail.oprice - pos_sales_detail.price, 0) as discounted_price'),
                DB::raw('COALESCE(pos_sales_detail.qty * pos_sales_detail.unit_qty * pos_sales_detail.price, 0) as amount'),
                DB::raw('COALESCE(pos_sales_detail.qty * pos_sales_detail.unit_qty * (pos_sales_detail.oprice - pos_sales_detail.price), 0) as discounted_amount'),
                DB::raw('COALESCE(((pos_sales_detail.oprice - pos_sales_detail.price) / pos_sales_detail.oprice) * 100, 0) as percentage_discount')
            )
            ->groupBy(
                'pos_sales.terminal_number',
                'pos_sales.transaction_date',
                'pos_sales.or_number',
                'user.full_name',
                'category.name',
                'product.name',
                'pos_sales_detail.id',
                'pos_sales_detail.qty',
                'pos_sales_detail.unit_qty',
                'pos_sales_detail.oprice',
                'pos_sales_detail.price'
            )
            ->whereNull('pos_sales.deleted_at')
            ->whereNull('pos_sales_detail.deleted_at')
            ->where('pos_sales.voided', Voided::NO)
            ->whereRaw('pos_sales_detail.price < pos_sales_detail.oprice');

        $builder = $this->setConditionsOnBuilder($builder, $filters, 'pos_sales', 'transaction_branch_id', [
            'transaction_branch_id' => function ($query, $filter, $method, $alias) {
                return $query->$method($alias . 'created_for', $filter->operator, $filter->value);
            }
        ]);

        $builder = $this->setConditionsOnBuilder($builder, $filters, 'pos_sales', 'transaction_terminal_number', [
            'transaction_terminal_number' => function ($query, $filter, $method, $alias) {
                if ($filter->value) {
                    return $query->$method($alias . 'terminal_number', $filter->operator, $filter->value);
                }
            }
        ]);

        $builder = $this->setConditionsOnBuilder($builder, $filters, 'pos_sales', 'date_from', [
            'date_from' => function ($query, $filter, $method, $alias) {
                return $query->$method($alias . 'transaction_date', $filter->operator, $filter->value);
            }
        ]);

        $builder = $this->setConditionsOnBuilder($builder, $filters, 'pos_sales', 'date_to', [
            'date_to' => function ($query, $filter, $method, $alias) {
                return $query->$method($alias . 'transaction_date', $filter->operator, $filter->value);
            }
        ]);

        $builder = $this->createSubQuery($builder)->orderBy('transaction_date', 'DESC');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new PosDiscountedSummaryReport());

        $this->resetModel();

        return $paginator;
    }

    public function getSeniorReport($filters = [])
    {
        $builder =  $this->model
            ->getQuery()
            ->where(function($query) {
                $query->whereNotNull('senior_number')->orWhereNotNull('senior_name');
            })
            ->leftJoin('pos_sales_detail', 'pos_sales_detail.transaction_id', '=', 'pos_sales.id')
            ->leftJoin('product', 'pos_sales_detail.product_id', '=', 'product.id')
            ->where('pos_sales_detail.is_senior', 1)
            ->whereNull('pos_sales.deleted_at')
            ->whereNull('pos_sales_detail.deleted_at')
            ->where('pos_sales.voided', Voided::NO)
            ->select(
                'transaction_date',
                'or_number',
                'senior_number',
                'senior_name',
                DB::raw('product.name as product_name'),
                'qty',
                'price'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            'pos_sales',
            [
                'or_number',
                'created_for',
                'transaction_date'
            ]
        );

        $builder = $this->createSubQuery($builder)->orderBy('transaction_date', 'DESC');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new SeniorTransactionReport());

        $this->resetModel();

        return $paginator;
    }

    /**
     * Get all detail transactions by transaction filter and product_id
     *
     * @param  array|int $ids
     * @param  int $limit
     * @return Model
     */
    public function findTransactionsByProduct($ids, $limit = null)
    {
        $that = $this;
        $model = $this->model->details()->getRelated();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $results = $model->with([
            'transaction',
            'transaction.creator',
            'transaction.cashier',
            'transaction.customer',
            'transaction.customerDetail'
        ])
        ->select('pos_sales_detail.*')
        ->whereIn('product_id', $ids)
        ->whereHas('transaction', function($query) use($that) {
            $criteria = $that->getCriteria();

            foreach ($criteria as $c) {
                if ($c instanceof CriteriaInterface) {
                    $query = $c->apply($query, $that);
                }
            }
            $query->where('voided', Voided::NO);
        })
        ->join('pos_sales', 'pos_sales.id', 'pos_sales_detail.transaction_id')
        ->orderBy('pos_sales.transaction_date', 'desc')
        ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();

        return $results;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
