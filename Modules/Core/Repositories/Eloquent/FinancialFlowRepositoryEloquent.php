<?php 

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

abstract class FinancialFlowRepositoryEloquent extends RepositoryEloquent
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_for',
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}