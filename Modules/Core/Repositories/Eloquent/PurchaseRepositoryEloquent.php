<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PurchaseRepository;
use Modules\Core\Entities\Purchase;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;

use DB;

/**
 * Class PurchaseRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PurchaseRepositoryEloquent extends InvoiceRepositoryEloquent implements PurchaseRepository
{
    public function model()
    {
        return Purchase::class; 
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['supplier_id', 'deliver_to', 'deliver_until']);
    }

    /**
     * Find remaining list of invoices based on filters
     *
     * @param  int $branch
     * @param  string $sheetNumber
     * @return Model
     */
    public function findRemaining($branch, $sheetNumber = '')
    {
        $model = $this->model
            ->where('sheet_number', 'LIKE', sprintf("%%%s%%", $sheetNumber))
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->whereIn('transaction_status', array(TransactionStatus::INCOMPLETE, TransactionStatus::NO_DELIVERY));

        if(!setting('purchase.inbound.display.purchase.other.branches')) {
            $model = $model->where('created_for', $branch);
        }

        $model = $model->orderBy('transaction_date', 'desc')
            ->limit(10)
            ->get();

        $this->resetModel();

        return $this->parserResult($model);
    }
}