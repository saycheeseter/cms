<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Entities\Hydration\PaymentDueWarning;
use Modules\Core\Entities\Hydration\PaymentDueWarningSummary;
use Modules\Core\Repositories\Contracts\PurchaseInboundRepository;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Enums\ApprovalStatus;

use DB;

/**
 * Class PurchaseInboundRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PurchaseInboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements PurchaseInboundRepository
{
    public function model()
    {
        return PurchaseInbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['supplier_id', 'dr_no', 'supplier_invoice_no']);
    }

    /**
     * Find transaction by the given reference number
     *
     * @param  string $reference
     * @return Model
     */
    public function findApprovedTransactionByReference($reference)
    {
        $model = $this->model
            ->with([
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.transactionable',
                'details.components'
            ])
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('sheet_number', $reference)
            ->first();

        $this->resetModel();

        return $this->parserResult($model);
    }

    public function transactionsWithBalance($branchId, $supplierId, $limit = null)
    {
        $builder = $this
            ->model
            ->with([
                'supplier',
                'payment',
                'for',
                'creator',
                'auditor',
                'from',
                'requested',
                'location',
                'reference'
            ])
            ->where('supplier_id', $supplierId)
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('remaining_amount', '>', 0);

        if(!setting('payment.display.transactions.of.other.branches')) {
            $builder = $builder->where('created_for', $branchId);
        }

        $results = $builder->paginate($limit);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function getDueWarning($filters = [], $all = false)
    {
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];
        $supplier = !isset($filters['supplier']) || empty($filters['supplier']) ? null : $filters['supplier'];

        $builder = $this->model
            ->getQuery()
            ->leftjoin('supplier', 'supplier.id', '=', 'purchase_inbound.supplier_id')
            ->select(
                'purchase_inbound.id',
                'purchase_inbound.transaction_date',
                'supplier.full_name as supplier_full_name',
                'purchase_inbound.sheet_number',
                'purchase_inbound.total_amount',
                'purchase_inbound.remaining_amount',
                'purchase_inbound.term',
                'purchase_inbound.remarks',
                'purchase_inbound.due_date'
            )
            ->whereNull('purchase_inbound.deleted_at')
            ->where([
                ['purchase_inbound.remaining_amount', '>', '0'],
                ['purchase_inbound.approval_status', '=', ApprovalStatus::APPROVED]
            ]);

        if(!filter_var($all, FILTER_VALIDATE_BOOLEAN)) {
            $builder = $builder->where('purchase_inbound.due_date', '<', Carbon::now());
        }

        if (!is_null($dateFrom)) {
            $builder = $builder->where('purchase_inbound.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('purchase_inbound.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('purchase_inbound.created_for', $branch);
        }

        if (!is_null($supplier)) {
            $builder = $builder->where('purchase_inbound.supplier_id', $supplier);
        }

        $paginator = $this->createPaginationFromQuery($builder->orderBy('transaction_date'));
        $paginator = $this->hydratePaginationItems($paginator, new PaymentDueWarning());

        $this->resetModel();

        return $paginator;
    }

    public function getDueWarningSummary($filters = [], $all = false)
    {
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];
        $supplier = !isset($filters['supplier']) || empty($filters['supplier']) ? null : $filters['supplier'];

        $builder = $this->model
            ->getQuery()
            ->leftjoin('supplier', 'supplier.id', '=', 'purchase_inbound.supplier_id')
            ->select(
                DB::raw('COUNT(purchase_inbound.id) as total_count'),
                DB::raw('SUM(purchase_inbound.total_amount) as total_amount'),
                DB::raw('SUM(purchase_inbound.remaining_amount) as total_remaining')
            )
            ->whereNull('purchase_inbound.deleted_at')
            ->where([
                ['purchase_inbound.remaining_amount', '>', '0'],
                ['purchase_inbound.approval_status', '=', ApprovalStatus::APPROVED]
            ]);

        if(!filter_var($all, FILTER_VALIDATE_BOOLEAN)) {
            $builder = $builder->where('purchase_inbound.due_date', '<', Carbon::now());
        }

        if (!is_null($dateFrom)) {
            $builder = $builder->where('purchase_inbound.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('purchase_inbound.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('purchase_inbound.created_for', $branch);
        }

        if (!is_null($supplier)) {
            $builder = $builder->where('purchase_inbound.supplier_id', $supplier);
        }

        return PaymentDueWarningSummary::hydrate($builder->get()->toArray())->first();
    }
}
