<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\CollectionRepository;
use Modules\Core\Entities\Collection;

use DB;

class CollectionRepositoryEloquent extends FinancialFlowRepositoryEloquent implements CollectionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'collection.deleted_at',
        'created_for',
        'customer.name',
        'customer_type'
    ];

    public function model()
    {
        return Collection::class;
    }

    public function search($limit = null)
    {
        $this->withTrashed()
            ->with([
                'details',
                'references',
                'from',
                'creator',
                'for',
                'modifier',
                'auditor',
                'customer'
            ]);

        $this->model = $this->model->leftJoin('customer', 'collection.customer_id', '=', 'customer.id')->select(DB::raw('collection.*'));

        return $this->paginate($limit);
    }
}