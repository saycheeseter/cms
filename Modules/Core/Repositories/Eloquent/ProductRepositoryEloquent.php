<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Entities\Hydration\ProductInventoryValue;
use Modules\Core\Entities\Hydration\ProductInventoryValueSummary;
use Modules\Core\Entities\Hydration\ProductInventoryWarning;
use Modules\Core\Entities\Hydration\ProductPriceSummary;
use Modules\Core\Entities\Hydration\ProductPeriodicSales;
use Modules\Core\Entities\Hydration\ProductBranchInventory;
use Modules\Core\Entities\Hydration\ProductTransactionList;
use Modules\Core\Enums\InventoryStatus;
use Modules\Core\Enums\InventoryWarning;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Entities\Product;
use Modules\Core\Entities\Customer;
use Modules\Core\Entities\ProductTransactionSummary;
use Modules\Core\Enums\SystemCodeType;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Core\Enums\CustomerPriceSettings;
use Modules\Core\Enums\CustomerPricingType;
use Modules\Core\Enums\GroupType;
use Modules\Core\Repositories\Criteria\RequestCriteria;
use Litipk\BigNumbers\Decimal;
use DB;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;

/**
 * Class ProductRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductRepositoryEloquent extends RepositoryEloquent implements ProductRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'supplier_id',
        'category_id',
        'brand_id',
        'name',
        'stock_no',
        'updated_at',
        'barcodes.code',
        'chinese_name',
        'description',
        'manage_type',
        'group_type',
        'status',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Returns a list of products based on filters
     * 
     * @param  array   $filters
     * @return LengthAwarePaginator
     */
    public function search($filters = [], $paging = true)
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                     ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('branch_price', function($join) use ($filters) {
                $join->on('product.id', 'branch_price.product_id')
                    ->where('branch_price.branch_id', branch());
            })
            ->leftJoin('product_branch_summary', function($join)  use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
            })
            ->join('product_branch_info', function($join) {
                $join->on('product.id', 'product_branch_info.product_id')
                    ->where('product_branch_info.branch_id', branch())
                    ->where('product_branch_info.status', 1);
            })
            ->select(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.supplier_sku',
                'product.description',
                'product.chinese_name',
                'product.memo',
                'product.status',
                'product.group_type',
                'product.type',
                'product.manage_type',
                'product.created_at',
                'product.updated_at',
                'branch_price.purchase_price AS purchase_price',
                'branch_price.selling_price AS selling_price',
                'branch_price.wholesale_price AS wholesale_price',
                'branch_price.price_a AS price_a',
                'branch_price.price_b AS price_b',
                'branch_price.price_c AS price_c',
                'branch_price.price_d AS price_d',
                'branch_price.price_e AS price_e',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_branch_summary.qty, 0) AS inventory')
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.supplier_sku',
                'product.description',
                'product.chinese_name',
                'product.memo',
                'product.status',
                'product.group_type',
                'product.type',
                'product.manage_type',
                'product.created_at',
                'product.updated_at',
                'branch_price.purchase_price',
                'branch_price.selling_price',
                'branch_price.wholesale_price',
                'branch_price.price_a',
                'branch_price.price_b',
                'branch_price.price_c',
                'branch_price.price_d',
                'branch_price.price_e',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'product_branch_summary.qty',
                'product_branch_summary.branch_detail_id'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'name',
                'stock_no',
                'supplier_sku',
                'description',
                'chinese_name',
                'memo',
                'status',
                'group_type',
                'type',
                'manage_type',
                'created_at',
                'updated_at',
                'purchase_price',
                'selling_price',
                'wholesale_price',
                'price_a',
                'price_b',
                'price_c',
                'price_d',
                'price_e',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                DB::raw('SUM(inventory) AS inventory')
            )
            ->groupBy(
                'id',
                'name',
                'stock_no',
                'supplier_sku',
                'description',
                'chinese_name',
                'memo',
                'status',
                'group_type',
                'type',
                'manage_type',
                'created_at',
                'updated_at',
                'purchase_price',
                'selling_price',
                'wholesale_price',
                'price_a',
                'price_b',
                'price_c',
                'price_d',
                'price_e',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode'
            );

        $builder = $this->createSubQuery($builder)->select('*');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'supplier',
                'category',
                'brand',
                'status',
                'stock_no',
                'name',
                'chinese_name',
                'created_at',
                'updated_at',
                'inventory',
                'barcode',
                'group_type',
                'manage_type',
                'type'
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                },
                'inventory' => function ($query, $filter, $method, $alias) {
                    switch($filter->value) {
                        case InventoryStatus::NEGATIVE:
                            $filter->operator = '<';
                            break;

                        case InventoryStatus::NO_INVENTORY:
                            $filter->operator = '=';
                            break;

                        case InventoryStatus::WITH_INVENTORY:
                            $filter->operator = '>';
                            break;
                    }

                    $filter->value = 0;

                    return $query->$method('inventory', $filter->operator, $filter->value);
                }
            ]);

        $result = null;

        if ($paging) {
            $result = $this->createPaginationFromQuery($builder);
            $result = $this->hydratePaginationItems($result, new ProductPriceSummary);
        } else {
            $result = ProductPriceSummary::hydrate($builder->get()->toArray());
        }

        $this->resetModel();

        return $result;
    }

    /**
     * Returns a list of products based on filters
     *
     * @return LengthAwarePaginator
     */
    public function filter()
    {
        $this->applyCriteria();
        $this->applyScope();
        $limit = request()->get('per_page') ?? config('repository.pagination.limit', 10);
        
        $results = $this->model
            ->with([
                'units',
                'barcodes',
                'prices' => function($query) {
                    $query->where('branch_id', branch());
                },
                'branches',
                'locations',
                'summaries' => function($query) {
                    $query->where('branch_id', branch());
                },
                'taxes'
            ])
            ->whereHas('branches', function($query) {
                $query->where('status', 1)
                    ->where('branch_id', branch());
            })
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function findByBarcode($barcode, $excluded = null)
    {
        $model = $this->model;

        if(!is_null($excluded)) {
            if(isset($excluded['group_type'])) {
                $model = $model->whereNotIn('group_type', $excluded['group_type']);
            }

            if(isset($excluded['manageable'])) {
                $model = $model->where('manageable', '<>', $excluded['manageable']);
            }

            if(isset($excluded['type'])) {
                $model = $model->where('type', '<>', $excluded['type']);
            }

            if(isset($excluded['manage_type'])) {
                $model = $model->whereNotIn('manage_type', array_wrap($excluded['manage_type']));
            }

            if(isset($excluded['status'])) {
                $model = $model->whereNotIn('status', array_wrap($excluded['status']));
            }
        }

        $results = $model
            ->whereHas('branches', function($query) {
                $query->where('status', 1)
                    ->where('branch_id', branch());
            })
            ->whereHas('barcodes', function($query) use ($barcode){
                $query->where('code', '=', $barcode);
            })
            ->get()
            ->first();

        $this->resetModel();

        return $results;
    }

    public function findByStockNo($stockNo, $excluded = null)
    {
        $model = $this->model;

        if(!is_null($excluded)) {
            if(isset($excluded['group_type'])) {
                $model = $model->whereNotIn('group_type', $excluded['group_type']);
            }

            if(isset($excluded['manageable'])) {
                $model = $model->where('manageable', '<>', $excluded['manageable']);
            }

            if(isset($excluded['type'])) {
                $model = $model->where('type', '<>', $excluded['type']);
            }

            if(isset($excluded['manage_type'])) {
                $model = $model->whereNotIn('manage_type', array_wrap($excluded['manage_type']));
            }

            if(isset($excluded['status'])) {
                $model = $model->whereNotIn('status', array_wrap($excluded['status']));
            }
        }

        $results = $model
            ->whereHas('branches', function($query) {
                $query->where('status', 1)
                    ->where('branch_id', branch());
            })
            ->where('stock_no', $stockNo)
            ->first();

        $this->resetModel();

        return $results;
    }

    public function searchByName($filters = [])
    {
        $builder = $this->model->getQuery()
            ->join('product_branch_info', function($join) {
                $join->on('product.id', 'product_branch_info.product_id')
                    ->where('product_branch_info.branch_id', branch())
                    ->where('product_branch_info.status', 1);
            })
            ->whereNull('deleted_at')
            ->take(10);

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            'product',
            ['name', 'group_type', 'type', 'manageable', 'manage_type', 'status']
        );

        $results = Product::hydrate($builder->get()->toArray());

        $this->resetModel();

        return $results;
    }

    /**
     * Return list of products with their corresponding supplier prices
     *
     * @param  int|array $productIds
     * @param  int $supplierId
     * @param  int $branchId
     * @return Collection
     */
    public function getSupplierPrices($productIds, $supplierId, $branchId, $locationId)
    {
        $productIds = array_wrap($productIds);
        $historicalPrice = setting('poi.price.based.on.history');
        $historicalUnit = setting('poi.unit.based.on.history');

        $eagerLoad = [
            'units',
            'barcodes',
            'units.uom',
            'prices' => function ($query) use ($branchId) {
                $query->where('branch_id', $branchId);
            },
            'summaries' => function ($query) use ($branchId, $locationId) {
                $query->where('branch_id', $branchId)
                    ->where('branch_detail_id', $locationId);
            }
        ];

        if ($historicalPrice || $historicalUnit) {
            $eagerLoad = array_merge($eagerLoad, [
                'suppliers' => function ($query) use ($supplierId) {
                    $query->where('supplier_id', $supplierId);
                }
            ]);
        }

        $results = $this->with($eagerLoad)->findMany($productIds);

        $this->resetModel();

        return $results;
    }

    /**
     * Return list of products with their corresponding customer prices
     * 
     * @param  int|array $productIds
     * @param  int $customerId
     * @param  int $branchId  
     * @return Collection            
     */
    public function getCustomerPrices($productIds, $customerId, $branchId, $locationId)
    {
        $productIds = array_wrap($productIds);
        $priceSetting = setting('soi.regular.customers.price.basis');
        $customer = Customer::find($customerId);

        $eagerLoad = [
            'units', 
            'barcodes', 
            'units.uom',
            'prices' => function ($query) use ($branchId) {
                $query->where('branch_id', $branchId);
            },
            'summaries' => function ($query) use ($branchId, $locationId) {
                $query->where('branch_id', $branchId)
                    ->where('branch_detail_id', $locationId);
            }
        ];

        if ($priceSetting ===  CustomerPriceSettings::CUSTOM 
            && $customer->pricing_type === CustomerPricingType::HISTORICAL
        ) {
            $eagerLoad = array_merge($eagerLoad, [
                'customers' => function ($query) use ($customerId) {
                    $query->where('customer_id', $customerId);
                }
            ]);
        }

        $results = $this->with($eagerLoad)->findMany($productIds);

        $this->resetModel();

        return $results;
    }

    /**
     * Return list of products with their corresponding summaries and prices
     *
     * @param  int|array $productIds
     * @param $branch
     * @param  int $location
     * @return Collection
     * @internal param int $branchId
     */
    public function getSummariesAndPrices($productIds, $branch, $location)
    {
        $productIds = array_wrap($productIds);

        $results = $this->with(array(
            'units', 
            'barcodes', 
            'units.uom',
            'prices' => function ($query) use ($branch) {
                $query->where('branch_id', $branch);
            },
            'summaries' => function ($query) use ($branch, $location) {
                $query->where('branch_id', $branch)
                    ->where('branch_detail_id', $location);
            }
        ))->findMany($productIds);

        $this->resetModel();

        return $results;
    }

    /**
     * Return list of products with their corresponding summaries and branch detail
     *
     * @param  array $productIds
     * @param $branch
     * @param  int $location
     * @return Collection
     * @internal param int $branchId
     */
    public function getSummaries($productIds, $branch, $location)
    {
        $array = true;

        if (!is_array($productIds)) {
            $array = false;
            $productIds = array_wrap($productIds);
        }

        $results = $this->with(array(
            'summaries' => function ($query) use ($branch, $location) {
                $query->where('branch_id', $branch)
                    ->where('branch_detail_id', $location);
            }
        ))->findMany($productIds);

        $this->resetModel();

        return $array ? $results : $results->first();
    }

    /**
     * Return list of products with their corresponding summaries and branch detail
     *
     * @param  array $productIds
     * @param $branch
     * @param  int $location
     * @return Collection
     * @internal param int $branchId
     */
    public function getSummariesAndLocations($productIds, $branch, $location)
    {
        $array = true;

        if (!is_array($productIds)) {
            $array = false;
            $productIds = array_wrap($productIds);
        }

        $results = $this->with(array(
            'locations' => function ($query) use ($branch, $location) {
                $query->where('branch_detail_id', $location);
            },
            'summaries' => function ($query) use ($branch, $location) {
                $query->where('branch_id', $branch)
                    ->where('branch_detail_id', $location);
            }
        ))->findMany($productIds);

        $this->resetModel();

        return $array ? $results : $results->first();
    }

    /**
     * Fetch the list of alternatives for the indicated product
     *
     * @param $product
     * @param $branch
     * @param $location
     * @return Collection
     */
    public function alternatives($product, $branch, $location)
    {
        $product = $this->model
            ->with([
                'alternatives' => function($query) {
                    $query->where('product.status', ProductStatus::ACTIVE);

                    $query->whereHas('branches', function($q) {
                        $q->where('status', 1)
                            ->where('branch_id', branch());
                    });
                },
                'alternatives.barcodes',
                'alternatives.summaries' => function($query) use ($branch, $location) {
                    $query->where('branch_id', $branch)
                        ->where('branch_detail_id', $location);
                },
            ])
            ->find($product);

        $this->resetModel();

        return $product->alternatives;
    }

    public function components($product)
    {
        $product = $this->with(['components.barcodes'])->find($product);

        $this->resetModel();

        return $product->components;
    }

    public function findGroupsById($ids)
    {
        $groups = $this->model
            ->where('group_type', '<>', GroupType::NONE)
            ->whereIn('id', $ids)
            ->get();

        $this->resetModel();

        return $groups;
    }

    public function getInventoryWarning($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->join('product_branch_summary', function($join) use ($filters) {
                $join = $join->on('product.id', '=', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_detail_id');
            })
            ->join('product_branch_detail', function($join) {
                $join->on('product.id', '=', 'product_branch_detail.product_id')
                    ->on('product_branch_detail.branch_detail_id', '=', 'product_branch_summary.branch_detail_id');
            })
            ->select(
                'product.name as product_name',
                DB::raw('SUM(product_branch_summary.qty) as inventory'),
                DB::raw('SUM(product_branch_summary.reserved_qty) as reserved_qty'),
                DB::raw('SUM(product_branch_summary.requested_qty) as requested_qty'),
                DB::raw('SUM(product_branch_detail.min) as min'),
                DB::raw('SUM(product_branch_detail.max) as max'),
                DB::raw('SUM(product_branch_summary.qty - product_branch_summary.reserved_qty + product_branch_summary.requested_qty) as projected_inventory')
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name'
            );

        $builder = $this->createSubQuery($builder);

        $warning = $this->getFiltersByKey($filters, 'warning_type');

        if(count($warning) > 0) {
            $builder = $this->setConditionsOnBuilder(
                $builder,
                $filters,
                'inner_query',
                ['warning_type', 'product_name'],
                [
                    'warning_type' => function ($query, $filter, $method, $alias) {
                        $filter->column = 'projected_inventory';
                        $filter->operator = (int) $filter->value === InventoryWarning::MIN ? '<' : '>';
                        $filter->value = (int) $filter->value === InventoryWarning::MIN
                            ? DB::raw('[inner_query].[min]')
                            : DB::raw('[inner_query].[max]');

                        return $query->$method($alias.$filter->column, $filter->operator, $filter->value);
                    },
                ]
            );
        } else {
            $builder = $this
                ->setConditionsOnBuilder($builder, $filters, 'inner_query', 'product_name')
                ->where(function ($query) {
                    $query->where(
                        'inner_query.projected_inventory',
                        '<',
                        DB::raw('[inner_query].[min]')
                    )->orWhere(
                        'inner_query.projected_inventory',
                        '>',
                        DB::raw('[inner_query].[max]')
                    );
                });
        }
        
        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new ProductInventoryWarning);

        $this->resetModel();

        return $paginator;
    }

    /**
     * Returns a list of with their corresponding inventory value
     *
     * @param  array   $filters
     * @return LengthAwarePaginator
     */
    public function getInventoryValue($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                    ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('product_branch_summary', function($join)  use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
            })
            ->leftJoin('branch_price', function($join) use ($filters) {
                $join->on('product.id', 'branch_price.product_id')
                    ->where('branch_price.branch_id', DB::raw('[product_branch_summary].[branch_id]'));
            })
            ->leftJoin('branch', 'branch.id', 'product_branch_summary.branch_id')
            ->leftJoin('branch_detail', 'branch_detail.id', 'product_branch_summary.branch_detail_id')
            ->select(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.status',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                'branch.name AS branch',
                'branch_detail.name AS location',
                'product_branch_summary.branch_detail_id as location_id',
                DB::raw('COALESCE(branch_price.purchase_price, 0) AS purchase_price'),
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_branch_summary.qty, 0) AS inventory'),
                DB::raw('COALESCE(product_branch_summary.current_cost, 0) AS current_cost')
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.status',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'branch.name',
                'branch_detail.name',
                'branch_price.purchase_price',
                'product_branch_summary.branch_detail_id',
                'product_branch_summary.qty',
                'product_branch_summary.current_cost'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'group_type',
                'status',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'branch',
                'location',
                'location_id',
                'barcode',
                DB::raw('SUM(inventory) AS inventory'),
                DB::raw('SUM(current_cost) AS current_cost'),
                DB::raw('SUM(purchase_price) AS purchase_price'),
                DB::raw('SUM(purchase_price) * SUM(inventory) AS purchase_inventory_value'),
                DB::raw('SUM(current_cost) * SUM(inventory) AS current_cost_inventory_value')
            )
            ->groupBy(
                'id',
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'group_type',
                'status',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'branch',
                'location_id',
                'location',
                'barcode'
            );

        $builder = $this->createSubQuery($builder)->select('*');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'supplier',
                'category',
                'brand',
                'status',
                'stock_no',
                'name',
                'chinese_name',
                'created_at',
                'updated_at',
                'inventory',
                'barcode',
                'group_type',
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                },
                'inventory' => function ($query, $filter, $method, $alias) {
                    switch($filter->value) {
                        case InventoryStatus::NEGATIVE:
                            $filter->operator = '<';
                            break;

                        case InventoryStatus::NO_INVENTORY:
                            $filter->operator = '=';
                            break;

                        case InventoryStatus::WITH_INVENTORY:
                            $filter->operator = '>';
                            break;
                    }

                    $filter->value = 0;

                    return $query->$method('inventory', $filter->operator, $filter->value);
                }
            ]);

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new ProductInventoryValue);

        $this->resetModel();

        return $paginator;
    }

    /**
     * Returns a list of with their corresponding inventory value
     *
     * @param  array   $filters
     * @return LengthAwarePaginator
     */
    public function getInventoryValueSummary($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                    ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('product_branch_summary', function($join)  use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
            })
            ->leftJoin('branch_price', function($join) use ($filters) {
                $join->on('product.id', 'branch_price.product_id')
                    ->where('branch_price.branch_id', DB::raw('[product_branch_summary].[branch_id]'));
            })
            ->select(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.status',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('COALESCE(branch_price.purchase_price, 0) AS purchase_price'),
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_branch_summary.qty, 0) AS inventory'),
                DB::raw('COALESCE(product_branch_summary.current_cost, 0) AS current_cost'),
                'product_branch_summary.branch_detail_id as location_id'
            )
            ->groupBy(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.status',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'branch_price.purchase_price',
                'product_branch_summary.branch_detail_id',
                'product_branch_summary.qty',
                'product_branch_summary.current_cost'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'group_type',
                'status',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                DB::raw('SUM(inventory) AS inventory'),
                DB::raw('SUM(current_cost) AS current_cost'),
                DB::raw('SUM(current_cost) * SUM(inventory) AS current_cost_inventory_value'),
                DB::raw('SUM(purchase_price) AS purchase_price'),
                DB::raw('SUM(purchase_price) * SUM(inventory) AS purchase_inventory_value')
            )
            ->groupBy(
                'id',
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'group_type',
                'status',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                'location_id'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory >= 0 THEN inventory
                            ELSE 0
                        END
                    ) as 'positive_inventory'
                "),
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory < 0 THEN inventory
                            ELSE 0
                        END
                    ) as 'negative_inventory'
                "),
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory >= 0 THEN current_cost_inventory_value
                            ELSE 0
                        END
                    ) as 'positive_ma_cost_amount'
                "),
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory < 0 THEN current_cost_inventory_value
                            ELSE 0
                        END
                    ) as 'negative_ma_cost_amount'
                "),
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory >= 0 THEN purchase_inventory_value
                            ELSE 0
                        END
                    ) as 'positive_cost_amount'
                "),
                DB::raw("
                    SUM(
                        CASE
                            WHEN inventory < 0 THEN purchase_inventory_value
                            ELSE 0
                        END
                    ) as 'negative_cost_amount'
                ")
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'supplier',
                'category',
                'brand',
                'status',
                'stock_no',
                'name',
                'chinese_name',
                'created_at',
                'updated_at',
                'inventory',
                'barcode',
                'group_type',
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                },
                'inventory' => function ($query, $filter, $method, $alias) {
                    switch($filter->value) {
                        case InventoryStatus::NEGATIVE:
                            $filter->operator = '<';
                            break;

                        case InventoryStatus::NO_INVENTORY:
                            $filter->operator = '=';
                            break;

                        case InventoryStatus::WITH_INVENTORY:
                            $filter->operator = '>';
                            break;
                    }

                    $filter->value = 0;

                    return $query->$method('inventory', $filter->operator, $filter->value);
                }
            ]);

        $result = $builder->first();

        $model = ProductInventoryValueSummary::newModelInstance()->newFromBuilder($result);

        $this->resetModel();

        return $model;
    }

    public function getPeriodicSales($filters = [], $paging = true)
    {
        $dateFilters = $this->getFiltersByKey($filters, 'year_month');
        $yearMonthList = $this->getMonthRangeList($dateFilters);
        
        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                     ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('product_branch_summary', function($join) use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
            })
            ->leftJoin('product_monthly_transactions', function($join) use ($filters) {
                $join = $join->on('product.id', 'product_monthly_transactions.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_monthly_transactions', 'transaction_branch_id', [
                    'transaction_branch_id' => function ($query, $filter, $method, $alias) {
                        return $query->$method($alias.'created_for', $filter->operator, $filter->value);
                    }
                ]);
            })
            ->select(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                'product_monthly_transactions.for_location AS product_monthly_transactions_for_location',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_branch_summary.qty, 0) AS inventory'),
                DB::raw('COALESCE(product_monthly_transactions.sales_outbound_total_amount + product_monthly_transactions.pos_sales_total_amount, 0) as total_amount'),
                DB::raw('COALESCE(product_monthly_transactions.sales_outbound_total_qty + product_monthly_transactions.pos_sales_total_qty, 0) as total_qty'),
                DB::raw('COALESCE(product_monthly_transactions.transaction_date, \'1800-01-01\') as transaction_date')
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'product_branch_summary.qty',
                'product_branch_summary.branch_detail_id',
                'product_monthly_transactions.for_location',
                'product_monthly_transactions.sales_outbound_total_amount',
                'product_monthly_transactions.sales_outbound_total_qty',
                'product_monthly_transactions.pos_sales_total_amount',
                'product_monthly_transactions.pos_sales_total_qty',
                'product_monthly_transactions.transaction_date'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                DB::raw('SUM(inventory) AS inventory'),
                'total_qty',
                'total_amount',
                'transaction_date'
            )
            ->groupBY(
                'id',
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                'product_monthly_transactions_for_location',
                'total_qty',
                'total_amount',
                'transaction_date'
            );

        $columns = [
            'name',
            'chinese_name',
            'status',
            'stock_no',
            'created_at',
            'updated_at',
            'group_type',
            'supplier_name',
            'supplier_code',
            'category_name',
            'category_code',
            'brand_name',
            'brand_code',
            'barcode',
            'inventory',
        ];

        foreach ($yearMonthList as $index => $value) {
            $columns[] = DB::raw(sprintf("
                SUM(
                    CASE
                        WHEN FORMAT(transaction_date, 'yyyy-MM') = '%s' THEN
                            total_qty
                        ELSE
                            0
                    END
                ) AS '%s_total_qty'
            ", $value, $value));

            $columns[] = DB::raw(sprintf("
                SUM(
                    CASE
                        WHEN FORMAT(transaction_date, 'yyyy-MM') = '%s' THEN
                            total_amount
                        ELSE
                            0
                    END
                ) AS '%s_total_amount'
            ", $value, $value));
        }

        $builder = $this->createSubQuery($builder)
            ->select($columns)
            ->groupBy(
                'id',
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                'inventory'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier',
                'category',
                'brand',
                'inventory',
                'barcode',
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                },
                'inventory' => function ($query, $filter, $method, $alias) {
                    switch($filter->value) {
                        case InventoryStatus::NEGATIVE:
                            $filter->operator = '<';
                            break;

                        case InventoryStatus::NO_INVENTORY:
                            $filter->operator = '=';
                            break;

                        case InventoryStatus::WITH_INVENTORY:
                            $filter->operator = '>';
                            break;
                    }

                    $filter->value = 0;

                    return $query->$method('inventory', $filter->operator, $filter->value);
                }
            ]
        );

        $result = null;

        if ($paging) {
            $result = $this->createPaginationFromQuery($builder);
            $result = $this->hydratePaginationItems($result, new ProductPeriodicSales);
        } else {
            $result = ProductPeriodicSales::hydrate($builder->get()->toArray());
        }

        $this->resetModel();

        return $result;
    }

    public function getTransactionList($basicFilters = [], $advancedFilters = [], $paging = true)
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                     ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('product_transaction_summary', function($join) use ($basicFilters) {
                $join = $join->on('product.id', 'product_transaction_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $basicFilters, 'product_transaction_summary', ['branch_detail_id', 'transaction_date'], [
                    'branch_detail_id' => function ($query, $filter, $method, $alias) {
                        return $query->$method($alias.'for_location', $filter->operator, $filter->value);
                    }
                ]);
            })
            ->select(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_transaction_summary.purchase_inbound, 0) AS purchase_inbound'),
                DB::raw('COALESCE(product_transaction_summary.purchase_return_outbound, 0) AS purchase_return_outbound'),
                DB::raw('COALESCE(product_transaction_summary.sales_outbound, 0) AS sales_outbound'),
                DB::raw('COALESCE(product_transaction_summary.sales_return_inbound, 0) AS sales_return_inbound'),
                DB::raw('COALESCE(product_transaction_summary.stock_delivery_inbound, 0) AS stock_delivery_inbound'),
                DB::raw('COALESCE(product_transaction_summary.stock_delivery_outbound, 0) AS stock_delivery_outbound'),
                DB::raw('COALESCE(product_transaction_summary.stock_return_inbound, 0) AS stock_return_inbound'),
                DB::raw('COALESCE(product_transaction_summary.adjustment_increase, 0) AS adjustment_increase'),
                DB::raw('COALESCE(product_transaction_summary.stock_return_outbound, 0) AS stock_return_outbound'),
                DB::raw('COALESCE(product_transaction_summary.damage_outbound, 0) AS damage_outbound'),
                DB::raw('COALESCE(product_transaction_summary.adjustment_decrease, 0) AS adjustment_decrease'),
                DB::raw('COALESCE(product_transaction_summary.product_conversion_increase, 0) AS product_conversion_increase'),
                DB::raw('COALESCE(product_transaction_summary.product_conversion_decrease, 0) AS product_conversion_decrease'),
                DB::raw('COALESCE(product_transaction_summary.auto_product_conversion_increase, 0) AS auto_product_conversion_increase'),
                DB::raw('COALESCE(product_transaction_summary.auto_product_conversion_decrease, 0) AS auto_product_conversion_decrease'),
                DB::raw('COALESCE(product_transaction_summary.pos_sales, 0) AS pos_sales'),
                DB::raw('COALESCE(product_transaction_summary.transaction_date, \'1800-01-01\') as transaction_date')
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'product_transaction_summary.purchase_inbound',
                'product_transaction_summary.purchase_return_outbound',
                'product_transaction_summary.sales_outbound',
                'product_transaction_summary.sales_return_inbound',
                'product_transaction_summary.stock_delivery_inbound',
                'product_transaction_summary.stock_delivery_outbound',
                'product_transaction_summary.stock_return_inbound',
                'product_transaction_summary.adjustment_increase',
                'product_transaction_summary.stock_return_outbound',
                'product_transaction_summary.damage_outbound',
                'product_transaction_summary.adjustment_decrease',
                'product_transaction_summary.product_conversion_increase',
                'product_transaction_summary.product_conversion_decrease',
                'product_transaction_summary.auto_product_conversion_increase',
                'product_transaction_summary.auto_product_conversion_decrease',
                'product_transaction_summary.pos_sales',
                'product_transaction_summary.transaction_date'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'barcode',
                'name',
                'chinese_name',
                DB::raw('SUM(purchase_inbound) AS purchase_inbound'),
                DB::raw('SUM(purchase_return_outbound) AS purchase_return_outbound'),
                DB::raw('SUM(sales_outbound) AS sales_outbound'),
                DB::raw('SUM(sales_return_inbound) AS sales_return_inbound'),
                DB::raw('SUM(stock_delivery_inbound) AS stock_delivery_inbound'),
                DB::raw('SUM(stock_delivery_outbound) AS stock_delivery_outbound'),
                DB::raw('SUM(stock_return_inbound) AS stock_return_inbound'),
                DB::raw('SUM(adjustment_increase) AS adjustment_increase'),
                DB::raw('SUM(stock_return_outbound) AS stock_return_outbound'),
                DB::raw('SUM(damage_outbound) AS damage_outbound'),
                DB::raw('SUM(adjustment_decrease) AS adjustment_decrease'),
                DB::raw('SUM(product_conversion_increase) AS product_conversion_increase'),
                DB::raw('SUM(product_conversion_decrease) AS product_conversion_decrease'),
                DB::raw('SUM(auto_product_conversion_increase) AS auto_product_conversion_increase'),
                DB::raw('SUM(auto_product_conversion_decrease) AS auto_product_conversion_decrease'),
                DB::raw('SUM(pos_sales) AS pos_sales'),
                DB::raw('
                    SUM(purchase_inbound)
                    - SUM(purchase_return_outbound)
                    - SUM(sales_outbound)
                    + SUM(sales_return_inbound)
                    + SUM(stock_delivery_inbound)
                    - SUM(stock_delivery_outbound)
                    + SUM(stock_return_inbound)
                    + SUM(adjustment_increase)
                    - SUM(stock_return_outbound)
                    - SUM(damage_outbound)
                    - SUM(adjustment_decrease)
                    + SUM(product_conversion_increase)
                    - SUM(product_conversion_decrease)
                    + SUM(auto_product_conversion_increase)
                    - SUM(auto_product_conversion_decrease)
                    - SUM(pos_sales) AS inventory
                ')
            )
            ->groupBy(
                'id',
                'barcode',
                'name',
                'chinese_name'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $basicFilters,
            '',
            [
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier',
                'category',
                'brand',
                'barcode'
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                }
            ]
        );

        if(!is_null($advancedFilters)) {
            $builder = $this->createSubQuery($builder)
                ->select(
                    'id',
                    'barcode',
                    'name',
                    'chinese_name',
                    'purchase_inbound',
                    'purchase_return_outbound',
                    'sales_outbound',
                    'sales_return_inbound',
                    'stock_delivery_inbound',
                    'stock_delivery_outbound',
                    'stock_return_inbound',
                    'adjustment_increase',
                    'stock_return_outbound',
                    'damage_outbound',
                    'adjustment_decrease',
                    'product_conversion_increase',
                    'product_conversion_decrease',
                    'auto_product_conversion_increase',
                    'auto_product_conversion_decrease',
                    'pos_sales',
                    'inventory'
                );

            if(array_key_exists('with', $advancedFilters)) {
                $withFilters = $advancedFilters['with'];

                $builder = $builder->where(function($query) use ($withFilters) {
                    foreach ($withFilters as $key => $value) {
                        $query = $query->orWhere($value, '<>', 0);
                    }
                });
            }

            if(array_key_exists('without', $advancedFilters)) {
                $withoutFilters = $advancedFilters['without'];

                $builder = $builder->where(function($query) use ($withoutFilters) {
                    foreach ($withoutFilters as $key => $value) {
                        $query = $query->orWhere($value, '=', 0);
                    }
                });
            }
        }

        $result = null;

        $date = $this->getFiltersByKey($basicFilters, 'transaction_date');

        if ($paging) {
            $paginator = $this->createPaginationFromQuery($builder);

            if (count($date) > 0) {
                $ids = array_column($paginator->items(), 'id');
                $beginning = $this->getTransactionListBeginningInventory($ids, $basicFilters)->keyBy('product_id');

                if ($beginning->count() > 0) {
                    $collection = $paginator->getCollection();

                    foreach ($collection as $key => $value) {
                        if (!$beginning->has($value->id)) {
                            continue;
                        }

                        $collection[$key]->beginning = $beginning->get($value->id)->beginning->innerValue();

                        $collection[$key]->inventory = Decimal::create(app('NumberFormatter')
                            ->parse($collection[$key]->inventory), setting('monetary.precision'))
                            ->add($beginning->get($value->id)->beginning)
                            ->innerValue();
                    }

                    $paginator->setCollection($collection);
                }

            }

            $result = $this->hydratePaginationItems($paginator, new ProductTransactionList);
        } else {
            $data = $builder->get()->toArray();

            if (count($date) > 0) {
                $ids = array_column($data, 'id');
                $beginning = $this->getTransactionListBeginningInventory($ids, $basicFilters)->keyBy('product_id');

                if ($beginning->count() > 0) {
                    foreach ($data as $key => $value) {
                        if (!$beginning->has($value->id)) {
                            continue;
                        }

                        $data[$key]->beginning = $beginning->get($value->id)->beginning->innerValue();

                        $data[$key]->inventory = Decimal::create(app('NumberFormatter')
                            ->parse($data[$key]->inventory), setting('monetary.precision'))
                            ->add($beginning->get($value->id)->beginning)
                            ->innerValue();
                    }
                }
            }

            $result = ProductTransactionList::hydrate($data);
        }

        $this->resetModel();

        return $result;
    }

    private function getTransactionListBeginningInventory($productIds = [], $filters = [])
    {
        $dates = $this->getFiltersByKey($filters, 'transaction_date');

        $location = array_first($this->getFiltersByKey($filters, 'branch_detail_id'))['value'];

        $date = array_first($dates,
            function($value, $key) {
                return $value['operator'] == '>=';
            }
        )['value'];

        $builder = ProductTransactionSummary::whereIn('product_id', $productIds)
            ->where('transaction_date', '<', Carbon::parse($date)->toDateString())
            ->select(
                DB::raw('product_id'),
                DB::raw('
                    SUM(purchase_inbound)
                    - SUM(purchase_return_outbound)
                    - SUM(sales_outbound)
                    + SUM(sales_return_inbound)
                    + SUM(stock_delivery_inbound)
                    - SUM(stock_delivery_outbound)
                    + SUM(stock_return_inbound)
                    + SUM(adjustment_increase)
                    - SUM(stock_return_outbound)
                    - SUM(damage_outbound)
                    - SUM(adjustment_decrease)
                    + SUM(product_conversion_increase)
                    - SUM(product_conversion_decrease)
                    + SUM(auto_product_conversion_increase)
                    - SUM(auto_product_conversion_decrease)
                    - SUM(pos_sales) AS beginning
                ')
            )
            ->groupBy('product_id');

        if (!is_null($location)) {
            $builder = $builder->where('for_location', '=', $location);
        }

        return $builder->get();
    }

    private function getMonthRangeList($dateFilters)
    {
        $yearMonthList = [];
        $fromYearMonth = '';
        $toYearMonth = '';

        foreach ($dateFilters as $key => $value) {
            if($value['operator'] == '>=') {
                $fromYearMonth = $value['value'];
            } else if($value['operator'] == '<=') {
                $toYearMonth = $value['value'];
            }
        }

        $start = (new DateTime($fromYearMonth.'-01'))->modify('first day of this month');
        $end = (new DateTime($toYearMonth.'-01'))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $yearMonthList[] = $dt->format("Y-m");
        }

        return $yearMonthList;
    }

    public function getBranchInventories($filters = [], $paging = true)
    {
        $locations = $this->getFiltersByKey($filters, 'branch_detail_id');
        $locationIds = array_pluck($locations, 'value');

        $builder = $this->model
            ->getQuery()
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                     ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->leftJoin('product_branch_summary', function($join) use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_detail_id');
            })
            ->select(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                'product_branch_summary.branch_detail_id AS product_branch_summary_branch_detail_id',
                'product_branch_summary.qty AS product_branch_summary_qty'
            )
            ->whereNull('product.deleted_at')
            ->groupBy(
                'product.id',
                'product.name',
                'product.chinese_name',
                'product.status',
                'product.stock_no',
                'product.created_at',
                'product.updated_at',
                'product.group_type',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'product_branch_summary.branch_detail_id',
                'product_branch_summary.qty'
            );

        $columns = [
            'name',
            'barcode'
        ];

        foreach ($locationIds as $id) {
            $columns[] = DB::raw(sprintf("
                SUM(
                    CASE
                        WHEN product_branch_summary_branch_detail_id = '%s' THEN
                            product_branch_summary_qty
                        ELSE 
                            0
                    END
                )
                AS 'location_%s'
            ", $id, $id));
        }

        $builder = $this->createSubQuery($builder)
            ->select($columns)
            ->groupBy(
                'id',
                'name',
                'barcode'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier',
                'category',
                'brand',
                'barcode',
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                }
            ]
        );

        $result = null;

        if ($paging) {
            $result = $this->createPaginationFromQuery($builder);
            $result = $this->hydratePaginationItems($result, new ProductBranchInventory);
        } else {
            $result = ProductBranchInventory::hydrate($builder->get()->toArray());
        }

        $this->resetModel();

        return $result;
    }

    public function whereActiveInBranch()
    {
        $this->model = $this->model->whereHas('branches', function($query) {
            $query->where('branch_id', branch())
                ->where('status', 1);
        });

        return $this;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
