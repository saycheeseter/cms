<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\GiftCheckRepository;
use Modules\Core\Entities\GiftCheck;
use Modules\Core\Enums\GiftCheckStatus;
use Carbon\Carbon;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class GiftCheckRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class GiftCheckRepositoryEloquent extends RepositoryEloquent implements GiftCheckRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'check_number',
        'status',
        'updated_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GiftCheck::class;
    }

    public function findValidCheckByNo($check)
    {   
        $now = Carbon::now()->toDateString();

        $builder = $this->model->where('check_number', $check)
            ->where(function($query) use($now){
                $query->where('date_from', '<=', $now)->where('date_to', '>=', $now);
            })->where('status', GiftCheckStatus::AVAILABLE);

        $this->resetModel();

        return $this->parserResult($builder->get()->first());
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
