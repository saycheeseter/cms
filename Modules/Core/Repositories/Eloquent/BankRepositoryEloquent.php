<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\BankRepository;
use Modules\Core\Entities\Bank;

/**
 * Class BankRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BankRepositoryEloquent extends SystemCodeRepositoryEloquent implements BankRepository
{
    public function model()
    {
        return Bank::class;
    }
}
