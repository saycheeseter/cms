<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Enums\UserType;
use Modules\Core\Repositories\Contracts\AuditTrailRepository;
use Modules\Core\Entities\Audit;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Criteria\LatestRecordFirstCriteria;

/**
 * Class AuditTrailRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class AuditTrailRepositoryEloquent extends RepositoryEloquent implements AuditTrailRepository
{   
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'event'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Audit::class;
    }

    public function search($limit = null)
    {
        $this->model = $this->model->whereHas('audited', function ($query) {
            $query->where('type', '<>', UserType::SUPERADMIN);
        });

        return $this->paginate($limit);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {   
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(LatestRecordFirstCriteria::class));
    }
}
