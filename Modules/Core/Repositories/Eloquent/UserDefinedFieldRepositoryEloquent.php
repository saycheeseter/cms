<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\UserDefinedFieldRepository;
use Modules\Core\Entities\UserDefinedField;

/**
 * Class UserDefinedFieldRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class UserDefinedFieldRepositoryEloquent extends RepositoryEloquent implements UserDefinedFieldRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserDefinedField::class;
    }

    public function findByModule($module)
    {
        return $this->model->where('module_id', $module)->get();
    }

    public function search($module, $limit)
    {
        $this->model = $this->model->where('module_id', $module);

        return $this->paginate($limit);
    }
}
