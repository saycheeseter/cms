<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\DamageOutboundRepository;
use Modules\Core\Entities\DamageOutbound;

/**
 * Class DamageRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class DamageOutboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements DamageOutboundRepository
{
    public function model()
    {
        return DamageOutbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['reason_id']);
    }
}
