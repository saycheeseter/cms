<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PaymentTypeRepository;
use Modules\Core\Entities\PaymentType;

/**
 * Class PaymentTypeRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PaymentTypeRepositoryEloquent extends SystemCodeRepositoryEloquent implements PaymentTypeRepository
{
    public function model()
    {
        return PaymentType::class; 
    }
}
