<?php 

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ProductTransactionSummaryRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Entities\ProductTransactionSummary;

use DB;

/**
 * Class ProductTransactionSummaryRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductTransactionSummaryRepositoryEloquent extends RepositoryEloquent implements ProductTransactionSummaryRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_for',
        'for_location',
        'transaction_date'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductTransactionSummary::class;
    }

    /**
     * Get the list of products by filters with their corresponding transaction summary
     * 
     * @param  array|int $ids
     * @param  int $limit
     * @return Model
     */
    public function search($ids, $limit = null) 
    {
        $products = ! is_array($ids) ? [$ids] : $ids;

        $query = $this->applyCriteria()
            ->model
            ->whereIn('product_id', $products)
            ->select(
                DB::raw('SUM(purchase_inbound) as purchase_inbound'),
                DB::raw('SUM(stock_delivery_inbound) as stock_delivery_inbound'),
                DB::raw('SUM(stock_return_inbound) as stock_return_inbound'),
                DB::raw('SUM(sales_return_inbound) as sales_return_inbound'),
                DB::raw('SUM(adjustment_increase) as adjustment_increase'),
                DB::raw('SUM(product_conversion_increase) as product_conversion_increase'),
                DB::raw('SUM(auto_product_conversion_increase) as auto_product_conversion_increase'),
                DB::raw('SUM(purchase_return_outbound) as purchase_return_outbound'),
                DB::raw('SUM(stock_delivery_outbound) as stock_delivery_outbound'),
                DB::raw('SUM(stock_return_outbound) as stock_return_outbound'),
                DB::raw('SUM(sales_outbound) as sales_outbound'),
                DB::raw('SUM(damage_outbound) as damage_outbound'),
                DB::raw('SUM(adjustment_decrease) as adjustment_decrease'),
                DB::raw('SUM(product_conversion_decrease) as product_conversion_decrease'),
                DB::raw('SUM(auto_product_conversion_decrease) as auto_product_conversion_decrease'),
                DB::raw('SUM(pos_sales) as pos_sales')
            )
            ->groupBy('product_id');

        $limit = is_null($limit) ? config('repository.pagination.limit', 10) : $limit;

        $results = !is_array($ids) ? $query->first() : $query->paginate($limit)->appends(app('request')->query());

        $this->resetModel();

        return $results;
    }

    /**
     * Get the beginning inventory of the given products before the specified date
     * 
     * @param  array|int $id 
     * @param  date $date Get inventory before this date
     * @return Model
     */
    public function getBeginning($ids, $date)
    {
        $fieldSearchableBuffer = $this->fieldSearchable;

        $products = ! is_array($ids) ? [$ids] : $ids;

        unset($this->fieldSearchable[2]);
        
        $query = $this->applyCriteria()
            ->model
            ->whereIn('product_id', $products)
            ->where('transaction_date', '<', $date)
            ->select(
                DB::raw('SUM(
                    purchase_inbound 
                    + stock_delivery_inbound
                    + stock_return_inbound
                    + sales_return_inbound
                    + adjustment_increase
                    + product_conversion_increase
                    + auto_product_conversion_increase
                    - purchase_return_outbound
                    - stock_delivery_outbound
                    - stock_return_outbound
                    - sales_outbound
                    - damage_outbound
                    - adjustment_decrease
                    - product_conversion_decrease
                    - auto_product_conversion_decrease
                    - pos_sales) 
                    AS beginning'
                )
            )
            ->groupBy('product_id');

        $this->fieldSearchable = $fieldSearchableBuffer;

        $results = !is_array($ids) ? $query->first() : $query->get();

        $this->resetModel();

        return $results;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}