<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PurchaseReturnOutboundRepository;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Enums\ApprovalStatus;

/**
 * Class PurchaseReturnOutboundRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PurchaseReturnOutboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements PurchaseReturnOutboundRepository
{
    public function model()
    {
        return PurchaseReturnOutbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['supplier_id']);
    }

    public function transactionsWithBalance($branchId, $supplierId, $limit = null)
    {
        $builder = $this
            ->model
            ->with([
                'supplier',
                'payment',
                'for',
                'creator',
                'auditor',
                'from',
                'requested',
                'location',
                'reference'
            ])
            ->where('supplier_id', $supplierId)
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('remaining_amount', '>', 0);

        if(!setting('payment.display.transactions.of.other.branches')) {
            $builder = $builder->where('created_for', $branchId);
        }

        $results = $builder->paginate($limit);

        $this->resetModel();

        return $this->parserResult($results);
    }
}
