<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\UserRepository;
use Modules\Core\Entities\User;
use Modules\Core\Enums\Permissions;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Criteria\RequestCriteria;
use Modules\Core\Repositories\Criteria\SuperadminExcludedCriteria;

/**
 * Class UserRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class UserRepositoryEloquent extends RepositoryEloquent implements UserRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'username',
        'full_name',
        'position',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
