<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\IncomeTypeRepository;
use Modules\Core\Entities\IncomeType;

/**
 * Class IncomeTypeRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class IncomeTypeRepositoryEloquent extends SystemCodeRepositoryEloquent implements IncomeTypeRepository
{
    public function model()
    {
        return IncomeType::class; 
    }
}
