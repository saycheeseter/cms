<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Criteria\ParentCriteria;
use Modules\Core\Repositories\Contracts\CategoryRepository;
use Modules\Core\Entities\Category;
use Modules\Core\Repositories\Criteria\RequestCriteria;
use Carbon\Carbon;
use Modules\Core\Entities\Hydration\CategoryMonthlySalesRanking;
use DB;

/**
 * Class CategoryRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class CategoryRepositoryEloquent extends RepositoryEloquent implements CategoryRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'parent_id',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    public function getMonthlySalesRanking($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('product', 'product.category_id', '=', 'category.id')
            ->leftJoin('product_monthly_transactions', 'product.id', '=', 'product_monthly_transactions.product_id')
            ->select(
                'category.code as category_code',
                'category.name as category_name',
                DB::raw('SUM(product_monthly_transactions.sales_outbound_total_qty) + SUM(product_monthly_transactions.pos_sales_total_qty) as total_qty'),
                DB::raw('SUM(product_monthly_transactions.sales_outbound_total_amount) + SUM(product_monthly_transactions.pos_sales_total_amount) as total_amount'),
                DB::raw('
                    SUM(product_monthly_transactions.sales_outbound_total_amount) 
                    + SUM(product_monthly_transactions.pos_sales_total_amount) 
                    - SUM(product_monthly_transactions.sales_outbound_total_cost)
                    - SUM(product_monthly_transactions.pos_sales_total_cost) as total_profit
                ')
            )
            ->whereNull('category.deleted_at')
            ->groupBy(
                'category.id',
                'category.code',
                'category.name'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            [
                'created_for' => 'product_monthly_transactions',
                'for_location' => 'product_monthly_transactions',
                'supplier_id' => 'product',
                'brand_id' => 'product',
            ],
            [
                'supplier_id',
                'created_for',
                'for_location',
                'category_id',
                'brand_id',
                'year_month',
            ],
            [
                'category_id' => function ($query, $filter, $method, $alias) {
                    $query->$method('category.id', $filter->operator, $filter->value);
                },
                'year_month' => function ($query, $filter, $method, $alias) {
                    $query->$method(function ($q) use ($filter) {
                        $date = Carbon::parse($filter->value);

                        $q->where('product_monthly_transactions.transaction_date', $filter->operator, $date->toDateString());
                    });
                },
            ]);

        $builder = $this->createSubQuery($builder)
            ->orderBy('total_amount', 'desc')
            ->orderBy('category_code', 'desc');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new CategoryMonthlySalesRanking);

        $this->resetModel();

        return $paginator;
    }

    /**
     * Returns the no. of associated children on the given category
     * 
     * @param  integer $id
     * @return integer    
     */
    public function children($id)
    {
        return $this->model->where('parent_id', $id)->count();
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(ParentCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
