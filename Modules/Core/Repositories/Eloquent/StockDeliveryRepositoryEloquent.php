<?php 

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockDeliveryRepository;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Enums\ApprovalStatus;
use DB;

/**
 * Class StockDeliveryRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockDeliveryRepositoryEloquent extends InvoiceRepositoryEloquent implements StockDeliveryRepository
{
    public function model()
    {
        return StockDelivery::class; 
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['deliver_to', 'deliver_to_location']);
    }

    /**
     * Retrieve invoice details whose remaining qty
     * is > 0
     *
     * @param  string $id
     * @return array
     */
    public function retrieve($id)
    {
        $model = $this->model
            ->with(array(
                'details' => function($query) {
                    $query->where('remaining_qty', '>', 0);
                },
                'deliverTo.details',
            ))
            ->find($id);

        $this->resetModel();

        return $this->parserResult($model);
    }
}