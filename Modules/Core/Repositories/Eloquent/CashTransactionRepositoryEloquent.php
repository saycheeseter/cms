<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\Hydration\CashTransaction;
use Modules\Core\Entities\Hydration\CashTransactionSummary;
use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Entities\Payment;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Repositories\Contracts\CashTransactionRepository;
use App;
use DB;
use Lang;

class CashTransactionRepositoryEloquent extends RepositoryEloquent implements CashTransactionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CashTransaction::class;
    }

    public function search($filters = [])
    {
        $builder = $this->buildTransactionQuery($filters);

        $results = $builder->orderBy('transaction_date')->get();

        $hydrated = CashTransaction::hydrate($results->toArray());

        return $hydrated;
    }

    public function getBeginning($filters = [])
    {
        if (isset($filters['date_from']) && !empty($filters['date_from'])) {
            $filters['beginning'] = $filters['date_from'];

            unset($filters['date_from']);
            unset($filters['date_to']);

            $builder = $this->buildTransactionQuery($filters);

            $result = $this->createSubQuery($builder)
                ->select(DB::raw("SUM([in]) - SUM([out]) AS total"))
                ->first();

            $result = (array) $result;
        } else {
            $result = ['total' => 0];
        }

        $hydrated = (new CashTransactionSummary())->newFromBuilder($result);

        return $hydrated;
    }

    protected function buildTransactionQuery($filters = [])
    {

        $beginning = !isset($filters['beginning']) || empty($filters['beginning']) ? null : Carbon::parse($filters['beginning']);
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];

        // Payment with cash method
        $builder = Payment::getQuery()
            ->leftJoin('supplier', 'supplier.id', 'payment.supplier_id')
            ->leftJoin('payment_detail', 'payment.id', 'payment_detail.transaction_id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Payment::class))),
                'payment.id AS reference_id',
                DB::raw("CAST(payment.transaction_date AS DATE) AS 'transaction_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', supplier.full_name, '; ') AS particular",
                        Lang::get('core::label.pay.to')
                    )
                ),
                DB::raw("0 AS 'in'"),
                "payment_detail.amount AS out",
                'remarks'
            )
            ->whereNull('payment.deleted_at')
            ->whereNull('payment_detail.deleted_at')
            ->where('payment.approval_status', ApprovalStatus::APPROVED)
            ->where('payment_detail.payment_method', PaymentMethod::CASH);

        if (!is_null($dateFrom)) {
            $builder = $builder->where('payment.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('payment.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $builder = $builder->where('payment.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('payment.created_for', $branch);
        }

        // Other payment with cash
        $query = OtherPayment::getQuery()
            ->leftJoin('other_payment_detail', 'other_payment_detail.transaction_id', 'other_payment.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherPayment::class))),
                'other_payment.id as reference_id',
                DB::raw("CAST(other_payment.transaction_date AS DATE) AS 'transaction_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', other_payment.reference_name, '; ') AS particular",
                        Lang::get('core::label.pay.to')
                    )
                ),
                DB::raw("0 AS 'in'"),
                DB::raw("SUM(other_payment_detail.amount) AS 'out'"),
                'remarks'
            )
            ->whereNull('other_payment.deleted_at')
            ->where('other_payment.payment_method', PaymentMethod::CASH)
            ->groupBy(
                'other_payment.id',
                DB::raw('CAST(other_payment.transaction_date AS DATE)'),
                'other_payment.reference_name',
                'remarks'
            );

        if (!is_null($dateFrom)) {
            $query = $query->where('other_payment.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('other_payment.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('other_payment.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_payment.created_from', $branch);
        }

        $builder->unionAll($query);

        // Collection with cash method
        $query = Collection::getQuery()
            ->leftJoin('customer', 'customer.id', 'collection.customer_id')
            ->leftJoin('collection_detail', 'collection.id', 'collection_detail.transaction_id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Collection::class))),
                'collection.id AS reference_id',
                DB::raw("CAST(collection.transaction_date AS DATE) AS 'transaction_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ',
                            CASE
                                WHEN collection.customer_type = %s THEN customer.name
                                ELSE collection.customer_walk_in_name
                            END, '; '
                        ) AS particular",
                        Lang::get('core::label.received.from'),
                        Customer::REGULAR
                    )
                ),
                "collection_detail.amount AS 'in'",
                DB::raw("0 AS 'out'"),
                'remarks'
            )
            ->whereNull('collection.deleted_at')
            ->whereNull('collection_detail.deleted_at')
            ->where('collection.approval_status', ApprovalStatus::APPROVED)
            ->where('collection_detail.payment_method', PaymentMethod::CASH);

        if (!is_null($dateFrom)) {
            $query = $query->where('collection.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('collection.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('collection.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('collection.created_for', $branch);
        }

        $builder->unionAll($query);

        // Other Income with cash
        $query = OtherIncome::getQuery()
            ->leftJoin('other_income_detail', 'other_income_detail.transaction_id', 'other_income.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherIncome::class))),
                'other_income.id as reference_id',
                DB::raw("CAST(other_income.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', other_income.reference_name, '; ') AS particular",
                        Lang::get('core::label.received.from')
                    )
                ),
                DB::raw("SUM(other_income_detail.amount) AS 'in'"),
                DB::raw("0 AS 'out'"),
                'remarks'
            )
            ->whereNull('other_income.deleted_at')
            ->where('other_income.payment_method', PaymentMethod::CASH)
            ->groupBy(
                'other_income.id',
                DB::raw('CAST(other_income.transaction_date AS DATE)'),
                'other_income.reference_name',
                'remarks'
            );

        if (!is_null($dateFrom)) {
            $query = $query->where('other_income.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('other_income.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('other_income.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_income.created_from', $branch);
        }

        $builder->unionAll($query);

        return $builder;
    }
}
