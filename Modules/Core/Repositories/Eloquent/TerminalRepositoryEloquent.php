<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\TerminalRepository;
use Modules\Core\Entities\Terminal;
use DB;

/**
 * Class TerminalRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class TerminalRepositoryEloquent extends RepositoryEloquent implements TerminalRepository
{
    public function model()
    {
        return Terminal::class;
    }

    public function getByBranch($branch)
    {
        return $this->findWhere(['branch_id' => $branch]);
    }
}
