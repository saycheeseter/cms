<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ProductConversionRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

use Modules\Core\Entities\ProductConversion;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;

/**
 * Class ProductConversionRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductConversionRepositoryEloquent extends RepositoryEloquent implements ProductConversionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_from',
        'created_for',
        'for_location',
        'transaction_date',
        'approval_status',
        'product_id',
        'sheet_number',
        'deleted_at',
        'updated_at',
        'group_type'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductConversion::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }

    public function getGroupIncrement($id, $limit = null)
    {
        return $this->findProductInInfoTransactions($id, 1, $limit);
    }

    public function getComponentIncrement($id, $limit = null)
    {
        return $this->findProductInDetailTransactions($id, 1, $limit);
    }

    public function getGroupDecrement($id, $limit = null)
    {
        return $this->findProductInInfoTransactions($id, 2, $limit);
    }

    public function getComponentDecrement($id, $limit = null)
    {
        return $this->findProductInDetailTransactions($id, 2, $limit);
    }

    public function findProductInInfoTransactions($id, $type, $limit = null)
    {
        $this->applyCriteria();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        $groupType = $type == 1 ? GroupType::MANUAL_GROUP : GroupType::MANUAL_UNGROUP;

        $results = $this->model->where('approval_status', ApprovalStatus::APPROVED)
            ->with(['creator'])
            ->where('product_id', $id)
            ->where('group_type', $groupType)
            ->orderBy('transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();
        
        return $results;
    }

    public function findProductInDetailTransactions($id, $type, $limit = null)
    {
        $that = $this;
        $model = $this->model->details()->getRelated();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        $groupType = $type == 1 ? GroupType::MANUAL_UNGROUP : GroupType::MANUAL_GROUP;

        $results = $model->with(['transaction', 'transaction.creator'])
            ->select('product_conversion_detail.*')
            ->where('component_id', $id)
            ->whereHas('transaction', function($query) use($that, $groupType){
                $query = $that->getCriteria()[0]->apply($query, $that);
                $query->where('approval_status', '=', ApprovalStatus::APPROVED)
                    ->where('group_type', $groupType);
            })
            ->join('product_conversion', 'product_conversion.id', 'product_conversion_detail.transaction_id')
            ->orderBy('product_conversion.transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();
        
        return $results;
    }
}