<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ProductBranchSummaryRepository;
use Modules\Core\Entities\ProductBranchSummary;

/**
 * Class ProductBranchSummaryRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductBranchSummaryRepositoryEloquent extends RepositoryEloquent implements ProductBranchSummaryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductBranchSummary::class;
    }

    /**
     * Find inventory record of the products by branch and location
     *
     * @param $products
     * @param $branch
     * @param $location
     * @return mixed
     */
    public function findByFilters($products, $branch, $location)
    {
        if (!is_array($products)) {
            $products = [$products];
        }

        $model = $this->model->where('branch_id', $branch)
            ->where('branch_detail_id', $location)
            ->whereIn('product_id', $products)
            ->get();

        $this->resetModel();

        return $this->parserResult($model);
    }
}
