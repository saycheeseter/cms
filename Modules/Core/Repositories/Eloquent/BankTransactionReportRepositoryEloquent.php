<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Entities\BankAccount;
use Modules\Core\Entities\Collection;
use Modules\Core\Entities\CollectionDetail;
use Modules\Core\Entities\Hydration\BankTransactionReport;
use Modules\Core\Entities\Hydration\BankTransactionSummary;
use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Entities\Payment;
use Modules\Core\Entities\PaymentDetail;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\CheckStatus;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Repositories\Contracts\BankTransactionReportRepository;
use App;
use DB;
use Lang;
use Modules\Core\Entities\BankTransaction;

/**
 * Class BankTransactionReportRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BankTransactionReportRepositoryEloquent extends RepositoryEloquent implements BankTransactionReportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BankTransactionReport::class;
    }

    public function search($filters = [])
    {
        $builder = $this->buildTransactionQuery($filters);

        $results = $builder->orderBy('check_date')->get();

        $hydrated = BankTransactionReport::hydrate($results->toArray());

        return $hydrated;
    }

    public function getBeginning($filters = [])
    {
        if (isset($filters['date_from']) && !empty($filters['date_from'])) {
            $filters['beginning'] = $filters['date_from'];

            unset($filters['date_from']);
            unset($filters['date_to']);

            $builder = $this->buildTransactionQuery($filters);

            // Bank Account opening balance
            $query = BankAccount::getQuery()
                ->select(
                    DB::raw("'' AS 'type'"),
                    DB::raw("0 AS 'reference_id'"),
                    DB::raw("'-' AS 'check_date'"),
                    DB::raw("'-' AS 'particular'"),
                    'opening_balance AS in',
                    DB::raw("0 AS 'out'")
                );

            if (!is_null($filters['bank_account']) && !empty($filters['bank_account'])) {
                $query = $query->where('id', $filters['bank_account']);
            }

            $builder->unionAll($query);

            $result = $this->createSubQuery($builder)
                ->select(DB::raw("SUM([in]) - SUM([out]) AS total"))
                ->first();

            $result = (array) $result;
        } else {
            $result = ['total' => 0];
        }

        $hydrated = (new BankTransactionSummary())->newFromBuilder($result);

        return $hydrated;
    }

    protected function buildTransactionQuery($filters = [])
    {
        $that = $this;

        $beginning = !isset($filters['beginning']) || empty($filters['beginning']) ? null : Carbon::parse($filters['beginning']);
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $bankAccount = !isset($filters['bank_account']) || empty($filters['bank_account']) ? null : $filters['bank_account'];
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];

        // Payment with check method
        $builder = Payment::getQuery()
            ->leftJoin('supplier', 'supplier.id', 'payment.supplier_id')
            ->leftJoin('payment_detail', 'payment.id', 'payment_detail.transaction_id')
            ->leftJoin('issued_check', function($join) use ($that) {
                $join->on('payment_detail.id', '=', 'issued_check.transactionable_id')
                    ->where('issued_check.transactionable_type', $that->getMorphMapKey(PaymentDetail::class));
            })
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Payment::class))),
                'payment.id AS reference_id',
                DB::raw("CAST(issued_check.transfer_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', supplier.full_name, '; %s : ',  issued_check.check_no) AS particular",
                        Lang::get('core::label.pay.to'),
                        Lang::get('core::label.check.no')
                    )
                ),
                DB::raw("0 AS 'in'"),
                "issued_check.amount AS out"
            )
            ->whereNull('payment.deleted_at')
            ->whereNull('payment_detail.deleted_at')
            ->whereNull('issued_check.deleted_at')
            ->where('payment.approval_status', ApprovalStatus::APPROVED)
            ->where('payment_detail.payment_method', PaymentMethod::CHECK)
            ->where('issued_check.status', CheckStatus::TRANSFERRED);

        if (!is_null($dateFrom)) {
            $builder = $builder->where('issued_check.transfer_date', '>=', $dateFrom->toDateString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('issued_check.transfer_date', '<=', $dateTo->toDateString());
        }

        if (!is_null($beginning)) {
            $builder = $builder->where('issued_check.transfer_date', '<', $beginning->toDateString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('payment.created_for', $branch);
        }

        if (!is_null($bankAccount)) {
            $builder = $builder->where('issued_check.bank_account_id', $bankAccount);
        }

        // Payment with bank deposit
        $query = Payment::getQuery()
            ->leftJoin('supplier', 'supplier.id', 'payment.supplier_id')
            ->leftJoin('payment_detail', 'payment_detail.transaction_id', 'payment.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Payment::class))),
                'payment.id as reference_id',
                DB::raw("CAST(payment.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(sprintf("CONCAT('%s : ', supplier.full_name) AS particular", Lang::get('core::label.pay.to'))),
                DB::raw("0 AS 'in'"),
                "payment_detail.amount AS 'out'"
            )
            ->whereNull('payment.deleted_at')
            ->whereNull('payment_detail.deleted_at')
            ->where('payment.approval_status', ApprovalStatus::APPROVED)
            ->where('payment_detail.payment_method', PaymentMethod::BANK_DEPOSIT);

        if (!is_null($dateFrom)) {
            $query = $query->where('payment.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('payment.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('payment.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('payment.created_for', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('payment_detail.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Other payment with check
        $query = OtherPayment::getQuery()
            ->leftJoin('issued_check', function($join) use ($that) {
                $join->on('other_payment.id', 'issued_check.transactionable_id')
                    ->where('issued_check.transactionable_type', $that->getMorphMapKey(OtherPayment::class));
            })
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherPayment::class))),
                'other_payment.id as reference_id',
                DB::raw("CAST(issued_check.transfer_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', other_payment.reference_name, '; %s : ',  issued_check.check_no) AS particular",
                        Lang::get('core::label.pay.to'),
                        Lang::get('core::label.check.no')
                    )
                ),
                DB::raw("0 AS 'in'"),
                "issued_check.amount AS 'out'"
            )
            ->whereNull('other_payment.deleted_at')
            ->whereNull('issued_check.deleted_at')
            ->where('other_payment.payment_method', PaymentMethod::CHECK)
            ->where('issued_check.status', CheckStatus::TRANSFERRED);

        if (!is_null($dateFrom)) {
            $query = $query->where('issued_check.transfer_date', '>=', $dateFrom->toDateString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('issued_check.transfer_date', '<=', $dateTo->toDateString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('issued_check.transfer_date', '<', $beginning->toDateString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_payment.created_from', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('issued_check.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Other Payment with bank deposit
        $query = OtherPayment::getQuery()
            ->leftJoin('other_payment_detail', 'other_payment_detail.transaction_id', 'other_payment.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherPayment::class))),
                'other_payment.id as reference_id',
                DB::raw("CAST(other_payment.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(sprintf("CONCAT('%s : ', other_payment.reference_name) AS particular", Lang::get('core::label.pay.to'))),
                DB::raw("0 AS 'in'"),
                DB::raw("SUM(other_payment_detail.amount) AS 'out'")
            )
            ->whereNull('other_payment.deleted_at')
            ->where('other_payment.payment_method', PaymentMethod::BANK_DEPOSIT)
            ->groupBy(
                'other_payment.id',
                DB::raw('CAST(other_payment.transaction_date AS DATE)'),
                'other_payment.reference_name'
            );

        if (!is_null($dateFrom)) {
            $query = $query->where('other_payment.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('other_payment.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('other_payment.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_payment.created_from', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('other_payment.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Collection with check method
        $query = Collection::getQuery()
            ->leftJoin('customer', 'customer.id', 'collection.customer_id')
            ->leftJoin('collection_detail', 'collection.id', 'collection_detail.transaction_id')
            ->leftJoin('received_check', function($join) use ($that) {
                $join->on('collection_detail.id', 'received_check.transactionable_id')
                    ->where('received_check.transactionable_type', $that->getMorphMapKey(CollectionDetail::class));
            })
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Collection::class))),
                'collection.id AS reference_id',
                DB::raw("CAST(received_check.transfer_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ',
                            CASE
                                WHEN collection.customer_type = %s THEN customer.name
                                ELSE collection.customer_walk_in_name
                            END, '; %s : ',  received_check.check_no
                        ) AS particular",
                        Lang::get('core::label.received.from'),
                        Customer::REGULAR,
                        Lang::get('core::label.check.no')
                    )
                ),
                "received_check.amount AS 'in'",
                DB::raw("0 AS 'out'")
            )
            ->whereNull('collection.deleted_at')
            ->whereNull('collection_detail.deleted_at')
            ->whereNull('received_check.deleted_at')
            ->where('collection.approval_status', ApprovalStatus::APPROVED)
            ->where('collection_detail.payment_method', PaymentMethod::CHECK)
            ->where('received_check.status', CheckStatus::TRANSFERRED);

        if (!is_null($dateFrom)) {
            $query = $query->where('received_check.transfer_date', '>=', $dateFrom->toDateString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('received_check.transfer_date', '<=', $dateTo->toDateString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('received_check.transfer_date', '<', $beginning->toDateString());
        }

        if (!is_null($branch)) {
            $query = $query->where('collection.created_for', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('received_check.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Collection with bank deposit
        $query = Collection::getQuery()
            ->leftJoin('customer', 'customer.id', 'collection.customer_id')
            ->leftJoin('collection_detail', 'collection_detail.transaction_id', 'collection.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(Collection::class))),
                'collection.id as reference_id',
                DB::raw("CAST(collection.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ',
                            CASE
                                WHEN collection.customer_type = %s THEN customer.name
                                ELSE collection.customer_walk_in_name
                            END
                        ) AS particular",
                        Lang::get('core::label.received.from'),
                        Customer::REGULAR
                    )
                ),
                "collection_detail.amount AS 'in'",
                DB::raw("0 AS 'out'")
            )
            ->whereNull('collection.deleted_at')
            ->whereNull('collection_detail.deleted_at')
            ->where('collection.approval_status', ApprovalStatus::APPROVED)
            ->where('collection_detail.payment_method', PaymentMethod::BANK_DEPOSIT);

        if (!is_null($dateFrom)) {
            $query = $query->where('collection.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('collection.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('collection.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('collection.created_for', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('collection_detail.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Other Income with check
        $query = OtherIncome::getQuery()
            ->leftJoin('received_check', function($join) use ($that) {
                $join->on('other_income.id', 'received_check.transactionable_id')
                    ->where('received_check.transactionable_type', $that->getMorphMapKey(OtherIncome::class));
            })
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherIncome::class))),
                'other_income.id as reference_id',
                DB::raw("CAST(received_check.transfer_date AS DATE) AS 'check_date'"),
                DB::raw(
                    sprintf(
                        "CONCAT('%s : ', other_income.reference_name, '; %s : ',  received_check.check_no) AS particular",
                        Lang::get('core::label.received.from'),
                        Lang::get('core::label.check.no')
                    )
                ),
                "received_check.amount AS 'in'",
                DB::raw("0 AS 'out'")
            )
            ->whereNull('other_income.deleted_at')
            ->whereNull('received_check.deleted_at')
            ->where('other_income.payment_method', PaymentMethod::CHECK)
            ->where('received_check.status', CheckStatus::TRANSFERRED);

        if (!is_null($dateFrom)) {
            $query = $query->where('received_check.transfer_date', '>=', $dateFrom->toDateString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('received_check.transfer_date', '<=', $dateTo->toDateString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('received_check.transfer_date', '<', $beginning->toDateString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_income.created_from', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('received_check.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        // Other Income with bank deposit
        $query = OtherIncome::getQuery()
            ->leftJoin('other_income_detail', 'other_income_detail.transaction_id', 'other_income.id')
            ->select(
                DB::raw(sprintf("'%s' as type", $this->getMorphMapKey(OtherIncome::class))),
                'other_income.id as reference_id',
                DB::raw("CAST(other_income.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(sprintf("CONCAT('%s : ', other_income.reference_name) AS particular", Lang::get('core::label.received.from'))),
                DB::raw("SUM(other_income_detail.amount) AS 'in'"),
                DB::raw("0 AS 'out'")
            )
            ->whereNull('other_income.deleted_at')
            ->where('other_income.payment_method', PaymentMethod::BANK_DEPOSIT)
            ->groupBy(
                'other_income.id',
                DB::raw('CAST(other_income.transaction_date AS DATE)'),
                'other_income.reference_name'
            );

        if (!is_null($dateFrom)) {
            $query = $query->where('other_income.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('other_income.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('other_income.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('other_income.created_from', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where('other_income.bank_account_id', $bankAccount);
        }

        $builder->unionAll($query);

        //Bank Transaction Module
        $query = BankTransaction::getQuery()
            ->select(
                DB::raw(sprintf("'%s' as type", Lang::get('core::label.bank.transaction'))),
                'bank_transaction.id as reference_id',
                DB::raw("CAST(bank_transaction.transaction_date AS DATE) AS 'check_date'"),
                DB::raw(sprintf("CASE 
                    WHEN bank_transaction.type = 0
                        THEN '%s'
                    WHEN bank_transaction.type = 1
                        THEN '%s'
                    WHEN bank_transaction.type = 2
                        THEN '%s'
                    END AS particular", 
                    Lang::get('core::label.deposit'),
                    Lang::get('core::label.withdrawal'),
                    Lang::get('core::label.transfer')
                )),
                DB::raw(sprintf("CASE 
                    WHEN bank_transaction.type = 0
                        THEN bank_transaction.amount
                    WHEN bank_transaction.type = 2 AND bank_transaction.to_bank = %s
                        THEN bank_transaction.amount
                    ELSE 0
                    END AS 'in'
                ", $bankAccount ?? 0)),
                DB::raw(sprintf("CASE 
                    WHEN bank_transaction.type = 1
                        THEN bank_transaction.amount
                    WHEN bank_transaction.type = 2 AND bank_transaction.from_bank = %s
                        THEN bank_transaction.amount
                    ELSE 0
                    END AS 'out'
                ", $bankAccount ?? 0))
            )
            ->whereNull('bank_transaction.deleted_at')
            ->groupBy(
                'bank_transaction.id',
                DB::raw('CAST(bank_transaction.transaction_date AS DATE)'),
                'bank_transaction.amount',
                'bank_transaction.type',
                'bank_transaction.to_bank',
                'bank_transaction.from_bank'
            );

        if (!is_null($dateFrom)) {
            $query = $query->where('bank_transaction.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $query = $query->where('bank_transaction.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($beginning)) {
            $query = $query->where('bank_transaction.transaction_date', '<', $beginning->toDateTimeString());
        }

        if (!is_null($branch)) {
            $query = $query->where('bank_transaction.branch_id', $branch);
        }

        if (!is_null($bankAccount)) {
            $query = $query->where(function($query) use ($bankAccount){
                $query->where('bank_transaction.from_bank', $bankAccount)
                    ->orWhere('bank_transaction.to_bank', $bankAccount);
            });
        }

        $builder->unionAll($query);

        return $builder;
    }
}
