<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\SalesReturnRepository;
use Modules\Core\Entities\SalesReturn;

/**
 * Class SalesReturnRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SalesReturnRepositoryEloquent extends InvoiceRepositoryEloquent implements SalesReturnRepository
{
    public function model()
    {
        return SalesReturn::class; 
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['salesman_id', 'customer_type', 'customer_id', 'customer_detail_id']);
    }
}