<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Repositories\Builder\Concrete\CustomReportSelectBuilder;
use Modules\Core\Repositories\Builder\Concrete\CustomReportSearchBuilder;

use DB;
use App;

/**
 * Class StockDeliveryInboundRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockDeliveryInboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements StockDeliveryInboundRepository
{
    public function model()
    {
        return StockDeliveryInbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['delivery_from', 'delivery_from_location']);
    }
}