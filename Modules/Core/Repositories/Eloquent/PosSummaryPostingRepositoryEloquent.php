<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\PosSummaryPosting;
use Modules\Core\Repositories\Contracts\PosSummaryPostingRepository;
use Modules\Core\Repositories\Criteria\POSSummaryPostingOwnBranchTerminal;

class PosSummaryPostingRepositoryEloquent extends RepositoryEloquent implements PosSummaryPostingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PosSummaryPosting::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(POSSummaryPostingOwnBranchTerminal::class));
    }
}