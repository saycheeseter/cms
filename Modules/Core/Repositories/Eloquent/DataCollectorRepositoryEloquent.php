<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\DataCollectorRepository;
use Modules\Core\Entities\DataCollectorTransaction;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

/**
 * Class DataCollectorRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class DataCollectorRepositoryEloquent extends RepositoryEloquent implements DataCollectorRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'import_date',
        'transaction_type',
        'filename',
        'source'
    ];

    public function model()
    {
        return DataCollectorTransaction::class;
    }

    /**
     * Find transaction by the given reference number
     *
     * @param $reference
     * @param null $transactionType
     * @return mixed
     */
    public function findByReference($reference, $transactionType = NULL)
    {
        $model = $this->model->where('reference_no', $reference);

        if(!is_null($transactionType)) {
            $model = $model->where('transaction_type', $transactionType);
        }

        $data = $model->first();

        $this->resetModel();

        return $this->parserResult($data);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {   
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}