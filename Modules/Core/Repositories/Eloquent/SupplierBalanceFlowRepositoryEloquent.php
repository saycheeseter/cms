<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\SupplierBalanceFlowRepository;
use Modules\Core\Entities\SupplierBalanceFlow;

use DB;

/**
 * Class SupplierBalanceFlowRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SupplierBalanceFlowRepositoryEloquent extends BalanceFlowRepositoryEloquent implements SupplierBalanceFlowRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'supplier.full_name',
        'flow_type',
        'transaction_date'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SupplierBalanceFlow::class;
    }

    /**
     * Get all the record of supplier balance flow by filters
     *
     * @return mixed
     */
    public function search()
    {
        $this->model = $this->model
            ->with(['supplier'])
            ->join('supplier', 'supplier_balance_flow.supplier_id', '=', 'supplier.id');

        return $this->all();
    }

    /**
     * Get the beginning value of the specified data by removing the transasction date filter
     * 
     * @param  string $date
     * @return string
     */
    public function getBeginning($date)
    {
        $fieldSearchableBuffer = $this->fieldSearchable;

        unset($this->fieldSearchable[2]);

        $query = $this->applyCriteria()
            ->model
            ->join('supplier', 'supplier_balance_flow.supplier_id', '=', 'supplier.id')
            ->where('transaction_date', '<', $date)
            ->select(
                DB::raw(
                    'SUM(
                        CASE flow_type
                            WHEN 0 THEN amount
                            ELSE (-1 * amount)
                        END
                    ) AS beginning'
                )
            );

        $this->fieldSearchable = $fieldSearchableBuffer;

        $result = $query->first();

        $this->resetModel();

        return $result;
    }

    /**
     * Get the balance of the specified supplier
     *
     * @param $id
     * @return SupplierBalanceFlow
     */
    public function getBalance($id) 
    {
        $result = $this->model
            ->where('supplier_id', $id)
            ->select(
                DB::raw(
                    'SUM(
                        CASE flow_type
                            WHEN 0 THEN amount
                            ELSE (-1 * amount)
                        END
                    ) AS balance'
                )
            )
            ->first();

        $this->resetModel();

        return $result;
    }
}
