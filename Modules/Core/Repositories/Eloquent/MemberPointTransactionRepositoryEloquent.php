<?php

namespace Modules\Core\Repositories\Eloquent;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\MemberPointTransactionRepository;
use Modules\Core\Entities\MemberPointTransaction;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class MemberPointTransactionRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class MemberPointTransactionRepositoryEloquent extends RepositoryEloquent implements MemberPointTransactionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'member_id',
        'transaction_date'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MemberPointTransaction::class;
    }

    /**
     * Get the sum of points for the indicated member
     * @param $memberId
     */
    public function points($memberId)
    {
        $points = $this->model
            ->where('member_id', $memberId)
            ->sum('amount');

        $this->resetModel();

        return Decimal::create(app('NumberFormatter')->parse($points), setting('monetary.precision'))->innerValue();
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
