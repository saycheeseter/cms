<?php 

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\ProductStockCardRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Eloquent\BranchDetailRepositoryEloquent;
use Modules\Core\Entities\ProductStockCard;
use Illuminate\Support\Str;
use Modules\Core\Entities\Branch;
use App;

/**
 * Class ProductStockCardRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductStockCardRepositoryEloquent extends RepositoryEloquent implements ProductStockCardRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_for',
        'for_location',
        'transaction_date'
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductStockCard::class;
    }

    /**
     * Get the specified product with their corresponding stock card info
     * 
     * @param  int $id
     * @param  int $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search($id, $limit = null)
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 10) : $limit;

        $results = $this->applyCriteria()
            ->model
            ->with(['product', 'branch', 'location'])
            ->where('product_id', $id)
            ->orderBy('created_at', 'desc')
            ->paginate($limit)
            ->appends(app('request')->query());

        $this->resetModel();

        return $results;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
