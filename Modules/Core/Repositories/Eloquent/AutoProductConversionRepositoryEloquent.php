<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\PosSalesDetail;
use Modules\Core\Repositories\Contracts\AutoProductConversionRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\SalesOutboundDetail;
use Modules\Core\Entities\SalesReturnInboundDetail;

use Modules\Core\Traits\HasMorphMap;

/**
 * Class AutoProductConversionRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class AutoProductConversionRepositoryEloquent extends RepositoryEloquent implements AutoProductConversionRepository
{
    use HasMorphMap;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_for',
        'transaction_date',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AutoProductConversion::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }

    public function findProductInInfoTransactions($id, $type, $limit = null)
    {
        $this->applyCriteria();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        $transactionType = [];

        switch($type) {
            case  1:
                $transactionType = [
                    $this->getMorphMapKey(SalesOutboundDetail::class),
                    $this->getMorphMapKey(PosSalesDetail::class),
                ];
                break;

            case 2:
                $transactionType = [$this->getMorphMapKey(SalesReturnInboundDetail::class)];
                break;
        }

        $results = $this->model->where('product_id', $id)
            ->whereIn('transaction_type', $transactionType)
            ->orderBy('transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();
        
        return $results;
    }

    public function findProductInDetailTransactions($id, $type, $limit = null)
    {
        $that = $this;
        $model = $this->model->details()->getRelated();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        $transactionType = [];

        switch($type) {
            case  1:
                $transactionType = [$this->getMorphMapKey(SalesReturnInboundDetail::class)];
                break;

            case 2:
                $transactionType = [
                    $this->getMorphMapKey(SalesOutboundDetail::class),
                    $this->getMorphMapKey(PosSalesDetail::class),
                ];
        }

        //\DB::enableQueryLog();

        $results = $model
            ->select('auto_product_conversion_detail.*')
            ->with(['transaction.transaction.transaction', 'transaction.creator'])
            ->where('auto_product_conversion_detail.product_id', $id)
            ->whereHas('transaction', function($query) use($that, $transactionType){
                $query = $that->getCriteria()[0]->apply($query, $that);
                $query->whereIn('transaction_type', $transactionType);
            })
            ->join('auto_product_conversion', 'auto_product_conversion.id', 'auto_product_conversion_detail.transaction_id')
            ->orderBy('auto_product_conversion.transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();
        
        return $results;
    }
}