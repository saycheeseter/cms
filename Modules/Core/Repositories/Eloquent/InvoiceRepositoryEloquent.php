<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;

/**
 * Class InvoiceRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
abstract class InvoiceRepositoryEloquent extends RepositoryEloquent
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_from',
        'created_for',
        'transaction_status',
        'for_location',
        'requested_by'
    ];

    /**
     * Find transaction by the given reference number
     *
     * @param  string $reference
     * @param  int $id
     * @return Model
     */
    public function findByReference($reference, $id = 0)
    {
        $builder = $this->model->where('sheet_number', $reference);

        if ($id !== 0) {
            $builder = $builder->where('id', '<>', $id);
        }

        $model = $builder->get()->first();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Find remaining list of invoices based on filters
     *
     * @param  string $sheetNumber
     * @return Model
     */
    public function findRemaining($branch, $sheetNumber = '')
    {
        $model = $this->model
            ->where('created_for', $branch)
            ->where('sheet_number', 'LIKE', sprintf("%%%s%%", $sheetNumber))
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->whereIn('transaction_status', array(TransactionStatus::INCOMPLETE, TransactionStatus::NO_DELIVERY))
            ->orderBy('transaction_date', 'desc')
            ->limit(10)
            ->get();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Retrieve invoice details whose remaining qty
     * is > 0
     *
     * @param  string $id
     * @return array
     */
    public function retrieve($id)
    {
        $model = $this->model
            ->with(array(
                'details' => function($query) {
                    $query->where('remaining_qty', '>', 0);
                },
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.components'
            ))
            ->find($id);

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
