<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\BranchDetail;
use Modules\Core\Entities\DamageOutboundDetail;
use Modules\Core\Entities\Hydration\AvailableProductSerialNumber;
use Modules\Core\Entities\Hydration\AvailableProductSerialNumberExport;
use Modules\Core\Entities\InventoryAdjustDetail;
use Modules\Core\Entities\PurchaseInboundDetail;
use Modules\Core\Entities\PurchaseReturnOutboundDetail;
use Modules\Core\Entities\SalesOutboundDetail;
use Modules\Core\Entities\SalesReturnInboundDetail;
use Modules\Core\Entities\StockDeliveryInboundDetail;
use Modules\Core\Entities\StockDeliveryOutboundDetail;
use Modules\Core\Entities\StockReturnInboundDetail;
use Modules\Core\Entities\StockReturnOutboundDetail;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Repositories\Contracts\ProductSerialNumberRepository;
use Modules\Core\Entities\ProductSerialNumber;
use Modules\Core\Entities\Customer;
use Modules\Core\Enums\ItemManagementOwner;

/**
 * Class ProductSerialNumberRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductSerialNumberRepositoryEloquent extends RepositoryEloquent implements ProductSerialNumberRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductSerialNumber::class;
    }

    /**
     * Get the list of serials by the indicated product id and apply if there are any constraint or
     * additional condition to the query (e.g. owner type)
     *
     * @param  int $id
     * @param  int|null $constraint
     * @param  int|null $entity
     * @param  int|null $status
     * @param  int|null $transaction
     * @param  int|null $supplier
     * @return Collection
     */
    public function search($id, $constraint = null, $entity = null, $status = null, $transaction = null, $supplier = null)
    {
        $builder = $this->model
            ->where('product_id', $id)
            ->where('status', $status);

        switch ($constraint) {
            case ItemManagementOwner::SUPPLIER:
                $builder = $builder->where('supplier_id', $supplier)
                    ->where('owner_type', $this->getMorphMapKey(BranchDetail::class))
                    ->where('owner_id', $entity);
                break;
            
            case ItemManagementOwner::CUSTOMER:
                $builder = $builder
                    ->where('owner_type', $this->getMorphMapKey(Customer::class))
                    ->where('owner_id', empty($entity) ? null : $entity);
                break;

            case ItemManagementOwner::BRANCH_DETAIL:
                $builder = $builder
                    ->where('owner_type', $this->getMorphMapKey(BranchDetail::class))
                    ->where('owner_id', $entity);
                break;
        }


        if (!is_null($transaction) && !empty($transaction)) {
            $builder = $builder->where('transaction', $transaction);
        }

        $results = $builder->get();

        $this->resetModel();

        return $this->parserResult($results);
    }

    /**
     * Get the list of available serials for the indicated product id
     *
     * @param $productId
     * @param array $filters
     * @param bool $paging
     * @return mixed
     */
    public function getAvailableSerials($productId, $filters = [], $paging = true)
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('branch_detail', 'branch_detail.id', 'product_serial_number.owner_id')
            ->leftJoin('branch', 'branch.id', 'branch_detail.branch_id')
            ->leftJoin('supplier', 'supplier.id', 'product_serial_number.supplier_id')
            ->select(
                'branch.name as branch',
                'supplier.full_name as supplier_name',
                'branch_detail.name as location',
                'product_serial_number.id',
                'product_serial_number.serial_number',
                'product_serial_number.expiration_date',
                'product_serial_number.manufacturing_date',
                'product_serial_number.admission_date',
                'product_serial_number.manufacturer_warranty_start',
                'product_serial_number.manufacturer_warranty_end',
                'product_serial_number.remarks'
            )
            ->where('product_serial_number.owner_type', $this->getMorphMapKey(BranchDetail::class))
            ->where('product_serial_number.product_id', $productId)
            ->where('product_serial_number.status', SerialStatus::AVAILABLE);

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'created_for',
                'for_location',
                'supplier_id',
            ],
            [
                'created_for' => function ($query, $filter, $method, $alias) {
                    return $query->$method('branch.id', $filter->operator, $filter->value);
                },
                'for_location' => function ($query, $filter, $method, $alias) {
                    return $query->$method('owner_id', $filter->operator, $filter->value);
                }
            ]
        );

        $result = null;

        if ($paging) {
            $result = $this->createPaginationFromQuery($builder);
            $result = $this->hydratePaginationItems($result, new AvailableProductSerialNumber);
        } else {
            $result = AvailableProductSerialNumber::hydrate(
                $builder->get()->toArray()
            );
        }

        $this->resetModel();

        return $result;
    }

    /**
     * Get the list of transactions where the indicated serial number is used (only approved transactions
     * will be shown)
     *
     * @param $id
     * @return static
     */
    public function getTransactionReport($id)
    {
        $serial = $this->findOrFail($id);

        $relationships = [
            PurchaseInboundDetail::class,
            PurchaseReturnOutboundDetail::class,
            DamageOutboundDetail::class,
            SalesOutboundDetail::class,
            SalesReturnInboundDetail::class,
            StockDeliveryOutboundDetail::class,
            StockDeliveryInboundDetail::class,
            StockReturnOutboundDetail::class,
            StockReturnInboundDetail::class,
            InventoryAdjustDetail::class
        ];

        $collection = collect([]);

        foreach ($relationships as $relationship) {
            $result = $serial->transactionDetails($relationship)
                ->with(['transaction.for', 'transaction.location'])
                ->whereHas('transaction', function($query) {
                    $query->where('approval_status', ApprovalStatus::APPROVED);
                })
                ->get();

            $collection = $collection->concat($result->pluck('transaction'));
        }

        $result = $collection->sortBy('transaction_date');

        $this->resetModel();

        return $result;
    }

    /**
     * Find the serial number by the indicated number filter
     *
     * @param $number
     * @return mixed
     */
    public function findByNumber($number)
    {
        return $this->with(['product.barcodes'])
            ->findWhere([
                'serial_number' => $number,
                'owner_type' => [
                    'owner_type',
                    '<>',
                    null
                ],
            ])
            ->first();
    }
}
