<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\IssuedCheckRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

use Modules\Core\Entities\IssuedCheck;

class IssuedCheckRepositoryEloquent extends RepositoryEloquent implements IssuedCheckRepository
{
    protected $fieldSearchable = [
        'created_from',
        'bank_account_id',
        'check_no',
        'check_date',
        'transaction_date',
        'transfer_date',
        'remarks',
        'status'
    ];

    public function model()
    {
        return IssuedCheck::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}