<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\SystemCodeRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Entities\SystemCode;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class SystemCodeRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
abstract class SystemCodeRepositoryEloquent extends RepositoryEloquent implements SystemCodeRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'remarks',
        'updated_at',
        'deleted_at'
    ];

    public function model()
    {
        return SystemCode::class; 
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
