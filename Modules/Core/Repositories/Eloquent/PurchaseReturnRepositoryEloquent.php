<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PurchaseReturnRepository;
use Modules\Core\Entities\PurchaseReturn;

/**
 * Class PurchaseReturnRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PurchaseReturnRepositoryEloquent extends InvoiceRepositoryEloquent implements PurchaseReturnRepository
{
    public function model()
    {
        return PurchaseReturn::class; 
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['supplier_id']);
    }
}