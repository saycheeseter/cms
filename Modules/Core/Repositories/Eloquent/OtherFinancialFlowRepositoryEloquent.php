<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Abstract Class OtherFinancialFlowRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
abstract class OtherFinancialFlowRepositoryEloquent extends RepositoryEloquent
{
     /**
     * @var array
     */
    protected $fieldSearchable = [
        'reference_name',
        'transaction_date',
        'payment_method',
        'remarks',
        'created_from'
    ];

    public function getSummary($filters = [])
    {
        $tables = [
            'base' => $this->model->getTable(),
            'detail' => $this->model->details()->getRelated()->getTable()
        ];

        $builder = $this->model
            ->getQuery()
            ->join($tables['detail'], $tables['base'].'.id', '=', $tables['detail'].'.transaction_id')
            ->selectRaw('COALESCE(SUM('.$tables['detail'].'.amount), 0) as total_amount')
            ->whereNull('deleted_at');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'year_month',
                'created_for'
            ],
            [
                'created_for' => function ($query, $filter, $method, $alias) {
                    $query->$method('created_from', $filter->operator, $filter->value);
                },
                'year_month' => function ($query, $filter, $method, $alias) {
                    $date = Carbon::parse($filter->value);

                    if (in_array($filter->operator, ['=', '<>'])) {
                        $method = $filter->operator === '='
                            ? $method.'Between'
                            : $method.'NotBetween';

                        $query->$method('transaction_date', [
                            $date->startOfMonth()->toDateTimeString(),
                            $date->endOfMonth()->toDateTimeString()
                        ]);
                    } else {
                        $value = '';

                        switch ($filter->operator) {
                            case '<':
                            case '>=':
                                $value = $date->startOfMonth()->toDateTimeString();
                                break;

                            case '>':
                            case '<=':
                                $value = $date->endOfMonth()->toDateTimeString();
                                break;
                        }

                        $query->$method('transaction_date', $filter->operator, $value);
                    }
                }
            ]
        );

        $this->resetModel();

        return $builder->first();
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}