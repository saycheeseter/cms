<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockReturnInboundRepository;
use Modules\Core\Entities\StockReturnInbound;

/**
 * Class StockReturnInboundRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockReturnInboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements StockReturnInboundRepository
{
    public function model()
    {
        return StockReturnInbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['delivery_from', 'delivery_from_location']);
    }
}
