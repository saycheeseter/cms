<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\PrintoutTemplateRepository;
use Modules\Core\Entities\PrintoutTemplate;

/**
 * Class PrintoutTemplateRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PrintoutTemplateRepositoryEloquent extends RepositoryEloquent implements PrintoutTemplateRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'module_id'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PrintoutTemplate::class;
    }

    /**
     * Find data by module_id
     * 
     * @param  int $module
     * @return PrintoutTempllate
     */
    public function findByModule($module)
    {
        return $this->findByField('module_id', $module);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
