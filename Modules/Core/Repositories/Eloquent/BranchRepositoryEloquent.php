<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\BranchRepository;
use Modules\Core\Entities\Branch;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class BranchRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BranchRepositoryEloquent extends RepositoryEloquent implements BranchRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'business_name',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Branch::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
