<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockReturnOutboundRepository;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;

/**
 * Class DamageRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockReturnOutboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements StockReturnOutboundRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_from',
        'deliver_to',
        'created_for',
        'for_location',
        'transaction_status',
        'deliver_to_location',
        'requested_by'
    ];

    public function model()
    {
        return StockReturnOutbound::class;
    }

    /**
     * Find remaining list of invoices based on filters
     *
     * @param  string $sheetNumber
     * @return Model
     */
    public function findRemaining($branch, $sheetNumber = '')
    {
        $model = $this->model
            ->where('deliver_to', $branch)
            ->where('sheet_number', 'LIKE', sprintf("%%%s%%", $sheetNumber))
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->whereIn('transaction_status', array(TransactionStatus::INCOMPLETE, TransactionStatus::NO_DELIVERY))
            ->orderBy('transaction_date', 'desc')
            ->limit(10)
            ->get();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Retrieve invoice details whose remaining qty
     * is > 0
     *
     * @param  string $id
     * @return array
     */
    public function retrieve($id)
    {
        $model = $this->model
            ->with(array(
                'details' => function($query) {
                    $query->where('remaining_qty', '>', 0);
                },
                'details.product',
                'details.barcodes',
                'details.units.uom',
                'details.components'
            ))
            ->find($id);

        $this->resetModel();

        return $this->parserResult($model);
    }
}
