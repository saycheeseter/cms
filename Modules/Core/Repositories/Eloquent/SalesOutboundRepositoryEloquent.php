<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Hydration\CollectionDueWarning;
use Modules\Core\Entities\Hydration\SalesmanReportTransactions;
use Modules\Core\Entities\Hydration\SalesmanReportTransactionsProducts;
use Modules\Core\Entities\Hydration\SalesOutboundNegativeProfitProduct;
use Modules\Core\Repositories\Contracts\SalesOutboundRepository;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Enums\Customer;
use Modules\Core\Entities\Hydration\CollectionDueWarningSummary;
use Modules\Core\Entities\Hydration\DetailSalesReport;
use Modules\Core\Entities\Hydration\DetailSalesReportSummary;

/**
 * Class SalesRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SalesOutboundRepositoryEloquent extends InventoryChainRepositoryEloquent implements SalesOutboundRepository
{   
    public function model()
    {
        return SalesOutbound::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['salesman_id', 'customer_type', 'customer_id', 'customer_detail_id']);
    }

    public function getTransactionsBySalesmanId($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('user', 'user.id', '=', 'sales_outbound.salesman_id')
            ->leftJoin('customer', 'customer.id', '=', 'sales_outbound.customer_id')
            ->leftJoin('customer_detail', 'customer_detail.id', '=', 'sales_outbound.customer_detail_id')
            ->leftJoin('branch', 'branch.id', '=', 'sales_outbound.created_for')
            ->select(
                'branch.name as branch_name',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                DB::raw("
                    CASE 
                        WHEN sales_outbound.customer_type = ".Customer::WALK_IN." THEN
                            sales_outbound.customer_walk_in_name
                        ELSE
                            CONCAT(customer.name, ' - ', customer_detail.name)
                    END AS customer_name
                "),
                'user.full_name as salesman_full_name',
                DB::raw('SUM(sales_outbound.total_amount) as transaction_total_amount')
            )
            ->groupBy(
                'sales_outbound.id',
                'branch.name',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'user.full_name',
                'customer.name',
                'customer_detail.name',
                'sales_outbound.customer_walk_in_name',
                'sales_outbound.customer_type'
            )
            ->whereNull('sales_outbound.deleted_at')
            ->where('sales_outbound.approval_status', ApprovalStatus::APPROVED)
            ->orderBy('transaction_date', 'desc');

         $builder = $this->setConditionsOnBuilder(
             $builder,
             $filters,
             'sales_outbound',
             [
                 'created_for',
                 'for_location',
                 'salesman_id',
                 'transaction_date'
             ]);

         $paginator = $this->createPaginationFromQuery($builder);
         $paginator = $this->hydratePaginationItems($paginator, new SalesmanReportTransactions);

         $this->resetModel();

        return $paginator;
    }

    public function getTransactionProductsBySalesmanId($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('sales_outbound_detail', 'sales_outbound_detail.transaction_id', '=', 'sales_outbound.id')
            ->leftJoin('product', 'product.id', '=', 'sales_outbound_detail.product_id')
            ->leftJoin('product_barcode', 'product_barcode.product_id', '=', 'product.id')
            ->select(
                DB::raw('dbo.GROUP_CONCAT(product_barcode.code) as product_barcode'),
                'product.name as product_name',
                'product.chinese_name as product_chinese_name',
                'sales_outbound_detail.qty',
                'sales_outbound_detail.unit_qty',
                'sales_outbound_detail.price',
                'product.id as product_id'
            )
            ->groupBy(
                'sales_outbound_detail.id',
                'sales_outbound_detail.qty',
                'sales_outbound_detail.unit_qty',
                'sales_outbound_detail.price',
                'product.id',
                'product.name',
                'product.chinese_name'
            )
            ->where('sales_outbound.approval_status', ApprovalStatus::APPROVED)
            ->whereNull('sales_outbound.deleted_at')
            ->whereNull('sales_outbound_detail.deleted_at');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            'sales_outbound',
            [
                'created_for',
                'for_location',
                'salesman_id',
                'transaction_date'
            ]);

        $builder = $this->createSubQuery($builder)
            ->select(
                'product_barcode',
                'product_name',
                'product_chinese_name',
                DB::raw('SUM(qty) as total_qty'),
                DB::raw('MIN(price) as minimum_price'),
                DB::raw('AVG(price) as average_price'),
                DB::raw('MAX(price) as maximum_price'),
                DB::raw('SUM(qty * unit_qty * price) as total_amount')
            )
            ->groupBy(
                'product_id',
                'product_barcode',
                'product_name',
                'product_chinese_name'
            )
            ->orderBy('total_amount', 'desc')
            ->orderBy('product_name', 'desc');

        $result = $builder->get();

        $hydrated = SalesmanReportTransactionsProducts::hydrate($result->toArray());

        $this->resetModel();

        return $hydrated;
    }

    public function transactionsWithBalance($branchId, $customer, $customerType, $limit = null)
    {
        $builder = $this
            ->model
            ->with([
                'customer',
                'salesman',
                'for',
                'creator',
                'auditor',
                'from',
                'requested',
                'location',
                'reference'
            ])
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('customer_type', $customerType)
            ->where('remaining_amount', '>', 0);

        if(!setting('collection.display.transactions.of.other.branches')) {
            $builder = $builder->where('created_for', $branchId);
        }

        if($customerType == Customer::WALK_IN) {
            $builder = $builder->where('customer_walk_in_name', $customer);
        } else if($customerType == Customer::REGULAR) {
            $builder = $builder->where('customer_id', $customer);
        }

        $results = $builder->paginate($limit);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function getDueWarning($filters = [], $all = false)
    {
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];
        $customer = !isset($filters['customer']) || empty($filters['customer']) ? null : $filters['customer'];

        $builder = $this->model
            ->getQuery()
            ->whereNull('sales_outbound.deleted_at')
            ->where([
                ['sales_outbound.remaining_amount', '>', '0'],
                ['sales_outbound.approval_status', '=', ApprovalStatus::APPROVED]
            ]);

        if (!is_null($dateFrom)) {
            $builder = $builder->where('sales_outbound.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('sales_outbound.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('sales_outbound.created_for', $branch);
        }

        if (!is_null($customer) && $customer > 0) {
            $builder = $builder->where('sales_outbound.customer_id', $customer);
        }

        $builder = ($customer < 0)
            ? $builder->whereNull('customer_id')->where('customer_type',Customer::WALK_IN)
            : $builder->leftjoin('customer', 'customer.id', '=', 'sales_outbound.customer_id');

        $builder->select(
            'sales_outbound.id',
            'sales_outbound.transaction_date',
            DB::raw(
                $customer < 0 ? 'sales_outbound.customer_walk_in_name AS customer'
                : sprintf(
                    "CASE
                        WHEN sales_outbound.customer_type = %d
                        THEN customer.name
                        ELSE sales_outbound.customer_walk_in_name
                    END AS customer",
                    Customer::REGULAR
                )
            ),
            'sales_outbound.sheet_number',
            'sales_outbound.total_amount',
            'sales_outbound.remaining_amount',
            'sales_outbound.term',
            'sales_outbound.remarks',
            'sales_outbound.due_date'
        );

        if(!filter_var($all, FILTER_VALIDATE_BOOLEAN)) {
            $builder = $builder->where('sales_outbound.due_date', '<', Carbon::now());
        }

        $paginator = $this->createPaginationFromQuery($builder->orderBy('transaction_date'));
        $paginator = $this->hydratePaginationItems($paginator, new CollectionDueWarning());

        $this->resetModel();

        return $paginator;
    }

    public function getDueWarningSummary($filters = [], $all = false)
    {
        $dateFrom = !isset($filters['date_from']) || empty($filters['date_from'])  ? null : Carbon::parse($filters['date_from']);
        $dateTo = !isset($filters['date_to']) || empty($filters['date_to']) ? null : Carbon::parse($filters['date_to']);
        $branch = !isset($filters['branch']) || empty($filters['branch']) ? null : $filters['branch'];
        $customer = !isset($filters['customer']) || empty($filters['customer']) ? null : $filters['customer'];

        $builder = $this->model
            ->getQuery()
            ->whereNull('sales_outbound.deleted_at')
            ->where([
                ['sales_outbound.remaining_amount', '>', '0'],
                ['sales_outbound.approval_status', '=', ApprovalStatus::APPROVED]
            ]);

        if (!is_null($dateFrom)) {
            $builder = $builder->where('sales_outbound.transaction_date', '>=', $dateFrom->toDateTimeString());
        }

        if (!is_null($dateTo)) {
            $builder = $builder->where('sales_outbound.transaction_date', '<=', $dateTo->toDateTimeString());
        }

        if (!is_null($branch)) {
            $builder = $builder->where('sales_outbound.created_for', $branch);
        }

        if (!is_null($customer) && $customer > 0) {
            $builder = $builder->where('sales_outbound.customer_id', $customer);
        }

        $builder = ($customer < 0)
            ? $builder->whereNull('customer_id')->where('customer_type',Customer::WALK_IN)
            : $builder->leftjoin('customer', 'customer.id', '=', 'sales_outbound.customer_id');

        $builder->select(
            DB::raw('COUNT(sales_outbound.id) as total_count'),
            DB::raw('SUM(sales_outbound.total_amount) as total_amount'),
            DB::raw('SUM(sales_outbound.remaining_amount) as total_remaining')
        );

        if(!filter_var($all, FILTER_VALIDATE_BOOLEAN)) {
            $builder = $builder->where('sales_outbound.due_date', '<', Carbon::now());
        }

        return CollectionDueWarningSummary::hydrate($builder->get()->toArray())->first();
    }

    public function getWalkInCustomers()
    {
        $result = $this
            ->model
            ->where('customer_type', Customer::WALK_IN)
            ->where('remaining_amount', '>', 0)
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->get();

        $this->resetModel();

        return $this->parserResult($result);
    }

    public function getNegativeProfitProduct($filters = [], $paging = true)
    {
        $builder = $this->model
            ->getQuery()
            ->join('sales_outbound_detail', 'sales_outbound.id', '=', 'sales_outbound_detail.transaction_id')
            ->join('product', 'sales_outbound_detail.product_id', '=', 'product.id')
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                     ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product_barcode.product_id', 'product.id')
            ->select(
                'sales_outbound.created_for',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'sales_outbound_detail.qty',
                'sales_outbound_detail.ma_cost',
                'sales_outbound_detail.price',
                'product.name',
                'product.stock_no',
                'product.chinese_name',
                'product.status',
                'product.group_type',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode')
            )
            ->whereNull('sales_outbound.deleted_at')
            ->whereRaw('(sales_outbound_detail.price - sales_outbound_detail.ma_cost) < 0')
            ->where('sales_outbound.approval_status', '=', ApprovalStatus::APPROVED)
            ->groupBy(
                'sales_outbound_detail.id',
                'sales_outbound.created_for',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'sales_outbound_detail.qty',
                'sales_outbound_detail.ma_cost',
                'sales_outbound_detail.price',
                'product.name',
                'product.stock_no',
                'product.chinese_name',
                'product.status',
                'product.group_type',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                'barcode',
                'name AS product',
                'sheet_number',
                'qty',
                'ma_cost AS cost',
                'price',
                DB::raw('ABS(price - ma_cost) * qty AS loss')
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'name',
                'chinese_name',
                'status',
                'stock_no',
                'created_at',
                'updated_at',
                'group_type',
                'supplier',
                'category',
                'brand',
                'barcode',
                'transaction_date',
                'created_for'
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                }
            ]
        );

        $result = null;

        if($paging) {
            $result = $this->createPaginationFromQuery($builder);
            $result = $this->hydratePaginationItems($result, new SalesOutboundNegativeProfitProduct);
        } else {
            $result = SalesOutboundNegativeProfitProduct::hydrate($builder->get()->toArray());
        }

        $this->resetModel();

        return $result;
    }

    public function detailSalesReport($filters = [], $paging = true)
    {
        $builder = $this->model
            ->getQuery()
            ->join('sales_outbound_detail', 'sales_outbound.id', '=', 'sales_outbound_detail.transaction_id')
            ->join('product', 'sales_outbound_detail.product_id', '=', 'product.id')
            ->leftJoin('sales', 'sales_outbound.reference_id', '=', 'sales.id')
            ->leftJoin('transaction_serial_number', function($join){
                $join->on('sales_outbound_detail.id', '=', 'transaction_serial_number.seriable_id');
                $join->on('transaction_serial_number.seriable_type', '=', DB::raw("'sales_outbound_detail'"));
            })
            ->leftJoin('product_serial_number', 'transaction_serial_number.serial_id', '=', 'product_serial_number.id')
            ->leftJoin('transaction_batch', function($join){
                $join->on('sales_outbound_detail.id', '=', 'transaction_batch.transactionable_id');
                $join->on('transaction_batch.transactionable_type', '=', DB::raw("'sales_outbound_detail'"));
            })
            ->leftJoin('product_batch', 'transaction_batch.batch_id', '=', 'product_batch.id')
            ->leftJoin('product_barcode', 'product_barcode.product_id', '=', 'product.id')
            ->leftJoin('branch as created_from_branch', 'created_from_branch.id', '=', 'sales_outbound.created_from') // created_from
            ->leftJoin('branch as created_for_branch', 'created_for_branch.id', '=', 'sales_outbound.created_for') // created_for
            ->leftJoin('branch_detail', 'branch_detail.id', '=', 'sales_outbound.for_location') // for_location
            ->leftJoin('customer', 'customer.id', '=', 'sales_outbound.customer_id') //customer
            ->leftJoin('user as salesman_user', 'salesman_user.id', '=', 'sales_outbound.salesman_id') // salesman
            ->leftJoin('user as requested_by_user', 'requested_by_user.id', '=', 'sales_outbound.requested_by') // requested_by
            ->leftJoin('user as created_by_user', 'created_by_user.id', '=', 'sales_outbound.created_by') // created_by
            ->leftJoin('user as audited_by_user', 'audited_by_user.id', '=', 'sales_outbound.audited_by') // audited_by
            ->leftJoin('system_code as unit_id_system_code', 'unit_id_system_code.id', '=', 'sales_outbound_detail.unit_id') //unit_id
            ->select(
                'created_from_branch.name as created_from_name',
                'sales_outbound.created_for',
                'created_for_branch.name as created_for_name',
                'sales_outbound.for_location',
                'branch_detail.name as for_location_name',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'sales.sheet_number as reference_id',
                'sales_outbound.approval_status',
                'sales_outbound.customer_type',
                'sales_outbound.customer_id',
                'customer.name as customer_name',
                'sales_outbound.customer_walk_in_name',
                'sales_outbound.total_amount',
                'sales_outbound.remaining_amount',
                'sales_outbound.remarks',
                'sales_outbound.salesman_id',
                'salesman_user.full_name as salesman_name',
                'sales_outbound.term',
                'sales_outbound.requested_by',
                'requested_by_user.full_name as requested_by_name',
                'sales_outbound.created_by',
                'created_by_user.full_name as created_by_name',
                'sales_outbound.created_at',
                'sales_outbound.audited_by',
                'audited_by_user.full_name as audited_by_name',
                'sales_outbound.audited_date',
                'sales_outbound.deleted_at',
                'product.stock_no',
                'product.chinese_name',
                'product.name AS product_name',
                'sales_outbound_detail.qty',
                'unit_id_system_code.name as unit_name',
                'sales_outbound_detail.unit_qty',
                'sales_outbound_detail.oprice',
                'sales_outbound_detail.price',
                'sales_outbound_detail.discount1',
                'sales_outbound_detail.discount2',
                'sales_outbound_detail.discount3',
                'sales_outbound_detail.discount4',
                DB::raw('sales_outbound_detail.qty * sales_outbound_detail.unit_qty AS total_qty'),
                DB::raw('(sales_outbound_detail.qty * sales_outbound_detail.unit_qty) * sales_outbound_detail.price as detail_total_amount'),
                'sales_outbound_detail.remarks AS detail_remarks',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS product_barcode'),
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_serial_number.serial_number) AS product_serial'),
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_batch.name) AS product_batch'),
                'sales_outbound.modified_by',
                'sales_outbound.updated_at',
                'product.brand_id',
                'product.category_id',
                'product.supplier_id'
            )
            ->groupBy(
                'sales_outbound.id',
                'created_from_branch.name',
                'sales_outbound.created_for',
                'created_for_branch.name',
                'sales_outbound.for_location',
                'branch_detail.name',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'sales.sheet_number',
                'sales_outbound.approval_status',
                'sales_outbound.customer_type',
                'sales_outbound.customer_id',
                'customer.name',
                'sales_outbound.customer_walk_in_name',
                'sales_outbound.total_amount',
                'sales_outbound.remaining_amount',
                'sales_outbound.remarks',
                'sales_outbound.salesman_id',
                'salesman_user.full_name',
                'sales_outbound.term',
                'sales_outbound.requested_by',
                'requested_by_user.full_name',
                'sales_outbound.created_by',
                'created_by_user.full_name',
                'sales_outbound.created_at',
                'sales_outbound.audited_by',
                'audited_by_user.full_name',
                'sales_outbound.audited_date',
                'sales_outbound.deleted_at',
                'product.stock_no',
                'product.chinese_name',
                'product.name',
                'sales_outbound_detail.id',
                'sales_outbound_detail.qty',
                'unit_id_system_code.name',
                'sales_outbound_detail.unit_qty',
                'sales_outbound_detail.oprice',
                'sales_outbound_detail.price',
                'sales_outbound_detail.discount1',
                'sales_outbound_detail.discount2',
                'sales_outbound_detail.discount3',
                'sales_outbound_detail.discount4',
                'sales_outbound_detail.remarks',
                'sales_outbound.modified_by',
                'sales_outbound.updated_at',
                'product.brand_id',
                'product.category_id',
                'product.supplier_id'
            )
            ->orderBy('sales_outbound.transaction_date', 'ASC')
            ->orderBy('sales_outbound_detail.id', 'ASC');

            $builder = $this->setConditionsOnBuilder(
                $builder,
                $filters,
                '',
                [
                    'sales_outbound.created_for',
                    'sales_outbound.for_location',
                    'sales_outbound.transaction_date',
                    'sales_outbound.sheet_number',
                    'sales_outbound.remarks',
                    'sales_outbound.approval_status',
                    'sales_outbound.deleted_at',
                    'sales_outbound.customer_type',
                    'sales_outbound.customer_id',
                    'sales_outbound.salesman_id',
                    'sales_outbound.requested_by',
                    'sales_outbound.created_by',
                    'sales_outbound.created_at',
                    'sales_outbound.audited_by',
                    'sales_outbound.audited_date',
                    'sales.sheet_number',
                    'product_barcode.code',
                    'product.name',
                    'sales_outbound.modified_by',
                    'sales_outbound.updated_at',
                    'product.brand_id',
                    'product.category_id',
                    'product.supplier_id',
                    'collection_status',
                ],
                [
                    'collection_status' => function($query, $filter, $method, $alias) {
                        if($filter->value == 0) {
                            $query->where('sales_outbound.remaining_amount', '>', DB::raw('0'));
                        } else {
                            $query->where('sales_outbound.remaining_amount', '=', DB::raw('0'));
                        }
                    },
                    'sales_outbound.deleted_at' => function($query, $filter, $method, $alias) {
                        if($filter->value == 0) {
                            $query->whereNull('sales_outbound.deleted_at');
                        } else {
                            $query->whereNotNull('sales_outbound.deleted_at');
                        }
                    }
                ]
            );

            $result = null;
    
            if($paging) {
                $result = $this->createPaginationFromQuery($builder);
                $result = $this->hydratePaginationItems($result, new DetailSalesReport);
            } else {
                $result = DetailSalesReport::hydrate($builder->get()->toArray());
            }
    
            $this->resetModel();
    
            return $result;
    }

    public function detailSalesReportSummary($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->join('sales_outbound_detail', 'sales_outbound.id', '=', 'sales_outbound_detail.transaction_id')
            ->join('product', 'sales_outbound_detail.product_id', '=', 'product.id')
            ->leftJoin('product_barcode', 'product_barcode.product_id', '=', 'product.id')
            ->leftJoin('sales', 'sales_outbound.reference_id', '=', 'sales.id')
            ->select(
                'sales_outbound.id',
                'sales_outbound.remaining_amount',
                'sales_outbound_detail.id AS detail_id',
                DB::raw('sales_outbound_detail.qty * sales_outbound_detail.unit_qty AS total_qty'),
                DB::raw('(sales_outbound_detail.qty * sales_outbound_detail.unit_qty) * sales_outbound_detail.price as total_amount'),
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS product_barcode'),
                'product.name AS product_name'
            )
            ->groupBy(
                'sales_outbound.id',
                'sales_outbound.remaining_amount',
                'sales_outbound_detail.id',
                'sales_outbound_detail.qty',
                'sales_outbound_detail.unit_qty',
                'sales_outbound_detail.price',
                'product.name'
            );
        
        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'sales_outbound.created_for',
                'sales_outbound.for_location',
                'sales_outbound.transaction_date',
                'sales_outbound.sheet_number',
                'sales_outbound.remarks',
                'sales_outbound.approval_status',
                'sales_outbound.deleted_at',
                'sales_outbound.customer_type',
                'sales_outbound.customer_id',
                'sales_outbound.salesman_id',
                'sales_outbound.requested_by',
                'sales_outbound.created_by',
                'sales_outbound.created_at',
                'sales_outbound.audited_by',
                'sales_outbound.audited_date',
                'sales.sheet_number',
                'product_barcode.code',
                'product.name',

                'sales_outbound.modified_by',
                'sales_outbound.updated_at',
                'product.brand_id',
                'product.category_id',
                'product.supplier_id',

                'collection_status',
            ],
            [
                'collection_status' => function($query, $filter, $method, $alias) {
                    if($filter->value == 0) {
                        $query->where('sales_outbound.remaining_amount', '>', DB::raw('0'));
                    } else {
                        $query->where('sales_outbound.remaining_amount', '=', DB::raw('0'));
                    }
                },
                'sales_outbound.deleted_at' => function($query, $filter, $method, $alias) {
                    if($filter->value == 0) {
                        $query->whereNull('sales_outbound.deleted_at');
                    } else {
                        $query->whereNotNull('sales_outbound.deleted_at');
                    }
                }
            ]
        );

        

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'remaining_amount',
                DB::raw('SUM(total_qty) AS total_qty'),
                DB::raw('SUM(total_amount) AS total_amount')
            )
            ->groupBy(
                'id',
                'remaining_amount'
            );
        
        $builder = $this->createSubQuery($builder)
            ->select(
                DB::raw('SUM(remaining_amount) AS total_remaining_amount'),
                DB::raw('SUM(total_qty) AS total_qty'),
                DB::raw('SUM(total_amount) AS total_amount')
            );

        $result = DetailSalesReportSummary::hydrate($builder->get()->toArray());

        $this->resetModel();
    
        return $result;
    }
}
