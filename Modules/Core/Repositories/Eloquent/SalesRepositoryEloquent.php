<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\SalesRepository;
use Modules\Core\Entities\Sales;

use DB;

/**
 * Class SalesRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SalesRepositoryEloquent extends InvoiceRepositoryEloquent implements SalesRepository
{
    public function model()
    {
        return Sales::class; 
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['salesman_id', 'customer_type', 'customer_id', 'customer_detail_id']);
    }
}