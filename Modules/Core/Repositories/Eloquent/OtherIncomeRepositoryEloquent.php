<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Repositories\Contracts\OtherIncomeRepository;
use Modules\Core\Entities\OtherIncome;

/**
 * Class OtherIncomeRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class OtherIncomeRepositoryEloquent extends OtherFinancialFlowRepositoryEloquent implements OtherIncomeRepository
{
    public function model()
    {
        return OtherIncome::class; 
    }
}