<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\InventoryAdjustRepository;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Enums\ApprovalStatus;
use DB;

/**
 * Class InventoryAdjustRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class InventoryAdjustRepositoryEloquent extends RepositoryEloquent implements InventoryAdjustRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_from',
        'created_for',
        'for_location',
        'requested_by'
    ];

    public function model()
    {
        return InventoryAdjust::class;
    }

    /**
     * Get all detail transactions by transction filter and product_id
     * 
     * @param  array|int $ids
     * @param  int $limit
     * @return Model
     */
    public function findTransactionsByProduct($ids, $type, $limit = null)
    {
        $that = $this;
        $model = $this->model->details()->getRelated();
        $limit = is_null($limit) ? config('repository.pagination.limit', 15) : $limit;

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $results = $model->with(['transaction', 'transaction.creator'])
            ->select('inventory_adjust_detail.*')
            ->whereIn('product_id', $ids)
            ->where(function($query) use ($type) {
                $operator = $type == 1 ? '>' : '<';
                $query->where(DB::raw('(((qty * unit_qty) + pc_qty) - inventory)'), $operator, 0);
            })
            ->whereHas('transaction', function($query) use($that){
                $query = $that->getCriteria()[0]->apply($query, $that);
                $query->where('approval_status', '=', ApprovalStatus::APPROVED);
            })
            ->join('inventory_adjust', 'inventory_adjust.id', 'inventory_adjust_detail.transaction_id')
            ->orderBy('inventory_adjust.transaction_date', 'desc')
            ->paginate($limit);

        $results->appends(app('request')->query());

        $this->resetModel();
        
        return $results;
    }

    public function whereApproved()
    {
        $this->model = $this->model->where('approval_status', ApprovalStatus::APPROVED);

        return $this;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
