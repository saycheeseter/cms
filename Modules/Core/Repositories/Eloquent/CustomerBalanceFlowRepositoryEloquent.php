<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\CustomerBalanceFlowRepository;
use Modules\Core\Entities\CustomerBalanceFlow;

use DB;

/**
 * Class CustomerBalanceFlowRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class CustomerBalanceFlowRepositoryEloquent extends BalanceFlowRepositoryEloquent implements CustomerBalanceFlowRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer.name',
        'flow_type',
        'transaction_date'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CustomerBalanceFlow::class;
    }

    /**
     * Get all the record of customer balance flow by filters
     * 
     * @return Collection
     */
    public function search() 
    {
        $this->model = $this->model
            ->with(['customer'])
            ->join('customer', 'customer_balance_flow.customer_id', '=', 'customer.id');

        return $this->all();
    }

    /**
     * Get the beginning value of the specified data by removing the transasction date filter
     * 
     * @param  string $date
     * @return string
     */
    public function getBeginning($date)
    {
        $fieldSearchableBuffer = $this->fieldSearchable;

        unset($this->fieldSearchable[2]);

        $query = $this->applyCriteria()
            ->model
            ->join('customer', 'customer_balance_flow.customer_id', '=', 'customer.id')
            ->where('transaction_date', '<', $date)
            ->select(
                DB::raw(
                    'SUM(
                        CASE flow_type
                            WHEN 0 THEN amount
                            ELSE (-1 * amount)
                        END
                    ) AS beginning'
                )
            );

        $this->fieldSearchable = $fieldSearchableBuffer;

        $result = $query->first();

        $this->resetModel();

        return $result;
    }

    /**
     * Get the balance of the specified customer
     * 
     * @return CustomerBalanceFlow
     */
    public function getBalance($id) 
    {
        $result = $this->model
            ->where('customer_id', $id)
            ->select(
                DB::raw(
                    'SUM(
                        CASE flow_type
                            WHEN 0 THEN amount
                            ELSE (-1 * amount)
                        END
                    ) AS balance'
                )
            )
            ->first();

        $this->resetModel();

        return $result;
    }
}
