<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\UOMRepository;
use Modules\Core\Entities\UOM;

/**
 * Class UOMRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class UOMRepositoryEloquent extends SystemCodeRepositoryEloquent implements UOMRepository
{
    public function model()
    {
        return UOM::class; 
    }
}
