<?php

namespace Modules\Core\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Core\Entities\Hydration\SupplierMonthlySalesRanking;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\SupplierRepository;
use Modules\Core\Entities\Supplier;
use Modules\Core\Repositories\Criteria\RequestCriteria;
use DB;

/**
 * Class SupplierRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class SupplierRepositoryEloquent extends RepositoryEloquent implements SupplierRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'full_name',
        'chinese_name',
        'memo',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Supplier::class;
    }

    public function getMonthlySalesRanking($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('product', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('product_monthly_transactions', 'product.id', '=', 'product_monthly_transactions.product_id')
            ->select(
                'supplier.code as supplier_code',
                'supplier.full_name as supplier_full_name',
                DB::raw('COALESCE(SUM(product_monthly_transactions.sales_outbound_total_qty) + SUM(product_monthly_transactions.pos_sales_total_qty), 0) as total_qty'),
                DB::raw('COALESCE(SUM(product_monthly_transactions.sales_outbound_total_amount) + SUM(product_monthly_transactions.pos_sales_total_amount), 0) as total_amount'),
                DB::raw('
                    COALESCE(
                        SUM(product_monthly_transactions.sales_outbound_total_amount) 
                        + SUM(product_monthly_transactions.pos_sales_total_amount) 
                        - SUM(product_monthly_transactions.sales_outbound_total_cost) 
                        - SUM(product_monthly_transactions.pos_sales_total_cost), 
                    0) as total_profit
                ')
            )
            ->groupBy(
                'supplier.id',
                'supplier.code',
                'supplier.full_name'
            )
            ->whereNull('supplier.deleted_at');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            [
                'created_for' => 'product_monthly_transactions',
                'for_location' => 'product_monthly_transactions',
                'category_id' => 'product',
                'brand_id' => 'product',
            ],
            [
                'supplier_id',
                'created_for',
                'for_location',
                'category_id',
                'brand_id',
                'year_month',
            ],
            [
                'supplier_id' => function ($query, $filter, $method, $alias) {
                    $query->$method('supplier.id', $filter->operator, $filter->value);
                },
                'year_month' => function ($query, $filter, $method, $alias) {
                    $query->$method(function ($q) use ($filter) {
                        $date = Carbon::parse($filter->value);

                        $q->where('product_monthly_transactions.transaction_date', $filter->operator, $date->toDateString());
                    });
                },
            ]);

        $builder = $this->createSubQuery($builder)
            ->orderBy('total_amount', 'desc')
            ->orderBy('supplier_code', 'desc');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new SupplierMonthlySalesRanking);

        $this->resetModel();

        return $paginator;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
