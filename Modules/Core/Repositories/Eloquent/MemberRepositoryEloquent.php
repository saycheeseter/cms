<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\MemberRepository;
use Modules\Core\Entities\Member;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class MemberRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class MemberRepositoryEloquent extends RepositoryEloquent implements MemberRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'full_name',
        'card_id',
        'barcode',
        'updated_at',
        'deleted_at',
        'rate_head_id'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Member::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
