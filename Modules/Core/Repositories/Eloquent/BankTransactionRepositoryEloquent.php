<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\BankTransaction;
use Modules\Core\Repositories\Criteria\RequestCriteria;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\BankTransactionRepository;

/**
 * Class BankRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BankTransactionRepositoryEloquent extends RepositoryEloquent implements BankTransactionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'branch_id',
        'from_bank',
        'to_bank',
        'type',
        'transaction_date'
    ];

    public function model()
    {
        return BankTransaction::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
