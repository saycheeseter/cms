<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\DamageRepository;
use Modules\Core\Entities\Damage;

/**
 * Class DamageRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class DamageRepositoryEloquent extends InvoiceRepositoryEloquent implements DamageRepository
{
    public function model()
    {
        return Damage::class;
    }

    public function boot()
    {
        parent::boot();
        $this->setFieldSearchable(['reason_id']);
    }
}
