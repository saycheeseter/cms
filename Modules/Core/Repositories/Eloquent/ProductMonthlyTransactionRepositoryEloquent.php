<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\Hydration\ProductMonthlySales;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Enums\InventoryStatus;
use Modules\Core\Enums\SystemCodeType;
use Modules\Core\Repositories\Contracts\ProductMonthlyTransactionRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use DB;
use Carbon\Carbon;

/**
 * Class ProductMonthlyTransactionRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductMonthlyTransactionRepositoryEloquent extends RepositoryEloquent implements ProductMonthlyTransactionRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_for',
        'for_location'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductMonthlyTransactions::class;
    }

    public function getIncomeStatementSummary($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->selectRaw('
                SUM(sales_outbound_total_amount) + SUM(pos_sales_total_amount) as gross_sales,
                SUM(sales_return_inbound_total_amount) as return_amount,
                SUM(sales_outbound_total_ma_cost) + SUM(pos_sales_total_ma_cost) as ma_cost_of_goods_sold,
                SUM(sales_outbound_total_cost) + SUM(pos_sales_total_cost) as cost_of_goods_sold,
                SUM(damage_outbound_total_amount) as damage,
                SUM(adjustment_increase_total_amount - adjustment_decrease_total_amount) as adjustment_discrepancy
            ');

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            [
                'created_for' => 'product_monthly_transactions',
                'for_location' => 'product_monthly_transactions'
            ],
            [
                'created_for',
                'for_location',
                'year_month',
            ],
            [
                'year_month' => function ($query, $filter, $method, $alias) {
                    $date = Carbon::parse($filter->value);
                    $query->$method('product_monthly_transactions.transaction_date', $filter->operator, $date);
                }
            ]
        );

        $this->resetModel();

        return $builder->first();
    }

    public function getProductMonthlySales($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftjoin('product','product_monthly_transactions.product_id','=','product.id')
            ->leftJoin('supplier', 'product.supplier_id', '=', 'supplier.id')
            ->leftJoin('category', 'product.category_id', '=', 'category.id')
            ->leftJoin('system_code', function($join) {
                $join->on('product.brand_id', '=', 'system_code.id')
                    ->where('system_code.type_id', '=', SystemCodeType::BRAND);
            })
            ->leftJoin('product_barcode', 'product.id', '=', 'product_barcode.product_id')
            ->leftJoin('product_branch_summary', function($join)  use ($filters) {
                $join = $join->on('product.id', 'product_branch_summary.product_id');
                $join = $this->setConditionsOnBuilder($join, $filters, 'product_branch_summary', 'branch_id');
            })
            ->select(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.status',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name AS supplier_name',
                'supplier.code AS supplier_code',
                'category.name AS category_name',
                'category.code AS category_code',
                'system_code.name AS brand_name',
                'system_code.code AS brand_code',
                DB::raw('dbo.GROUP_CONCAT(DISTINCT product_barcode.code) AS barcode'),
                DB::raw('COALESCE(product_branch_summary.qty, 0) AS inventory'),
                'product_monthly_transactions.sales_outbound_total_qty',
                'product_monthly_transactions.sales_outbound_total_amount',
                'product_monthly_transactions.sales_outbound_total_cost',
                'product_monthly_transactions.pos_sales_total_qty',
                'product_monthly_transactions.pos_sales_total_amount',
                'product_monthly_transactions.pos_sales_total_cost',
                'product_monthly_transactions.created_for AS product_monthly_transactions_created_for',
                'product_monthly_transactions.for_location AS product_monthly_transactions_for_location',
                'product_monthly_transactions.transaction_date AS product_monthly_transactions_transaction_date'
            )
            ->groupBy(
                'product.id',
                'product.name',
                'product.stock_no',
                'product.status',
                'product.description',
                'product.chinese_name',
                'product.group_type',
                'product.created_at',
                'product.updated_at',
                'supplier.full_name',
                'supplier.code',
                'category.name',
                'category.code',
                'system_code.name',
                'system_code.code',
                'product_branch_summary.branch_detail_id',
                'product_branch_summary.qty',
                'product_monthly_transactions.created_for',
                'product_monthly_transactions.for_location',
                'product_monthly_transactions.transaction_date',
                'product_monthly_transactions.sales_outbound_total_qty',
                'product_monthly_transactions.sales_outbound_total_amount',
                'product_monthly_transactions.sales_outbound_total_cost',
                'product_monthly_transactions.pos_sales_total_qty',
                'product_monthly_transactions.pos_sales_total_amount',
                'product_monthly_transactions.pos_sales_total_cost'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            'product_monthly_transactions',
            [
                'created_for',
                'for_location',
                'year_month',
            ],
            [
                'year_month' => function ($query, $filter, $method, $alias) {
                    $query->$method('transaction_date', $filter->operator, Carbon::parse($filter->value)->toDateString());
                }
            ]);

        $builder = $this->createSubQuery($builder)
            ->select(
                'id',
                'name',
                'stock_no',
                'status',
                'description',
                'chinese_name',
                'group_type',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                DB::raw('SUM(inventory) AS inventory'),
                'sales_outbound_total_qty',
                'sales_outbound_total_amount',
                'sales_outbound_total_cost',
                'pos_sales_total_qty',
                'pos_sales_total_amount',
                'pos_sales_total_cost',
                'product_monthly_transactions_created_for',
                'product_monthly_transactions_for_location',
                'product_monthly_transactions_transaction_date'
            )->groupBy(
                'id',
                'name',
                'stock_no',
                'status',
                'description',
                'chinese_name',
                'group_type',
                'created_at',
                'updated_at',
                'supplier_name',
                'supplier_code',
                'category_name',
                'category_code',
                'brand_name',
                'brand_code',
                'barcode',
                'product_monthly_transactions_created_for',
                'product_monthly_transactions_for_location',
                'product_monthly_transactions_transaction_date',
                'sales_outbound_total_qty',
                'sales_outbound_total_amount',
                'sales_outbound_total_cost',
                'pos_sales_total_qty',
                'pos_sales_total_amount',
                'pos_sales_total_cost'
            );

        $builder = $this->createSubQuery($builder)
            ->select(
                DB::raw('SUM(sales_outbound_total_qty) + SUM(pos_sales_total_qty) as total_quantity'),
                DB::raw('SUM(sales_outbound_total_amount) + SUM(pos_sales_total_amount) as total_amount'),
                DB::raw('SUM(sales_outbound_total_cost) + SUM(pos_sales_total_cost) as total_cost'),
                DB::raw('
                    SUM(sales_outbound_total_amount)
                    + SUM(pos_sales_total_amount)
                    - SUM(sales_outbound_total_cost)
                    - SUM(pos_sales_total_cost) as total_profit
                '),
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'supplier_name as supplier',
                'category_name as category',
                'brand_name as brand',
                'barcode'
            )->groupBy(
                'name',
                'stock_no',
                'description',
                'chinese_name',
                'supplier_name',
                'category_name',
                'brand_name',
                'barcode'
            );

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            '',
            [
                'supplier',
                'category',
                'brand',
                'stock_no',
                'status',
                'name',
                'chinese_name',
                'group_type',
                'created_at',
                'updated_at',
                'inventory',
                'barcode',
            ],
            [
                'supplier' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('supplier_name', $filter->operator, $filter->value)
                            ->orWhere('supplier_code', $filter->operator, $filter->value);
                    });
                },
                'category' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('category_name', $filter->operator, $filter->value)
                            ->orWhere('category_code', $filter->operator, $filter->value);
                    });
                },
                'brand' => function ($query, $filter, $method, $alias) {
                    return $query->$method(function ($q) use ($filter) {
                        $q->where('brand_name', $filter->operator, $filter->value)
                            ->orWhere('brand_code', $filter->operator, $filter->value);
                    });
                },
                'inventory' => function ($query, $filter, $method, $alias) {
                    switch($filter->value) {
                        case InventoryStatus::NEGATIVE:
                            $filter->operator = '<';
                            break;

                        case InventoryStatus::NO_INVENTORY:
                            $filter->operator = '=';
                            break;

                        case InventoryStatus::WITH_INVENTORY:
                            $filter->operator = '>';
                            break;
                    }

                    $filter->value = 0;

                    return $query->$method('inventory', $filter->operator, $filter->value);
                },
            ]);

        // Another sub query for the pagination to properly work
        $builder = $this->createSubQuery($builder)
            ->orderBy('total_amount','desc');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new ProductMonthlySales);

        $this->resetModel();

        return $paginator;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {   
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}