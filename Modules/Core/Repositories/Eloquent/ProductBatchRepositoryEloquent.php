<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Entities\BranchDetail;
use Modules\Core\Entities\Customer;
use Modules\Core\Enums\BatchStatus;
use Modules\Core\Enums\ItemManagementOwner;
use Modules\Core\Repositories\Contracts\ProductBatchRepository;
use Modules\Core\Entities\ProductBatch;

/**
 * Class ProductBatchRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductBatchRepositoryEloquent extends RepositoryEloquent implements ProductBatchRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductBatch::class;
    }

    /**
     * Get the list of batches by the indicated product id and apply if there are any constraint or
     * additional condition to the query (e.g. owner type)
     *
     * @param  int $product
     * @param  int|null $constraint
     * @param  int|null $entity
     * @param  int|null $supplier
     * @return Collection
     */
    public function search($product, $constraint = null, $entity = null, $supplier = null)
    {
        $that = $this;

        $builder = $this->model
            ->whereHas('owners', function($query) use ($that, $constraint, $entity) {
                $that->getManagementOwnerConstraint($query, $constraint, $entity);
            })
            ->with(['owners' => function ($query) use ($that, $constraint, $entity) {
                $that->getManagementOwnerConstraint($query, $constraint, $entity);
            }])
            ->where('product_id', $product)
            ->where('status', BatchStatus::AVAILABLE);

        if (!is_null($supplier) && !empty($supplier)) {
            $builder = $builder->where('supplier_id', $supplier);
        }

        $results = $builder->get();

        $this->resetModel();

        return $this->parserResult($results);
    }

    /**
     * Execute the corresponding constraint for the Product Batch Owner relationship
     *
     * @param $query
     * @param $constraint
     * @param $entity
     */
    private function getManagementOwnerConstraint($query, $constraint, $entity)
    {
        switch ($constraint) {
            case ItemManagementOwner::SUPPLIER:
            case ItemManagementOwner::BRANCH_DETAIL:
                $query->where('owner_type', $this->getMorphMapKey(BranchDetail::class))
                    ->where('owner_id', $entity)
                    ->where('current_qty', '>', 0);
                break;

            case ItemManagementOwner::CUSTOMER:
                $query->where('owner_type', $this->getMorphMapKey(Customer::class))
                    ->where('owner_id', empty($entity) ? null : $entity)
                    ->where('current_qty', '>', 0);
                break;
        }
    }
}
