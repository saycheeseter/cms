<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\BrandRepository;
use Modules\Core\Repositories\Criteria\BrandOnlyCriteria;
use Modules\Core\Entities\Brand;
use Modules\Core\Entities\Hydration\BrandMonthlySales;
use Modules\Core\Enums\SystemCodeType;
use Carbon\Carbon;
use DB;

/**
 * Class BrandRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BrandRepositoryEloquent extends SystemCodeRepositoryEloquent implements BrandRepository
{
    public function model()
    {
        return Brand::class;
    }

    public function getBrandMonthlySales($filters = [])
    {
        $builder = $this->model
            ->getQuery()
            ->leftJoin('product', 'product.brand_id', '=', 'system_code.id')
            ->leftJoin('product_monthly_transactions', 'product.id', '=', 'product_monthly_transactions.product_id')
            ->select(
                'system_code.code as code',
                'system_code.name as name',
                'system_code.remarks as remarks',
                DB::raw('SUM(product_monthly_transactions.sales_outbound_total_qty) + SUM(product_monthly_transactions.pos_sales_total_qty) as total_qty'),
                DB::raw('SUM(product_monthly_transactions.sales_outbound_total_amount) + SUM(product_monthly_transactions.pos_sales_total_amount) as total_amount'),
                DB::raw('
                    SUM(product_monthly_transactions.sales_outbound_total_amount) 
                    + SUM(product_monthly_transactions.pos_sales_total_amount) 
                    - SUM(product_monthly_transactions.sales_outbound_total_cost)
                    - SUM(product_monthly_transactions.pos_sales_total_cost) as total_profit
                ')
            )
            ->groupBy(
                'system_code.code',
                'system_code.name',
                'system_code.remarks'
            )
            ->whereNull('system_code.deleted_at')
            ->where('system_code.type_id', '=', SystemCodeType::BRAND);

        $builder = $this->setConditionsOnBuilder(
            $builder,
            $filters,
            [
                'created_for' => 'product_monthly_transactions',
                'for_location' => 'product_monthly_transactions',
                'supplier_id' => 'product',
                'category_id' => 'product',
            ],
            [
                'created_for',
                'for_location',
                'year_month',
                'supplier_id',
                'category_id',
                'brand_id',
            ],
            [
                'year_month' => function ($query, $filter, $method, $alias) {
                    $query->$method(
                        'product_monthly_transactions.transaction_date',
                        $filter->operator,
                        Carbon::parse($filter->value)->toDateString()
                    );
                },
                'brand_id' => function ($query, $filter, $method, $alias) {
                    $query->$method('system_code.id', $filter->operator, $filter->value);
                },
            ]);

        $builder = $this->createSubQuery($builder)
            ->orderBy('total_amount', 'desc')
            ->orderBy('code', 'desc');

        $paginator = $this->createPaginationFromQuery($builder);
        $paginator = $this->hydratePaginationItems($paginator, new BrandMonthlySales);

        $this->resetModel();

        return $paginator;
    }
}
