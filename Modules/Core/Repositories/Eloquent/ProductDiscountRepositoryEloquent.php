<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\ProductDiscountRepository;
use Modules\Core\Entities\ProductDiscount;
use Modules\Core\Enums\SelectType;
use Modules\Core\Enums\Status;
use Carbon\Carbon;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class ProductDiscountRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductDiscountRepositoryEloquent extends RepositoryEloquent implements ProductDiscountRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'valid_from',
        'valid_to',
        'target_type',
        'discount_scheme',
        'select_type',
        'status',
        'created_for',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductDiscount::class;
    }

    public function filter($attributes, $id = null) 
    {
        $days = ['mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun'];
        $daysFilter = [];

        $this->resetModel();

        $model = $this->model->with([
                'details',
                'targets'
            ])->where('status', Status::ACTIVE);

        foreach ($attributes as $key => $value) {
            if($key == 'valid_from' && $attributes['valid_to']) {
                $model = $model->whereBetween($key, [$value, 
                    Carbon::createFromFormat('Y-m-d', $attributes['valid_to'])->endOfDay()->toDateTimeString()
                ]);
            } else if($key == 'valid_to' && $attributes['valid_from']) {
                $model = $model->whereBetween($key, [$attributes['valid_from'], 
                    Carbon::createFromFormat('Y-m-d', $value)->endOfDay()->toDateTimeString()
                ]);
            } else if($key == 'select_type' && $value == SelectType::ALL_PRODUCTS) {
                $model = $model->where(function ($query) use ($key, $value) {
                    $query->orWhere($key, $value)
                          ->orWhere($key, SelectType::SPECIFIC_PRODUCT);
                });
            }else if(in_array($key, $days) && $value != 0) {
                $daysFilter[$key] = $value;
            } else if(!in_array($key, $days)) {
                $model = $model->where($key, $value);
            }
        }

        if(count($daysFilter) > 0) {
            $model = $model->where(function ($query) use ($daysFilter) {
                foreach ($daysFilter as $key => $value) {
                    $query->orWhere($key, $value);
                }
            });
        }

        if($id) {
            $model = $model->where('id', '!=', $id);
        }

        $return = $model->get();

        $this->resetModel();

        return $return;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
