<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockDeliveryOutboundDetailRepository;
use Modules\Core\Entities\StockDeliveryOutboundDetail;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;

/**
 * Class StockDeliveryOutboundDetailRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockDeliveryOutboundDetailRepositoryEloquent extends RepositoryEloquent implements StockDeliveryOutboundDetailRepository
{
    public function model()
    {
        return StockDeliveryOutboundDetail::class;
    }

    /**
     * Retrieve the list of serials of the indicated transaction detail
     *
     * @param $id
     * @return mixed
     */
    public function serials($id)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model
            ->with(['serials' => function($query) { 
                $query->where('status', SerialStatus::ON_TRANSIT)
                    ->where('transaction', SerialTransaction::STOCK_DELIVERY_OUTBOUND); 
            }])
            ->find($id)
            ->serials;
        
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Retrieve the list of batches of the indicated transaction detail
     *
     * @param $id
     * @return mixed
     */
    public function batches($id)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model
            ->with(['batches'])
            ->find($id)
            ->batches;

        $this->resetModel();

        return $this->parserResult($model);
    }
}
