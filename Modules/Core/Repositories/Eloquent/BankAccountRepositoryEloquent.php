<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\BankAccountRepository;
use Modules\Core\Entities\BankAccount;

/**
 * Class BankAccountRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BankAccountRepositoryEloquent extends RepositoryEloquent implements BankAccountRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'account_name',
        'account_number'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BankAccount::class;
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
