<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\StockRequestRepository;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Carbon\Carbon;
use DB;

/**
 * Class StockRequestRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class StockRequestRepositoryEloquent extends InvoiceRepositoryEloquent implements StockRequestRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sheet_number',
        'transaction_date',
        'remarks',
        'approval_status',
        'deleted_at',
        'created_from',
        'request_to',
        'transaction_status',
        'for_location',
        'requested_by'
    ];

    public function model()
    {
        return StockRequest::class;
    }

    /**
     * Find remaining list of invoices based on filters
     *
     * @param  string $sheetNumber
     * @return Model
     */
    public function findRemaining($branch, $sheetNumber = '')
    {
        $model = $this->model
            ->where('request_to', $branch)
            ->where('sheet_number', 'LIKE', sprintf("%%%s%%", $sheetNumber))
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->whereIn('transaction_status', array(TransactionStatus::INCOMPLETE, TransactionStatus::NO_DELIVERY))
            ->orderBy('transaction_date', 'desc')
            ->limit(10)
            ->get();

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Retrieve invoice details whose remaining qty
     * is > 0
     *
     * @param  string $id
     * @return array
     */
    public function retrieve($id)
    {
        $model = $this->model
            ->with(array(
                'details' => function($query) {
                    $query->where('remaining_qty', '>', 0);
                },
                'for.details'
            ))
            ->find($id);

        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Get other requested products by filters
     * 
     * @param  array  $productIds
     * @param  array $filters
     * @param  int|null $except
     * @return Collection
     */
    public function findOtherRequestedProducts(array $productIds, $filters, $except = null)
    {   
        $builder = $this->model->getQuery();

        $builder->join('stock_request_detail', 'stock_request.id', '=', 'stock_request_detail.transaction_id')
            ->whereIn('stock_request_detail.product_id', $productIds)
            ->where('created_for', $filters['branch'])
            ->where('transaction_date', '>=', Carbon::now()->subDays($filters['days'])->startOfDay()->toDateTimeString())
            ->where('transaction_date', '<=', Carbon::now()->toDateTimeString())
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->where('stock_request.deleted_at', null)
            ->where('stock_request_detail.deleted_at', null);

        if (!is_null($except)) {
            $builder->where('stock_request.id', '<>', $except);
        }

        switch ($filters['delivery_status']) {
            case TransactionStatus::NO_DELIVERY:
                $comparison = '=';
                break;
            
            case TransactionStatus::INCOMPLETE:
                $comparison = '<';
                break;
        }

        $data = $builder->where('stock_request_detail.remaining_qty', 
            $comparison, 
            DB::raw('(stock_request_detail.qty * stock_request_detail.unit_qty)')
        )->get();

        $this->resetModel();

        return $data;
    }
}
