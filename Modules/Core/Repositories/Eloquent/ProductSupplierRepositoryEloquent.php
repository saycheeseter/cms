<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\ProductSupplierRepository;
use Modules\Core\Entities\ProductSupplier;

use App;

/**
 * Class ProductSupplierRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class ProductSupplierRepositoryEloquent extends RepositoryEloquent implements ProductSupplierRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'supplier.full_name',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductSupplier::class;
    }

    /**
     * Search data based on products and apply filter criteria implementation
     * 
     * @return Collection
     */
    public function search($limit = null)
    {
        $this->model = $this->model
            ->with([
                'product',
                'unit',
                'supplier',
            ])
            ->select('product_supplier.*')
            ->join('supplier', 'product_supplier.supplier_id', '=', 'supplier.id');

        if (app('request')->has('products')) {
            $this->model = $this->model->whereIn('product_id', app('request')->get('products'));
        }

        return $this->paginate($limit);
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}
