<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\UserMenuRepository;
use Modules\Core\Entities\UserMenu;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;

/**
 * Class UserMenuRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class UserMenuRepositoryEloquent extends RepositoryEloquent implements UserMenuRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserMenu::class;
    }

    /**
     * Find transaction by the given id
     *
     * @param  string $id
     * @return Model
     */
    public function findByUser($id)
    {
        $model = $this->model->where('user_id', $id)->first();

        $this->resetModel();

        return $this->parserResult($model);
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}

