<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\BranchSettingRepository;
use Modules\Core\Entities\BranchSetting;

/**
 * Class BranchSettingRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class BranchSettingRepositoryEloquent extends RepositoryEloquent implements BranchSettingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchSetting::class;
    }
}
