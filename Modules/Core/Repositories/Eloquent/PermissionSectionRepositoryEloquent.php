<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\PermissionSectionRepository;
use Modules\Core\Entities\PermissionSection;

/**
 * Class PermissionSectionRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class PermissionSectionRepositoryEloquent extends RepositoryEloquent implements PermissionSectionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PermissionSection::class;
    }
}
