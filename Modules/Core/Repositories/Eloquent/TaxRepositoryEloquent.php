<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\TaxRepository;
use Modules\Core\Entities\Tax;

/**
 * Class BranchRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class TaxRepositoryEloquent extends RepositoryEloquent implements TaxRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tax::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {

    }
}
