<?php 

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Contracts\CustomerRepository;
use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Entities\Customer;
use Modules\Core\Repositories\Criteria\RequestCriteria;

/**
 * Class CustomerRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class CustomerRepositoryEloquent extends RepositoryEloquent implements CustomerRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'updated_at',
        'deleted_at'
    ];

    public function model()
    {
        return Customer::class; 
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }
}