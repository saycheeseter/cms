<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\RoleRepository;
use Modules\Core\Entities\Role;

/**
 * Class RoleRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class RoleRepositoryEloquent extends RepositoryEloquent implements RoleRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}