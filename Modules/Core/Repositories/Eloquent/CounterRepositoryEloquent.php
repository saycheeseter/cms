<?php

namespace Modules\Core\Repositories\Eloquent;

use Modules\Core\Repositories\Criteria\ColumnFilterCriteria;
use Modules\Core\Repositories\Contracts\CounterRepository;
use Modules\Core\Entities\Counter;

/**
 * Class CounterRepositoryEloquent
 * @package namespace Modules\Core\Repositories\Eloquent;
 */
class CounterRepositoryEloquent extends RepositoryEloquent implements CounterRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'created_for',
        'customer_id',
        'transaction_date',
        'sheet_number',
        'remarks',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Counter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(ColumnFilterCriteria::class));
    }
}