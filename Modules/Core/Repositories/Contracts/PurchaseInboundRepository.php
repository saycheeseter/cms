<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PurchaseInboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PurchaseInboundRepository extends InventoryChainRepository
{
    public function findApprovedTransactionByReference($reference);
    public function transactionsWithBalance($branchId, $supplierId, $limit = null);
    public function getDueWarning($filters = [], $all = false);
    public function getDueWarningSummary($filters = [], $all = false);
}
