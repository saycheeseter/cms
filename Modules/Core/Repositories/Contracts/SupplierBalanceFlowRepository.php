<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SupplierBalanceFlowRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SupplierBalanceFlowRepository extends BalanceFlowRepository
{
    public function getBalance($id);
}
