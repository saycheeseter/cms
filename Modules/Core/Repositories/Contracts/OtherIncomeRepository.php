<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface OtherIncomeRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface OtherIncomeRepository extends OtherFinancialFlowRepository
{
    
}
