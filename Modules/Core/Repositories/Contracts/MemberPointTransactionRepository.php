<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface MemberPointTransactionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface MemberPointTransactionRepository extends Repository
{
    public function points($memberId);
}
