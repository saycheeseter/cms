<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface UserMenuRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface UserMenuRepository extends Repository
{
    public function findByUser($id);
}
