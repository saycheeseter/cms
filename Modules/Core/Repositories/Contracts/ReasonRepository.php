<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ReasonRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ReasonRepository extends SystemCodeRepository
{
    //
}
