<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductTransactionSummaryRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductTransactionSummaryRepository extends Repository
{
    public function search($ids, $limit = null);
    public function getBeginning($ids, $date);
}
