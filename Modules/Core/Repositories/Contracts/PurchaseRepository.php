<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PurchaseRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PurchaseRepository extends InvoiceRepository
{

}
