<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface DamageOutboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface DamageOutboundRepository extends InventoryChainRepository
{

}
