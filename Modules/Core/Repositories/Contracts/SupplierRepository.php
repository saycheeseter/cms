<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SupplierRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SupplierRepository extends Repository
{
    public function getMonthlySalesRanking($filters = []);
}
