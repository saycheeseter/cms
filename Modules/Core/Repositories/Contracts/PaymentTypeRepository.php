<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PaymentTypeRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PaymentTypeRepository extends SystemCodeRepository
{
    //
}
