<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface BankRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface BankRepository extends SystemCodeRepository
{
    //
}
