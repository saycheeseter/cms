<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PurchaseReturnRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PurchaseReturnRepository extends InvoiceRepository
{

}
