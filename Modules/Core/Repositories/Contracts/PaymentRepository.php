<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PaymentRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PaymentRepository extends FinancialFlowRepository
{

}