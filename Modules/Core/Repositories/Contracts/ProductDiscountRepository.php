<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductDiscountRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductDiscountRepository extends Repository
{
    public function filter($attributes, $id = null);
}
