<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockReturnOutboundDetailRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockReturnOutboundDetailRepository extends Repository
{
    public function serials($id);
    public function batches($id);
}
