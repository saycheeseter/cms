<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductBatchRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductBatchRepository extends Repository
{
    public function search($product, $constraint = null, $entity = null, $supplier = null);
}