<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PurchaseReturnOutboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PurchaseReturnOutboundRepository extends InventoryChainRepository
{
    public function transactionsWithBalance($branchId, $supplierId, $limit = null);
}
