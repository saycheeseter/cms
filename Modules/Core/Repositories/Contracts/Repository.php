<?php

namespace Modules\Core\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface Repository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface Repository extends RepositoryInterface
{
    public function findMany($ids, $columns = ['*']);
    public function findOrFail($id, $columns = ['*']);
    public function withTrashed();
}
