<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface DamageRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface DamageRepository extends InvoiceRepository
{

}
