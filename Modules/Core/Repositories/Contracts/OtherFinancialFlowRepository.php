<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface OtherFinancialFlowRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface OtherFinancialFlowRepository extends Repository
{
    public function getSummary($filters = []);
}
