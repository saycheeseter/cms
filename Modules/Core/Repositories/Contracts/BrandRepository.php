<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface BrandRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface BrandRepository extends SystemCodeRepository
{
    public function getBrandMonthlySales($filters = []);
}
