<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface AuditTrailRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface AuditTrailRepository extends Repository
{
    public function search($limit = null);

}
