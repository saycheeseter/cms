<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface AutoProductConversionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface AutoProductConversionRepository extends Repository
{
    public function findProductInInfoTransactions($id, $type, $limit = null);
    public function findProductInDetailTransactions($id, $type, $limit = null);
}