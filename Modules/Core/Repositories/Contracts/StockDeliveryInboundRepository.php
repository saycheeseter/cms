<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockDeliveryInboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockDeliveryInboundRepository extends InventoryChainRepository
{
    
}
