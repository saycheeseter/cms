<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface OtherPaymentRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface OtherPaymentRepository extends OtherFinancialFlowRepository
{

}
