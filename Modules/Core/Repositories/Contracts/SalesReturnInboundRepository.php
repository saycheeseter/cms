<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SalesReturnInboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SalesReturnInboundRepository extends InventoryChainRepository
{
    public function transactionsWithBalance($branchId, $customer, $customerType, $limit = null);
    public function getWalkInCustomers();
}
