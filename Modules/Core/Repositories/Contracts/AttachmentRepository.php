<?php

namespace Modules\Core\Repositories\Contracts;

interface AttachmentRepository extends Repository
{
    public function getByType($type, $id);
}