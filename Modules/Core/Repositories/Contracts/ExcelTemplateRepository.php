<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ExcelTemplateRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ExcelTemplateRepository extends Repository
{
    public function findByModule($id = 0);
}
