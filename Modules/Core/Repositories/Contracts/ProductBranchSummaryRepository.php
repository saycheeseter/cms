<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductBranchSummaryRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductBranchSummaryRepository extends Repository
{
    public function findByFilters($products, $branch, $location);
}
