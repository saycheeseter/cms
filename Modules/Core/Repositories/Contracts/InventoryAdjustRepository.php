<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface InventoryAdjustRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface InventoryAdjustRepository extends Repository
{
    public function findTransactionsByProduct($ids, $type, $limit = null);
    public function getCustomReportData($format, $filters = []);
    public function whereApproved();
}
