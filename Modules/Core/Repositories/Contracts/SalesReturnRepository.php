<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SalesReturnRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SalesReturnRepository extends InvoiceRepository
{

}
