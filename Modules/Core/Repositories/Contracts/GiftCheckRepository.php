<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface GiftCheckRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface GiftCheckRepository extends Repository
{
    public function findValidCheckByNo($check);
}
