<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface UOMRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface UOMRepository extends SystemCodeRepository
{
    //
}
