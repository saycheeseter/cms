<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductSerialNumberRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductSerialNumberRepository extends Repository
{
    public function search($id, $constraint = null, $entity = null, $status = null, $transaction = null, $supplier = null);
    public function getAvailableSerials($productId, $filters = [], $paging = true);
    public function getTransactionReport($id);
    public function findByNumber($number);
}
