<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductStockCardRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductStockCardRepository extends Repository
{
    public function search($id, $limit = null);
}
