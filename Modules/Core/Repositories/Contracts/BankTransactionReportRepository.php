<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface BankTransactionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface BankTransactionReportRepository extends Repository
{
    public function search($filters = []);
    public function getBeginning($filters = []);
}
