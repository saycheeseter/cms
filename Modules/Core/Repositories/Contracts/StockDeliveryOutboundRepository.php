<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockDeliveryOutboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockDeliveryOutboundRepository extends InventoryChainRepository
{
    public function findRemaining($branch, $sheetNumber = '');
    public function retrieve($id);
}
