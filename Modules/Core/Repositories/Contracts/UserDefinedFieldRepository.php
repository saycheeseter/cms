<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface UserDefinedFieldRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface UserDefinedFieldRepository extends Repository
{
    public function findByModule($module);
    public function search($module, $limit);
}
