<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductSupplierRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductSupplierRepository extends Repository
{
    public function search($limit);
}
