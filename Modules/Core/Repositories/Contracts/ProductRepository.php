<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductRepository extends Repository
{
    public function search($filters = [], $paging = true);
    public function filter();
    public function getSupplierPrices($productIds, $supplierId, $branchId, $locationId);
    public function getCustomerPrices($productIds, $customerId, $branchId, $locationId);
    public function getSummariesAndPrices($productIds, $branch, $location);
    public function getSummaries($productIds, $branch, $location);
    public function getSummariesAndLocations($productIds, $branch, $location);
    public function findByBarcode($barcode, $excluded = null);
    public function findByStockNo($stockNo, $excluded = null);
    public function searchByName($filters = []);
    public function alternatives($product, $branch, $location);
    public function components($product);
    public function findGroupsById($ids);
    public function getInventoryWarning($filters = []);
    public function getInventoryValue($filters = []);
    public function getInventoryValueSummary($filters = []);
    public function getPeriodicSales($filters = [], $paging = true);
    public function getBranchInventories($filters = [], $paging = true);
    public function getTransactionList($basicfilters = [], $advanceFilters = [], $paging = true);
    public function whereActiveInBranch();
}