<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface CustomerBalanceFlowRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface BalanceFlowRepository extends Repository
{
    public function search();
    public function getBeginning($date);
}
