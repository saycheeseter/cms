<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface CategoryRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface CategoryRepository extends Repository
{
    public function children($id);
    public function getMonthlySalesRanking($filters = []);
}
