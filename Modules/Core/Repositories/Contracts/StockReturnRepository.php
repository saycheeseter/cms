<?php 

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockReturnRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockReturnRepository extends InvoiceRepository
{

}