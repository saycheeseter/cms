<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PosSalesRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PosSalesRepository extends Repository
{
    public function getTerminalReport($filters = [], $terminals = []);
    public function getDiscountedSummaryReport($filters = []);
    public function getSeniorReport($filters = []);
}
