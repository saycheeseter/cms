<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockReturnInboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockReturnInboundRepository extends InventoryChainRepository
{

}
