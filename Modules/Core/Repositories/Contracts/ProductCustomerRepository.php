<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductCustomerRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductCustomerRepository extends Repository
{
    public function search($limit);
}
