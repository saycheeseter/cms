<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductMonthlyTransactionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductMonthlyTransactionRepository
{
    public function getIncomeStatementSummary($filters);
    public function getProductMonthlySales($filters = []);
}