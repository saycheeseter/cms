<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface UserColumnSettingsRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface UserColumnSettingsRepository extends Repository
{
    public function getByModule($module);
}
