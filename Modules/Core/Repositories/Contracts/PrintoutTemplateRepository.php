<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PrintoutTemplateRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PrintoutTemplateRepository extends Repository
{
    public function findByModule($module);
}
