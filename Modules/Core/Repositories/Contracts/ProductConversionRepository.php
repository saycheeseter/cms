<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ProductConversionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ProductConversionRepository extends Repository
{
    public function getGroupIncrement($id, $limit = null);
    public function getComponentIncrement($id, $limit = null);
    public function getGroupDecrement($id, $limit = null);
    public function getComponentDecrement($id, $limit = null);
    public function findProductInInfoTransactions($id, $type, $limit = null);
    public function findProductInDetailTransactions($id, $type, $limit = null);
}
