<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SalesOutboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SalesOutboundRepository extends InventoryChainRepository
{
    public function transactionsWithBalance($branchId, $customer, $customerType, $limit = null);
    public function getWalkInCustomers();
    public function getTransactionsBySalesmanId($filters = []);
    public function getTransactionProductsBySalesmanId($filters = []);
    public function getNegativeProfitProduct($filters = [], $paging = false);
    public function getDueWarning($filters = [], $all = false);
    public function detailSalesReport($filters = [], $paging = true);
    public function detailSalesReportSummary($filters = []);
}
