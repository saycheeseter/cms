<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface SalesRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface SalesRepository extends InvoiceRepository
{

}
