<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface InvoiceRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface InvoiceRepository extends Repository
{
    public function findByReference($reference, $id = 0);
    public function findRemaining($branch, $sheetNumber = '');
    public function retrieve($id);
}
