<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface CustomerBalanceFlowRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface CustomerBalanceFlowRepository extends BalanceFlowRepository
{
    public function getBalance($id);
}
