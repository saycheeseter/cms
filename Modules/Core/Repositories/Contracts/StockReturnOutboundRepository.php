<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockReturnOutboundRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockReturnOutboundRepository extends InventoryChainRepository
{
    public function findRemaining($branch, $sheetNumber = '');
    public function retrieve($id);
}
