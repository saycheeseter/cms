<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface DataCollectorRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface DataCollectorRepository extends Repository
{
    public function findByReference($reference, $transactionType = NULL);
}