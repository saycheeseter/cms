<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface ReportBuilderRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface ReportBuilderRepository extends Repository
{
    public function findBySlug($slug);
}
