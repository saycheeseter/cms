<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockRequestRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockRequestRepository extends InvoiceRepository
{
    public function findOtherRequestedProducts(array $productIds, $filters, $except = null);
}
