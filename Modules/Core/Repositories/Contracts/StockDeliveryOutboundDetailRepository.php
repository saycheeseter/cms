<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockDeliveryOutboundDetailRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockDeliveryOutboundDetailRepository extends Repository
{
    public function serials($id);
    public function batches($id);
}
