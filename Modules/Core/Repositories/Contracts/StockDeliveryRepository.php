<?php 

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface StockDeliveryRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface StockDeliveryRepository extends InvoiceRepository
{

}