<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface TerminalRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface TerminalRepository extends Repository
{
    public function getByBranch($branch);
}
