<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PaymentMethodRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface PaymentMethodRepository extends SystemCodeRepository
{
    //
}
