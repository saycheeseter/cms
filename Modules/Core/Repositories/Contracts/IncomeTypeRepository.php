<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface IncomeTypeRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface IncomeTypeRepository extends SystemCodeRepository
{
    //
}
