<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface PurchaseRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface InventoryChainRepository extends Repository
{
    public function findByReference($reference, $id = 0);
    public function findTransactionsByProduct($ids, $limit = null);
    public function getCustomReportData($format, $filters = []);
    public function findBySheetNumber($sheetNumber);
    public function findByTransactionDateRangeAndStatuses($dateFrom, $dateTo, $statuses = [], $perPage = 10);
    public function findBySheetNumberRangeAndStatuses($sheets, $statuses = [], $perPage = 10);
    public function whereApproved();
}
