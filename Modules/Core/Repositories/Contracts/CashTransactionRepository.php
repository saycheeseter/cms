<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface CashTransactionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface CashTransactionRepository extends Repository
{
    public function search($filters = []);
    public function getBeginning($filters = []);
}
