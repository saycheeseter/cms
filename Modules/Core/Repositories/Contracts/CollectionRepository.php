<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface CollectionRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface CollectionRepository extends FinancialFlowRepository
{

}