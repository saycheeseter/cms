<?php

namespace Modules\Core\Repositories\Contracts;

/**
 * Interface GenericSettingRepository
 * @package namespace Modules\Core\Repositories\Contracts;
 */
interface GenericSettingRepository extends Repository
{
    //
}
