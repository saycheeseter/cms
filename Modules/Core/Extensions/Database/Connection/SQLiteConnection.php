<?php 

namespace Modules\Core\Extensions\Database\Connection;

use Illuminate\Database\SQLiteConnection as OriginalSQLiteConnection;

class SQLiteConnection extends OriginalSQLiteConnection
{

    /**
     * Get the default schema grammar instance.
     *
     * @return \Illuminate\Database\Schema\Grammars\SQLiteGrammar
     */
    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new SQLiteGrammar);
    }
}
