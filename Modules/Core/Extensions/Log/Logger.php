<?php

namespace Modules\Core\Extensions\Log;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as Writer;

class Logger
{
    protected $logger;

    public function __construct($name, $type = 'daily')
    {
        $file = '';

        switch ($type) {
            case 'single';
                $file = sprintf('%s.log', Str::lower($name));
                break;

            case 'daily':
                $file = sprintf('%s-%s.log',
                    Str::lower($name),
                    Carbon::now()->toDateString()
                );
                break;
        }

        $this->logger = new Writer($name);
        $this->logger->pushHandler(new StreamHandler(storage_path().'/logs/'.$file));
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->logger, $name)) {
            call_user_func_array(array($this->logger, $name), $arguments);
        }
    }

    public function getMonolog()
    {
        return $this->logger;
    }
}