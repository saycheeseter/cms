<?php

namespace Modules\Core\Extensions\Log;

class JobLogger extends Logger
{
    public function __construct($type = 'daily')
    {
        parent::__construct('Jobs', $type);
    }
}