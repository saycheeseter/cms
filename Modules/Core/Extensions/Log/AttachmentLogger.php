<?php

namespace Modules\Core\Extensions\Log;

class AttachmentLogger extends Logger
{
    public function __construct($type = 'daily')
    {
        parent::__construct('Attachments', $type);
    }
}