<?php 

namespace Modules\Core\Extensions\VueInternationalizationGenerator;

use MartinLindhe\VueInternationalizationGenerator\GeneratorProvider as BaseGeneratorProvider;

class GeneratorProvider extends BaseGeneratorProvider
{
    public function boot()
    {
        $this->app->singleton('vue-i18n.generate', function () {
            return new Commands\GenerateInclude;
        });

        $this->commands(
            'vue-i18n.generate'
        );

        $this->publishes([
            base_path('vendor/martinlindhe/laravel-vue-i18n-generator/src/config').'/vue-i18n-generator.php' => config_path('vue-i18n-generator.php'),
        ]);

         $this->mergeConfigFrom(
            base_path('vendor/martinlindhe/laravel-vue-i18n-generator/src/config').'/vue-i18n-generator.php',
            'vue-i18n-generator'
        );
    }
}
