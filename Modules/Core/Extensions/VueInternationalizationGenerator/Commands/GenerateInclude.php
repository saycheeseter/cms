<?php 

namespace Modules\Core\Extensions\VueInternationalizationGenerator\Commands;

use MartinLindhe\VueInternationalizationGenerator\Commands\GenerateInclude as BaseGenerateInclude;
use Modules\Core\Extensions\VueInternationalizationGenerator\Generator;

class GenerateInclude extends BaseGenerateInclude
{
    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        $root = base_path() . config('vue-i18n-generator.langPath');

        $data = (new Generator)
            ->generateFromPath($root);

        $jsFile = base_path() . config('vue-i18n-generator.jsFile');

        file_put_contents($jsFile, $data);

        echo "Written to " . $jsFile . PHP_EOL;
    }
}
