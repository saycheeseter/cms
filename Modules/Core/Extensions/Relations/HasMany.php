<?php

namespace Modules\Core\Extensions\Relations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany as LaravelHasMany;
use DB;
use Illuminate\Support\Collection;

class HasMany extends LaravelHasMany
{
    protected $primaryKey = ['id'];

    public function withPrimaryKey($key)
    {
        $this->primaryKey = array_wrap($key);

        return $this;
    }

    /**
     * Sync the the collection of attributes to the parent model
     *
     * @param  array $attributes
     * @return array
     */
    public function sync($attributes)
    {
        $changes = [
            'attached' => collect([]), 'detached' => collect([]), 'updated' => collect([]),
        ];


        // First we need to retrieve all the model associated with the parent
        // and spin through the given ids
        $originalKeys = collect($this->get($this->primaryKey)->toArray());

        // We need to extract the keys that are currently in the attributes
        // for comparison
        $inputKeys = $this->getAttributesKey($attributes);

        // Next we need to determine what items are missing on the passed attributes
        $detached = $this->getAttributesDiff($originalKeys, $inputKeys);

        // We also need to know if what are the new data to be inserted (all items that are not
        // present in the original keys)
        $attached = $this->getAttributesDiff($inputKeys, $originalKeys);

        // We need to know what attributes are still present or retained
        $updated = $this->getAttributesIntersect($inputKeys, $originalKeys);

        // If there are data to be detached, delete those data
        if ($detached->count() > 0) {
            $deleted = $this->deleteByKeys($detached);
            $changes['detached'] = $deleted;
        }

        // If there are any data to be inserted, get only those data in the attributes
        // that needs to be inserted
        if ($attached->count() > 0) {
            $data = $this->getAttributesByKey($attributes, $attached);
            $created = $this->attachAttributes($data);
            $changes['attached'] = $created;
        }

        // If there are any data to be updated, get only those data in the attributes
        // that needs to be updated
        if ($updated->count() > 0) {
            $data = $this->getAttributesByKey($attributes, $updated);
            $update = $this->updateAttributes($data);
            $changes['updated'] = $update;
        }

        return $changes;
    }

    /**
     * Delete the data by the given keys
     *
     * @param Collection $keys
     */
    protected function deleteByKeys(Collection $keys)
    {
        $that = $this;

        $builder = $this->related->query();

        // We must build the where condition so that we can get the data even if its declared as
        // composite key. This can also be used if there's only one single primary key declared
        $keys->each(function ($item, $key) use ($builder) {
            $builder = $builder->orWhere(function($query) use ($item) {
                foreach ($item as $column => $value) {
                    $query->where($column, $value);
                }
            });
        });

        $data = $builder->get();

        // After getting the data that needs to be deleted, we need to emit the corresponding events
        // based on its cycle to simulate laravel behaviour of event dispatching upon deleting
        $data->each(function ($item, $key) use ($that, $keys) {
            event(sprintf('eloquent.deleting: %s', get_class($item)), $item);

            // Simulate also touch owners condition
            $item->touchOwners();

            // We need to delete the data based on the declared primary key/s. Since
            // if we use the delete method, it will write the query as delete from [table]
            // since laravel doesn't support composite keys
            $builder = $item->newQueryWithoutScopes();

            $keys->each(function ($item, $key) use ($builder) {
                $builder = $builder->orWhere(function($query) use ($item) {
                    foreach ($item as $column => $value) {
                        $query->where($column, $value);
                    }
                });
            });

            if (method_exists($item, 'isForceDeleting') && ! $item->isForceDeleting()) {
                $this->runSoftDelete($item, $builder);
            } else {
                $builder->delete();
            }

            event(sprintf('eloquent.deleted: %s', get_class($item)), $item);
        });

        return $data;
    }

    /**
     * Insert new data based on the given collection
     *
     * @param Collection $data
     */
    protected function attachAttributes(Collection $data)
    {
        $collection = [];

        foreach ($data as $value) {
            $model = $this->related->newModelInstance($value);

            // If data is not declared as auto increment, we need to append the
            // declared primary key in the model instance
            if (!$this->related->incrementing) {
                foreach ($this->primaryKey as $column) {
                    if (!isset($value[$column]) && $column === $this->getForeignKeyName()) {
                        $model->$column = $this->parent->getKey();
                    }
                }
            }

            // Attached the current instance to the parent model
            $collection[] = $this->save($model);

            // Simulate touch implementation
            $model->touchOwners();
        }

        return collect($collection);
    }

    /**
     * Update data based on the given collection
     *
     * @param Collection $data
     */
    protected function updateAttributes(Collection $data)
    {
        $collection = [];

        foreach ($data as $value) {
            $builder = $this->related->query();

            // We must build the query to find the specified data based on
            // the declared primary key/s
            foreach ($this->primaryKey as $column) {
                $builder = $builder->where($column,
                    $column === $this->getForeignKeyName()
                        ? $this->parent->getKey()
                        : $value[$column]
                );

                array_pull($value, $column);
            }

            $instance = $builder->first();

            $instance->fill($value);

            if (! $instance->isDirty()) {
                continue;
            }

            $collection[] = $instance;

            // We must also simulate the events behaviour and timestamps implementation by the laravel models
            event(sprintf('eloquent.updating: %s', get_class($instance)), $instance);

            if ($instance->usesTimestamps()) {
                $instance = $this->setInstanceTimestamps($instance);
            }

            $dirty = $instance->getDirty();

            $builder->update($dirty);

            $instance->touchOwners();

            event(sprintf('eloquent.updated: %s', get_class($instance)), $instance);

            $instance->syncChanges();
        }

        return collect($collection);
    }

    /**
     * Get all items that are not present in the target data
     *
     * @param $base
     * @param $target
     * @return Collection
     */
    protected function getAttributesDiff(Collection $base, Collection $target)
    {
        return $base->filter(function ($item, $key) use ($target) {
            $collection = $target;

            foreach ($item as $column => $value) {
                $collection = $collection->where($column, $value);
            }

            return $collection->count() == 0;
        })->values();
    }

    /**
     * Get all items that are present in the target data
     *
     * @param $base
     * @param $target
     * @return Collection
     */
    protected function getAttributesIntersect(Collection $base, Collection $target)
    {
        return $base->filter(function ($item, $key) use ($target) {
            $collection = $target;

            foreach ($item as $column => $value) {
                $collection = $collection->where($column, $value);
            }

            return $collection->count() === 1;
        })->values();
    }

    /**
     * Get all the keys in the attributes based on the declared primary key
     *
     * @param array $attributes
     * @param $keys
     * @return Collection
     */
    protected function getAttributesKey(array $attributes)
    {
        $that = $this;

        $primaryKey = $this->primaryKey;

        return collect($attributes)->map(function ($item, $key) use ($that, $primaryKey) {
            $temp = [];

            foreach ($primaryKey as $column) {
                $temp[$column] = $column === $that->getForeignKeyName()
                    ? $that->parent->getKey()
                    : $item[$column];
            }

            return $temp;
        });
    }

    /**
     * Get all the items in the attributes based on the given keys
     *
     * @param array $attributes
     * @param Collection $keys
     * @return Collection
     */
    protected function getAttributesByKey(array $attributes, Collection $keys)
    {
        $that = $this;

        $attributes = collect($attributes);

        $primaryKey = $this->primaryKey;

        return $attributes->filter(function ($item, $key) use ($that, $primaryKey, $keys) {
            return $keys->contains(function ($val, $index) use ($that, $primaryKey, $item) {
                $equals = true;

                foreach ($primaryKey as $column) {
                    if ($column === $that->getForeignKeyName()) {
                        continue;
                    } else if ($val[$column] != $item[$column]) {
                        $equals = false;
                        break;
                    }
                }

                return $equals;
            });
        })->values();
    }

    protected function setInstanceTimestamps($instance)
    {
        $time = $instance->freshTimestamp();

        if (! is_null(Model::UPDATED_AT) && ! $instance->isDirty(Model::UPDATED_AT)) {
            $instance->setUpdatedAt($time);
        }

        if (! $instance->exists && ! $instance->isDirty(Model::CREATED_AT)) {
            $instance->setCreatedAt($time);
        }

        return $instance;
    }

    protected function runSoftDelete(&$instance, $query)
    {
        $time = $instance->freshTimestamp();

        $columns = [$instance->getDeletedAtColumn() => $instance->fromDateTime($time)];

        $instance->{$instance->getDeletedAtColumn()} = $time;

        if ($instance->timestamps && ! is_null($instance->getUpdatedAtColumn())) {
            $instance->{$instance->getUpdatedAtColumn()} = $time;

            $columns[$instance->getUpdatedAtColumn()] = $instance->fromDateTime($time);
        }

        $query->update($columns);
    }
}