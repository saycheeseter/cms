<?php

namespace Modules\Core\Extensions\Relations;

use Illuminate\Database\Eloquent\Relations\Pivot as BasePivot;
use Modules\Core\Traits\HasRelationships;
use Modules\Core\Traits\FormatsNumber;
use Modules\Core\Traits\HasAttributes;

class Pivot extends BasePivot
{
    use HasRelationships,
        FormatsNumber,
        HasAttributes;
}