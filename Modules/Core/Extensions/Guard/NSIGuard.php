<?php

namespace Modules\Core\Extensions\Guard;

use Illuminate\Auth\SessionGuard;
use Modules\Core\Entities\Branch;
use DB;
use App;
use Modules\Core\Enums\UserType;

class NSIGuard extends SessionGuard
{
    /**
     * Data buffer for properties
     * 
     * @var array
     */
    protected $configBuffer = [
        'branch' => null,
        'permissions' => null
    ];

    /**
     * List of additional config that can be set
     * 
     * @var array
     */
    protected $config =  [
        'list' => [
            'branch'
        ],
        'methods' => [
            'branch',
            'permissions'
        ]
    ];

    /**
     * Set session for other user config options (Branch, etc.)
     */
    public function setConfig()
    {
        $args = func_get_args();

        for ($i=0; $i < count($args); $i++) {
            if ($args[$i] instanceof Branch) {
                $value = $args[$i]->id;
                $key = 'branch';
            } else {
                continue;
            }

            $this->updateConfigSession($key, $value);
        }
    }

    /**
     * Extends logout function from SessionGuard
     * 
     * @return void
     */
    public function logout()
    {
        parent::logout();

        $this->clearConfigFromStorage();
    }

    /**
     * Overrides default check method, which only checks user. Added permission and 
     * branch checker
     * 
     * @return bool
     */
    public function check()
    {
        return ! (is_null($this->user()) || ! $this->configExist() || $this->isEmptyLicenseKey());
    }

    public function branch()
    {
        $id = $this->session->get($this->salt('branch'));
        $driver = config('auth.providers.branches.driver');

        if (is_null($this->configBuffer['branch'])) {
            switch ($driver) {
                case 'eloquent':
                    $this->configBuffer['branch'] = App::make(config('auth.providers.branches.model'))->findOrFail($id);
                    break;

                case 'database':
                    $this->configBuffer['branch'] = DB::table(config('auth.providers.branches.table'))->find($id);
                    break;
            }
        }

        return $this->configBuffer['branch'];
    }

    public function permissions()
    {
        $table = config('auth.providers.permissions.table');

        $branchId = $this->session->get($this->salt('branch'));
        $userId = $this->id();

        if (is_null($this->configBuffer['permissions'])) {
            $this->configBuffer['permissions'] = DB::table($table)
                ->select([
                    "$table.permission_id",
                    "$table.user_id",
                    "$table.branch_id",
                    "permission.alias"
                ])
                ->join('permission', 'permission.id', 'branch_permission_user.permission_id')
                ->where('user_id', $userId)
                ->where('branch_id', $branchId)
                ->get();
        }

        return $this->configBuffer['permissions'];
    }

    public function loginBranchUsingId($id)
    {
        $this->updateConfigSession('branch', $id);

        return $this->branch();
    }

    public function inSession()
    {
        return $this->user()->session_id === $this->session->getId();
    }

    protected function salt($name)
    {
        return $name.'_'.$this->name.'_'.sha1(static::class);
    }

    protected function configExist()
    {
        foreach ($this->config['list'] as $key => $name) {
            if (is_null($this->session->get($this->salt($name)))) {
                return false;
            }
        }

        return true;
    }

    protected function isEmptyLicenseKey()
    {
        if ($this->user()->type === UserType::SUPERADMIN
            || (config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false)
        ) {
            return false;
        }

        return empty($this->user()->license_key);
    }

    /**
     * Set session
     *
     * @param  string $name
     * @param  string $value
     * @return void
     */
    protected function updateConfigSession($name, $value)
    {
        $this->session->put($this->salt($name), $value);

        $this->session->migrate(true);
    }

    /**
     * Remove permission and branch session
     *
     * @return void
     */
    protected function clearConfigFromStorage()
    {
        for ($i=0; $i < count($this->config['list']); $i++) {
            $this->session->remove($this->salt($this->config['list'][$i]));
        }
    }
}