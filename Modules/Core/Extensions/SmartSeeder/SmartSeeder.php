<?php

namespace Modules\Core\Extensions\SmartSeeder;

use Eighty8\LaravelSeeder\Migration\MigratableSeeder;
use Eighty8\LaravelSeeder\Repository\DisableForeignKeysTrait;
use Modules\Core\Traits\DatabaseTransaction;
use Modules\Core\Traits\ManagesDispatcher;

abstract class SmartSeeder extends MigratableSeeder
{
    use DisableForeignKeysTrait,
        DatabaseTransaction,
        ManagesDispatcher;

    /**
     * Run the database seeder.
     */
    public function run(): void
    {
        $that = $this;

        $this->transaction(function() use ($that) {
            $that->disableDispatcher();
            $that->execute();
            $that->enableDispatcher();
        });
    }

    /**
     * Reverses the database seeder.
     */
    public function down(): void
    {
        $that = $this;

        $this->transaction(function() use ($that) {
            $that->disableDispatcher();
            $that->back();
            $that->enableDispatcher();
        });
    }

    protected abstract function execute();
    protected abstract function back();
}