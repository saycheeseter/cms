<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\Voided;

class PosSalesDetailPresenter extends Presenter
{
    public function isSenior()
    {
        return get_enum_by_value(
            Voided::class,
            $this->model->is_senior
        )->getDescription();
    }

    public function isManageable()
    {
        return get_enum_by_value(
            Voided::class,
            $this->model->manageable
        )->getDescription();
    }

    public function groupTypeLabel()
    {
        return get_enum_by_value(
            GroupType::class,
            $this->model->group_type
        )->getDescription();
    }

    public function productTypeLabel()
    {
        return get_enum_by_value(
            ProductType::class,
            $this->model->product_type
        )->getDescription();
    }

    public function absAddedInventoryValue()
    {
        return number_format($this->model->added_inventory_value->abs()->innerValue(), setting('monetary.precision'));
    }

    public function absAmount()
    {
        return number_format($this->model->amount->abs()->innerValue(), setting('monetary.precision'));
    }

    public function unitBarcode()
    {
        $barcodes = $this->model->barcodes->groupBy('uom_id');

        return $barcodes->has($this->model->unit_id)
            ? $barcodes[$this->model->unit_id]->implode('code', ', ')
            : '';
    }
}