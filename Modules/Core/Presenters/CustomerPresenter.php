<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Status;
use Modules\Core\Enums\CustomerPricingType;
use Lang;

class CustomerPresenter extends Presenter
{
    public function status()
    {
        return $this->model->status === Status::ACTIVE 
            ? Lang::get('core::label.active')
            : Lang::get('core::label.inactive');
    }

    public function pricingType()
    {
        return get_enum_by_value(CustomerPricingType::class, $this->model->pricing_type)->getDescription();
    }
}