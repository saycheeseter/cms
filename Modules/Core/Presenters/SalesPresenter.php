<?php

namespace Modules\Core\Presenters;

class SalesPresenter extends InvoicePresenter
{
    use SalesPresenterTrait;
}
