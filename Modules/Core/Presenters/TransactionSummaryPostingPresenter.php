<?php

namespace Modules\Core\Presenters;

use Illuminate\Support\Str;
use ReflectionClass;

class TransactionSummaryPostingPresenter extends Presenter
{
    public function transactionType()
    {
        $reflection = (new ReflectionClass($this->model->transaction));

        return Str::title(Str::snake($reflection->getShortName(), ' '));
    }
}