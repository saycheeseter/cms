<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Status;
use Lang;

class MemberPresenter extends Presenter
{
    public function status()
    {
        return $this->model->status === Status::ACTIVE 
            ? Lang::get('core::label.active')
            : Lang::get('core::label.inactive');
    }

    public function gender()
    {
        return $this->model->gender == 0 
            ? Lang::get('core::label.male')
            : Lang::get('core::label.female');
    }
}
