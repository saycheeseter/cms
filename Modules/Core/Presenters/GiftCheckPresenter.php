<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\GiftCheckStatus;

class GiftCheckPresenter extends Presenter
{
    protected function status()
    {
        return get_enum_by_value(GiftCheckStatus::class, $this->model->status)->getDescription();
    }
}