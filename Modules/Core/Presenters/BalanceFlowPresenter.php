<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\FlowType;

use Lang;

abstract class BalanceFlowPresenter extends Presenter
{
    public function in()
    {
        return $this->model->flow_type == FlowType::IN 
            ? $this->model->number()->amount
            : number_format(0, setting('monetary.precision'));
    }

    public function out()
    {
        return $this->model->flow_type == FlowType::OUT 
            ? $this->model->number()->amount
            : number_format(0, setting('monetary.precision'));
    }
}