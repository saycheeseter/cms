<?php

namespace Modules\Core\Presenters;

use Lang;
use Modules\Core\Enums\PaymentMethod;

class PurchasePresenter extends InventoryChainPresenter
{
    public function particular()
    {
        return sprintf("%s: %s", 
            Lang::get('core::label.supplier'),
            $this->model->supplier->full_name
        );
    }

    public function payment()
    {
        $label = '';

        foreach (PaymentMethod::getEnumValues() as $key => $method) {
            if ($method->getValue() === $this->model->payment_method) {
                $label = $method->getDescription();
                break;
            }
        }

        return $label;
    }
}
