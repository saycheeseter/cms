<?php

namespace Modules\Core\Presenters;

class DeliveryInboundPresenter extends DeliveryPresenter
{
    public function deliveryFrom()
    {
        return sprintf("%s - %s",
            $this->model->deliveryFrom->name,
            $this->model->deliveryFromLocation->name
        );
    }
}