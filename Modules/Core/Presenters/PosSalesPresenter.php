<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Voided;

class PosSalesPresenter extends Presenter
{
    use SalesPresenterTrait;

    public function isNonVatable()
    {
        return get_enum_by_value(
            Voided::class,
            $this->model->is_non_vat
        )->getDescription();
    }

    public function isWholesale()
    {
        return get_enum_by_value(
            Voided::class,
            $this->model->is_wholesale
        )->getDescription();
    }

    public function voided()
    {
        return get_enum_by_value(
            Voided::class,
            $this->model->voided
        )->getDescription();
    }
}