<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Entities\Collection;
use Modules\Core\Enums\FlowType;
use Lang;

class CustomerBalanceFlowPresenter extends BalanceFlowPresenter
{
    public function particulars()
    {
        if($this->model->transactionable instanceof Collection) {
            return sprintf("%s %s - %s", 
                Lang::get('core::label.collection'),
                $this->model->transactionable->sheet_number,
                $this->model->flow_type == FlowType::IN
                    ? Lang::get('core::label.in')
                    : Lang::get('core::label.out')
            );
        }
    }
}