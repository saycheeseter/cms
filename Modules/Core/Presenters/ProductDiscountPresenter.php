<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\TargetType;
use Modules\Core\Enums\DiscountScheme;
use Modules\Core\Enums\SelectType;
use Modules\Core\Enums\Status;

use Lang;

class ProductDiscountPresenter extends Presenter
{
    public function status()
    {
        return $this->model->status === Status::ACTIVE 
            ? Lang::get('core::label.active')
            : Lang::get('core::label.inactive');
    }

    public function discountScheme()
    {
        $label = '';

        foreach (DiscountScheme::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->discount_scheme) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function targetType()
    {
        $label = '';

        foreach (TargetType::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->target_type) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function selectType()
    {
        $label = '';

        foreach (SelectType::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->select_type) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }
}