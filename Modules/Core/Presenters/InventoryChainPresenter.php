<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\TransactionType;

class InventoryChainPresenter extends TransactionPresenter
{
    public function invoiceReference()
    {
        return $this->model->reference ? $this->model->reference->sheet_number : '';
    }

    public function transactionType()
    {
        $label = '';

        foreach (TransactionType::getEnumValues() as $key => $type) {
            if ($type->getValue() === $this->model->transaction_type) {
                $label = $type->getDescription();
                break;
            }
        }

        return $label;
    }
}
