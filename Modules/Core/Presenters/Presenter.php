<?php 

namespace Modules\Core\Presenters;

use Illuminate\Support\Str;
use Exception;

abstract class Presenter
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }
    
    public function __get($key)
    {
        $method = Str::camel($key);
        if (!method_exists($this, $method)) {
            throw new Exception("Property $key doesn't exist.");
        }

        return call_user_func([$this, $method]);
    }
}