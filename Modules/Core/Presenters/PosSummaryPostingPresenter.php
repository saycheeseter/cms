<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\PosSummaryPostingStatus;
use Lang;

class PosSummaryPostingPresenter extends Presenter
{
    public function status()
    {
        switch ($this->model->status) {
            case PosSummaryPostingStatus::IDLE:
                return Lang::get('core::label.idle');
                break;

            case PosSummaryPostingStatus::GENERATING:
                return Lang::get('core::label.generating');
                break;

            default:
                return '';
                break;
        }
    }
}