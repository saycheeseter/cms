<?php

namespace Modules\Core\Presenters;

use Lang;

class InventoryAdjustPresenter extends TransactionPresenter
{
    public function particular()
    {
        return sprintf("%s: %s", 
            Lang::get('core::label.request.by'),
            $this->model->requested->full_name
        );
    }
}
