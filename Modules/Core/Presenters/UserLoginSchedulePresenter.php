<?php

namespace Modules\Core\Presenters;

use Carbon\Carbon;

class UserLoginSchedulePresenter extends Presenter
{
    public function start()
    {
        return Carbon::parse($this->model->start)->toTimeString();
    }

    public function end()
    {
        return Carbon::parse($this->model->end)->toTimeString();
    }
}
