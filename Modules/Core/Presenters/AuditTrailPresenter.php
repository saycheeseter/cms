<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\AuditEvent;
use Illuminate\Database\Eloquent\Relations\Relation;
use Lang;
use DateTime;
use Litipk\BigNumbers\Decimal;
use App;

class AuditTrailPresenter extends Presenter
{   
    public function action()
    {
        $label = '';

        foreach (AuditEvent::getEnumValues() as $key => $event) {
            if ($event->getValue() === $this->model->event) {
                $label = $event->getDescription();
                break;
            }
        }

        return $label;
    }

    public function message()
    {
        $message = '';

        switch ($this->model->event) {
            case AuditEvent::CREATED:
                $message = $this->parseCreateMessage();
                break;
            case AuditEvent::UPDATED:
                $message = $this->parseUpdateMessage();
                break;
            case AuditEvent::DELETED:
                $message = $this->parseDeleteMessage();
                break;
        }

        return $message;

    }

    public function module()
    {
        return ucwords(str_replace("_", " " , $this->model->auditable_type));
    }

    private function parseKeyAsReadable($key)
    {
        return ucwords(str_replace("_", " " , $key));
    }

    private function parseCreateMessage() 
    {
        return sprintf("Created Data: %s %s", $this->getAuditTags(), $this->parseAuditNewValue($this->model->new_values));
    }

    private function parseUpdateMessage() 
    {
        return sprintf("Updated Data: %s %s", $this->getAuditTags(), $this->parseAuditNewValue($this->model->new_values));
    }

    private function parseDeleteMessage() 
    {
        return sprintf("Deleted Data: %s %s", $this->getAuditTags(), $this->parseAuditNewValue($this->model->old_values));
    }

    private function parseAuditNewValue($values)
    {   
        $parsed = '';

        foreach ($values as $key => $value) {
            if(strlen($parsed) > 0) {
                $parsed = sprintf("%s,", $parsed);
            }

            if(is_array($value)) {
                $value = (empty($value)) ? '' : $value;
                $value = isset($value['date']) ? $value['date'] : '';
            }

            if(!in_array($key, ['id'])) {
                $parsed = sprintf("%s %s : %s", $parsed, $this->parseKeyAsReadable($key), $value);
            }
        }

        return $parsed;
    }

    private function getAuditTags()
    {
        return ($this->model->tags) ? sprintf('[%s]', implode(",", $this->model->tags) ) : '';
    }
}
