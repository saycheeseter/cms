<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\Manageable;
use Modules\Core\Enums\ProductType;

class TransactionDetailPresenter extends BarcodeUnitPresenter
{
    public function unitBarcode()
    {
        $barcodes = $this->model->barcodes->groupBy('uom_id');

        return $barcodes->has($this->model->unit_id)
            ? $barcodes[$this->model->unit_id]->implode('code', ', ')
            : '';
    }

    public function computedUnitBarcode()
    {
        $barcodes = $this->model->barcodes->groupBy('uom_id');

        return $barcodes->has($this->model->computed_unit_id)
            ? $barcodes[$this->model->computed_unit_id]->implode('code', ', ')
            : '';
    }

    public function discounts()
    {
        return $this->discounts;
    }

    public function decimalDiscounts()
    {
        $discounts = [];

        foreach ($this->non_zero_discounts as $discount) {
            $discounts[] = number_format($discount->innerValue(), setting('monetary.precision'));
        }

        return implode(', ', $discounts);
    }

    public function wholeNumberDiscounts()
    {
        $discounts = [];

        foreach ($this->non_zero_discounts as $discount) {
            $discounts[] = number_format($discount->innerValue(), 0);
        }

        return implode(', ', $discounts);
    }

    public function nonZeroDiscounts()
    {
        $discounts = [
            $this->model->discount1,
            $this->model->discount2,
            $this->model->discount3,
            $this->model->discount4,
        ];

        for ($i = count($discounts) - 1; $i >= 0; $i--) {
            if ((int) $discounts[$i]->innerValue() <= 0) {
                unset($discounts[$i]);
            }
        }

        return $discounts;
    }

    public function decimalUnitCodeAndSpecs()
    {
        return sprintf('%s(%s)',
            $this->model->unit->code,
            $this->model->number()->unit_qty
        );
    }

    public function wholeNumberUnitCodeAndSpecs()
    {
        return sprintf('%s(%s)',
            $this->model->unit->code,
            number_format($this->model->unit_qty->innerValue(), 0)
        );
    }

    public function productType()
    {
        $label = '';

        foreach (ProductType::getEnumValues() as $key => $type) {
            if ($type->getValue() == $this->model->product_type) {
                $label = $type->getDescription();
                break;
            }
        }

        return $label;
    }

    public function groupType()
    {
        $label = '';

        foreach(GroupType::getEnumValues() as $key => $type){
            if($type->getValue() == $this->model->group_type){
                $label = $type->getDescription();
                break;
            }
        }

        return $label;
    }

    public function manageable()
    {
        $label = '';

        foreach(Manageable::getEnumValues() as $key => $value){
            if($value->getValue() == $this->model->manageable){
                $label = $value->getDescription();
            }
        }

        return $label;
    }
}
