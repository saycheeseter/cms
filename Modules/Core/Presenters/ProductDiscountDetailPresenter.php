<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\SelectType;

use Lang;

class ProductDiscountDetailPresenter extends Presenter
{
    public function entityTypeLabel()
    {
        $label = '';

        foreach (SelectType::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->discount->select_type) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function entityName()
    {
        return $this->model->entity->name ?? ($this->model->entity->full_name ?? Lang::get('core::label.all.products'));
    }
}