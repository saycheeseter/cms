<?php 

namespace Modules\Core\Presenters;

use Exception;

class UDFFormat extends Presenter
{
    public function all()
    {
        $udfs = [];

        foreach ($this->model->getUDFColumns() as $key => $value) {
            $udfs[$key] = $this->formatAttribute($key);
        }

        return $udfs;
    }

    protected function formatAttribute($key)
    {
        $type = $this->model->getUDFColumns()[$key];

        switch ($type) {
            case 'decimal':
                return $this->model->number()->{$key};
                break;

            case 'date':
                return $this->model->{$key} ? $this->model->{$key}->toDateString() : null;
                break;

            case 'datetime':
                return $this->model->{$key} ? $this->model->{$key}->toDateTimeString() : null;
                break;

            default:
                return $this->model->{$key};
                break;
        }
    }

    public function __get($key)
    {
        if (!$this->model->isUDFColumn($key)) {
            throw new Exception("$key is not a valid User Definend Field.");
        }

        return $this->formatAttribute($key);
    }
}