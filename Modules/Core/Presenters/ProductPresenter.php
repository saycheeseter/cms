<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\GroupType;

class ProductPresenter extends BarcodeUnitPresenter
{
    public function createdByAndDate()
    {
        return sprintf("%s - %s", 
            $this->model->creator->full_name ?? '',
            $this->model->created_at
        );
    }

    public function modifiedByAndDate()
    {
        return sprintf("%s - %s", 
            $this->model->modifier->full_name ?? '',
            $this->model->updated_at
        );
    }

    public function typeLabel()
    {
        return get_enum_by_value(ProductType::class, $this->model->type)->getDescription();
    }

    public function groupTypeLabel()
    {
        return get_enum_by_value(GroupType::class, $this->model->group_type)->getDescription();
    }
}