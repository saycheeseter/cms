<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Customer;

use Lang;

class CollectionPresenter extends TransactionPresenter
{
    public function customer()
    {
        return $this->model->customer_type == Customer::WALK_IN 
            ? $this->model->customer_walk_in_name
            : $this->model->customer->name;
    }

    public function customer_type()
    {
        return $this->model->customer_type == Customer::WALK_IN
            ? Lang::get('core::label.walk.in.customer')
            : Lang::get('core::label.regular.customer');
    }
}
