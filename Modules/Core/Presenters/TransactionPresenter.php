<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\TransactionType;
use Lang;

class TransactionPresenter extends Presenter
{
    public function approval()
    {
        $label = '';

        foreach (ApprovalStatus::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->approval_status) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function transaction()
    {
        $label = '';

        foreach (TransactionStatus::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->transaction_status) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }
    
    public function deleted()
    {
        return is_null($this->model->deleted_at)
            ? Lang::get('core::label.no')
            : Lang::get('core::label.yes');
    }
}