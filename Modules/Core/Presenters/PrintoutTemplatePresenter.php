<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\ModuleDefinedFields;

class PrintoutTemplatePresenter extends Presenter
{
    public function module()
    {
        $label = '';

        foreach (ModuleDefinedFields::getEnumValues() as $key => $module) {
            if ($module->getValue() === $this->model->module_id) {
                $label = $module->getDescription();
                break;
            }
        }

        return $label;
    }
}
