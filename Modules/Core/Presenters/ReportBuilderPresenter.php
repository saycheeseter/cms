<?php

namespace Modules\Core\Presenters;
use Modules\Core\Enums\ModuleGroup;

class ReportBuilderPresenter extends Presenter
{
    public function moduleGroup()
    {   
        $description = '';

        foreach (ModuleGroup::getEnumValues() as $id) {
            if ($id->getValue() == $this->model->group_id) {
                $description = $id->getDescription();
                break;
            }
        }

        return $description;
    }

}
