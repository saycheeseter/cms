<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Customer;
use Lang;

trait SalesPresenterTrait
{
    public function customerAndBranch()
    {
        if ($this->model->customer_type == Customer::WALK_IN) {
            return '-';
        }

        return sprintf('%s - %s',
            $this->model->customer->name,
            $this->model->customerDetail->name
        );
    }

    public function customer()
    {
        return $this->model->customer_type == Customer::WALK_IN
            ? $this->model->customer_walk_in_name
            : $this->customerAndBranch();
    }

    public function customerAddress()
    {
        return $this->model->customer_type == Customer::WALK_IN
            ? '-'
            : $this->model->customerDetail->address;
    }

    public function particular()
    {
        $value = $this->model->customer_type == Customer::WALK_IN
            ? $this->model->customer_walk_in_name
            : $this->model->customer->name;

        return sprintf("%s: %s",
            Lang::get('core::label.customer'),
            $value
        );
    }

    public function customerTypeLabel()
    {
        return $this->model->customer_type == Customer::REGULAR
            ? Lang::get('core::label.regular.customer')
            : Lang::get('core::label.walk.in.customer');
    }
}
