<?php

namespace Modules\Core\Presenters;

class DeliveryOutboundPresenter extends DeliveryPresenter
{
    public function deliverTo()
    {
        return sprintf("%s - %s",
            $this->model->deliverTo->name,
            $this->model->deliverToLocation->name
        );
    }
}