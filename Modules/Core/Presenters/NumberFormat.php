<?php 

namespace Modules\Core\Presenters;

use Illuminate\Database\Eloquent\Model;
use Litipk\BigNumbers\Decimal;
use Exception;

class NumberFormat extends Presenter
{
    public function __construct($model)
    {
        if (!$model instanceof Model) {
            throw new Exception("Class $model must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        $this->model = $model;
    }

    /**
     * Parse nullable decimal fields to 0
     * 
     * @return Decimal
     */
    private function parseAttribute($value, $key)
    {
        // Only parse the field if 
        // - Null
        // - Set as decimal
        if (is_null($value) 
            && ((array_key_exists($key, $this->model->getCasts())
                    && $this->model->getCasts()[$key] === 'decimal')
                || $this->model->getUDFColumns()[$key] === 'decimal')
        ){
            return Decimal::create(0, setting('monetary.precision'));
        }

        return $value;
    }

    public function __get($key)
    {
        $attribute = $this->model->getAttribute($key);
        $attribute = $this->parseAttribute($attribute, $key);
        
        if (!$attribute instanceof Decimal) {
            throw new Exception("Key $key must be an instance of Litipk\\BigNumbers\\Decimal");
        }

        return number_format($attribute->innerValue(), setting('monetary.precision'));
    }
}