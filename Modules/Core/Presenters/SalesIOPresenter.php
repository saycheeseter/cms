<?php

namespace Modules\Core\Presenters;

class SalesIOPresenter extends InventoryChainPresenter
{
    use SalesPresenterTrait;
}