<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\DataCollectorTransactionType;
use Modules\Core\Enums\TransactionSource;
use Lang;

class DataCollectorTransactionPresenter extends Presenter
{
    public function transactionLabel()
    {
        return get_enum_by_value(DataCollectorTransactionType::class, $this->model->transaction_type)->getDescription();
    }

    public function sourceLabel()
    {
        return get_enum_by_value(TransactionSource::class, $this->model->source)->getDescription();
    }
}