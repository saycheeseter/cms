<?php 

namespace Modules\Core\Presenters;

abstract class BarcodeUnitPresenter extends Presenter
{
    public function unitList()
    {
        return $this->model->units->map(function ($item) {
            return array(
                'id'        => $item->uom_id,
                'label'     => $item->uom->name,
                'attribute' => $item->number()->qty
            );
        })->values()->toArray();
    }

    public function barcodeList()
    {
        return $this->model->barcodes->groupBy('uom_id')->map(function ($item) {
            return $item->implode('code', ', ');
        })->toArray();
    }

    public function barcodes()
    {
        return $this->model->barcodes->implode('code', ', ');
    }
}