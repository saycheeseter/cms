<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\GroupType;

use Lang;

class ProductConversionPresenter extends TransactionPresenter
{
    public function groupTypeLabel()
    {
        return get_enum_by_value(GroupType::class, $this->model->group_type)->getDescription();
    }

    public function particular()
    {
        return sprintf("%s: %s", 
            Lang::get('core::label.created.by'),
            $this->model->creator->full_name
        );
    }
}