<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Entities\Branch;

class BranchPresenter extends Presenter
{
    public function fullName()
    {
        return sprintf("%s - %s", 
            $this->model->code,
            $this->model->name
        );
    }
}