<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Entities\PosSalesDetail;
use Modules\Core\Entities\SalesOutboundDetail;
use Modules\Core\Entities\SalesReturnInboundDetail;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ModuleGroup;
use Modules\Core\Traits\HasMorphMap;
use Lang;

class AutoProductConversionPresenter extends Presenter
{
    use HasMorphMap;

    public function groupTypeLabel()
    {
        return get_enum_by_value(GroupType::class, $this->model->group_type)->getDescription();
    }

    public function transactionLabel()
    {
        switch ($this->model->transaction_type) {
            case $this->getMorphMapKey(SalesOutboundDetail::class):
                return Lang::get('core::label.sales.outbound');
                break;

            case $this->getMorphMapKey(PosSalesDetail::class):
                return Lang::get('core::label.pos.sales');
                break;

            case $this->getMorphMapKey(SalesReturnInboundDetail::class):
                return Lang::get('core::label.sales.return.inbound');
                break;
        }
    }

    public function module()
    {
        switch ($this->model->transaction_type) {
            case $this->getMorphMapKey(SalesOutboundDetail::class):
                return ModuleGroup::SALES_OUTBOUND;
                break;

            case $this->getMorphMapKey(SalesReturnInboundDetail::class):
                return ModuleGroup::SALES_RETURN_INBOUND;
                break;

            case $this->getMorphMapKey(PosSalesDetail::class):
                return ModuleGroup::POS;
                break;
        }
    }

    public function referenceNumber()
    {
        $reference = $this->model->transaction->transaction;

        switch ($this->model->transaction_type) {
            case $this->getMorphMapKey(SalesOutboundDetail::class):
            case $this->getMorphMapKey(SalesReturnInboundDetail::class):
                return $reference->sheet_number;
                break;

            case $this->getMorphMapKey(PosSalesDetail::class):
                return $reference->or_number;
                break;
        }
    }
}