<?php

namespace Modules\Core\Presenters;

use Lang;

class DeliveryPresenter extends InventoryChainPresenter
{
    public function particular()
    {
        return sprintf("%s: %s | %s: %s", 
            Lang::get('core::label.branch'),
            $this->model->for->name,
            Lang::get('core::label.for.location'),
            $this->model->location->name
        );
    }
}
