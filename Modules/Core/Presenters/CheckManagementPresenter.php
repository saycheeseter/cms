<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\CheckStatus;
use Lang;

class CheckManagementPresenter extends Presenter
{
    public function checkStatus()
    {
        return ($this->model->status == CheckStatus::PENDING)
            ? Lang::get('core::label.pending')
            : Lang::get('core::label.transferred');
    }
}
