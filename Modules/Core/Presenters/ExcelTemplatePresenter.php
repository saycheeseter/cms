<?php

namespace Modules\Core\Presenters;
use Modules\Core\Enums\ModuleDefinedFields;

class ExcelTemplatePresenter extends Presenter
{
    public function module()
    {   
        $description = '';

        foreach (ModuleDefinedFields::getEnumValues() as $id) {
            if ($id->getValue() == $this->model->module) {
                $description = $id->getDescription();
                break;
            }
        }

        return $description;
    }

}
