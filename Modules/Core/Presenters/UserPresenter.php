<?php

namespace Modules\Core\Presenters;

use Modules\Core\Enums\Status;
use Lang;

class UserPresenter extends Presenter
{
    public function status()
    {
        return $this->model->status === Status::ACTIVE 
            ? Lang::get('core::label.active')
            : Lang::get('core::label.inactive');
    }
}
