<?php

namespace Modules\Core\Presenters;

use Lang;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\ApprovalStatus;

class DetailSalesReportPresenter extends Presenter
{
    public function approval()
    {
        $label = '';

        foreach (ApprovalStatus::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->approval_status) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function deleted()
    {
        return is_null($this->model->deleted_at)
            ? Lang::get('core::label.no')
            : Lang::get('core::label.yes');
    }

    public function customerType()
    {
        return $this->model->customer_type == Customer::WALK_IN
            ? Lang::get('core::label.walk.in.customer')
            : Lang::get('core::label.regular.customer');
    }
}