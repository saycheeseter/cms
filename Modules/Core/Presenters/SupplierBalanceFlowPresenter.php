<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Entities\Payment;
use Modules\Core\Enums\FlowType;
use Lang;

class SupplierBalanceFlowPresenter extends BalanceFlowPresenter
{
    public function particulars()
    {
        if($this->model->transactionable instanceof Payment) {
            return sprintf("%s %s - %s", 
                Lang::get('core::label.payment'),
                $this->model->transactionable->sheet_number,
                $this->model->flow_type == FlowType::IN
                    ? Lang::get('core::label.in')
                    : Lang::get('core::label.out')
            );
        }
    }
}