<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\TransactionSummary;
use Modules\Core\Entities\ProductStockCard;

class ProductStockCardPresenter extends Presenter
{
    public function referenceName()
    {
        $enum = get_enum_by_value(TransactionSummary::class, $this->model->reference_type);

        return $enum->getDescription();
    }
}