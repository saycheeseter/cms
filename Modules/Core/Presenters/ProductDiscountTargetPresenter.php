<?php 

namespace Modules\Core\Presenters;

use Modules\Core\Enums\TargetType;

use Lang;

class ProductDiscountTargetPresenter extends Presenter
{
    public function targetTypeLabel() 
    {
        $label = '';

        foreach (TargetType::getEnumValues() as $key => $status) {
            if ($status->getValue() === $this->model->discount->target_type) {
                $label = $status->getDescription();
                break;
            }
        }

        return $label;
    }

    public function targetName()
    {
        return $this->model->target->name ?? ($this->model->target->full_name ?? '');
    }
}