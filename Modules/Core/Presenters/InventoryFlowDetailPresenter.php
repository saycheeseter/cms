<?php

namespace Modules\Core\Presenters;

class InventoryFlowDetailPresenter extends TransactionDetailPresenter
{
    public function absAddedInventoryValue()
    {
        return number_format($this->model->added_inventory_value->abs()->innerValue(), setting('monetary.precision'));
    }

    public function absAmount()
    {
        return number_format($this->model->amount->abs()->innerValue(), setting('monetary.precision'));
    }
}