@extends('core::invoice.detail')
@section('title', trans('core::label.purchase.return.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.supplier'):</td>
                <td>
                    <chosen
                        class="required"
                        name='supplier'
                        v-model='info.supplier_id' 
                        :options='reference.tsuppliers'
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.payment.method'):</td>
                <td>
                    <chosen
                        class="required"
                        name='payment-method'
                        v-model='info.payment_method'
                        :options='reference.paymentMethods'
                        :disabled='!updatable'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.term'):</td>
                <td><number-input name='term' v-model='info.term' :disabled='!updatable' :precision='false'></number-input></td>
            </tr>
        </table>
    </div>
@stop

@push('modals')
    <dialog-box
        :show='autoGroupProduct.dialog.message !=""'
        @close='breakToComponents'>
        <template slot='content'>@{{ autoGroupProduct.dialog.message }}</template>
    </dialog-box>
@endpush

@push('script')
    @javascript('tsuppliers', $tsuppliers)
    @javascript('paymentMethods', $paymentMethods)

    <script src="{{ elixir('js/PurchaseReturn/detail.js') }}"></script>
@endpush