<div class="mask page" v-show="loading">
    <img src="{{ asset('images/loading.gif') }}">
</div>

<div class="mask processing" v-show="processing">
    <img src="{{ asset('images/loading.gif') }}">
</div>