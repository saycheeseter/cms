<modal
    class="fixed-height"
    v-bind:id="modal.detail.id"
    v-show="modal.detail.visible"
    v-on:open="modal.detail.visible = true"
    v-on:close="modal.detail.visible = false">
    <template slot="header">
        @lang('core::label.member.rate.detail')
    </template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="message.detail.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.detail.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.detail.success != ''">
                <span v-text="message.detail.success"></span>
            </message>
            <message type="info" :show="message.detail.info != ''">
                <span v-text="message.detail.info"></span>
            </message>
        </div>
        <div class="c-content-group c-b">
            <table id="modal-head" class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.name'):</td>
                    <td><input class="required" name='name' type="text" v-model="rate.info.name"></td>
                    <td valign="top">
                        <label class="radio-chkbx">
                            <input name="active" type="checkbox" v-model="rate.info.status" value="1">
                            <span>@lang('core::label.active')</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.memo'):</td>
                    <td colspan="2">
                        <textarea name='memo' rows="3" v-model="rate.info.memo"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div class="tbl-member-rate flex flex-column flex-1 c-has-bl c-has-br overflow-hidden">
            <datatable
                v-bind:tableId="datatable.detail.id"
                v-bind:settings="datatable.detail.settings"
                v-bind:table="datatable.detail.value"
                v-on:destroy="confirmDetail"
                v-on:update="updateDetail"
                ref="detail">
                <template slot="header">
                    <table-header>@lang('core::label.amount.from')</table-header>
                    <table-header>@lang('core::label.amount.to')</table-header>
                    <table-header>@lang('core::label.date.from')</table-header>
                    <table-header>@lang('core::label.date.to')</table-header>
                    <table-header>@lang('core::label.discount')</table-header>
                    <table-header>@lang('core::label.amount.per.point')</table-header>
                    <table-header></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input class="required" v-model="row.data.amount_from" @keydown.native="changeState(row.index, 'update', 'detail')" />
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input class="required" type="text" v-model="row.data.amount_to" @keydown.native="changeState(row.index, 'update', 'detail')" />
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'date-from-' + row.index" class="date-from" format="yyyy-MM-dd" v-model='row.data.date_from' v-on:selected="changeState(row.index, 'update', 'detail')"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'date-to-' + row.index" class="date-to" format="yyyy-MM-dd" v-model='row.data.date_to' v-on:selected="changeState(row.index, 'update', 'detail')"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--percent">
                        <number-input class="required" type="text" v-model="row.data.discount" max=100 @keydown.native="changeState(row.index, 'update', 'detail')" />
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input class="required" type="text" v-model="row.data.amount_per_point" @keydown.native="changeState(row.index, 'update', 'detail')" />
                    </td>
                </template>
                @if ($permissions['create']))
                    <template slot="input">
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                            <number-input class="required" v-focus v-model="rate.detail.amount_from" />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                            <number-input class="required" v-model="rate.detail.amount_to" />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                            <datepicker id="date-from" class="date-from" format="yyyy-MM-dd" v-model='rate.detail.date_from'></datepicker>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                            <datepicker id="date-to" class="date-to" format="yyyy-MM-dd" v-model='rate.detail.date_to'></datepicker>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--percent">
                            <number-input class="required" v-model="rate.detail.discount" max=100 />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                            <number-input class="required" v-model="rate.detail.amount_per_point" />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                            <img src="{{ asset('images/iconupdate.png') }}" @click="createDetail()">
                        </table-input>
                    </template>
                @endif
            </datatable>
        </div>
    </template>
    <template slot="footer">
        <button id="save" class="c-btn c-dk-green" @click="save">@lang('core::label.save')</button>
    </template>
</modal>
