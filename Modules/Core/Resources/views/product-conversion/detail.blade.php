@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.conversion.detail'))

@section('stylesheet')
    <style type="text/css">
        .vdp-datepicker__calendar {
            right: 0px;
        }
    </style>
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td width="76px">@lang('core::label.sheet.no'):</td>
                    <td><span name="sheet-no"><b>@{{ info.sheet_number }}</b></span></td>
                </tr>
                <tr>
                    <td width="86px">@lang('core::label.created.for'):</td>
                    <td>
                        <chosen
                            v-model='info.created_for'
                            :options='references.branches'
                            @change='getLocations()'
                            :disabled='true'
                        ></chosen>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.for.location'):</td>
                    <td>
                        <chosen
                            name='for-location'
                            v-model='info.for_location'
                            :options='references.locations'
                            :disabled='disabled'
                        ></chosen>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.product.name'):</td>
                    <td width="200px"><input type='text' v-model='info.product_name' disabled="true"></td>
                    <td>
                        <button class="c-btn c-dk-blue c-mgl-5" @click="productPicker.visible = true" v-show='!disabled'>
                            <i class="fa fa-list-alt"></i>
                            @lang('core::label.product.selector')
                        </button>
                    </td>
                </tr>
            </table>
            <div class="c-content-group">
                <div class="pull-left">
                    <table class="c-tbl-layout">
                        <tr>
                            <td width="86px">@lang('core::label.group.type'):</td>
                            <td width="130px"><b><span>@{{ info.group_type_label }}</span></b></td>
                            <td width="86px">@lang('core::label.barcode'):</td>
                            <td width="130px"><b><span>@{{ info.barcode }}</span></b></td>
                        </tr>
                        <tr>
                            <td width="86px">@lang('core::label.stock.no'):</td>
                            <td width="130px"><span><b>@{{ info.stock_number }}</b></span></td>
                            <td width="86px">@lang('core::label.qty'):</td>
                            <td width="130px"><input type="text" class="field__qty" v-model='info.qty' :disabled='disabled'></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.transaction.date'):</td>
                    <td>
                        <date-time-picker
                            id='main-info-datetimepicker'
                            name='date-time'
                            v-model='info.transaction_date'
                            :date="datepickerFormat"
                            :disabled='disabledDate'
                            :left='true'>
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <control-bar id="controls">
        <template slot="left">
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.details.visible = true">
                {{-- @lang('core::label.column.settings') --}}
            </control-button>
        </template>
        <template slot="right">
            <control-button name='save-add-new' v-if="controls.draft" icon="fa fa-save" @click='process(true)'>
                @lang('core::label.save.and.add.new')
            </control-button>
            <control-button name='save' icon="fa fa-save" v-if="controls.draft" @click="process">
                {{-- @lang('core::label.save') --}}
            </control-button>
            @if($permissions['update'])
                <control-button name='for-approval' icon="fa fa-thumbs-o-up" v-if="controls.forApproval" @click="forApprovalTransaction()">
                    @lang('core::label.for.approval')
                </control-button>
            @endif

            @if($permissions['approve'])
                <control-button name='approve' icon="fa fa-check fa-green" v-if="controls.approveDecline" @click="approveTransaction()">
                    @lang('core::label.approve')
                </control-button>
            @endif

            @if($permissions['decline'])
                <control-button name='decline' icon="fa fa-close fa-red" v-if="controls.approveDecline" @click="declineTransaction()">
                    @lang('core::label.decline')
                </control-button>
            @endif

            @if($permissions['decline'] || $permissions['approve'])
                <control-button name='revert' v-if="controls.revert" icon="fa fa-arrow-circle-left" @click="revertTransaction()">
                    @lang('core::label.revert')
                </control-button>
            @endif
        </template>
    </control-bar>
    <div id='status-label' class="label status-label" :class='approvalClass'>@{{ info.approval_status_label }}</div>
    <datatable
        v-bind:tableId="datatable.detail.id"
        v-bind:settings="datatable.detail.settings"
        v-bind:table="datatable.detail.value">
        <template slot="header">
            <table-header>@lang('core::label.product.name')</table-header>
            <table-header v-show='columnSettings.details.selections.stock_number'>@lang('core::label.stock.no')</table-header>
            <table-header v-show='columnSettings.details.selections.barcode'>@lang('core::label.barcode')</table-header>
            <table-header v-show='columnSettings.details.selections.required_qty'>@lang('core::label.required.qty')</table-header>
            <table-header v-show='columnSettings.details.selections.allocated_qty'>@lang('core::label.allocated.qty')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stock_number'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.details.selections.required_qty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.required_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.details.selections.allocated_qty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.allocated_qty }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.transaction.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.transaction.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.transaction.success != ''">
        <span v-text="message.transaction.success"></span>
    </message>
    <message type="info" :show="message.transaction.info != ''">
        <span v-text="message.transaction.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-selector')

    <column-select 
        :columns='columnSettings.details.columns'
        v-model='columnSettings.details.selections'
        v-on:close='columnSettings.details.visible = false'
        v-show='columnSettings.details.visible'
        :module='columnSettings.details.module'
        id='details-column-modal'>
    </column-select>

    <dialog-box
        :show='dialog.process.message !=""'
        @close='redirect'>
        <template slot='content'>@{{ dialog.process.message }}</template>
    </dialog-box>

    <modal 
        v-show='dialog.directApproval.visible'
        @close='redirect'>
        <template slot="header">
            @{{ dialog.directApproval.message }}
        </template>
        <template slot="content">
            @lang('core::info.direct.approve')
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-blue" @click='directApproval()'>@lang('core::label.approve')</button>
            <button class="c-btn c-dk-green" ref="saveOnly" @click='redirect'>@lang('core::label.proceed')</button>
        </template>
    </modal>
@stop

@section('script')
    @javascript('permissions', $permissions)
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('id', Request::segment(2))

    @stack('script')

    <script src="{{ elixir('js/ProductConversion/detail.js') }}"></script>
@stop