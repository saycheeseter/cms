@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.conversion.list'))

@section('stylesheet')
@stop

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            @if($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.product.conversion')
                </control-button>
            @endif
            <control-button name='filters' icon="fa fa-search" @click="filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
        <template slot="right">
            <control-button name='export-excel' icon="fa fa-file-excel-o" @click="excel">
                @lang('core::label.export.excel')
            </control-button>
            @if($permissions['approve'])
                <control-button name='approve' icon="fa fa-check fa-green" @click="approveTransaction()">
                    @lang('core::label.approve')
                </control-button>
            @endif
            @if($permissions['decline'])
                <control-button name='decline' icon="fa fa-close fa-red" @click="declineTransaction()">
                    @lang('core::label.decline')
                </control-button>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirmation"
        v-model="datatable.selected">
        <template slot="header">
            <table-header v-show='columnSettings.selections.approval_status'>@lang('core::label.approval.status')</table-header>
            <table-header v-show='columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header v-show='columnSettings.selections.created_from'>@lang('core::label.created.from')</table-header>
            <table-header v-show='columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
            <table-header v-show='columnSettings.selections.for_location'>@lang('core::label.for.location')</table-header>
            <table-header v-show='columnSettings.selections.product'>@lang('core::label.product')</table-header>
            <table-header v-show='columnSettings.selections.qty'>@lang('core::label.qty')</table-header>
            <table-header v-show='columnSettings.selections.created_by'>@lang('core::label.created.by')</table-header>
            <table-header v-show='columnSettings.selections.created_date'>@lang('core::label.created.date')</table-header>
            <table-header v-show='columnSettings.selections.audited_by'>@lang('core::label.audit.by')</table-header>
            <table-header v-show='columnSettings.selections.audited_date'>@lang('core::label.audit.date')</table-header>
            <table-header v-show='columnSettings.selections.deleted'>@lang('core::label.deleted')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='columnSettings.selections.approval_status' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.approval_status_label }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.transaction_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.created_from' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.created_for' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.for_location' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.product' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.qty' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" v-show='columnSettings.selections.created_by' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.created_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--auditedBy" v-show='columnSettings.selections.audited_by' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.audited_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--deleted" v-show='columnSettings.selections.deleted' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.deleted }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.invoice'),
        'content' => trans('core::confirm.delete.invoice')
    ])
    @endcomponent

    @include('core::modal.invoice-filter', [
        'show' => 'filter.visible',
        'options' => 'filter.options',
        'model' => 'filter.data',
        'search' => 'paginate'
    ])

    <column-select
        v-bind:columns='columnSettings.columns'
        v-bind:id='columnSettings.id'
        v-show="columnSettings.visible"
        v-model='columnSettings.selections'
        @close="columnSettings.visible = false"
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @javascript('permissions', $permissions)

    @stack('script')

    <script src="{{ elixir('js/ProductConversion/list.js') }}"></script>
@stop