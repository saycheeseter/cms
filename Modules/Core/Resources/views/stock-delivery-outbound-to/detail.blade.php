@extends('core::stock-delivery-outbound.detail')

@section('title', trans('core::label.stock.delivery.outbound.detail'))

@push('script')
    <script src="{{ elixir('js/StockDeliveryOutboundTo/detail.js') }}"></script>
@endpush
