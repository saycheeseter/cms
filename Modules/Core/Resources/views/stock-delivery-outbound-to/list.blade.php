@extends('core::stock-delivery-outbound.list')
@section('title', trans('core::label.stock.delivery.outbound.list'))

@push('script')
    <script src="{{ elixir('js/StockDeliveryOutboundTo/list.js') }}"></script>
@endpush
