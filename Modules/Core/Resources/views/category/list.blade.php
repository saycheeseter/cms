@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.category'))

@section('container-1')
    <div class="c-content-group c-has-bb">
        <control-bar id="controls">
            <template slot="left">
                <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                    @lang('core::label.filters')
                </control-button>
            </template>
        </control-bar>
    </div>
    <div class="c-sub-nav-content">
        <div class="c-sub-sidebar">
            <ul id="tree" class="v-tree-item-root">
                <tree-item
                    :nodes="treeData"
                    :level='-1'
                    v-bind:optional='true'
                    v-bind:collapsed='true'
                ></tree-item>
            </ul>
        </div>
        <div class="c-sub-main-content c-no-bt">
            <datatable
                v-bind:tableId="datatable.id"
                v-bind:settings="datatable.settings"
                v-bind:table="datatable.value"
                v-on:reload="paginate"
                v-on:destroy="confirm"
                v-on:update="update">
                <template slot="header">
                    <table-header>@lang('core::label.code')</table-header>
                    <table-header>@lang('core::label.name')</table-header>
                    <table-header></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                        <input type="text" class="required" v-model="row.data.code" @keydown="changeState(row.index, 'update')"/>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                        <input type="text" class="required" v-model="row.data.name" @keydown.enter="update(row.data, row.index)" @keydown="changeState(row.index, 'update')"/>
                    </td>
                </template>
                @if ($permissions['create'])
                    <template slot="input">
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--code">
                            <input class="required" v-focus v-model="fields.code" type="text" />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                            <input class="required" v-model="fields.name" type="text" @keydown.enter="store()"/>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                            <img src="{{ asset('images/iconupdate.png') }}" @click.stop="store()">
                        </table-input>
                    </template>
                @endif
            </datatable>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('loading')
    @include('core::common.loading', [
        'show' => 'processing'
    ])
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'destroy',
        'header' => Lang::get('core::label.delete.category'),
        'content' => Lang::get('core::confirm.delete.category')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Category/list.js') }}"></script>
@stop
