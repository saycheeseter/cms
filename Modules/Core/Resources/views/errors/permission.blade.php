<!DOCTYPE html>
<html>
<head>
	<title>@lang('core::error.no.permission.page')</title>
	<style type="text/css">
		html, body {
			height: 100%;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}
		body {
			margin: 0;
			padding: 0;
			background-image: url('/images/spiration-light.png');
		}
		span {
			font-size: 60px;
			color: #565656;
			font-family: Impact;
			display: block;
			margin-top: 10px;
			text-transform: uppercase;
		}
		div {
			padding: 0px 160px;
		}
		img {
			width: 35%;
		}
	</style>
</head>
<body>
	<div align="center">
		<span>@lang('core::error.no.permission.page')</span>
	</div>
</body>
</html>