@extends('core::layouts.print')
@section('title', trans('core::label.inventory.adjust'))

@section('content')
    <div class="c-pda-20">
        <table class="print-page">
            <thead>
                <tr>
                    <td colspan="99" class="print-header">
                        <div class="content-group clearfix">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td>{{ Auth::branch()->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ Auth::branch()->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ Auth::branch()->contact }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <table>
                                    <tr>
                                        <td align="right">@lang('core::label.inventory.adjust')</td>
                                    </tr>
                                    <tr>
                                        <td align="right">{{ $data->sheet_number }}</td>
                                    </tr>
                                    <tr>
                                        <td align="right">@lang('core::label.prepared.by'): {{ $data->creator->full_name }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="content-group clearfix c-pdt-20 fs-12">
                            <div class="pull-left">
                                <table>
                                    <tr>
                                        <td>@lang('core::label.for.branch'):</td>
                                        <td>{{ $data->for->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('core::label.for.location'):</td>
                                        <td>{{ $data->location->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('core::label.request.by'):</td>
                                        <td>{{ $data->requested->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <td valign="top">@lang('core::label.remarks'):</td>
                                        <td>{{ $data->remarks }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="pull-right">
                                <table>
                                    <tr>
                                        <td>@lang('core::label.date'):</td>
                                        <td align="right">{{ $data->transaction_date->toDateString() }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="table-header">
                    <td>@lang('core::label.barcode')</td>
                    <td>@lang('core::label.product.name')</td>
                    <td>@lang('core::label.chinese.name')</td>
                    <td>@lang('core::label.inventory')</td>
                    <td>@lang('core::label.qty')</td>
                    <td>@lang('core::label.uom')</td>
                    <td>@lang('core::label.unit.specs')</td>
                    <td>@lang('core::label.pc.qty')</td>
                    <td>@lang('core::label.price')</td>
                    <td>@lang('core::label.amount')</td>
                    <td>@lang('core::label.remarks')</td>
                </tr>
            </thead>
            <tbody class="print-body">
                @for ($i = 0; $i < count($data->details); $i++)
                    <tr>
                        <td valign="top"> {{ $data->details[$i]->presenter()->unitBarcode }} </td>
                        <td valign="top"> {{ $data->details[$i]->product->name }} </td>
                        <td valign="top"> {{ $data->details[$i]->product->chinese_name }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->inventory }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->qty }} </td>
                        <td valign="top"> {{ $data->details[$i]->unit->name }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->unit_qty }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->pc_qty }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->price }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->amount }} </td>
                        <td valign="top"> {{ $data->details[$i]->remarks }} </td>
                    </tr>
                @endfor
            </tbody>
        </table>
        <div class="print-footer">
            <div class="content-group clearfix">
                <div class="pull-left">
                    <div class="content-group fs-9">
                        @lang('core::note.received.good.condition')<br>
                        @lang('core::note.received.above.articles.good.condition')
                    </div>
                    <div class="content-group">
                        <table>
                            <tr>
                                <td>______________________</td>
                            </tr>
                            <tr>
                                <td align="center">@lang('core::label.prepared.by'):</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    <table class="c-tbl-layout">
                        <tr>
                            <td align="right">@lang('core::label.total.lost.amount.cost'):</td>
                            <td align="right">{{ $data->number()->total_lost_amount }}</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('core::label.total.gain.amount.cost'):</td>
                            <td align="right">{{ $data->number()->total_gain_amount }}</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('core::label.total.lost.gain.amount.cost'):</td>
                            <td align="right">{{ $data->number()->total_amount }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop