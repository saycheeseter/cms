@extends('core::layouts.master')
@extends('core::layouts.flexible.three-container')
@section('title', trans('core::label.inventory.adjust.detail'))

@section('stylesheet')
    <style type="text/css">
        .container-1 {
            flex: 0 0 auto;
        }
        .container-3 {
            max-height: 110px;
        }
    </style>
@stop

@section('container-1')
    @include('core::inventory-adjust.sections.detail.main-info')
@stop

@section('container-2')
    @include('core::inventory-adjust.sections.detail.controls')

    <div class="c-content-group c-tb c-lr c-has-bt" align="center">
        <span class="c-form-note">
            @lang('core::note.changing.diff.qty')
        </span>
    </div>

    @include('core::inventory-adjust.sections.detail.details')
@stop

@section('container-3')
    @include('core::inventory-adjust.sections.detail.summary')
@stop

@section('message')
    <message type="danger" :show="message.transaction.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.transaction.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.transaction.success != ''">
        <span v-text="message.transaction.success"></span>
    </message>
    <message type="info" :show="message.transaction.info != ''">
        <span v-text="message.transaction.info"></span>
    </message>
@stop

@section('modals')
    @include('core::inventory-adjust.sections.detail.modals')
@stop

@section('script')
    @javascript('permissions', $permissions)
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('users', $users)
    @javascript('udfs', $udfs)
    @javascript('id', Request::segment(2))

    @stack('script')

    <script src="{{ elixir('js/InventoryAdjust/detail.js') }}"></script>
@stop