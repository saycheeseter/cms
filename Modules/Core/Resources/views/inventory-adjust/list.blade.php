@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.inventory.adjust.list'))

@section('container-1')
    @include('core::inventory-adjust.sections.list.controls')
    @include('core::inventory-adjust.sections.list.table')
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.invoice'),
        'content' => trans('core::confirm.delete.invoice')
    ])
    @endcomponent

    @include('core::modal.invoice-filter', [
        'show' => 'filter.visible',
        'options' => 'filter.options',
        'model' => 'filter.data',
        'search' => 'paginate'
    ])

    <column-select
        v-bind:columns='columnSettings.columns'
        v-bind:id='columnSettings.id'
        v-show="columnSettings.visible"
        v-model='columnSettings.selections'
        @close="columnSettings.visible = false"
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @javascript('udfs', $udfs)
    @javascript('permissions', $permissions)

    @stack('script')

    <script src="{{ elixir('js/InventoryAdjust/list.js') }}"></script>
@stop
