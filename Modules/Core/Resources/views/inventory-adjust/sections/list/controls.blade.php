<control-bar id='controls'>
    <template slot="left">
        @if($permissions['create'])
            <control-button v-if='enabled' name='add-new-transaction' icon="fa fa-plus" @click="create">
                @lang('core::label.add.new.transaction')
            </control-button>
        @endif

        <control-button name='filters' icon="fa fa-search" @click="filter.visible = true">
            @lang('core::label.filters')
        </control-button>

        @if($permissions['column_settings'])
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        @endif
    </template>
    <template slot="right">
        @if($permissions['export'])
            <control-dropdown v-if='enabled' name='export-excel' class="list" icon="fa fa-file-excel-o">
                @lang('core::label.export.excel')
                <template slot="list">
                    <li>
                        <a @click="excel()">
                            @lang('core::label.default.excel.template')
                        </a>
                    </li>
                    @foreach ($templates as $template)
                        <li>
                            <a @click="excel( {{ $template->id }} )" >
                                {{ $template->name }} 
                            </a> 
                        </li>
                    @endforeach
                </template>
            </control-dropdown>
        @endif
        @if($permissions['approve'])
            <control-button v-if='enabled' name='approve' icon="fa fa-check fa-green" @click="approveTransaction()">
                @lang('core::label.approve')
            </control-button>
        @endif

        @if($permissions['decline'])
            <control-button v-if='enabled' name='decline' icon="fa fa-close fa-red" @click="declineTransaction()">
                @lang('core::label.decline')
            </control-button>
        @endif
    </template>
</control-bar>