@component('core::components.confirm', [
    'id' => 'confirm-details',
    'show' => 'confirm.visible',
    'callback' => 'removeProduct',
    'header' => trans('core::label.remove.product'),
    'content' => trans('core::confirm.remove.product')
])
@endcomponent

@include('core::modal.product-transaction-selector')
@include('core::modal.serial-number-form')
@include('core::modal.serial-number-selection')

<column-select 
    :columns='columnSettings.details.columns'
    v-model='columnSettings.details.selections'
    v-on:close='columnSettings.details.visible = false'
    v-show='columnSettings.details.visible'
    :module='columnSettings.details.module'
    id='details-column-modal'>
</column-select>

<dialog-box
    :show='dialog.process.message !=""'
    v-on:close='redirect'>
    <template slot='content'>@{{ dialog.process.message }}</template>
</dialog-box>

@component('core::components.confirm', [
    'id' => 'warning',
    'show' => 'warning.visible',
    'callback' => 'removeProducts',
    'header' => trans('core::label.warning'),
    'cancel' => 'revertInfo',
    'content' => trans('core::confirm.changing.will.remove.products')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'retrieve-unsaved-data',
    'show' => 'cache.modal',
    'callback' => 'loadCache',
    'header' => trans('core::label.existing.unsaved.data'),
    'content' => trans('core::confirm.retrieve.unsaved.data')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'delete-import-data-collector-transaction',
    'show' => 'dataCollector.visible',
    'callback' => 'deleteDataCollectorTransaction',
    'header' => trans('core::label.delete.data.collector.transaction'),
    'content' => trans('core::confirm.delete.data.collector.transaction')
])
@endcomponent

<confirm 
    id="warning-inventory"
    :show="discrepancyWarning.visible"
    ok="@lang('core::label.ok')"
    cancel="@lang('core::label.cancel')"
    @ok="proceed"
    @cancel="discrepancyWarning.visible = false"
    @close="discrepancyWarning.visible = false">
    <template slot="header">
        @lang('core::label.warning')
    </template>
    <template slot="content">
        @lang('core::confirm.ff.product.inventory.are.not.equal')
        <li v-for="warning in discrepancyWarning.messages">@{{ warning }}</li>
        <div class="c-mgt-10">
            <label class="radio-chkbx" v-show="!isForApprovalOrDecline">
                <input type="radio" v-model="discrepancyWarning.option" value="2">
                @lang('core::label.refresh.inventory')
            </label>
            <label class="radio-chkbx" v-show="!isForApprovalOrDecline">
                <input type="radio" v-model="discrepancyWarning.option" value="1">
                @lang('core::label.proceed')
            </label>

            <label v-show="isForApprovalOrDecline">
                @lang('core::confirm.do.you.want.to.continue')
            </label>
        </div>
    </template>
</confirm>

<modal 
    v-show='dialog.directApproval.visible'
    @close='redirect'>
    <template slot="header">
        @{{ dialog.directApproval.message }}
    </template>
    <template slot="content">
        @lang('core::info.direct.approve')
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-blue" @click='directApproval()'>@lang('core::label.approve')</button>
        <button class="c-btn c-dk-green" ref="saveOnly" @click='redirect'>@lang('core::label.proceed')</button>
    </template>
</modal>