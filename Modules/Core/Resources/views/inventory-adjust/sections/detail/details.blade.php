<datatable
    v-bind:tableId="datatable.details.id"
    v-bind:settings="datatable.details.settings"
    v-bind:table="datatable.details.value"
    v-on:destroy="confirmation"
    v-on:update="validateDetail"
    ref='detail'>
    <template slot="header">
        <table-header v-show='columnSettings.details.selections.barcode'>@lang('core::label.barcode')</table-header>
        <table-header v-show='columnSettings.details.selections.stockNo'>@lang('core::label.stock.no')</table-header>
        <table-header>@lang('core::label.product.name')</table-header>
        <table-header>@lang('core::label.s.n.') / @lang('core::label.batch')</table-header>
        <table-header v-show='columnSettings.details.selections.chineseName'>@lang('core::label.chinese.name')</table-header>
        <table-header>@lang('core::label.inventory')</table-header>
        <table-header>@lang('core::label.qty')</table-header>
        <table-header>@lang('core::label.uom')</table-header>
        <table-header>@lang('core::label.unit.specs')</table-header>
        <table-header>@lang('core::label.pc.qty')</table-header>
        <table-header>@lang('core::label.total.qty')</table-header>
        <table-header>@lang('core::label.diff.qty')</table-header>
        <table-header>@lang('core::label.cost')</table-header>

        @if($permissions['show_price_amount'])
            <table-header>@lang('core::label.amount')</table-header>
        @endif

        <table-header v-show='columnSettings.details.selections.remarks'>@lang('core::label.remarks')</table-header>
        <table-header v-show='!disabled'>
            <div @click="finalizeAll('detail')">
                <img class="edit-image" src="{{ asset('images/iconupdate.png') }}">
            </div>
        </table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.barcode }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stockNo'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.stock_no }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--action">
            <button class="c-btn c-md-green" :disabled="isDiffQtyZero(row.data.diff_qty)" @click="showItemManagement(row.data, row.dataIndex)" v-show="row.data.manage_type === 1">
                <i class="fa fa-search"></i>
                @lang('core::label.view')
            </button>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.chinese_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.inventory }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <number-input
                onfocus="this.select()"
                :ref = "'qty-' + row.dataIndex"
                v-model="row.data.qty"
                @input="setTotalQty(row.dataIndex);changeState(row.index, 'update');"
                @blur.native="clearItemManagementData(row.data)"
                @focus.native="setPrevDiffQty(row.data)"
                @keydown.native.enter="validateDetail(row.data, row.index, row.dataIndex)"
                :disabled='disabled'>
            </number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
            <chosen
                name='uom'
                v-model='row.data.unit_id'
                :options='row.data.units'
                :row='row.dataIndex'
                @attribute='setUnitSpecs'
                @change="changeState(row.index, 'update');clearItemManagementData(row.data)"
                @click.native="setPrevDiffQty(row.data)"
                :disabled='disabled'
            ></chosen>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.unit_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <number-input
                v-model="row.data.pc_qty"
                @input="setTotalQty(row.dataIndex);changeState(row.index, 'update');"
                @blur.native="clearItemManagementData(row.data)"
                @focus.native="setPrevDiffQty(row.data)"
                :disabled='disabled'>
            </number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.total_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.diff_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.price }}
                </template>
            </table-cell-data>
        </td>

        @if($permissions['show_price_amount'])
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
        @endif

        <td class="v-dataTable__tableCell" v-show='columnSettings.details.selections.remarks'>
            <input type="text" v-model="row.data.remarks" @keydown="changeState(row.index, 'update')" :disabled='disabled'/>
        </td>
    </template>
    <template slot="input" v-if='showBarcodeStockNoInput'>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
            <input tabindex="-1" type="text" ref="barcodeInput" @keydown.enter="findByBarcodeInput"/>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stockNo'>
            <input tabindex="-1" type="text" @keydown.enter="findByStockNoInput"/>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name">
            <chosen
                :options="reference.products"
                :callback="searchProductByName"
                @fresh="clearProductDropdownAndValue"
                @selected="findByProductSelected">
            </chosen>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--uom"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount"></table-input>

        @if($permissions['show_price_amount'])
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount"></table-input>
        @endif

        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.details.selections.remarks'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action"></table-input>
    </template>
</datatable>