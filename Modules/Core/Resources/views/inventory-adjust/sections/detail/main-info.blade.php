<div class="c-content-group c-tb c-lr clearfix" id='main-info'>
    <div class="c-content-group clearfix">
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.date.time'):</td>
                    <td>
                        <date-time-picker
                            id='main-info-datetimepicker'
                            name='date-time'
                            v-model='info.transaction_date'
                            :date="datepickerFormat"
                            :disabled='disabledDate'
                            :left='true'>
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td width="76px">@lang('core::label.sheet.no'):</td>
                    <td><span name="sheet-no"><b>@{{ info.sheet_number }}</b></span></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="c-content-group clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.created.for'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='created-for'
                            v-model='info.created_for'
                            :options='reference.branches'
                            @change='notify'
                            :disabled='true'
                        ></chosen>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.for.location'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='for-location'
                            v-model='info.for_location'
                            :options='reference.locations'
                            @change='notify'
                            :disabled='disabled'
                        ></chosen>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.request.by'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='requested-by'
                            v-model='info.requested_by'
                            :options='reference.users'
                            :disabled='!updatable'
                        ></chosen>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td valign="top">@lang('core::label.remarks'):</td>
                    <td><textarea name='remarks' v-model='info.remarks' rows="3" :disabled='!updatable'></textarea></td>
                </tr>
            </table>
        </div>
        <div class="udf-group" v-if="udfs.data.length > 0">
            <table class="c-tbl-layout">
                <tr v-for="udf in udfs.data" v-if="udf.visible">
                    <td>@{{ udf.label }}:</td>
                    <td>
                        <template v-if="udf.type == udfs.type.STR">
                            <input type="text" v-model="info[udf.alias]" :disabled='!updatable'>
                        </template>
                        <template v-if="udf.type == udfs.type.INTR">
                            <number-input v-model="info[udf.alias]" :disabled='!updatable' :precision='false'></number-input>
                        </template>
                        <template v-if="udf.type == udfs.type.DECIMAL">
                            <number-input v-model="info[udf.alias]" :disabled='!updatable'></number-input>
                        </template>
                        <template v-if="udf.type == udfs.type.DATE">
                            <datepicker :id="udf.alias" format="yyyy-MM-dd" v-model='info[udf.alias]' :disabled-picker='!updatable'></datepicker>
                        </template>
                        <template v-if="udf.type == udfs.type.DATETIME">
                            <date-time-picker
                                :id="udf.alias"
                                v-model='info[udf.alias]'
                                :date="{ format: 'yyyy-MM-dd' }"
                                :disabled='!updatable'
                                :left='true'>
                            </date-time-picker>
                        </template>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div id='status-label' class="label status-label" :class='approvalClass'>@{{ info.approval_status_label }}</div>