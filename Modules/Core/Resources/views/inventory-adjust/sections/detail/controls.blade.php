<control-bar id='controls'>
    <template slot="left">
        <control-button name='add-product' icon="fa fa-plus" v-if="!disabled" @click="productPicker.visible = true">
            @lang('core::label.product')
        </control-button>
        @if($permissions['column_settings'])
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.details.visible = true">
                {{-- @lang('core::label.column.settings') --}}
            </control-button>
        @endif
        <div class="control-border control-form" v-if="!disabled">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.qty'):</td>
                    <td><number-input class="field__qty" v-model='scan.quantity'></number-input></td>
                </tr>
            </table>
        </div>
        <div class="control-border control-form" v-if="!disabled">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.scan'):</td>
                    <td><input type="text" @keydown.enter="findByBarcode" placeholder="@lang('core::label.barcode')"></td>
                </tr>
            </table>
        </div>
        @include('core::attachment.transaction')
        <control-dropdown icon="fa fa-database">
            @lang('core::label.data.collector')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td class="ws-nowrap">@lang('core::label.reference.no'):</td>
                            <td><input type="text" placeholder="@lang('core::label.reference.no')" v-model='dataCollector.referenceNumber' @keypress.enter='importDataCollectorTransaction()'></td>
                            <td><input type="button" class="c-btn c-dk-blue" value="@lang('core::label.search')" @click='importDataCollectorTransaction()'></td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
    </template>
    <template slot="right">
        @if($templates->count() > 0 && $permissions['print'])
            <control-dropdown name='print' icon="fa fa-print" v-if="!newTransaction">
                {{-- @lang('core::label.print') --}}
                <template slot="list">
                    @foreach($templates as $printout)
                        <li><a href="javascript:void();" @click="print({{ $printout->id }})">{{ $printout->name }}</a></li>
                    @endforeach
                </template>
            </control-dropdown>
        @endif

        <control-button name='save-add-new' v-if="controls.draft" icon="fa fa-save" @click='process(true)'>
            @lang('core::label.save.and.add.new')
        </control-button>

        <control-button name='save' v-if="controls.draft" icon="fa fa-save" @click="process">
            {{-- @lang('core::label.save') --}}
        </control-button>

        <control-button name='save' v-if="approvedTransaction" icon="fa fa-save" @click="updateApprovedTransaction()">
            {{-- @lang('core::label.save') --}}
        </control-button>

        @if($permissions['update'])
            <control-button name='for-approval' v-if="controls.forApproval" icon="fa fa-thumbs-o-up" @click="forApprovalTransaction()">
                @lang('core::label.for.approval')
            </control-button>
        @endif

        @if($permissions['approve'])
            <control-button name='approve' v-if="controls.approveDecline" icon="fa fa-check fa-green" @click="approveTransaction()">
                @lang('core::label.approve')
            </control-button>
        @endif

        @if($permissions['decline'])
            <control-button name='decline' v-if="controls.approveDecline" icon="fa fa-close fa-red" @click="declineTransaction()">
                @lang('core::label.decline')
            </control-button>
        @endif

        @if($permissions['decline'] || $permissions['approve'])
            <control-button name='revert' v-if="controls.revert" icon="fa fa-arrow-circle-left" @click="revertTransaction()">
                @lang('core::label.revert')
            </control-button>
        @endif
    </template>
</control-bar>
