<div class="c-content-group c-tb c-lr">
    <table class="c-tbl-layout">
        <tr>
            <td>@lang('core::label.total.qty'):</td>
            <td align="right"><b>@{{ totalQty }}</b></td>
        </tr>
        <tr>
            <td>@lang('core::label.total.gain.qty'):</td>
            <td align="right"><b>@{{ totalGainQty }}</b></td>
            <td class="c-pdl-30">@lang('core::label.total.gain.amount.cost'):</td>
            <td align="right"><b>@{{ totalGainCost }}</b></td>
        </tr>
        <tr>
            <td>@lang('core::label.total.lost.qty'):</td>
            <td align="right"><b>@{{ totalLostQty }}</b></td>
            <td class="c-pdl-30">@lang('core::label.total.lost.amount.cost'):</td>
            <td align="right"><b>@{{ totalLostCost }}</b></td>
        </tr>
        <tr>
            <td>@lang('core::label.total.gain.lost.qty'):</td>
            <td align="right"><b>@{{ totalGainLostQty }}</b></td>
            <td class="c-pdl-30">@lang('core::label.total.gain.lost.amount.cost'):</td>
            <td align="right"><b>@{{ totalGainLostAmount }}</b></td>
        </tr>
    </table>
</div>
