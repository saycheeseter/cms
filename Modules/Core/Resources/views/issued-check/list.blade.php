@extends('core::check-management.list')
@section('title', trans('core::label.issued.check.list'))

@push('script')
    <script src="{{ elixir('js/IssuedCheck/list.js') }}"></script>
@endpush