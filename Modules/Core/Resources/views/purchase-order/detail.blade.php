@extends('core::invoice.detail')
@section('title', trans('core::label.purchase.order.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.deliver.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to'
                        v-model='info.deliver_to'
                        :options='reference.branchDetails'
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.deliver.until'):</td>
                <td>
                    <datepicker id="deliver-until" class="required" format="yyyy-MM-dd" v-model='info.deliver_until' :disabled-picker='disabled'></datepicker>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.supplier'):</td>
                <td>
                    <chosen
                        class="required"
                        name='supplier'
                        v-model='info.supplier_id' 
                        :options='reference.tsuppliers'
                        @change='notify'
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.payment.method'):</td>
                <td>
                    <chosen
                        class="required"
                        name='payment-method'
                        v-model='info.payment_method'
                        :options='reference.paymentMethods'
                        :disabled='!updatable'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.term'):</td>
                <td><number-input name='term' v-model='info.term' :disabled='!updatable' :precision='false'></number-input></td>
            </tr>
        </table>
    </div>
@stop

@section('summary')
    <div class="pull-left c-mgl-30" v-if="info.transactionable_type !== null">
        <table class="c-tbl-layout">
            <tr>
                <td>
                    @lang('core::info.this.transaction.was.based.from', [
                        'sheet_number' => '<a href="javascript:void();" @click="viewReference(info.transactionable_id)">@{{ info.transactionable_sheet_number }}</a>'
                    ])
                </td>
            </tr>
        </table>
    </div>
@stop

@push('modals')
    <dialog-box
        :show='autoGroupProduct.dialog.message !=""'
        @close='breakToComponents'>
        <template slot='content'>@{{ autoGroupProduct.dialog.message }}</template>
    </dialog-box>
@endpush

@push('script')
    @javascript('tsuppliers', $tsuppliers)
    @javascript('paymentMethods', $paymentMethods)
    @javascript('branchDetails', $branchDetails)

    <script src="{{ elixir('js/PurchaseOrder/detail.js') }}"></script>
@endpush