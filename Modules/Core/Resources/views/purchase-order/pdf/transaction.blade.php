@extends('core::invoice.pdf.transaction')
@section('title', trans('core::label.purchase.order'))

@section('stylesheet')
    <style type="text/css">
    </style>
@stop

@section('main-info')
    <div class="content-group clearfix">
        <div class="pull-left">
            <table>
                <tr>
                    <td>{{ Auth::branch()->name }}</td>
                </tr>
                <tr>
                    <td>{{ Auth::branch()->address }}</td>
                </tr>
                <tr>
                    <td>{{ Auth::branch()->contact }}</td>
                </tr>
            </table>
        </div>
        <div class="pull-right">
            <table>
                <tr>
                    <td align="right">@lang('core::label.purchase.order')</td>
                </tr>
                <tr>
                    <td align="right">{{ $data->sheet_number }}</td>
                </tr>
                <tr>
                    <td align="right">@lang('core::label.prepared.by'): {{ $data->creator->full_name }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="content-group clearfix c-pdt-20 fs-12">
        <div class="pull-left">
            <table>
                <tr>
                    <td>@lang('core::label.for.branch'):</td>
                    <td>{{ $data->for->name }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.for.location'):</td>
                    <td>{{ $data->location->name }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.request.by'):</td>
                    <td>{{ $data->requested->full_name }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.deliver.to'):</td>
                    <td>{{ $data->deliverTo->name }}</td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.remarks'):</td>
                    <td>{{ $data->remarks }}</td>
                </tr>
            </table>
        </div>
        <div class="pull-right">
            <table>
                <tr>
                    <td>@lang('core::label.date'):</td>
                    <td align="right">{{ $data->transaction_date->toDateString() }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.term'):</td>
                    <td align="right">{{ $data->term }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.deliver.until'):</td>
                    <td align="right">{{ $data->deliver_until }}</td>
                </tr>
            </table>
        </div>
    </div>
@stop