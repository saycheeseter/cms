@extends('core::layouts.print')

@section('content')
    <div class="c-pda-20">
        <table class="print-page">
            <thead>
                <tr>
                    <td colspan="99" class="print-header">
                        @yield('main-info')
                    </td>
                </tr>
                <tr class="table-header">
                    <td>@lang('core::label.barcode')</td>
                    <td>@lang('core::label.product.name')</td>
                    <td>@lang('core::label.chinese.name')</td>
                    <td>@lang('core::label.qty')</td>
                    <td>@lang('core::label.uom')</td>
                    <td>@lang('core::label.unit.specs')</td>
                    <td>@lang('core::label.o.price')</td>
                    <td>@lang('core::label.price')</td>
                    <td>@lang('core::label.discount.1')</td>
                    <td>@lang('core::label.discount.2')</td>
                    <td>@lang('core::label.discount.3')</td>
                    <td>@lang('core::label.discount.4')</td>
                    <td>@lang('core::label.amount')</td>
                    <td>@lang('core::label.remarks')</td>
                </tr>
            </thead>
            <tbody class="print-body">
                @for ($i = 0; $i < count($data->details); $i++)
                    <tr>
                        <td valign="top"> {{ $data->details[$i]->presenter()->unitBarcode }} </td>
                        <td valign="top"> {{ $data->details[$i]->product->name }} </td>
                        <td valign="top"> {{ $data->details[$i]->product->chinese_name }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->qty }} </td>
                        <td valign="top"> {{ $data->details[$i]->unit->name }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->unit_qty }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->oprice }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->price }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->discount1 }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->discount2 }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->discount3 }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->discount4 }} </td>
                        <td valign="top"> {{ $data->details[$i]->number()->amount }} </td>
                        <td valign="top"> {{ $data->details[$i]->remarks }} </td>
                    </tr>
                @endfor
            </tbody>
        </table>
        <div class="print-footer">
            <div class="content-group clearfix">
                <div class="pull-left">
                    <div class="content-group fs-9">
                        @lang('core::note.received.good.condition')<br>
                        @lang('core::note.received.above.articles.good.condition')
                    </div>
                    <div class="content-group">
                        <table>
                            <tr>
                                <td>______________________</td>
                            </tr>
                            <tr>
                                <td align="center">@lang('core::label.prepared.by'):</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    <table class="c-tbl-layout">
                        <tr>
                            <td align="right">@lang('core::label.net.total'):</td>
                            <td align="right">{{ $data->number()->total_amount }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop