@component('core::components.confirm', [
    'id' => 'confirm-details',
    'show' => 'confirm.visible',
    'callback' => 'removeProduct',
    'header' => trans('core::label.remove.product'),
    'content' => trans('core::confirm.remove.product')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'warning',
    'show' => 'warning.visible',
    'callback' => 'removeProducts',
    'cancel' => 'revertInfo',
    'header' => trans('core::label.warning'),
    'content' => trans('core::confirm.changing.will.remove.products')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'import-warning-zero-qty',
    'show' => 'importWarning.zeroQty.visible',
    'callback' => 'removeEmptyInvoice',
    'header' => trans('core::label.warning'),
    'content' => trans('core::confirm.zero.quantity.invoice')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'import-warning-same-invoice',
    'show' => 'importWarning.sameInvoice.visible',
    'callback' => 'replaceAndRetrieveInvoice',
    'header' => trans('core::label.warning'),
    'content' => trans('core::confirm.reimporting.invoice.will.replace.previous.product')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'delete-import-data-collector-transaction',
    'show' => 'dataCollector.visible',
    'callback' => 'deleteDataCollectorTransaction',
    'header' => trans('core::label.delete.data.collector.transaction'),
    'content' => trans('core::confirm.delete.data.collector.transaction')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'retrieve-unsaved-data',
    'show' => 'cache.modal',
    'callback' => 'loadCache',
    'header' => trans('core::label.existing.unsaved.data'),
    'content' => trans('core::confirm.retrieve.unsaved.data')
])
@endcomponent

@include('core::modal.product-transaction-selector')
@include('core::modal.serial-number-selection')
@include('core::modal.batch-number-selection')
@include('core::modal.alternative-item-dialog')

<column-select
    :columns='columnSettings.details.columns'
    v-model='columnSettings.details.selections'
    @close='columnSettings.details.visible = false'
    v-show='columnSettings.details.visible'
    :module='columnSettings.details.module'
    id='details-column-modal'>
</column-select>

<dialog-box
    :show='dialog.process.message !=""'
    @close='redirect'>
    <template slot='content'>@{{ dialog.process.message }}</template>
</dialog-box>

<modal 
    v-show='dialog.directApproval.visible'
    @close='redirect'>
    <template slot="header">
        @{{ dialog.directApproval.message }}
    </template>
    <template slot="content">
        @lang('core::info.direct.approve')
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-blue" @click='directApproval()'>@lang('core::label.approve')</button>
        <button class="c-btn c-dk-green" ref="saveOnly" @click='redirect()'>@lang('core::label.proceed')</button>
    </template>
</modal>

<modal
    v-show='dialog.removeItemManagementData.visible'
    @close='cancelItemManagementForLocationChanges()'>
    <template slot="header">
        @lang('core::label.remove.serials.and.batches')
    </template>
    <template slot="content">
        @lang('core::info.changing.for.location.will.remove.serials.batches')
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-blue" @click='cancelItemManagementForLocationChanges()'>@lang('core::label.cancel')</button>
        <button class="c-btn c-dk-green" ref="proceedClearItemManagementData" @click='clearItemManagementData()'>@lang('core::label.proceed')</button>
    </template>
</modal>

@include('core::inventory-chain.sections.invoice-selector')

<filter-modal
    v-show="selector.invoice.filter.visible"
    @close="selector.invoice.filter.visible = false"
    :filters="selector.invoice.filter.options"
    v-model='selector.invoice.filter.data'
    v-on:search="getInvoicesForSelector">
</filter-modal>