<div id='main-info' class="c-content-group c-tb c-lr clearfix">
    <div class="pull-right">
        <div class="c-content-group clearfix">
            <div class="pull-right">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.date.time'):</td>
                        <td>
                            <date-time-picker
                                id='main-info-datetimepicker'
                                name='date-time'
                                v-model='info.transaction_date'
                                :date="datepickerFormat"
                                :disabled='disabledDate'
                                :left='true'>
                            </date-time-picker>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="c-content-group clearfix">
            <div id='transaction-type-container' class="pull-right" v-if="controls.create">
                <table class="c-tbl-layout">
                    <tr>
                        <td>
                            <label class="radio-chkbx">
                                <input :disabled='disabled' type="radio" name='transaction-type' @click='notify("transaction-type")' :value='directTransaction' v-model='info.transaction_type'>
                                @lang('core::label.direct')
                            </label>
                        </td>

                        <td>
                            <label class="radio-chkbx c-mgl-5">
                                <input :disabled='disabled' type="radio" name='transaction-type' @click='notify("transaction-type")' :value='fromInvoiceTransaction' v-model='info.transaction_type'>
                                @yield('transaction-type-label', trans('core::label.from.invoice'))
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="c-content-group" v-show='fromInvoice'>
            <table class="c-tbl-layout">
                <tr>
                    <td>
                        @yield('transaction-label', trans('core::label.invoice')):
                    </td>
                    <td>
                        <chosen
                            name='reference-id'
                            v-model='info.reference_id'
                            :options='reference.invoice'
                            :disabled='disabled'
                            :callback='getInvoice'
                            @update:options='val => reference.invoice = val'
                            @change='notify("reference")'
                            @click='buffer.reference.invoice = reference.invoice.slice(0)'
                        ></chosen>
                    </td>
                    <td>
                    </td>
                    <td>
                        <button
                            class="c-btn c-lt-green"
                            @click="showInvoiceSelector"
                            v-show='!disabled'>
                            <i class="fa fa-list-alt"></i>
                        </button>
                        <input :disabled='disabled' name='import-invoice' type="button" class="c-btn c-lt-green" value="@lang('core::label.import')" @click='importInvoice'>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="pull-left">
        <div class="c-content-group">
            <table class="c-tbl-layout">
                <tr>
                    <td width="76px">@lang('core::label.sheet.no'):</td>
                    <td><span name="sheet-no"><b>@{{ info.sheet_number }}</b></span></td>
                </tr>
            </table>
        </div>
        <div class="c-content-group clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.created.for'):</td>
                        <td>
                            <chosen
                                class="required"
                                name='created-for'
                                v-model='info.created_for'
                                :options='reference.branches'
                                :disabled='true'
                                @change='notify("created-for")'
                            ></chosen>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.for.location'):</td>
                        <td>
                            <chosen
                                class="required"
                                name='for-location'
                                v-model='info.for_location'
                                :options='reference.locations'
                                :disabled='disabled'
                                @change='removeItemManagementDataWarning'
                            ></chosen>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.request.by'):</td>
                        <td>
                            <chosen
                                class="required"
                                name='request-by'
                                v-model='info.requested_by'
                                :options='reference.users'
                                :disabled='!updatable'
                            ></chosen>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">@lang('core::label.remarks'):</td>
                        <td><textarea name='remarks' v-model='info.remarks' rows="3" :disabled='!updatable'></textarea></td>
                    </tr>
                </table>
            </div>
            @yield('main-info')
            <div class="udf-group" v-if="udfs.data.length > 0">
                <table class="c-tbl-layout">
                    <tr v-for="udf in udfs.data" v-if="udf.visible">
                        <td>@{{ udf.label }}:</td>
                        <td>
                            <template v-if="udf.type == udfs.type.STR">
                                <input type="text" v-model="info[udf.alias]" :disabled='!updatable'>
                            </template>
                            <template v-if="udf.type == udfs.type.INTR">
                                <number-input v-model="info[udf.alias]" :disabled='!updatable' :precision='false'></number-input>
                            </template>
                            <template v-if="udf.type == udfs.type.DECIMAL">
                                <number-input v-model="info[udf.alias]" :disabled='!updatable'></number-input>
                            </template>
                            <template v-if="udf.type == udfs.type.DATE">
                                <datepicker :id="udf.alias" format="yyyy-MM-dd" v-model='info[udf.alias]' :disabled-picker='!updatable'></datepicker>
                            </template>
                            <template v-if="udf.type == udfs.type.DATETIME">
                                <date-time-picker
                                    :id="udf.alias"
                                    v-model='info[udf.alias]'
                                    :date="{ format: 'yyyy-MM-dd' }"
                                    :disabled='!updatable'
                                    :left='true'>
                                </date-time-picker>
                            </template>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div id='status-label' class="label status-label" :class='approvalClass'>@{{ info.approval_status_label }}</div>