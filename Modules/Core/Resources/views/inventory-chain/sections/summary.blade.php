<div class="c-content-group c-tb c-lr clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.invoiced.qty'):</td>
                <td><b><span name='invoiced-qty'>@{{ invoicedQty }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.qty'):</td>
                <td><b><span name='total-qty'>@{{ totalQty }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.remaining.qty'):</td>
                <td><b><span name='remaining-qty'>@{{ remainingQty }}</span></b></td>
            </tr>
            @if($permissions['show_price_amount'])
                <tr>
                    <td>@lang('core::label.total.amount'):</td>
                    <td><b><span name='total-amount'>@{{ totalAmount }}</span></b></td>
                </tr>
            @endif
        </table>
    </div>
    <div class="pull-left c-mgl-30">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.last.modified'):</td>
                <td><b><span name='modified-date'>@{{ info.modified_date }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.date.created'):</td>
                <td><b><span name='date-created'>@{{ info.created_date }}</span></b></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-30">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.created.by'):</td>
                <td><b><span name='created-by'>@{{ info.created_by }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.modified.by'):</td>
                <td><b><span name='modified-by'>@{{ info.modified_by }}</span></b></td>
            </tr>
        </table>
    </div>
</div>