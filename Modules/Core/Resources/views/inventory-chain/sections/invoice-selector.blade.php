@component('core::components.selector', [
    'id' => 'invoice-modal-selector',
    'show' => 'selector.invoice.visible',
    'datatable' => [
        'id' => 'invoice-selector-datatable',
        'settings' => 'selector.invoice.datatable.settings',
        'value' => 'selector.invoice.datatable.value',
        'model' => 'selector.invoice.datatable.selected',
        'reload' => 'getInvoicesForSelector',
        'ref' => 'invoice-selector-datatable'
    ],
    'columnSettings' => [
        'id' => 'invoice-selector-column-settings',
        'visible' => 'selector.invoice.columnSettings.visible',
        'model' => 'selector.invoice.columnSettings.selections',
        'columns' => 'selector.invoice.columnSettings.columns',
        'module' => 'selector.invoice.columnSettings.module',
    ]
])
    @slot('controls')
        <control-button name='search' icon="fa fa-search" @click="selector.invoice.filter.visible = true">
            @lang('core::label.search')
        </control-button>
        <control-button
            name='column-settings'
            icon="fa fa-gear"
            @click="selector.invoice.columnSettings.visible = true">
            @lang('core::label.column.settings')
        </control-button>
    @endslot
    @slot('headers')
        <table-header v-show='selector.invoice.columnSettings.selections.approval_status'>@lang('core::label.approval.status')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
        <table-header>@lang('core::label.sheet.no')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.created_from'>@lang('core::label.created.from')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.for_location'>@lang('core::label.for.location')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.amount'>@lang('core::label.amount')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.transaction_status'>@lang('core::label.transaction.status')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.requested_by'>@lang('core::label.request.by')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.created_by'>@lang('core::label.created.by')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.created_date'>@lang('core::label.created.date')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.audited_by'>@lang('core::label.audit.by')</table-header>
        <table-header v-show='selector.invoice.columnSettings.selections.audited_date'>@lang('core::label.audit.date')</table-header>
    @endslot
    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='selector.invoice.columnSettings.selections.approval_status' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.approval_status_label }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.invoice.columnSettings.selections.transaction_date' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='selector.invoice.columnSettings.selections.created_from' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_from }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='selector.invoice.columnSettings.selections.created_for' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_for }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='selector.invoice.columnSettings.selections.for_location' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.for_location }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='selector.invoice.columnSettings.selections.amount' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.amount }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='selector.invoice.columnSettings.selections.transaction_status' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_status }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='selector.invoice.columnSettings.selections.remarks' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remarks }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--requestedBy" v-show='selector.invoice.columnSettings.selections.requested_by' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.requested_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" v-show='selector.invoice.columnSettings.selections.created_by' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.invoice.columnSettings.selections.created_date' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--auditedBy" v-show='selector.invoice.columnSettings.selections.audited_by' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.audited_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.invoice.columnSettings.selections.audited_date' @click="importInvoiceFromSelector(row.data.id, row.data.sheet_number)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.audited_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--deleted" v-show='selector.invoice.columnSettings.selections.deleted' @click="show(row.data.id)">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.deleted }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent