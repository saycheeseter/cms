@extends('core::stock-request.detail')

@section('title', trans('core::label.stock.request.from.detail'))

@section('stylesheet')
    <style type="text/css">
        #po-list-modal .modal-footer {
            display: none;
        }
        #po-list-modal .modal-body {
            max-height: 550px;
            overflow: auto;
        }
        #po-list-modal ul{
            padding-left: 15px;
        }
        .container-1 {
            flex: 0 auto;
            overflow: initial;
        }
        .container-3 {
            max-height: 60px;
        }
    </style>
@stop

@push('controls-right')
    <control-button name='print' v-if="approvedTransaction && hasRemainingItems" icon="fa fa-shopping-basket" @click="showOrderForm">
        @lang('core::label.add.new.purchase.order')
    </control-button>
@endpush

@push('modals')
    @include('core::stock-request-from.purchase-form')
    @include('core::stock-request-from.purchase-list')
@endpush

@push('table-header')
    <table-header>@lang('core::label.reference')</table-header>
@endpush

@push('table-display')
    <td class="text-center">
        <div>
            <button class="c-btn c-dk-green" v-if="row.data.references.length > 0" @click="viewPurchases(row.dataIndex)">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </td>
@endpush

@push('script')
    <script src="{{ elixir('js/StockRequestFrom/detail.js') }}"></script>
@endpush
