@extends('core::stock-request.list')
@section('title', trans('core::label.stock.request.from.list'))

@push('script')
    <script src="{{ elixir('js/StockRequestFrom/list.js') }}"></script>
@endpush
