<modal id="purchase-form" v-show="orderForm.visible" @close="orderForm.visible = false">
    <template slot="header">@lang('core::label.add.new.purchase.order')</template>
    <template slot="content">
        <message type="danger" :show="orderForm.message.error.any()" @close="orderForm.message.reset()" class="c-mgb-5">
            <ul>
                <li v-for="(value, key) in orderForm.message.error.get()" v-text="value"></li>
            </ul>
        </message>
        <div class="c-content-group clearfix flex flex-1">
            <div class="flex flex-column flex-1 c-has-bl c-has-br datatable overflow-hidden">
                <control-bar id='product-selector-controls'>
                    <template slot="left">
                        <control-button name='create-purchase' icon="fa fa-shopping-basket" @click="createOrder">
                            @lang('core::label.add.new.purchase.order')
                        </control-button>
                    </template>
                </control-bar>
                <datatable
                    v-bind:tableId="orderForm.table.id"
                    v-bind:settings="orderForm.table.settings"
                    v-bind:table="orderForm.table.value"
                    v-on:destroy="removeOrder"
                    ref="purchase-form">
                    <template slot="header">
                        <table-header v-show='columnSettings.details.selections.barcode'>@lang('core::label.barcode')</table-header>
                        <table-header>@lang('core::label.product.name')</table-header>
                        <table-header v-show='columnSettings.details.selections.chineseName'>@lang('core::label.chinese.name')</table-header>
                        <table-header>@lang('core::label.qty')</table-header>
                        <table-header>@lang('core::label.uom')</table-header>
                        <table-header>@lang('core::label.unit.specs')</table-header>
                        <table-header>@lang('core::label.total.qty')</table-header>
                        <table-header></table-header>
                    </template>
                    <template slot="display" scope="row">
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.barcode }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.name }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.chinese_name }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                            <number-input 
                                v-model="row.data.qty" 
                                @input="setOrderTotalQty(row.dataIndex)">
                            </number-input>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
                            <chosen
                                name='uom'
                                v-model='row.data.unit_id'
                                :options='row.data.units'
                                :row='row.dataIndex'
                                @attribute='setOrderUnitSpecs'
                            ></chosen>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.unit_qty }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.total_qty }}
                                </template>
                            </table-cell-data>
                        </td>
                    </template>
                </datatable>
            </div>
        </div>
    </template>
</modal>