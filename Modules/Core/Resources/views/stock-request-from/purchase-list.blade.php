<modal id="po-list-modal" v-show="transactionable.visible" @close="transactionable.visible = false">
    <template slot="header">@lang('core::label.purchase.order.list')</template>
    <template slot="content">
        <ul>
            <li v-for="reference in transactionable.details">
                <a href="javascript:void();" @click="viewPurchase(reference.id)">@{{ reference.sheet_number }}</a>
            </li>
        </ul>
    </template>
</modal>