@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.changelogs'))

@section('stylesheet')
@stop

@section('container-1')
    <div class="changelogs">
        <div class="changelogs__version">@lang('core::label.version') 2.0.9.0 (October 12, 2018)</div>
        <div class="changelogs__subHeading">Product Inventory Value Report</div>
            <ul>
                <li>
                    Added Cost, Purchase Price, and additional footer details that shows Inventory Value based from Cost column
                </li>
            </ul>
        <div class="changelogs__subHeading">Detail Sales Report</div>
            <ul>
                <li>
                    Added a new report that shows the detail summary of Sales Outbound module
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.8.0 (October 10, 2018)</div>
        <div class="changelogs__subHeading">Bank Transaction</div>
            <ul>
                <li>
                    Users may now record bank transactions and mark it by the following: Deposit, Withdrawal, or Transfer
                </li>
                <li>
                    Created bank transaction records can be traced as well by using the Bank Transaction Report
                </li>
            </ul>
        <div class="changelogs__subHeading">Automatic Group Transaction History</div>
            <ul>
                <li>
                    Transacted Automatic Group in Sales Outbound, Sales Return Inbound, and/or POS Sales now records transaction history
                </li>
                <li>
                    Users may check the transaction history of Automatic Group by viewing its own Product Summary Report
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.7.0 (October 8, 2018)</div>
        <div class="changelogs__subHeading">Product Status Integration</div>
            <ul>
                <li>
                    Integrated the Product Status marker whether it is Inactive, Active, or Discontinued
                </li>
                <li>
                    Active products can be transacted in any parts of the system
                </li>
                <li>
                    Discontinued products are not allowed to be transacted in Purchase Order, Purchase Inbound, and Stock Request
                </li>
            </ul>
        <div class="changelogs__subHeading">Active Product on Branch</div>
            <ul>
                <li>
                    In Product Details of Main Branch, under Branch tab, Users are now capable to choose which branches where the product be shown
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.6.0 (October 2, 2018)</div>
        <div class="changelogs__subHeading">Data Collector Integration</div>
        <ul>
            <li>
                Applied Data Collector function in the Inventory Chain. User may now transact collected data from the data collector device by importing specific reference number in every transaction
            </li>
        </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.5.0 (October 1, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Payment and Collection Due Warning Report display all</div>
            <ul>
                <li>
                    Added an option to display all of the rows including those entries that are not yet due
                </li>
            </ul>
        <div class="changelogs__subHeading">Time Schedule’s select all days by default</div>
            <ul>
                <li>
                    Upon creation of a User Account, Active Day options are all selected by default
                </li>
            </ul>
        <div class="changelogs__subHeading">Additional informative columns in the table list</div>
            <ul>
                <li>
                    Applied on the following modules: Customer List, Supplier List, User List, and Member List
                </li>
                <li>
                    Added additional columns for efficient viewing purposes
                </li>
                <li>
                    New additional columns can be enabled/disabled in the Column Settings as well
                </li>
            </ul>
        <div class="changelogs__subHeading">Transaction Filter Cleanup</div>
            <ul>
                <li>
                    Added additional filter criteria on the Transactions’ List for more efficient and more effective way of searching specific rows
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.4.0 (September 26, 2018)</div>
        <div class="changelogs__subHeading">POS Group Item with Components Integration</div>
            <ul>
                <li>
                    Integrated to calculate the total inventory movements when user transacts Group Item with its Components in POS
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.3.0 (September 24, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Credit Limit</div>
            <ul>
                <li>
                    Integrated Credit Limit function for Customers
                </li>
                <li>
                    Users may now check the status of Customers’ credit while making a Collection
                </li>
            </ul>
        <div class="changelogs__subHeading">Permission for all Branches</div>
            <ul>
                <li>
                    Added in the Permissions List
                </li>
                <li>
                    Users are now capable of selecting permissions in all branches by enabling the mode in the Permissions List
                </li>
            </ul>
        <div class="changelogs__subHeading">Move all Serial Numbers</div>
            <ul>
                <li>
                    Updated the Serial Number modal. Users may now select all of the available serials by moving it from left panel to right panel
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.2.0 (September 12, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Product Transaction Summary in Manual Mode</div>
            <ul>
                <li>
                    Product Transaction Summary is now included in approval of transactions and will immediately appear in product transaction summary report
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.1.0 (September 10, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Auto-update cost Settings for Purchase Inbound</div>
            <ul>
                <li>
                    Under Generic Settings, users may now choose criteria to update cost. It can be based on its own branch, all branches, and/or copy to a specific branch only
                </li>
            </ul>
        <div class="changelogs__subHeading">Settings to display transactions of other branches in Payment and Collection</div>
            <ul>
                <li>
                    Under Branch Settings, users can now choose whether they want to show the transactions of other branches in Payment or in Collection page or not
                </li>
            </ul>
        <div class="changelogs__subHeading">Multiple Login</div>
            <ul>
                <li>
                    Implemented a checker if account is logged or not. Users will be logged out if another session has been made to other device
                </li>
            </ul>
        <div class="changelogs__subHeading">Product List Express Search</div>
            <ul>
                <li>
                    Integrated an express search in the Product List where user can now search basic strings
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Detail modifications</div>
            <ul>
                <li>
                    If Product is already transacted, under Product Grouping tab, it will display a message that the product is already used
                </li>
                <li>
                    Under Branch tab: newly created Product will be activated in all available branches by default
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 2.0.0.0 (September 3, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Manual Posting</div>
            <ul>
                <li>
                    Integrated Manual Posting under Generic Settings. Through Manual mode, Users may now be able to Revert transactions which are already Approved. All transactions made under this setting should be posted manually under Transaction Summary Posting page
                </li>
            </ul>
        <div class="changelogs__subHeading">Payment and Collection allow returns</div>
            <ul>
                <li>
                    In Payment Module, users may know select Approved transactions under Purchase Return Outbound and transact it as a return
                </li>
                <li>
                    In Collection Module, users may know select Approved transactions under Sales Return Inbound and transact it as a return
                </li>
            </ul>
        <div class="changelogs__subHeading">User Permission Time Frame</div>
            <ul>
                <li>
                    Integrated time frame under User Permissions to set specific time range if when a user is allowed to access and use the system
                </li>
            </ul>
        <div class="changelogs__subHeading">Import Sales Outbound in Stock Request</div>
            <ul>
                <li>
                    Integrated under Stock Request where user can choose Sales Outbound and import the details of it
                </li>
            </ul>
        <div class="changelogs__subHeading">Import Sales Outbound in Stock Delivery</div>
            <ul>
                <li>
                    Integrated under Stock Delivery where user can choose Sales Outbound and import the details of it
                </li>
            </ul>
        <div class="changelogs__subHeading">Custom My Menu Update</div>
            <ul>
                <li>
                    Updated the list of Custom My Menu and added the reports section
                </li>
            </ul>
        <div class="changelogs__subHeading">Apply Current Purchase Price</div>
            <ul>
                <li>
                    Integrated in Product details, under Price tab. Users may now update the cost of all transactions based on the current Purchase Price of an item by selecting a date range
                </li>
            </ul>
        <div class="changelogs__subHeading">Check all per module in User Permissions</div>
            <ul>
                <li>
                    Integrated “check all” function per module of User Permissions
                </li>
            </ul>
        <div class="changelogs__subHeading">“Save and Add New” button for all transactions</div>
            <ul>
                <li>
                    Integrated in Invoicing and Inventory Chain pages. Users may now create transactions consecutively by clicking the “Save and Add New” button
                </li>
            </ul>
        <div class="changelogs__subHeading">Inbound and Outbound new Modal to choose existing transactions</div>
            <ul>
                <li>
                    Added another Import function for Inventory Chain pages and Stock Delivery. Users may now choose available previous Invoices and transact it respectively
                </li>
            </ul>
        <div class="changelogs__subHeading">Supplier Column for Serial Tab in Product Summary</div>
            <ul>
                <li>
                    Added Supplier details of the item’s serial in Product Summary under Serial Tab
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.9.0.0 (August 17, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">POS Sales Head and Detail Report</div>
            <ul>
                <li>
                    Enhanced the POS Sales Report. Users may now view the details information of any POS transaction
                </li>
            </ul>
        <div class="changelogs__subHeading">Purchase Price as Cost Integration</div>
            <ul>
                <li>
                    Updated all of the transactions and added the cost column (database level)
                </li>
            </ul>
        <div class="changelogs__subHeading">New Income Statement Report</div>
            <ul>
                <li>
                    Changed the cost of goods sold, and based it to the Purchase Price
                </li>
            </ul>
        <div class="changelogs__subHeading">Transaction Cache</div>
            <ul>
                <li>
                    Users may now retrieve the details in the transaction they are currently doing if they encountered some issues
                </li>
            </ul>
        <div class="changelogs__subHeading">POS Integration</div>
            <ul>
                <li>
                    Integrated POS transactions in Product Transaction List and in Product Summary Report
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.8.0.0 (July 27, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Export Serial</div>
            <ul>
                <li>
                    Integrated export excel function under Product Reports, in the Serial Number tab to generate a local file that consists the Serial Numbers present on that product
                </li>
            </ul>
        <div class="changelogs__subHeading">Data Collector</div>
            <ul>
                <li>
                    Added another page to easily view and list the data collector details available for different modules
                </li>
            </ul>
        <div class="changelogs__subHeading">User Roles</div>
            <ul>
                <li>
                    Altered the User Roles function, it is now capable to select specific permissions for different branches available
                </li>
            </ul>
        <div class="changelogs__subHeading">Branch Filter Freezing</div>
            <ul>
                <li>
                    Updated the listing functionalities, it will now depend on where the user is currently logged, whether in the Main branch or in the other branches
                </li>
            </ul>
        <div class="changelogs__subHeading">String Filter</div>
            <ul>
                <li>
                    Changed the filter criteria for the user to easily chunk the list based on their preferences
                </li>
            </ul>
        <div class="changelogs__subHeading">Serial Auto Add</div>
            <ul>
                <li>
                    Added scan feature for Serial products that selects automatically the details when used
                </li>
            </ul>
        <div class="changelogs__subHeading">Product List Export Excel</div>
            <ul>
                <li>
                    Integrated export excel function in the Product List where user can now generate file that will contain product details
                </li>
            </ul>
        <div class="changelogs__subHeading">Product List Image Column</div>
            <ul>
                <li>
                    Added Image column in the Product list for the user to efficiently navigate product images
                </li>
            </ul>
        <div class="changelogs__subHeading">Mobile Usable</div>
            <ul>
                <li>
                    Revamped some parts of the system which makes it more viewable in smaller devices
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.7.0.0 (July 12, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Discounted Summary Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of all the transactions applied with direct discounts
                </li>
            </ul>
        <div class="changelogs__subHeading">Senior Transaction Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of all the transactions applied with Senior discounts
                </li>
            </ul>
        <div class="changelogs__subHeading">User Licensing</div>
            <ul>
                <li>
                    Added user license input in user module
                </li>
                <li>
                    Added license checking in login to allow only users with valid license
                </li>
            </ul>
        <div class="changelogs__subHeading">Dropdown Auto Complete</div>
            <ul>
                <li>
                    Integrated another feature for product selection where user can now add an item by selecting in the chosen box
                </li>
            </ul>
        <div class="changelogs__subHeading">Import Excel Function for Member</div>
            <ul>
                <li>
                    Added import excel function in the Member module which would help for bulk entry of details
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.6.1.0 (July 5, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Delivery Modules’ price basis settings integration</div>
            <ul>
                <li>
                    Added additional settings for Delivery Modules where users can now choose which price to reflect when making deliveries [Cost Price, Wholesale Price, Retail Price]
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.6.0 (June 27, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Stock Card Column Settings</div>
            <ul>
                <li>
                    Enhanced the Stock Card Report by adding column settings allowing users to choose their desired columns to display
                </li>
            </ul>
        <div class="changelogs__subHeading">Cash Transaction Report</div>
            <ul>
                <li>
                    Added another report generator that shows cash flow and transactions paid with cash
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Branch Inventory Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of product inventory depending on the selected branch
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Transaction List Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details per product and where it is transacted in the system
                </li>
            </ul>
        <div class="changelogs__subHeading">Negative Profit Product Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of all the products sold lower than the cost price
                </li>
            </ul>
        <div class="changelogs__subHeading">Member Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of all the transactions applied with Member discounts
                </li>
            </ul>
        <div class="changelogs__subHeading">Collection Due Warning</div>
            <ul>
                <li>
                    Added another report generator that lists the details of all the Sales Outbound transactions which are overdue and should be collected
                </li>
            </ul>
        <div class="changelogs__subHeading">Payment Due Warning</div>
            <ul>
                <li>
                    Added another report generator that lists the details of all the Purchase Inbound transactions which are overdue and should be paid
                </li>
            </ul>
        <div class="changelogs__subHeading">Import Excel Function for System Code</div>
            <ul>
                <li>
                    Added import excel function in the System Code which would help for bulk entry of details. This will take effect on the following modules: Brand, Bank, UOM, Reason, Payment Type, and Income Type
                </li>
            </ul>
        <div class="changelogs__subHeading">Import Excel Function for Product</div>
            <ul>
                <li>
                    Added import excel function in the Product module which would help for bulk entry of details
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.5.0 (June 18, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">POS API and POS Summary Posting</div>
            <ul>
                <li>
                    Integrated POS API to review and finalize transactions, specifically the everyday sales. It will also generate Summary Data of the finalized sales
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.4.0 (June 1, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Product Serial Number in Printout</div>
            <ul>
                <li>
                    Integrated Serial Number display in printout maker. It also has settings where user may manage how they want the details be shown
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Periodic Sales Report</div>
            <ul>
                <li>
                    Added another report generator that shows the product sales depending on users’ desired date range. This also comes with an excel generator
                </li>
            </ul>
        <div class="changelogs__subHeading">Salesman Report</div>
            <ul>
                <li>
                    Added another report generator that shows the monthly sales of a specific Salesman depending on users’ desired date range. This also shows off the details such as products, transactions, and amounts that the salesman transacted
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Serial Transaction Report</div>
            <ul>
                <li>
                    Added another report generator that shows the details of transactions where the Serial Number had gone through
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Report filter Design</div>
            <ul>
                <li>
                    Enhanced the filter function of Product Report by dividing it, and integrating it per tab, with its specific filter scopes
                </li>
            </ul>
        <div class="changelogs__subHeading">Serial Number Handling on Inventory Adjustment</div>
            <ul>
                <li>
                    Integrated Serial Number input on Inventory Adjustment. Users may now adjust inventory of Product which contains Serial Numbers by adding or removing them that depends on the total Difference Qty
                </li>
            </ul>
        <div class="changelogs__subHeading">POS Sales Report</div>
            <ul>
                <li>
                    Added another report generator that shows the transactions made in a POS from specific branch and location, on a specific date range
                </li>
            </ul>
        <div class="changelogs__subHeading">Edit details of approved transactions</div>
            <ul>
                <li>
                    Capability to alter details of an approved transaction. This includes the following information: attachments, remarks, header fields such as salesman, requested by, term, and others
                </li>
            </ul>
        <div class="changelogs__subHeading">Bank Transaction Report</div>
            <ul>
                <li>
                    Added another report generator that shows the ins and outs of money to and from a specific bank account
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.3.0 (May 17, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Product Inventory Warning Report</div>
            <ul>
                <li>
                    Added another report generator that shows the list of products which inventory count reached the minimum or the maximum value
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Monthly Sales Ranking</div>
            <ul>
                <li>
                    Added another report generator that shows the monthly sales per Product in descending order, based on its total amount
                </li>
            </ul>
        <div class="changelogs__subHeading">Supplier Monthly Sales Ranking</div>
            <ul>
                <li>
                    Added another report generator that shows the monthly sales per Supplier in descending order, based on its total amount
                </li>
            </ul>
        <div class="changelogs__subHeading">Category Monthly Sales Ranking</div>
            <ul>
                <li>
                    Added another report generator that shows the monthly sales per Category in descending order, based on its total amount
                </li>
            </ul>
        <div class="changelogs__subHeading">Brand Monthly Sales Ranking</div>
            <ul>
                <li>
                    Added another report generator that shows the monthly sales per Brand in descending order, based on its total amount
                </li>
            </ul>
        <div class="changelogs__subHeading">Income Statement</div>
            <ul>
                <li>
                    Added another report generator that displays the income statement based on users’ preferences and filters. The values displayed on this report has a specific redirection page, and the results displayed on that specific page depends on the users’ filters done under income statement
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Serial Availability Reports</div>
            <ul>
                <li>
                    Added another product report that shows all of the available serials in a branch and/or location
                </li>
            </ul>
        <div class="changelogs__subHeading">Application Table Tooltip</div>
            <ul>
                <li>
                    Enhanced the data table user experience, and added tooltip on every column which has too long information
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.2.0 (May 2, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Columns Settings User Save State</div>
            <ul>
                <li>
                    Capable to save columns on transaction details, selectors, and table list, which depends on users’ desire
                </li>
            </ul>
        <div class="changelogs__version">@lang('core::label.version') 1.1.0 (April 6, 2018)</div>
        <div class="changelogs__subHeading">System Stability Update</div>
        <div class="changelogs__subHeading">Product Group Type</div>
            <ul>
                <li>
                    Displayed the group type in the Product List whether it is a Manual Group, Manual Ungroup, Automatic Group, and/or Automatic Ungroup
                </li>
            </ul>
        <div class="changelogs__subHeading">Counter HTML Sheet Printout</div>
            <ul>
                <li>
                    Added a printout template for the Counter details
                </li>
            </ul>
        <div class="changelogs__subHeading">Express Search [Stock Number, Barcode, Product Name] in Product Selector</div>
            <ul>
                <li>
                    Added designated fields for product’s Stock Number, Barcode, and Name to instantly filter the results
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Default Unit Implementation</div>
            <ul>
                <li>
                    Integrated additional product information named “Default Unit” where user can choose what default unit to be shown as always in the transaction.
                    <i>Disclaimer: This won’t take effect if Customer or Supplier is under Historical Settings</i>
                </li>
                <li>
                    For Automatic Group Products, components of the group and its unit will be forced to <b>piece</b>, and the total quantity of it will be multiplied on the user-indicated component’s specific quantity
                </li>
            </ul>
        <div class="changelogs__subHeading">Product Price Instant Copy to Other Branches</div>
            <ul>
                <li>
                    Added another column where user can choose the specific branch to copy product price and set as their branch price
                </li>
            </ul>
        <div class="changelogs__subHeading">Initial Data</div>
            <ul>
                <li>
                    Added initial data named <b>None</b> for Brand, Category, and Supplier
                </li>
            </ul>
        <div class="changelogs__subHeading">Direct Approval After Saving the Sheet</div>
            <ul>
                <li>
                    Integrated direct approval function after saving a sheet. User can now choose to direct approve the sheet, or just proceed, and sheet will be saved
                </li>
            </ul>
        <div class="changelogs__subHeading">Continuous Product Creation</div>
            <ul>
                <li>
                    Added a new button under Product page where user can create entries continuously
                </li>
            </ul>
        <div class="changelogs__subHeading">Column Settings Implementation</div>
            <ul>
                <li>
                    Integrated Column Settings for the following pages: Branch, Customer, Supplier, Member, and User. User can now select the columns what to display when they are viewing the results in the list
                </li>
            </ul>
        <div class="changelogs__subHeading">Icons Revamped</div>
            <ul>
                <li>
                    Modified the Menu Icons into flat design for the user to quickly understand the function of the page
                </li>
            </ul>
    </div>
@stop

@section('message')
@stop

@section('script')
    <script src="{{ elixir('js/ChangeLogs/changelogs.js') }}"></script>
@stop
