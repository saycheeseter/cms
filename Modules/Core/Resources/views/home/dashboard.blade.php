@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container')
@section('title', trans('core::label.dashboard'))

@section('stylesheet')
    <style type="text/css">
        .dashboard-display {
            padding-top: 150px;
        }
        .dashboard-display img {
            width: 550px;
        }
        .dashboard-display h3 {
            margin-top: -5px;
        }
        .container-1 {
            background-image: url('/images/spiration-light.png');
        }
    </style>
@stop

@section('container-1')
    <div class="dashboard-display c-has-bt" align="center">
        <img src="{{ asset('images/Nelsoft_Logo_Landscape_4.png') }}">
        <h1>NELSOFT SYSTEMS INCORPORATED</h1>
        <h3>CLOUD INVENTORY AND RETAIL MANAGEMENT SYSTEM</h3>
    </div>
@stop

@section('message')
@stop

@section('script')
    <script src="{{ elixir('js/Dashboard/dashboard.js') }}"></script>
@stop