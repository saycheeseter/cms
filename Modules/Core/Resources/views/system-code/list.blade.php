@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', $title)

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
            <template slot="right">
                @if($permissions['import_excel'])
                    @component('core::components.import-excel')
                    @endcomponent
                @endif
            </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm"
        v-on:update="validate"
        v-on:add="validate">
        <template slot="header">
            <table-header>@lang('core::label.code')</table-header>
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                <input type="text" class="required" :disabled="disabled(row.data.deletable)" v-model="row.data.code" @keydown="(permissions.update) ? changeState(row.index, 'update') : false" />
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <input type="text" class="required" :disabled="disabled(row.data.deletable)" v-model="row.data.name" @keydown="(permissions.update) ? changeState(row.index, 'update') : false"/>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <input type="text" :disabled="disabled(row.data.deletable)" v-model="row.data.remarks" @keydown="(permissions.update) ? changeState(row.index, 'update') : false"/>
            </td>
        </template>
        @if ($permissions['create'])
            <template slot="input">
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--code">
                    <input v-focus v-model="fields.code" class="required" type="text" />
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <input v-model="fields.name" class="required" type="text"/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                    <input v-model="fields.remarks" @keydown.enter="validate()" type="text"/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <img src="{{ asset('images/iconupdate.png') }}" @click="validate()">
                </table-input>
            </template>
        @endif
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.data'),
        'content' => trans('core::confirm.delete.data')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    @stack('modals')
@stop

@section('script')
    @javascript('segment', Request::segment(1))
    @javascript('unit', setting('default.unit'))
    @javascript('permissions', $permissions)
    
    <script src="{{ elixir('js/SystemCode/list.js') }}"></script>
@stop
