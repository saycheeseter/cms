<control-bar id='controls'>
    <template slot="left">
        <control-button name='add-product' icon="fa fa-plus" v-if="fullyEnabled" @click="productPicker.visible = true" v-show="direct">
            @lang('core::label.product')
        </control-button>
        @if($permissions['column_settings'])
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.details.visible = true">
                {{-- @lang('core::label.column.settings') --}}
            </control-button>
        @endif
        {{-- <control-dropdown icon="fa fa-barcode" v-if="fullyEnabled">
            @lang('core::label.scan.mode')
            <template slot="list">
                <li class="radio-checkbox">
                    <a href="#">
                        <label>
                            <input name="scan" type="radio">
                            @lang('core::label.by.bulk')
                        </label>
                    </a>
                </li>
                <li class="radio-checkbox">
                    <a href="#">
                        <label>
                            <input name="scan" type="radio">
                            @lang('core::label.by.piece')
                        </label>
                    </a>
                </li>
            </template>
        </control-dropdown> --}}
        <div class="control-border control-form" v-if="fullyEnabled" v-show="direct">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.qty'):</td>
                    <td><number-input class="field__qty" v-model='scan.quantity'></number-input></td>
                </tr>
            </table>
        </div>
        <div class="control-border control-form" v-if="fullyEnabled" v-show="direct">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.scan'):</td>
                    <td><input type="text" @keydown.enter="findByBarcode" placeholder="@lang('core::label.barcode')"></td>
                </tr>
            </table>
        </div>
        <control-dropdown name='import' icon="fa fa-upload" v-if="fullyEnabled" v-show="direct">
            @lang('core::label.copy')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.transaction'):</td>
                            <td><input name='reference-number' v-model='importing.referenceNumber' type="text" @keydown.enter='importTransaction' placeholder="@lang('core::label.sheet.number')"></td>
                            <td><input name='import-reference' type="button" class="c-btn c-dk-blue" value="@lang('core::label.copy')" @click='importTransaction'></td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
        @include('core::attachment.transaction')
        @stack('controls-left')
    </template>
    <template slot="right">
        @stack('controls-right')

        @if($templates->count() > 0 && $permissions['print'])
            <control-dropdown name='print' icon="fa fa-print" v-if="!newTransaction">
                {{-- @lang('core::label.print') --}}
                <template slot="list">
                    @foreach($templates as $printout)
                        <li><a href="javascript:void();" @click="print({{ $printout->id }})">{{ $printout->name }}</a></li>
                    @endforeach
                </template>
            </control-dropdown>
        @endif

        <control-button name='save-add-new' v-if="controls.draft" icon="fa fa-save" @click='process(true)'>
            @lang('core::label.save.and.add.new')
        </control-button>

        <control-button name='save' v-if="controls.draft" icon="fa fa-save" @click="process">
            {{-- @lang('core::label.save') --}}
        </control-button>

        <control-button name='save' v-if="approvedTransaction && !forViewingOnly" icon="fa fa-save" @click="updateApprovedTransaction()">
            {{--@lang('core::label.save')--}}
        </control-button>

        @if($permissions['update'])
            <control-button name='for-approval' v-if="controls.forApproval" icon="fa fa-thumbs-o-up" @click="forApprovalTransaction()">
                @lang('core::label.for.approval')
            </control-button>
        @endif

        @if($permissions['approve'])
            <control-button name='approve' v-if="controls.approveDecline" icon="fa fa-check fa-green" @click="approveTransaction()">
                @lang('core::label.approve')
            </control-button>
        @endif

        @if($permissions['decline'])
            <control-button name='decline' v-if="controls.approveDecline" icon="fa fa-close fa-red" @click="declineTransaction()">
                @lang('core::label.decline')
            </control-button>
        @endif

        @if($permissions['decline'] || $permissions['approve'])
            <control-button name='revert' v-if="controls.revert" icon="fa fa-arrow-circle-left" @click="revertTransaction()">
                @lang('core::label.revert')
            </control-button>
        @endif
    </template>
</control-bar>
