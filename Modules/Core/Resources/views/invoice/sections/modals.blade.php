@component('core::components.confirm', [
    'id' => 'confirm-details',
    'show' => 'confirm.visible',
    'callback' => 'removeProduct',
    'header' => trans('core::label.remove.product'),
    'content' => trans('core::confirm.remove.product')
])
@endcomponent

@include('core::modal.product-transaction-selector')
@include('core::modal.alternative-item-dialog')

<column-select
    :columns='columnSettings.details.columns'
    v-model='columnSettings.details.selections'
    v-on:close='columnSettings.details.visible = false'
    v-show='columnSettings.details.visible'
    id='details-column-modal'
    :module='columnSettings.details.module'>
</column-select>

<dialog-box
    :show='dialog.process.message !=""'
    @close='redirect'>
    <template slot='content'>@{{ dialog.process.message }}</template>
</dialog-box>

@component('core::components.confirm', [
    'id' => 'warning',
    'show' => 'warning.visible',
    'callback' => 'removeProducts',
    'header' => trans('core::label.warning'),
    'cancel' => 'revertInfo',
    'content' => trans('core::confirm.changing.will.remove.products')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'retrieve-unsaved-data',
    'show' => 'cache.modal',
    'callback' => 'loadCache',
    'header' => trans('core::label.existing.unsaved.data'),
    'content' => trans('core::confirm.retrieve.unsaved.data')
])
@endcomponent

<modal 
    v-show='dialog.directApproval.visible'
    @close='redirect'>
    <template slot="header">
        @{{ dialog.directApproval.message }}
    </template>
    <template slot="content">
        @lang('core::info.direct.approve')
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-blue" @click='directApproval()'>@lang('core::label.approve')</button>
        <button class="c-btn c-dk-green" ref="saveOnly" @click='redirect'>@lang('core::label.proceed')</button>
    </template>
</modal>