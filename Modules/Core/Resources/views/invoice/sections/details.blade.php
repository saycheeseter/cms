<datatable
    v-bind:tableId="datatable.details.id"
    v-bind:settings="datatable.details.settings"
    v-bind:table="datatable.details.value"
    v-on:destroy="confirmation"
    v-on:update="validateDetail"
    ref='detail'>
    <template slot="header">
        <table-header v-show='columnSettings.details.selections.barcode'>@lang('core::label.barcode')</table-header>
        <table-header v-show='columnSettings.details.selections.stockNo'>@lang('core::label.stock.no')</table-header>
        <table-header>@lang('core::label.product.name')</table-header>
        <table-header v-show='columnSettings.details.selections.chineseName'>@lang('core::label.chinese.name')</table-header>
        <table-header>@lang('core::label.qty')</table-header>
        <table-header>@lang('core::label.uom')</table-header>
        <table-header>@lang('core::label.unit.specs')</table-header>
        <table-header>@lang('core::label.total.qty')</table-header>

        @if($permissions['show_price_amount'])
            <table-header>@lang('core::label.o.price')</table-header>
            <table-header>@lang('core::label.price')</table-header>
        @endif

        <table-header v-show='columnSettings.details.selections.discount1'>@lang('core::label.discount.1')</table-header>
        <table-header v-show='columnSettings.details.selections.discount2'>@lang('core::label.discount.2')</table-header>
        <table-header v-show='columnSettings.details.selections.discount3'>@lang('core::label.discount.3')</table-header>
        <table-header v-show='columnSettings.details.selections.discount4'>@lang('core::label.discount.4')</table-header>

        @if($permissions['show_price_amount'])
            <table-header>@lang('core::label.amount')</table-header>
        @endif

        <table-header v-show='columnSettings.details.selections.remarks'>@lang('core::label.remarks')</table-header>

        @stack('table-header')
        <table-header v-show='!disabled'>
            <div @click="finalizeAll('detail')">
                <img class="edit-image" src="/images/iconupdate.png">
            </div>
        </table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.barcode }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stockNo'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.stock_no }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <i class="fa fa-warning fa-lg fa-gold v-dataTable__inlineIcon" v-show="row.data.inventory_warning" @click="showAlternativeItems(row.data.product_id, row.dataIndex)"></i>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.chinese_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <number-input
                onfocus="this.select()"
                :ref = "'qty-' + row.dataIndex"
                v-model="row.data.qty" 
                @input="setTotalQty(row.dataIndex);changeState(row.index, 'update')"
                @blur="checkInventoryForWarning(row.data)"
                :disabled='disabled'>
            </number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
            <chosen 
                name='uom'
                v-model='row.data.unit_id'
                :options='row.data.units'
                :row='row.dataIndex' 
                @attribute='setUnitSpecs'
                @change="changeState(row.index, 'update');checkInventoryForWarning(row.data)"
                :disabled='disabled'
            ></chosen>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.unit_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.total_qty }}
                </template>
            </table-cell-data>
        </td>
        @if($permissions['show_price_amount'])
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.oprice }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <number-input
                    v-model="row.data.price"
                    @keydown.native="zeroDiscounts(row.dataIndex)"
                    @keydown.native.enter="validateDetail(row.data, row.index, row.dataIndex)"
                    @input="setAmount(row.dataIndex);changeState(row.index, 'update')"
                    :disabled='disabled'>
                </number-input>
            </td>
        @endif
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount1'>
            <number-input v-model="row.data.discount1" @input="setDiscount(row.dataIndex);changeState(row.index, 'update')" :disabled='disabled' :max='100'></number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount2'>
            <number-input v-model="row.data.discount2" @input="setDiscount(row.dataIndex);changeState(row.index, 'update')" :disabled='disabled' :max='100'></number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount3'>
            <number-input v-model="row.data.discount3" @input="setDiscount(row.dataIndex);changeState(row.index, 'update')" :disabled='disabled' :max='100'></number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount4'>
            <number-input v-model="row.data.discount4" @input="setDiscount(row.dataIndex);changeState(row.index, 'update')" :disabled='disabled' :max='100'></number-input>
        </td>
        @if($permissions['show_price_amount'])
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
        @endif
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.details.selections.remarks'>
            <input type="text" v-model="row.data.remarks" @keydown="changeState(row.index, 'update')" :disabled='disabled'/>
        </td>
        @stack('table-display')
    </template>
    <template slot="input" v-if='showBarcodeStockNoInput'>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
            <input tabindex="-1" type="text" ref="barcodeInput" @keydown.enter="findByBarcodeInput"/>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stockNo'>
            <input tabindex="-1" type="text" @keydown.enter="findByStockNoInput"/>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name ws-normal">
            <chosen
                :options="reference.products"
                :callback="searchProductByName"
                @fresh="clearProductDropdownAndValue"
                @selected="findByProductSelected">
            </chosen>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--uom"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs"></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty"></table-input>

        @if($permissions['show_price_amount'])
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount"></table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount"></table-input>
        @endif

        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount1'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount2'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount3'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount4'></table-input>

        @if($permissions['show_price_amount'])
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount"></table-input>
        @endif

        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.details.selections.remarks'></table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action"></table-input>
    </template>
</datatable>