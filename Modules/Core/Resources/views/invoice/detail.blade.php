@extends('core::layouts.master')
@extends('core::layouts.flexible.three-container')

@section('stylesheet')
    <style type="text/css">
        .container-1 {
            flex: 0 0 auto;
            overflow: initial;
        }
        .container-3 {
            max-height: 80px;
        }
    </style>
@stop

@section('container-1')
    @include('core::invoice.sections.main-info')
@stop

@section('container-2')
    @include('core::invoice.sections.controls')
    @include('core::invoice.sections.details')
@stop

@section('container-3')
    @include('core::invoice.sections.summary')
@stop

@section('message')
    <message type="danger" :show="message.transaction.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.transaction.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.transaction.success != ''">
        <span v-text="message.transaction.success"></span>
    </message>
    <message type="info" :show="message.transaction.info != ''"  @close="message.reset()">
        <span v-text="message.transaction.info"></span>
    </message>
@stop

@section('modals')
    @include('core::invoice.sections.modals')
    @stack('modals')
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('users', $users)
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('permissions', $permissions)
    @javascript('udfs', $udfs)
    
    @stack('script')
@stop