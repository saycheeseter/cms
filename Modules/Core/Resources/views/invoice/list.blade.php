@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            @if($permissions['create'])
                <control-button v-if='enabled' name='add-new-transaction' icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.transaction')
                </control-button>
            @endif

            <control-button name='filters' icon="fa fa-search" @click="filter.visible = true">
                @lang('core::label.filters')
            </control-button>

            @if($permissions['column_settings'])
                <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
        </template>
        <template slot="right">
            @if($permissions['export'])
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a> 
                        </li>
                        @foreach ($templates as $template)
                            <li>
                                <a @click="excel( {{ $template->id }} )" >
                                    {{ $template->name }} 
                                </a> 
                            </li>
                        @endforeach
                    </template>
                </control-dropdown>
            @endif

            @if($permissions['approve'])
                <control-button v-if='enabled' name='approve' icon="fa fa-check fa-green" @click="approveTransaction()">
                    @lang('core::label.approve')
                </control-button>
            @endif

            @if($permissions['decline'])
                <control-button v-if='enabled' name='decline' icon="fa fa-close fa-red" @click="declineTransaction()">
                    @lang('core::label.decline')
                </control-button>
            @endif
        </template>
    </control-bar>
    
    @yield('datatable')
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.invoice'),
        'content' => trans('core::confirm.delete.invoice')
    ])
    @endcomponent

    @include('core::modal.invoice-filter', [
        'show' => 'filter.visible',
        'options' => 'filter.options',
        'model' => 'filter.data',
        'search' => 'paginate'
    ])

    <column-select
        v-bind:columns='columnSettings.columns'
        v-bind:id='columnSettings.id'
        v-show="columnSettings.visible"
        v-model='columnSettings.selections'
        @close="columnSettings.visible = false"
        :module='columnSettings.module'>
    </column-select>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('permissions', $permissions)
    @javascript('udfs', $udfs)

    @stack('script')
@stop