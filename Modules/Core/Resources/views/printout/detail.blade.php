@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.printout.template'))

@section('stylesheet')
    <style type="text/css">
        .c-sub-sidebar {
            flex-basis: 300px;
            min-width: 300px;
        }
        .c-sub-main-content {
            background: #ddd;
            font-family: Arial;
            overflow: initial;
        }
        #margin img {
            margin-top: 1px;
        }
        #paper i {
            margin-top: 2px;
        }
        #paper .dropdown-menu {
            min-width: 200px;
        }
        .printout-page {
            background: #FFF;
            box-shadow: 0 4px 5px rgba(75, 75, 75, 0.2);
        }
        .extra-info {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .extra-info table {
            padding-left: -3px;
            padding-right: -3px;
            width: 100%;
        }
        .extra-info td {
            text-align: left;
        }
    </style>
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td class="text-right">@lang('core::label.name'):</td>
                <td><input type="text" class="required" v-model="template.name"></td>
                <td>@lang('core::label.module'):</td>
                <td>
                    <chosen
                        class="required"
                        name="module"
                        v-model="template.module_id"
                        :disabled="disabled"
                        :options="modules"
                        @change="changeTemplate()"
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
    @include('core::printout.sections.detail.controls')
    <div class="c-sub-nav-content">
        <div class="c-sub-sidebar flex flex-column">
            <div id='tabs' class="c-nav-tabs clearfix">
                <template v-for="(tab, key) in tabs.get()">
                    <div class="c-nav-tab"
                         v-text="tab.label"
                         v-if="section != 'content' || key != 'labels'"
                         :class="{ active: tab.active }"
                         @click="tabs.active(key)">
                    </div>
                </template>
            </div>
            <div class="c-nav-tabs-container flex flex-column flex-1 overflow">
                <div class="c-nav-tabs-content" v-if="tabs.labels.active">
                    @include('core::printout.sections.detail.labels')
                </div>
                <div class="c-nav-tabs-content" v-if="tabs.values.active">
                    @include('core::printout.sections.detail.values')
                </div>
            </div>
        </div>
        <div id="print" class="c-sub-main-content">
            <div class="c-sub-header c-has-bb">
                @lang('core::label.print.preview')
            </div>
            <div class="overflow">
                @include('core::printout.sections.detail.preview')
            </div>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Printout/detail.js') }}"></script>
@stop