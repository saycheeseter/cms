@inject('paper', "Modules\Core\Services\Contracts\PaperServiceInterface")

@php
    $content = count($data['details']);

    if (property_exists($template->format->content, 'serials')
        && filter_var($template->format->content->serials->enabled, FILTER_VALIDATE_BOOLEAN)
        && array_key_exists('serials', $data['details'][0])
    ) {
        switch ($template->format->content->serials->type) {
            case 'basic':
                $serialsSet = array_pluck(array_pluck($data['details'], 'serials'), '*.serial_number');

                foreach($serialsSet as $serials) {
                    $content += ceil(count($serials) / (int) $template->format->content->serials->basic->cell);
                }
                break;

            case 'advance':
                // Temporary but will be changed upon implementation of the new printout
                $content += $serials;
                break;
        }
    }

    $pages = ceil($content / (((int) $template->format->general->content->rows) - 1));

    $GLOBALS['iterator'] = [
        'details' => [
            'continue' => true,
            'index' => 0,
        ],
        'serials' => [
            'index' => 0
        ],
        'count' => 0,
        'current' => 0
    ];
@endphp

@extends('core::layouts.print')

@section('stylesheet')
    <style type="text/css">
        .c-sub-sidebar {
            flex-basis: 300px;
            min-width: 300px;
            overflow: initial;
        }
        .container-1 {
            max-height: 70px;
        }
        .container-2 {
            max-height: 577px;
        }
        .c-sub-main-content {
            background: #FFF;
            font-family: Arial;
        }
        @page {
            margin: 0cm;
        }
        .pr-margin {
            margin-top: {{ $template->format->general->margin->top }}cm;
            margin-bottom: {{ $template->format->general->margin->bottom }}cm;
            margin-left: {{ $template->format->general->margin->left }}cm;
            margin-right: {{ $template->format->general->margin->right }}cm;
        }
    </style>
@stop

@section('content')
    {{--Should be change to do-while or infinite for loop--}}
    @for($i = 0; $i < $pages; $i++)
        <div class="
            printout-page
            {{ $template->format->general->paper->size }}
            {{ $template->format->general->paper->orientation }}
        ">
            <div class="
                printout-wrapper
                pr-margin
            ">
                @include('core::printout.sections.template.header')
                @include('core::printout.sections.template.content')

                {{--If duplicate footer option is enabled or if not, then if all transaction details have been printed--}}
                @if(filter_var($template->format->general->footer->duplicate, FILTER_VALIDATE_BOOLEAN) || ($i + 1) == $pages)
                    @include('core::printout.sections.template.footer')
                @else
                    {{--Print only an empty row--}}
                    @for ($y = 1; $y <= (int) $template->format->general->footer->rows; $y++)
                        <div class="printout-row"></div>
                    @endfor
                @endif

                {{--Print the current page number--}}
                @if(!empty($template->format->general->pageNumber))
                    <div class="printout-page-number {{ $template->format->general->pageNumber }}">Page {{ ($i + 1) }} of {{ $pages }}</div>
                @endif
            </div>
        </div>
    @endfor
@stop