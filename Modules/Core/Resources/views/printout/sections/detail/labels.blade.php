<div v-if="section == 'header' && template.module_id != null">
    <grid-label v-for="(field, key) in sortObjectByKey(template.format.header.labels)" :label="$t('label.' + field.header)" key="key" v-model="template.format.header.labels[key]" :options="options.header"></grid-label>
</div>
<div v-if="section == 'footer' && template.module_id != null">
    <grid-label v-for="(field, key) in sortObjectByKey(template.format.footer.labels)" :label="$t('label.' + field.header)" key="key" v-model="template.format.footer.labels[key]" :options="options.footer"></grid-label>
</div>