<div v-if="section == 'header' && template.module_id != null">
    <grid-info v-for="(field, key) in sortObjectByKey(template.format.header.values)" :label="$t('label.' + field.header)" key="key" v-model="template.format.header.values[key]" :options="options.header"></grid-info>
</div>
<div v-if="section == 'content' && template.module_id != null">
    <grid-content v-for="(field, key) in sortObjectByKey(template.format.content.columns)" :label="$t('label.' + field.header)" key="key" v-model="template.format.content.columns[key]"></grid-content>
</div>
<div v-if="section == 'footer' && template.module_id != null">
    <grid-info v-for="(field, key) in sortObjectByKey(template.format.footer.values)" :label="$t('label.' + field.header)" key="key" v-model="template.format.footer.values[key]" :options="options.footer"></grid-info>
</div>