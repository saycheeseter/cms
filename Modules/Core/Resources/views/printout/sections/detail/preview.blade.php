<div v-if="template.module_id != null" :class="[
    'printout-page',
    template.format.general.paper.size,
    template.format.general.paper.orientation]" :style="{
    'padding-top': template.format.general.margin.top + 'cm',
    'padding-bottom': template.format.general.margin.bottom + 'cm',
    'padding-left': template.format.general.margin.left + 'cm',
    'padding-right': template.format.general.margin.right + 'cm'
    }">
    <div class="printout-wrapper">
        <div :class="['printout-row', { bordered: bordered }]" v-for="n in template.format.general.header.rows">
            <template v-for="field in sortInfoFieldsBy(template.format.header, 'sequenceInRow')">
                <template v-if="field.y == n && field.checked">
                    <div :class="['pr-column', { bordered: bordered }, 'col-xs-' + field.width]">
                        <div :class="['col-xs-offset-' + field.x, 'text-' + field.alignment]">
                            <div :class="{
                                'border-top': field.border.top,
                                'border-left': field.border.left,
                                'border-right': field.border.right,
                                'border-bottom': field.border.bottom }">
                                <span :class="[
                                    'fs-' + field.font.size,
                                    'font-weight-' + field.font.weight,
                                    'font-style-' + field.font.style,
                                    'text-decoration-' + field.font.decoration]">
                                    @{{ field.label || $t('label.' + field.header) }}
                                </span>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
        </div>
        <div :class="['printout-row content clearfix', { bordered: bordered }]">
            <div class="pr-column">
                <template v-if="isAnyContentFieldChecked ">
                    <table class="pr-datatable" :border="template.format.general.content.border == 1 ? 1 : 0">
                        <tr
                            v-for="n in this.template.format.general.content.rows"
                            :class="{
                                'pr-border-bottom': template.format.general.content.border == 2,
                                'pr-border-top': template.format.general.content.border == 3,
                            }"
                        >
                            <template v-if="isSerialsEnabled">
                                <td :class="['pr-column', { bordered: bordered }, 'text-center']"
                                    v-if="n === 1 || n % 2 === 0"
                                    style="width:0.5%">
                                    @{{ n === 1 ? '' : n / 2 }}
                                </td>
                            </template>

                            <template v-if="!isSerialsEnabled">
                                <td :class="['pr-column', { bordered: bordered }, 'text-center']"
                                    style="width:0.5%">
                                    @{{ n === 1 ? '' : n - 1 }}
                                </td>
                            </template>

                            <template v-if="(n == 1 || (isSerialsEnabled && n % 2 === 0) || (!isSerialsEnabled))">
                                <template v-for="field in sortFieldsBy(template.format.content.columns, 'sequence')">
                                    <template v-if="field.checked">
                                        <td :class="[
                                            'pr-column', { bordered: bordered },
                                            'text-' + (n == 1 ? 'center' : field.alignment),
                                            'font-weight-' + (n == 1 ? field.font.weight : ''),
                                            'font-style-' + (n == 1 ? field.font.style : ''),
                                            'text-decoration-' + (n == 1 ? field.font.decoration : '')]"
                                            :style="{ width: field.width + '%' }">
                                            @{{ field.label }}
                                        </td>
                                    </template>
                                </template>
                            </template>

                            <template v-if="isSerialsEnabled && n % 2 !== 0 && n !== 1">
                                <td :class="['pr-column', { bordered: bordered }]" :colspan="template.format.content.serials.startAt">@{{ '' }}</td>
                                <td :class="['pr-column', { bordered: bordered }]" :colspan="(contentFieldCheckedCount + 1) - template.format.content.serials.startAt" class="extra-info">
                                    <table class="pr-datatable" :border="template.format.general.content.border == 1 ? 1 : 0">
                                        <tr>
                                            <template v-if="template.format.content.serials.type === 'basic'">
                                                <td :class="['pr-column', { bordered: bordered }]" v-for="x in template.format.content.serials.basic.cell">1234567890</td>
                                            </template>

                                            <template v-if="template.format.content.serials.type === 'advance'">
                                                <td :class="['pr-column', { bordered: bordered }]" v-for="field in template.format.content.serials.advance" v-if="field.enabled" :style="{ width: field.width + '%' }">
                                                    @{{ $t('label.' + field.label) }}
                                                </td>
                                            </template>
                                        </tr>
                                    </table>
                                </td>
                            </template>
                        </tr>
                    </table>
                </template>

            </div>
        </div>
        <div :class="['printout-row', { bordered: bordered }]" v-for="n in template.format.general.footer.rows">
            <template v-for="field in sortInfoFieldsBy(template.format.footer, 'sequenceInRow')">
                <template v-if="field.y == n && field.checked">
                    <div :class="['pr-column', { bordered: bordered }, 'col-xs-' + field.width]">
                        <div :class="['col-xs-offset-' + field.x, 'text-' + field.alignment]">
                            <div :class="{
                                'border-top': field.border.top,
                                'border-left': field.border.left,
                                'border-right': field.border.right,
                                'border-bottom': field.border.bottom }">
                                <span :class="[
                                    'fs-' + field.font.size,
                                    'font-weight-' + field.font.weight,
                                    'font-style-' + field.font.style,
                                    'text-decoration-' + field.font.decoration]">
                                    @{{ field.label || $t('label.' + field.header) }}
                                </span>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
        </div>
        <div :class="[
            'printout-page-number',
            template.format.general.pageNumber]"
             v-show="template.format.general.pageNumber != ''">
            Page 1 of 1
        </div>
    </div>
</div>