<control-bar id="controls" class="c-has-bb">
    <template slot="left">
        <div class="control-border control-form">
            <table class="c-tbl-layout">
                <tr>
                    <td>
                        @lang('core::label.section'):
                    </td>
                    <td class="c-width-100">
                        <select v-model="section" @change="changeSection()">
                            <option value="header">@lang('core::label.header')</option>
                            <option value="content">@lang('core::label.content')</option>
                            <option value="footer">@lang('core::label.footer')</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="control-border control-form">
            <table class="c-tbl-layout">
                <tr>
                    <td>
                        <label class="radio-chkbx">
                            <input type="checkbox" v-model="bordered">
                            @lang('core::label.show.borders')
                        </label>
                    </td>
                </tr>
            </table>
        </div>

        <template v-if="template.format.content.hasOwnProperty('decimals')">
            <control-dropdown id="show-decimal" icon="fa fa-circle" :show="section == 'content'">
                @lang('core::label.show.decimal')
                <template slot="list">
                    <li class="radio-checkbox dropdown-form">
                        <table class="c-tbl-layout">
                            <tr v-for="decimal in template.format.content.decimals">
                                <td class="c-width-100">@{{ $t('label.' + decimal.label) }}:</td>
                                <td>
                                    <input type="checkbox" v-model="decimal.value">
                                </td>
                            </tr>
                        </table>
                    </li>
                </template>
            </control-dropdown>
        </template>

        <template v-if="template.format.content.hasOwnProperty('serials')">
            <control-dropdown id="show-serials" icon="fa fa-barcode" :show="section == 'content'">
                @lang('core::label.show.serials')
                <template slot="list">
                    <li class="radio-checkbox dropdown-form">
                        <table class="c-tbl-layout">
                            <tr>
                                <td>@lang('core::label.enabled'):</td>
                                <td><input type="checkbox" v-model="template.format.content.serials.enabled"></td>
                            </tr>
                            <tr>
                                <td>@lang('core::label.start.at.column'):</td>
                                <td colspan="2"><number-input v-model.number="template.format.content.serials.startAt" :precision="false" :max="contentFieldCheckedCount" :formatted="false"></number-input></td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td>@lang('core::label.type'):</td>--}}
                                {{--<td colspan="2">--}}
                                    {{--<select v-model="template.format.content.serials.type">--}}
                                        {{--<option value="basic">@lang('core::label.basic')</option>--}}
                                        {{--<option value="advance">@lang('core::label.advance')</option>--}}
                                    {{--</select>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            <template v-if="template.format.content.serials.type === 'basic'">
                                <tr>
                                    <td>@lang('core::label.cell'):</td>
                                    <td>
                                        <number-input v-model.number="template.format.content.serials.basic.cell" :precision="false" :formatted="false"></number-input>
                                    </td>
                                </tr>
                            </template>
                            <template v-if="template.format.content.serials.type === 'advance'">
                                <tr v-for="field in template.format.content.serials.advance">
                                    <td>@{{ $t('label.' + field.label) }}</td>
                                    <td>
                                        <input type="checkbox" v-model="field.enabled">
                                    </td>
                                    <td>
                                        <number-input v-model.number="field.width" :formatted="false"></number-input>
                                    </td>
                                </tr>
                            </template>
                        </table>
                    </li>
                </template>
            </control-dropdown>
        </template>

        <div class="control-border control-form" v-show="section == 'footer'">
            <table class="c-tbl-layout">
                <tr>
                    <td>
                        <label class="radio-chkbx">
                            <input type="checkbox" v-model="template.format.general.footer.duplicate">
                            @lang('core::label.duplicate.footer')
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <control-dropdown id="margin" image="/images/svg/margins.svg">
            @lang('core::label.margin')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.left'):</td>
                            <td><number-input type="text" v-model="template.format.general.margin.left"></number-input></td>
                            <td>cm</td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.right'):</td>
                            <td><number-input type="text" v-model="template.format.general.margin.right"></number-input></td>
                            <td>cm</td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.top'):</td>
                            <td><number-input type="text" v-model="template.format.general.margin.top"></number-input></td>
                            <td>cm</td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.bottom'):</td>
                            <td><number-input type="text" v-model="template.format.general.margin.bottom"></number-input></td>
                            <td>cm</td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
        <control-dropdown id="paper" icon="fa fa-newspaper-o">
            @lang('core::label.paper')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.size'):</td>
                            <td class="c-width-150">
                                <select v-model="template.format.general.paper.size">
                                    <option value="A4">A4</option>
                                    <option value="A4-half">A4 - Half</option>
                                    <option value="letter">Letter</option>
                                    <option value="letter-half">Letter - Half</option>
                                    <option value="long">Long</option>
                                    <option value="long-half">Long - Half</option>
                                    <option value="legal">Legal</option>
                                    <option value="legal-half">Legal - Half</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.orientation'):</td>
                            <td>
                                <select v-model="template.format.general.paper.orientation">
                                    <option value="landscape">@lang('core::label.landscape')</option>
                                    <option value="portrait">@lang('core::label.portrait')</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
        <control-dropdown icon="fa fa-square-o" :show="section == 'content'">
            @lang('core::label.border')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.type'):</td>
                            <td>
                                <select v-model="template.format.general.content.border">
                                    <option value="1">@lang('core::label.full.border')</option>
                                    <option value="2">@lang('core::label.bottom.border')</option>
                                    <option value="3">@lang('core::label.top.border')</option>
                                    <option value="0">@lang('core::label.no.border')</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
        <control-dropdown icon="fa fa-hash">
            @lang('core::label.page.number')
            <template slot="list">
                <li class="radio-checkbox dropdown-form">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.type'):</td>
                            <td>
                                <select v-model="template.format.general.pageNumber">
                                    <option value="">@lang('core::label.none')</option>
                                    <option value="top-left">@lang('core::label.top.left')</option>
                                    <option value="top-center">@lang('core::label.top.center')</option>
                                    <option value="top-right">@lang('core::label.top.right')</option>
                                    <option value="bottom-left">@lang('core::label.bottom.left')</option>
                                    <option value="bottom-center">@lang('core::label.bottom.center')</option>
                                    <option value="bottom-right">@lang('core::label.bottom.right')</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </li>
            </template>
        </control-dropdown>
        <div class="control-border control-form">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.no.of.rows'):</td>
                    <td class="c-width-100">
                        <input type="text" v-model.number="template.format.general[section].rows">
                    </td>
                </tr>
            </table>
        </div>
    </template>
    <template slot="right">
        <control-button name='save' icon="fa fa-save" @click="save">
            @lang('core::label.save')
        </control-button>
    </template>
</control-bar>