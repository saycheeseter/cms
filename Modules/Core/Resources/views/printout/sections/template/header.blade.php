@php ($headers = array_to_object(collect(object_to_array($template->format->header))->flatten(1)->sortBy('sequenceInRow')->toArray()))
@php ($values = collect(object_to_array($template->format->header->values)))

@for ($i = 1; $i <= (int) $template->format->general->header->rows; $i++)
    <div class="printout-row">
        @foreach($headers as $key => $field)
            @php ($column = $values->where('header', $field->header)->keys()->first())
            @if((int) $field->y == $i && filter_var($field->checked, FILTER_VALIDATE_BOOLEAN))
                <div class="col-xs-{{ $field->width }}">
                    <div class="
                        col-xs-offset-{{ $field->x }} 
                        text-{{ $field->alignment }}
                        {{ $field->border->top ? 'border-top' : ''}}
                        {{ $field->border->left ? 'border-left' : ''}}
                        {{ $field->border->right ? 'border-right' : ''}}
                        {{ $field->border->bottom ? 'border-bottom' : ''}}">
                        <span class="
                            fs-{{ $field->font->size }}
                            font-weight-{{ $field->font->weight }}
                            font-style-{{ $field->font->style }}
                            text-decoration-{{ $field->font->decoration }}">
                            @if(isset($field->label))
                                {{ $field->label }}
                            @elseif(!isset($field->label) && ( (array_key_exists($column, $data['settings'])) ? $data['settings'][$column] : true) )
                                {{ $data['info'][$column] }}
                            @endif
                        </span>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endfor