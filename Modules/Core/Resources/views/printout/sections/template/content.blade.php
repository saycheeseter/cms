@php
    $columns = collect($template->format->content->columns);
@endphp

<div class="printout-row content clearfix">
    <div class="pr-column">
        <table class="pr-datatable" border="{{ $template->format->general->content->border == 1 ? 1 : 0 }}">
            <tr class="
                {{ $template->format->general->content->border == 2 ? 'pr-border-bottom' : '' }}
                {{ $template->format->general->content->border == 3 ? 'pr-border-top' : '' }}
                ">

                {{-- Print only the auto number header if there are any fields checked --}}
                @if($columns->where('checked', true)->count() > 0)
                    <td class="text-center" style="width: 0.5%"></td>
                @endif

                {{-- Print the header of the checked fields --}}
                @foreach($columns->sortBy('sequence') as $key => $field)
                    @if(filter_var($field->checked, FILTER_VALIDATE_BOOLEAN)  && ((array_key_exists($key, $data['settings'])) ? $data['settings'][$key] : true))
                        <td class="
                            text-left
                            font-weight-{{ $field->font->weight }}
                            font-style-{{ $field->font->style }}
                            text-decoration-{{ $field->font->decoration }}"
                            style="width: {{ $field->width }}%">
                            {{ $field->label }}
                        </td>
                    @endif
                @endforeach
            </tr>

            {{-- Print the details according to the set limit row --}}
            @for($x = $GLOBALS['iterator']['details']['index']; $x < count($data['details']); $x++)
                {{--Should only print the item data if there are no serials to be printed--}}
                @if($GLOBALS['iterator']['details']['continue'])
                    <tr class="
                        {{ $template->format->general->content->border == 2 ? 'pr-border-bottom' : '' }}
                        {{ $template->format->general->content->border == 3 ? 'pr-border-top' : '' }}
                        ">

                        {{-- Print the auto number field --}}
                        @if($columns->where('checked', true)->count() > 0)
                            <td class="text-center" style="width: 0.2%">{{ ($x + 1) }}</td>
                        @endif

                        {{-- Print selected fields --}}
                        @foreach($columns->sortBy('sequence') as $key => $field)
                            @if(filter_var($field->checked, FILTER_VALIDATE_BOOLEAN) && ((array_key_exists($key, $data['settings'])) ? $data['settings'][$key] : true))
                                <td class="text-{{ $field->alignment }}" style="max-width: 1px; width: {{ $field->width }}%">
                                    @if(in_array($key, ['discounts', 'unit_code_specs']))
                                        {{
                                            $data['details'][$x][
                                                (
                                                    !property_exists($template->format->content, 'decimals')
                                                    || filter_var($template->format->content->decimals->$key->value, FILTER_VALIDATE_BOOLEAN)
                                                        ? 'decimal'
                                                        : 'whole_number'
                                                ).'_'.$key]
                                        }}
                                    @elseif ($data['details'][$x][$key] instanceof Litipk\BigNumbers\Decimal)
                                        {{
                                            number_format(
                                                $data['details'][$x][$key]->innerValue(),
                                                !property_exists($template->format->content, 'decimals')
                                                || filter_var($template->format->content->decimals->$key->value, FILTER_VALIDATE_BOOLEAN)
                                                    ? setting('monetary.precision')
                                                    : 0
                                            )
                                        }}
                                    @else
                                        {{ $data['details'][$x][$key] }}
                                    @endif
                                </td>
                            @endif
                        @endforeach
                    </tr>

                    @php
                        $GLOBALS['iterator']['count']++;
                        $GLOBALS['iterator']['current']++;
                        $GLOBALS['iterator']['details']['index'] = $x;
                    @endphp
                @endif

                {{--Print Serials Content--}}
                @if(property_exists($template->format->content, 'serials')
                    && filter_var($template->format->content->serials->enabled, FILTER_VALIDATE_BOOLEAN)
                    && array_key_exists('serials', $data['details'][$x])
                    && count($data['details'][$x]['serials']) > 0)

                    @php
                        $GLOBALS['iterator']['details']['continue'] = false;
                    @endphp

                    @if($GLOBALS['iterator']['current'] === (((int) $template->format->general->content->rows) - 1))
                        @php
                            $GLOBALS['iterator']['current'] = 0;
                            break;
                        @endphp
                    @endif

                    <tr class="
                        {{ $template->format->general->content->border == 2 ? 'pr-border-bottom' : '' }}
                        {{ $template->format->general->content->border == 3 ? 'pr-border-top' : '' }}
                    ">
                        <td colspan="{{ (int) $template->format->content->serials->startAt }}"></td>
                        <td colspan="{{ ($columns->where('checked', true)->count() + 1) - ((int) $template->format->content->serials->startAt) }}" class="extra-info">
                            <table class="pr-datatable" border="{{ $template->format->general->content->border == 1 ? 1 : 0 }}">
                                @if($template->format->content->serials->type === 'basic')
                                    @for($q = $GLOBALS['iterator']['serials']['index']; $q < count($data['details'][$x]['serials']); $q++)
                                        @if ($q % (int) $template->format->content->serials->basic->cell === 0)
                                            <tr>
                                        @endif

                                        <td class="text-left">{{ $data['details'][$x]['serials'][$q]['serial_number'] }}</td>

                                        @if (($q + 1) % (int) $template->format->content->serials->basic->cell !== 0 && ($q + 1) !== count($data['details'][$x]['serials']))
                                            @php
                                                continue;
                                            @endphp
                                        @endif

                                        @if (($q + 1) % (int) $template->format->content->serials->basic->cell === 0)
                                            </tr>
                                        @endif

                                        @php
                                            $GLOBALS['iterator']['count']++;
                                            $GLOBALS['iterator']['current']++;
                                            $GLOBALS['iterator']['serials']['index'] = $q;
                                        @endphp

                                        @if($GLOBALS['iterator']['current'] === (((int) $template->format->general->content->rows) - 1))
                                            @php
                                                if (($GLOBALS['iterator']['serials']['index'] + 1) < count($data['details'][$x]['serials'])) {
                                                    $GLOBALS['iterator']['serials']['index']++;
                                                } else {
                                                    $GLOBALS['iterator']['details']['continue'] = true;
                                                    $GLOBALS['iterator']['serials']['index'] = 0;
                                                }
                                                break;
                                            @endphp
                                        @endif
                                    @endfor
                                @endif

                                {{--@if($template->format->content->serials->type === 'advance')--}}
                                    {{--@for($q = $GLOBALS['iterator']['serials']['index']; $q < count($data['details'][$x]['serials']); $q++)--}}
                                        {{--@if ($q % (int) $template->format->content->serials->basic->cell === 0)--}}
                                            {{--<tr>--}}
                                                {{--@foreach($template->format->content->serials->advance as $advance)--}}
                                                    {{--@if(! (bool)$advance->enabled)--}}
                                                        {{--@php--}}
                                                            {{--continue;--}}
                                                        {{--@endphp--}}
                                                    {{--@endif--}}

                                                    {{--<td>{{ @lang('label.'.str_replace('_', '.', $advance->label)) }}</td>--}}
                                                {{--@endforeach--}}
                                            {{--</tr>--}}
                                        {{--@endif--}}

                                        {{--@php--}}
                                            {{--$GLOBALS['iterator']['count']++;--}}
                                            {{--$GLOBALS['iterator']['current']++;--}}
                                            {{--$GLOBALS['iterator']['serials']['index'] = $q;--}}
                                        {{--@endphp--}}

                                        {{--@if($GLOBALS['iterator']['current'] === (((int) $template->format->general->content->rows) - 1))--}}
                                            {{--@php--}}
                                                {{--if (($GLOBALS['iterator']['serials']['index'] + 1) < count($data['details'][$x]['serials'])) {--}}
                                                    {{--$GLOBALS['iterator']['serials']['index']++;--}}
                                                {{--}--}}
                                                {{--break;--}}
                                            {{--@endphp--}}
                                        {{--@endif--}}
                                    {{--@endfor--}}
                                {{--@endif--}}
                            </table>
                        </td>
                    </tr>
                    @if (($GLOBALS['iterator']['serials']['index'] + 1) === count($data['details'][$x]['serials']) && $GLOBALS['iterator']['current'] !== (((int) $template->format->general->content->rows) - 1))
                        @php
                            $GLOBALS['iterator']['details']['continue'] = true;
                            $GLOBALS['iterator']['serials']['index'] = 0;
                        @endphp
                    @endif
                @endif
                {{--If no. rendered rows is equal to the number of content rows--}}
                @php
                    if ($GLOBALS['iterator']['current'] === (((int) $template->format->general->content->rows) - 1)
                        && $GLOBALS['iterator']['count'] !== $content
                    ) {
                        if ($GLOBALS['iterator']['details']['continue']) {
                            $GLOBALS['iterator']['details']['index']++;
                        }

                        $GLOBALS['iterator']['current'] = 0;
                        break;
                    }
                @endphp

                {{-- If final product is already printed and not all the indicated row numbers are consumed, print an empty row --}}
                @if($GLOBALS['iterator']['count'] == $content
                    && $GLOBALS['iterator']['details']['continue']
                    && ($content % (((int) $template->format->general->content->rows) - 1)) > 0)
                    @for($z = $content % (((int) $template->format->general->content->rows) - 1); $z < (((int) $template->format->general->content->rows) - 1); $z++)
                        <tr style="visibility: hidden;"><td>&nbsp;</td></tr>
                    @endfor
                @endif
            @endfor
        </table>
    </div>
</div>

