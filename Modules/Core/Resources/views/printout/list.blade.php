@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.printout.template.list'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            @if($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.printout.template')
                </control-button>
            @endif
            <control-button name="filters" icon="fa fa-search" @click="filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirmation">
        <template slot="header">
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.module')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.module }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.printout.template'),
        'content' => trans('core::confirm.delete.printout.template')
    ])
    @endcomponent

    <filter-modal
        v-show="filter.visible"
        @close="filter.visible = false"
        :filters="filter.options"
        v-model='filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Printout/list.js') }}"></script>
@stop
