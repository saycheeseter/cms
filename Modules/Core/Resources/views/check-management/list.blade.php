@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])

@section('stylesheet')
@stop

@section('container-1')
    <control-bar class="c-content-group">
        <template slot="left">
            <control-button name='filters' icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            @if($permissions['column_settings'])
                <control-button name='column-settings' icon="fa fa-gear" @click="modal.columnSettings.visible = true">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
            @if($permissions['transfer'])
                <control-dropdown icon="fa fa-arrow-circle-right">
                    @lang('core::label.transfer')
                    <template slot="list">
                        <div class="c-content-group c-lr transfer-date">
                            <table class="c-tbl-layout">
                                <tr>
                                    <td>@lang('core::label.date')</td>
                                    <td><datepicker id="transfer-date" class="field__date" format="yyyy-MM-dd" v-model='transfer_date'></datepicker></td>
                                    <td><input type="button" class="c-btn c-dk-blue" value="@lang('core::label.transfer')" @click='modal.confirm.visible = true'></td>
                                </tr>
                            </table>
                        </div>
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-model="datatable.selected"
        v-on:reload="paginate">
        <template slot="header">
            <table-header v-show='modal.columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header v-show='modal.columnSettings.selections.created_from'>@lang('core::label.created.from')</table-header>
            <table-header v-show='modal.columnSettings.selections.bank'>@lang('core::label.bank')</table-header>
            <table-header>@lang('core::label.check.no')</table-header>
            <table-header v-show='modal.columnSettings.selections.check_date'>@lang('core::label.check.date')</table-header>
            <table-header v-show='modal.columnSettings.selections.transfer_date'>@lang('core::label.transfer.date')</table-header>
            <table-header v-show='modal.columnSettings.selections.amount'>@lang('core::label.amount')</table-header>
            <table-header v-show='modal.columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='modal.columnSettings.selections.remarks'>@lang('core::label.check.status')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.transaction_date' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='modal.columnSettings.selections.created_from' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='modal.columnSettings.selections.bank' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.bank }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number" @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.check_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.check_date' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.check_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.transfer_date' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transfer_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='modal.columnSettings.selections.amount' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='modal.columnSettings.selections.remarks' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='modal.columnSettings.selections.remarks' @click='redirect(row.data.type, row.data.transaction_id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'transfer',
        'header' => trans('core::label.transfer.check'),
        'content' => trans('core::confirm.transfer.check')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        v-bind:columns='modal.columnSettings.columns'
        v-show="modal.columnSettings.visible"
        v-model='modal.columnSettings.selections'
        @close="modal.columnSettings.visible = false"
        :module="modal.columnSettings.module">
    </column-select>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('permissions', $permissions)
    @javascript('bankAccounts', $bankAccounts)
    @stack('script')
@stop