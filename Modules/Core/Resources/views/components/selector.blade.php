<modal
    class="selector"
    id="{{ $id ?? 'transaction-selector' }}"
    v-show="{{ $show ?? 'selector.transaction.visible' }}"
    @close="{{ $show ?? 'selector.transaction.visible' }} = false">
    <template slot="header">
        {{ $header ?? trans('core::label.transaction.selector') }}
    </template>
    <template slot="content">
        {{ $message ?? '' }}
        <div class="c-content-group c-t clearfix flex">
            <div class="flex flex-column flex-1 c-has-bl c-has-br datatable overflow-hidden">
                <control-bar>
                    <template slot="left">
                        {{ $controls }}
                    </template>
                </control-bar>
                <datatable
                    tableId="{{ $datatable['id'] ?? 'selector-transaction-table' }}"
                    v-bind:settings="{{ $datatable['settings'] ?? 'selector.transaction.datatable.settings' }}"
                    v-bind:table="{{ $datatable['value'] ?? 'selector.transaction.datatable.value' }}"
                    v-model="{{ $datatable['model'] ?? 'selector.transaction.datatable.selected' }}"
                    v-on:reload="{{ $datatable['reload'] ?? 'getTransactions' }}"
                    ref="{{ $datatable['ref'] ?? 'selector-transaction-table' }}">
                    <template slot="header">
                        {{ $headers }}
                    </template>
                    <template slot="display" scope="row">
                        {{ $display }}
                    </template>
                </datatable>
            </div>
        </div>
    </template>
</modal>

<column-select
    id="{{ $columnSettings['id'] ?? 'selector-column-settings' }}"
    v-show="{{ $columnSettings['visible'] ?? 'selector.transaction.columnSettings.visible' }}"
    v-model="{{ $columnSettings['model'] ?? 'selector.transaction.columnSettings.selections' }}"
    :columns="{{ $columnSettings['columns'] ?? 'selector.transaction.columnSettings.columns'}}"
    :module="{{ $columnSettings['module'] ?? 'selector.transaction.columnSettings.module'}}"
    @close="{{ $columnSettings['visible'] ?? 'selector.transaction.columnSettings.visible' }} = false">
</column-select>