<control-dropdown class="transfer-excel-upload" icon="fa fa-file-excel-o">
    @lang('core::label.import')
    <template slot="list">
        <table class="c-tbl-layout">
            <tr>
                <td colspan="3">
                    <input type="file" class="control-form-upload" ref="excelFile" id="upload-excel-file" @change="onImportExcelChangeFile"/>
                </td>
            </tr>
            <tr>
                <td>
                    <button class="c-btn c-dk-blue" name="import" @click="importExcelFile">
                        @lang('core::label.import')
                    </button>
                </td>
                <td>
                    <button class="c-btn c-dk-green" name="download-import-excel-template" @click="downloadImportExcelTemplate">
                        @lang('core::label.excel.template')
                    </button>
                </td>
                <td>
                    <button class="c-btn c-lt-green" name="view-logs" @click="viewImportExcelLogs">
                        @lang('core::label.view.logs')
                    </button>
                </td>
            </tr>
        </table>
    </template>
</control-dropdown>

@prepend('modals')
    <modal
        class="view-logs"
        v-show='importExcel.logs.visible'
        @close='importExcel.logs.visible = false'
    >
        <template slot="header">
            @lang('core::label.view.logs')
        </template>
        <template slot="content">
            <ul>
                <li v-for="log in importExcel.logs.content">@{{ log }}</li>
            </ul>
        </template>
    </modal>
@endprepend