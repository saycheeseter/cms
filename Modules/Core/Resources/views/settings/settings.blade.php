@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.settings'))

@section('stylesheet')
    <style type="text/css" media="screen">
        .c-sub-sidebar{
            max-width: 25%;
        }
    </style>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('container-1')
    <div class="c-nav-tabs clearfix">
        <div class="c-nav-tab" v-for="(tab, key) in tabs.get()"
            v-text="tab.label"
            :class="{ active: tab.active }"
            @click="tabs.active(key)">
        </div>
    </div>
    <div class="c-nav-tabs-container flex flex-column flex-1 overflow-hidden">
        @include('core::settings.sections.generic')
        @include('core::settings.sections.branch')
        @include('core::settings.sections.user-defined-fields')
    </div>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id'   => 'confirm-delete-udf',
        'callback' => 'deleteUDF',
        'show' => 'confirm.deleteUDF.visible',
        'header' => trans('core::label.delete.udf'),
        'content' => trans('core::confirm.delete.udf')
    ])
    @endcomponent
@stop

@section('script')
    <script src="{{ elixir('js/Settings/settings.js') }}"></script>
@stop