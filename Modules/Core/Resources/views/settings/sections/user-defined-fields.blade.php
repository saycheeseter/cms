<div class="c-nav-tabs-content overflow-hidden c-t" v-show="tabs.udf.active">
    <control-bar class="c-has-bb">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.module'):</td>
                        <td>
                            <chosen
                                name="module"
                                v-model='udf.module'
                                :options='udf.options'
                                @change="changeUDFs">
                            </chosen>
                        </td>
                        <td>
                            @lang('core::info.do.not.add.udf.during.transaction.hours')
                        </td>
                    </tr>
                </table>
            </div>
        </template>
    </control-bar>
    <datatable
        class="c-no-bt"
        :show="udf.module != null"
        :tableid="udf.table.id"
        :settings="udf.table.settings"
        :table="udf.table.value"
        @reload="paginateUDFs"
        @destroy="confirmDeleteUDF"
        @update="saveUDF">
        <template slot="header">
            <table-header>@lang('core::label.label')</table-header>
            <table-header>@lang('core::label.type')</table-header>
            <table-header>@lang('core::label.visible')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <input type="text" class="required" v-model="row.data.label" @keydown="changeState(row.index, 'update')" />
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--type">
                <chosen
                    name='type'
                    v-model='row.data.type'
                    :options='udf.types'
                    @change="changeState(row.index, 'update')">
                </chosen>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <input type="checkbox" v-model="row.data.visible" @click="changeState(row.index, 'update')"/>
            </td>
        </template>
        <template slot="input">
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <input v-model="udf.fields.label" type="text" />
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--type">
                <chosen
                    name='type'
                    v-model='udf.fields.type'
                    :options='udf.types'>
                </chosen>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <input type="checkbox" v-model="udf.fields.visible"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <img src="{{ asset('images/iconupdate.png') }}" @click="saveUDF(udf.fields.data())">
            </table-input>
        </template>
    </datatable>
</div>