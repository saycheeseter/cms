<div class="c-nav-tabs-content overflow-hidden" v-show="tabs.branch.active">
    <control-bar class="c-has-bb">
        <template slot="right">
            <control-button icon="fa fa-save" @click="updateBranchSettings" id="update-branch-settings">
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    <div class="flex flex-1 c-content-group c-tb c-lr overflow-hidden">
        <div class="c-sub-sidebar c-all-b flex-1 overflow" id="branch_section">
            <tree-list v-model="sections.branch"></tree-list>
        </div>
        <div class="c-sub-main-content c-all-b c-mgl-5 flex flex-column flex-1">
            @include('core::settings.sections.branch.purchase-inbound')
            @include('core::settings.sections.branch.payment')
            @include('core::settings.sections.branch.collection')
        </div>
    </div>
</div>