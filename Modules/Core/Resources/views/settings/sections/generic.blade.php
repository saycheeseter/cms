<div class="c-nav-tabs-content overflow-hidden" v-show="tabs.generic.active">
    <control-bar class="c-has-bb">
        <template slot="right">
            <control-button icon="fa fa-save" @click="updateGenericSettings" id="update-generic-settings">
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    <div class="flex flex-1 c-content-group c-tb c-lr overflow-hidden">
        <div class="c-sub-sidebar c-all-b flex-1 overflow" id="generic_section">
            <tree-list v-model="sections.generic"></tree-list>
        </div>
        <div class="c-sub-main-content c-all-b c-mgl-5 flex flex-column flex-1">
            @include('core::settings.sections.generic.general')
            @include('core::settings.sections.generic.product')
            @include('core::settings.sections.generic.sheet-number-prefix')
            @include('core::settings.sections.generic.purchase-and-purchase-inbound')
            @include('core::settings.sections.generic.purchase-inbound')
            @include('core::settings.sections.generic.sales')
            @include('core::settings.sections.generic.sales-outbound')
            @include('core::settings.sections.generic.delivery-modules')
            @include('core::settings.sections.generic.stock-request')
            @include('core::settings.sections.generic.stock-delivery-outbound')
        </div>
    </div>
</div>