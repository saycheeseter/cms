<div class="c-content" v-if="sections.generic[9].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.stock.delivery.outbound')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.warning.inventory.reached.minimum')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model="settings.generic.delivery_outbound_warn_if_inventory_reaches_minimum">
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model="settings.generic.delivery_outbound_warn_if_inventory_reaches_minimum">
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>