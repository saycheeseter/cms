<div class="c-content" v-if="sections.generic[0].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.general')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                <table class="c-tbl-layout">
                    <!-- <tr>
                        <td>@lang('core::label.company.logo'):</td>
                        <td><input type="file"></td>
                    </tr> -->
                    <tr>
                        <td>@lang('core::label.monetary.precision'):</td>
                        <td><input type="text" class="field__qty" v-model="settings.generic.monetary_precision" ></td>
                    </tr>
                </table>
            </div>
            <div class="c-form-group">
                @lang('core::info.barcode.scan.unit.based.on'):
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="1" v-model="settings.generic.barcode_scan_default_unit">
                        @lang('core::label.barcode.unit')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="2" v-model="settings.generic.barcode_scan_default_unit">
                        @lang('core::label.default.unit')
                    </label>
                </div>
            </div>
            <div class="c-form-group">
                @lang('core::info.mode.for.summary.computation'):
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="1" v-model="settings.generic.transaction_summary_computation_mode">
                        @lang('core::label.automatic')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="2" v-model="settings.generic.transaction_summary_computation_mode">
                        @lang('core::label.manual')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>