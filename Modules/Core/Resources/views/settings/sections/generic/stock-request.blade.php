<div class="c-content" v-if="sections.generic[8].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.stock.request')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::note.repeated.requests.warnings')
                <table class="c-tbl-layout">
                    <tr>
                        <td><input type="checkbox" v-model="settings.generic.request_warning_already_requested_delivered_toggle"></td>
                        <td>@lang('core::info.already.requested.and.delivered')</td>
                        <td><number-input class="field__qty" :precision="false" v-model="settings.generic.request_warning_already_requested_delivered_days" :disabled="!settings.generic.request_warning_already_requested_delivered_toggle"> </number-input></td>
                        <td>@lang('core::label.days')</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" v-model="settings.generic.request_warning_already_requested_not_delivered_toggle"></td>
                        <td>@lang('core::info.already.requested.but.not.yet.delivered')</td>
                        <td><number-input class="field__qty" :precision="false" v-model="settings.generic.request_warning_already_requested_not_delivered_days" :disabled="!settings.generic.request_warning_already_requested_not_delivered_toggle"></number-input></td>
                        <td>@lang('core::label.days')</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>