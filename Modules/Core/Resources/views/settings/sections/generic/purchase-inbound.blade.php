<div class="c-content" v-if="sections.generic[4].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.purchase.inbound')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::label.auto.update.cost')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="1" v-model="settings.generic.purchase_inbound_auto_update_cost">
                        @lang('core::label.own.branch')
                    </label>
                    <label class="radio-chkbx">
                        <input type="radio" value="2" v-model="settings.generic.purchase_inbound_auto_update_cost">
                        @lang('core::label.all.branches')
                    </label>
                    <label class="radio-chkbx">
                        <input type="radio" value="3" v-model="settings.generic.purchase_inbound_auto_update_cost">
                        @lang('core::label.copied.to.branch')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="0" v-model="settings.generic.purchase_inbound_auto_update_cost">
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>