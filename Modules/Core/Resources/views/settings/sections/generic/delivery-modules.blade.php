<div class="c-content" v-if="sections.generic[7].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.delivery.modules')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.price.shown.is.based.from')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="1" v-model='settings.generic.delivery_module_price_basis'>
                        @lang('core::label.cost')
                    </label>
                    <label class="radio-chkbx">
                        <input type="radio" value="2" v-model='settings.generic.delivery_module_price_basis'>
                        @lang('core::label.wholesale.price')
                    </label>
                    <label class="radio-chkbx">
                        <input type="radio" value="3" v-model='settings.generic.delivery_module_price_basis'>
                        @lang('core::label.retail.price')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>