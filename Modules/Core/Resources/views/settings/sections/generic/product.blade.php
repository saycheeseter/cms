<div class="c-content" v-if="sections.generic[1].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.product')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::label.view.product.prices.of')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value='0' v-model='settings.generic.product_view_prices' >
                        @lang('core::label.this.branch.only')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value='1' v-model='settings.generic.product_view_prices' >
                        @lang('core::label.all.branches')
                    </label>
                </div>
            </div>
            <div class="c-form-group">
                @lang('core::info.show.pricing.abnormality.warning')
                (@lang('core::info.cost.greater.than.wholesale.retail'))
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model='settings.generic.product_show_price_abnormality_warning'>
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model='settings.generic.product_show_price_abnormality_warning'>
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>