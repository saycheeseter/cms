<div class="c-content" v-if="sections.generic[5].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.sales.modules')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.regular.customer.price.basis')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="0" v-model="settings.generic.soi_regular_customers_price_basis">
                        @lang('core::info.based.on.customer.setting')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="1" v-model="settings.generic.soi_regular_customers_price_basis">
                        @lang('core::label.wholesale')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="2" v-model="settings.generic.soi_regular_customers_price_basis">
                        @lang('core::label.retail')
                    </label>
                </div>
            </div>
            <div class="c-form-group">
                @lang('core::info.walkin.customer.price.basis')
                <div class="c-width-100">
                    <select v-model="settings.generic.soi_walkin_customers_price_basis">
                        <option value="0" >@lang('core::label.wholesale')</option>
                        <option value="1" >@lang('core::label.retail')</option>
                        <option value="2" >@lang('core::label.price.a')</option>
                        <option value="3" >@lang('core::label.price.b')</option>
                        <option value="4" >@lang('core::label.price.c')</option>
                        <option value="5" >@lang('core::label.price.d')</option>
                        <option value="6" >@lang('core::label.price.e')</option>
                    </select>
                </div>
            </div>
            <div class="c-form-group">
                @lang('core::info.unit.shown.is.based.on.history')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model="settings.generic.soi_unit_shown_base_on_history">
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model="settings.generic.soi_unit_shown_base_on_history">
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>