<div class="c-content" v-if="sections.generic[2].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.sheet.number.prefix')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.purchase.order'):</td>
                        <td>
                            <input type="text"
                                   :defaults="settings.defaults.generic.purchase_sheet_number_prefix"
                                   v-model="settings.generic.purchase_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('purchase_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.purchase.inbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.purchase_inbound_sheet_number_prefix"
                                   v-model="settings.generic.purchase_inbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('purchase_inbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.purchase.return'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.purchase_return_sheet_number_prefix"
                                   v-model="settings.generic.purchase_return_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('purchase_return_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.purchase.return.outbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.purchase_return_outbound_sheet_number_prefix"
                                   v-model="settings.generic.purchase_return_outbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('purchase_return_outbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.damage'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.damage_sheet_number_prefix"
                                   v-model="settings.generic.damage_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('damage_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.damage.outbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.damage_outbound_sheet_number_prefix"
                                   v-model="settings.generic.damage_outbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('damage_outbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.sales'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.sales_sheet_number_prefix"
                                   v-model="settings.generic.sales_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('sales_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.sales.outbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.sales_outbound_sheet_number_prefix"
                                   v-model="settings.generic.sales_outbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('sales_outbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.sales.return'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.sales_return_sheet_number_prefix"
                                   v-model="settings.generic.sales_return_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('sales_return_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.sales.return.inbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.sales_return_inbound_sheet_number_prefix"
                                   v-model="settings.generic.sales_return_inbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('sales_return_inbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.request'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_request_sheet_number_prefix"
                                   v-model="settings.generic.stock_request_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_request_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.delivery'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_delivery_sheet_number_prefix"
                                   v-model="settings.generic.stock_delivery_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_delivery_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.delivery.outbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_delivery_outbound_sheet_number_prefix"
                                   v-model="settings.generic.stock_delivery_outbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_delivery_outbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.delivery.inbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_delivery_inbound_sheet_number_prefix"
                                   v-model="settings.generic.stock_delivery_inbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_delivery_inbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.return'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_return_sheet_number_prefix"
                                   v-model="settings.generic.stock_return_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_return_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.return.outbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_return_outbound_sheet_number_prefix"
                                   v-model="settings.generic.stock_return_outbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_return_outbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.return.inbound'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.stock_return_inbound_sheet_number_prefix"
                                   v-model="settings.generic.stock_return_inbound_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('stock_return_inbound_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.payment'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.payment_sheet_number_prefix"
                                   v-model="settings.generic.payment_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('payment_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.collection'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.collection_sheet_number_prefix"
                                   v-model="settings.generic.collection_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('collection_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.counter'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.counter_sheet_number_prefix"
                                   v-model="settings.generic.counter_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('counter_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.inventory.adjust'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.inventory_adjust_sheet_number_prefix"
                                   v-model="settings.generic.inventory_adjust_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('inventory_adjust_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.product.conversion'):</td>
                        <td><input type="text"
                                   :defaults="settings.defaults.generic.product_conversion_sheet_number_prefix"
                                   v-model="settings.generic.product_conversion_sheet_number_prefix"
                                   @blur="forceDefaultSheetNumberPrefix('product_conversion_sheet_number_prefix', $event)"
                            >
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>