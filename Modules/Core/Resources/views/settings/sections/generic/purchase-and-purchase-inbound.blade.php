<div class="c-content" v-if="sections.generic[3].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.purchase.and.purchase.inbound')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.price.shown.is.based.on.history')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model='settings.generic.poi_price_based_on_history'>
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model='settings.generic.poi_price_based_on_history'>
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
            <div class="c-form-group">
                @lang('core::info.unit.shown.is.based.on.history')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model='settings.generic.poi_unit_based_on_history'>
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model='settings.generic.poi_unit_based_on_history'>
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>