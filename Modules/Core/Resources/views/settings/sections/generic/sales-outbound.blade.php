<div class="c-content" v-if="sections.generic[6].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.sales.outbound')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.enable.customer.credit.limit')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="false" v-model="settings.generic.sales_customer_credit_limit">
                        @lang('core::label.no')
                    </label>
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model="settings.generic.sales_customer_credit_limit">
                        @lang('core::label.yes')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>