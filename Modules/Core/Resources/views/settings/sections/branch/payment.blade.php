<div class="c-content" v-if="sections.branch[1].active">
    <div class="c-sub-header c-has-bb">
        @lang('core::label.payment')
    </div>
    <div class="overflow">
        <div class="c-content-group c-tb c-lr">
            <div class="c-form-group">
                @lang('core::info.display.transactions.of.other.branches')
                <div class="c-mgl-5">
                    <label class="radio-chkbx">
                        <input type="radio" value="true" v-model="settings.branch.payment_display_transactions_of_other_branches">
                        @lang('core::label.yes')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" value="false" v-model="settings.branch.payment_display_transactions_of_other_branches">
                        @lang('core::label.no')
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>