@extends('core::layouts.master')
@extends('core::layouts.flexible.three-container')
@section('title', trans('core::label.pos.transaction.detail'))

@section('stylesheet')
    <style type="text/css">
        .container-1 {
            flex: 0 0 auto;
            overflow: initial;
        }
        .container-3 {
            max-height: 80px;
        }
    </style>
@stop

@section('container-1')
    @include('core::pos.sections.detail.info')
@stop

@section('container-2')
    <control-bar id='controls'>
        <template slot="left">
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.details.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    @include('core::pos.sections.detail.details')
@stop

@section('container-3')
    <div class="c-content-group c-tb c-lr clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.qty'):</td>
                    <td><b><span name='total-qty'>@{{ totalQty }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.amount'):</td>
                    <td><b><span name='total-amount'>@{{ info.total_amount }}</span></b></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <column-select
        :columns='columnSettings.details.columns'
        v-model='columnSettings.details.selections'
        @close='columnSettings.details.visible = false'
        v-show='columnSettings.details.visible'
        id='details-column-modal'>
    </column-select>
@stop

@section('script')
    @javascript('id', Request::segment(2))

    <script src="{{ elixir('js/PosSales/detail.js') }}"></script>
@stop