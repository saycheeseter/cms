<div class="c-content-group c-tb c-lr clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.or.si.no'):</td>
                <td><span name="or-si-number"><b>@{{ info.or_number }}</b></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.transaction.number'):</td>
                <td><span name="transaction-number"><b>@{{ info.transaction_number }}</b></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.created.for'):</td>
                <td><b>@{{ info.created_for }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.for.location'):</td>
                <td><b>@{{ info.for_location }}</b></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-10">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.terminal.number'):</td>
                <td><span name="terminal-number"><b>@{{ info.terminal_number }}</b></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.customer.type'):</td>
                <td><b>@{{ info.customer_type_label }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.customer'):</td>
                <td><b>@{{ info.customer }}</b></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-10">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.senior'):</td>
                <td><b>@{{ (info.senior_name == '' ? '-' : info.senior_name) }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.senior.number'):</td>
                <td><b>@{{ (info.senior_number == '' ? '-' : info.senior_number) }}</b></td>
            </tr>
            <tr>
                <td valign="top">@lang('core::label.remarks'):</td>
                <td><b>@{{ (info.remarks == '' ? '-' : info.remarks) }}</b></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-10">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.member'):</td>
                <td><b>@{{ (info.member == '' ? '-' : info.member) }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.cashier'):</td>
                <td><b>@{{ (info.cashier == '' ? '-' : info.cashier) }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.salesman'):</td>
                <td><b>@{{ (info.salesman == '' ? '-' : info.salesman) }}</b></td>
            </tr>
        </table>
    </div>
    <div class="pull-right">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.date.time'):</td>
                <td><b>@{{ info.transaction_date }}</b></td>
            </tr>
            <tr>
                <td>@lang('core::label.voided'):</td>
                <td><b>@{{ typeof info.voided === 'object' ? info.voided.label : '' }}</b></td>
            </tr>
        </table>
    </div>
</div>