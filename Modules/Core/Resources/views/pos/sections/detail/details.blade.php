<datatable
    v-bind:tableId="datatable.details.id"
    v-bind:settings="datatable.details.settings"
    v-bind:table="datatable.details.value">
    <template slot="header">
        <table-header v-show='columnSettings.details.selections.barcode'>@lang('core::label.barcode')</table-header>
        <table-header v-show='columnSettings.details.selections.stockNo'>@lang('core::label.stock.no')</table-header>
        <table-header>@lang('core::label.product.name')</table-header>
        <table-header v-show='columnSettings.details.selections.chineseName'>@lang('core::label.chinese.name')</table-header>
        <table-header>@lang('core::label.qty')</table-header>
        <table-header>@lang('core::label.uom')</table-header>
        <table-header>@lang('core::label.unit.specs')</table-header>
        <table-header>@lang('core::label.total.qty')</table-header>
        <table-header>@lang('core::label.o.price')</table-header>
        <table-header>@lang('core::label.price')</table-header>
        <table-header v-show='columnSettings.details.selections.discount1'>@lang('core::label.discount.1')</table-header>
        <table-header v-show='columnSettings.details.selections.discount2'>@lang('core::label.discount.2')</table-header>
        <table-header v-show='columnSettings.details.selections.discount3'>@lang('core::label.discount.3')</table-header>
        <table-header v-show='columnSettings.details.selections.discount4'>@lang('core::label.discount.4')</table-header>
        <table-header v-show='columnSettings.details.selections.remarks'>@lang('core::label.remarks')</table-header>
        <table-header v-show='columnSettings.details.selections.vat'>@lang('core::label.vat')</table-header>
        <table-header v-show='columnSettings.details.selections.senior'>@lang('core::label.senior')</table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.details.selections.barcode'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.barcode }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.details.selections.stockNo'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.stock_no }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.details.selections.chineseName'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.chinese_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--uom">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.uom }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.unit_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.total_qty }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.oprice }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.price }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount1'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.discount1 }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount2'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.discount2 }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount3'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.discount3 }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--percent" v-show='columnSettings.details.selections.discount4'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.discount4 }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.details.selections.remarks'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remarks }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--number" v-show='columnSettings.details.selections.vat'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.vat }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--number" v-show='columnSettings.details.selections.senior'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.is_senior }}
                </template>
            </table-cell-data>
        </td>
    </template>
</datatable>