@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.pos.transaction.list'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header v-show='columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
            <table-header v-show='columnSettings.selections.for_location'>@lang('core::label.for.location')</table-header>
            <table-header v-show='columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.transaction.number')</table-header>
            <table-header>@lang('core::label.or.si.no')</table-header>
            <table-header v-show='columnSettings.selections.terminal_number'>@lang('core::label.terminal.no')</table-header>
            <table-header v-show='columnSettings.selections.cashier'>@lang('core::label.cashier')</table-header>
            <table-header v-show='columnSettings.selections.salesman'>@lang('core::label.salesman')</table-header>
            <table-header v-show='columnSettings.selections.customer_type'>@lang('core::label.customer.type')</table-header>
            <table-header v-show='columnSettings.selections.customer'>@lang('core::label.customer')</table-header>
            <table-header v-show='columnSettings.selections.member'>@lang('core::label.member')</table-header>
            <table-header v-show='columnSettings.selections.senior_name'>@lang('core::label.senior.name')</table-header>
            <table-header v-show='columnSettings.selections.senior_number'>@lang('core::label.senior.number')</table-header>
            <table-header v-show='columnSettings.selections.total_amount'>@lang('core::label.total.amount')</table-header>
            <table-header v-show='columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='columnSettings.selections.voided'>@lang('core::label.voided')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.created_for' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.for_location' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.transaction_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.or_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number" v-show='columnSettings.selections.terminal_number' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.terminal_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.cashier' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.cashier }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.salesman' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.salesman }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type" v-show='columnSettings.selections.customer_type' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer_type_label }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.customer' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.member' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.member }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.senior_name' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.senior_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" v-show='columnSettings.selections.senior_number' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.senior_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_amount' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.selections.remarks' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='columnSettings.selections.voided' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.voided.label }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        @:search="paginate">
    </filter-modal>

    <column-select
        id='columnSettings.id'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @javascript('branches', $branches)

    <script src="{{ elixir('js/PosSales/list.js') }}"></script>
@stop