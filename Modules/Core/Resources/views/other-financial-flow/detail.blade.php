<modal
    v-bind:id="modal.detail.id"
    v-show="modal.detail.visible"
    v-on:open="modal.detail.visible = true"
    v-on:close="modal.detail.visible = false">
    <template slot="header">
        {{ $detailTitle }}
    </template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="message.detail.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.detail.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.detail.success != ''">
                <span v-text="message.detail.success"></span>
            </message>
            <message type="info" :show="message.detail.info != ''">
                <span v-text="message.detail.info"></span>
            </message>
        </div>
        <div class="c-content-group c-tb clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>{{ $referenceNameLabel }}:</td>
                        <td><input type="text" class="required" name="reference-name" v-model='record.info.reference_name'></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.payment.method'):</td>
                        <td>
                            <chosen class="required" name="payment-method" :options='chosen.payment_methods' v-model='record.info.payment_method'></chosen>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-5">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.transaction.date'):</td>
                        <td>
                            <datepicker id="transaction-date" class="transaction-date" format="yyyy-MM-dd" v-model='record.info.transaction_date'></datepicker>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">@lang('core::label.remarks'):</td>
                        <td><textarea rows="3" name="remarks" v-model='record.info.remarks'></textarea></td>
                    </tr>
                </table>
            </div>
        </div>
        <div  class="c-content-group c-b clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr v-show="record.info.payment_method == method.CHECK || record.info.payment_method == method.BANK_DEPOSIT">
                        <td width="100px">@lang('core::label.bank.account'):</td>
                        <td>
                            <chosen name="bank-account" :options='chosen.bank_accounts' v-model='record.info.bank_account_id'></chosen>
                        </td>
                    </tr>
                    <tr v-show="record.info.payment_method == method.CHECK" >
                        <td>@lang('core::label.check.no'):</td>
                        <td><input class="required" type="text" name="check-no" v-model='record.info.check_number'></td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-5">
                <table class="c-tbl-layout">
                    <tr v-show="record.info.payment_method == method.CHECK">
                        <td width="99px">@lang('core::label.check.date'):</td>
                        <td>
                            <datepicker id="check-date" class="check-date" format="yyyy-MM-dd" v-model='record.info.check_date'></datepicker>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="flex flex-column flex-1 c-has-bl c-has-br overflow-hidden">
            <datatable
                v-bind:tableId="datatable.detail.id"
                v-bind:settings="datatable.detail.settings"
                v-bind:table="datatable.detail.value"
                v-on:destroy="confirmDetail"
                v-on:update="updateDetail"
                ref="detail">
                <template slot="header">
                    <table-header>@lang('core::label.type')</table-header>
                    <table-header>@lang('core::label.amount')</table-header>
                    <table-header></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--type">
                        <chosen
                            class="required"
                            name='payment-type'
                            v-model='row.data.type'
                            :options='chosen.types'
                            :row='row.dataIndex'
                            @change="changeState(row.index, 'update', 'detail')"
                        ></chosen>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input class="required" :unsigned="true" v-model="row.data.amount" @keydown.native="changeState(row.index, 'update', 'detail')" />
                    </td>
                </template>
                <template slot="input">
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--type">
                        <chosen class="required" name="payment-type" :options='chosen.types' v-model='record.detail.type'></chosen>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input class="required" :unsigned="true" name="amount" v-model='record.detail.amount'></number-input>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                        <img src="{{ asset('images/iconupdate.png') }}" @click="createDetail()">
                    </table-input>
                </template>
            </datatable>
        </div>
    </template>
    <template slot="footer">
        <div class="c-content-group clearfix">
            <div class="pull-left">
                <table>
                    <tr>
                        <td>@lang('core::label.total.amount'):</td>
                        <td class="c-pdl-10"><b><span>@{{ totalAmount }}</span></b></td>
                    </tr>
                </table>
            </div>
            <div class="pull-right">
                <button id="save" class="c-btn c-dk-green" @click="validate()">@lang('core::label.save')</button>
            </div>
        </div>
    </template>
</modal>