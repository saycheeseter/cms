@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])

@section('stylesheet')
    <style type="text/css">
        #modal-detail .modal-container {
            width: 570px;
        }
        #modal-detail .modal-body {
            max-height: initial;
            height: 470px;
        }
    </style>
@stop

@section('container-1')
    <control-bar>
        <template slot="left">
            @if ($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="modal.detail.visible = true">
                    {{ $addNewLabel }}
                </control-button>
            @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.list.id"
        v-bind:settings="datatable.list.settings"
        v-bind:table="datatable.list.value"
        v-on:reload="paginate"
        v-on:destroy="confirm">
        <template slot="header">
            <table-header>@lang('core::label.date')</table-header>
            <table-header>{{ $referenceNameLabel }}</table-header>
            <table-header>@lang('core::label.payment.method')</table-header>
            <table-header>@lang('core::label.total.amount')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reference_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--method" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.method }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('modals')

    @stack('modals')

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="paginate">
    </filter-modal>

@stop

@section('script')
    @javascript('permissions', $permissions)
    @javascript('types', $types)
    @javascript('paymentMethods', $paymentMethods)
    @javascript('bankAccounts', $bankAccounts)
    @javascript('id', Request::segment(2))
    @javascript('branches', $branches)

    @stack('script')
@stop