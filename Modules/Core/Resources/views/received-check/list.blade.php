@extends('core::check-management.list')
@section('title', trans('core::label.received.check.list'))

@push('script')
    <script src="{{ elixir('js/ReceivedCheck/list.js') }}"></script>
@endpush