@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.discount.list'))

@section('stylesheet')
    <style type="text/css">
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            @if($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.product.discount')
                </control-button>
            @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            @if($permissions['column_settings'])
                <control-button name='column-settings' icon="fa fa-gear" @click="modal.columnSettings.visible = true">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        @destroy="confirmation">
        <template slot="header">
            <table-header>@lang('core::label.code')</table-header>
            <table-header v-show='modal.columnSettings.selections.name'>@lang('core::label.name')</table-header>
            <table-header v-show='modal.columnSettings.selections.valid_from'>@lang('core::label.valid.from')</table-header>
            <table-header v-show='modal.columnSettings.selections.valid_to'>@lang('core::label.valid.to')</table-header>
            <table-header v-show='modal.columnSettings.selections.target_type'>@lang('core::label.target.type')</table-header>
            <table-header v-show='modal.columnSettings.selections.discount_scheme'>@lang('core::label.discount.scheme')</table-header>
            <table-header v-show='modal.columnSettings.selections.select_type'>@lang('core::label.select.type')</table-header>
            <table-header v-show='modal.columnSettings.selections.status'>@lang('core::label.status')</table-header>
            <table-header v-show='modal.columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='modal.columnSettings.selections.name' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.valid_from' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.valid_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.valid_to' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.valid_to }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type" v-show='modal.columnSettings.selections.target_type' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.target_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--discountType" v-show='modal.columnSettings.selections.discount_scheme' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discount_scheme }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--selectType" v-show='modal.columnSettings.selections.select_type' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.select_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='modal.columnSettings.selections.status' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='modal.columnSettings.selections.created_for' @click='edit(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        v-show="modal.columnSettings.visible"
        @close="modal.columnSettings.visible = false"
        v-bind:columns='modal.columnSettings.columns'
        v-model="modal.columnSettings.selections"
        :module='modal.columnSettings.module'>
    </column-select>

    @component('core::components.confirm', [
        'id' => 'warning',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.warning'),
        'content' => trans('core::confirm.delete.product.discount')
    ])
    @endcomponent
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/ProductDiscount/list.js') }}"></script>
@stop
