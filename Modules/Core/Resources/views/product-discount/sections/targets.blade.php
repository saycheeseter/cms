<control-bar id="controls" v-show="targetVisibility">
    <template slot="left">
        <control-button name="target-selector" icon="fa fa-plus" @click="showTargetSelector()">
            @lang('core::label.target.selector')
        </control-button>
    </template>
</control-bar>
<datatable
        v-bind:tableId="datatable.target.id"
        v-bind:settings="datatable.target.settings"
        v-bind:table="datatable.target.value"
        v-show="targetVisibility"
        v-on:destroy="confirmRemoveTarget">
    <template slot="header">
        <table-header>@lang('core::label.target.type')</table-header>
        <table-header>@lang('core::label.name')</table-header>
        <table-header></table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.target_type_label }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.target_name }}
                </template>
            </table-cell-data>
        </td>
    </template>
</datatable>