<control-bar id="controls" class="c-no-bt" v-show="detailVisibility">
    <template slot="left">
        <control-button name="product-selector" icon="fa fa-plus" @click="showDetailSelector">
            @lang('core::label.selector')
        </control-button>
    </template>
</control-bar>
<datatable
    v-bind:tableId="datatable.detail.id"
    v-bind:settings="datatable.detail.settings"
    v-bind:table="datatable.detail.value"
    v-on:destroy="confirmRemoveDetail">
    <template slot="header">
        <table-header>@lang('core::label.type')</table-header>
        <table-header>@lang('core::label.name')</table-header>
        <table-header>@lang('core::label.discount.type')</table-header>
        <table-header>@lang('core::label.discount.value')</table-header>
        <table-header>@lang('core::label.qty')</table-header>
        <table-header></table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.entity_type_label }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--discountType">
            <chosen :options="references.discountTypes" v-model="row.data.discount_type"></chosen>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
            <number-input class="required" v-model="row.data.discount_value"></number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
            <number-input v-model="row.data.qty" :disabled='info.discount_scheme == references.discountSchemeEnum.SIMPLE'></number-input>
        </td>
    </template>
</datatable>