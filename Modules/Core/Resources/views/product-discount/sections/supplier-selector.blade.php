@component('core::components.selector', [
    'show' => 'selector.visible.supplier',
    'header' => trans('core::label.supplier'),
    'datatable' => [
        'reload' => 'getSelect',
        'ref' => 'supplier-transaction-table'
    ]
])

    @slot('controls')
        <control-button name='add' icon="fa fa-plus" @click="addSelect">
            @lang('core::label.add')
        </control-button>
        <control-button name='add-continue' icon="fa fa-plus" @click="addSelect(true)">
            @lang('core::label.add.and.continue')
        </control-button>
    @endslot

    @slot('headers')
        <table-header>@lang('core::label.code')</table-header>
        <table-header>@lang('core::label.name')</table-header>
        <table-header>@lang('core::label.chinese.name')</table-header>
        <table-header>@lang('core::label.contact')</table-header>
    @endslot

    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click="selectRow(row.data, 'supplier-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.code }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'supplier-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.full_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'supplier-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.chinese_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="selectRow(row.data, 'supplier-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.contact }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent