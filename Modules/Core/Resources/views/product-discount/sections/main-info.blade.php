<div class="c-content-group c-tb c-lr c-has-bt clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.code'):</td>
                <td><input class="required" type="text" v-model="info.code"></td>
            </tr>
            <tr>
                <td>@lang('core::label.name'):</td>
                <td><input class="required" type="text" v-model="info.name"></td>
            </tr>
            <tr>
                <td>@lang('core::label.created.for'):</td>
                <td><chosen :disabled="true" :options="references.branches" v-model="info.created_for"></chosen></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.valid.from'):</td>
                <td><datepicker id="valid-from" format="yyyy-MM-dd" v-model="info.valid_from"></datepicker></td>
            </tr>
            <tr>
                <td>@lang('core::label.valid.to'):</td>
                <td><datepicker id="valid-to" format="yyyy-MM-dd" v-model="info.valid_to"></datepicker></td>
            </tr>
            <tr>
                <td>@lang('core::label.discount.scheme'):</td>
                <td><chosen :options="references.discountSchemes" v-model="info.discount_scheme"></chosen></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.target.type'):</td>
                <td><chosen :options="references.targetTypes" v-model="info.target_type" @change='notifyTarget()'></chosen></td>
            </tr>
            <tr>
                <td>@lang('core::label.select.type'):</td>
                <td><chosen :options="references.selectTypes" v-model="info.select_type" @change='notifyDetail()'></chosen></td>
            </tr>
            <tr>
                <td>
                    <label class="radio-chkbx">
                        <input type="checkbox" name="" v-model="info.status">
                        <span>@lang('core::label.status')</span>
                    </label>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="c-content-group c-b c-lr">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>
                    @lang('core::label.effectivity.day'):
                </td>
                <td>
                    <label class="c-mgl-10">
                        <input type="checkbox" name="" v-model="info.mon" :disabled='info.everyday'>
                        <span>@lang('core::label.monday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.tue" :disabled='info.everyday'>
                        <span>@lang('core::label.tuesday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.wed" :disabled='info.everyday'>
                        <span>@lang('core::label.wednesday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.thur" :disabled='info.everyday'>
                        <span>@lang('core::label.thursday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.fri" :disabled='info.everyday'>
                        <span>@lang('core::label.friday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.sat" :disabled='info.everyday'>
                        <span>@lang('core::label.saturday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.sun" :disabled='info.everyday'>
                        <span>@lang('core::label.sunday')</span>
                    </label>
                    <label class="c-mgl-5">
                        <input type="checkbox" name="" v-model="info.everyday">
                        <span>@lang('core::label.everyday')</span>
                    </label>
                </td>
                <td width="140px" align="right">
                    <input type="button" class="c-btn c-md-green c-width-60" value="@lang('core::label.save')" @click='process'>
                </td>
            </tr>
        </table>
    </div>
</div>