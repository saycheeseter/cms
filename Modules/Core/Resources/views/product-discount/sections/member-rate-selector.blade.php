@component('core::components.selector', [
    'show' => 'selector.visible.memberRate',
    'header' => trans('core::label.member.rate'),
    'datatable' => [
        'reload' => 'getTargets',
        'ref' => 'member-rate-transaction-table'
    ]
])

    @slot('controls')
        <control-button name='add' icon="fa fa-plus" @click="addTargets">
            @lang('core::label.add')
        </control-button>
        <control-button name='add-continue' icon="fa fa-plus" @click="addTargets(true)">
            @lang('core::label.add.and.continue')
        </control-button>
    @endslot

    @slot('headers')
        <table-header>@lang('core::label.name')</table-header>
        <table-header>@lang('core::label.memo')</table-header>
        <table-header>@lang('core::label.active')</table-header>
    @endslot

    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'member-rate-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="selectRow(row.data, 'member-rate-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.memo }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--action" @click="selectRow(row.data, 'member-rate-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.status }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent