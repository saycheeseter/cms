@component('core::components.selector', [
    'show' => 'selector.visible.member',
    'header' => trans('core::label.member'),
    'datatable' => [
        'reload' => 'getTargets',
        'ref' => 'member-transaction-table'
    ]
])

    @slot('controls')
        <control-button name='add' icon="fa fa-plus" @click="addTargets">
            @lang('core::label.add')
        </control-button>
        <control-button name='add-continue' icon="fa fa-plus" @click="addTargets(true)">
            @lang('core::label.add.and.continue')
        </control-button>
    @endslot

    @slot('headers')
        <table-header>@lang('core::label.full.name')</table-header>
        <table-header>@lang('core::label.member.type')</table-header>
        <table-header>@lang('core::label.card.id')</table-header>
        <table-header>@lang('core::label.barcode')</table-header>
        <table-header>@lang('core::label.mobile.no')</table-header>
        <table-header>@lang('core::label.telephone.no')</table-header>
    @endslot

    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.full_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.member_type }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--cardId" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.card_id }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.barcode }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.mobile }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="selectRow(row.data, 'member-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.telephone }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent