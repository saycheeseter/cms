@component('core::components.selector', [
    'show' => 'selector.visible.category',
    'header' => trans('core::label.category'),
    'datatable' => [
        'reload' => 'getSelect',
        'ref' => 'category-transaction-table'
    ]
])

    @slot('controls')
        <control-button name='add' icon="fa fa-plus" @click="addSelect">
            @lang('core::label.add')
        </control-button>
        <control-button name='add-continue' icon="fa fa-plus" @click="addSelect(true)">
            @lang('core::label.add.and.continue')
        </control-button>
    @endslot

    @slot('headers')
        <table-header>@lang('core::label.code')</table-header>
        <table-header>@lang('core::label.name')</table-header>
    @endslot

    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click="selectRow(row.data, 'category-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.code }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'category-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.name }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent