@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.discount.detail'))

@section('stylesheet')
    <style type="text/css">
    </style>
@stop

@section('container-1')
    @include('core::product-discount.sections.main-info')
    @include('core::product-discount.sections.targets')
    @include('core::product-discount.sections.details')
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <dialog-box
        :show='dialog.message !=""'
        v-on:close='redirect'
    >
        <template slot='content'>@{{ dialog.message }}</template>
    </dialog-box>

    @include('core::product-discount.sections.member-rate-selector')
    @include('core::product-discount.sections.customer-selector')
    @include('core::product-discount.sections.member-selector')

    @include('core::modal.product-transaction-selector')
    @include('core::product-discount.sections.brand-selector')
    @include('core::product-discount.sections.category-selector')
    @include('core::product-discount.sections.supplier-selector')

    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.deleteTargets.visible',
        'callback' => 'removeTarget',
        'header' => trans('core::label.remove.target'),
        'content' => trans('core::confirm.remove.target')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'confirm-details',
        'show' => 'confirm.deleteDetails.visible',
        'callback' => 'removeDetail',
        'header' => trans('core::label.remove.detail'),
        'content' => trans('core::confirm.remove.detail')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'target-warning',
        'show' => 'confirm.targetWarning.visible',
        'callback' => 'clearTargets',
        'header' => trans('core::label.warning'),
        'cancel' => 'revertInfo',
        'content' => trans('core::confirm.changing.will.remove.targets')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'detail-warning',
        'show' => 'confirm.detailWarning.visible',
        'callback' => 'clearDetails',
        'header' => trans('core::label.warning'),
        'cancel' => 'revertInfo',
        'content' => trans('core::confirm.changing.will.remove.details')
    ])
    @endcomponent
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('categories', $categories)
    @javascript('suppliers', $suppliers)
    @javascript('brands', $brands)
    @javascript('permissions', $permissions)
    @javascript('id', Request::segment(2))

    @stack('script')

    <script src="{{ elixir('js/ProductDiscount/detail.js') }}"></script>
@stop
