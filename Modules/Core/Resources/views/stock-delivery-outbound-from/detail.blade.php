@extends('core::stock-delivery-outbound.detail')

@section('title', trans('core::label.stock.delivery.outbound.from.other.branches'))

@push('script')
    <script src="{{ elixir('js/StockDeliveryOutboundFrom/detail.js') }}"></script>
@endpush