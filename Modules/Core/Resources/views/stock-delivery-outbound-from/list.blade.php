@extends('core::stock-delivery-outbound.list')
@section('title', trans('core::label.stock.delivery.outbound.from.other.branches'))

@push('script')
    @javascript('permissions', $permissions)
    <script src="{{ elixir('js/StockDeliveryOutboundFrom/list.js') }}"></script>
@endpush
