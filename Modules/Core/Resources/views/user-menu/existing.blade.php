<div class="sidebar-section">
    <div class="c-sub-header c-has-bb">
        <span>@lang('core::label.existing.sidebar.sections')</span>
    </div>
    <div class="sidebar-collection">
        <div class="curtain" v-for="(section, sectionKey) in sections">
            <div class="curtain-header clearfix">
                <div class="pull-left">
                    @{{ section.label }}
                </div>
            </div>
            <div class="curtain-body">
                <ul class="tree">
                    <ul class="accordion-group tree">
                        <li v-for="(module, moduleKey) in section.modules">
                            <div v-if="module.visible" :class="(module.selected) ? 'selected' : '' " class="list" @click="selectModule(sectionKey, moduleKey)">
                            <div class="system-sprite" :class="module.sprite"></div>
                                <span>@{{ module.label }}</span>
                            </div>
                        </li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
</div>