<li>
    <div class="list">
        <div :class="(child.type == 2) ? 'folder-opened' : 'file'"></div>
        <span >@{{ child.label }}</span>
    </div>
</li>
<ul class="accordion-group tree" v-for="(child, childKey) in child.children" class="curtain" >
    <li>
        <div class="list">
            <div></div>
            <div :class="(child.type == 2) ? 'folder-opened' : 'file'"></div>
            <span >@{{ child.label }}</span>
        </div>
    </li>
</ul>