<div class="sidebar-section">
    <div class="c-sub-header clearfix c-has-bb">
        <div class="pull-left">
            <span>@lang('core::label.sidebar.sections')</span>
        </div>
        <div class="pull-right">
            <button class="c-btn c-dk-blue" @click="modal.addSection.visible = true">
                <i class="fa fa-plus"></i>
                @lang('core::label.add.new.sidebar.section')
            </button>
        </div>
    </div>
    <div class="sidebar-collection">
        <ul class="sortable" v-sortable="{onEnd: sortSection}" >
            <li v-for="(section, sectionKey) in menu" :key="section.key" class="curtain" :class="isSectionSelected(section.key)" >
                <div class="curtain-header clearfix" @click="selectSection(section.key, sectionKey)">
                    <div class="pull-left">
                        <i class="fa fa-arrows"></i>
                    </div>
                    <div class="pull-left c-mgl-5">
                        <input type="text" v-on:click.stop.prevent="selectSection" class="section-label-input" v-model="section.label">
                    </div>
                    <div class="pull-right">
                        <button class="c-btn c-dk-green" v-on:click.stop.prevent="selectSection" @click="modal.addFolder.visible = true; customMenuPane.selected.section.index = sectionKey; ">
                            <i class="fa fa-plus"></i>
                            @lang('core::label.add.folder')
                        </button>
                        <button class="c-btn c-dk-red" v-on:click.stop.prevent="selectSection" @click="confirm.section.visible = true; confirm.section.id=sectionKey">
                            <i class="fa fa-close"></i>
                            @lang('core::label.delete.section')
                        </button>
                    </div>
                </div>
                <div class="curtain-body">
                    <ul class="sortable tree"  v-sortable="{onEnd: sortMenu}" v-for="(child, childKey) in section.children" :key="sectionKey">
                        <my-menu :child="child" level=0 :location="section.key +'|'+ child.label" :indices="sectionKey+'|'+childKey"></my-menu>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>