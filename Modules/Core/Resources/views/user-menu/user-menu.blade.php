@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.my.menu'))

@section('stylesheet')
    <style type="text/css">
        .sidebar-collection .move-buttons {
            display: none;
            width: auto;
            padding-top: 4px;
            cursor: pointer;
        }
        .sidebar-collection .list:hover .move-buttons {
            display: inline-block;
        }
    </style>
@stop

@section('container-1')
    <control-bar class="c-content-group c-has-bb">
        <template slot="right">
            <control-button icon="fa fa-save" @click="save">
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    <div id="user-menu" class="c-content-group c-tb c-lr flex flex-1 overflow-hidden">
        @include('core::user-menu.new')
        <div class="transfer-button">
            <div class="c-mgt-5">
                <button class="btn btn-primary" @click="removeModuleFromMenu">
                    <i class="fa fa-angle-double-right fa-lg"></i>
                </button>
            </div>
            <div class="c-mgt-5">
                <button class="btn btn-primary" @click="addModuleOnMenu()">
                    <i class="fa fa-angle-double-left fa-lg"></i>
                </button>
            </div>
        </div>
        @include('core::user-menu.existing')
    </div>
@stop

@section('modals')
    <modal v-show="modal.addSection.visible" @close="modal.addSection.visible = false">
        <template slot="header">@lang('core::label.add.new.sidebar.section')</template>
        <template slot="content">
            <div class="c-content-group c-tb c-lr">
                <message type="danger" :show="message.modal.error.any()" @close="message.reset()">
                    <ul>
                        <li v-for="(value, key) in message.modal.error.get()" v-text="value"></li>
                    </ul>
                </message>
                <message type="success" :show="message.modal.success != ''">
                    <span v-text="message.modal.success"></span>
                </message>
                <message type="info" :show="message.modal.info != ''">
                    <span v-text="message.modal.info"></span>
                </message>
            </div>
            <div align="center">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.name'):</td>
                        <td width="220px"><input type="text" v-model="customMenuPane.addMenu.value.label" maxlength="50"></td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-green" @click="addSectionOnMenu">@lang('core::label.add')</button>
        </template>
    </modal>

    <modal v-show="modal.addFolder.visible" @close="modal.addFolder.visible = false">
        <template slot="header">@lang('core::label.add.new.folder')</template>
        <template slot="content">
            <div class="c-content-group c-tb c-lr">
                <message type="danger" :show="message.modal.error.any()" @close="message.reset()">
                    <ul>
                        <li v-for="(value, key) in message.modal.error.get()" v-text="value"></li>
                    </ul>
                </message>
                <message type="success" :show="message.modal.success != ''">
                    <span v-text="message.modal.success"></span>
                </message>
                <message type="info" :show="message.modal.info != ''">
                    <span v-text="message.modal.info"></span>
                </message>
            </div>
            <div align="center">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.name'):</td>
                        <td width="220px"><input type="text" v-model="customMenuPane.addMenu.value.label"></td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-green" @click="addFolderOnMenu">@lang('core::label.add')</button>
        </template>
    </modal>

    @component('core::components.confirm', [
        'id' => 'confirm-section',
        'show' => 'confirm.section.visible',
        'callback' => 'removeSectionFromMenu',
        'header' => trans('core::label.delete.custom.menu.section'),
        'content' => trans('core::confirm.delete.custom.menu.section')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'confirm-folder',
        'show' => 'confirm.folder.visible',
        'callback' => 'removeFolderFromMenu',
        'header' => trans('core::label.delete.custom.menu.folder'),
        'content' => trans('core::confirm.delete.custom.menu.folder')
    ])
    @endcomponent

@stop

@section('message')
    <message type="danger" :show="message.head.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.head.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.head.success != ''">
        <span v-text="message.head.success"></span>
    </message>
    <message type="info" :show="message.head.info != ''">
        <span v-text="message.head.info"></span>
    </message>
@stop

@section('script')
    @javascript('menu', $menu)
    @javascript('customReports', $reports)

    <script src="{{ elixir('js/UserMenu/user-menu.js') }}"></script>
@stop