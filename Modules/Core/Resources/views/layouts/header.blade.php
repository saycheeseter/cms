<header id="app-header">
    <div class="pull-left c-version">
        <a href="{{ $loggedIn ? set_url('changelogs') : 'javascript:void();' }}">
            <span class="label label-danger">
                @lang('core::label.version') {{ config('app.version') }}
            </span>
        </a>
        @if ($loggedIn)
            {{ Auth::branch()->presenter()->fullName }}
        @endif
    </div>
    <div class="c-user-credentials">
        @if ($loggedIn)
            <div class="pull-right c-user-btn">
                <a href="{{ action('Modules\Core\Http\Controllers\LoginController@logout') }}">
                    <button name="logout" class="btn c-logout">
                        <i class="fa fa-power-off"></i>
                        @lang('core::label.logout')
                    </button>
                </a>
            </div>
            <div class="pull-right c-user-detail c-user-menu clearfix c-br">
                <div class="welcome">@lang('core::label.welcome'):</div>
                <control-dropdown>
                    {{ Auth::user()->username }}
                    <template slot="list">
                        <li>
                            <a href="javascript:void(0)" @click="modal.changePassword.visible = true">
                                <i class="fa fa-lock"></i>
                                @lang('core::label.change.password')
                            </a> 
                        </li>
                        <li>
                            <a href="{{ set_url('user/my-menu') }}">
                                <i class="fa fa-list-alt"></i>
                                @lang('core::label.my.menu')
                            </a> 
                        </li>
                    </template>
                </control-dropdown>
            </div>
            <div class="pull-right c-user-detail c-language c-bl c-br">
                @lang('core::label.language')
                <span v-for="locale in locales" 
                    :class="{ 'c-active': locale.value === '{{ config('app.locale') }}' }" 
                    @click="setLanguage(locale.value)">
                    @{{ locale.label }}
                </span>
            </div>
        @endif
        <div class="pull-right c-header-links">
            @if ($loggedIn)
                <a href="{{ set_url('dashboard') }}">
                    <button class="btn">
                        <i class="fa fa-home"></i> 
                        @lang('core::label.dashboard')
                    </button>
                </a>
                {{-- <a href="tutoriallist.php">
                    <button class="btn">
                        <i class="fa fa-book"></i> 
                        @lang('core::label.system.tutorial')
                    </button>
                </a> --}}
                <a href="{{ set_url('settings') }}">
                    <button class="btn">
                        <i class="fa fa-gears"></i>
                        @lang('core::label.settings')
                    </button>
                </a>
            @endif
            {{-- <a href="javascript:void(0)" @click="openHelpDesk">
                <button class="btn">
                    <i class="fa fa-phone"></i>
                    @lang('core::label.help.desk')
                </button>
            </a> --}}
        </div>
    </div>
</header>
