<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="{{ asset('images/nelsoft_favicon.ico') }}">
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/core.css') }}">
        @yield('stylesheet')
    </head>
    <body>
        <div id="app" @unless ($loggedIn) class="c-login-body" @endunless>
            @include('core::common.loading')
            @include('core::layouts.header')

            <div class="page">
                @if ($loggedIn)
                    @include('core::layouts.sidebar')
                @endif
                @yield('content')
                @yield('footer')
            </div>

            <div class="modal-section">
                @yield('modals')

                @if ($loggedIn)
                    @include('core::modal.change-password')
                @endif

                @include('core::modal.help-desk')
            </div>
        </div>

        @include('core::layouts.scripts')

        <script type="text/javascript" src="{{ elixir('js/core.js') }}"></script>

        @yield('script')
    </body>
</html>
