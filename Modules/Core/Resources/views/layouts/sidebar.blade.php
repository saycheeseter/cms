<div class="sidebar-wrapper" v-show="sidebar.visible">
    <div id='tabs' class="c-nav-tabs clearfix">
        <div class="c-nav-tab" v-for="(tab, key) in sidebar.tabs.get()" 
            v-text="tab.label" 
            :class="{ active: tab.active }" 
            @click="sidebar.tabs.active(key)">
        </div>
    </div>
    <sidebar v-show="sidebar.tabs.mainMenu.active">
        @include('core::layouts.main-menu.data')
        @include('core::layouts.main-menu.purchase')
        @include('core::layouts.main-menu.sales')
        @include('core::layouts.main-menu.stock-transfer')
        @include('core::layouts.main-menu.others')
        @include('core::layouts.main-menu.data-collector')
        @include('core::layouts.main-menu.reports')
        @include('core::layouts.main-menu.custom-reports')
    </sidebar>
    <div class="custom-menu" v-show="sidebar.tabs.customMenu.active">
        <sidebar>
             <accordion  v-for="(section, sectionKey) in custom_menu" :key="sectionKey" :section="section.label">
                @{{ section.label }}
                <template slot="content">
                    <my-menu v-for="(child, childKey) in section.children"
                        :renderonmenu=true
                        :child="child"
                        level=0
                        :key="childKey"
                        :location="sectionKey +'|'+ childKey">
                    </my-menu>
                </template>
            </accordion>
        </sidebar>
    </div>
</div>
<div :class="sidebar.toggle" class="sidebar-icon" @click="toggleSidebar"></div>
