@javascript('Laravel', [
    'csrfToken' => csrf_token(),
    'auth' => [
        'branch' => $loggedIn ? Auth::branch()->id : null,
        'user' => $loggedIn && Auth::user()->type !== \Modules\Core\Enums\UserType::SUPERADMIN
            ? Auth::user()->id
            : null
    ],
    'settings' => $loggedIn ? settings('_') : [],
    'locale' => config('app.locale'),
    'config' => [
        'custom' => config('custom')
    ]
])

@javascript('customMenu', $customMenu)