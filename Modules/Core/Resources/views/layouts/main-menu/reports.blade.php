<accordion section="reports">
    @lang('core::label.reports')
    <template slot="content">
        @if(Auth::user()->hasAccessTo('view_customer_product_price'))
            <accordion-link active="{{ set_active_path('reports/customer/product-price') }}" href="{{ set_url('reports/customer/product-price') }}" sprite="report">@lang('core::label.customer.product.price')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_customer_balance'))
            <accordion-link active="{{ set_active_path('reports/customer/balance') }}" href="{{ set_url('reports/customer/balance') }}" sprite="report">@lang('core::label.customer.balance')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_supplier_product_price'))
            <accordion-link active="{{ set_active_path('reports/supplier/product-price') }}" href="{{ set_url('reports/supplier/product-price') }}" sprite="report">@lang('core::label.supplier.product.price')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_supplier_balance'))
            <accordion-link active="{{ set_active_path('reports/supplier/balance') }}" href="{{ set_url('reports/supplier/balance') }}" sprite="report">@lang('core::label.supplier.balance')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_audit_trail'))
            <accordion-link active="{{ set_active_path('reports/audit-trail') }}" href="{{ set_url('reports/audit-trail') }}" sprite="audittrail">@lang('core::label.audit.trail')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_inventory_value'))
            <accordion-link active="{{ set_active_path('reports/product/inventory-value') }}" href="{{ set_url('reports/product/inventory-value') }}" sprite="report">@lang('core::label.product.inventory.value')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_inventory_warning'))
            <accordion-link active="{{ set_active_path('reports/product/inventory-warning') }}" href="{{ set_url('reports/product/inventory-warning') }}" sprite="report">@lang('core::label.product.inventory.warning')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_supplier_monthly_sales_ranking'))
            <accordion-link active="{{ set_active_path('reports/supplier/monthly-sales-ranking') }}" href="{{ set_url('reports/supplier/monthly-sales-ranking') }}" sprite="report">@lang('core::label.supplier.monthly.sales.ranking')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_category_monthly_sales_ranking'))
            <accordion-link active="{{ set_active_path('reports/category/monthly-sales-ranking') }}" href="{{ set_url('reports/category/monthly-sales-ranking') }}" sprite="report">@lang('core::label.category.monthly.sales.ranking')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_brand_monthly_sales_ranking'))
            <accordion-link active="{{ set_active_path('reports/brand/monthly-sales-ranking') }}" href="{{ set_url('reports/brand/monthly-sales-ranking') }}" sprite="report">@lang('core::label.brand.monthly.sales.ranking')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_monthly_sales_ranking'))
            <accordion-link active="{{ set_active_path('reports/product/monthly-sales-ranking') }}" href="{{ set_url('reports/product/monthly-sales-ranking') }}" sprite="report">@lang('core::label.product.monthly.sales.ranking')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_serial_transaction_report'))
            <accordion-link active="{{ set_active_path('reports/product/serial-transaction') }}" href="{{ set_url('reports/product/serial-transaction') }}" sprite="report">@lang('core::label.product.serial.transaction.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_periodic_sales'))
            <accordion-link active="{{ set_active_path('reports/product/periodic-sales') }}" href="{{ set_url('reports/product/periodic-sales') }}" sprite="report">@lang('core::label.product.periodic.sales.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_cash_transaction'))
            <accordion-link active="{{ set_active_path('reports/cash-transaction') }}" href="{{ set_url('reports/cash-transaction') }}" sprite="report">@lang('core::label.cash.transaction')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_branch_inventory'))
            <accordion-link active="{{ set_active_path('reports/product/branch-inventory') }}" href="{{ set_url('reports/product/branch-inventory') }}" sprite="report">@lang('core::label.product.branch.inventory.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_salesman_report'))
            <accordion-link active="{{ set_active_path('reports/salesman/report') }}" href="{{ set_url('reports/salesman/report') }}" sprite="report">@lang('core::label.salesman.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_bank_transaction_report'))
            <accordion-link active="{{ set_active_path('reports/bank-transaction') }}" href="{{ set_url('reports/bank-transaction') }}" sprite="report">@lang('core::label.bank.transaction.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_income_statement'))
            <accordion-link active="{{ set_active_path('reports/income-statement') }}" href="{{ set_url('reports/income-statement') }}" sprite="report">@lang('core::label.income.statement')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_pos_sales_report'))
            <accordion-link active="{{ set_active_path('reports/pos-sales') }}" href="{{ set_url('reports/pos-sales') }}" sprite="report">@lang('core::label.pos.sales.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_product_transaction_list'))
            <accordion-link active="{{ set_active_path('reports/product/transaction-list') }}" href="{{ set_url('reports/product/transaction-list') }}" sprite="report">@lang('core::label.product.transaction.list.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_negative_profit_product'))
            <accordion-link active="{{ set_active_path('reports/product/negative-profit') }}" href="{{ set_url('reports/product/negative-profit') }}" sprite="report">@lang('core::label.negative.profit.product.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_member_report'))
            <accordion-link active="{{ set_active_path('reports/member/report') }}" href="{{ set_url('reports/member/report') }}" sprite="report">@lang('core::label.member.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_payment_due_warning'))
            <accordion-link active="{{ set_active_path('reports/payment/due-warning') }}" href="{{ set_url('reports/payment/due-warning') }}" sprite="report">@lang('core::label.payment.due.warning')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_collection_due_warning'))
            <accordion-link active="{{ set_active_path('reports/collection/due-warning') }}" href="{{ set_url('reports/collection/due-warning') }}" sprite="report">@lang('core::label.collection.due.warning')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_pos_discounted_summary'))
            <accordion-link active="{{ set_active_path('reports/pos/discounted-summary') }}" href="{{ set_url('reports/pos/discounted-summary') }}" sprite="report">@lang('core::label.discount.summary.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_senior_transaction'))
            <accordion-link active="{{ set_active_path('reports/senior/transaction-report') }}" href="{{ set_url('reports/senior/transaction-report') }}" sprite="report">@lang('core::label.senior.transaction.report')</accordion-link>
        @endif

        @if(Auth::user()->hasAccessTo('view_detail_sales_report'))
            <accordion-link active="{{ set_active_path('reports/sales/detail-report') }}" href="{{ set_url('reports/sales/detail-report') }}" sprite="report">@lang('core::label.detail.sales.report')</accordion-link>
        @endif
    </template>
</accordion>