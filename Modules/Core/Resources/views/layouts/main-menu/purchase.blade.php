<accordion section="purchase">
    @lang('core::label.poi')
    <template slot="content">
        @if(Auth::user()->can('view', \Modules\Core\Entities\Purchase::class))
            <accordion-link active="{{ set_active_path('purchase-order') }}" href="{{ set_url('purchase-order') }}" sprite="purchase">@lang('core::label.purchase.order')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\PurchaseInbound::class))
            <accordion-link active="{{ set_active_path('purchase-inbound') }}" href="{{ set_url('purchase-inbound') }}" sprite="purchaseinbound">@lang('core::label.purchase.inbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\PurchaseReturn::class))
            <accordion-link active="{{ set_active_path('purchase-return') }}" href="{{ set_url('purchase-return') }}" sprite="purchasereturn">@lang('core::label.purchase.return')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\PurchaseReturnOutbound::class))
            <accordion-link active="{{ set_active_path('purchase-return-outbound') }}" href="{{ set_url('purchase-return-outbound') }}" sprite="purchasereturnoutbound">@lang('core::label.purchase.return.outbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Payment::class))
            <accordion-link active="{{ set_active_path('payment') }}" href="{{ set_url('payment') }}" sprite="payment">@lang('core::label.payment')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\OtherPayment::class))
            <accordion-link active="{{ set_active_path('other-payment') }}" href="{{ set_url('other-payment') }}" sprite="otherpayment">@lang('core::label.other.payment')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\IssuedCheck::class))
            <accordion-link active="{{ set_active_path('issued-check') }}" href="{{ set_url('issued-check') }}" sprite="issuedcheck">@lang('core::label.issued.check')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Damage::class))
            <accordion-link active="{{ set_active_path('damage') }}" href="{{ set_url('damage') }}" sprite="damage">@lang('core::label.damage')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\DamageOutbound::class))
            <accordion-link active="{{ set_active_path('damage-outbound') }}" href="{{ set_url('damage-outbound') }}" sprite="damageoutbound">@lang('core::label.damage.outbound')</accordion-link>
        @endif
    </template>
</accordion>