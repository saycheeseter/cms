<accordion section="others">
    @lang('core::label.others')
    <template slot="content">
        @if(Auth::user()->can('view', \Modules\Core\Entities\InventoryAdjust::class))
            <accordion-link active="{{ set_active_path('inventory-adjust') }}" href="{{ set_url('inventory-adjust') }}" sprite="inventoryadjust">@lang('core::label.inventory.adjust')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\GiftCheck::class))
            <accordion-link active="{{ set_active_path('gift-check') }}" href="{{ set_url('gift-check') }}" sprite="giftcheck">@lang('core::label.gift.check')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\PrintoutTemplate::class))
            <accordion-link active="{{ set_active_path('printout-template') }}" href="{{ set_url('printout-template') }}" sprite="printouttemplate">@lang('core::label.printout.template')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\ExcelTemplate::class))
            <accordion-link active="{{ set_active_path('excel-template') }}" href="{{ set_url('excel-template') }}" sprite="exceltemplate">@lang('core::label.excel.template')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\ProductConversion::class))
            <accordion-link active="{{ set_active_path('product-conversion') }}" href="{{ set_url('product-conversion') }}" sprite="productconversion">@lang('core::label.product.conversion')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\ReportBuilder::class))
            <accordion-link active="{{ set_active_path('reports/builder') }}" href="{{ set_url('reports/builder') }}" sprite="customreportbuilder">@lang('core::label.custom.report.builder')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\ProductDiscount::class))
            <accordion-link active="{{ set_active_path('product-discount') }}" href="{{ set_url('product-discount') }}" sprite="productdiscount">@lang('core::label.product.discount')</accordion-link>
        @endif

        <accordion-link active="{{ set_active_path('pos-summary-posting') }}" href="{{ set_url('pos-summary-posting') }}" sprite="report">@lang('core::label.pos.summary.posting')</accordion-link>

        @if(setting('transaction.summary.computation.mode') === \Modules\Core\Enums\TransactionSummaryComputation::MANUAL)
            <accordion-link active="{{ set_active_path('transaction-summary-posting') }}" href="{{ set_url('transaction-summary-posting') }}" sprite="report">@lang('core::label.transaction.summary.posting')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\BankTransaction::class))
            <accordion-link active="{{ set_active_path('bank-transaction') }}" href="{{ set_url('bank-transaction') }}" sprite="report">@lang('core::label.bank.transaction')</accordion-link>
        @endif
    </template>
</accordion>