<accordion section="datacollector">
    @lang('core::label.data.collector')
    <template slot="content">
        <accordion-link active="{{ set_active_path('data-collector') }}" href="{{ set_url('data-collector') }}" sprite="report">@lang('core::label.data.collector.transaction.list')</accordion-link>
    </template>
</accordion>