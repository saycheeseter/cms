<accordion section="data">
    @lang('core::label.data')
    <template slot="content">
        @if(Auth::user()->can('view', \Modules\Core\Entities\Product::class))
            <accordion-link active="{{ set_active_path('product') }}" href="{{ set_url('product') }}" sprite="product">@lang('core::label.product')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Branch::class))
            <accordion-link active="{{ set_active_path('brand') }}" href="{{ set_url('brand') }}" sprite="brand">@lang('core::label.brand')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Branch::class))
            <accordion-link active="{{ set_active_path('branch') }}" href="{{ set_url('branch') }}" sprite="branch">@lang('core::label.branch')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Category::class))
            <accordion-link active="{{ set_active_path('category') }}" href="{{ set_url('category') }}" sprite="category">@lang('core::label.category')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Supplier::class))
            <accordion-link active="{{ set_active_path('supplier') }}" href="{{ set_url('supplier') }}" sprite="supplier">@lang('core::label.supplier')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Customer::class))
            <accordion-link active="{{ set_active_path('customer') }}" href="{{ set_url('customer') }}" sprite="customer">@lang('core::label.customer')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Member::class))
            <accordion-link active="{{ set_active_path('member') }}" href="{{ set_url('member') }}" sprite="member">@lang('core::label.member')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\MemberRate::class))
            <accordion-link active="{{ set_active_path('member-rate') }}" href="{{ set_url('member-rate') }}" sprite="memberrate">@lang('core::label.member.rate')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\User::class))
            <accordion-link active="{{ set_active_path('user') }}" href="{{ set_url('user') }}" sprite="user">@lang('core::label.user')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Role::class))
            <accordion-link active="{{ set_active_path('role') }}" href="{{ set_url('role') }}" sprite="role">@lang('core::label.role')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Bank::class))
            <accordion-link active="{{ set_active_path('bank') }}" href="{{ set_url('bank') }}" sprite="bank">@lang('core::label.bank')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\BankAccount::class))
            <accordion-link active="{{ set_active_path('bank-account') }}" href="{{ set_url('bank-account') }}" sprite="bankaccount">@lang('core::label.bank.account')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\UOM::class))
            <accordion-link active="{{ set_active_path('uom') }}" href="{{ set_url('uom') }}" sprite="uom">@lang('core::label.uom')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Reason::class))
            <accordion-link active="{{ set_active_path('reason') }}" href="{{ set_url('reason') }}" sprite="reason">@lang('core::label.reason')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\PaymentType::class))
            <accordion-link active="{{ set_active_path('payment-type') }}" href="{{ set_url('payment-type') }}" sprite="paymenttype">@lang('core::label.payment.type.label')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\IncomeType::class))
            <accordion-link active="{{ set_active_path('income-type') }}" href="{{ set_url('income-type') }}" sprite="incometype">@lang('core::label.income.type')</accordion-link>
        @endif
    </template>
</accordion>