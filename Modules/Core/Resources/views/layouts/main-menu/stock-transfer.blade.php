<accordion section="stocktransferring">
    @lang('core::label.stock.transferring')
    <template slot="content">
        @if(Auth::user()->can('view', \Modules\Core\Entities\StockRequest::class))
            <accordion-link active="{{ set_active_path('stock-request-to') }}" href="{{ set_url('stock-request-to') }}" sprite="stockrequestto">@lang('core::label.stock.requests.to.other.branches')</accordion-link>
        @endif

        @if(Auth::user()->can('viewFrom', \Modules\Core\Entities\StockRequest::class))
            <accordion-link active="{{ set_active_path('stock-request-from') }}" href="{{ set_url('stock-request-from') }}" sprite="stockrequestfrom">@lang('core::label.stock.requests.from.other.branches')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockDelivery::class))
            <accordion-link active="{{ set_active_path('stock-delivery') }}" href="{{ set_url('stock-delivery') }}" sprite="stockdelivery">@lang('core::label.stock.delivery')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockDeliveryOutbound::class))
            <accordion-link active="{{ set_active_path('stock-delivery-outbound-to') }}" href="{{ set_url('stock-delivery-outbound-to') }}" sprite="stockdeliveryoutbound">@lang('core::label.stock.delivery.outbound')</accordion-link>
        @endif

        @if(Auth::user()->can('viewFrom', \Modules\Core\Entities\StockDeliveryOutbound::class))
            <accordion-link active="{{ set_active_path('stock-delivery-outbound-from') }}" href="{{ set_url('stock-delivery-outbound-from') }}" sprite="stockdeliveryoutboundfrom">@lang('core::label.stock.delivery.outbound.from.other.branches')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockDeliveryInbound::class))
            <accordion-link active="{{ set_active_path('stock-delivery-inbound') }}" href="{{ set_url('stock-delivery-inbound') }}" sprite="stockdeliveryinbound">@lang('core::label.stock.delivery.inbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockReturn::class))
            <accordion-link active="{{ set_active_path('stock-return') }}" href="{{ set_url('stock-return') }}" sprite="stockreturn">@lang('core::label.stock.return')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockReturnOutbound::class))
            <accordion-link active="{{ set_active_path('stock-return-outbound') }}" href="{{ set_url('stock-return-outbound') }}" sprite="stockreturnoutbound">@lang('core::label.stock.return.outbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\StockReturnInbound::class))
            <accordion-link active="{{ set_active_path('stock-return-inbound') }}" href="{{ set_url('stock-return-inbound') }}" sprite="stockreturninbound">@lang('core::label.stock.return.inbound')</accordion-link>
        @endif
    </template>
</accordion>