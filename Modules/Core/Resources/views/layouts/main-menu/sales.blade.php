<accordion section="sales">
    @lang('core::label.soi')
    <template slot="content">
        @if(Auth::user()->can('view', \Modules\Core\Entities\Sales::class))
            <accordion-link active="{{ set_active_path('sales') }}" href="{{ set_url('sales') }}" sprite="salesoutbound">@lang('core::label.sales')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\SalesOutbound::class))
            <accordion-link active="{{ set_active_path('sales-outbound') }}" href="{{ set_url('sales-outbound') }}" sprite="salesoutbound">@lang('core::label.sales.outbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\SalesReturn::class))
            <accordion-link active="{{ set_active_path('sales-return') }}" href="{{ set_url('sales-return') }}" sprite="salesreturn">@lang('core::label.sales.return')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\SalesReturnInbound::class))
            <accordion-link active="{{ set_active_path('sales-return-inbound') }}" href="{{ set_url('sales-return-inbound') }}" sprite="salesreturninbound">@lang('core::label.sales.return.inbound')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Counter::class))
            <accordion-link active="{{ set_active_path('counter') }}" href="{{ set_url('counter') }}" sprite="counter">@lang('core::label.counter')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\Collection::class))
            <accordion-link active="{{ set_active_path('collection') }}" href="{{ set_url('collection') }}" sprite="collection">@lang('core::label.collection')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\OtherIncome::class))
            <accordion-link active="{{ set_active_path('other-income') }}" href="{{ set_url('other-income') }}" sprite="otherpayment">@lang('core::label.other.income')</accordion-link>
        @endif

        @if(Auth::user()->can('view', \Modules\Core\Entities\ReceivedCheck::class))
            <accordion-link active="{{ set_active_path('received-check') }}" href="{{ set_url('received-check') }}" sprite="receivedcheck">@lang('core::label.received.check')</accordion-link>
        @endif

        <!-- <accordion-link active="{{ set_active_path('pos-cash-collect') }}" href="{{ set_url('pos-cash-collect') }}" sprite="poscashcollect">@lang('core::label.pos.cash.collect')</accordion-link> -->
    </template>
</accordion>