<accordion section="customreports">
    @lang('core::label.custom.report')
    <template slot="content">
        @foreach($reports as $index => $report)
            <accordion-group module='{{ key($reports[$index])}}'>
                <template slot="child">
                    <ul class="tree">
                        @foreach($report[key($reports[$index])] as $custom)
                            @if(Auth::user()->hasAccessTo(sprintf('view_%s', str_replace('-', '_', $custom['slug']))))
                                <accordion-link active="{{ set_active_path('reports/custom/'.$custom['slug']) }}" href="{{ set_url('reports/custom/'.$custom['slug']) }}" sprite="report">{{ $custom['name'] }}</accordion-link>
                            @endif
                        @endforeach
                    </ul>
                </template>
            </accordion-group>
        @endforeach
    </template>
</accordion>