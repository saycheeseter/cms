@section('content')
    <div class="template-container {{ isset($page_type) ? $page_type : '' }}">
        <div class="single-container container-1">
            <div class="c-sub-header">
                @yield('title')
            </div>
            @yield('container-1')
        </div>
        <div class="message">
            @yield('message')
        </div>
    </div>
    @yield('loading')
@stop