@section('content')
    <div class="template-container">
        <div class="single-container container-1 flex-column">
            <div class="c-sub-header c-has-bb">
                @yield('title')
            </div>
            @yield('container-1')
        </div>
        <div class="single-container container-2 flex-column">
            @yield('container-2')
        </div>
        <div class="message">
            @yield('message')
        </div>
    </div>
    @yield('loading')
@stop