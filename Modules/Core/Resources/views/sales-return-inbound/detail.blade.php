@extends('core::inventory-chain.detail')
@section('title', trans('core::label.sales.return.inbound.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>
                    <label class="radio-chkbx">
                        <input type="radio" name="customer-type" v-model="info.customer_type" @change='notify("customer-type")' :value="regularCustomer" :disabled='disabled'>
                        @lang('core::label.regular.customer')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" name="customer-type" v-model="info.customer_type" @change='notify("customer-type")' :value="walkInCustomer" :disabled='disabled'>
                        @lang('core::label.walk.in.customer')
                    </label>
                </td>
            </tr>
        </table>
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr v-show="isRegularCustomer">
                    <td width="110px">@lang('core::label.customer'):</td>
                    <td width="156px">
                         <chosen
                            class="required"
                            name="customer"
                            v-model='info.customer_id'
                            :options='reference.customers'
                            @change='notify("customer")'
                            :disabled='disabled'>
                        </chosen>
                    </td>
                </tr>
                <tr v-show="isRegularCustomer">
                    <td>@lang('core::label.customer.branch'):</td>
                    <td>
                        <chosen
                            name="customer-detail"
                            v-model='info.customer_detail_id'
                            :options='reference.customerDetails'
                            :disabled='disabled'>
                        </chosen>
                    </td>
                </tr>
                <tr v-show="isWalkInCustomer">
                    <td width="110px">@lang('core::label.walk.in.name'):</td>
                    <td width="156px"><input type="text" class="required" name="customer-walk-in-name" ref="walkin" v-model="info.customer_walk_in_name" :disabled='disabled'></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.salesman'):</td>
                    <td>
                        <chosen
                            class="required"
                            name="salesman"
                            v-model='info.salesman_id'
                            :options='reference.users'
                            :disabled='!updatable'>
                        </chosen>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.term'):</td>
                    <td><number-input name="term" v-model='info.term' :disabled='!updatable' :precision='false'></number-input></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@push('script')
    @javascript('customers', $customers)

    <script src="{{ elixir('js/SalesReturnInbound/detail.js') }}"></script>
@endpush
