@extends('core::inventory-chain.detail')
@section('title', trans('core::label.stock.return.outbound.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.deliver.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to'
                        v-model='info.deliver_to'
                        :options='reference.branches'
                        :disabled='fromInvoice || disabled'
                        @change='getDeliveryLocations'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.deliver.to.location'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to-location'
                        v-model='info.deliver_to_location'
                        :options='reference.deliveryLocations'
                        :disabled='fromInvoice || disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
@stop

@push('script')
    <script src="{{ elixir('js/StockReturnOutbound/detail.js') }}"></script>
@endpush
