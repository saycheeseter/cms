@extends('core::inventory-chain.list')
@section('title', trans('core::label.stock.return.outbound.list'))

@section('datatable')
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirmation"
        v-model="datatable.selected">
        <template slot="header">
            <table-header v-show='columnSettings.selections.approval_status'>@lang('core::label.approval.status')</table-header>
            <table-header v-show='columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header v-show='columnSettings.selections.reference'>@lang('core::label.reference')</table-header>
            <table-header v-show='columnSettings.selections.created_from'>@lang('core::label.created.from')</table-header>
            <table-header v-show='columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
            <table-header v-show='columnSettings.selections.for_location'>@lang('core::label.for.location')</table-header>
            <table-header v-show='columnSettings.selections.deliver_to'>@lang('core::label.deliver.to')</table-header>

            @if($permissions['show_price_amount'])
                <table-header v-show='columnSettings.selections.amount'>@lang('core::label.amount')</table-header>
            @endif

            <table-header v-show='columnSettings.selections.transaction_status'>@lang('core::label.transaction.status')</table-header>
            <table-header v-show='columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='columnSettings.selections.requested_by'>@lang('core::label.request.by')</table-header>
            <table-header v-show='columnSettings.selections.created_by'>@lang('core::label.created.by')</table-header>
            <table-header v-show='columnSettings.selections.created_date'>@lang('core::label.created.date')</table-header>
            <table-header v-show='columnSettings.selections.audited_by'>@lang('core::label.audit.by')</table-header>
            <table-header v-show='columnSettings.selections.audited_date'>@lang('core::label.audit.date')</table-header>
            <table-header v-show='columnSettings.selections.deleted'>@lang('core::label.deleted')</table-header>
            <table-header v-for='udf in udfs' :key='udf.alias'>
                @{{ udf.label }}
            </table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='columnSettings.selections.approval_status' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.approval_status_label }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.transaction_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--reference" v-show='columnSettings.selections.reference' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reference.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.created_from' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.created_for' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.for_location' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.deliver_to' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.deliver_to }}
                    </template>
                </table-cell-data>
            </td>

            @if($permissions['show_price_amount'])
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.amount' @click="show(row.data.id)">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data.amount }}
                        </template>
                    </table-cell-data>
                </td>
            @endif

            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='columnSettings.selections.transaction_status' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_status }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.selections.remarks' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--requestedBy" v-show='columnSettings.selections.requested_by' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.requested_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" v-show='columnSettings.selections.created_by' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.created_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--auditedBy" v-show='columnSettings.selections.audited_by' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.audited_date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--deleted" v-show='columnSettings.selections.deleted' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.deleted }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--udf" v-for='udf in udfs' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data[udf.alias] }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@push('script')
    <script src="{{ elixir('js/StockReturnOutbound/list.js') }}"></script>
@endpush
