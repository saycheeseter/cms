@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.detail.sales.report'))

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            <control-button name='filters' icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
        <template slot="right">
            @if($exportPermission)
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a> 
                        </li>
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-model="datatable.selected">
        <template slot="header">
            <table-header v-show='columnSettings.selections.created_from_name'>@lang('core::label.created.from')</table-header>
            <table-header v-show='columnSettings.selections.created_for_name'>@lang('core::label.created.for')</table-header>
            <table-header v-show='columnSettings.selections.for_location_name'>@lang('core::label.for.location')</table-header>
            <table-header v-show='columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header v-show='columnSettings.selections.reference_id'>@lang('core::label.reference')</table-header>
            <table-header v-show='columnSettings.selections.approval'>@lang('core::label.approval.status')</table-header>
            <table-header v-show='columnSettings.selections.customer_type'>@lang('core::label.customer.type')</table-header>
            <table-header v-show='columnSettings.selections.customer_name'>@lang('core::label.customer')</table-header>
            <table-header v-show='columnSettings.selections.total_amount'>@lang('core::label.total.amount')</table-header>
            <table-header v-show='columnSettings.selections.remaining_amount'>@lang('core::label.remaining.amount')</table-header>
            <table-header v-show='columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='columnSettings.selections.salesman_name'>@lang('core::label.salesman')</table-header>
            <table-header v-show='columnSettings.selections.term'>@lang('core::label.term')</table-header>
            <table-header v-show='columnSettings.selections.requested_by_name'>@lang('core::label.request.by')</table-header>
            <table-header v-show='columnSettings.selections.created_by_name'>@lang('core::label.created.by')</table-header>
            <table-header v-show='columnSettings.selections.created_at'>@lang('core::label.created.date')</table-header>
            <table-header v-show='columnSettings.selections.audited_by_name'>@lang('core::label.audit.by')</table-header>
            <table-header v-show='columnSettings.selections.audited_date'>@lang('core::label.audit.date')</table-header>
            <table-header v-show='columnSettings.selections.deleted'>@lang('core::label.deleted')</table-header>
            <table-header v-show='columnSettings.selections.product_barcode'>@lang('core::label.barcode')</table-header>
            <table-header v-show='columnSettings.selections.stock_no'>@lang('core::label.stock.no')</table-header>
            <table-header v-show='columnSettings.selections.chinese_name'>@lang('core::label.chinese.name')</table-header>
            <table-header v-show='columnSettings.selections.product_name'>@lang('core::label.product.name')</table-header>
            <table-header v-show='columnSettings.selections.qty'>@lang('core::label.qty')</table-header>
            <table-header v-show='columnSettings.selections.unit_name'>@lang('core::label.uom')</table-header>
            <table-header v-show='columnSettings.selections.unit_qty'>@lang('core::label.unit.specs')</table-header>
            <table-header v-show='columnSettings.selections.total_qty'>@lang('core::label.total.qty')</table-header>
            <table-header v-show='columnSettings.selections.product_serial'>@lang('core::label.serial')</table-header>
            <table-header v-show='columnSettings.selections.product_batch'>@lang('core::label.batch')</table-header>
            <table-header v-show='columnSettings.selections.oprice'>@lang('core::label.o.price')</table-header>
            <table-header v-show='columnSettings.selections.price'>@lang('core::label.price')</table-header>
            <table-header v-show='columnSettings.selections.discount1'>@lang('core::label.discount.1')</table-header>
            <table-header v-show='columnSettings.selections.discount2'>@lang('core::label.discount.2')</table-header>
            <table-header v-show='columnSettings.selections.discount3'>@lang('core::label.discount.3')</table-header>
            <table-header v-show='columnSettings.selections.discount4'>@lang('core::label.discount.4')</table-header>
            <table-header v-show='columnSettings.selections.detail_total_amount'>@lang('core::label.amount')</table-header>
            <table-header v-show='columnSettings.selections.detail_remarks'>@lang('core::label.detail.remarks')</table-header>
        </template>
        <template slot="display" scope="row">
            <td v-show='columnSettings.selections.created_from_name' class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_from_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.created_for_name' class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.for_location_name' class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.transaction_date' class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.reference_id' class="v-dataTable__tableCell v-dataTable__tableCell--reference">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reference_id }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.approval' class="v-dataTable__tableCell v-dataTable__tableCell--status">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.approval }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.customer_type' class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer_type }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.customer_name' class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.total_amount' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.remaining_amount' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remaining_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.remarks' class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.salesman_name' class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.salesman_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.term' class="v-dataTable__tableCell v-dataTable__tableCell--term">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.term }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.requested_by_name' class="v-dataTable__tableCell v-dataTable__tableCell--requestedBy">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.requested_by_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.created_by_name' class="v-dataTable__tableCell v-dataTable__tableCell--createdBy">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.created_at' class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_at }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.audited_by_name' class="v-dataTable__tableCell v-dataTable__tableCell--auditedBy">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_by_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.audited_date' class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_date }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.deleted' class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.deleted }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.product_barcode' class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.stock_no' class="v-dataTable__tableCell v-dataTable__tableCell--stockNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_no }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.chinese_name' class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.product_name' class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.qty' class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.qty }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.unit_name' class="v-dataTable__tableCell v-dataTable__tableCell--uom">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.unit_name }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.unit_qty' class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.unit_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.total_qty' class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.product_serial' class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_serial }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.product_batch' class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_batch }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.oprice' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.oprice }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.price' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.discount1' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discount1 }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.discount2' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discount2 }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.discount3' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discount3 }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.discount4' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discount4 }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.detail_total_amount' class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.detail_total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.detail_remarks' class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.detail_remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.qty'):</td>
                    <td><b><span>@{{ summary.total_qty }}</span></b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-30">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.amount'):</td>
                    <td><b><span>@{{ summary.total_amount }}</span></b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-30">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.remaining'):</td>
                    <td><b><span>@{{ summary.total_remaining_amount }}</span></b></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close='message.reset()'>
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="search">
    </filter-modal>

    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @stack('script')
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('customers', $customers)
    @javascript('users', $users)
    @javascript('brands', $brands)
    @javascript('categories', $categories)
    @javascript('suppliers', $suppliers)

    <script src="{{ elixir('js/Reports/Sales/detail-report.js') }}"></script>
@stop