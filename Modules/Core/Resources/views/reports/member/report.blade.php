@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.member.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
        @if(Auth::user()->hasAccessTo('export_member_report'))
            <template slot="right">
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            </template>
        @endif
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.full.name')</table-header>
            <table-header>@lang('core::label.member.rate')</table-header>
            <table-header>@lang('core::label.card.id')</table-header>
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.date.created')</table-header>
            <table-header>@lang('core::label.points')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.full_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.rate }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--cardId">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.card_id }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date_created }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.points }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('rates', $rates)

    <script src="{{ elixir('js/Reports/Member/report.js') }}"></script>
@stop