@extends('core::reports.product-price.container')
@section('title', trans('core::label.customer.product.price'))

@section('table')
    <datatable
        v-bind:tableId="datatable.details.id"
        v-bind:settings="datatable.details.settings"
        v-bind:table="datatable.details.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.customer')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.latest.unit')</table-header>
            <table-header>@lang('core::label.latest.unit.qty')</table-header>
            <table-header>@lang('core::label.latest.price')</table-header>
            <table-header>@lang('core::label.latest.fields.updated.date')</table-header>
            <table-header>@lang('core::label.lowest.price')</table-header>
            <table-header>@lang('core::label.highest.price')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--unitSpecs">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.latest_unit }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.latest_unit_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.latest_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.latest_fields_updated_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.lowest_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.highest_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@push('script')
    @javascript('customers', $customers)

    <script src="{{ elixir('js/Reports/Customer/product-price.js') }}"></script>
@endpush
