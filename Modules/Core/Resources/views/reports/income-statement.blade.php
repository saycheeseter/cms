@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.income.statement'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <div class="incomeStatement" align="center">
        <div class="incomeStatement__branchName">@{{ labels.branch }}</div>
        <div class="incomeStatement__dateRange">@{{ labels.date }}</div>
        <table class="incomeStatement__computation">
            <tr>
                <td width="400px">@lang('core::label.gross.sales')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-green" class="link-green" @click='goToSalesOutbound'>@{{ summary.gross_sales }}</a></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.return')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-red" @click='goToSalesReturnInbound'>@{{ summary.return_amount }}</a></span></td>
            </tr>
            <tr class="equation-line">
                <td>@lang('core::label.net.sales')</td>
                <td align="right"><span>@{{ netSales }}</span></td>
            </tr>
            <tr>
                <td>@lang('core::label.cost.of.goods.sold') (@lang('core::label.purchase.price'), @lang('core::label.moving.average'))</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-red" @click='goToSalesOutbound'>@{{ summary.cost_of_goods_sold }} (@{{ summary.ma_cost_of_goods_sold }})</a></span></td>
            </tr>
            <tr class="equation-line">
                <td>@lang('core::label.gross.profit')</td>
                <td align="right"><span>@{{ grossProfit }} (@{{ maGrossProfit }})</span></td>
            </tr>
            <tr>
                <td>@lang('core::label.other.payment')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-red" @click='goToOtherPayment'>@{{ summary.other_payment }}</a></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.other.income')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-green" @click='goToOtherIncome'>@{{ summary.other_income }}</a></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.damage')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-red" @click='goToDamageOutbound'>@{{ summary.damage }}</a></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.adjustment.discrepancy')</td>
                <td align="right"><span><a href='javascript:void(0)' class="link-red" @click='goToInventoryAdjust'>@{{ summary.adjustment_discrepancy }}</a></span></td>
            </tr>
            <tr class="equation-line">
                <td><b>@lang('core::label.net.income')</b></td>
                <td align="right"><b><span>@{{ netIncome }} (@{{ maNetIncome }})</span></b></td>
            </tr>
        </table>
    </div>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="fetch">
    </filter-modal>
@stop

@section('script')
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/income-statement.js') }}"></script>
@stop
