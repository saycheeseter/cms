@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.senior.transaction.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.or.si.no')</table-header>
            <table-header>@lang('core::label.senior.number')</table-header>
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header>@lang('core::label.price')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.or_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.senior_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.senior_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/Senior/transaction-report.js') }}"></script>
@stop