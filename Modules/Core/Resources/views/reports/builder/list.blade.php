@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.custom.report.builder.list'))

@section('stylesheet')
    <style type="text/css">
        .tree-lists {
            padding-left: 0px !important;
            margin-bottom: 0px !important;
            list-style: none !important;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            @if($permissions['create'])
                <control-button icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.custom.report')
                </control-button>
            @endif
            <control-button icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy='confirmation'>
        <template slot="header">
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.module.group')</table-header>
            <table-header>@lang('core::label.created.by')</table-header>
            <table-header>@lang('core::label.created.at')</table-header>
            <table-header>@lang('core::label.modified.by')</table-header>
            <table-header>@lang('core::label.updated.at')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.module_group }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_at }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--modifiedBy" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.modified_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click='view(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.updated_at }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.custom.report'),
        'content' => trans('core::confirm.delete.custom.report')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('permissions', $permissions)
    <script src="{{ elixir('js/Reports/Builder/list.js') }}"></script>
@stop