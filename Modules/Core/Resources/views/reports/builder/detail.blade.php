@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.custom.report.builder.detail'))

@section('stylesheet')
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <td>@lang('core::label.module.name'):</td>
            <td width="200px"><input maxlength="120" type="text" v-model='module.name'></td>
            <td>@lang('core::label.module'):</td>
            <td width="200px">
                <chosen v-model='module.groupId' :options='references.module' @change='changeModule()' :disabled="(module.id != 'create')" ></chosen>
            </td>
        </table>
    </div>
    <control-bar class="c-content-group c-has-bb">
        <template slot="right">
            <control-button icon="fa fa-gear" @click="excel.visible = true">
                @lang('core::label.column.settings')
            </control-button>
            <control-button icon="fa fa-save" @click='save()'>
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    @include('core::reports.builder.sections.content')
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::reports.builder.sections.excel-settings')
    <dialog-box
        :show='dialog.message !=""'
        v-on:close='redirect'
    >
        <template slot='content'>@{{ dialog.message }}</template>
    </dialog-box>
@stop

@section('script')
    @javascript('id', Request::segment(3))
    @javascript('permissions', $permissions)
    <script src="{{ elixir('js/Reports/Builder/detail.js') }}"></script>
@stop