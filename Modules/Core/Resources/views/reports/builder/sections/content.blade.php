<div class="c-content-group c-tb c-lr flex flex-1 overflow-hidden">
    <div id="report-builder" class="flex flex-1">
        <div class="wrapper fields">
            <div class="c-sub-header">
                <div class="c-content-group clearfix">
                    <div class="c-mgl-5 c-mgt-5">
                        @lang('core::label.fields')
                    </div>
                </div>
                <div class="c-content-group clearfix">
                    <table class="tbl-design field-icons">
                        <tbody>
                            <tr>
                                <td width="220px"><input type="text" v-model='search'></td>
                                <td width="24px"><img class="action-icon" src="{{ asset('images/table-column.svg') }}"></td>
                                <td width="24px"><img class="action-icon" src="{{ asset('images/grouping.svg') }}"></td>
                                <td width="24px"><img class="action-icon" src="{{ asset('images/sorting.svg') }}"></td>
                                <td width="24px"><img class="action-icon" src="{{ asset('images/summation.svg') }}"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="c-sub-sidebar">
                <div class="c-content-group">
                    <table border="1" class="tbl-design full-width horizontal-border">
                        <tbody>
                            <tr v-for='(field, index) in references.fields' v-show='field.visibility'>
                                <td width="220px">@{{ field.header_name }}</td>
                                <td width="24px" align="center"><input type="checkbox" v-model='field.checkbox.columnArrangement' @change='toggleColumnArrangementSection(field, field.checkbox.columnArrangement, index)'></td>
                                <td width="24px" align="center"><input type="checkbox" v-model='field.checkbox.grouping' @change='toggleGroupingSection(field, field.checkbox.grouping, index)'></td>
                                <td width="24px" align="center"><input type="checkbox" v-model='field.checkbox.order' @change='toggleOrderSection(field, field.checkbox.order, index)'></td>
                                <td width="24px" align="center"><input type="checkbox" v-model='field.checkbox.total' @change='toggleTotalSection(field, field.checkbox.total, index)'></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="wrapper column-arrangement">
            <div class="c-sub-header">
                <img class="action-icon" src="{{ asset('images/table-column.svg') }}"> @lang('core::label.column.arrangement')
            </div>
            <div class="c-sub-sidebar">
                <ul class="sortable" v-sortable="{onEnd: sortColumnArrangement}">
                    <li class="sortable-content" v-for="field in module.selections.columnArrangement" :key="field.key">@{{ field.header_name }}</li>
                </ul>
            </div>
        </div>
        <div class="wrapper grouping">
            <div class="c-sub-header">
                <img class="action-icon" src="{{ asset('images/grouping.svg') }}"> @lang('core::label.grouping')
            </div>
            <div class="c-sub-sidebar">
                <ul class="sortable" v-sortable="{onEnd: sortGrouping}">
                    <li class="sortable-content" v-for="field in module.selections.grouping" :key="field.key">
                        @{{ field.header_name }}
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrapper order-by">
            <div class="c-sub-header">
                <img class="action-icon" src="{{ asset('images/sorting.svg') }}"> @lang('core::label.order.by')
            </div>
            <div class="c-sub-sidebar">
                <ul class="sortable" v-sortable="{onEnd: sortOrder}">
                    <li class="sortable-content clearfix" v-for="(field, index) in module.selections.order" :key="field.key">
                        <div class="pull-left">
                            @{{ field.header_name }}
                        </div>
                        <div class="pull-right">
                            <input type="button" class="c-btn c-lt-green btn-order-by" v-model="field.order_status" @click='toggleOrder(field.order_status, index)'>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrapper total">
            <div class="c-sub-header">
                <img class="action-icon" src="{{ asset('images/summation.svg') }}"> @lang('core::label.total')
            </div>
            <div class="c-sub-sidebar">
                <ul>
                    <li class="clearfix" v-for="(field, index) in module.selections.total" :key="field.key">
                        <div class="pull-left">
                            @{{ field.header_name }}
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>