<modal
    id="excel-settings-modal"
    v-show="excel.visible"
    @close="excel.visible = false"
>
    <template slot="header">
        @lang('core::label.column.settings')
    </template>
    <template slot="content">
        <div class="c-content-group c-b">
            <h4>@lang('core::label.header.style')</h4>
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.text.align'):</td>
                    <td>
                        <select v-model='module.excel.headerStyle.textAlign'>
                            <option value='left'>@lang('core::label.left')</option>
                            <option value='center'>@lang('core::label.center')</option>
                            <option value='right'>@lang('core::label.right')</option>
                        </select>
                    </td>
                    <td>
                        <label class="radio-chkbx">
                            <input type="checkbox" v-model='module.excel.headerStyle.bold'>
                            @lang('core::label.bold')
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="c-content-group c-has-bl c-has-br c-has-bb flex flex-column flex-1">
            <datatable
                v-bind:tableId="excel.id"
                v-bind:settings="excel.settings"
                v-bind:table="excel.value">
                <template slot="header">
                    <table-header>@lang('core::label.column.name')</table-header>
                    <table-header>@lang('core::label.header.name')</table-header>
                    <table-header width="50px">@lang('core::label.cell.width')</table-header>
                    <table-header>@lang('core::label.text.align')</table-header>
                    <table-header>@lang('core::label.type')</table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.key }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                        <input type="text" v-model="row.data.header_name" maxlength="40">
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                        <number-input v-model="row.data.excel.width" :precision="false" max="999" >
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
                        <select v-model="row.data.excel.textAlign">
                            <option value="left">@lang('core::label.left')</option>
                            <option value="center">@lang('core::label.center')</option>
                            <option value="right">@lang('core::label.right')</option>
                        </select>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
                        <select v-model="row.data.excel.type">
                            <option :value="type.TEXT">@lang('core::label.text')</option>
                            <option :value="type.NUMERIC">@lang('core::label.numeric')</option>
                        </select>
                    </td>
                </template>
            </datatable>
        </div>
    </template>
    <template slot="footer">
        <input class="c-btn c-dk-blue" type="button" value="@lang('core::label.proceed')" @click='excel.visible = false'>
    </template>
</modal>