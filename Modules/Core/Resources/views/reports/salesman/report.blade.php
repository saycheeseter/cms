@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.salesman.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>

    @include('core::reports.salesman.transactions')
    @include('core::reports.salesman.products')

    <div class="c-content-group c-tb c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.amount'):</td>
                <td align="right"><b><span> @{{ totalAmount }}</span></b></td>
            </tr>
        </table>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="search">
    </filter-modal>
    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('salesmen', $salesmen)

    <script src="{{ elixir('js/Reports/Salesman/report.js') }}"></script>
@stop