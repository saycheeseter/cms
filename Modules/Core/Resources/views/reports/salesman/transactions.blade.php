<datatable
    v-bind:tableId="datatable.transactions.settings.id"
    v-bind:settings="datatable.transactions.settings"
    v-bind:table="datatable.transactions.value"
    v-on:reload='paginateTransactions'>
    <template slot="header">
        <table-header v-show='columnSettings.selections.branch_name'>@lang('core::label.branch')</table-header>
        <table-header v-show='columnSettings.selections.transaction_date'>@lang('core::label.date')</table-header>
        <table-header v-show='columnSettings.selections.sheet_number'>@lang('core::label.sheet.number')</table-header>
        <table-header v-show='columnSettings.selections.customer_name'>@lang('core::label.customer')</table-header>
        <table-header>@lang('core::label.salesman')</table-header>
        <table-header v-show='columnSettings.selections.total_amount'>@lang('core::label.amount')</table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='columnSettings.selections.branch_name'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.branch_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.transaction_date'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" v-show='columnSettings.selections.sheet_number'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.customer_name'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.customer_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.salesman_full_name }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_amount'>
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_total_amount }}
                </template>
            </table-cell-data>
        </td>
    </template>
</datatable>