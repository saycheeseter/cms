<datatable
        v-bind:tableId="datatable.products.settings.id"
        v-bind:settings="datatable.products.settings"
        v-bind:table="datatable.products.value">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.chinese.name')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header>@lang('core::label.lowest.price')</table-header>
            <table-header>@lang('core::label.average.price')</table-header>
            <table-header>@lang('core::label.highest.price')</table-header>
            <table-header>@lang('core::label.total.amount')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.minimum_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.average_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.maximum_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
        </template>
</datatable>