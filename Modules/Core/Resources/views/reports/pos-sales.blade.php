@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.pos.sales.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.transaction.date')</table-header>
            <template v-for="terminal in references.terminals">
                <table-header>@lang('core::label.terminal') @{{ terminal }}</table-header>
            </template>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date" @click="transactions(row.data.transaction_date)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <template v-for="terminal in references.terminals">
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="transactions(row.data.transaction_date)">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data['terminal_' + terminal] }}
                        </template>
                    </table-cell-data>
                </td>
            </template>
        </template>
    </datatable>
@stop


@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="search">
    </filter-modal>
@stop

@section('script')
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/pos-sales.js') }}"></script>
@stop