@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.inventory.warning'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
        <template slot="right">
            <control-dropdown name='export-excel' class="list" icon="fa fa-file-excel-o">
                @lang('core::label.export.excel')
                <template slot="list">
                    <li>
                        <a @click="excel()">
                            @lang('core::label.default.excel.template')
                        </a>
                    </li>
                </template>
            </control-dropdown>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.product.name')</table-header>
            <table-header v-show='columnSettings.selections.inventory'>@lang('core::label.inventory')</table-header>
            <table-header v-show='columnSettings.selections.reservedQty'>@lang('core::label.reserved.qty')</table-header>
            <table-header v-show='columnSettings.selections.requestedQty'>@lang('core::label.requested.qty')</table-header>
            <table-header v-show='columnSettings.selections.projectedInventory'>@lang('core::label.projected.inventory')</table-header>
            <table-header v-show='columnSettings.selections.min'>@lang('core::label.min')</table-header>
            <table-header v-show='columnSettings.selections.max'>@lang('core::label.max')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.inventory'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.inventory }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.reservedQty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reserved_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.requestedQty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.requested_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.projectedInventory'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.projected_inventory }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.min'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.min }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.max'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.max }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('locations', $locations)

    <script src="{{ elixir('js/Reports/Product/inventory-warning.js') }}"></script>
@stop