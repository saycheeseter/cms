@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.negative.profit.product.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td width="100px">
                            <datepicker format="yyyy-MM-dd" v-model='filter.from'></datepicker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td width="100px">
                            <datepicker format="yyyy-MM-dd" v-model='filter.to'></datepicker>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                name='branches'
                                v-model='filter.branch'
                                :options='references.branches'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='paginate()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="right">
            @if(Auth::user()->hasAccessTo('export_negative_profit_product'))
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.sheet.number')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header>@lang('core::label.cost')</table-header>
            <table-header>@lang('core::label.price')</table-header>
            <table-header>@lang('core::label.loss')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.cost }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.loss }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('brands', $brands)
    @javascript('categories', $categories)
    @javascript('suppliers', $suppliers)

    <script src="{{ elixir('js/Reports/Product/negative-profit.js') }}"></script>
@stop