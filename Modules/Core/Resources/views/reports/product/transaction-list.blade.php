@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.transaction.list.report'))

@section('stylesheet')
    <style type="text/css">
        .advanced-info .dropdown-menu {
            right: -55px !important;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name='filters' icon="fa fa-search" @click="filter.product.visible = true">
                @lang('core::label.product.filters')
            </control-button>
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td width="100px">
                            <datepicker format="yyyy-MM-dd" v-model='filter.transaction.from'></datepicker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td width="100px">
                            <datepicker format="yyyy-MM-dd" v-model='filter.transaction.to'></datepicker>
                        </td>
                        <td>@lang('core::label.location'):</td>
                        <td>
                            <chosen
                                name='location'
                                v-model='filter.transaction.location'
                                :options='references.locations'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='paginate()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="right">
            <control-dropdown class="advanced-info" icon="fa fa-arrow-circle-right">
                @lang('core::label.show.advanced.info')
                <template slot="list">
                    <div class="c-content-group c-lr">
                        <table class="table-design full-width hover-default" border="1">
                            <thead>
                                <th class="text-center">@lang('core::label.particular')</th>
                                <th class="text-center">@lang('core::label.purchase.inbound')</th>
                                <th class="text-center">@lang('core::label.purchase.return.outbound')</th>
                                <th class="text-center">@lang('core::label.sales.outbound')</th>
                                <th class="text-center">@lang('core::label.sales.return.inbound')</th>
                                <th class="text-center">@lang('core::label.stock.delivery.inbound')</th>
                                <th class="text-center">@lang('core::label.stock.delivery.outbound')</th>
                                <th class="text-center">@lang('core::label.stock.return.inbound')</th>
                                <th class="text-center">@lang('core::label.adjustment.increase')</th>
                                <th class="text-center">@lang('core::label.stock.return.outbound')</th>
                                <th class="text-center">@lang('core::label.damage.outbound')</th>
                                <th class="text-center">@lang('core::label.adjustment.decrease')</th>
                                <th class="text-center">@lang('core::label.product.conversion.increase')</th>
                                <th class="text-center">@lang('core::label.product.conversion.decrease')</th>
                                <th class="text-center">@lang('core::label.auto.product.conversion.increase')</th>
                                <th class="text-center">@lang('core::label.auto.product.conversion.decrease')</th>
                                <th class="text-center">@lang('core::label.pos.sales')</th>
                                <th class="text-center">@lang('core::label.check.all')</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>@lang('core::label.with.transactions')</td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.purchase_inbound' @click="check($event, 'purchase_inbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.purchase_return_outbound' @click="check($event, 'purchase_return_outbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.sales_outbound' @click="check($event, 'sales_outbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.sales_return_inbound' @click="check($event, 'sales_return_inbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.stock_delivery_inbound' @click="check($event, 'stock_delivery_inbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.stock_delivery_outbound' @click="check($event, 'stock_delivery_outbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.stock_return_inbound' @click="check($event, 'stock_return_inbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.adjustment_increase' @click="check($event, 'adjustment_increase', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.stock_return_outbound' @click="check($event, 'stock_return_outbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.damage_outbound' @click="check($event, 'damage_outbound', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.adjustment_decrease' @click="check($event, 'adjustment_decrease', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.product_conversion_increase' @click="check($event, 'product_conversion_increase', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.product_conversion_decrease' @click="check($event, 'product_conversion_decrease', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.auto_product_conversion_increase' @click="check($event, 'auto_product_conversion_increase', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.auto_product_conversion_decrease' @click="check($event, 'auto_product_conversion_decrease', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.with.pos_sales' @click="check($event, 'pos_sales', 'without')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.all.with' @click="checkAll($event, 'with')"></td>
                                </tr>
                                <tr>
                                    <td>@lang('core::label.without.transactions')</td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.purchase_inbound' @click="check($event, 'purchase_inbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.purchase_return_outbound' @click="check($event, 'purchase_return_outbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.sales_outbound' @click="check($event, 'sales_outbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.sales_return_inbound' @click="check($event, 'sales_return_inbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.stock_delivery_inbound' @click="check($event, 'stock_delivery_inbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.stock_delivery_outbound' @click="check($event, 'stock_delivery_outbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.stock_return_inbound' @click="check($event, 'stock_return_inbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.adjustment_increase' @click="check($event, 'adjustment_increase', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.stock_return_outbound' @click="check($event, 'stock_return_outbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.damage_outbound' @click="check($event, 'damage_outbound', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.adjustment_decrease' @click="check($event, 'adjustment_decrease', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.product_conversion_increase' @click="check($event, 'product_conversion_increase', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.product_conversion_decrease' @click="check($event, 'product_conversion_decrease', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.auto_product_conversion_increase' @click="check($event, 'auto_product_conversion_increase', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.auto_product_conversion_decrease' @click="check($event, 'auto_product_conversion_decrease', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.without.pos_sales' @click="check($event, 'pos_sales', 'with')"></td>
                                    <td align="center"><input type="checkbox" v-model='filter.advancedInfo.all.without' @click="checkAll($event, 'without')"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </template>
            </control-dropdown>
            @if(Auth::user()->hasAccessTo('export_product_transaction_list'))
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.chinese.name')</table-header>
            <table-header>@lang('core::label.beginning')</table-header>
            <table-header>@lang('core::label.purchase.inbound')</table-header>
            <table-header>@lang('core::label.purchase.return.outbound')</table-header>
            <table-header>@lang('core::label.sales.outbound')</table-header>
            <table-header>@lang('core::label.sales.return.inbound')</table-header>
            <table-header>@lang('core::label.stock.delivery.inbound')</table-header>
            <table-header>@lang('core::label.stock.delivery.outbound')</table-header>
            <table-header>@lang('core::label.stock.return.inbound')</table-header>
            <table-header>@lang('core::label.adjustment.increase')</table-header>
            <table-header>@lang('core::label.stock.return.outbound')</table-header>
            <table-header>@lang('core::label.damage.outbound')</table-header>
            <table-header>@lang('core::label.adjustment.decrease')</table-header>
            <table-header>@lang('core::label.product.conversion.increase')</table-header>
            <table-header>@lang('core::label.product.conversion.decrease')</table-header>
            <table-header>@lang('core::label.auto.product.conversion.increase')</table-header>
            <table-header>@lang('core::label.auto.product.conversion.decrease')</table-header>
            <table-header>@lang('core::label.pos.sales')</table-header>
            <table-header>@lang('core::label.inventory')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.beginning }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.purchase_inbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.purchase_return_outbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sales_outbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sales_return_inbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_delivery_inbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_delivery_outbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_return_inbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.adjustment_increase }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_return_outbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.damage_outbound }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.adjustment_decrease }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_conversion_increase }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_conversion_decrease }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.auto_product_conversion_increase }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.auto_product_conversion_decrease }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.pos_sales }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.inventory }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-filter', [
        'show' => 'filter.product.visible',
        'options' => 'filter.product.options',
        'model' => 'filter.product.data',
        'search' => 'paginate'
    ])
@stop

@section('script')
    @stack('script')

    @javascript('locations', $locations)

    <script src="{{ elixir('js/Reports/Product/transaction-list.js') }}"></script>
@stop