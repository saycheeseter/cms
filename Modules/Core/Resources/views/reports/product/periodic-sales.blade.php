@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.periodic.sales.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name='filters' icon="fa fa-search" @click="filter.product.visible = true">
                @lang('core::label.product.filters')
            </control-button>
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td width="100px">
                            <select v-model='filter.transaction.fromMonth'>
                                <option v-for='month in references.month' :value='month.id'> @{{ month.label }}</option>
                            </select>
                        </td>
                        <td width="100px">
                            <select v-model='filter.transaction.fromYear'>
                                <option v-for='year in references.year' :value='year.id'> @{{ year.label }}</option>
                            </select>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td width="100px">
                            <select v-model='filter.transaction.toMonth'>
                                <option v-for='month in references.month' :value='month.id'> @{{ month.label }}</option>
                            </select>
                        </td>
                        <td width="100px">
                            <select v-model='filter.transaction.toYear'>
                                <option v-for='year in references.year' :value='year.id'> @{{ year.label }}</option>
                            </select>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                name='branches'
                                v-model='filter.transaction.branch'
                                :options='references.branches'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='paginate()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>

        @if(Auth::user()->hasAccessTo('export_product_periodic_sales'))
            <template slot="right">
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            </template>
        @endif
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.chinese.name')</table-header>
            <table-header>@lang('core::label.inventory')</table-header>
            <template v-for='month in references.monthRange'>
                <table-header>@{{ month }} @lang('core::label.total.amount')</table-header>
                <table-header>@{{ month }} @lang('core::label.total.qty')</table-header>
            </template>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.inventory }}
                    </template>
                </table-cell-data>
            </td>
            <template v-for='month in references.monthRange'>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data[month + '_total_amount'] }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data[month + '_total_qty'] }}
                        </template>
                    </table-cell-data>
                </td>
            </template>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-filter', [
        'show' => 'filter.product.visible',
        'options' => 'filter.product.options',
        'model' => 'filter.product.data',
        'search' => 'paginate'
    ])
@stop

@section('script')
    @stack('script')

    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/Product/periodic-sales.js') }}"></script>
@stop