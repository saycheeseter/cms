@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.inventory.value'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name='filter-by-product' icon="fa fa-search"  @click="filter.product.visible = true">
                @lang('core::label.filter.by.product')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-on:reload="paginate"
        v-bind:table="datatable.value"> 
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.chinese.name')</table-header>
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.location')</table-header>
            <table-header>@lang('core::label.supplier')</table-header>
            <table-header>@lang('core::label.inventory')</table-header>
            <table-header>@lang('core::label.ma.cost')</table-header>
            <table-header>@lang('core::label.value.ma.cost')</table-header>
            <table-header>@lang('core::label.cost')</table-header>
            <table-header>@lang('core::label.value.cost')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.branch }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--supplier">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.inventory }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.current_cost }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.current_cost_inventory_value }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.purchase_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.purchase_inventory_value }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.positive.ma.cost.amount'):</td>
                    <td align="right"><b><span v-html="summaries.positive_ma_cost_amount"></span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.negative.ma.cost.amount'):</td>
                    <td align="right"><b><span v-html="summaries.negative_ma_cost_amount"></span></b></td>
                </tr>
                <tr class="equation-line">
                    <td>@lang('core::label.total.ma.cost.amount'):</td>
                    <td align="right"><b><span v-html="totalMACostAmount"></span></b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-30">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.positive.inventory'):</td>
                    <td align="right"><b><span v-html="summaries.positive_inventory"></span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.negative.inventory'):</td>
                    <td align="right"><b><span v-html="summaries.negative_inventory"></span></b></td>
                </tr>
                <tr class="equation-line">
                    <td>@lang('core::label.net.inventory.qty'):</td>
                    <td align="right"><b><span v-html="netInventoryQty"></span></b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-30">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.positive.cost.amount'):</td>
                    <td align="right"><b><span v-html="summaries.positive_cost_amount"></span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.negative.cost.amount'):</td>
                    <td align="right"><b><span v-html="summaries.negative_cost_amount"></span></b></td>
                </tr>
                <tr class="equation-line">
                    <td>@lang('core::label.total.cost.amount'):</td>
                    <td align="right"><b><span v-html="totalCostAmount"></span></b></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-filter', [
        'show' => 'filter.product.visible',
        'options' => 'filter.product.options',
        'model' => 'filter.product.data',
        'search' => 'paginate'
    ])
@stop

@section('script')
    @stack('script')

    <script src="{{ elixir('js/Reports/product/inventory-value.js') }}"></script>
@stop