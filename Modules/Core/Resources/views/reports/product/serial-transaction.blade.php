@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.serial.transaction.report'))

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td width="89px">@lang('core::label.serial.no'):</td>
                <td><input type="text" v-model="serialNumberText" @keydown.enter="findSerialByNumber"></td>
            </tr>
        </table>
        <div class="c-content-group">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.product.name'):</td>
                        <td><b><span>@{{ serial.product.name }}</span></b></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.chinese.name'):</td>
                        <td><b><span>@{{ serial.product.chineseName }}</span></b></td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-30">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.barcode'):</td>
                        <td><b><span>@{{ serial.product.barcodes }}</span></b></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.stock.no'):</td>
                        <td><b><span>@{{ serial.product.stockNo }}</span></b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value">
        <template slot="header">
            <table-header>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.transaction.type')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header>@lang('core::label.created.for')</table-header>
            <table-header>@lang('core::label.for.location')</table-header>
            <table-header>@lang('core::label.particular')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--datetime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.serial_transaction.label }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        <a href="javascript:void(0);" @click="goToTransaction(row.data.serial_transaction.type, row.data.id)">
                            @{{ row.data.sheet_number }}
                        </a>
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.particulars }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    <script src="{{ elixir('js/Reports/Product/serial-transaction.js') }}"></script>
@stop