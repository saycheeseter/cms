@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container')
@section('title', trans('core::label.product.branch.inventory.report'))

@section('stylesheet')
    <style type="text/css">
        /*Partial fix - to be removed*/
        .chosen {
            max-width: initial !important;
        }
        .chosen .selected .fa-angle-up,
        .chosen .selected .fa-angle-down {
            position: relative !important;
            cursor: pointer !important;
            top: 4px !important;
            right: initial !important;
            float: right !important;
            margin-left: 3px;
        }
    </style>
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.locations'):</td>
                <td>
                    <chosen
                        v-model='filter.locations'
                        :options='references.locations.list'
                        v-bind:multiple='true'>
                    </chosen>
                </td>
                <td valign="top"><input type="button" class="c-btn c-dk-blue" @click='paginate()' value="@lang('core::label.search')"></td>
            </tr>
        </table>
    </div>
    <control-bar id="controls">
        <template slot="left">
            <control-button name='filters' icon="fa fa-search" @click="filter.product.visible = true">
                @lang('core::label.product.filters')
            </control-button>
        </template>

        @if(Auth::user()->hasAccessTo('export_product_branch_inventory'))
            <template slot="right">
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            </template>
        @endif
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <template v-for='location in references.locations.selected'>
                <table-header>@{{ location.label }}</table-header>
            </template>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <template v-for='location in references.locations.selected'>
                <table-header>@{{ row.data['location_' + location.id] }}</table-header>
            </template>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-filter', [
        'show' => 'filter.product.visible',
        'options' => 'filter.product.options',
        'model' => 'filter.product.data',
        'search' => 'paginate'
    ])
@stop

@section('script')
    @stack('script')

    @javascript('locations', $locations)

    <script src="{{ elixir('js/Reports/Product/branch-inventory.js') }}"></script>
@stop