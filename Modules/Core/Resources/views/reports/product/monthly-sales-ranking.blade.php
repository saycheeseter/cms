@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.monthly.sales.ranking'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.product.visible = true">
                @lang('core::label.filter.by.product')
            </control-button>
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.monthlySales.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header v-show='columnSettings.selections.stock_no'>@lang('core::label.stock.no')</table-header>
            <table-header>@lang('core::label.product.name')</table-header>
            <table-header v-show='columnSettings.selections.chinese_name'>@lang('core::label.chinese.name')</table-header>
            <table-header v-show='columnSettings.selections.description'>@lang('core::label.description')</table-header>
            <table-header v-show='columnSettings.selections.brand'>@lang('core::label.brand')</table-header>
            <table-header v-show='columnSettings.selections.supplier'>@lang('core::label.supplier')</table-header>
            <table-header v-show='columnSettings.selections.category'>@lang('core::label.category')</table-header>
            <table-header v-show='columnSettings.selections.total_qty'>@lang('core::label.total.qty')</table-header>
            <table-header v-show='columnSettings.selections.total_amount'>@lang('core::label.total.amount')</table-header>
            <table-header v-show='columnSettings.selections.total_profit'>@lang('core::label.total.profit')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.selections.stock_no'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.chinese_name'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.description'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.description }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.brand'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.brand }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.supplier'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.selections.category'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.category }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.total_qty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_quantity }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_amount'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_profit'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_profit }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.product-filter', [
        'show' => 'modal.filter.product.visible',
        'options' => 'modal.filter.product.options',
        'model' => 'modal.filter.product.data',
        'search' => 'paginate'
    ])

    <filter-modal
        v-show="modal.filter.monthlySales.visible"
        @close="modal.filter.monthlySales.visible = false"
        :filters="modal.filter.monthlySales.options"
        v-model='modal.filter.monthlySales.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>
@stop

@section('script')
    @stack('script')

    @javascript('locations', $locations)
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/Product/monthly-sales-ranking.js') }}"></script>
@stop