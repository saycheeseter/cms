@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.pos.discounted.summary.report'))

@section('stylesheet')
    <style type="text/css">
        .chosen__terminal {
            min-width: 100px !important;
            max-width: 100px !important;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td>
                            <date-time-picker
                                id="date-from"
                                name='date-from'
                                v-model='filter.transaction.date_from'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td>
                            <date-time-picker
                                id="date-to"
                                name='date-to'
                                v-model='filter.transaction.date_to'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                @change="populateTerminals()"
                                name='branches'
                                v-model='filter.transaction.branch'
                                :options='references.branches'
                            ></chosen>
                        </td>
                        <td>@lang('core::label.terminal'):</td>
                        <td>
                            <chosen
                                name='terminal'
                                class="chosen__terminal"
                                v-model='filter.transaction.terminal'
                                :options='references.terminals'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='paginate()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.terminal')</table-header>
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.or.si.no')</table-header>
            <table-header>@lang('core::label.cashier')</table-header>
            <table-header>@lang('core::label.department')</table-header>
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.product')</table-header>
            <table-header>@lang('core::label.total.qty')</table-header>
            <table-header>@lang('core::label.o.price')</table-header>
            <table-header>@lang('core::label.price')</table-header>
            <table-header>@lang('core::label.discount.price')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header>@lang('core::label.discount.amount')</table-header>
            <table-header>@lang('core::label.discount.percent')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.terminal_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--datetime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.or_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.cashier_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.category_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.product_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.oprice }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discounted_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.discounted_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.percentage_discount }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Reports/Pos/discounted-summary.js') }}"></script>
@stop