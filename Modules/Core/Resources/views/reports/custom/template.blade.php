@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', $template->name)

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
        <template slot="right">
            <control-button icon="fa fa-file-excel-o" @click="excel({{$template->id}})">
                @lang('core::label.export.excel')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-on:reload="paginate"
        v-bind:table="datatable.value">
        <template slot="header">
            <table-header
                :style="'min-width: ' + reference.format.excel.columns[key].width * 4 + 'px; max-width: ' + reference.format.excel.columns[key].width * 4 + 'px; text-align: ' + reference.format.excel.columns[key].textAlign"
                v-show='columnSettings.selections[key]'
                v-for="(column, key) in reference.format.columns">@{{column.header_name}}</table-header>
        </template>
        <template slot="display" scope="row">
            <td
                class="v-dataTable__tableCell"
                :style="'min-width: ' + reference.format.excel.columns[key].width * 4 + 'px; max-width: ' + reference.format.excel.columns[key].width * 4 + 'px; text-align: ' + reference.format.excel.columns[key].textAlign"
                v-show='columnSettings.selections[key]'
                v-for="(column, key) in reference.format.columns">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data[key] }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        v-bind:columns='columnSettings.columns'
        v-bind:id='columnSettings.id'
        v-show="columnSettings.visible"
        v-model='columnSettings.selections'
        @close="columnSettings.visible = false">
    </column-select>
@stop

@section('script')
    @javascript('template', $template)
    @javascript('filters', $filters)
    @javascript('slug', Request::segment(3))
    <script src="{{ elixir('js/Reports/Custom/template.js') }}"></script>
@stop