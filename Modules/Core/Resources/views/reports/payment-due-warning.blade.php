@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.payment.due.warning'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td>
                            <date-time-picker
                                id="date-from"
                                name='date-from'
                                v-model='filter.transaction.date_from'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td>
                            <date-time-picker
                                id="date-to"
                                name='date-to'
                                v-model='filter.transaction.date_to'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                name="transaction-branch"
                                v-model="filter.transaction.branch"
                                :options="references.branches"
                            ></chosen>
                        </td>
                        <td>@lang('core::label.supplier'):</td>
                        <td>
                            <chosen
                                name="transaction-supplier"
                                v-model="filter.transaction.supplier"
                                :options="references.suppliers"
                            ></chosen>
                        </td>
                        <td>
                            <input type='checkbox' v-model='filter.all'>
                            @lang('core::label.all.transactions')
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='search()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
    </control-bar>
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.supplier')</table-header>
            <table-header>@lang('core::label.sheet.number')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header>@lang('core::label.remaining')</table-header>
            <table-header>@lang('core::label.term')</table-header>
            <table-header>@lang('core::label.days.overdue')</table-header>
            <table-header>@lang('core::label.due.date')</table-header>
            <table-header>@lang('core::label.memo')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--datetime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                <table-cell-data>
                    <template scope="content">
                        <a href="javascript:void(0)" @click="goToPurchaseInbound(row.data.id)">@{{ row.data.sheet_number }}</a>
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remaining }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.term }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.days_overdue }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--datetime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.due_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.no.of.transactions'):</td>
                <td><b><span>@{{ summary.noOfTransactions }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.amount'):</td>
                <td><b><span>@{{ summary.totalAmount }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.remaining'):</td>
                <td><b><span>@{{ summary.totalRemaining }}</span></b></td>
            </tr>
        </table>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('suppliers', $suppliers)

    <script src="{{ elixir('js/Reports/payment-due-warning.js') }}"></script>
@stop