@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.bank.transaction.report'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td>
                            <date-time-picker
                                id="date-from"
                                name='date-from'
                                v-model='filter.transaction.date_from'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td>
                            <date-time-picker
                                id="date-to"
                                name='date-to'
                                v-model='filter.transaction.date_to'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td width="100px">
                            <chosen
                                name='branches'
                                v-model='filter.transaction.branch'
                                :options='references.branches'
                            ></chosen>
                        </td>
                        <td>@lang('core::label.bank.account'):</td>
                        <td width="100px">
                            <chosen
                                name='bank-account'
                                v-model='filter.transaction.bank_account'
                                :options='references.bankAccounts'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='search()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value">
        <template slot="header">
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.particular')</table-header>
            <table-header>@lang('core::label.in')</table-header>
            <table-header>@lang('core::label.out')</table-header>
            <table-header>@lang('core::label.balance')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.particular }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.in }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.out }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.balance }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.in'):</td>
                <td align="right"><b><span>@{{ totalIn }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.out'):</td>
                <td align="right"><b><span>@{{ totalOut }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total'):</td>
                <td align="right"><b><span>@{{ total }}</span></b></td>
            </tr>
        </table>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('bankAccounts', $bankAccounts)

    <script src="{{ elixir('js/Reports/bank-transaction.js') }}"></script>
@stop