@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.supplier.balance'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value">
        <template slot="header">
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.particular')</table-header>
            <table-header>@lang('core::label.in')</table-header>
            <table-header>@lang('core::label.out')</table-header>
            <table-header>@lang('core::label.balance')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.particular }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.in }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.out }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.balance }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td colspan="2">
                        <b>
                            <span class="c-tx-red">
                                @lang('core::label.total') = @lang('core::label.total.IN') - @lang('core::label.total.OUT')
                            </span>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.IN'):</td>
                    <td align="right"><b><span>@{{ totalIn }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.OUT'):</td>
                    <td align="right"><b><span>@{{ totalOut }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total'):</td>
                    <td align="right"><b><span>@{{ total }}</span></b></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('suppliers', $suppliers)

    <script src="{{ elixir('js/Reports/Supplier/balance.js') }}"></script>
@stop