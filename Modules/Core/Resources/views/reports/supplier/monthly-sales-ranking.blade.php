@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.supplier.monthly.sales.ranking'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate">
        <template slot="header">
            <table-header v-show='columnSettings.selections.code'>@lang('core::label.code')</table-header>
            <table-header>@lang('core::label.supplier')</table-header>
            <table-header v-show='columnSettings.selections.total_qty'>@lang('core::label.total.qty')</table-header>
            <table-header v-show='columnSettings.selections.total_amount'>@lang('core::label.total.amount')</table-header>
            <table-header v-show='columnSettings.selections.total_profit'>@lang('core::label.total.profit')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code" v-show='columnSettings.selections.code'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier_code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier_full_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.selections.total_qty'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_amount'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.selections.total_profit'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_profit }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::modal.monthly-sales-ranking-filter', [
        'show' => 'modal.filter.visible',
        'options' => 'modal.filter.options',
        'model' => 'modal.filter.data',
        'search' => 'paginate'
    ])

    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'>
    </column-select>
@stop

@section('script')
    @stack('script')

    <script src="{{ elixir('js/Reports/Supplier/monthly-sales-ranking.js') }}"></script>
@stop