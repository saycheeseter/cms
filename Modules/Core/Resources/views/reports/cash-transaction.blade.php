@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container')
@section('title', trans('core::label.cash.transaction'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td>
                            <date-time-picker
                                id="date-from"
                                name='date-from'
                                v-model='filter.transaction.date_from'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.to'):</td>
                        <td>
                            <date-time-picker
                                id="date-to"
                                name='date-to'
                                v-model='filter.transaction.date_to'
                                :date="{ format: 'yyyy-MM-dd' }">
                            </date-time-picker>
                        </td>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                name='location'
                                v-model='filter.transaction.branch'
                                :options='references.branches'
                            ></chosen>
                        </td>
                        <td>
                            <button class="c-btn c-dk-blue" @click='search()'>
                                @lang('core::label.search')
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
    </control-bar>
    <datatable
            v-bind:tableId="datatable.id"
            v-bind:settings="datatable.settings"
            v-bind:table="datatable.value"
            v-on:reload="search">
        <template slot="header">
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.particular')</table-header>
            <table-header>@lang('core::label.in')</table-header>
            <table-header>@lang('core::label.out')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.particular }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.in }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.out }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="goToPage(row.data.type, row.data.reference_id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
    <div class="c-content-group c-tb c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.in'):</td>
                <td align="right"><b><span>@{{ totalIn }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.out'):</td>
                <td align="right"><b><span>@{{ totalOut }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total'):</td>
                <td align="right"><b><span>@{{ total }}</span></b></td>
            </tr>
        </table>
    </div>
@stop

@section('modals')
@stop

@section('script')
    @javascript('branches', $branches)
    <script src="{{ elixir('js/Reports/cash-transaction.js') }}"></script>
@stop