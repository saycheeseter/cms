@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])

@section('container-1')
    <control-bar>
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='filter-by-product' icon="fa fa-search"  @click="productPicker.visible = true">
                @lang('core::label.filter.by.product')
            </control-button>
        </template>
    </control-bar>
    @yield('table')
@stop

@section('modals')
    @include('core::modal.product-report-selector')

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="paginate">
    </filter-modal>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @stack('script')
@stop
