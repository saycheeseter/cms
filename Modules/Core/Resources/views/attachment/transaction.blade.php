<control-dropdown class="attachments" icon="fa fa-file">
    {{-- @lang('core::label.attachments') --}}
    <template slot="list">
        <div class="upload-btn" v-if="!info.is_deleted">
            <label for="upload-photo" >
                <i class="fa fa-plus"></i>
                @lang('core::label.upload.more.files')
            </label>
            <input
                type="file"
                id="upload-photo"
                name="attachments[]"
                @change="filesChange($event.target.files);"
            />
        </div>
        <ul class="file-list">
            <li v-for="(attachment, index) in attachment.files.attached" class="clearfix">
                <div class="pull-left file-name"><a :href="attachment['link']"  target="_blank">@{{ attachment['original'] }}</a></div>
                <div class="pull-right" v-if="!info.is_deleted"><i class="fa fa-trash fa-lg" @click="detachFiles(attachment, index)" ></i></div>
            </li>
            <li v-for="(attachment, index) in attachment.files.temporary" class="clearfix">
                <div class="pull-left file-name"><a :href="attachment.link"  target="_blank">@{{ attachment.original }}</a></div>
                <div class="pull-right" v-if="!info.is_deleted"><i class="fa fa-trash fa-lg" @click="detachFiles(attachment, index)" ></i></div>
            </li>
    </template>
</control-dropdown>