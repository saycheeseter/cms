@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.translation.manager'))

@section('stylesheet')
    <style type="text/css">
    </style>
@stop

@section('container-1')
    <div class="c-content-group flex flex-column flex-1">
        <div class="c-content-group c-tb c-lr c-has-bt">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.search'):</td>
                    <td><input type="text"></td>
                    <td><button class="c-btn c-dk-blue">@lang('core::label.search')</button></td>
                </tr>
            </table>
        </div>
        <control-bar id="controls">
            <template slot="left">
                <control-button icon="fa fa-plus">
                    @lang('core::label.add.new.language')
                </control-button>
            </template>
        </control-bar>
        <datatable
            v-bind:tableId="datatable.id"
            v-bind:settings="datatable.settings"
            v-bind:table="datatable.value"
            v-on:reload="paginate"
            v-on:destroy="confirm"
            v-on:update="validate"
            v-on:add="validate">
            <template slot="header">
                <table-header>@lang('core::label.language.key')</table-header>
                <table-header>@lang('core::label.english')</table-header>
                <table-header>@lang('core::label.chinese')</table-header>
                <table-header></table-header>
                <table-header></table-header>
            </template>
            <template slot="display" scope="row">
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <table-cell-data>
                        <template scope="content">
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <table-cell-data>
                        <template scope="content">
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <table-cell-data>
                        <template scope="content">
                        </template>
                    </table-cell-data>
                </td>
            </template>
            <template slot="input">
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <input v-focus type="text"/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <input type="text"/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <input type="text"/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <img src="{{ asset('images/iconedit.png') }}">
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <img src="{{ asset('images/icondelete.png') }}">
                </table-input>
            </template>
        </datatable>
    </div>
@stop

@section('message')
@stop

@section('script')
    <script src="{{ elixir('js/TranslationManager/list.js') }}"></script>
@stop