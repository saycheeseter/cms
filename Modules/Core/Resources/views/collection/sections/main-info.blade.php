<div class="c-content-group c-tb c-lr">
    <div class="c-content-group clearfix">
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.date.time'):</td>
                    <td>
                        <date-time-picker
                            id='main-info-datetimepicker'
                            name='date-time'
                            v-model='info.transaction_date'
                            :date='datetimepicker.date'
                            :disabled='true'
                            :left='true'>
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.sheet.no'):</td>
                    <td><b><span>@{{ info.sheet_number }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.for.branch'):</td>
                    <td><chosen
                        name='created-for'
                        v-model='info.created_for'
                        :options='references.branches'
                        :disabled='disabled'
                        @change='notify()'>
                    </chosen></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>
                        <label class="radio-chkbx">
                            <input type="radio" name='customer-type' v-model='info.customer_type' :value='regularCustomer' @change='notify("customer_type")' :disabled='disabled'>
                            @lang('core::label.regular.customer')
                        </label>
                        <label class="radio-chkbx c-mgl-5">
                            <input type="radio" name='customer-type' v-model='info.customer_type' :value='walkInCustomer' @change='notify("customer_type")' :disabled='disabled'>
                            @lang('core::label.walk.in.customer')
                        </label>
                    </td>
                </tr>
            </table>
            <table class="c-tbl-layout">
                <tr>
                    <td v-if='isRegularCustomer'>
                        @lang('core::label.customer'):
                    </td>
                    <td v-if='isWalkInCustomer'>
                        @lang('core::label.walk.in.name'):
                    </td>
                    <td>
                        <chosen
                            class="required"
                            name='customer'
                            v-model='info.customer'
                            :options='references.selectedCustomerType'
                            :disabled='disabled'
                            @change='notify("customer")'
                            v-show='!isApprovedOrDeclined || isRegularCustomer'>
                        </chosen>
                        <span v-show='isApprovedOrDeclined && isWalkInCustomer'>
                            @{{ info.customer }}
                        </span>
                    </td>
                    {{--<td v-show='isRegularCustomer'>@lang('core::label.actual.customer.balance'):</td>--}}
                    {{--<td v-show='isRegularCustomer'>@{{ accountBalance }}</td>--}}
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.remarks'):</td>
                    <td><textarea v-model='info.remarks' rows="3" :disabled='disabled'></textarea></td>
                    {{--<td valign="top" v-show='isRegularCustomer'>@lang('core::label.remaining.customer.balance'):</td>--}}
                    {{--<td valign="top" v-show='isRegularCustomer'>@{{ actualAccountBalance }}</td>--}}
                </tr>
            </table>
        </div>
    </div>
</div>
<div id='status-label' class="label status-label" :class='approvalClass'>@{{ info.approval_status_label }}</div>