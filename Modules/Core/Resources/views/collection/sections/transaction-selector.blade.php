@component('core::components.selector')
    @slot('message')
        <div class="c-content-group c-b">
            <message type="danger" :show="message.selector.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.selector.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.selector.success != ''">
                <span v-text="message.selector.success"></span>
            </message>
            <message type="info" :show="message.selector.info != ''">
                <span v-text="message.selector.info"></span>
            </message>
        </div>
    @endslot
    @slot('controls')
        <control-button name='add' icon="fa fa-plus" @click="{{ $callback ?? 'addTransactions' }}">
            @lang('core::label.add')
        </control-button>
        <control-button name='add-continue' icon="fa fa-plus" @click="{{ ($callback ?? 'addTransactions').'(true)' }}">
            @lang('core::label.add.and.continue')
        </control-button>
        <control-button
            name='column-settings'
            icon="fa fa-gear"
            @click="{{ $columnSettings['visible'] ?? 'selector.transaction.columnSettings.visible = true' }}">
            @lang('core::label.column.settings')
        </control-button>
    @endslot
    @slot('headers')
        <table-header v-show='selector.transaction.columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
        <table-header>@lang('core::label.sheet.no')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.for_location'>@lang('core::label.for.location')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.customer'>@lang('core::label.customer')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.customer_detail'>@lang('core::label.customer_detail')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.salesman'>@lang('core::label.salesman')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.requested_by'>@lang('core::label.request.by')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.created_by'>@lang('core::label.created.by')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.created_date'>@lang('core::label.created.date')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.audited_by'>@lang('core::label.audit.by')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.audited_date'>@lang('core::label.audit.date')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.deleted'>@lang('core::label.deleted')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.approval_status'>@lang('core::label.approval.status')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.reference'>@lang('core::label.reference')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.amount'>@lang('core::label.amount')</table-header>
        <table-header v-show='selector.transaction.columnSettings.selections.remaining_amount'>@lang('core::label.remaining.amount')</table-header>
    @endslot
    @slot('display')
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.transaction.columnSettings.selections.transaction_date' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='selector.transaction.columnSettings.selections.created_for' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_for }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='selector.transaction.columnSettings.selections.for_location' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.for_location }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='selector.transaction.columnSettings.selections.remarks' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remarks }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='selector.transaction.columnSettings.selections.customer' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.customer }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='selector.transaction.columnSettings.selections.customer_detail' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.customer_detail }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='selector.transaction.columnSettings.selections.salesman' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.salesman }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--requestedBy" v-show='selector.transaction.columnSettings.selections.requested_by' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.requested_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" v-show='selector.transaction.columnSettings.selections.created_by' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.transaction.columnSettings.selections.created_date' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.created_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--auditedBy" v-show='selector.transaction.columnSettings.selections.audited_by' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.audited_by }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='selector.transaction.columnSettings.selections.audited_date' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.audited_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--deleted" v-show='selector.transaction.columnSettings.selections.deleted' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.deleted }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='selector.transaction.columnSettings.selections.approval_status' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.approval_status_label }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--reference" v-show='selector.transaction.columnSettings.selections.reference' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.reference.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='selector.transaction.columnSettings.selections.amount' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.amount }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='selector.transaction.columnSettings.selections.remaining_amount' @click="selectRow(row.data, 'selector-transaction-table')">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remaining_amount }}
                </template>
            </table-cell-data>
        </td>
    @endslot
@endcomponent