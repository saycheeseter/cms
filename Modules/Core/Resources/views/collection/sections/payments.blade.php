<datatable
    v-bind:tableId="datatable.payments.settings.id"
    ref='payments'
    v-bind:settings="datatable.payments.settings"
    v-bind:table="datatable.payments.value"
    v-on:update='updatePayment'
    v-on:destroy="confirmRemovePayment">
    <template slot="header">
        <table-header>@lang('core::label.method')</table-header>
        <table-header>@lang('core::label.bank.acct')</table-header>
        <table-header>@lang('core::label.check.no')</table-header>
        <table-header>@lang('core::label.check.date')</table-header>
        <table-header>@lang('core::label.amount')</table-header>
        <table-header v-show='!disabled'></table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--method">
            <chosen
                name='payment-method'
                v-model='row.data.payment_method'
                :options='references.paymentMethods'
                :disabled='disabled'
                @change="changeState(row.index, 'update', 'payments')"
            ></chosen>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name">
            <chosen
                name='bank-account'
                v-model='row.data.bank_account_id'
                :options='references.bankAccounts'
                v-show='isCheckOrBankDepositPayment(row.data.payment_method)'
                :disabled='disabled'
                @change="changeState(row.index, 'update', 'payments')"
            ></chosen>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--checkNo">
            <input type="text" v-model='row.data.check_no' v-show='isCheckPayment(row.data.payment_method)' @keypress="changeState(row.index, 'update', 'payments')" :disabled='disabled'>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
            <datepicker
                :id="'check-date-' + row.index"
                v-model='row.data.check_date' 
                v-show='isCheckPayment(row.data.payment_method)'
                :format='datetimepicker.date.format'
                v-on:selected="changeState(row.index, 'update', 'payments')"
            ></datepicker>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <number-input
                class="required"
                v-model="row.data.amount"
                :disabled='disabled'
                :unsigned="false"
                @keydown.native.enter="updatePayment(row.data, row.index)"
                @input="changeState(row.index, 'update', 'payments')">
            </number-input>
        </td>
    </template>
    <template slot="input">
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--method" v-show='!disabled'>
            <chosen
                name='payment-method'
                v-model='datatable.payments.input.payment_method'
                :options='references.paymentMethods'>
            </chosen>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name" v-show='!disabled'>
            <chosen
                name='bank-account'
                v-model='datatable.payments.input.bank_account_id'
                :options='references.bankAccounts'
                v-show='isCheckOrBankDepositPayment(datatable.payments.input.payment_method)'>
            </chosen>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--checkNo" v-show='!disabled'>
            <input type="text" v-model='datatable.payments.input.check_no' v-show='isCheckPayment(datatable.payments.input.payment_method)'>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='!disabled'>
            <datepicker
                id="check-date"
                v-model='datatable.payments.input.check_date' 
                v-show='isCheckPayment(datatable.payments.input.payment_method)'
                :format='datetimepicker.date.format'
            ></datepicker>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='!disabled'>
            <number-input
                class="required"
                ref="amountInput"
                v-model="datatable.payments.input.amount"
                :unsigned="false"
                @keydown.native.enter="storePayment()">
            </number-input>
        </table-input>
        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action" v-show='!disabled'>
            <img src="{{ asset('images/iconupdate.png') }}" @click="storePayment">
        </table-input>
    </template>
</datatable>