@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.collection.list'))

@section('container-1')
    <control-bar>
        <template slot="left">
            @if ($permissions['create'])
                <control-button icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.collection')
                </control-button>
            @endif
            <control-button icon="fa fa-search" @click="search">
                @lang('core::label.filters')
            </control-button>
            @if ($permissions['column_settings'])
                <control-button icon="fa fa-gear" @click="columnSettings">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
        </template>
        <template slot="right">
            @if($permissions['approve'])
                <control-button name='approve' icon="fa fa-check fa-green" @click="approveTransaction()">
                    @lang('core::label.approve')
                </control-button>
            @endif

            @if($permissions['decline'])
                <control-button name='decline' icon="fa fa-close fa-red" @click="declineTransaction()">
                    @lang('core::label.decline')
                </control-button>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-model="datatable.selected"
        v-on:reload="paginate"
        @destroy='confirmation'>
        <template slot="header">
            <table-header v-show='modal.columnSettings.selections.approval_status'>@lang('core::label.approval.status')</table-header>
            <table-header v-show='modal.columnSettings.selections.date'>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header v-show='modal.columnSettings.selections.location'>@lang('core::label.location')</table-header>
            <table-header v-show='modal.columnSettings.selections.customer'>@lang('core::label.customer')</table-header>
            <table-header v-show='modal.columnSettings.selections.customer_type'>@lang('core::label.customer.type')</table-header>
            <table-header v-show='modal.columnSettings.selections.amount'>@lang('core::label.amount')</table-header>
            <table-header v-show='modal.columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='modal.columnSettings.selections.deleted'>@lang('core::label.deleted')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" v-show='modal.columnSettings.selections.approval_status' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.approval_status_label }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.date' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='modal.columnSettings.selections.location' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='modal.columnSettings.selections.customer' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type" v-show='modal.columnSettings.selections.customer_type' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='modal.columnSettings.selections.amount' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='modal.columnSettings.selections.remarks' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--deleted" v-show='modal.columnSettings.selections.deleted' @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.deleted }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
    <column-select
        v-bind:columns='modal.columnSettings.columns'
        v-bind:id='modal.columnSettings.id'
        v-show="modal.columnSettings.visible"
        v-model='modal.columnSettings.selections'
        @close="modal.columnSettings.visible = false"
        :module='modal.columnSettings.module'>
    </column-select>

    @component('core::components.confirm', [
        'id' => 'modal.confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.invoice'),
        'content' => trans('core::confirm.delete.invoice')
    ])
    @endcomponent
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('customers', $customers)
    @javascript('permissions', $permissions)
    @javascript('segment', Request::segment(1))
    <script src="{{ elixir('js/Collection/list.js') }}"></script>
@stop