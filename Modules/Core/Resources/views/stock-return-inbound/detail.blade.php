@extends('core::inventory-chain.detail')
@section('title', trans('core::label.stock.return.inbound.detail'))
@section('transaction-type-label', trans('core::label.from.outbound'))
@section('transaction-label', trans('core::label.outbound'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr v-show='fromInvoice'>
                <td>@lang('core::label.delivery.from'):</td>
                <td>
                    <b><span>@{{ info.delivery_from_label }}</span></b>
                </td>
            </tr>
            <tr v-show='fromInvoice'>
                <td>@lang('core::label.delivery.from.location'):</td>
                <td>
                    <b><span>@{{ info.delivery_from_location_label }}</span></b>
                </td>
            </tr>
        </table>
    </div>
@stop

@push('script')
    <script src="{{ elixir('js/StockReturnInbound/detail.js') }}"></script>
@endpush
