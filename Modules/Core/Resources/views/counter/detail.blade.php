@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.counter.detail'))

@section('stylesheet')
@stop

@section('container-1')
    @include('core::counter.sections.detail.main-info')
    @include('core::counter.sections.detail.controls')
    @include('core::counter.sections.detail.details')
    @include('core::counter.sections.detail.summary')
@stop

@section('message')
    <message type="danger" :show="message.transaction.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.transaction.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.transaction.success != ''">
        <span v-text="message.transaction.success"></span>
    </message>
    <message type="info" :show="message.transaction.info != ''">
        <span v-text="message.transaction.info"></span>
    </message>
@stop

@section('modals')
    @include('core::collection.sections.transaction-selector')

    <dialog-box
        :show='dialog.message !=""'
        v-on:close='redirect'
    >
        <template slot='content'>@{{ dialog.message }}</template>
    </dialog-box>

    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.deleteTransactions.visible',
        'callback' => 'removeTransaction',
        'header' => trans('core::label.remove.transaction'),
        'content' => trans('core::confirm.remove.transaction')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'warning',
        'show' => 'confirm.warning.visible',
        'callback' => 'removeAllTransactions',
        'header' => trans('core::label.warning'),
        'cancel' => 'revertInfo',
        'content' => trans('core::confirm.changing.will.remove.collections')
    ])
    @endcomponent
@stop

@section('message')
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('customers', $customers)
    @javascript('permissions', $permissions)
    @javascript('id', Request::segment(2))

    <script src="{{ elixir('js/Counter/detail.js') }}"></script>
@stop