@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.counter.list'))

@section('stylesheet')
@stop

@section('container-1')
    <control-bar class="c-content-group">
        <template slot="left">
            @if($permissions['create'])
            <control-button icon="fa fa-plus" @click="create">
                @lang('core::label.add.new.counter')
            </control-button>
            @endif
            <control-button icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            @if($permissions['column_settings'])
                <control-button icon="fa fa-gear" @click="modal.columnSettings.visible = true">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
        </template>
        <template slot="right">
            @if($permissions['export'])
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a> 
                        </li>
                        @foreach ($templates as $template)
                            <li>
                                <a @click="excel( {{ $template->id }} )" >
                                    {{ $template->name }} 
                                </a> 
                            </li>
                        @endforeach
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        @destroy="confirmation">
        <template slot="header">
            <table-header v-show='modal.columnSettings.selections.transaction_date'>@lang('core::label.transaction.date')</table-header>
            <table-header>@lang('core::label.sheet.no')</table-header>
            <table-header v-show='modal.columnSettings.selections.created_from'>@lang('core::label.created.from')</table-header>
            <table-header v-show='modal.columnSettings.selections.created_for'>@lang('core::label.created.for')</table-header>
            <table-header v-show='modal.columnSettings.selections.customer'>@lang('core::label.customer')</table-header>
            <table-header v-show='modal.columnSettings.selections.total_amount'>@lang('core::label.total.amount')</table-header>
            <table-header v-show='modal.columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header v-show='modal.columnSettings.selections.created_by'>@lang('core::label.created.by')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='modal.columnSettings.selections.transaction_date' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sheet_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='modal.columnSettings.selections.created_from' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location" v-show='modal.columnSettings.selections.created_for' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='modal.columnSettings.selections.customer' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.customer }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='modal.columnSettings.selections.total_amount' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.total_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='modal.columnSettings.selections.remarks' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" v-show='modal.columnSettings.selections.created_by' @click='redirect(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        v-show="modal.columnSettings.visible"
        @close="modal.columnSettings.visible = false"
        v-bind:columns='modal.columnSettings.columns'
        v-model="modal.columnSettings.selections"
        :module='modal.columnSettings.module'>
    </column-select>

    @component('core::components.confirm', [
        'id' => 'warning',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.warning'),
        'content' => trans('core::confirm.delete.counter')
    ])
    @endcomponent
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('customers', $customers)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Counter/list.js') }}"></script>
@stop