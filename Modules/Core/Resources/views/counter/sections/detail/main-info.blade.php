<div class="c-content-group c-tb c-lr c-has-bt">
    <div class="c-content-group clearfix">
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.date.time'):</td>
                    <td>
                        <date-time-picker
                            id='main-info-datetimepicker'
                            name='date-time'
                            v-model='info.transaction_date'
                            :date="{ format: 'yyyy-MM-dd' }"
                            :disabled='true'
                            :left='true'>
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td width="76px">@lang('core::label.sheet.no'):</td>
                    <td><span><b>@{{ info.sheet_number }}</b></span></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="c-content-group clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.created.for'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='created-for'
                            :options='references.branches'
                            :disabled='true'
                            v-model='info.created_for'
                            @change='notify()'
                        ></chosen>
                    </td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.remarks'):</td>
                    <td><textarea rows="3" v-model='info.remarks'></textarea></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.customer'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='customer'
                            :options='references.customers'
                            v-model='info.customer_id'
                            @change='notify()'
                        ></chosen>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>