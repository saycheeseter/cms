<datatable
    v-bind:tableId="datatable.transactions.settings.id"
    v-bind:settings="datatable.transactions.settings"
    v-bind:table="datatable.transactions.value"
    v-on:destroy="confirmRemoveTransaction">
    <template slot="header">
        <table-header>@lang('core::label.transaction.date')</table-header>
        <table-header>@lang('core::label.sheet.no')</table-header>
        <table-header>@lang('core::label.remaining')</table-header>
        <table-header></table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remaining_amount }}
                </template>
            </table-cell-data>
        </td>
    </template>
</datatable>