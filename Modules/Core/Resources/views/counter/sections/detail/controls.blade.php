<control-bar class="c-content-group">
    <template slot="left">
        <control-button icon="fa fa-money" @click="openTransactionSelector">
            @lang('core::label.transaction.selector')
        </control-button>
    </template>
    <template slot="right">
        @if($templates->count() > 0 && $permissions['print'])
            <control-dropdown name='print' icon="fa fa-print" v-if="exists">
                @lang('core::label.print')
                <template slot="list">
                    @foreach($templates as $printout)
                        <li><a href="javascript:void();" @click="print({{ $printout->id }})">{{ $printout->name }}</a></li>
                    @endforeach
                </template>
            </control-dropdown>
        @endif
        <control-button icon="fa fa-save" @click="process">
            @lang('core::label.save')
        </control-button>
    </template>
</control-bar>