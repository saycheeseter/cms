<div class="c-content-group c-tb c-lr">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.remaining'):</td>
                <td><b><span>@{{ totalSelectedRemainingAmount }}</span></b></td>
            </tr>
        </table>
    </div>
</div>