@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.data.collector.transaction.list'))

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.date.from'):</td>
                <td>
                    <datepicker
                        id="date-to"
                        name='date-to'
                        :date="{ format: 'yyyy-MM-dd' }"
                        v-model='filters.dateFrom'
                    ></datepicker>
                </td>
                <td>@lang('core::label.date.to'):</td>
                <td>
                    <datepicker
                        id="date-from"
                        name='date-from'
                        :date="{ format: 'yyyy-MM-dd' }"
                        v-model='filters.dateTo'
                    ></datepicker>
                </td>
                <td><input class="c-btn c-dk-blue" type="button" value="@lang('core::label.search')" @click='paginate'></td>
            </tr>
            <tr>
                <td>@lang('core::label.transaction.type'):</td>
                <td><chosen
                    v-model='filters.transactionType'
                    :options='references.transactionType'
                ></chosen></td>
            </tr>
        </table>
    </div>
    <control-bar id="controls">
        <template slot="left">
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.settings.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload='paginate'
        v-on:destroy='confirm'>
        <template slot="header">
            <table-header v-show='columnSettings.selections.importDate'>@lang('core::label.import.date')</table-header>
            <table-header>@lang('core::label.reference.no')</table-header>
            <table-header v-show='columnSettings.selections.fileSize'>@lang('core::label.file.size')</table-header>
            <table-header v-show='columnSettings.selections.totalCount'>@lang('core::label.total.count')</table-header>
            <table-header v-show='columnSettings.selections.transactionType'>@lang('core::label.transaction.type')</table-header>
            <table-header v-show='columnSettings.selections.source'>@lang('core::label.source')</table-header>
            <table-header v-show='columnSettings.selections.importedBy'>@lang('core::label.imported.by')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td v-show='columnSettings.selections.importDate' class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.import_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reference_no }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.fileSize' class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.filesize }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.totalCount' class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.row_count }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.transactionType' class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_type }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.source' class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.source }}
                    </template>
                </table-cell-data>
            </td>
            <td v-show='columnSettings.selections.importedBy' class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.imported_by }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>

    @component('core::components.confirm', [
        'id' => 'delete-import-data-collector-transaction',
        'show' => 'confirmation.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.data.collector.transaction'),
        'content' => trans('core::confirm.delete.data.collector.transaction')
    ])
    @endcomponent
@stop

@section('script')
    <script src="{{ elixir('js/DataCollector/transaction-list.js') }}"></script>
@stop