@extends('core::other-financial-flow.list', [
    'referenceNameLabel' => trans('core::label.received.from'),
    'addNewLabel' => trans('core::label.add.new.income')
])

@section('title', trans('core::label.other.income.list'))

@push('modals')
    @component('core::components.confirm', [
        'id' => 'confirm-list',
        'show' => 'modal.list.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.other.income'),
        'content' => trans('core::confirm.delete.other.income')
    ])
    @endcomponent
    
    @include('core::other-financial-flow.detail', [
        'detailTitle' => trans('core::label.other.income.detail'),
        'referenceNameLabel' => trans('core::label.received.from')
    ])

    @component('core::components.confirm', [
        'id' => 'confirm-detail',
        'show' => 'modal.detail.confirm.visible',
        'callback' => 'destroyDetail',
        'header' => trans('core::label.delete.other.income'),
        'content' => trans('core::confirm.delete.other.income')
    ])
    @endcomponent
@endpush

@push('script')
    <script src="{{ elixir('js/OtherIncome/list.js') }}"></script>
@endpush