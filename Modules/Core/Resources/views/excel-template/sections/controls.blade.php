<control-bar id="controls" class="c-has-bb">
    <template slot="right">
        <control-button name='save' icon="fa fa-save" @click='process()'>
            @lang('core::label.save')
        </control-button>
        <control-button  v-show="headDisable" name='preview' icon="fa fa-eye" @click='preview()'>
            @lang('core::label.preview')
        </control-button>
    </template>
</control-bar>