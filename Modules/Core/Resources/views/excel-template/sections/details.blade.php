<div class="c-sub-nav-content">
    <div class="c-sub-sidebar">
        <div class="c-sub-header c-has-bb">
           @lang('core::label.header.style')
        </div>
        <div class="c-content-group c-tb c-lr">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.text.align'):</td>
                    <td>
                        <select v-model="template.format.header.textAlign">
                            <option value="left">@lang('core::label.left')</option>
                            <option value="center">@lang('core::label.center')</option>
                            <option value="right">@lang('core::label.right')</option>
                        </select>
                    </td>
                    <td>
                        <label class="radio-chkbx">
                            <input type="checkbox" v-model="template.format.header.bold">
                            <span>@lang('core::label.bold')</span>
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="c-sub-header c-has-bt c-has-bb">
            <label class="radio-chkbx">
                <input type="checkbox" v-model="allColumns" @change="toggleAllColumns()">
                <span>@lang('core::label.table.column')</span>
            </label>
        </div>
        <div class="overflow flex-1">
            <excel-column v-for="(column, key) in template.format.columns" :name="column.header_name"  v-model="template.format.columns[key]"></excel-column>
        </div>
    </div>
    <div class="c-sub-main-content">
        <ul v-sortable="{onEnd: reorder}">
            <li class="sortable-content" v-for="sortable in sortables" :key="sortable.key">@{{ sortable.header_name }}</li>
        </ul>
    </div>
</div>