<div class="c-content-group c-tb c-lr c-has-bt" id='main-info'>
    <table class="c-tbl-layout">
        <tr>
            <td>@lang('core::label.name'):</td>
            <td><input type="text" class="required" v-model="template.info.name"></td>
            <td>@lang('core::label.module'):</td>
            <td>
                <chosen
                    class="required"
                    name='module'
                    v-model='template.info.module'
                    :options='reference.modules'
                    @change='moduleChange()'
                    :disabled='headDisable'
                ></chosen>
            </td>
        </tr>
    </table>
</div>