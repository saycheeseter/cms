@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.excel.template.detail'))

@section('stylesheet')
    <style type="text/css">
        .c-sub-sidebar {
            flex-basis: 400px;
            min-width: 400px;
        }
        .sortable-content {
            background: #E7EFF0;
            border: 1px #b8d0d6 solid;
            padding: 5px;
            margin: 5px 5px 0px 5px;
            font-family: H-Bold;
        }
        .sortable-content:hover {
            cursor: move;
        }
        .c-sub-main-content ul {
            padding-left: 0px;
            padding-bottom: 5px;
        }
        .c-sub-main-content ul li {
            list-style: none;
        }
    </style>
@stop

@section('container-1')
    @include('core::excel-template.sections.main-info')
    @include('core::excel-template.sections.controls')
    @include('core::excel-template.sections.details')
@stop

@section('modals')
    <dialog-box
        :show='dialog.message !=""'
        v-on:close='redirect'
    >
        <template slot='content'>@{{ dialog.message }}</template>
    </dialog-box>
@stop
@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/ExcelTemplate/detail.js') }}"></script>
@stop