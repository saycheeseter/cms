@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.excel.template.list'))

@section('stylesheet')
@stop

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            @if($permissions['create'])
                <control-button name="create" icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.template')
                </control-button>
            @endif
            <control-button name="filter" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm">
        <template slot="header">
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.module')</table-header>
            <table-header>@lang('core::label.created.by')</table-header>
            <table-header>@lang('core::label.created.date')</table-header>
            <table-header>@lang('core::label.modified.by')</table-header>
            <table-header>@lang('core::label.last.modified')</table-header>
            @if($permissions['delete'])
                <table-header></table-header>
            @endif
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.module }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--createdBy" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_at }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--modifiedBy" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.modified_by }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="view(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.updated_at }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('loading')
    @include('core::common.loading', [
        'show' => 'processing'
    ])
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible', 
        'callback' => 'destroy',
        'header' => Lang::get('core::label.delete.template'),
        'content' => Lang::get('core::confirm.delete.template')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('permissions', $permissions)
    <script src="{{ elixir('js/ExcelTemplate/list.js') }}"></script>
@stop
