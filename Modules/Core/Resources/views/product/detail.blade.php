@extends('core::layouts.master')
@extends('core::layouts.flexible.two-container')
@section('title', trans('core::label.product.detail'))

@section('stylesheet')
    <style type="text/css">
        .container-1 {
            flex: 0 0 auto;
        }
    </style>
@stop

@section('container-1')
    @include('core::product.sections.detail.main-info')
@stop

@section('container-2')
    @include('core::product.sections.detail.controls')

    <div class="c-nav-tabs-container flex flex-column flex-1 overflow">
        @include('core::product.tabs.form.advanced-info')
        @include('core::product.tabs.form.product-unit')
        @include('core::product.tabs.form.product-unit-barcode')
        @include('core::product.tabs.form.branch-price')
        @include('core::product.tabs.form.branch-info')
        @include('core::product.tabs.form.location')
        @include('core::product.tabs.form.alternative-items')
        @include('core::product.tabs.form.product-grouping')
        @include('core::product.tabs.form.product-image')
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close='message.reset()'>
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @include('core::product.sections.detail.modals')
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('uoms', $uoms)
    @javascript('branches', $branches)
    @javascript('locations', $locations)
    @javascript('suppliers', $suppliers)
    @javascript('taxes', $taxes)
    @javascript('brands', $brands)
    @javascript('categories', $categories)
    @javascript('pc', setting('default.unit'))
    @javascript('permissions', $permissions)

    @stack('script')

    <script src="{{ elixir('js/Product/detail.js') }}"></script>
@stop
