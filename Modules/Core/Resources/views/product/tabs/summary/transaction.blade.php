<div v-show="tabs.transactionSummary.active">
    <div class="c-content-group c-has-bb c-t">
        <control-bar>
            <template slot="left">
                <control-button name="transaction-summary-filters" icon="fa fa-search" @click="filter.transactionSummary.visible = true">
                    @lang('core::label.filters')
                </control-button>
            </template>
        </control-bar>
    </div>
    <div class="c-sub-nav-content">
        <div class="c-sub-sidebar">
            <table border="1" class="tbl-design">
                <tr>
                    <td>@lang('core::label.beginning')</td>
                    <td width="60px" align="right">@{{ transaction.summary.beginning }}</td>
                </tr>
                <tr>
                    <td>@lang('core::label.purchase.inbound')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.purchase_inbound, "@lang('core::label.purchase.inbound')")'>@{{ transaction.summary.purchase_inbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.purchase.return.outbound')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.purchase_return_outbound, "@lang('core::label.purchase.return.outbound')")'>@{{ transaction.summary.purchase_return_outbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.sales.outbound')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.sales_outbound, "@lang('core::label.sales.outbound')")'>@{{ transaction.summary.sales_outbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.sales.return.inbound')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.sales_return_inbound, "@lang('core::label.sales.return.inbound')")'>@{{ transaction.summary.sales_return_inbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.stock.delivery.inbound')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.stock_delivery_inbound, "@lang('core::label.stock.delivery.inbound')")'>@{{ transaction.summary.stock_delivery_inbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.stock.delivery.outbound')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.stock_delivery_outbound, "@lang('core::label.stock.delivery.outbound')")'>@{{ transaction.summary.stock_delivery_outbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.stock.return.inbound')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.stock_return_inbound, "@lang('core::label.stock.return.inbound')")'>@{{ transaction.summary.stock_return_inbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.adjustment.increase')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.adjust_increase, "@lang('core::label.adjustment.increase')")'>@{{ transaction.summary.adjustment_increase }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.stock.return.outbound')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.stock_return_outbound, "@lang('core::label.stock.return.outbound')")'>@{{ transaction.summary.stock_return_outbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.damage.outbound')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.damage_outbound, "@lang('core::label.damage.outbound')")'>@{{ transaction.summary.damage_outbound }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.adjustment.decrease')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.adjust_decrease, "@lang('core::label.adjustment.decrease')")'>@{{ transaction.summary.adjustment_decrease }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.product.conversion.increase')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.product_conversion_increase, "@lang('core::label.product.conversion.increase')")'>@{{ transaction.summary.product_conversion_increase }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.product.conversion.decrease')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.product_conversion_decrease, "@lang('core::label.product.conversion.decrease')")'>@{{ transaction.summary.product_conversion_decrease }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.auto.product.conversion.increase')</td>
                    <td align="right"><span class="link-green" @click='setTransactionType(service.auto_product_conversion_increase, "@lang('core::label.auto.product.conversion.increase')")'>@{{ transaction.summary.auto_product_conversion_increase }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.auto.product.conversion.decrease')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.auto_product_conversion_decrease, "@lang('core::label.auto.product.conversion.decrease')")'>@{{ transaction.summary.auto_product_conversion_decrease }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.pos.sales')</td>
                    <td align="right"><span class="link-red" @click='setTransactionType(service.pos_sales, "@lang('core::label.pos.sales')")'>@{{ transaction.summary.pos_sales }}</span></td>
                </tr>
                <tr>
                    <td>@lang('core::label.inventory')</td>
                    <td align="right">@{{ inventory }}</td>
                </tr>
            </table>
        </div>
        <div class="c-sub-main-content">
            <div class="c-sub-header c-has-bt empty-height">
                @{{ transaction.label }}
            </div>
            <datatable
                v-bind:tableId="transaction.datatable.id"
                v-bind:settings="transaction.datatable.settings"
                v-bind:table="transaction.datatable.value"
                v-on:reload="getTransactions">
                <template slot="header">
                    <table-header>@lang('core::label.date')</table-header>
                    <table-header>@lang('core::label.sheet.no')</table-header>
                    <table-header>@lang('core::label.particulars')</table-header>
                    <table-header>@lang('core::label.qty')</table-header>
                    <table-header>@lang('core::label.price')</table-header>
                    <table-header>@lang('core::label.amount')</table-header>
                    <table-header>@lang('core::label.prepared.by')</table-header>
                    <table-header>@lang('core::label.remarks')</table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.transaction_date }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                <span class="link-blue">@{{ row.data.sheet_number }}</span>
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.particulars }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.qty }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.price }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.amount }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.prepared_by }}
                            </template>
                        </table-cell-data>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click='showTransaction(row.data)'>
                        <table-cell-data>
                            <template scope="content">
                                @{{ row.data.remarks }}
                            </template>
                        </table-cell-data>
                    </td>
                </template>
            </datatable>
        </div>
    </div>
</div>