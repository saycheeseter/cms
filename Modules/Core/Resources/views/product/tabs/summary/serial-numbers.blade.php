<div class="c-nav-tabs-content" v-show="tabs.serialNumbers.active">
    <control-bar>
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="filter.serialNumber.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>

        @if(Auth::user()->hasAccessTo('export_excel_product_serial_number'))
            <template slot="right">
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="exportSerial()">
                                @lang('core::label.export.serial.number')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            </template>
        @endif
    </control-bar>
    <datatable
        :tableId="serialNumbers.datatable.id"
        :settings="serialNumbers.datatable.settings"
        :table="serialNumbers.datatable.value"
        @reload="getAvailableSerials">
        <template slot="header">
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.supplier')</table-header>
            <table-header>@lang('core::label.location')</table-header>
            <table-header>@lang('core::label.serial.number')</table-header>
            <table-header>@lang('core::label.expiration.date')</table-header>
            <table-header>@lang('core::label.manufacturing.date')</table-header>
            <table-header>@lang('core::label.admission.date')</table-header>
            <table-header>@lang('core::label.manufacturer.warranty.start')</table-header>
            <table-header>@lang('core::label.manufacturer.warranty.end')</table-header>
            <table-header>@lang('core::label.remarks')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.branch }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--serialNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.serial_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.expiration_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.manufacturing_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.admission_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.manufacturer_warranty_start }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.manufacturer_warranty_end }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.remarks }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
</div>