<div class="c-nav-tabs-content" v-show="tabs.inventoryStockCard.active">
    <control-bar>
        <template slot="left">
            <control-button name="stock-card-filters" icon="fa fa-search" @click="filter.stockCard.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="stockCard.columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="stockCard.datatable.id"
        v-bind:settings="stockCard.datatable.settings"
        v-bind:table="stockCard.datatable.value"
        v-on:reload="getStockCard">
        <template slot="header">
            <table-header>@lang('core::label.audit.date')</table-header>
            <table-header>@lang('core::label.created.for')</table-header>
            <table-header>@lang('core::label.for.location')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.sequenceNo'>@lang('core::label.sequence.no')</table-header>
            <table-header>@lang('core::label.transaction.type')</table-header>
            <table-header>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header>@lang('core::label.price')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.cost'>@lang('core::label.cost')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.preInventory'>@lang('core::label.pre.inventory')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.preAverageCost'>@lang('core::label.pre.average.cost')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.preAmount'>@lang('core::label.pre.amount')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.postInventory'>@lang('core::label.post.inventory')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.postAverageCost'>@lang('core::label.post.average.cost')</table-header>
            <table-header v-show='stockCard.columnSettings.selections.postAmount'>@lang('core::label.post.amount')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.audited_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.created_for }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.for_location }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number" v-show='stockCard.columnSettings.selections.sequenceNo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.sequence_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.reference_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.transaction_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.qty }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='stockCard.columnSettings.selections.cost'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.cost }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='stockCard.columnSettings.selections.preInventory'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.pre_inventory }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='stockCard.columnSettings.selections.preAverageCost'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.pre_avg_cost }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='stockCard.columnSettings.selections.preAmount'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.pre_amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='stockCard.columnSettings.selections.postInventory'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.post_inventory }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='stockCard.columnSettings.selections.postAverageCost'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.post_avg_cost }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='stockCard.columnSettings.selections.postAmount'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.post_amount }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
</div>