<div class="c-nav-tabs-content" v-show="tabs.barcode.active">
    <datatable
        tableid="unit-barcode-table"
        :settings="tables.barcodes.settings"
        :table="tables.barcodes.value"
        ref="barcodes"
        @destroy="confirmDeleteBarcode"
        @update="updateBarcode">
        <template slot="header">
            <table-header>@lang('core::label.uom')</table-header>
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.memo')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
                <chosen class="required" :options='units' v-model='row.data.uom_id' @change="changeState(row.dataIndex, 'update', 'barcodes')"></chosen>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <input type="text" class="required" v-model="row.data.code" @input="changeState(row.dataIndex, 'update', 'barcodes')"/>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <input type="text" v-model="row.data.memo" @input="changeState(row.dataIndex, 'update', 'barcodes')"/>
            </td>
        </template>
        <template slot="input">
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
                <chosen class="required" name='barcode-units' :options='units' v-model='tables.barcodes.fields.uom_id'></chosen>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <input v-model="tables.barcodes.fields.code" class="required" type="text"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <input v-model="tables.barcodes.fields.memo" type="text"/>
            </table-input>
            <table-input class="v-dataTable__tableCell">
                <img src="{{ asset('images/iconupdate.png') }}" @click="storeBarcode(tables.barcodes.fields.data())">
            </table-input>
        </template>
    </datatable>
</div>
