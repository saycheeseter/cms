<div class="c-nav-tabs-content" v-show="tabs.grouping.active">
    <div class="c-content-group c-b c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.grouping'):</td>
                <td><chosen :options="references.groupType" v-model="info.group_type" :disabled='info.type == references.type.COMPONENT || info.is_used == true' @change='notifyGroupType()'></chosen></td>
                <td v-if='info.is_used'><span class='c-tx-red'>*@lang('core::label.already.transacted')</span></td>
            </tr>
        </table>
    </div>
    <control-bar>
        <template slot="left">
            <control-button icon="fa fa-list-alt" @click="productPicker.visible = true" v-show='info.group_type != references.groupType[0].id'>
                @lang('core::label.product.selector')
            </control-button>
        </template>
    </control-bar>
    <datatable
        tableid="grouping-table"
        :settings="tables.components.settings"
        :table="tables.components.value"
        @destroy="confirmDeleteComponent"
        ref="components"
        :show='info.group_type != references.groupType[0].id'>
        <template slot="header">
            <table-header>@lang('core::label.stock.no')</table-header>
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <number-input v-model="row.data.qty"/>
            </td>
        </template>
    </datatable>
</div>