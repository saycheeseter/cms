<div class="c-nav-tabs-content" v-show="tabs.branch.active && isLoggedAsMainBranch">
    <datatable
        tableid="branch-info-table"
        :settings="tables.branches.settings"
        :table="tables.branches.value">
        <template slot="header">
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.active')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.branch_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <input type="checkbox" v-model="row.data.status" :disabled="isMainBranch(row.data.branch_id)"/>
            </td>
        </template>
    </datatable>
</div>
