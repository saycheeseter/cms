<div class="c-nav-tabs-content" v-show="tabs.advance.active">
    <div class="c-content-group c-lr">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.senior')</td>
                <td>
                    <select v-model="info.senior">
                        <option value="0">@lang('core::label.non.senior')</option>
                        <option value="1">@lang('core::label.senior.20%')</option>
                        <option value="2">@lang('core::label.senior.5%')</option>
                    </select>
                </td>
                <td>@lang('core::label.default.unit'):</td>
                <td>
                    <chosen
                        class="required"
                        :options='units'
                        v-model='info.default_unit'>
                    </chosen>
                </td>
                <td colspan="2">
                    <label class="radio-chkbx">
                        <input type="checkbox" v-model='info.manageable'>
                        <span>@lang('core::label.manageable')</span>
                    </label>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.chinese.name'):</td>
                <td><input type="text" v-model='info.chinese_name'></td>
                <td>@lang('core::label.supplier.sku'):</td>
                <td><input type="text" v-model='info.supplier_sku'></td>
                <td>@lang('core::label.vatable'):</td>
                <td>
                    <chosen
                        name='tax'
                        class="required"
                        :options='references.taxes'
                        :multiple='true'
                        v-model='info.taxes'>
                    </chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.management.type')</td>
                <td>
                    <select v-model="info.manage_type">
                        <option value="0">@lang('core::label.normal')</option>
                        <option value="1">@lang('core::label.serial.number')</option>
                        <option value="2">@lang('core::label.batch')</option>
                    </select>
                </td>
                <td>@lang('core::label.brand'):</td>
                <td class="c-width-150">
                    <chosen name='brand' class="required" :options='references.brands' v-model='info.brand_id'></chosen>
                </td>
                <td>@lang('core::label.category'):</td>
                <td class="c-width-150">
                    <chosen name='category' class="required" :options='references.categories' v-model='info.category_id'></chosen>
                </td>
            </tr>
            <tr>
                <td valign="top">@lang('core::label.description'):</td>
                <td><textarea rows="3" v-model='info.description'></textarea></td>
            </tr>
        </table>
    </div>
</div>