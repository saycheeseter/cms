<div class="c-nav-tabs-content" v-show="tabs.location.active">
    <datatable
        tableid="branch-detail-table"
        :settings="tables.locations.settings"
        :table="tables.locations.value">
        <template slot="header">
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.location')</table-header>
            <table-header>@lang('core::label.min')</table-header>
            <table-header>@lang('core::label.max')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.branch_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.branch_detail_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <number-input v-model="row.data.min"/>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <number-input v-model="row.data.max"/>
            </td>
        </template>
    </datatable>
</div>
