<div class="c-nav-tabs-content" v-show="tabs.alternativeItems.active">
    <control-bar>
        <template slot="left">
            <control-button icon="fa fa-list-alt" @click="productPicker.visible = true">
                @lang('core::label.product.selector')
            </control-button>
        </template>
    </control-bar>
    <datatable
        tableid="alternatives-table"
        :settings="tables.alternatives.settings"
        :table="tables.alternatives.value"
        @destroy="confirmDeleteAlternativeItem"
        ref="alternatives">
        <template slot="header">
            <table-header>@lang('core::label.barcode')</table-header>
            <table-header>@lang('core::label.stock.no')</table-header>
            <table-header>@lang('core::label.name')</table-header>
            <table-header>@lang('core::label.chinese.name')</table-header>
            <table-header>@lang('core::label.description')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.description }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
</div>