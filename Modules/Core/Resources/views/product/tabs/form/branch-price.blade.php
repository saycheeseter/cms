<div class="c-nav-tabs-content" v-show="tabs.price.active">
    <datatable
        tableid="branch-price-table"
        :settings="tables.prices.settings"
        :table="tables.prices.value"
        ref="prices">
        <template slot="header">
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.copy.from')</table-header>
            @if ($permissions['show_cost'])
                <table-header>@lang('core::label.purchase.price')</table-header>
            @endif
            <table-header v-if='info.id != 0'>@lang('core::label.apply') @lang('core::label.cost')</table-header>
            <table-header>@lang('core::label.wholesale')</table-header>
            <table-header>@lang('core::label.retail')</table-header>
            <table-header>@lang('core::label.price.a')</table-header>
            <table-header>@lang('core::label.price.b')</table-header>
            <table-header>@lang('core::label.price.c')</table-header>
            <table-header>@lang('core::label.price.d')</table-header>
            <table-header>@lang('core::label.price.e')</table-header>
            <table-header>@lang('core::label.editable')</table-header>
        </template>
        <template slot="display" scope="row">
            <template v-if="showPricesOfOtherBranches(row.data.branch_id)">
                <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data.branch_name }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--location">
                    <chosen
                        :options='getOtherBranches(row.data.branch_id)'
                        v-model='row.data.copy_from'
                        @change='copyPrices()'>
                    </chosen>
                </td>
                @if ($permissions['show_cost'])
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                        <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.purchase_price"></number-input>
                    </td>
                @endif
                <td class="v-dataTable__tableCell v-dataTable__tableCell--action" v-if='info.id != 0'>
                    <control-dropdown
                        class="datatable-dropdown c-dk-blue full-width"
                        icon="fa fa-calendar"
                        :ref="'purchaseprice' + row.data.branch_id">
                        @lang('core::label.apply')
                        <template slot="list">
                            <div class="c-content-group c-lr">
                                <table class="c-tbl-layout">
                                    <tr>
                                        <td>@lang('core::label.from'):</td>
                                        <td><datepicker class="date-center" v-model='cost.to' format="yyyy-MM-dd"></datepicker></td>
                                        <td>@lang('core::label.to'):</td>
                                        <td><datepicker class="date-center" v-model='cost.from' format="yyyy-MM-dd"></datepicker></td>
                                        <td>
                                            <button class="c-btn c-dk-green" @click='applyPurchasePriceToTransactions(row.data)'>
                                                @lang('core::label.save')
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </template>
                    </control-dropdown>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.wholesale_price"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.selling_price"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.price_a"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.price_b"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.price_c"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.price_d"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input @input="copyPrices()" :disabled="row.data.copy_from !== null" v-model="row.data.price_e"></number-input>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <input type="checkbox" v-model="row.data.editable"/>
                </td>
            </template>
        </template>
    </datatable>
</div>
