<div class="c-nav-tabs-content" v-show="tabs.unit.active">
    <datatable
        tableid="product-unit-table"
        :settings="tables.units.settings"
        :table="tables.units.value"
        ref="units"
        @destroy="confirmDeleteUnit"
        @update="updateUnit">
        <template slot="header">
            <table-header>@lang('core::label.uom')</table-header>
            <table-header>@lang('core::label.qty')</table-header>
            <table-header>@lang('core::label.length')</table-header>
            <table-header>@lang('core::label.width')</table-header>
            <table-header>@lang('core::label.height')</table-header>
            <table-header>@lang('core::label.weight')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
                <chosen 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    :options='isDefaultUnit(row.data.uom_id) ? references.uoms.initial : references.uoms.current' 
                    v-model='row.data.uom_id' 
                    @change="changeState(row.dataIndex, 'update', 'units')">
                </chosen>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <number-input 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    v-model="row.data.qty"
                    @input="changeState(row.dataIndex, 'update', 'units')">
                 </number-input>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    v-model="row.data.length"
                    @input="changeState(row.dataIndex, 'update', 'units')">
                </number-input>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    v-model="row.data.width"
                    @input="changeState(row.dataIndex, 'update', 'units')">
                </number-input>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    v-model="row.data.height"
                    @input="changeState(row.dataIndex, 'update', 'units')">
                </number-input>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input 
                    :disabled='isDefaultUnit(row.data.uom_id)' 
                    v-model="row.data.weight"
                    @input="changeState(row.dataIndex, 'update', 'units')">
                </number-input>
            </td>
        </template>
        <template slot="input">
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--uom">
                <chosen name='uom' :options='references.uoms.current' v-model='tables.units.fields.uom_id'></chosen>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                <number-input v-model="tables.units.fields.qty" />
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input v-model="tables.units.fields.length"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input v-model="tables.units.fields.width"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input v-model="tables.units.fields.height"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--measure">
                <number-input v-model="tables.units.fields.weight"/>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <img src="{{ asset('images/iconupdate.png') }}" @click="storeUnit(tables.units.fields.data())">
            </table-input>
        </template>
    </datatable>
</div>
