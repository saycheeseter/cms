<div class="c-nav-tabs-content" v-show="tabs.image.active">
    <div class="mask"></div>
    <div class="c-content-group c-b">
        <div class="uploaded-files">
            <ul>
                <li v-for="(attachment, index) in attachment.files.attached">
                    <div :class="orientation(attachment['attribute'])" @click="preview(attachment['link'], attachment['key'])">
                        <img :src="attachment['link']">
                    </div>
                    <i class="fa fa-trash" @click="detachFiles(attachment, index)"></i>
                </li>
                <li v-for="(attachment, index) in attachment.files.temporary">
                    <div :class="orientation(attachment.attribute)" @click="preview(attachment.link, attachment.key)">
                        <img :src="attachment.link">
                    </div>
                    <i class="fa fa-trash" @click="detachFiles(attachment, index)"></i>
                </li>
                <li class="upload-plus">
                    <label for="upload-photo">
                        <i class="fa fa-plus fa-3x"></i>
                        @lang('core::label.add.new.image')
                    </label>
                    <input
                        type="file"
                        name="attachments[]"
                        id="upload-photo"
                        accept="image/*"
                        @change="filesChange($event.target.files);"
                    />
                </li>
            </ul>
        </div>
    </div>
</div>