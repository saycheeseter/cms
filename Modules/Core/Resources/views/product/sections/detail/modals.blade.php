@component('core::components.confirm', [
    'id' => 'confirm-delete-unit',
    'show' => 'confirm.deleteUnit.visible',
    'callback' => 'deleteUnit',
    'header' => Lang::get('core::label.delete.product.unit'),
    'content' => Lang::get('core::confirm.delete.product.unit')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'confirm-delete-unit',
    'show' => 'confirm.deleteBarcode.visible',
    'callback' => 'deleteBarcode',
    'header' => Lang::get('core::label.delete.product.barcode'),
    'content' => Lang::get('core::confirm.delete.product.barcode')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'confirm-delete-alternative-item',
    'show' => 'confirm.deleteAlternativeItem.visible',
    'callback' => 'deleteAlternativeItem',
    'header' => Lang::get('core::label.delete.alternative.product'),
    'content' => Lang::get('core::confirm.delete.alternative.product')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'confirm-delete-component',
    'show' => 'confirm.deleteComponent.visible',
    'callback' => 'deleteComponent',
    'header' => Lang::get('core::label.delete.component'),
    'content' => Lang::get('core::confirm.delete.component')
])
@endcomponent

@component('core::components.confirm', [
    'id' => 'confirm-change-group-type',
    'show' => 'confirm.changeGroupType.visible',
    'callback' => 'changeGroupType',
    'cancel' => 'revertGroupType',
    'header' => Lang::get('core::label.group.type'),
    'content' => Lang::get('core::confirm.changing.will.remove.products')
])
@endcomponent

<confirm 
    id="price-abnormality-warning"
    :show="warning.priceAbnormality.visible"
    ok="@lang('core::label.ok')"
    cancel="@lang('core::label.cancel')"
    @ok="proceedOnPriceWarning"
    @cancel="warning.priceAbnormality.visible = false"
    @close="warning.priceAbnormality.visible = false">
    <template slot="header">
        @lang('core::confirm.continue.save.price')
    </template>
    <template slot="content">
        @lang('core::info.price.abnomarmality.on.branches')
    </template>
    <template slot="content">
        <li v-for="message in warning.priceAbnormality.messages">@{{ message }}</li>
    </template>
</confirm>

<modal
    id="image-preview-modal"
    v-show="attachment.modal.visible"
    @close="attachment.modal.visible = false">
    <template slot="header">
        @lang('core::label.preview')
    </template>
    <template slot="content">
        <img :src="attachment.modal.link" class="img-thumbnail">
    </template>
    <template slot="footer">
        <button type="button" class="c-btn c-dk-red" @click="attachment.modal.visible = false">@lang('core::label.close')</button>
    </template>
</modal>

@include('core::modal.product-transaction-selector')