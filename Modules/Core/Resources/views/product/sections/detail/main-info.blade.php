<div class="basic-info c-content-group c-tb c-lr clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.status'):</td>
                <td>
                    <select v-model="info.status" :disabled="!isNew && isComponent">
                        <option value="0">@lang('core::label.inactive')</option>
                        <option value="1">@lang('core::label.active')</option>
                        <option value="2">@lang('core::label.discontinued')</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.stock.no'):</td>
                <td><input id='stock-no' type="text" class="required" v-model='info.stock_no'></td>
                <td>@lang('core::label.supplier'):</td>
                <td>
                    <chosen name='supplier' :options='references.suppliers' class="required" v-model='info.supplier_id'></chosen>
                </td>
            </tr>
            <tr>
                <td valign="top">@lang('core::label.name'):</td>
                <td valign="top"><input id='product-name' type="text" class="required" v-model='info.name'></td>
                <td valign="top">@lang('core::label.product.memo'):</td>
                <td><textarea rows="3" v-model='info.memo'></textarea></td>
            </tr>
        </table>
    </div>
    <div class="pull-right">
        <table class="c-tbl-layout">
            <tr>
                <td align="right">@lang('core::label.created.by'):</td>
                <td>@{{ info.created }}</td>
            </tr>
            <tr>
                <td align="right">@lang('core::label.modified.by'):</td>
                <td>@{{ info.modified }}</td>
            </tr>
        </table>
    </div>
</div>