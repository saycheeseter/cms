<control-bar id="controls">
    <template slot="right">
        <control-button name='save-add-new' icon="fa fa-save" @click='process(false)'>
            @lang('core::label.save.and.add.new')
        </control-button>
        <control-button name='save' icon="fa fa-save" @click='process(true      )'>
            @lang('core::label.save')
        </control-button>
    </template>
</control-bar>
<div id='tabs' class="c-nav-tabs clearfix">
    <div class="c-nav-tab" v-for="(tab, key) in tabs.get()" 
        v-text="tab.label"
        :class="{ active: tab.active }"
        v-show="key === 'branch' ? isLoggedAsMainBranch : true"
        @click="tabs.active(key)">
    </div>
</div>