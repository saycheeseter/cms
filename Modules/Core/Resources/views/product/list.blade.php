@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.list'))

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.unit.barcode'):</td>
                <td><input type="text" v-model='filter.product.express.barcode' @keydown.enter="paginate(1)"></td>
                <td>@lang('core::label.stock.no'):</td>
                <td><input type="text" class="field__stockNo" v-model='filter.product.express.stock_no' @keydown.enter="paginate(1)"></td>
                <td>@lang('core::label.name'):</td>
                <td width="250px"><input type="text" class="field__name" v-model='filter.product.express.name' @keydown.enter="paginate(1)"></td>
                <td><input type="button" class="c-btn c-dk-green" value="@lang('core::label.search')" @click='paginate(1)'></td>
            </tr>
        </table>
    </div>
    <control-bar id='controls'>
        <template slot="left">
            @if ($permissions['create'])
                <control-button name='create' icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.product')
                </control-button>
            @endif
            @if ($permissions['column_settings'])
                <control-button name='columns' icon="fa fa-gear" @click="columnSettings.product.visible = true">
                    @lang('core::label.column.settings')
                </control-button>
            @endif
            <control-button name='filters' icon="fa fa-search" @click="filter.product.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
        <template slot="right">
            @if($permissions['import_excel'])
                @component('core::components.import-excel')
                @endcomponent
            @endif
            @if($permissions['export_excel'])
                <control-dropdown class="list" icon="fa fa-file-excel-o">
                    @lang('core::label.export.excel')
                    <template slot="list">
                        <li>
                            <a @click="excel()">
                                @lang('core::label.default.excel.template')
                            </a>
                        </li>
                    </template>
                </control-dropdown>
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirmation">
        <template slot="header">
            <table-header v-show='columnSettings.product.selections.barcode'>@lang('core::label.unit.barcode')</table-header>
            <table-header v-show='columnSettings.product.selections.stock_no'>@lang('core::label.stock.no')</table-header>
            <table-header>@lang('core::label.name')</table-header>
            <table-header v-show='columnSettings.product.selections.chinese_name'>@lang('core::label.chinese.name')</table-header>
            <table-header v-show='columnSettings.product.selections.description'>@lang('core::label.description')</table-header>
            <table-header v-show='columnSettings.product.selections.memo'>@lang('core::label.memo')</table-header>
            <table-header v-show='columnSettings.product.selections.brand'>@lang('core::label.brand')</table-header>
            <table-header v-show='columnSettings.product.selections.category'>@lang('core::label.category')</table-header>
            <table-header v-show='columnSettings.product.selections.supplier'>@lang('core::label.supplier')</table-header>
            <table-header v-show='columnSettings.product.selections.group_type_label'>@lang('core::label.group.type')</table-header>
            @if ($permissions['show_cost'])
                <table-header v-show='columnSettings.product.selections.purchase_price'>@lang('core::label.cost')</table-header>
            @endif
            <table-header v-show='columnSettings.product.selections.wholesale_price'>@lang('core::label.wholesale')</table-header>
            <table-header v-show='columnSettings.product.selections.selling_price'>@lang('core::label.retail')</table-header>
            <table-header v-show='columnSettings.product.selections.price_a'>@lang('core::label.price.a')</table-header>
            <table-header v-show='columnSettings.product.selections.price_b'>@lang('core::label.price.b')</table-header>
            <table-header v-show='columnSettings.product.selections.price_c'>@lang('core::label.price.c')</table-header>
            <table-header v-show='columnSettings.product.selections.price_d'>@lang('core::label.price.d')</table-header>
            <table-header v-show='columnSettings.product.selections.price_e'>@lang('core::label.price.e')</table-header>
            <table-header v-show='columnSettings.product.selections.inventory'>@lang('core::label.inventory')</table-header>
            <table-header>@lang('core::label.image')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" v-show='columnSettings.product.selections.barcode' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" v-show='columnSettings.product.selections.stock_no' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.stock_no }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.product.selections.chinese_name' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.product.selections.description' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.description }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.product.selections.memo' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" v-show='columnSettings.product.selections.brand' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.brand }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--category" v-show='columnSettings.product.selections.category' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.category }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--supplier" v-show='columnSettings.product.selections.supplier' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.supplier }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--groupType" v-show='columnSettings.product.selections.group_type_label' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.group_type_label }}
                    </template>
                </table-cell-data>
            </td>
            @if ($permissions['show_cost'])
                <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.purchase_price' @click="edit(row.data.id)">
                    <table-cell-data>
                        <template scope="content">
                            @{{ row.data.purchase_price }}
                        </template>
                    </table-cell-data>
                </td>
            @endif
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.wholesale_price' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.wholesale_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.selling_price' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.selling_price }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.price_a' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price_a }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.price_b' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price_b }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.price_c' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price_c }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.price_d' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price_d }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" v-show='columnSettings.product.selections.price_e' @click="edit(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.price_e }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" v-show='columnSettings.product.selections.inventory' @click="report(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        <span class="link-blue">
                            @{{ row.data.inventory }}
                        </span>
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--action" @click="previewImages(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        <i class="fa fa-picture-o fa-lg"></i>
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close='message.reset()'>
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.visible',
        'callback' => 'destroy',
        'header' => Lang::get('core::label.delete.product'),
        'content' => Lang::get('core::confirm.delete.product')
    ])
    @endcomponent

    @include('core::modal.product-filter', [
        'show' => 'filter.product.visible',
        'options' => 'filter.product.options',
        'model' => 'filter.product.data',
        'search' => 'paginate'
    ])

    <column-select
        :columns='columnSettings.product.columns'
        v-show='columnSettings.product.visible'
        @close="columnSettings.product.visible = false"
        id = 'column-modal'
        v-model='columnSettings.product.selections'
        :module='columnSettings.product.module.list'>
    </column-select>

    @stack('modals')

    <modal
        id='product-image-modal'
        v-show='modal.images.visible'
        @close="modal.images.visible = false">
        <template slot="header">
            @lang('core::label.product.images')
        </template>
        <template slot="content">
            <carousel
                :images = "modal.images.content"
                :visible = "modal.images.visible"
            ></carousel>
        </template>
    </modal>
@stop

@section('script')
    @stack('script')

    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Product/list.js') }}"></script>
@append