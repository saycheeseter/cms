@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.product.summary'))

@section('stylesheet')
    <style type="text/css">
        .c-sub-sidebar {
            flex-basis: 300px;
            min-width: 300px;
            border: none;
        }
        .c-sub-sidebar table {
            width: 100%;
        }
        .c-sub-main-content {
            border-top: none;
            border-bottom: none;
            border-left: 1px #ccc solid;
            border-right: 1px #ccc solid;
        }
    </style>
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.name'):</td>
                    <td><b><span>{{ $product->name }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.stock.no'):</td>
                    <td><b><span>{{ $product->stock_no }}</span></b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-15">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.chinese.name'):</td>
                    <td><b><span>{{ $product->chinese_name }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.barcode'):</td>
                    <td><b><span>{{ $product->presenter()->barcodes }}</span></b></td>
                </tr>
            </table>
        </div>
    </div>
    <div id='tabs' class="c-nav-tabs clearfix">
        <div class="c-nav-tab" v-for="(tab, key) in tabs.get()" 
            v-text="tab.label" 
            :class="{ active: tab.active }" 
            @click="tabs.active(key)">
        </div>
    </div>
    <div class="c-nav-tabs-container flex flex-column flex-1 overflow">
        @include('core::product.tabs.summary.transaction')
        @include('core::product.tabs.summary.inventory-stock-card')
        @include('core::product.tabs.summary.serial-numbers')
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        id="transaction-filter-form"
        v-show="filter.transactionSummary.visible"
        @close="filter.transactionSummary.visible = false"
        :filters="filter.transactionSummary.options"
        v-model="filter.transactionSummary.data"
        v-on:search='getTransactionSummary'>
    </filter-modal>

    <filter-modal
        id="stock-card-filter-form"
        v-show="filter.stockCard.visible"
        @close="filter.stockCard.visible = false"
        :filters="filter.stockCard.options"
        v-model="filter.stockCard.data"
        v-on:search='getStockCard'>
    </filter-modal>

    <filter-modal
        id="serial-filter-form"
        v-show="filter.serialNumber.visible"
        @close="filter.serialNumber.visible = false"
        :filters="filter.serialNumber.options"
        v-model="filter.serialNumber.data"
        @search='getAvailableSerials'>
    </filter-modal>

    <column-select
        :columns='stockCard.columnSettings.columns'
        v-show='stockCard.columnSettings.visible'
        @close="stockCard.columnSettings.visible = false"
        id = 'column-modal'
        v-model='stockCard.columnSettings.selections'
        :module='stockCard.columnSettings.module'>
    </column-select>
@stop

@section('script')
    @javascript('branches', $branches)
    @javascript('suppliers', $suppliers)
    @javascript('id', Request::segment(2))

    <script src="{{ elixir('js/Product/summary.js') }}"></script>
@stop