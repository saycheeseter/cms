@extends('core::modal.product-selector')

@section('selector-controls')
    <control-button name='add' icon="fa fa-plus" @click="add(false)">
        @lang('core::label.add')
    </control-button>
    <control-button name='add-continue' icon="fa fa-plus" @click="add">
        @lang('core::label.add.and.continue')
    </control-button>
@stop