<modal id="product-selector" v-show="productPicker.visible" @close="productPicker.visible = false">
    <template slot="header">@lang('core::label.product.selector')</template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="message.product.error.any()" @close="message.product.reset()">
                <ul>
                    <li v-for="(value, key) in message.product.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.product.success != ''">
                <span v-text="message.product.success"></span>
            </message>
            <message type="info" :show="message.product.info != ''">
                <span v-text="message.product.info"></span>
            </message>
        </div>
        <div class="c-content-group c-tb">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.barcode'):</td>
                    <td><input type="text" name="" v-model='filter.product.express.barcode' @keydown.enter='productSearch(1)'></td>
                    <td>@lang('core::label.stock.no'):</td>
                    <td><input type="text" class="field__stockNo" name="" v-model='filter.product.express.stock_no' @keydown.enter='productSearch(1)'></td>
                    <td>@lang('core::label.product.name'):</td>
                    <td><input type="text" class="field__name" name="" v-model='filter.product.express.name' @keydown.enter='productSearch(1)'></td>
                    <td><input type="button" class="c-btn c-dk-green" value="@lang('core::label.search')" @click='productSearch(1)'></td>
                </tr>
            </table>
        </div>
        <div class="c-content-group clearfix flex">
            <div class="flex flex-column flex-1 c-has-bl c-has-br datatable overflow-hidden">
                <control-bar id='product-selector-controls'>
                    <template slot="left">
                        @yield('selector-controls')

                        @if(isset($permissions['column_settings']) && $permissions['column_settings'])
                            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.product.visible = true">
                                @lang('core::label.column.settings')
                            </control-button>
                        @endif

                        <control-button name='search' icon="fa fa-search" @click="filter.product.visible = true">
                            @lang('core::label.search')
                        </control-button>
                    </template>
                </control-bar>
                <datatable
                    v-bind:tableId="datatable.product.id"
                    v-bind:settings="datatable.product.settings"
                    v-bind:table="datatable.product.value"
                    v-model="datatable.product.checked"
                    v-on:reload="productSearch"
                    ref="product-list">
                    <template slot="header">
                        <table-header v-show='columnSettings.product.selections.barcode'>@lang('core::label.barcode')</table-header>
                        <table-header v-show='columnSettings.product.selections.stock_no'>@lang('core::label.stock.no')</table-header>
                        <table-header>@lang('core::label.product.name')</table-header>
                        <table-header v-show='columnSettings.product.selections.chinese_name'>@lang('core::label.chinese.name')</table-header>
                        <table-header v-show='columnSettings.product.selections.description'>@lang('core::label.description')</table-header>
                        <table-header v-show='columnSettings.product.selections.memo'>@lang('core::label.memo')</table-header>
                        <table-header v-show='columnSettings.product.selections.brand'>@lang('core::label.brand')</table-header>
                        <table-header v-show='columnSettings.product.selections.category'>@lang('core::label.category')</table-header>
                        <table-header v-show='columnSettings.product.selections.supplier'>@lang('core::label.supplier')</table-header>
                        <table-header v-show='columnSettings.product.selections.group_type_label'>@lang('core::label.group.type')</table-header>
                        @if( (isset($permission['show_cost_product'])) ? $permission['show_cost_product'] : true )
                            <table-header v-show='columnSettings.product.selections.purchase_price'>@lang('core::label.cost')</table-header>
                        @endif
                        <table-header v-show='columnSettings.product.selections.wholesale_price'>@lang('core::label.wholesale')</table-header>
                        <table-header v-show='columnSettings.product.selections.selling_price'>@lang('core::label.retail')</table-header>
                        <table-header v-show='columnSettings.product.selections.price_a'>@lang('core::label.price.a')</table-header>
                        <table-header v-show='columnSettings.product.selections.price_b'>@lang('core::label.price.b')</table-header>
                        <table-header v-show='columnSettings.product.selections.price_c'>@lang('core::label.price.c')</table-header>
                        <table-header v-show='columnSettings.product.selections.price_d'>@lang('core::label.price.d')</table-header>
                        <table-header v-show='columnSettings.product.selections.price_e'>@lang('core::label.price.e')</table-header>
                        <table-header v-show='columnSettings.product.selections.inventory'>@lang('core::label.inventory')</table-header>
                    </template>
                    <template slot="display" scope="row">
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--barcode" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.barcode'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.barcode }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--stockNo" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.stock_no'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.stock_no }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" >
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.name }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.chinese_name'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.chinese_name }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.description'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.description }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.memo'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.memo }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.brand'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.brand }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.category'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.category }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.supplier'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.supplier }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.group_type_label'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.group_type_label }}
                                </template>
                            </table-cell-data>
                        </td>
                        @if( (isset($permission['show_cost_product'])) ? $permission['show_cost_product'] : true )
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.purchase_price'>
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.purchase_price }}
                                    </template>
                                </table-cell-data>
                            </td>
                        @endif
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.wholesale_price'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.wholesale_price }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.selling_price'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.selling_price }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.price_a'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.price_a }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.price_b'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.price_b }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.price_c'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.price_c }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.price_d'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.price_d }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.price_e'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.price_e }}
                                </template>
                            </table-cell-data>
                        </td>
                        <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" @click="selectRow(row.data, 'product-list')" v-show='columnSettings.product.selections.inventory'>
                            <table-cell-data>
                                <template scope="content">
                                    @{{ row.data.inventory }}
                                </template>
                            </table-cell-data>
                        </td>
                    </template>
                </datatable>
            </div>
        </div>
    </template>
</modal>

@include('core::modal.product-filter', [
    'show' => 'filter.product.visible',
    'options' => 'filter.product.options',
    'model' => 'filter.product.data',
    'search' => 'productSearch'
])

<column-select
    :columns='columnSettings.product.columns'
    v-model='columnSettings.product.selections'
    v-on:close='columnSettings.product.visible = false'
    v-show='columnSettings.product.visible'
    :module='columnSettings.product.module.selector'
    id='product-column-modal'>
</column-select>