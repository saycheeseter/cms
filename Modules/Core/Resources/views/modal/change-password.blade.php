<modal id="change-password" v-show="modal.changePassword.visible" @close="modal.changePassword.visible = false">
    <template slot="header">@lang('core::label.change.password')</template>
    <template slot="content">
        <div class="c-content-group c-b">
            <message type="danger" :show="modal.changePassword.message.error.any()" @close="modal.changePassword.message.reset()">
                <ul>
                    <li v-for="(value, key) in modal.changePassword.message.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="modal.changePassword.message.success != ''">
                <span v-text="modal.changePassword.message.success"></span>
            </message>
        </div>
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.old.password'):</td>
                <td><input type="password" v-model='modal.changePassword.credentials.old'></td>
            </tr>
            <tr>
                <td>@lang('core::label.new.password'):</td>
                <td><input type="password" placeholder="@lang('core::info.change.password.min.length')" v-model='modal.changePassword.credentials.new'></td>
            </tr>
            <tr>
                <td>@lang('core::label.confirm.password'):</td>
                <td><input type="password" placeholder="@lang('core::info.change.password.min.length')" v-model='modal.changePassword.credentials.new_confirmation'></td>
            </tr>
        </table>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" @click='changePassword()'>@lang('core::label.save')</button>
    </template>
</modal>