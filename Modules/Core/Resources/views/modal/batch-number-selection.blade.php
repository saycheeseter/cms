<modal id="batch-number-selection" v-show="itemManagementSelection.batch.visible" @close="closeBatchSelection()">
    <template slot="header">@lang('core::label.batch.number.selection')</template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="itemManagementSelection.batch.message.error.any()" @close="itemManagementSelection.batch.message.reset()">
                <ul>
                    <li v-for="(value, key) in itemManagementSelection.batch.message.error.get()" v-text="value"></li>
                </ul>
            </message>
        </div>
        <div class="c-content-group c-b">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.find'):</td>
                    <td><input type="text" ref="batchSelectionSearchString" @keydown.enter="searchBatchSelectionByString"></td>
                </tr>
            </table>
        </div>
        <div class="flex flex-column flex-1">
            <div class="flex flex-1">
                <div class="flex flex-column flex-1 c-has-bl c-has-br datatable">
                    <div class="c-sub-header c-has-bt">
                        @lang('core::label.available.batches')
                    </div>
                    <datatable
                        v-bind:tableid="datatable.batch.list.id"
                        v-bind:settings="datatable.batch.list.settings"
                        v-bind:table="datatable.batch.list.value"
                        v-bind:ref="datatable.batch.list.id"
                        :searchondata="{
                            key: 'name',
                            value: itemManagementSelection.batch.searchString
                        }"
                        v-model="datatable.batch.list.value.selected">
                        <template slot="header">
                            <table-header>@lang('core::label.batch')</table-header>
                            <table-header>@lang('core::label.available.qty')</table-header>
                            <table-header>@lang('core::label.remaining.qty')</table-header>
                            <table-header></table-header>
                        </template>
                        <template slot="display" scope="row">
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, datatable.batch.list.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.name }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" @click="selectRow(row.data, datatable.batch.list.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.qty }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty" @click="selectRow(row.data, datatable.batch.list.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.remaining_qty }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                                <number-input v-model="row.data.get_qty" @blur="validateBatchGetQty(row.data)" :disabled="disabled" :unsigned="true"></number-input>
                            </td>
                        </template>
                    </datatable>
                </div>
                <div class="action-buttons">
                    <button class="btn btn-default" @click="putBatches">
                        <i class="fa fa-chevron-right"></i>
                    </button>
                    <button class="btn btn-default" @click="returnBatches">
                        <i class="fa fa-chevron-left"></i>
                    </button>
                </div>
                <div class="flex flex-column flex-1 c-has-bl c-has-br datatable">
                    <div class="c-sub-header c-has-bt">
                        @lang('core::label.selected.batches')
                    </div>
                    <datatable
                        v-bind:tableid="datatable.batch.selected.id"
                        v-bind:settings="datatable.batch.selected.settings"
                        v-bind:table="datatable.batch.selected.value"
                        v-bind:ref="datatable.batch.selected.id"
                        v-model="datatable.batch.selected.value.selected">
                        <template slot="header">
                            <table-header>@lang('core::label.batch')</table-header>
                            <table-header>@lang('core::label.selected.qty')</table-header>
                        </template>
                        <template slot="display" scope="row">
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="selectRow(row.data, datatable.batch.selected.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.name }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.qty }}
                                    </template>
                                </table-cell-data>
                            </td>
                        </template>
                    </datatable>
                </div>
            </div>
        </div>
        <div class="c-content-group c-t">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.available.quantity'):</td>
                    <td><b><span>@{{ availableBatchQty }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.selected'):</td>
                    <td><b><span>@{{ selectedBatchCount }}</span></b></td>
                </tr>
            </table>
        </div>
    </template>
    <template slot="footer">
        <input type="button" name="proceed" class="c-btn c-dk-green" value="@lang('core::label.proceed')" @click="itemManagementSelection.batch.visible = false"/>
    </template>
</modal>