<modal id="serial-number-selection" v-show="itemManagementSelection.serial.visible" @close="closeSerialSelection()">
    <template slot="header">@lang('core::label.serial.number.selection')</template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="itemManagementSelection.serial.message.error.any()" @close="itemManagementSelection.serial.message.reset()">
                <ul>
                    <li v-for="(value, key) in itemManagementSelection.serial.message.error.get()" v-text="value"></li>
                </ul>
            </message>
        </div>
        <div class="c-content-group c-b">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.find'):</td>
                    <td><input type="text" ref="serialSelectionSearchString" @keydown.enter="searchSerialSelectionByString"></td>
                    <td>@lang('core::label.scan'):</td>
                    <td><input type="text" ref="serialSelectionScan" @keydown.enter="scanSerialSelection"></td>
                </tr>
            </table>
        </div>
        <div class="flex flex-column flex-1">
            <div class="flex flex-1">
                <div class="flex flex-column flex-1 c-has-bb c-has-bl c-has-br datatable">
                    <div class="c-sub-header c-has-bt">
                        @lang('core::label.available.serial.numbers')
                    </div>
                    <datatable
                        :tableid="datatable.serial.list.id"
                        :settings="datatable.serial.list.settings"
                        :table="datatable.serial.list.value"
                        :searchondata="{
                            key: 'serial_number',
                            value: itemManagementSelection.serial.searchString
                        }"
                        :ref="datatable.serial.list.id"
                        v-model="datatable.serial.list.value.selected">
                        <template slot="header">
                            <table-header>@lang('core::label.serial.number')</table-header>
                            <table-header>@lang('core::label.remarks')</table-header>
                        </template>
                        <template slot="display" scope="row">
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--serialNo" @click="selectRow(row.data, datatable.serial.list.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.serial_number }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="selectRow(row.data, datatable.serial.list.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.remarks }}
                                    </template>
                                </table-cell-data>
                            </td>
                        </template>
                    </datatable>
                </div>
                <div class="action-buttons">
                    <button class="btn btn-default" @click="putSerials">
                        <i class="fa fa-angle-right fa-lg"></i>
                    </button>
                    <button class="btn btn-default" @click="returnSerials">
                        <i class="fa fa-angle-left fa-lg"></i>
                    </button>
                    <button class="btn btn-default" @click='putAllSerials'>
                        <i class="fa fa-angle-double-right fa-lg"></i>
                    </button>
                    <button class="btn btn-default" @click='returnAllSerials'>
                        <i class="fa fa-angle-double-left fa-lg"></i>
                    </button>
                </div>
                <div class="flex flex-column flex-1 c-has-bb c-has-bl c-has-br datatable">
                    <div class="c-sub-header c-has-bt">
                        @lang('core::label.selected.serial.numbers')
                    </div>
                    <datatable
                        :tableid="datatable.serial.selected.id"
                        :settings="datatable.serial.selected.settings"
                        :table="datatable.serial.selected.value"
                        :ref="datatable.serial.selected.id"
                        v-model="datatable.serial.selected.value.selected">
                        <template slot="header">
                            <table-header>@lang('core::label.serial.number')</table-header>
                            <table-header>@lang('core::label.remarks')</table-header>
                        </template>
                        <template slot="display" scope="row">
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--serialNo" @click="selectRow(row.data, datatable.serial.selected.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.serial_number }}
                                    </template>
                                </table-cell-data>
                            </td>
                            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="selectRow(row.data, datatable.serial.selected.id)">
                                <table-cell-data>
                                    <template scope="content">
                                        @{{ row.data.remarks }}
                                    </template>
                                </table-cell-data>
                            </td>
                        </template>
                    </datatable>
                </div>
            </div>
        </div>
        <div class="c-content-group c-t">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.available.quantity'):</td>
                    <td><b><span>@{{ availableSerialQty }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.selected'):</td>
                    <td><b><span>@{{ selectedSerialCount }}</span></b></td>
                </tr>
            </table>
        </div>
    </template>
    <template slot="footer">
        <input type="button" name="proceed" class="c-btn c-dk-green" value="@lang('core::label.proceed')" @click="itemManagementSelection.serial.visible = false"/>
    </template>
</modal>