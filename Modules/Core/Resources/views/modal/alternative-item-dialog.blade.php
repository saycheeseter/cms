<dialog-box
    id="alternative-item-dialog"
    :show="dialog.alternativeItems.content.length > 0"
    @close='dialog.alternativeItems.content = []'>
    <template slot='content'>
        @lang('core::info.current.product.might.reach.negative.inv')
        <div class="c-content-group c-tb">
            <table class="table-design full-width">
                <thead>
                    <th>@lang('core::label.barcode')</th>
                    <th>@lang('core::label.product.name')</th>
                    <th>@lang('core::label.chinese.name')</th>
                    <th>@lang('core::label.inventory')</th>
                </thead>
                <tbody>
                    <tr v-for="product in dialog.alternativeItems.content" @click="replaceItemWithAlternative(product.id)">
                        <td>@{{ product.barcode }}</td>
                        <td>@{{ product.name }}</td>
                        <td>@{{ product.chinese_name }}</td>
                        <td>@{{ product.inventory }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </template>
</dialog-box>