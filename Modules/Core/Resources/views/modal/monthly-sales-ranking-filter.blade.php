<filter-modal
    v-show="{{ $show }}"
    @close="{{ $show }} = false"
    :filters="{{ $options }}"
    v-model='{{ $model }}'
    v-on:search="{{ $search }}">
</filter-modal>

@prepend('script')
    @javascript('locations', $locations)
    @javascript('branches', $branches)
    @javascript('brands', $brands)
    @javascript('categories', $categories)
    @javascript('suppliers', $suppliers)
@endprepend