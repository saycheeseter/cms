<modal id="help-desk" v-show="modal.helpDesk.visible" @close="modal.helpDesk.visible = false">
    <template slot="header">@lang('core::label.help.desk')</template>
    <template slot="content">
        <iframe :src="modal.helpDesk.url"></iframe>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-red" @click='modal.helpDesk.visible = false'>@lang('core::label.close')</button>
    </template>
</modal>