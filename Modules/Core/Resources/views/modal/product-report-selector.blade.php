@extends('core::modal.product-selector')

@section('selector-controls')
    <control-button name='apply' icon="fa fa-plus" @click="filterByProduct()">
        @lang('core::label.apply')
    </control-button>
@stop