<modal id="serial-number-form" v-show="itemManagement.serial.visible" @close="closeSerialForm()">
    <template slot="header">@lang('core::label.serial.number.assigning')</template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="itemManagement.serial.message.error.any()" @close="itemManagement.serial.message.reset()">
                <ul>
                    <li v-for="(value, key) in itemManagement.serial.message.error.get()" v-text="value"></li>
                </ul>
            </message>
        </div>
        <div class="c-content-group c-b c-lr">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.scan'):</td>
                    <td><input type="text" ref="serialFormScan" @keydown.enter="storeScannedSerial"></td>
                </tr>
            </table>
        </div>
        <div class="flex flex-column flex-1 c-has-bl c-has-br overflow-hidden">
            <div class="c-sub-header c-has-bt">
                @lang('core::label.selected.serial.numbers')
            </div>
            <datatable
                :tableId="datatable.serial.form.id"
                :settings="datatable.serial.form.settings"
                :table="datatable.serial.form.value"
                :ref="datatable.serial.form.id"
                @destroy="confirmDeleteSerial"
                @update="updateSerial">
                <template slot="header">
                    <table-header>@lang('core::label.serial.number')</table-header>
                    <table-header>@lang('core::label.expiration.date')</table-header>
                    <table-header>@lang('core::label.manufacturing.date')</table-header>
                    <table-header>@lang('core::label.admission.date')</table-header>
                    <table-header>@lang('core::label.manufacturer.warranty.start')</table-header>
                    <table-header>@lang('core::label.manufacturer.warranty.end')</table-header>
                    <table-header>@lang('core::label.remarks')</table-header>
                    <table-header v-show="!disabled" ></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--serialNo">
                        <input :disabled="disabled" type="text" v-model="row.data.serial_number" @keydown="changeState(row.index, 'update', datatable.serial.form.id)"/>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'expiration-date-' + row.index" :disabled-picker="disabled" class="date-from" format="yyyy-MM-dd" v-model='row.data.expiration_date' @selected="changeState(row.index, 'update', datatable.serial.form.id)"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'manufacturing-date-' + row.index" :disabled-picker="disabled" class="date-from" format="yyyy-MM-dd" v-model='row.data.manufacturing_date' @selected="changeState(row.index, 'update', datatable.serial.form.id)"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'admission-date-' + row.index" :disabled-picker="disabled" class="date-from" format="yyyy-MM-dd" v-model='row.data.admission_date' @selected="changeState(row.index, 'update', datatable.serial.form.id)"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'manufacturer-warranty-start-' + row.index" :disabled-picker="disabled" class="date-from" format="yyyy-MM-dd" v-model='row.data.manufacturer_warranty_start' @selected="changeState(row.index, 'update', datatable.serial.form.id)"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker :id="'manufacturer-warranty-end-' + row.index" :disabled-picker="disabled" class="date-from" format="yyyy-MM-dd" v-model='row.data.manufacturer_warranty_end' @selected="changeState(row.index, 'update', datatable.serial.form.id)"></datepicker>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                        <input :disabled="disabled" type="text" v-model="row.data.remarks" @keydown="changeState(row.index, 'update', datatable.serial.form.id)">
                    </td>
                </template>
                <template slot="input" v-if="!disabled">
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--serialNo">
                        <input v-model="datatable.serial.form.fields.serial_number" @keydown.enter="storeSerial()" type="text" ref="serialNumberFormFirstField"/>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker id="expiration-date" class="date-from" format="yyyy-MM-dd" v-model='datatable.serial.form.fields.expiration_date'></datepicker>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker id="manufacturing-date" class="date-from" format="yyyy-MM-dd" v-model='datatable.serial.form.fields.manufacturing_date'></datepicker>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker id="admission-date" class="date-from" format="yyyy-MM-dd" v-model='datatable.serial.form.fields.admission_date'></datepicker>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker id="manufacturer-warranty-start" class="date-from" format="yyyy-MM-dd" v-model='datatable.serial.form.fields.manufacturer_warranty_start'></datepicker>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
                        <datepicker id="manufacturer-warranty-end" class="date-from" format="yyyy-MM-dd" v-model='datatable.serial.form.fields.manufacturer_warranty_end'></datepicker>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc">
                        <input v-model="datatable.serial.form.fields.remarks" @keydown.enter="storeSerial()" type="text" />
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                        <img src="{{ asset('images/iconupdate.png') }}" @click="storeSerial()">
                    </table-input>
                </template>
            </datatable>
        </div>
        <div class="c-content-group c-t">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.qty'):</td>
                    <td><b><span>@{{ maxSerialCount }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.serial.number'):</td>
                    <td><b><span>@{{ currentSerialCount }}</span></b></td>
                </tr>
            </table>
        </div>
    </template>
    <template slot="footer">
        <input type="button" name="proceed" class="c-btn c-dk-green" value="@lang('core::label.proceed')" @click="itemManagement.serial.visible = false"/>
    </template>
</modal>

@component('core::components.confirm', [
    'id' => 'serial-warning',
    'show' => 'itemManagement.serial.confirmDelete.visible',
    'callback' => 'deleteSerial',
    'header' => trans('core::label.remove.serial.number'),
    'content' => trans('core::confirm.delete.serial.number')
])
@endcomponent