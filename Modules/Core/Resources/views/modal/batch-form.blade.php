<modal id="serial-number-form" v-show="itemManagement.batch.visible" @close="closeBatchForm()">
    <template slot="header">@lang('core::label.batch.assigning')</template>
    <template slot="content">
        <div class="c-content-group c-tb c-lr">
            <message type="danger" :show="itemManagement.batch.message.error.any()" @close="itemManagement.batch.message.reset()">
                <ul>
                    <li v-for="(value, key) in itemManagement.batch.message.error.get()" v-text="value"></li>
                </ul>
            </message>
        </div>
        <div class="flex flex-column flex-1 c-has-bl c-has-br overflow-hidden">
            <div class="c-sub-header c-has-bt">
                @lang('core::label.selected.batches')
            </div>
            <datatable
                :tableId="datatable.batch.form.id"
                :settings="datatable.batch.form.settings"
                :table="datatable.batch.form.value"
                :ref="datatable.batch.form.id"
                @destroy="confirmDeleteBatch"
                @update="updateBatch">
                <template slot="header">
                    <table-header>@lang('core::label.name')</table-header>
                    <table-header>@lang('core::label.qty')</table-header>
                    <table-header v-show="!disabled" ></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                        <input :disabled="disabled" type="text" v-model="row.data.name" @keydown="changeState(row.index, 'update', datatable.batch.form.id)"/>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                        <number-input :disabled="disabled" v-model="row.data.qty" @input="changeState(row.index, 'update', datatable.batch.form.id)"></number-input>
                    </td>
                </template>
                <template slot="input" v-if="!disabled">
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
                        <input type="text" v-model="datatable.batch.form.fields.name" ref="batchFormFirstField"/>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--qty">
                        <number-input @keydown.native.enter="storeBatch()" v-model="datatable.batch.form.fields.qty"></number-input>
                    </table-input>
                    <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                        <img src="{{ asset('images/iconupdate.png') }}" @click="storeBatch()">
                    </table-input>
                </template>
            </datatable>
        </div>
        <div class="c-content-group c-t">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.total.qty'):</td>
                    <td><b><span>@{{ maxBatchCount }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.total.batch.count'):</td>
                    <td><b><span>@{{ currentBatchCount }}</span></b></td>
                </tr>
            </table>
        </div>
    </template>
    <template slot="footer">
        <input type="button" name="proceed" class="c-btn c-dk-green" value="@lang('core::label.proceed')" @click="itemManagement.batch.visible = false"/>
    </template>
</modal>

@component('core::components.confirm', [
    'id' => 'batch-warning',
    'show' => 'itemManagement.batch.confirmDelete.visible',
    'callback' => 'deleteBatch',
    'header' => trans('core::label.remove.batch.number'),
    'content' => trans('core::confirm.delete.batch')
])
@endcomponent