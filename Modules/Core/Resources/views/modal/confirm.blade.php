<confirm 
    id="{{ $id }}"
    :show="{{ $show }}"
    ok="@lang('core::label.ok')"
    cancel="@lang('core::label.cancel')"
    @ok="{{ $callback }}"
    @cancel="{{ isset($cancel) ? $cancel : $show.' = false' }}"
    @close="{{ isset($cancel) ? $cancel : $show.' = false' }}">
    <template slot="header">
        {{ $header }}
    </template>
    <template slot="content">
        {{ $content }}
    </template>
</confirm>