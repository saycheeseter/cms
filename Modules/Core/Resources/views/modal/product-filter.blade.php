<filter-modal
    v-show="{{ $show }}"
    @close="{{ $show }} = false"
    :filters="{{ $options }}"
    v-model='{{ $model }}'
    v-on:search="{{ $search }}">
</filter-modal>

@prepend('script')
    @javascript('references', [
        'filters' => [
            'product' => $references['filters']['product']
        ]
    ])
@endprepend