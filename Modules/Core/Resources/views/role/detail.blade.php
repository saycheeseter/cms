@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.role.detail'))

@section('stylesheet')
@stop

@section('container-1')
    <div class="c-content-group c-tb c-lr c-has-bt">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.code'):</td>
                <td width="200px"><input type="text" v-model='role.info.code' max="50"></td>
                <td>@lang('core::label.role.name'):</td>
                <td width="200px"><input type="text" v-model='role.info.name' max="45"></td>
            </tr>
        </table>
    </div>
    <control-bar id="controls">
        <template slot="left">
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.branch'):</td>
                        <td><chosen :options='branches' v-model='selected.branch' @change='permissions' :disabled='allBranches.value'></chosen></td>
                    </tr>
                </table>
            </div>
            <div class="control-border control-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" v-model='allBranches.value' @click='onClickAllBranch'>
                                @lang('core::label.all.branches')
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="right">
            <control-button name="save" icon="fa fa-save" @click='save'>
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    <div class="c-sub-nav-content c-has-bt">
        <div class="c-sub-sidebar" id="sections">
            <ul class="c-ul-links">
                <li v-for="(section, key) in tabs.permissions.get()"
                    :name="key"
                    :class="{ active: section.active }"
                    @click="tabs.permissions.active(key)"
                    v-text="section.label">
                </li>
            </ul>
        </div>
        <div class="c-sub-main-content overflow-hidden">
            <div class="c-content" v-for="(section, keySection) in tabs.permissions.get()" v-show="section.active">
                <div class="c-sub-header c-has-bb clearfix">
                    <label class="radio-chkbx">
                        <input type="checkbox" 
                            :name="keySection"
                            v-model="section.all"
                            @change="toggleSectionPermissions(keySection)">
                        <span>@{{ section.label }}</span>
                    </label>
                </div>
                <div class="overflow">
                    <div class="c-content-group c-t c-lr">
                        <div class="c-form-group clearfix" v-for="(module, keyModule) in section.modules">
                            <div class="c-content-group">
                                <label>
                                    <input type="checkbox"
                                        :name="keyModule"
                                        v-model="module.all"
                                        @change="toggleModulePermissions(keySection, keyModule)">
                                    <b>@{{ module.label }}</b>
                                </label>
                            </div>
                            <template v-for="(permission, keyPermission) in module.permissions">
                                <template v-if="((keyPermission + 1) % 5) === 0 || (keyPermission + 1) === module.permissions.length">
                                    <div class="pull-left c-mgl-15">
                                        <table>
                                            <tr v-for="n in range(getLowerRangePermission(keyPermission), (keyPermission + 1))">
                                                <td>
                                                    <label class="radio-chkbx">
                                                        <input type="checkbox" class="permissions" :value="module.permissions[n].code" v-model="selected.code">
                                                        <span>@{{ module.permissions[n].label }}</span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </template>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
@stop

@section('modals')
    <confirm 
        id="confirm"
        :show="confirm.visible"
        ok="@lang('core::label.yes')"
        cancel="@lang('core::label.no')"
        @ok="update(true)"
        @cancel="update(false)"
        @close="confirm.visible = false">
        <template slot="header">
            @lang('core::label.warning')
        </template>
        <template slot="content">
            @lang('core::confirm.apply.role.to.user')
        </template>
    </confirm>

    <confirm
        id="allBranches"
        :show='allBranches.confirm'
        ok="@lang('core::label.yes')"
        cancel="@lang('core::label.no')"
        @ok="clearPermissions"
        @cancel="closeAllBranchConfirm"
        @close="closeAllBranchConfirm">
        <template slot="header">
            @lang('core::label.warning')
        </template>
        <template slot="content">
            @lang('core::confirm.this.will.clear.permissions')
        </template>
    </confirm>
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('sections', $sections)
    @javascript('userPermissions', $permissions)
    @javascript('branches', $branches)

    <script src="{{ elixir('js/Role/detail.js') }}"></script>
@stop
