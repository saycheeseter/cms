@extends('core::inventory-chain.detail')
@section('title', trans('core::label.sales.outbound.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>
                    <label class="radio-chkbx">
                        <input type="radio" name="customer-type" v-model="info.customer_type" @change='notify("customer-type")' :value="regularCustomer" :disabled='disabled'>
                        @lang('core::label.regular.customer')
                    </label>
                    <label class="radio-chkbx c-mgl-5">
                        <input type="radio" name="customer-type" v-model="info.customer_type" @change='notify("customer-type")' :value="walkInCustomer" :disabled='disabled'>
                        @lang('core::label.walk.in.customer')
                    </label>
                </td>
            </tr>
        </table>
        <div class="pull-left">
            <div class="c-content-group">
                <table class="c-tbl-layout">
                    <tr v-show="isRegularCustomer">
                        <td width="110px">@lang('core::label.customer'):</td>
                        <td width="156px">
                            <chosen
                                class="required"
                                name="customer"
                                v-model='info.customer_id'
                                :options='reference.customers'
                                @change='notify("customer")'
                                :disabled='disabled'>
                            </chosen>
                        </td>
                    </tr>
                    <tr v-show="isWalkInCustomer">
                        <td width="110px">@lang('core::label.walk.in.name'):</td>
                        <td width="156px"><input type="text" class="required" name="customer-walk-in-name" ref="walkin" v-model="info.customer_walk_in_name" :disabled='disabled'></td>
                    </tr>
                    <tr v-show="isRegularCustomer">
                        <td>@lang('core::label.customer.branch'):</td>
                        <td>
                            <chosen
                                name="customer-detail"
                                v-model='info.customer_detail_id'
                                :options='reference.customerDetails'
                                :disabled='disabled'>
                            </chosen>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="c-tbl-layout" v-if="enableCreditLimit && !approvedTransaction">
                <tr>
                    <td width="110px">@lang('core::label.credit.limit'):</td>
                    <td width="80px"><b>@{{ customer.credit.limit }}</b></td>
                    <td width="50px">@lang('core::label.due'):</td>
                    <td><b>@{{ customer.credit.due }}</b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.running.due'):</td>
                    <td><b>@{{ creditRunningDue }}</b></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.salesman'):</td>
                    <td>
                        <chosen
                            class="required"
                            name="salesman"
                            v-model='info.salesman_id'
                            :options='reference.users'
                            :disabled='!updatable'>
                        </chosen>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.term'):</td>
                    <td><number-input name="term" v-model='info.term' :disabled='!updatable' :precision='false'></number-input></td>
                </tr>
            </table>
        </div>
    </div>
@stop

@push('modals')
    @component('core::components.confirm', [
        'id' => 'credit-limit-warning',
        'show' => 'customer.credit.warning.visible',
        'callback' => 'proceedOverTheCreditLimitTransaction',
        'header' => trans('core::label.warning'),
        'content' => trans('core::confirm.transaction.over.credit.limit')
    ])
    @endcomponent

    <modal
        v-show='customer.credit.notify'
        @close='closeCustomerCreditNotify'>
    <template slot="header">
        @lang('core::label.warning')
    </template>
    <template slot="content">
        @lang('core::info.transaction.over.credit.limit')
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" @click='closeCustomerCreditNotify()'>@lang('core::label.close')</button>
    </template>
    </modal>
@endpush

@push('script')
    @javascript('customers', $customers)

    <script src="{{ elixir('js/SalesOutbound/detail.js') }}"></script>
@endpush
