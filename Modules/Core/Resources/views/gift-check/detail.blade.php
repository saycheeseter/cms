<modal
    v-bind:id="form.id"
    v-show="form.visible"
    v-on:close="form.visible = false">
    <template slot="header">
        @lang('core::label.gift.check.detail')
    </template>
    <template slot="content">
        <div class="c-content-group c-b">
            <message type="danger" :show="message.details.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.details.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.details.success != ''">
                <span v-text="message.details.success"></span>
            </message>
        </div>
        <div class="c-content-group c-tb clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.code'):</td>
                        <td><input type="text" v-model="fields.code" :disabled="disabled"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.check.no'):</td>
                        <td><input type="text" v-model="fields.check_number" :disabled="disabled"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.date.from'):</td>
                        <td><datepicker id="date-from" format="yyyy-MM-dd" v-model="fields.date_from" :disabled-picker="disabled"></datepicker></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.date.to'):</td>
                        <td><datepicker id="date-to" format="yyyy-MM-dd" v-model="fields.date_to" :disabled-picker="disabled"></datepicker></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.amount'):</td>
                        <td><number-input v-model="fields.amount" :min="0" :disabled="disabled" :unsigned="true"></number-input></td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-5">
                <table class="c-tbl-layout leveled-tr">
                    <tr>
                        <td>@lang('core::label.status'):</td>
                        <td><span><b>@{{ fields.status_label }}</b></span></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.redeem.by'):</td>
                        <td><span>@{{ fields.redeemed_by }}</span></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.redeem.date'):</td>
                        <td><span>@{{ fields.redeemed_date }}</span></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.reference.no'):</td>
                        <td><span>@{{ fields.reference_number }}</span></td>
                    </tr>
                </table>
            </div>
        </div>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" name="save" @click="save" v-show="!disabled">@lang('core::label.save')</button>
    </template>
</modal>