@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.gift.check.list'))

@section('stylesheet')
    <style type="text/css">
        #modal-detail .modal-container {
            width: 500px;
        }
        #modal-detail .modal-body,
        #bulk-entry .modal-body {
            overflow: initial;
        }
        #bulk-entry .modal-container {
            width: 610px;
        }
    </style>
@stop

@section('container-1')
    <control-bar>
        <template slot="left">
            @if ($permissions['create'])
                <control-button icon="fa fa-plus" @click="form.visible = true">
                    @lang('core::label.add.new.gift.check')
                </control-button>
            @endif
            @if ($permissions['create'])
                <control-button icon="fa fa-bold" @click="bulkEntry.visible = true">
                    @lang('core::label.bulk.entry')
                </control-button>
            @endif
            <control-button icon="fa fa-search" @click="filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        @reload="paginate"
        @destroy="confirm">
        <template slot="header">
            <table-header>@lang('core::label.code')</table-header>
            <table-header>@lang('core::label.check.no')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header>@lang('core::label.date.from')</table-header>
            <table-header>@lang('core::label.date.to')</table-header>
            <table-header>@lang('core::label.status')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.check_number }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.amount }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date_from }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.date_to }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id'   => 'confirm-delete',
        'show' => 'confirmation.delete.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.gift.check'),
        'content' => trans('core::confirm.delete.gift.check')
    ])
    @endcomponent

    <filter-modal
        v-show="filter.visible"
        @close="filter.visible = false"
        :filters="filter.options"
        v-model='filter.data'
        v-on:search="paginate">
    </filter-modal>

    @include('core::gift-check.detail')
    @include('core::gift-check.bulk-entry')
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/GiftCheck/list.js') }}"></script>
@stop