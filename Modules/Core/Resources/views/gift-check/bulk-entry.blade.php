<modal
    v-bind:id="bulkEntry.id"
    v-show="bulkEntry.visible"
    v-on:close="bulkEntry.visible = false">
    <template slot="header">
        @lang('core::label.bulk.entry')
    </template>
    <template slot="content">
        <div class="c-content-group c-b">
            <message type="danger" :show="message.bulkEntry.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.bulkEntry.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.bulkEntry.success != ''">
                <span v-text="message.bulkEntry.success"></span>
            </message>
        </div>
        <div class="c-content-group c-tb clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.date.from'):</td>
                        <td><datepicker id="bulk-entry-date-from" format="yyyy-MM-dd" v-model="bulkEntry.fields.date_from"></datepicker></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.code.series.start'):</td>
                        <td><number-input type="text" v-model="bulkEntry.fields.code_series" :unsigned="true" :precision="false" :formatted="false"></number-input></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.qty'):</td>
                        <td><number-input class="field__qty" v-model="bulkEntry.fields.qty" :min="1" :unsigned="true" :precision="false"></number-input></td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-5">
                <table class="c-tbl-layout leveled-tr">
                    <tr>
                        <td>@lang('core::label.date.to'):</td>
                        <td><datepicker id="bulk-entry-date-to" format="yyyy-MM-dd" v-model="bulkEntry.fields.date_to"></datepicker></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.check.no.series.start'):</td>
                        <td><number-input type="text" v-model="bulkEntry.fields.check_number_series" :unsigned="true" :precision="false" :formatted="false"></number-input></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.amount'):</td>
                        <td><number-input v-model="bulkEntry.fields.amount" :min="0" :unsigned="true"></number-input></td>
                    </tr>
                </table>
            </div>
        </div>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" @click="createBulkEntry">@lang('core::label.create')</button>
    </template>
</modal>