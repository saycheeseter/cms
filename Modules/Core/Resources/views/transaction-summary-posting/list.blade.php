@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.transaction.summary.posting'))

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            <control-button name='post' icon="fa fa-plus" @click="showPostingForm">
                @lang('core::label.post')
            </control-button>
        </template>
    </control-bar>

    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.sheet.number')</table-header>
            <table-header>@lang('core::label.transaction.type')</table-header>
            <table-header>@lang('core::label.transaction.date')</table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--sheet-number">
                <span>@{{ row.data.sheet_number }}</span>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--description">
                <span>@{{ row.data.transaction_type.label }}</span>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--date">
                <span>@{{ row.data.transaction_date }}</span>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <modal
        v-show='posting.form.visible'
        @close='closePostingForm()'>
        <template slot="header">
            @lang('core::label.generate.summary.data')
        </template>
        <template slot="content">
            <table class="c-tbl-layout">
                <tr>
                    <td width="200px">@lang('core::label.last.post.date'):</td>
                    <td width="200px">
                        @{{ posting.form.data.lastPostDate }}
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::info.indicate.up.to.what.date'):</td>
                    <td>
                        <datepicker
                            id='post-date'
                            name='post-date'
                            :format='datepickerFormat.format'
                            :disabled='datepickerFormat.disabled'
                            v-model='posting.form.data.date'>
                        </datepicker>
                    </td>
                </tr>
            </table>
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-blue" @click='closePostingForm()'>@lang('core::label.cancel')</button>
            <button class="c-btn c-dk-green" @click='generateSummary()'>@lang('core::label.proceed')</button>
        </template>
    </modal>
@stop

@section('script')
    <script src="{{ elixir('js/TransactionSummaryPosting/list.js') }}"></script>
@stop
