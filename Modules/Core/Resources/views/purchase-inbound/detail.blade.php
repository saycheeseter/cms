@extends('core::inventory-chain.detail')
@section('title', trans('core::label.purchase.inbound.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.supplier'):</td>
                <td>
                    <chosen
                        class="required"
                        name='supplier'
                        v-model='info.supplier_id' 
                        :options='reference.tsuppliers'
                        :disabled='disabled'
                        @change='notify'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.dr.no'):</td>
                <td>
                    <input name='dr-no' v-model='info.dr_no' type="text" :disabled='!updatable'>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.supplier.invoice.no'):</td>
                <td>
                    <input name='supplier-invoice-no' v-model='info.supplier_invoice_no' type="text" :disabled='!updatable'>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.supplier.box.count'):</td>
                <td>
                    <input name='supplier-box-count' v-model='info.supplier_box_count' type="text" :disabled='!updatable'>
                </td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.payment.method'):</td>
                <td>
                    <chosen 
                        name='payment-method'
                        v-model='info.payment_method'
                        :options='reference.paymentMethods'
                        :disabled='!updatable'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.term'):</td>
                <td><number-input name='term' v-model='info.term' :disabled='!updatable' :precision='false'></number-input></td>
            </tr>
        </table>
    </div>
@stop

@push('modals')
    @include('core::modal.serial-number-form')
    @include('core::modal.batch-form')

    <dialog-box
        :show='autoGroupProduct.dialog.message !=""'
        @close='breakToComponents'>
        <template slot='content'>@{{ autoGroupProduct.dialog.message }}</template>
    </dialog-box>
@endpush

@push('script')
    @javascript('tsuppliers', $tsuppliers)
    @javascript('paymentMethods', $paymentMethods)

    <script src="{{ elixir('js/PurchaseInbound/detail.js') }}"></script>
@endpush