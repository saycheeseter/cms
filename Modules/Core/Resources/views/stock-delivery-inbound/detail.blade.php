@extends('core::inventory-chain.detail')
@section('title', trans('core::label.stock.delivery.inbound.detail'))
@section('transaction-type-label', trans('core::label.from.outbound'))
@section('transaction-label', trans('core::label.outbound'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.delivery.from'):</td>
                <td><span><b>@{{ info.delivery_from_label }}</b></span></td>
            </tr>
            <tr>
                <td>@lang('core::label.delivery.from.location'):</td>
                <td><span><b>@{{ info.delivery_from_location_label }}</b></span></td>
            </tr>
        </table>
    </div>
@stop

@push('script')
    <script src="{{ elixir('js/StockDeliveryInbound/detail.js') }}"></script>
@endpush