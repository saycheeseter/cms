@extends('core::stock-request.list')
@section('title', trans('core::label.stock.request.to.list'))

@push('script')
    <script src="{{ elixir('js/StockRequestTo/list.js') }}"></script>
@endpush
