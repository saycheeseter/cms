@extends('core::stock-request.detail')
@section('title', trans('core::label.stock.request.to.detail'))

@include('core::inventory-flow.sections.detail.sales-outbound-import')

@push('script')
    <script src="{{ elixir('js/StockRequestTo/detail.js') }}"></script>
@endpush
