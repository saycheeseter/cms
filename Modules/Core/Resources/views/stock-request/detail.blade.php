@extends('core::invoice.detail')

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.request.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='request-to'
                        v-model='info.request_to'
                        :options='reference.branches'
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
@stop

@prepend('modals')
    <confirm 
        id="requestedWarning.id"
        :show="requestedWarning.visible"
        ok="@lang('core::label.ok')"
        cancel="@lang('core::label.cancel')"
        @ok="proceed"
        @cancel="requestedWarning.visible = false"
        @close="requestedWarning.visible = false">
        <template slot="header">
            @lang('core::confirm.continue.add.product')
        </template>
        <template slot="content">
            <li v-for="message in requestedWarning.messages">
                @{{ message }}
            </li>
        </template>
    </confirm>
@endprepend