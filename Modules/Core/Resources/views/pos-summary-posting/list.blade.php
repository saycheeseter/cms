@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.pos.summary.posting'))

@section('container-1')
    <datatable
        :tableId="datatable.id"
        :settings="datatable.settings"
        :table="datatable.value"
        @reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.terminal.number')</table-header>
            <table-header>@lang('core::label.branch')</table-header>
            <table-header>@lang('core::label.last.post.date')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--number">
                <span>@{{ row.data.terminal_number }}</span>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--location">
                <span>@{{ row.data.branch.name }}</span>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--datetime">
                <span>@{{ row.data.last_post_date }}</span>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--action">
                <button class="c-btn c-md-green" @click="showPostingForm(row.data.id, row.dataIndex)">
                    @lang('core::label.post')
                </button>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <modal
        v-show='posting.form.visible'
        @close='closePostingForm()'>
        <template slot="header">
            @lang('core::label.generate.summary.data')
        </template>
        <template slot="content">
            <table class="c-tbl-layout">
                <tr>
                    <td width="200px">@lang('core::info.indicate.up.to.what.date'):</td>
                    <td width="200px">
                        <date-time-picker
                            id='post-ate'
                            name='post-date'
                            v-model='posting.form.data.date'
                            :date="{ format: 'yyyy-MM-dd' }">
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-blue" @click='closePostingForm()'>@lang('core::label.cancel')</button>
            <button class="c-btn c-dk-green" @click='generateSummary()'>@lang('core::label.proceed')</button>
        </template>
    </modal>
@stop

@section('script')
    <script src="{{ elixir('js/PosSummaryPosting/list.js') }}"></script>
@stop
