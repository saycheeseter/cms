@extends('core::inventory-chain.detail')

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.deliver.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to'
                        v-model='info.deliver_to'
                        :options='reference.branches'
                        :disabled='fromInvoice || disabled'
                        @change='getDeliveryLocations'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.deliver.to.location'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to-location'
                        v-model='info.deliver_to_location'
                        :options='reference.deliveryLocations'
                        :disabled='fromInvoice || disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
@stop

@prepend('modals')
    <confirm 
        :id="minimumInventoryWarning.id"
        :show="minimumInventoryWarning.visible"
        ok="@lang('core::label.ok')"
        cancel="@lang('core::label.cancel')"
        @ok="proceed"
        @cancel="minimumInventoryWarning.visible = false"
        @close="minimumInventoryWarning.visible = false">
        <template slot="header">
            @lang('core::confirm.continue.add.product')
        </template>
        <template slot="content">
            <li v-for="message in minimumInventoryWarning.messages">
                @{{ message }}
            </li>
        </template>
    </confirm>
@endprepend