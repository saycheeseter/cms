@extends('core::layouts.master')
@extends('core::layouts.resizable.cd')
@section('title', 'Testing')

@section('stylesheet')
    <style type="text/css">
        
    </style>
@stop

@section('form')
    <div class="c-content-group c-tb c-lr clearfix">
        <table class="c-tbl-layout">
            <tr>
                <td><input type="button" class="c-btn c-dk-green" value="Search" id="btnSearch" @click='openModal'></td>
            </tr>
        </table>
        <!-- This part is the component declaration -->
        <!-- <filtermodal v-show="modal.visible" @close="modal.visible = false" :filters="filters">
            
        </filtermodal> -->
        <!-- end declaration -->
    </div>
    <!-- <v-select :value.sync="selected" :options="options" @input='setSelected'></v-select> -->
    <chosen :options='options' :default='selected' :label='emitLabel.supplier' multiple='true' @select="onSelect"></chosen>
    <!-- <basic-select
        :options="options"
        :selected-option="item"
        placeholder="select item"
        @select="onSelect"
    ></basic-select> -->
@stop

@section('datatable')
    
@stop

@section('modals')
@stop

@section('script')
<script src="{{ asset('js/testing/testing.js') }}"></script>
@stop