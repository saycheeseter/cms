@extends('core::invoice.detail')
@section('title', trans('core::label.stock.return.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.deliver.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to'
                        v-model='info.deliver_to' 
                        :options='reference.branches'
                        @change="getDeliveryLocations"
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.deliver.to.location'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to-location'
                        v-model='info.deliver_to_location' 
                        :options='reference.deliveryLocations'
                        :disabled='disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
@stop

@push('script')
    <script src="{{ elixir('js/StockReturn/detail.js') }}"></script>
@endpush