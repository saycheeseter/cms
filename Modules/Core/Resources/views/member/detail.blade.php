<modal
    v-bind:id="modal.details.id"
    v-show="modal.details.visible"
    v-on:close="modal.details.visible = false">
    <template slot="header">
        @lang('core::label.member.detail')
    </template>
    <template slot="content">
        <div class="c-content-group c-b">
            <message type="danger" :show="message.details.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.details.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" :show="message.details.success != ''">
                <span v-text="message.details.success"></span>
            </message>
        </div>
        <div class="c-content-group clearfix">
            <div class="pull-left">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.card.id'):</td>
                        <td width="200px"><input type="text" class="required" name="card-id" v-model="fields.card_id"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.full.name'):</td>
                        <td><input type="text" class="required" name="full-name" v-model="fields.full_name"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.telephone.no'):</td>
                        <td><input type="text" name="telephone" v-model="fields.telephone"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.birth.date'):</td>
                        <td>
                            <datepicker id="birth-date" class="birth-date" format="yyyy-MM-dd" v-model='fields.birth_date'></datepicker>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.gender'):</td>
                        <td class="radio-chkbx">
                            <label>
                                <input type="radio" name="gender" v-model='fields.gender' value="0">
                                <span>@lang('core::label.male')</span>
                            </label>
                            <label>
                                <input type="radio" name="gender" v-model='fields.gender' value="1">
                                <span>@lang('core::label.female')</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.email'):</td>
                        <td><input type="text" name="email" v-model="fields.email"></td>
                    </tr>
                    <tr>
                        <td valign="top">@lang('core::label.address'):</td>
                        <td>
                            <textarea rows="3" name="address" v-model="fields.address"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-left c-mgl-5">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.barcode'):</td>
                        <td width="200px"><number-input class="required" name="barcode" type="text" v-model="fields.barcode" :formatted="false" :precision="false" ></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.member.type'):</td>
                        <td>
                            <chosen :options='rates' name="rates" v-model='fields.rate_head_id'></chosen>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.status'):</td>
                        <td class="radio-chkbx">
                            <label>
                                <input type="checkbox" name="status" v-model="fields.status" value="1">
                                <span>@lang('core::label.active')</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.mobile.no'):</td>
                        <td><input type="text" name="mobile" v-model="fields.mobile"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.registration.date'):</td>
                        <td>
                            <datepicker id="registration-date" class="registration-date" format="yyyy-MM-dd" v-model='fields.registration_date'></datepicker>
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.expiration.date'):</td>
                        <td>
                            <datepicker id="expiration-date" class="expiration-date" format="yyyy-MM-dd" v-model='fields.expiration_date'></datepicker>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">@lang('core::label.memo'):</td>
                        <td>
                            <textarea rows="3" name="memo" v-model="fields.memo"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" name="save" @click="validate">@lang('core::label.save')</button>
    </template>
</modal>