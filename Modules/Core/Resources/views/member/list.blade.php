@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.member'))

@section('stylesheet')
    <style type="text/css">
        #details .modal-container {
            width: 632px;
        }
        #details .modal-container .modal-body {
            overflow: initial;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            @if ($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="modal.details.visible = true">
                    @lang('core::label.add.new.member')
                </control-button>
            @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="columnSettings.member.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
        <template slot="right">
            @if($permissions['import_excel'])
                @component('core::components.import-excel')
                @endcomponent
            @endif
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm">
        <template slot="header">
            <table-header>@lang('core::label.full.name')</table-header>
            <table-header v-show='columnSettings.member.selections.memberType'>@lang('core::label.member.type')</table-header>
            <table-header v-show='columnSettings.member.selections.cardId'>@lang('core::label.card.id')</table-header>
            <table-header v-show='columnSettings.member.selections.barcode'>@lang('core::label.barcode')</table-header>
            <table-header v-show='columnSettings.member.selections.mobileNo'>@lang('core::label.mobile.no')</table-header>
            <table-header v-show='columnSettings.member.selections.telephoneNo'>@lang('core::label.telephone.no')</table-header>
            <table-header v-show='columnSettings.member.selections.birthDate'>@lang('core::label.birth.date')</table-header>
            <table-header v-show='columnSettings.member.selections.gender'>@lang('core::label.gender')</table-header>
            <table-header v-show='columnSettings.member.selections.email'>@lang('core::label.email')</table-header>
            <table-header v-show='columnSettings.member.selections.address'>@lang('core::label.address')</table-header>
            <table-header v-show='columnSettings.member.selections.memo'>@lang('core::label.memo')</table-header>
            <table-header v-show='columnSettings.member.selections.registrationDate'>@lang('core::label.registration.date')</table-header>
            <table-header v-show='columnSettings.member.selections.expirationDate'>@lang('core::label.expiration.date')</table-header>
            <table-header v-show='columnSettings.member.selections.status'>@lang('core::label.status')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.full_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type" @click="show(row.data.id)" v-show='columnSettings.member.selections.memberType'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.member_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--userCode" @click="show(row.data.id)" v-show='columnSettings.member.selections.cardId'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.card_id }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--userCode" @click="show(row.data.id)" v-show='columnSettings.member.selections.barcode'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.barcode }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.member.selections.mobileNo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.mobile }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.member.selections.telephoneNo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.telephone }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--type" @click="show(row.data.id)" v-show='columnSettings.member.selections.birthDate'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.birth_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--gender" @click="show(row.data.id)" v-show='columnSettings.member.selections.gender'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.gender }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--email" @click="show(row.data.id)" v-show='columnSettings.member.selections.email'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.email }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--address" @click="show(row.data.id)" v-show='columnSettings.member.selections.address'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.address }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="show(row.data.id)" v-show='columnSettings.member.selections.memo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="show(row.data.id)" v-show='columnSettings.member.selections.registrationDate'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.registration_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="show(row.data.id)" v-show='columnSettings.member.selections.expirationDate'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.expiration_date }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" @click="show(row.data.id)" v-show='columnSettings.member.selections.status'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id'   => 'confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.member'),
        'content' => trans('core::confirm.delete.member')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    @include('core::member.detail')

    <column-select
        :columns='columnSettings.member.columns'
        v-show='columnSettings.member.visible'
        @close="columnSettings.member.visible = false"
        id = 'column-modal'
        v-model='columnSettings.member.selections'
        :module='columnSettings.member.module'>
    </column-select>
    
    @stack('modals')
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('script')
    @javascript('rates', $rates)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Member/list.js') }}"></script>
@stop