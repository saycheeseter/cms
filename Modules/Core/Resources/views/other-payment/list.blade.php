@extends('core::other-financial-flow.list', [
    'referenceNameLabel' => trans('core::label.pay.to'),
    'addNewLabel' => trans('core::label.add.new.payment')
])

@section('title', trans('core::label.other.payment.list'))

@push('modals')
    @component('core::components.confirm', [
        'id' => 'confirm-list',
        'show' => 'modal.list.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.other.payment'),
        'content' => trans('core::confirm.delete.other.payment')
    ])
    @endcomponent

    @include('core::other-financial-flow.detail', [
        'detailTitle' => trans('core::label.other.payment.detail'),
        'referenceNameLabel' => trans('core::label.pay.to'),

    ])

    @component('core::components.confirm', [
        'id' => 'confirm-detail',
        'show' => 'modal.detail.confirm.visible',
        'callback' => 'destroyDetail',
        'header' => trans('core::label.delete.other.payment'),
        'content' => trans('core::confirm.delete.other.payment')
    ])
    @endcomponent
@endpush

@push('script')
    <script src="{{ elixir('js/OtherPayment/list.js') }}"></script>
@endpush