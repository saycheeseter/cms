@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.supplier'))

@section('stylesheet')
    <style type="text/css">
        #details .modal-container {
            width: 480px;
        }
        #details .modal-body {
            padding: 0px;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
        @if ($permissions['create'])
            <control-button name="add" icon="fa fa-plus" @click="modal.details.visible = true">
                @lang('core::label.add.new.supplier')
            </control-button>
        @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="columnSettings.supplier.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm"
        v-on:update="update"
        v-on:add="store">
        <template slot="header">
            <table-header>@lang('core::label.code')</table-header>
            <table-header v-show='columnSettings.supplier.selections.name'>@lang('core::label.name')</table-header>
            <table-header v-show='columnSettings.supplier.selections.chineseName'>@lang('core::label.chinese.name')</table-header>
            <table-header v-show='columnSettings.supplier.selections.contact'>@lang('core::label.contact')</table-header>
            <table-header v-show='columnSettings.supplier.selections.ownerName'>@lang('core::label.owner.name')</table-header>
            <table-header v-show='columnSettings.supplier.selections.address'>@lang('core::label.address')</table-header>
            <table-header v-show='columnSettings.supplier.selections.landline'>@lang('core::label.landline')</table-header>
            <table-header v-show='columnSettings.supplier.selections.fax'>@lang('core::label.fax')</table-header>
            <table-header v-show='columnSettings.supplier.selections.mobile'>@lang('core::label.mobile')</table-header>
            <table-header v-show='columnSettings.supplier.selections.email'>@lang('core::label.email')</table-header>
            <table-header v-show='columnSettings.supplier.selections.website'>@lang('core::label.website')</table-header>
            <table-header v-show='columnSettings.supplier.selections.contactPerson'>@lang('core::label.contact.person')</table-header>
            <table-header v-show='columnSettings.supplier.selections.memo'>@lang('core::label.memo')</table-header>
            <table-header v-show='columnSettings.supplier.selections.term'>@lang('core::label.term')</table-header>
            <table-header v-show='columnSettings.supplier.selections.status'>@lang('core::label.status')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.name'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.full_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.chineseName'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.chinese_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.contact'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.contact }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.ownerName'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.owner_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--address" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.address'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.address }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.landline'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.landline }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.fax'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.fax }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.mobile'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.mobile }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--email" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.email'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.email }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--website" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.website'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.website }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.contactPerson'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.contact_person }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.memo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--term" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.term'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.term }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" @click="show(row.data.id)" v-show='columnSettings.supplier.selections.status'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.supplier'),
        'content' => trans('core::confirm.delete.supplier')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    @include('core::supplier.detail')

    <column-select
        :columns='columnSettings.supplier.columns'
        v-show='columnSettings.supplier.visible'
        @close="columnSettings.supplier.visible = false"
        id = 'column-modal'
        v-model='columnSettings.supplier.selections'
        :module='columnSettings.supplier.module'>
    </column-select>
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Supplier/list.js') }}"></script>
@stop
