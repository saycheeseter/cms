<modal
    v-bind:id="modal.details.id"
    v-show="modal.details.visible"
    v-on:close="modal.details.visible = false">
    <template slot="header">
        @lang('core::label.supplier.detail')
    </template>
    <template slot="content">
        <div class="c-nav-tabs clearfix">
            <div class="c-nav-tab" v-for="(tab, key) in tabs.get()" 
                :name="key"
                v-text="tab.label" 
                :class="{ active: tab.active }" 
                @click="tabs.active(key)">
            </div>
        </div>
        <div class="c-nav-tabs-container clearfix">
            <div class="c-content-group c-t c-lr">
                <message type="danger" :show="message.details.error.any()" @close="message.reset()">
                    <ul>
                        <li v-for="(value, key) in message.details.error.get()" v-text="value"></li>
                    </ul>
                </message>
                <message type="success" :show="message.details.success != ''">
                    <span v-text="message.details.success"></span>
                </message>
            </div>
            <div class="basic-info" v-show='tabs.basic.active'>
                <div class="c-content-group c-tb c-lr">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.code'):</td>
                            <td><input type="text" class="required" name="code" v-model="fields.code"></td>
                            <td>@lang('core::label.full.name'):</td>
                            <td><input type="text" class="required" name="full_name" v-model="fields.full_name"></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.chinese.name'):</td>
                            <td><input type="text" name="chinese_name" v-model="fields.chinese_name"></td>
                            <td>@lang('core::label.contact'):</td>
                            <td><input type="text" name="contact" v-model="fields.contact"></td>
                        </tr>
                        <tr>
                            <td valign="top">@lang('core::label.address'):</td>
                            <td colspan="3"><textarea rows="3" name="address" v-model="fields.address"></textarea></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.term'):</td>
                            <td><number-input name="term" v-model="fields.term" :precision='false'></td>
                            <td>@lang('core::label.status'):</td>
                            <td class="radio-chkbx">
                                <label>
                                    <input type="checkbox" name="status" v-model="fields.status">
                                    <span>@lang('core::label.active')</span>
                                </label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="advanced-info" v-show='tabs.advance.active'>
                <div class="c-content-group c-tb c-lr">
                    <table class="c-tbl-layout">
                        <tr>
                            <td>@lang('core::label.owners.name'):</td>
                            <td width="200px"><input type="text" name="owners_name" v-model="fields.owner_name"></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.landline'):</td>
                            <td><input type="text" name="landline" v-model="fields.landline" ></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.fax.number'):</td>
                            <td><input type="text" name="fax" v-model="fields.fax" ></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.mobile.no'):</td>
                            <td><input type="text" name="mobile" v-model="fields.mobile"></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.email.address'):</td>
                            <td><input type="text" name="email" v-model="fields.email"></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.company.website'):</td>
                            <td><input type="text" name="website" v-model="fields.website"></td>
                        </tr>
                        <tr>
                            <td>@lang('core::label.contact.person'):</td>
                            <td><input type="text" name="contact_person" v-model="fields.contact_person"></td>
                        </tr>
                        <tr>
                            <td valign="top">@lang('core::label.memo'):</td>
                            <td><textarea rows="3" name="memo" v-model="fields.memo"></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" name="save" @click="save">@lang('core::label.save')</button>
    </template>
</modal>