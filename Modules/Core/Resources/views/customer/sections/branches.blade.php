<section class="customer-branches" v-show='tabs.branches.active'>
    <div class="c-content-group c-t c-lr clearfix">
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.name'):</td>
                    <td width="212px"><input type="text" class="required" name="branch-name" v-model="customer.branch.name"></td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.address'):</td>
                    <td><textarea rows="3" name="branch-address" v-model="customer.branch.address"></textarea></td>
                </tr>
                <tr>
                    <td>@lang('core::label.owners.name'):</td>
                    <td><input type="text" name="branch-owner-name" v-model="customer.branch.owner_name"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.landline'):</td>
                    <td><input type="text" name="branch-landline" v-model="customer.branch.landline"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.mobile.no'):</td>
                    <td><input type="text" name="branch-mobile" v-model="customer.branch.mobile"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.tin'):</td>
                    <td><input type="text" name="branch-tin" v-model="customer.branch.tin"></td>
                </tr>
            </table>
        </div>
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.chinese.name'):</td>
                    <td width="212px"><input type="text" name="branch-chinese-name" v-model="customer.branch.chinese_name"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.contact'):</td>
                    <td><input type="text" name="branch-contact" v-model="customer.branch.contact"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.contact.person'):</td>
                    <td><input type="text" name="branch-contact-person" v-model="customer.branch.contact_person"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.fax.number'):</td>
                    <td><input type="text" name="branch-fax" v-model="customer.branch.fax"></td>
                </tr>
                <tr>
                    <td>@lang('core::label.email.address'):</td>
                    <td><input type="text" name="branch-email" v-model="customer.branch.email"></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right">
                        <input type="button" name="branch-clear" class="c-btn c-lt-blue" value="@lang('core::label.clear')" @click='clearBranchFields'>
                        <input v-show='state.create.active'
                            name="branch-new" 
                            type="button" 
                            class="c-btn c-lt-green" 
                            value="@lang('core::label.create.new')" 
                            @click='createBranch'>
                        <input v-show='state.update.active' 
                            name="branch-update" 
                            type="button" 
                            class="c-btn c-lt-green" 
                            value="@lang('core::label.update')" 
                            @click='updateBranch'>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="c-content-group c-t c-lr">
        <datatable
            v-bind:tableId="datatable.detail.id"
            v-bind:settings="datatable.detail.settings"
            v-bind:table="datatable.detail.value"
            v-on:destroy="confirmBranch">
            <template slot="header">
                <table-header>@lang('core::label.name')</table-header>
                <table-header>@lang('core::label.address')</table-header>
                <table-header>@lang('core::label.chinese.name')</table-header>
                <table-header>@lang('core::label.owners.name')</table-header>
                <table-header>@lang('core::label.landline')</table-header>
                <table-header>@lang('core::label.mobile.no')</table-header>
                <table-header>@lang('core::label.tin')</table-header>
                <table-header>@lang('core::label.contact')</table-header>
                <table-header>@lang('core::label.contact.person')</table-header>
                <table-header>@lang('core::label.fax.number')</table-header>
                <table-header>@lang('core::label.email.address')</table-header>
                <table-header></table-header>
            </template>
            <template slot="display" scope="branches">
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.name }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--address" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.address }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.chinese_name }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.owner_name }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.landline }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.mobile }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--number" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.tin }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.contact }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.contact_person }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.fax }}
                        </template>
                    </table-cell-data>
                </td>
                <td class="v-dataTable__tableCell v-dataTable__tableCell--email" @click='editBranch(branches)'>
                    <table-cell-data>
                        <template scope="content">
                            @{{ branches.data.email }}
                        </template>
                    </table-cell-data>
                </td>
            </template>
        </datatable>
    </div>
</section>