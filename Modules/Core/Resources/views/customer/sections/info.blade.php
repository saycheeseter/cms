<section class="customer-info" v-show='tabs.info.active'>
    <div class="" v-show="tabs.info.active">
        <div class="c-content-group c-tb c-lr">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.code'):</td>
                    <td><input type="text" class="required" name="code" v-model='customer.info.code'></td>
                    <td>@lang('core::label.name'):</td>
                    <td><input type="text" class="required" name="name" v-model='customer.info.name'></td>
                </tr>
                <tr>
                    <td width="155px">@lang('core::label.selling.price.based'):</td>
                    <td width="200px">
                        <chosen name="price-basis" :options='chosen.priceBasis' v-model='customer.info.pricing_type'></chosen>
                    </td>
                    <td>@lang('core::label.term'):</td>
                    <td><number-input name="term" class="field__term" v-model='customer.info.term' :precision="false"></td>
                </tr>
                <tr v-show='showPricingRate'>
                    <td valign="top">@lang('core::label.pricing.rate'):</td>
                    <td valign="top">
                        <number-input name="pricing-rate" v-model='customer.info.pricing_rate'>
                    </td>
                </tr>
                <tr v-show='showHistoricalOptions'>
                    <td>@lang('core::info.revert.price.to.zero'):</td>
                    <td class="radio-chkbx">
                        <label>
                            <input type="radio" name="historical-revert" value="1" v-model='customer.info.historical_change_revert'>
                            @lang('core::label.yes')
                        </label>
                        <label>
                            <input type="radio" name="historical-revert" value="0" v-model='customer.info.historical_change_revert'>
                            @lang('core::label.no')
                        </label>
                    </td>
                </tr>
                <tr v-show='showHistoricalOptions'>
                    <td>
                        @lang('core::label.default.historical.price'):
                    </td>
                    <td>
                        <label>
                            <input type="radio" name="historical-default" value="0" v-model='customer.info.historical_default_price'>
                            @lang('core::label.wholesale.price')
                        </label>
                        <label>
                            <input type="radio" name="historical-default" value="1" v-model='customer.info.historical_default_price'>
                            @lang('core::label.retail.price')
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>@lang('core::label.salesman'):</td>
                    <td>
                        <chosen name="salesman" :options='chosen.salesmen' v-model='customer.info.salesman_id'></chosen>
                    </td>
                    <td>@lang('core::label.status'):</td>
                    <td class="radio-chkbx">
                        <label>
                            <input type="checkbox" name="status" v-model="customer.info.status">
                            @lang('core::label.active')
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.memo'):</td>
                    <td valign="top"><textarea rows="3" name="memo" v-model='customer.info.memo'></textarea></td>
                </tr>
                <tr>
                    <td>@lang('core::label.company.website'):</td>
                    <td><input type="text" name="website" v-model='customer.info.website'></td>
                </tr>
                <tr>
                    <td>@lang('core::label.credit.limit'):</td>
                    <td><number-input name="credit-limit" v-model='customer.info.credit_limit'></td>
                </tr>
            </table>
        </div>
    </div>
</section>