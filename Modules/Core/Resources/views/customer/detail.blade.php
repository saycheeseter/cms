<modal
    v-bind:id="modal.detail.id"
    v-show="modal.detail.visible"
    v-on:open="modal.detail.visible = true"
    v-on:close="modal.detail.visible = false">
    <template slot="header">
        @lang('core::label.customer.detail')
    </template>
    <template slot="content">
        <div class="c-nav-tabs clearfix">
            <div class="c-nav-tab" v-for="(tab, key) in tabs.get()" 
                :name="key"
                v-text="tab.label" 
                :class="{ active: tab.active }" 
                @click="tabs.active(key)">
            </div>
        </div>
        <div class="c-nav-tabs-container">
            <div class="c-content-group c-tb c-lr">
                <message type="danger" :show="message.detail.error.any()" @close="message.reset()">
                    <ul>
                        <li v-for="(value, key) in message.detail.error.get()" v-text="value"></li>
                    </ul>
                </message>
                <message type="success" :show="message.detail.success != ''">
                    <span v-text="message.detail.success"></span>
                </message>
                <message type="info" :show="message.detail.info != ''">
                    <span v-text="message.detail.info"></span>
                </message>
                @include('core::customer.sections.info')
                @include('core::customer.sections.branches')
            </div>
        </div>
    </template>
    <template slot="footer">
        <button class="c-btn c-dk-green" name="save" @click='validate'>@lang('core::label.save')</button>
    </template>
</modal>