@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.customer'))

@section('stylesheet')
    <style type="text/css">
        #modal-detail .modal-container {
            width: 650px;
        }
        #modal-detail .modal-body {
            padding: 0px;
        }
        #modal-detail .v-dataTable {
            height: 180px;
            border-left: 1px #ccc solid;
            border-right: 1px #ccc solid;
        }
        #modal-detail .c-nav-tabs-container {
            overflow: auto;
            flex: 1;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
        @if ($permissions['create'])
            <control-button name="add" icon="fa fa-plus" @click="modal.detail.visible = true">
                @lang('core::label.add.new.customer')
            </control-button>
        @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="columnSettings.customer.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.list.id"
        v-bind:settings="datatable.list.settings"
        v-bind:table="datatable.list.value"
        v-on:destroy="confirm"
        v-on:reload="paginate">
        <template slot="header">
            <table-header>@lang('core::label.code')</table-header>
            <table-header v-show='columnSettings.customer.selections.name'>@lang('core::label.name')</table-header>
            <table-header v-show='columnSettings.customer.selections.salesman'>@lang('core::label.salesman')</table-header>
            <table-header v-show='columnSettings.customer.selections.term'>@lang('core::label.term')</table-header>
            <table-header v-show='columnSettings.customer.selections.website'>@lang('core::label.company.website')</table-header>
            <table-header v-show='columnSettings.customer.selections.memo'>@lang('core::label.memo')</table-header>
            <table-header v-show='columnSettings.customer.selections.pricingType'>@lang('core::label.pricing.type')</table-header>
            <table-header v-show='columnSettings.customer.selections.creditLimit'>@lang('core::label.credit.limit')</table-header>
            <table-header v-show='columnSettings.customer.selections.status'>@lang('core::label.status')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click='show(row.data.id)'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='show(row.data.id)' v-show='columnSettings.customer.selections.name'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click='show(row.data.id)' v-show='columnSettings.customer.selections.salesman'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.salesman }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--term" @click='show(row.data.id)' v-show='columnSettings.customer.selections.term'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.term }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--website" @click='show(row.data.id)' v-show='columnSettings.customer.selections.website'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.website }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click='show(row.data.id)' v-show='columnSettings.customer.selections.memo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--pricingType" @click='show(row.data.id)' v-show='columnSettings.customer.selections.pricingType'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.pricing_type }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount" @click='show(row.data.id)' v-show='columnSettings.customer.selections.creditLimit'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.credit_limit }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" @click='show(row.data.id)' v-show='columnSettings.customer.selections.status'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm-list',
        'show' => 'modal.list.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.customer'),
        'content' => trans('core::confirm.delete.customer')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="paginate">
    </filter-modal>
    
    @include('core::customer.detail')

    @component('core::components.confirm', [
        'id' => 'confirm-detail',
        'show' => 'modal.detail.confirm.visible',
        'callback' => 'destroyBranch',
        'header' => trans('core::label.delete.customer'),
        'content' => trans('core::confirm.delete.customer')
    ])
    @endcomponent

    @include('core::modal.confirm', [
        'id' => 'confirm-update',
        'show' => 'modal.update.confirm.visible',
        'callback' => 'update',
        'header' => trans('core::info.update.pricing.type.customer'),
        'content' => trans('core::confirm.update.pricing.type.customer')
    ])

    <column-select
        :columns='columnSettings.customer.columns'
        v-show='columnSettings.customer.visible'
        @close="columnSettings.customer.visible = false"
        id = 'column-modal'
        v-model='columnSettings.customer.selections'
        :module='columnSettings.customer.module'>
    </column-select>
@stop

@section('script')
    @javascript('salesmen', $salesmen)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Customer/list.js') }}"></script>
@stop
