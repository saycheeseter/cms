<datatable
    v-bind:tableId="datatable.transactions.settings.id"
    v-bind:settings="datatable.transactions.settings"
    v-bind:table="datatable.transactions.value"
    v-on:destroy="confirmRemoveTransaction">
    <template slot="header">
        <table-header>@lang('core::label.transaction.date')</table-header>
        <table-header>@lang('core::label.sheet.no')</table-header>
        <table-header>@lang('core::label.term')</table-header>
        <table-header>@lang('core::label.dr.no')</table-header>
        <table-header>@lang('core::label.remarks')</table-header>
        <table-header>@lang('core::label.amount')</table-header>
        <table-header>@lang('core::label.paid.amount')</table-header>
        <table-header>@lang('core::label.pay.amount')</table-header>
        <table-header v-show='!disabled'></table-header>
        <table-header>@lang('core::label.remaining')</table-header>
        <table-header v-show='!disabled'></table-header>
    </template>
    <template slot="display" scope="row">
        <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.transaction_date }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.sheet_number }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--term">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.term }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--sheetNo">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.dr_no }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--desc">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remarks }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.amount }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.paid_amount }}
                </template>
            </table-cell-data>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <number-input
                v-model="row.data.payable"
                :unsigned="isPurchaseInbound(row.data.reference_type)"
                :disabled='disabled'
                :max="isPurchaseInbound(row.data.reference_type) ? row.data.remaining : 0"
                :min="isPurchaseInbound(row.data.reference_type) ? 0 : negativeRemainingAmount(row.data.remaining)"
            ></number-input>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--action" v-show='!disabled'>
            <i class="fa fa-arrow-circle-left fa-2x" @click='fillRemainingPayableTransaction(row.index, row.data.reference_type)'></i>
        </td>
        <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
            <table-cell-data>
                <template scope="content">
                    @{{ row.data.remaining }}
                </template>
            </table-cell-data>
        </td>
    </template>
</datatable>