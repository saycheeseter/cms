<control-bar>
    <template slot="left">
        <control-button v-show='!disabled' icon="fa fa-money" @click="showPurchaseInboundTransactionSelector()">
            @lang('core::label.purchase.inbound.selector')
        </control-button>
        <control-button v-show='!disabled' icon="fa fa-money" @click="showPurchaseReturnOutboundTransactionSelector()">
            @lang('core::label.purchase.return.outbound.selector')
        </control-button>
    </template>
    <template slot="right">
        @include('core::attachment.transaction')
        <control-button name='save-add-new' v-if="controls.draft" icon="fa fa-save" @click='process(true)'>
            @lang('core::label.save.and.add.new')
        </control-button>
        <control-button name='save' v-if="controls.draft" icon="fa fa-save" @click="process">
            {{-- @lang('core::label.save') --}}
        </control-button>

        @if($permissions['update'])
            <control-button name='for-approval' v-if="controls.forApproval" icon="fa fa-thumbs-o-up" @click="forApprovalTransaction()">
                @lang('core::label.for.approval')
            </control-button>
        @endif

        @if($permissions['approve'])
            <control-button name='approve' v-if="controls.approveDecline" icon="fa fa-check fa-lg fa-green" @click="approveTransaction()">
                @lang('core::label.approve')
            </control-button>
        @endif

        @if($permissions['decline'])
            <control-button name='decline' v-if="controls.approveDecline" icon="fa fa-close fa-lg fa-red" @click="declineTransaction()">
                @lang('core::label.decline')
            </control-button>
        @endif

        @if($permissions['decline'] || $permissions['approve'])
            <control-button name='revert' v-if="controls.revert" icon="fa fa-arrow-circle-left" @click="revertTransaction()">
                @lang('core::label.revert')
            </control-button>
        @endif
    </template>
</control-bar>