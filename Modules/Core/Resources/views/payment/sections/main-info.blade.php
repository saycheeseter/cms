<div class="c-content-group c-tb c-lr">
    <div class="c-content-group clearfix">
        <div class="pull-right">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.date.time'):</td>
                    <td>
                        <date-time-picker
                            id='main-info-datetimepicker'
                            name='date-time'
                            v-model='info.transaction_date'
                            :date='datetimepicker.date'
                            :disabled='true'
                            :left='true'>
                        </date-time-picker>
                    </td>
                </tr>
            </table>
        </div>
        <div class="pull-left">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.sheet.no'):</td>
                    <td><b><span>@{{ info.sheet_number }}</span></b></td>
                </tr>
                <tr>
                    <td>@lang('core::label.for.branch'):</td>
                    <td><chosen
                        name='created-for'
                        v-model='info.created_for'
                        :options='references.branches'
                        :disabled='disabled'
                        @change='notify()'>
                    </chosen></td>
                </tr>
            </table>
        </div>
        <div class="pull-left c-mgl-5">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.supplier'):</td>
                    <td>
                        <chosen
                            class="required"
                            name='supplier'
                            v-model='info.supplier_id'
                            :options='references.suppliers'
                            :disabled='disabled'
                            @change='notify("supplier")'>
                        </chosen>
                    </td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.remarks'):</td>
                    <td><textarea v-model='info.remarks' rows="3" :disabled='disabled'></textarea></td>
                </tr>
            </table>
        </div>
        {{--<div class="pull-left c-mgl-5">--}}
            {{--<table class="c-tbl-layout">--}}
                {{--<tr>--}}
                    {{--<td>@lang('core::label.suppliers.account.balance'):</td>--}}
                    {{--<td>@{{ accountBalance }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>@lang('core::label.remaining.supplier.balance'):</td>--}}
                    {{--<td>@{{ actualAccountBalance }}</td>--}}
                {{--</tr>--}}
            {{--</table>--}}
        {{--</div>--}}
    </div>
</div>
<div id='status-label' class="label status-label" :class='approvalClass'>@{{ info.approval_status_label }}</div>