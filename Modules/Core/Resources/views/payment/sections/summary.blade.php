<div class="c-content-group c-tb c-lr clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.selected.payable.amount'):</td>
                <td><b><span>@{{ totalSelectedTransactionAmount }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.pay.amount'):</td>
                <td><b><span>@{{ totalPaymentAmount }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.remaining.amount'):</td>
                <td><b><span>@{{ totalRemainingAmount }}</span></b></td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-30">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.total.selected.amount'):</td>
                <td><b><span>@{{ totalSelectedAmount }}</span></b></td>
            </tr>
            <tr>
                <td>@lang('core::label.total.selected.remaining.amount'):</td>
                <td><b><span>@{{ totalSelectedRemainingAmount }}</span></b></td>
            </tr>
        </table>
    </div>
</div>