@extends('core::layouts.master')
@extends('core::layouts.flexible.four-container')
@section('title', trans('core::label.payment.detail'))

@section('stylesheet')
    <style type="text/css">
        .container-1 {
            flex: 0 0 auto;
            overflow: initial;
        }
        .container-2 .v-dataTable .v-dataTable__pagination{
            border-bottom: none;
        }
        .container-4 {
            max-height: 82px;
        }
    </style>
@stop

@section('container-1')
    @include('core::payment.sections.main-info')
@stop

@section('container-2')
    @include('core::payment.sections.controls')
    @include('core::payment.sections.transactions')
@stop

@section('container-3')
    @include('core::payment.sections.payments')
@stop

@section('container-4')
    @include('core::payment.sections.summary')
@stop

@section('message')
    <message type="danger" :show="message.transaction.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.transaction.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.transaction.success != ''">
        <span v-text="message.transaction.success"></span>
    </message>
    <message type="info" :show="message.transaction.info != ''">
        <span v-text="message.transaction.info"></span>
    </message>
@stop

@section('modals')
    <dialog-box
        :show='dialog.transaction.message !=""'
        @close='redirect'>
        <template slot='content'>@{{ dialog.transaction.message }}</template>
    </dialog-box>

    @include('core::payment.sections.transaction-selector')

    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'confirm.deleteTransactions.visible',
        'callback' => 'removeTransaction',
        'header' => trans('core::label.remove.transaction'),
        'content' => trans('core::confirm.remove.transaction')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'confirm-details',
        'show' => 'confirm.deletePayments.visible',
        'callback' => 'removePayment',
        'header' => trans('core::label.remove.payment'),
        'content' => trans('core::confirm.remove.payment')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'warning',
        'show' => 'confirm.warning.visible',
        'callback' => 'removeTransactionsAndPayments',
        'header' => trans('core::label.warning'),
        'cancel' => 'revertInfo',
        'content' => trans('core::confirm.changing.will.remove.payments')
    ])
    @endcomponent

    <modal
        v-show='dialog.directApproval.visible'
        @close='redirect'>
        <template slot="header">
            @{{ dialog.directApproval.message }}
        </template>
        <template slot="content">
            @lang('core::info.direct.approve')
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-blue" @click='directApproval()'>@lang('core::label.approve')</button>
            <button class="c-btn c-dk-green" ref="saveOnly" @click='redirect'>@lang('core::label.proceed')</button>
        </template>
    </modal>
@stop

@section('script')
    @javascript('permissions', $permissions)
    @javascript('suppliers', $suppliers)
    @javascript('branches', $branches)
    @javascript('paymentMethods', $paymentMethods)
    @javascript('bankAccounts', $bankAccounts)
    @javascript('id', Request::segment(2))
    @javascript('types', $types)

    <script src="{{ elixir('js/Payment/detail.js') }}"></script>
@stop