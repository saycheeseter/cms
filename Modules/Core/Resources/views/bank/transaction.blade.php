@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.bank.transaction'))

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='column-settings' icon="fa fa-gear" @click="columnSettings.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm"
        v-on:update="process"
        v-on:add="process">
        <template slot="header">
            <table-header v-show='columnSettings.selections.type'>@lang('core::label.type')</table-header>
            <table-header v-show='columnSettings.selections.date'>@lang('core::label.date')</table-header>
            <table-header>@lang('core::label.amount')</table-header>
            <table-header v-show='columnSettings.selections.from_bank'>@lang('core::label.from.bank')</table-header>
            <table-header v-show='columnSettings.selections.to_bank'>@lang('core::label.to.bank')</table-header>
            <table-header v-show='columnSettings.selections.remarks'>@lang('core::label.remarks')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--type v-dataTable__tableCell--chosen" v-show='columnSettings.selections.type'>
                <select v-model='row.data.type' @change="(permissions.update) ? changeState(row.index, 'update') : false">
                    <option v-for="option in transactionType" v-bind:value="option.id">
                        @{{ option.label }}
                    </option>
                </select>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.date'>
                <datepicker format="yyyy-MM-dd" v-model='row.data.transaction_date' @change="(permissions.update) ? changeState(row.index, 'update') : false"></datepicker>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <number-input v-model="row.data.amount" :unsigned="true" @keydown.native="(permissions.update) ? changeState(row.index, 'update') : false"/></number-input>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name v-dataTable__tableCell--chosen" v-show='columnSettings.selections.from_bank'>
                <chosen :options='references.bankAccounts' v-model='row.data.from_bank' v-show='isNotDeposit(row.data.type)' @selected="(permissions.update) ? changeState(row.index, 'update') : false">
                </chosen>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name v-dataTable__tableCell--chosen" v-show='columnSettings.selections.to_bank'>
                <chosen :options='references.bankAccounts' v-model='row.data.to_bank' v-show='isNotWithdrawal(row.data.type)' @selected="(permissions.update) ? changeState(row.index, 'update') : false">
                </chosen>
            </table-input>
            <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.selections.remarks'>
                <input type="text" v-model='row.data.remarks' @keydown="(permissions.update) ? changeState(row.index, 'update') : false"/>
            </table-input>
        </template>
        @if($permissions['create'])
            <template slot="input">
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--type v-dataTable__tableCell--chosen" v-show='columnSettings.selections.type'>
                    <select v-model='fields.type'>
                        <option v-for="option in transactionType" v-bind:value="option.id">
                            @{{ option.label }}
                        </option>
                    </select>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" v-show='columnSettings.selections.date'>
                    <datepicker format="yyyy-MM-dd" v-model='fields.transaction_date'></datepicker>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input v-model="fields.amount" :unsigned="true"/></number-input>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name v-dataTable__tableCell--chosen" v-show='columnSettings.selections.from_bank'>
                    <chosen :options='references.bankAccounts' v-model='fields.from_bank' v-show='isNotDeposit(fields.type)'>
                    </chosen>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name v-dataTable__tableCell--chosen" v-show='columnSettings.selections.to_bank'>
                    <chosen :options='references.bankAccounts' v-model='fields.to_bank' v-show='isNotWithdrawal(fields.type)'>
                    </chosen>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--desc" v-show='columnSettings.selections.remarks'>
                    <input type="text" v-model='fields.remarks'/>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <img src="{{ asset('images/iconupdate.png') }}" @click='process()'>
                </table-input>
            </template>
        @endif
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="paginate">
    </filter-modal>

    <column-select
        id='column-modal'
        :columns='columnSettings.columns'
        v-show='columnSettings.visible'
        @close="columnSettings.visible = false"
        v-model='columnSettings.selections'
        :module='columnSettings.module'>
    </column-select>

    @component('core::components.confirm', [
        'id'   => 'confirm',
        'callback' => 'destroy',
        'show' => 'modal.confirm.visible',
        'header' => trans('core::label.delete.bank.transaction'),
        'content' => trans('core::confirm.delete.bank.transaction')
    ])
    @endcomponent
@stop

@section('script')
    @stack('script')
    @javascript('branches', $branches)
    @javascript('bankAccounts', $bankAccounts)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Bank/transaction.js') }}"></script>
@stop