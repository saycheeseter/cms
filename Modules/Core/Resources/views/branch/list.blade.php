@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.branch'))

@section('stylesheet')
    <style type="text/css">
        #modal-detail .modal-container {
            width: 630px;
        }
        #modal-detail .v-dataTable {
            height: 200px;
            border-left: 1px #ccc solid;
            border-right: 1px #ccc solid;
        }
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            @if ($permissions['create'])
                <control-button name="add" icon="fa fa-plus" @click="modal.detail.visible = true">
                    @lang('core::label.add.new.branch')
                </control-button>
            @endif
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="columnSettings.branch.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.list.id"
        v-bind:settings="datatable.list.settings"
        v-bind:table="datatable.list.value"
        v-on:reload="paginate"
        v-on:destroy="confirm">
        <template slot="header">
            <table-header>
                @lang('core::label.code')
            </table-header>
            <table-header v-show='columnSettings.branch.selections.name'>
                @lang('core::label.name')
            </table-header>
            <table-header v-show='columnSettings.branch.selections.address'>
                @lang('core::label.address')
            </table-header>
            <table-header v-show='columnSettings.branch.selections.contact'>
                @lang('core::label.contact')
            </table-header>
            <table-header v-show='columnSettings.branch.selections.businessName'>
                @lang('core::label.business.name')
            </table-header>
            <table-header>
            </table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--code" @click="show(row.data.id)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.branch.selections.name'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--address" @click="show(row.data.id)" v-show='columnSettings.branch.selections.address'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.address }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="show(row.data.id)" v-show='columnSettings.branch.selections.contact'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.contact }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="show(row.data.id)" v-show='columnSettings.branch.selections.businessName'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.business_name }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.list.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.list.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.list.success != ''">
        <span v-text="message.list.success"></span>
    </message>
    <message type="info" :show="message.list.info != ''">
        <span v-text="message.list.info"></span>
    </message>
@stop

@section('modals')
    @include('core::branch.detail')

    @component('core::components.confirm', [
        'id' => 'confirm-list',
        'show' => 'modal.list.confirm.visible',
        'callback' => 'destroy',
        'header' => trans('core::label.delete.branch'),
        'content' => trans('core::confirm.delete.branch')
    ])
    @endcomponent

    @component('core::components.confirm', [
        'id' => 'confirm-detail',
        'show' => 'modal.detail.confirm.visible',
        'callback' => 'destroyDetail',
        'header' => trans('core::label.delete.branch'),
        'content' => trans('core::confirm.delete.branch')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model="modal.filter.data"
        v-on:search="paginate">
    </filter-modal>

    <column-select
        :columns='columnSettings.branch.columns'
        v-show='columnSettings.branch.visible'
        @close="columnSettings.branch.visible = false"
        id = 'column-modal'
        v-model='columnSettings.branch.selections'
        :module='columnSettings.branch.module'>
    </column-select>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/Branch/list.js') }}"></script>
@stop
