<modal
    v-bind:id="modal.detail.id"
    v-show="modal.detail.visible"
    v-on:open="modal.detail.visible = true"
    v-on:close="modal.detail.visible = false">
    <template slot="header">
        @lang('core::label.branch.detail')
    </template>
    <template slot="content">
        <message type="danger" :show="message.detail.error.any()" @close="message.reset()">
            <ul>
                <li v-for="(value, key) in message.detail.error.get()" v-text="value"></li>
            </ul>
        </message>
        <message type="success" :show="message.detail.success != ''">
            <span v-text="message.detail.success"></span>
        </message>
        <message type="info" :show="message.detail.info != ''">
            <span v-text="message.detail.info"></span>
        </message>
        <div class="c-content-group c-tb">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.code'):</td>
                    <td><input type="text" class="required" name="info-code" v-model="branch.info.code"></td>
                    <td>@lang('core::label.name'):</td>
                    <td><input type="text" class="required" name="info-name" v-model="branch.info.name"></td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.business.name'):</td>
                    <td valign="top"><input name="info-business-name" type="text" v-model="branch.info.business_name"></td>
                    <td>@lang('core::label.contact'):</td>
                    <td><input type="text" name="info-contact" v-model="branch.info.contact"></td>
                </tr>
                <tr>
                    <td valign="top">@lang('core::label.address'):</td>
                    <td valign="top">
                        <textarea rows="3" name="info-address" v-model="branch.info.address"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div class="c-content-group c-t flex flex-column flex-1 overflow-hidden">
            <datatable
                v-bind:tableId="datatable.detail.id"
                v-bind:settings="datatable.detail.settings"
                v-bind:table="datatable.detail.value"
                v-on:destroy="confirmDetail"
                v-on:update="updateDetail"
                ref="detail">
                <template slot="header">
                    <table-header>@lang('core::label.code')</table-header>
                    <table-header>@lang('core::label.name')</table-header>
                    <table-header>@lang('core::label.contact')</table-header>
                    <table-header>@lang('core::label.address')</table-header>
                    <table-header>@lang('core::label.business.name')</table-header>
                    <table-header></table-header>
                </template>
                <template slot="display" scope="row">
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--code">
                        <input type="text" class="required" v-model="row.data.code" @keydown="changeState(row.index, 'update', 'detail')" />
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                        <input type="text" class="required" v-model="row.data.name" @keydown="changeState(row.index, 'update', 'detail')"/>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--contact">
                        <input type="text" v-model="row.data.contact" @keydown="changeState(row.index, 'update', 'detail')" />
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--address">
                        <input type="text" v-model="row.data.address" @keydown="changeState(row.index, 'update', 'detail')"/>
                    </td>
                    <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                        <input type="text" v-model="row.data.business_name" @keydown="changeState(row.index, 'update', 'detail')"/>
                    </td>
                </template>
                @if ($permissions['create']))
                    <template slot="input">
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--code">
                            <input v-focus v-model="branch.detail.code" class="required" type="text" />
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                            <input v-model="branch.detail.name" class="required" type="text"/>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--contact">
                            <input v-model="branch.detail.contact" type="text"/>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--address">
                            <input v-model="branch.detail.address" type="text"/>
                        </table-input>
                        <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                            <input v-model="branch.detail.business_name" type="text"/>
                        </table-input>
                        <table-input class="v-dataTable__tableCell">
                            <img src="{{ asset('images/iconupdate.png') }}" @click="createDetail()">
                        </table-input>
                    </template>
                @endif
            </datatable>
        </div>
    </template>
    <template slot="footer">
        <input type="button" name="save" class="c-btn c-dk-green" value="@lang('core::label.save')" @click="validate">
    </template>
</modal>