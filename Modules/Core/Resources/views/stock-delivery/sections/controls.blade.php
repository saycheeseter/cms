@extends('core::invoice.sections.controls')

@push('controls-left')
    <control-dropdown name='import-purchase-inbound' icon="fa fa-upload" v-if="!disabled" v-show="direct">
        @lang('core::label.purchase.inbound')
        <template slot="list">
            <li class="radio-checkbox dropdown-form">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.transaction'):</td>
                        <td><input name='purchase-inbound-reference-number' placeholder="Sheet Number" v-model='importing.purchaseInbound.reference' type="text" @keydown.enter="getPurchaseInboundTransaction"></td>
                        <td><input name='purchase-inbound-import-reference' type="button" class="c-btn c-dk-blue" value="@lang('core::label.import')" @click="getPurchaseInboundTransaction"></td>
                    </tr>
                </table>
            </li>
        </template>
    </control-dropdown>
@endpush