@extends('core::invoice.detail')
@section('title', trans('core::label.stock.delivery.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.deliver.to'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to'
                        v-model='info.deliver_to' 
                        :options='reference.branches'
                        @change="getDeliveryLocations"
                        :disabled='fromInvoice || disabled'
                    ></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.deliver.to.location'):</td>
                <td>
                    <chosen
                        class="required"
                        name='deliver-to-location'
                        v-model='info.deliver_to_location' 
                        :options='reference.deliveryLocations'
                        :disabled='fromInvoice || disabled'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
    <div class="pull-right">
        <table class="c-tbl-layout pull-right">
            <tr>
                <td>
                    <label class="radio-chkbx">
                        <input 
                            type="radio" 
                            name="transaction-type" 
                            value="0"
                            v-model="info.transaction_type" 
                            @change='notify("transaction-type")'
                            :disabled='disabled'
                        >
                        <span>@lang('core::label.direct')</span>
                    </label>
                </td>
                <td>
                    <label class="radio-chkbx c-mgl-5">
                        <input 
                            type="radio" 
                            name="transaction-type"
                            value="1"
                            v-model="info.transaction_type" 
                            @change='notify("transaction-type")'
                            :disabled='disabled'
                        >
                        <span>@lang('core::label.from.invoice')</span>
                    </label>
                </td>
            </tr>
        </table>
        <div class="c-content-group">
            <table class="c-tbl-layout" v-show='fromInvoice'>
                <tr>
                    <td>@lang('core::label.invoice'):</td>
                    <td>
                        <chosen 
                            name='stock-request-invoice'
                            v-model='info.reference_id'
                            :callback='getStockRequest'
                            @update:options='val => reference.requests = val'
                            @change='notify("stock-request-invoice")'
                            @click='buffer.reference.requests = reference.requests.slice(0)'
                            :options='reference.requests'
                            :disabled='disabled'>
                        </chosen>
                    </td>
                    <td>
                    </td>
                    <td>
                        <button
                            class="c-btn c-lt-green"
                            @click="showInvoiceSelector"
                            v-show='!disabled'>
                            <i class="fa fa-list-alt"></i>
                        </button>
                        <input type="button" name='stock-request-import' class="c-btn c-lt-green" value="@lang('core::label.import')" :disabled='disabled' @click='retrieve'>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@stop

@include('core::inventory-flow.sections.detail.sales-outbound-import')

@section('container-2')
    @include('core::stock-delivery.sections.controls')
    @include('core::stock-delivery.sections.details')
@stop

@push('modals')
    @include('core::inventory-chain.sections.invoice-selector')

    <filter-modal
        v-show="selector.invoice.filter.visible"
        @close="selector.invoice.filter.visible = false"
        :filters="selector.invoice.filter.options"
        v-model='selector.invoice.filter.data'
        v-on:search="getInvoicesForSelector">
    </filter-modal>
@endpush

@push('script')
    <script src="{{ elixir('js/StockDelivery/detail.js') }}"></script>
@endpush