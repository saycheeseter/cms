@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.bank.account'))

@section('stylesheet')
    <style type="text/css">
    </style>
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="left">
            <control-button name="filters" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm"
        v-on:update="validate"
        v-on:add="validate">
        <template slot="header">
            <table-header>@lang('core::label.bank')</table-header>
            <table-header>@lang('core::label.account.number')</table-header>
            <table-header>@lang('core::label.account.name')</table-header>
            <table-header>@lang('core::label.opening.balance')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name">
                <chosen class="required" name="banks" :options='banks' v-model='row.data.bank_id' @selected="changeState(row.index, 'update')"></chosen>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--accountNo">
                <input class="required" type="text" v-model="row.data.account_number" @keydown="changeState(row.index, 'update')" />
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name">
                <input class="required" type="text" v-model="row.data.account_name" @keydown="changeState(row.index, 'update')"/>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                <number-input name="opening-balance" v-model="row.data.opening_balance" @keydown.native="changeState(row.index, 'update')"/></number-input>
            </td>
        </template>
        @if ($permissions['create'])
            <template slot="input">
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--chosen v-dataTable__tableCell--name">
                    <chosen name="banks" :options='banks' v-model='fields.bank_id'></chosen>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--accountNo">
                    <input v-focus v-model="fields.account_number" type="text" />
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--name">
                    <input v-model="fields.account_name" type="text" />
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--amount">
                    <number-input name="opening-balance" v-model="fields.opening_balance" @keydown.native.enter="validate()" /></number-input>
                </table-input>
                <table-input class="v-dataTable__tableCell v-dataTable__tableCell--action">
                    <img src="{{ asset('images/iconupdate.png') }}" @click="validate()">
                </table-input>
            </template>
        @endif
    </datatable>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('loading')
    @include('core::common.loading', [
        'show' => 'processing'
    ])
@stop

@section('modals')
    @component('core::components.confirm', [
        'id'   => 'confirm',
        'callback' => 'destroy',
        'show' => 'modal.confirm.visible',
        'header' => trans('core::label.delete.bank.account'),
        'content' => trans('core::confirm.delete.bank.account')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>
@stop

@section('script')
    @javascript('banks', $banks)
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/BankAccount/list.js') }}"></script>
@stop