@extends('core::inventory-chain.detail')
@section('title', trans('core::label.damage.outbound.detail'))

@section('main-info')
    <div class="pull-left c-mgl-5">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.reason'):</td>
                <td>
                    <chosen
                        class="required"
                        name='reason'
                        v-model='info.reason_id'
                        :options='reference.reasons'
                        :disabled='!updatable'
                    ></chosen>
                </td>
            </tr>
        </table>
    </div>
@stop

@push('script')
    @javascript('reasons', $reasons)

    <script src="{{ elixir('js/DamageOutbound/detail.js') }}"></script>
@endpush