@extends('core::layouts.master')
@section('title', trans('core::label.login'))

@section('stylesheet')
    <style type="text/css">
        .branch-dropdown {
            min-width: 212px !important;
            max-width: 212px !important;
        }
        .revoked-message {
            color: red;
            font-size: 12px;
        }
    </style>
@stop

@section('content')
    <div class="c-login-content">
        <div class="c-login">
            <form method="POST">
                <div class="c-login-form" align="center">
                    <div class="c-login-logo">
                        <img src="{{ asset('images/Nelsoft_Logo_Landscape_4.png') }}">
                    </div>
                    <div class="form-group">
                        @if(Session::has('revoked'))
                            <message ref="revokeMessage" type="danger" :timer="false" :show="true">
                                @lang('core::info.you.have.been.logged.out')
                                (@lang('core::label.ip.address'): {{ Session::get('revoked')['ip_address'] }}),
                                (@lang('core::label.user.agent'): {{ Session::get('revoked')['user_agent'] }})
                            </message>
                        @endif

                        <message type="danger" :show="message.form.error.any()" @close="message.reset()">
                            <ul>
                                <li v-for="(value, key) in message.form.error.get()" v-text="value"></li>
                            </ul>
                        </message>
                        <message type="success" :show="message.form.success != ''">
                            <span v-text="message.form.success"></span>
                        </message>
                        <message type="info" :show="message.form.info != ''">
                            <span v-text="message.form.info"></span>
                        </message>
                    </div>
                    <div class="form-group">
                        <input type="text"
                            ref="username"
                            name="username"
                            class="form-control c-input"
                            placeholder="@lang('core::label.username')"
                            v-model="credentials.username"
                            @keyup.enter="$refs.password.focus()"
                        />
                    </div>
                    <div class="form-group">
                        <input type="password"
                            ref="password"
                            name="password"
                            class="form-control c-input"
                            placeholder="@lang('core::label.password')"
                            v-model="credentials.password"
                            @keydown.enter="login"
                        />
                    </div>
                    <div class="form-group">
                        <input type="button" name="login" @click="login" :disabled="processing" class="c-login-btn" value="@lang('core::label.login')" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('footer')
    @include('core::layouts.footer')
@stop

@section('modals')
    <modal id="branch-modal" v-show="modal.visible" @close="modal.visible = false">
        <template slot="header">@lang('core::label.select.branch')</template>
        <template slot="content">
            <message type="danger" v-show="message.branch.error.any()" @close="message.reset()">
                <ul>
                    <li v-for="(value, key) in message.branch.error.get()" v-text="value"></li>
                </ul>
            </message>
            <message type="success" v-show="message.branch.success != ''">
                <span v-text="message.branch.success"></span>
            </message>
            <message type="info" v-show="message.branch.info != ''">
                <span v-text="message.branch.info"></span>
            </message>
            <div class="c-content-group c-t">
                <table class="c-tbl-layout">
                    <tr>
                        <td>@lang('core::label.branch'):</td>
                        <td>
                            <chosen
                                class="branch-dropdown"
                                name="branches"
                                v-model="branch.id"
                                :options="branches"
                            ></chosen>
                        </td>
                    </tr>
                </table>
            </div>
        </template>
        <template slot="footer">
            <input type="button" name="auth" ref="authenticate" class="c-btn c-dk-green" value="@lang('core::label.login')" @click="authenticate" :disabled="processing">
        </template>
    </modal>
@stop

@section('script')
    <script src="{{ elixir('js/Login/auth.js') }}"></script>
@stop