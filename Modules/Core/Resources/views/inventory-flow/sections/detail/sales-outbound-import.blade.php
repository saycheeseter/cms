@push('controls-left')
    <control-button icon="fa fa-download" v-if="enableSalesOutboundImport" @click="modal.salesOutbound.visible = true">
        @lang('core::label.sales.outbound')
    </control-button>
@endpush

@push('modals')
    <modal
        id="stock-request-filters"
        v-show="modal.salesOutbound.visible"
        @close="closeSalesOutboundImport">
        <template slot="header">
            @lang('core::label.import') @lang('core::label.sales.outbound')
        </template>
        <template slot="content">
            <div class="c-content-group c-b">
                <table class="c-tbl-layout">
                    <tr>
                        <td>
                            <label class="radio-chkbx">
                                <input type="radio" value="transaction-date" v-model="modal.salesOutbound.option.type">
                                @lang('core::label.transaction.date')
                            </label>
                            <label class="radio-chkbx c-mgl-10">
                                <input type="radio" value="sheet-number" v-model="modal.salesOutbound.option.type">
                                @lang('core::label.sheet.number')
                            </label>
                        </td>
                    </tr>
                </table>

                <table class="c-tbl-layout" v-show="salesOutboundByDate">
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td><date-time-picker id="date-from" name='date-from' v-model='modal.salesOutbound.option.date.from' :date="{ format: 'yyyy-MM-dd' }"></date-time-picker></td>
                        <td>@lang('core::label.to'):</td>
                        <td><date-time-picker id="date-to" name='date-to' v-model='modal.salesOutbound.option.date.to' :date="{ format: 'yyyy-MM-dd' }"></date-time-picker></td>
                    </tr>
                </table>

                <table class="c-tbl-layout" v-show="salesOutboundBySheetNumber">
                    <tr>
                        <td>@lang('core::label.sheet.number.prefix'):</td>
                        <td><input name='sheet-number_prefix' v-model="modal.salesOutbound.option.sheetNumber.prefix" type="text" placeholder="@lang('core::label.sheet.number.prefix')"></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.from'):</td>
                        <td><number-input :precision="false" :formatted="false" v-model="modal.salesOutbound.option.sheetNumber.from" type="text" placeholder="@lang('core::label.sheet.number')"></number-input></td>
                    </tr>
                    <tr>
                        <td>@lang('core::label.to'):</td>
                        <td><number-input :precision="false" :formatted="false" v-model="modal.salesOutbound.option.sheetNumber.to" type="text" placeholder="@lang('core::label.sheet.number')"></number-input></td>
                    </tr>
                </table>
            </div>

            <div class="c-content-group c-t">
                @lang('core::label.approval.status'):
                <table>
                    <tr>
                        <td>
                            <label>
                                <input type='checkbox' :value="reference.approvalStatus.DRAFT" v-model="modal.salesOutbound.option.statuses">
                                @lang('core::label.draft')
                            </label>
                            <label class="c-mgl-10">
                                <input type='checkbox' :value="reference.approvalStatus.FOR_APPROVAL" v-model="modal.salesOutbound.option.statuses">
                                @lang('core::label.for.approval')
                            </label>
                            <label class="c-mgl-10">
                                <input type='checkbox' :value="reference.approvalStatus.APPROVED" v-model="modal.salesOutbound.option.statuses">
                                @lang('core::label.approved')
                            </label>
                            <label class="c-mgl-10">
                                <input type='checkbox' :value="reference.approvalStatus.DECLINED" v-model="modal.salesOutbound.option.statuses">
                                @lang('core::label.declined')
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </template>

        <template slot="footer">
            <input type="button" class="c-btn c-dk-green" value="@lang('core::label.import')" @click="importFromSalesOutbound">
        </template>
    </modal>
@endpush