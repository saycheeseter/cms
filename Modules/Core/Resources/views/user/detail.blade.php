@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.user.detail'))

@section('stylesheet')
@stop

@section('container-1')
    <control-bar id="controls">
        <template slot="right">
            <control-button name='save' icon="fa fa-save" @click='validate()'>
                @lang('core::label.save')
            </control-button>
        </template>
    </control-bar>
    <div class="c-nav-tabs clearfix">
        <div class="c-nav-tab" v-for="(tab, key) in tabs.content.get()"
            :name='key' 
            v-text="tab.label" 
            :class="{ active: tab.active }" 
            @click="tabs.content.active(key)">
        </div>
    </div>
    <div class="c-nav-tabs-container clearfix flex flex-1 flex-column overflow-hidden">
        <div class="user-info" v-show="tabs.content.info.active">
            @include('core::user.info')
        </div>
        <div class="user-permissions c-nav-tabs-content" v-show="tabs.content.permissions.active">
            @include('core::user.permissions')
        </div>
    </div>
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible', 
        'callback' => 'getRolePermissions',
        'cancel' => 'cancel',
        'close' => 'cancel',
        'header' => Lang::get('core::label.change.role'),
        'content' => Lang::get('core::confirm.changing.will.remove.permissions')
    ])
    @endcomponent

    <confirm
        id="allBranches"
        :show='allBranches.confirm'
        ok="@lang('core::label.yes')"
        cancel="@lang('core::label.no')"
        @ok="clearPermissions"
        @cancel="closeAllBranchConfirm"
        @close="closeAllBranchConfirm">
        <template slot="header">
            @lang('core::label.warning')
        </template>
        <template slot="content">
            @lang('core::confirm.this.will.clear.permissions')
        </template>
    </confirm>
@stop

@section('script')
    @javascript('id', Request::segment(2))
    @javascript('sections', $sections)
    @javascript('branches', $branches)
    @javascript('roles', $roles)
    @javascript('userPermissions', $permissions)

    <script src="{{ elixir('js/User/detail.js') }}"></script>
@stop
