<div class="c-content-group c-t c-lr clearfix">
    <div class="pull-left">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.branch'):</td>
                <td>
                    <chosen name='branch' :options='branches' v-model='selected.branch' @change='permissions' :disabled='allBranches.value'></chosen>
                </td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-15">
        <table class="c-tbl-layout">
            <tr>
                <td>
                    <label>
                        <input type="checkbox" v-model='allBranches.value' @click='onClickAllBranch'>
                        @lang('core::label.all.branches')
                    </label>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="c-sub-nav-content" v-show="selected.branch != null">
    <div class="c-sub-sidebar" id="sections">
        <ul class="c-ul-links">
            <li v-for="(section, key) in tabs.permissions.get()"
                :name="key"
                :class="{ active: section.active }"
                @click="tabs.permissions.active(key)"
                v-text="section.label">
            </li>
        </ul>
    </div>
    <div class="c-sub-main-content overflow-hidden">
        <div class="c-content" v-for="(section, keySection) in tabs.permissions.get()" v-show="section.active">
            <div class="c-sub-header c-has-bb clearfix">
                <label class="radio-chkbx">
                    <input type="checkbox" 
                        :name="keySection"
                        v-model="section.all"
                        @change="toggleSectionPermissions(keySection)">
                    <span>@{{ section.label }}</span>
                </label>
            </div>
            <div class="overflow">
                <div class="c-content-group c-t c-lr">
                    <div class="c-form-group clearfix" v-for="(module, keyModule) in section.modules">
                        <div class="c-content-group">
                            <label class="cursor-pointer">
                                <input type="checkbox"
                                    :name="keyModule"
                                    v-model="module.all"
                                    @change="toggleModulePermissions(keySection, keyModule)">
                                <b>@{{ module.label }}</b>
                            </label>
                        </div>
                        <template v-for="(permission, keyPermission) in module.permissions">
                            <template v-if="((keyPermission + 1) % 5) === 0 || (keyPermission + 1) === module.permissions.length">
                                <div class="pull-left c-mgl-15">
                                    <table>
                                        <tr v-for="n in range(getLowerRangePermission(keyPermission), (keyPermission + 1))">
                                            <td>
                                                <label class="radio-chkbx">
                                                    <input type="checkbox" class="permissions" :value="module.permissions[n].code" v-model="selected.code">
                                                    <span>@{{ module.permissions[n].label }}</span>
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </template>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>