<div class="c-content-group c-tb c-lr clearfix">
    <div class="pull-left">
        <div class="user-image">
            <img  v-if="attachment.files.attached.length == 0 " src="{{ asset('images/no-image.png') }}" class="img-thumbnail c-img-1x1">
            <img  v-if="attachment.files.attached.length > 0 " :src="attachment.files.attached[0].link" class="img-thumbnail c-img-1x1">
            <label class="c-btn c-dk-green" for="upload-photo">
                @lang('core::label.upload')
            </label>
            <input 
                type="file" 
                id="upload-photo" 
                name="attachments[]"
                accept="image/*"
                @change="filesChange($event.target.files);"
            />
        </div>
    </div>
    <div class="pull-left c-mgl-10">
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.user.code'):</td>
                <td><input class="required" name="code" type="text" v-model="user.info.code"></td>
                <td>@lang('core::label.username'):</td>
                <td><input class="required" name="user-name" type="text" v-model="user.info.username"></td>
            </tr>
            <tr>
                <td>@lang('core::label.full.name'):</td>
                <td><input class="required" name="full-name" type="text" v-model="user.info.full_name"></td>
                <td>@lang('core::label.password'):</td>
                <td><input class="required" name="password" type="password" v-model="user.info.password"></td>
            </tr>
            <tr>
                <td>@lang('core::label.license.key'):</td>
                <td colspan="3"><input class="required" type="text" v-model="user.info.license_key"></td>
            </tr>
            <tr>
                <td>@lang('core::label.status'):</td>
                <td>
                    <label class="radio-chkbx">
                        <input name="status" type="checkbox" v-model="user.info.status">
                        <span>@lang('core::label.active')</span>
                    </label>
                </td>
                <td>@lang('core::label.role'):</td>
                <td>
                    <chosen name='role' :options='roles' v-model='user.info.role_id' @change='confirmChangeRole'></chosen>
                </td>
            </tr>
            <tr>
                <td>@lang('core::label.telephone.no'):</td>
                <td><input name="telephone" type="text" v-model="user.info.telephone"></td>
                <td>@lang('core::label.mobile.no'):</td>
                <td><input name="mobile" type="text" v-model="user.info.mobile"></td>
            </tr>
            <tr>
                <td>@lang('core::label.position'):</td>
                <td><input name="position" type="text" v-model="user.info.position"></td>
                <td>@lang('core::label.email.address'):</td>
                <td><input name="email" type="text" v-model="user.info.email"></td>
            </tr>
            <tr>
                <td valign="top">@lang('core::label.address'):</td>
                <td colspan="3">
                    <textarea name="address" rows="3" v-model="user.info.address"></textarea>
                </td>
            </tr>
            <tr>
                <td valign="top">@lang('core::label.memo'):</td>
                <td colspan="3">
                    <textarea name="memo" rows="3" v-model="user.info.memo"></textarea>
                </td>
            </tr>
        </table>
    </div>
    <div class="pull-left c-mgl-10">
        <b>@lang('core::label.login.time.schedule')</b>
        <table>
            <tr>
                <td class="radio-chkbx">
                    <label>
                        <input type="checkbox" v-model="user.schedule.mon">
                        <span>@lang('core::label.monday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox" v-model="user.schedule.tue">
                        <span>@lang('core::label.tuesday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox" v-model="user.schedule.wed">
                        <span>@lang('core::label.wednesday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox" v-model="user.schedule.thu">
                        <span>@lang('core::label.thursday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox"  v-model="user.schedule.fri">
                        <span>@lang('core::label.friday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox"  v-model="user.schedule.sat">
                        <span>@lang('core::label.saturday')</span>
                    </label>
                </td>
                <td class="radio-chkbx c-pdl-5">
                    <label>
                        <input type="checkbox"  v-model="user.schedule.sun">
                        <span>@lang('core::label.sunday')</span>
                    </label>
                </td>
            </tr>
        </table>
        <table class="c-tbl-layout">
            <tr>
                <td>@lang('core::label.start'):</td>
                <td><timepicker2 v-model='user.schedule.start'></timepicker></td>
                <td>@lang('core::label.end'):</td>
                <td><timepicker2 v-model='user.schedule.end'></timepicker></td>
            </tr>
        </table>
    </div>
</div>