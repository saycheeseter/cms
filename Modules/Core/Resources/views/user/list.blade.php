@extends('core::layouts.master')
@extends('core::layouts.flexible.one-container', ['page_type' => 'scroll'])
@section('title', trans('core::label.user.list'))

@section('container-1')
    <control-bar id='controls'>
        <template slot="left">
            @if ($permissions['create'])
                <control-button name="create" icon="fa fa-plus" @click="create">
                    @lang('core::label.add.new.user')
                </control-button>
            @endif
            <control-button name="filter" icon="fa fa-search" @click="modal.filter.visible = true">
                @lang('core::label.filters')
            </control-button>
            <control-button name='columns' icon="fa fa-gear" @click="columnSettings.user.visible = true">
                @lang('core::label.column.settings')
            </control-button>
        </template>
    </control-bar>
    <datatable
        v-bind:tableId="datatable.id"
        v-bind:settings="datatable.settings"
        v-bind:table="datatable.value"
        v-on:reload="paginate"
        v-on:destroy="confirm">
        <template slot="header">
            <table-header>@lang('core::label.user.code')</table-header>
            <table-header v-show='columnSettings.user.selections.name'>@lang('core::label.name')</table-header>
            <table-header v-show='columnSettings.user.selections.username'>@lang('core::label.username')</table-header>
            <table-header v-show='columnSettings.user.selections.position'>@lang('core::label.position')</table-header>
            <table-header v-show='columnSettings.user.selections.status'>@lang('core::label.status')</table-header>
            <table-header v-show='columnSettings.user.selections.licenseKey'>@lang('core::label.license.key')</table-header>
            <table-header v-show='columnSettings.user.selections.mobile'>@lang('core::label.mobile')</table-header>
            <table-header v-show='columnSettings.user.selections.email'>@lang('core::label.email')</table-header>
            <table-header v-show='columnSettings.user.selections.address'>@lang('core::label.address')</table-header>
            <table-header v-show='columnSettings.user.selections.memo'>@lang('core::label.memo')</table-header>
            <table-header v-show='columnSettings.user.selections.scheduleStart'>@lang('core::label.schedule.start')</table-header>
            <table-header v-show='columnSettings.user.selections.scheduleEnd'>@lang('core::label.schedule.end')</table-header>
            <table-header></table-header>
        </template>
        <template slot="display" scope="row">
            <td class="v-dataTable__tableCell v-dataTable__tableCell--userCode" @click="view(row.data.id, row.data.type)">
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.code }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.name'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.full_name }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.username'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.username }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--position" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.position'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.position }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--status" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.status'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.status }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--name" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.licenseKey'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.license_key }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--contact" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.mobile'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.mobile }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--email" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.email'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.email }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--address" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.address'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.address }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--desc" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.memo'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.memo }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.scheduleStart'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.schedule_start }}
                    </template>
                </table-cell-data>
            </td>
            <td class="v-dataTable__tableCell v-dataTable__tableCell--dateTime" @click="view(row.data.id, row.data.type)" v-show='columnSettings.user.selections.scheduleEnd'>
                <table-cell-data>
                    <template scope="content">
                        @{{ row.data.schedule_end }}
                    </template>
                </table-cell-data>
            </td>
        </template>
    </datatable>
@stop

@section('loading')
    @include('core::common.loading', [
        'show' => 'processing'
    ])
@stop

@section('message')
    <message type="danger" :show="message.error.any()" @close="message.reset()">
        <ul>
            <li v-for="(value, key) in message.error.get()" v-text="value"></li>
        </ul>
    </message>
    <message type="success" :show="message.success != ''">
        <span v-text="message.success"></span>
    </message>
    <message type="info" :show="message.info != ''">
        <span v-text="message.info"></span>
    </message>
@stop

@section('modals')
    @component('core::components.confirm', [
        'id' => 'confirm',
        'show' => 'modal.confirm.visible', 
        'callback' => 'destroy',
        'header' => Lang::get('core::label.delete.user'),
        'content' => Lang::get('core::confirm.delete.user')
    ])
    @endcomponent

    <filter-modal
        v-show="modal.filter.visible"
        @close="modal.filter.visible = false"
        :filters="modal.filter.options"
        v-model='modal.filter.data'
        v-on:search="paginate">
    </filter-modal>

    <column-select
        :columns='columnSettings.user.columns'
        v-show='columnSettings.user.visible'
        @close="columnSettings.user.visible = false"
        id = 'column-modal'
        v-model='columnSettings.user.selections'
        :module='columnSettings.user.module'>
    </column-select>

    <modal
        v-show="modal.changeAdminPassword.visible"
        @close="closeChangeAdminPasswordForm()">
        <template slot="header">
            @lang('core::label.reset.admin.password')
        </template>
        <template slot="content">
            <table class="c-tbl-layout">
                <tr>
                    <td>@lang('core::label.password'):</td>
                    <td><input type="password" v-model="modal.changeAdminPassword.password"></td>
                </tr>
            </table>
        </template>
        <template slot="footer">
            <button class="c-btn c-dk-blue" @click="changeAdminPassword()">@lang('core::label.save')</button>
            <button class="c-btn c-dk-red" @click="closeChangeAdminPasswordForm()">@lang('core::label.cancel')</button>
        </template>
    </modal>
@stop

@section('script')
    @javascript('permissions', $permissions)

    <script src="{{ elixir('js/User/list.js') }}"></script>
@stop
