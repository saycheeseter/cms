<?php

return [
    'big.qty.latest.purchase.receive' => '注：单位数量是按照最新的采购入库的历史数据。',
    'changing.diff.qty' => 'Note: For Serial and Batch Products, upon change of QTY fields, encoded or selected entries will be cleared.',
    'discount.basis' => 'Basis: Used to indicate where current discount will base on. Note: Basis of (-1) will point to the base price.',
    'discount.position' => 'Position: Used to set the hierarchy of the discounts.',
    'discount.value' => 'Value: Value of the discount in decimal.',
    'purchase.price.latest.purchase.receive' => '注：采购价是按照最新的采购入库的历史数据。',
    'purchased.and.received.warning.days' => '注: 系统将会显示警告如果相同商品在__天内有采购并入库过。',
    'purchased.not.received.warning.days' => '注: 系统将会显示警告如果相同商品在__天内有采购但未入库过。',
    'purchased.warning.days' => '注: 系统将会显示警告如果相同商品在__天内有开过采购单。',
    'received.above.articles.good.condition' => 'Received the above articles in good order and condition.',
    'received.good.condition' => 'Received in good condition.',
    'repeated.purchases.warnings' => '显示重复采购过的产品提示功能。可按照以下不同的规定。',
    'repeated.requests.warnings' => '显示重负要求过的产品提示功能。可按照以下不同的规定。',
];