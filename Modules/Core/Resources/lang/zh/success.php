<?php

return [
    'apply.purchase.price' => 'Purchase price succesfully applied.',
    'attachment.temporary.uploaded' => '附件已暂时保存。',
    'attachment.uploaded' => '附件已上传。',
    'changed' => '已更改。',
    'connected' => '已连接。',
    'created' => '已创建。',
    'custom.menu.saved' => '自定义菜单已存。',
    'deleted' => '已删除。',
    'gift.check.successfully.redeemed' => '礼券已领取。',
    'inserted' => 'Successfully inserted.',
    'password.changed' => '密码已更换。',
    'saved' => '保存成功。',
    'summary.generated' => '系统结算成功。',
    'synced' => '已同步。',
    'transaction.successfully.imported' => '交易单已导入。',
    'transferred' => 'Transferred successfully.',
    'updated' => '已更新。',
];