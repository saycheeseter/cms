<?php

return [
    'big.qty.latest.purchase.receive' => 'Note: Big Qty shown is based on the latest purchase receive',
    'changing.diff.qty' => 'Note: For Serial and Batch Products, upon change of QTY fields, encoded or selected entries will be cleared.',
    'discount.basis' => 'Basis: Used to indicate where current discount will base on. Note: Basis of (-1) will point to the base price.',
    'discount.position' => 'Position: Used to set the hierarchy of the discounts.',
    'discount.value' => 'Value: Value of the discount in decimal.',
    'purchase.price.latest.purchase.receive' => 'Note: Purchase Price shown is based on the latest purchase receive.',
    'purchased.and.received.warning.days' => 'Note: Warning will appear for __ days if product is already purchased and received.',
    'purchased.not.received.warning.days' => 'Note: Warning will appear for __ days if product is already purchased but not yet received.',
    'purchased.warning.days' => 'Note: Warning will appear for __ days if product is already purchased.',
    'received.above.articles.good.condition' => 'Received the above articles in good order and condition.',
    'received.good.condition' => 'Received in good condition.',
    'repeated.purchases.warnings' => 'Check to turn on the following warnings for repeated purchases. Indicate duration of warning (in days) in the textbox.',
    'repeated.requests.warnings' => 'Check to turn on the following warnings for repeated requests. Indicate duration of warning (in days) in the textbox.',
];