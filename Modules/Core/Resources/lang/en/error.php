<?php

return [
    'apply.purchase.price.failed' => 'Unable to apply purchase price. Please try again',
    'batch.detail.row' => 'Batch Error at detail row ',
    'branch.location.required' => 'Branch Location is required.',
    'change.password.failed' => 'Change password failed. Please try again.',
    'create.from.stock.request.failed' => 'Unable to create a new transaction based from Stock Request. Please try again.',
    'customer.not.found' => 'Customer not found.',
    'date.range.incorrect' => 'To Date must be after From Date.',
    'delete.failed' => 'Unable to delete data. Please try again.',
    'files.not.found' => 'Files not found.',
    'gift.check.not.found' => 'Gift Check not found.',
    'gift.check.not.valid' => 'Gift Check is not valid.',
    'insufficient.inv.no.alternatives.found' => 'Insufficient inventory and no Alternative Products found.',
    'invalid.login.schedule' => 'Invalid login schedule.',
    'invalid.page' => 'Invalid Page',
    'invalid.sheet.numbers' => 'Invalid Sheet Numbers.',
    'language.switch.failed' => 'Unable to switch language. Please try again.',
    'license.key.invalid' => 'License Key invalid.',
    'location.not.found' => 'Location does not exist for Branch.',
    'no.batches.found' => 'No Product Batches found.',
    'no.categories.found' => 'No categories found.',
    'no.components.found' => 'No components found.',
    'no.customers.found' => 'No customers found.',
    'no.file.selected' => 'No file selected.',
    'no.license.key' => 'No License Key found.',
    'no.logs.to.show' => 'No logs to show.',
    'no.member.rates.found' => 'No member rates found.',
    'no.members.found' => 'No members found.',
    'no.permission.page' => 'Sorry, you do not have the permission to access this content.',
    'no.products.found' => 'No products found.',
    'no.records.found' => 'No records found.',
    'no.serials.found' => 'No Product Serials found.',
    'no.suppliers.found' => 'No suppliers found.',
    'no.system.code.found' => 'No system code found.',
    'no.terminals.found' => 'No terminals found.',
    'no.transaction.found' => 'No transaction found.',
    'page.not.found' => 'Page not found',
    'parameter.error' => 'Parameter must be an instance of :class',
    'record.not.found' => 'Record not found.',
    'row' => 'Error at row ',
    'save.failed' => 'Unable to save data. Please try again.',
    'select.terminal' => 'Please Select Terminal',
    'serial.detail.row' => 'Serial Number Error at detail row ',
    'serial.number.not.found' => 'Serial Number not found.',
    'show.failed' => 'Unable to find the specific data requested. Please try again.',
    'supplier.not.found' => 'Supplier not found.',
    'sync.failed' => 'Unable to sync data. Please try again.',
    'transaction.not.found' => 'Transaction not found.',
    'transfer.failed' => 'Transfer failed.',
    'unable.to.generate.summary' => 'Unable to generate summary. Please try again.',
    'unable.to.import.excel' => 'Unable to import excel file. Please try again.',
    'unable.to.upload' => 'Unable to upload files.',
    'unauthorized.action' => 'This action is unauthorized.',
    'update.failed' => 'Unable to update data. Please try again.',
    'used.data' => 'Unable to delete already in use data.',
];
