<?php

return [
    'apply.purchase.price' => 'Purchase price succesfully applied.',
    'attachment.temporary.uploaded' => 'Attachments uploaded temporarily.',
    'attachment.uploaded' => 'Attachments uploaded successfully.',
    'changed' => 'Changed successfully.',
    'connected' => 'Connected.',
    'created' => 'Created successfully.',
    'custom.menu.saved' => 'Your custom menu saved successfully.',
    'deleted' => 'Deleted successfully.',
    'gift.check.successfully.redeemed' => 'Gift Check successfully redeemed',
    'inserted' => 'Successfully inserted.',
    'password.changed' => 'Password successfully changed',
    'saved' => 'Saved successfully.',
    'summary.generated' => 'Summary successfully generated.',
    'synced' => 'Synced successfully.',
    'transaction.successfully.imported' => 'Transaction successfully imported.',
    'transferred' => 'Transferred successfully.',
    'updated' => 'Updated successfully.',
];