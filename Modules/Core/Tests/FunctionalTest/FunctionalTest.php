<?php

use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;
use Modules\Core\Tests\Traits\Application;
use Modules\Core\Tests\Traits\ApiResponse;
use Modules\Core\Tests\Traits\Assertions;
use Modules\Core\Tests\Traits\HttpRequests;
use Modules\Core\Tests\Traits\Authentication;
use Modules\Core\Tests\Traits\Migrations;
use Modules\Core\Tests\Traits\DataTransformer;
use Modules\Core\Tests\Traits\UpdatesApplicationSettings;
use Faker\Factory as Faker;

abstract class FunctionalTest extends BaseTestCase
{
    use DatabaseTransactions,
        Application,
        Assertions,
        HttpRequests,
        Authentication,
        Migrations,
        ApiResponse,
        DataTransformer,
        UpdatesApplicationSettings;

    protected $baseUrl;
    protected $faker;

    public function setUp()
    {
        parent::setUp();

        ini_set('memory_limit', '2G');

        $this->baseUrl = env('APP_URL', 'http://localhost/');

        Factory::$factoriesPath = 'Modules/Core/Tests/Factories';

        $this->runMigrations();
        $this->resetTableIndex();

        $this->faker = Faker::create();

        $this->faker->unique(true);
    }
}
