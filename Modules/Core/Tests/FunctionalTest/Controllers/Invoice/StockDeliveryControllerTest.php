<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Enums\Voided;
use Illuminate\Support\Str;

use Litipk\BigNumbers\Decimal;

class StockDeliveryControllerTest extends InvoiceControllerTest
{
    protected $uri = '/stock-delivery';
    protected $prefix = 'SD';

    protected $tables = [
        'info' => 'stock_delivery',
        'detail' => 'stock_delivery_detail'
    ];

    protected $factories = [
        'info' => 'stock_delivery',
        'detail' => 'stock_delivery_detail'
    ];

    private $references = [
        'info' => 'stock_request',
        'detail' => 'stock_request_detail'
    ];

    private $branches;
    private $users;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_deliver_to_or_deliver_to_location_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_deliver_to_or_deliver_to_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'deliver_to' => 99,
            'deliver_to_location' => 99,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function validation_on_reference_id_upon_creating_should_only_be_triggered_if_its_not_null()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array_merge(
            $this->item($build['info'], 'invoiceBasicTransformer'),
            array(
                'id' => 1,
                'sheet_number' => $this->generateReferenceNumber(1, $build['info']->created_from)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $expected,
            ), Lang::get('core::success.created')));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_reference_id_will_trigger_an_error()
    {
        $build = $this->build(array(
            'reference_id' => 99
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reference_id' => array(
                    Lang::get('core::validation.stock.request.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_deliver_to_or_deliver_to_location_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_deliver_to_or_deliver_to_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = 99;
        $data['info']['deliver_to_location'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function validation_on_reference_id_upon_updating_the_data_should_only_be_triggered_if_its_not_null()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_existing_reference_id_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['reference_id'] = 99;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reference_id' => array(
                    Lang::get('core::validation.stock.request.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function validation_on_detail_reference_id_upon_creating_will_only_be_triggered_if_its_not_null()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array_merge(
            $this->item($build['info'], 'invoiceBasicTransformer'),
            array(
                'id' => 1,
                'sheet_number' => $this->generateReferenceNumber(1, $build['info']->created_from)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $expected
            ), Lang::get('core::success.created')));
    }

    /**
     * @test
     */
    public function creating_a_data_with_a_non_existing_reference_id_in_details_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['reference_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.reference_id' => array(
                    sprintf('%s[1] %s',
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.reference.product.doesnt.exists')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function validation_on_detail_reference_id_upon_updating_will_only_be_triggered_if_its_not_null()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_existing_reference_id_in_details_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][0]['reference_id'] = 99;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.reference_id' => array(
                    sprintf('%s[1] %s',
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.reference.product.doesnt.exists')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_through_the_edit_api_will_display_the_full_details_of_the_transaction()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'invoice' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($data['detail'], 'invoiceDetailFullTransformer')
                ),
                'requests' => [],
                'for_locations' => $this->collect($data['info']->for->details, 'branchChosenTransformer'),
                'delivery_locations' => $this->collect($data['info']->deliverTo->details, 'branchChosenTransformer')
            ]));
    }

    /**
     * @test
     */
    public function retrieving_an_existing_transaction_with_remaining_qty_will_return_its_corresponding_transaction_and_details()
    {
        $created = $this->create();

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $this->item($created['info'], 'retrieveInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => 2,
                            'count' => 2,
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($created['detail'], 'retrieveDetailTransformer')
                ),
                'delivery_locations' => $this->collect($created['info']->deliverTo->details, 'branchChosenTransformer'),
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * @test
     */
    public function remaining_qty_for_the_stock_request_detail_reference_will_only_be_deducted_if_current_transaction_is_approved()
    {
        $request = Factory::create($this->references['info']);
        $detail = Factory::create($this->references['detail'], array(
            'transaction_id' => $request->id
        ));

        $created = $this->create(array(
            'reference_id' => $request->id,
            'transaction_type' => TransactionType::FROM_INVOICE
        ), array(
            'reference_id' => $detail->id,
            'product_id' => $detail->product_id,
            'qty' => $detail->qty,
            'unit_qty' => $detail->unit_qty,
            'price' => $detail->price,
            'oprice' => $detail->oprice,
            'unit_id' => $detail->unit_id,
        ));

        $expected = array(
            'initial' => array(
                'id' => $detail->id,
                'remaining_qty' => $detail->remaining_qty->innerValue()
            ),
            'approved' => array(
                'id' => $detail->id,
                'remaining_qty' => $detail->total_qty->mul(Decimal::create('-1', setting('monetary.precision')), setting('monetary.precision'))->innerValue()
            )
        );

        $data = array(
            'for_approval' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::FOR_APPROVAL
            ),
            'declined' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::DECLINED
            ),
            'approved' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::APPROVED
            ),
            'draft' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::DRAFT
            )
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data['for_approval'])
            ->seeInDatabase($this->references['detail'], $expected['initial']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['declined'])
            ->seeInDatabase($this->references['detail'], $expected['initial']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['draft']);
        $this->postJson(sprintf('%s/approval', $this->uri), $data['for_approval']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['approved'])
            ->seeInDatabase($this->references['detail'], $expected['approved']);
    }

    /**
     * @test
     */
    public function transaction_status_for_the_stock_request_reference_will_only_be_changed_if_current_transaction_is_approved()
    {
        $request = Factory::create($this->references['info']);
        $detail = Factory::create($this->references['detail'], array(
            'transaction_id' => $request->id
        ));

        $created = $this->create(array(
            'reference_id' => $request->id,
            'transaction_type' => TransactionType::FROM_INVOICE
        ), array(
            'reference_id' => $detail->id,
            'product_id' => $detail->product_id,
            'qty' => $detail->qty,
            'unit_qty' => $detail->unit_qty,
            'price' => $detail->price,
            'oprice' => $detail->oprice,
            'unit_id' => $detail->unit_id,
        ));

        $expected = array(
            'initial' => array(
                'id' => $request->id,
                'transaction_status' => TransactionStatus::NO_DELIVERY
            ),
            'approved' => array(
                'id' => $request->id,
                'transaction_status' => TransactionStatus::EXCESS
            )
        );

        $data = array(
            'for_approval' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::FOR_APPROVAL
            ),
            'declined' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::DECLINED
            ),
            'approved' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::APPROVED
            ),
            'draft' => array(
                'transactions' => $created['info']->id,
                'status' => ApprovalStatus::DRAFT
            )
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data['for_approval'])
            ->seeInDatabase($this->references['info'], $expected['initial']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['declined'])
            ->seeInDatabase($this->references['info'], $expected['initial']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['draft']);
        $this->postJson(sprintf('%s/approval', $this->uri), $data['for_approval']);

        $this->postJson(sprintf('%s/approval', $this->uri), $data['approved'])
            ->seeInDatabase($this->references['info'], $expected['approved']);
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $deliver = $this->branches[rand(0, 2)];
        $for = $this->branches[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for' => $for->id,
            'for_location' => $for->details->first()->id,
            'deliver_to' => $deliver->id,
            'deliver_to_location' => $deliver->details->first()->id,
            'requested_by' => $this->users[rand(0, 2)]->id
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        foreach ($this->branches as $key => $branch) {
            Factory::times(2)->create('branch_detail', array(
                'branch_id' => $branch->id
            ));
        }

        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'deliver_to'            => $data->deliverTo->name,
            'deliver_to_location'   => $data->deliverToLocation->name,
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'created_by'            => $data->creator->full_name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'transaction_status'    => $data->presenter()->transaction,
            'amount'                => $data->number()->total_amount,
            'deleted'               => $data->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'deliver_to'            => $model->deliver_to,
            'deliver_to_location'   => $model->deliver_to_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'transaction_type'      => $model->transaction_type,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'reference_id'          => is_null($model->reference_id)
                ? null
                : $model->reference_id,
        ];
    }

    protected function invoiceDetailFullTransformer($model)
    {
        $precision = setting('monetary.precision');

        return array_merge(parent::invoiceDetailFullTransformer($model), [
               'reference_id'         => $model->reference_id,
               'invoice_qty'          => $model->reference->total_qty ?? number_format(0, $precision),
               'remaining_qty'        => $model->reference->remaining_qty ?? number_format(0, $precision),
               'spare_qty'            => is_null($model->reference)
                   ? number_format(0, $precision)
                   : $model->number()->spare_qty,
               'transactionable_id'   => $model->transactionable_id,
               'transactionable_type' => $model->transactionable_type,
               'references'           => is_null($model->transactionable) ? [] : [
               'id'                   => $model->transactionable->transaction->id,
               'sheet_number'         => $model->transactionable->transaction->sheet_number 
            ]
        ]);
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'for_location'        => $model->for_location,
            'deliver_to'          => $model->deliver_to,
            'deliver_to_location' => $model->deliver_to_location,
            'requested_by'        => $model->requested_by,
            'remarks'             => $model->remarks,
        ];
    }

    protected function branchChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name
        ];
    }
}
