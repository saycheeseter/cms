<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class DamageControllerTest extends InvoiceControllerTest
{
    protected $uri = '/damage';
    protected $prefix = 'DMG';

    protected $tables = [
        'info' => 'damage',
        'detail' => 'damage_detail'
    ];

    protected $factories = [
        'info' => 'damage',
        'detail' => 'damage_detail'
    ];

    private $reasons;
    
    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas([
                'reasons',
            ]);
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_reason_id_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['reason_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reason_id' => array(
                    Lang::get('core::validation.reason.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_reason_id_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'reason_id' => '99',
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reason_id' => array(
                    Lang::get('core::validation.reason.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk()
            ->assertViewHas([
                'reasons',
            ]);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_reason_id_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['reason_id'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reason_id' => array(
                    Lang::get('core::validation.reason.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_reason_id_that_doesnt_exist_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['reason_id'] = '99';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reason_id' => array(
                    Lang::get('core::validation.reason.doesnt.exists')
                ),
            )));
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->reasons = Factory::times(3)->create('reason');

        return $this;
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], array_merge(array(
            'reason_id' => $this->reasons[rand(0, 2)]->id,
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'reason'                => $data->reason->name,
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'created_by'            => $data->creator->full_name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'transaction_status'    => $data->presenter()->transaction,
            'amount'                => $data->number()->total_amount,
            'deleted'               => $data->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'reason_id'             => $model->reason_id,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'reason_id'    => $model->reason_id,
            'requested_by' => $model->requested_by,
            'remarks'      => $model->remarks,
            'for_location' => $model->for_location,
        ];
    }
}
