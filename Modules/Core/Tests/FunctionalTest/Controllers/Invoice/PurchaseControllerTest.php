<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class PurchaseControllerTest extends InvoiceControllerTest
{
    protected $uri = '/purchase';
    protected $prefix = 'PO';
    
    protected $tables = [
        'info' => 'purchase',
        'detail' => 'purchase_detail'
    ];

    protected $factories = [
        'info' => 'purchase',
        'detail' => 'purchase_detail'
    ];

    private $suppliers;
    private $paymentMethods;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas([
                'tsuppliers', 
                'paymentMethods',
                'branchDetails'
            ]);
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_supplier_id_payment_method_deliver_until_or_deliver_to_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);
        
        $data['info']['supplier_id'] = '';
        $data['info']['payment_method'] = '';
        $data['info']['deliver_to'] = '';
        $data['info']['deliver_until'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                ),
                'info.deliver_until' => array(
                    Lang::get('core::validation.deliver.until.required')
                ),
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_supplier_id_payment_method_or_deliver_to_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'supplier_id' => 99,
            'payment_method' => 99,
            'deliver_to' => 99
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists'),
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists'),
                ),
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_deliver_until_date_format_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['deliver_until'] = Carbon::now()->toDateTimeString();
        
        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_until' => array(
                    Lang::get('core::validation.deliver.until.invalid.format'),
                ),
            )));
    }
    
    /**
     * @test
     */
    public function saving_a_non_numeric_term_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['term'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk()
            ->assertViewHas([
                'tsuppliers', 
                'paymentMethods', 
                'branchDetails'
            ]);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_supplier_id_payment_method_deliver_until_or_deliver_to_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = '';
        $data['info']['payment_method'] = '';
        $data['info']['deliver_until'] = null;
        $data['info']['deliver_to'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                ),
                'info.deliver_until' => array(
                    Lang::get('core::validation.deliver.until.required')
                ),
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_supplier_id_payment_method_or_deliver_to_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = 99;
        $data['info']['payment_method'] = 99;
        $data['info']['deliver_to'] = 99;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists'),
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists'),
                ),
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect__deliver_until_date_format_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_until'] = Carbon::now()->toDateTimeString();
        
        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_until' => array(
                    Lang::get('core::validation.deliver.until.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_numeric_term_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['term'] = 'A';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            )));
    }

    /**
     * Build non-persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], array_merge(array(
            'supplier_id' => $this->suppliers[rand(0, 2)]->id,
            'payment_method' => $this->paymentMethods[rand(0, 2)]->id,
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }
        
    /**
     * Build reference data for the transaction
     * 
     * @return $this        
     */
    protected function references()
    {
        parent::references();

        $this->suppliers = Factory::times(3)->create('supplier');
        $this->paymentMethods = Factory::times(3)->create('payment_method');

        return $this;
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'created_from'          => $data->from->name,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'supplier'              => $data->supplier->full_name,
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'deliver_until'         => $data->deliver_until->toDateString(),
            'created_by'            => $data->creator->full_name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'transaction_status'    => $data->presenter()->transaction,
            'amount'                => $data->number()->total_amount,
            'deleted'               => $data->presenter()->deleted,
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                           => $model->id,
            'sheet_number'                 => $model->sheet_number,
            'transaction_date'             => $model->transaction_date->toDateTimeString(),
            'supplier_id'                  => $model->supplier_id,
            'remarks'                      => $model->remarks,
            'requested_by'                 => $model->requested_by,
            'payment_method'               => $model->payment_method,
            'deliver_until'                => $model->deliver_until->toDateString(),
            'term'                         => $model->term,
            'created_for'                  => $model->created_for,
            'for_location'                 => $model->for_location,
            'deliver_to'                   => $model->deliver_to,
            'created_date'                 => $model->created_at->toDateTimeString(),
            'modified_date'                => $model->updated_at->toDateTimeString(),
            'approval_status'              => $model->approval_status,
            'approval_status_label'        => $model->presenter()->approval,
            'created_by'                   => $model->creator->full_name,
            'modified_by'                  => is_null($model->modifier) ? '' : $model->modifier->full_name,
            'transactionable_id'           => $model->transactionable_id,
            'transactionable_type'         => $model->transactionable_type,
            'transactionable_sheet_number' => !is_null($model->transactionable) ? $model->transactionable->sheet_number : null,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'supplier_id'          => $model->supplier_id,
            'payment_method'       => $model->payment_method,
            'term'                 => $model->term, 
            'remarks'              => $model->remarks,
            'requested_by'         => $model->requested_by,
            'for_location'         => $model->for_location,
            'transactionable_id'   => $model->transactionable_id,
            'transactionable_type' => $model->transactionable_type,
        ];
    }

    protected function invoiceDetailFullTransformer($model)
    {
        return array_merge(parent::invoiceDetailFullTransformer($model), [
            'transactionable_id'   => $model->transactionable_id,
            'transactionable_type' => $model->transactionable_type,
        ]);
    }

    protected function retrieveDetailTransformer($model)
    {
        return array_merge(parent::retrieveDetailTransformer($model), [
            'transactionable_id'   => $model->transactionable_id,
            'transactionable_type' => $model->transactionable_type,
        ]);
    }

    /**
     * Convert transaction to Array
     * 
     * @param  array $transaction
     * @return array
     */
    protected function toArray($transaction)
    {
        $data = parent::toArray($transaction);

        $data['info']['deliver_until'] = $transaction['info']->deliver_until->toDateString();

        return $data;
    }
}