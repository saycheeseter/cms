<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\Voided;
use Illuminate\Support\Str;

class StockRequestControllerTest extends InvoiceControllerTest
{
    protected $uri = '/stock-request';
    protected $to = '/stock-request-to';
    protected $from = '/stock-request-from';
    protected $prefix = 'SR';

    protected $tables = [
        'info' => 'stock_request',
        'detail' => 'stock_request_detail'
    ];

    protected $factories = [
        'info' => 'stock_request',
        'detail' => 'stock_request_detail'
    ];

    private $branches;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->to))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function visiting_the_stock_request_from_page_will_display_the_form_with_its_corresponding_references()
    {
        $created = $this->create();

        $this->visit(sprintf('%s/%s/view', $this->from, $created['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_request_to_will_return_the_corresponding_collection_of_data()
    {
        $this->transactions();

        $data = $this->create(array('request_to' => $this->branches[1]->id))['info'];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->request_to,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'request_to'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'invoices' => array(
                $this->item($data, 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_request_to_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['request_to'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.request_to' => array(
                    Lang::get('core::validation.request.to.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_request_to_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'request_to' => 99,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.request_to' => array(
                    Lang::get('core::validation.request.to.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->to, $data['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_request_to_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['request_to'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.request_to' => array(
                    Lang::get('core::validation.request.to.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_request_to_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['request_to'] = 99;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.request_to' => array(
                    Lang::get('core::validation.request.to.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function retrieving_an_existing_transaction_with_remaining_qty_will_return_its_corresponding_transaction_and_details()
    {
        $created = $this->create();

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $this->item($created['info'], 'retrieveInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => 2,
                            'count' => 2,
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($created['detail'], 'retrieveDetailTransformer')
                ),
                'delivery_locations' => $this->collect($created['info']->for->details, 'branchChosenTransformer'),
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], array_merge(array(
            'request_to' => $this->branches[rand(0, 2)]->id,
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        return $this;
    }

    protected function transactionBasicTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'request_to'            => $model->to->name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested->full_name,
            'created_by'            => $model->creator->full_name,
            'created_date'          => $model->created_at->toDateTimeString(),
            'audited_by'            => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'          => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_status'    => $model->presenter()->transaction,
            'amount'                => $model->number()->total_amount,
            'deleted'               => $model->presenter()->deleted,
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'request_to'            => $model->request_to,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }

    protected function retrieveDetailTransformer($model)
    {
        return [
            'id'            => 0,
            'reference_id'  => $model->id,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcodeList,
            'barcode'       => $model->presenter()->computedUnitBarcode,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'uom'           => [
                'options' => $model->presenter()->unitList,
                'value'   => $model->computed_unit_id
            ],
            'unit_qty'      => $model->number()->computed_unit_qty,
            'qty'           => $model->number()->computed_qty,
            'total_qty'     => $model->number()->computed_total_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'amount'        => $model->number()->computed_amount,
            'remarks'       => $model->remarks,
            'remaining_qty' => $model->number()->remaining_qty,
            'invoice_qty'   => $model->number()->total_qty,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'created_for'  => $model->created_for,
            'for_location' => $model->for_location
        ];
    }

    protected function invoiceDetailFullTransformer($model)
    {
        $purchases = [];

        foreach ($model->purchases as $key => $purchase) {
            $purchases[] = [
                'id' => $purchase->transaction->id,
                'sheet_number' => $purchase->transaction->sheet_number
            ];
        }

        return array_merge(parent::invoiceDetailFullTransformer($model), [
            'references' => $purchases,
            'remaining_qty' => $model->number()->remaining_qty
        ]);
    }

    protected function branchChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name
        ];
    }
}
