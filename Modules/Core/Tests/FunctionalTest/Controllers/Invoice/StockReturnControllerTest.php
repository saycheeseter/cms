<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class StockReturnControllerTest extends InvoiceControllerTest
{
    protected $uri = '/stock-return';
    protected $prefix = 'SDR';
    
    protected $tables = [
        'info' => 'stock_return',
        'detail' => 'stock_return_detail'
    ];

    protected $factories = [
        'info' => 'stock_return',
        'detail' => 'stock_return_detail'
    ];

    private $branches;
    private $users;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_deliver_to_or_deliver_to_location_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_deliver_to_or_deliver_to_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'deliver_to' => 99,
            'deliver_to_location' => 99,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

     /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_deliver_to_or_deliver_to_location_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_deliver_to_or_deliver_to_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = 99;
        $data['info']['deliver_to_location'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_through_the_edit_api_will_display_the_full_details_of_the_transaction()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'invoice' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($data['detail'], 'invoiceDetailFullTransformer')
                ),
                'for_locations' => $this->collect($data['info']->for->details, 'branchChosenTransformer'),
                'delivery_locations' => $this->collect($data['info']->deliverTo->details, 'branchChosenTransformer')
            ]));
    }

    /**
     * @test
     */
    public function retrieving_an_existing_transaction_with_remaining_qty_will_return_its_corresponding_transaction_and_details()
    {
        $created = $this->create();

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $this->item($created['info'], 'retrieveInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => 2,
                            'count' => 2,
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($created['detail'], 'retrieveDetailTransformer')
                ),
                'delivery_locations' => $this->collect($created['info']->deliverTo->details, 'branchChosenTransformer'),
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * Build non-persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {
        $deliver = $this->branches[rand(0, 2)];
        $for = $this->branches[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for' => $for->id,
            'for_location' => $for->details->first()->id,
            'deliver_to' => $deliver->id,
            'deliver_to_location' => $deliver->details->first()->id,
            'requested_by' => $this->users[rand(0, 2)]->id
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    /**
     * Build reference data for the transaction
     * 
     * @return $this        
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        foreach ($this->branches as $key => $branch) {
            Factory::times(2)->create('branch_detail', array(
                'branch_id' => $branch->id
            ));
        }

        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'deliver_to'            => $data->deliverTo->name,
            'deliver_to_location'   => $data->deliverToLocation->name,
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'created_by'            => $data->creator->full_name,
            'created_date'          => $data->created_at->toDateTimeString(), 
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'transaction_status'    => $data->presenter()->transaction,
            'amount'                => $data->number()->total_amount,
            'deleted'               => $data->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'deliver_to'            => $model->deliver_to,
            'deliver_to_location'   => $model->deliver_to_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'for_location'        => $model->for_location,
            'deliver_to'          => $model->deliver_to,
            'deliver_to_location' => $model->deliver_to_location,
            'requested_by'        => $model->requested_by,
            'remarks'             => $model->remarks,
        ];
    }

    protected function branchChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name
        ];
    }
}