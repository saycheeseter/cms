<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\Customer;

class SalesReturnControllerTest extends InvoiceControllerTest
{
    protected $uri = '/sales-return';
    protected $prefix = 'SAR';
    
    protected $tables = [
        'info' => 'sales_return',
        'detail' => 'sales_return_detail'
    ];

    protected $factories = [
        'info' => 'sales_return',
        'detail' => 'sales_return_detail'
    ];

    private $customers;
  
    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas([
                'customers',
            ]);
    }

    /**
     * @test
     */
    public function saving_an_empty_customer_type_or_salesman_id_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['salesman_id'] = '';
        $data['info']['customer_type'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.required')
                ),
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_customer_type_or_term_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['term'] = 'A';
        $data['info']['customer_type'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.numeric')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_salesman_id_will_trigger_an_error()
    {
        $build = $this->build([
            'salesman_id' => 100,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function customer_id_and_customer_detail_id_will_only_be_required_if_customer_type_is_set_as_regular()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['customer_id'] = '';
        $data['info']['customer_detail_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.required'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function customer_walk_in_name_is_only_required_if_customer_type_is_set_as_walk_in()
    {
        $build = $this->build([
            'customer_id' => 0,
            'customer_detail_id' => 0,
            'customer_walk_in_name' => '',
            'customer_type' => Customer::WALK_IN
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_walk_in_name' => array(
                    Lang::get('core::validation.customer.walk.in.name.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_customer_id_or_customer_detail_id_while_customer_type_is_set_as_regular_will_trigger_an_error()
    {
        $build = $this->build([
            'customer_id' => 100,
            'customer_detail_id' => 100,
            'customer_type' => Customer::REGULAR
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.doesnt.exists'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk()
            ->assertViewHas([
                'customers',
            ]);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_customer_type_or_salesman_id_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['customer_type'] = '';
        $data['info']['salesman_id'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.required')
                ),
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_numeric_customer_type_or_term_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['customer_type'] = 'A';
        $data['info']['term'] = 'A';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.numeric')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_existing_salesman_id_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['salesman_id'] = 100;
        
        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function upon_updating_customer_id_and_customer_detail_id_will_only_be_required_if_customer_type_is_set_as_regular()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['customer_id'] = '';
        $data['info']['customer_detail_id'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.required'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function upon_updating_customer_walk_in_name_is_only_required_if_customer_type_is_set_as_walk_in()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['customer_id'] = 0;
        $data['info']['customer_detail_id'] = 0;
        $data['info']['customer_walk_in_name'] = '';
        $data['info']['customer_type'] = Customer::WALK_IN;
        
        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_walk_in_name' => array(
                    Lang::get('core::validation.customer.walk.in.name.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_existing_customer_id_or_customer_detail_id_while_customer_type_is_set_as_regular_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['customer_id'] = 100;
        $data['info']['customer_detail_id'] = 100;
        $data['info']['customer_type'] = Customer::REGULAR;
        
        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.doesnt.exists'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_through_the_edit_api_will_display_the_full_details_of_the_transaction()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'invoice' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'locations' => $this->collect($data['info']->for->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($data['detail'], 'invoiceDetailFullTransformer')
                ),
                'customer_details' => $this->collect($data['info']->customer->details, 'customerDetailChosenTransformer'),
            ]));
    }

    /**
     * @test
     */
    public function retrieving_an_existing_transaction_with_remaining_qty_will_return_its_corresponding_transaction_and_details()
    {
        $created = $this->create();

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'invoice' => $this->item($created['info'], 'retrieveInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => 2,
                            'count' => 2,
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collect($created['detail'], 'retrieveDetailTransformer')
                ),
                'customer_details' => $this->collect($created['info']->customer->details, 'customerDetailChosenTransformer')
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $customer = $this->customers[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'customer_id' => $customer['info']->id,
            'customer_detail_id' => $customer['details'][rand(0, 1)]->id,
            'customer_type' => Customer::REGULAR,
            'customer_walk_in_name' => '',
            'salesman_id' => 1,
            'requested_by' => 1
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }
    
    /**
     * Build persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function create($info = array(), $detail = array())
    {
        $customer = $this->customers[rand(0, 2)];

        $info = Factory::create($this->factories['info'], array_merge(array(
            'customer_id' => $customer['info']->id,
            'customer_detail_id' => $customer['details'][rand(0, 1)]->id,
            'customer_type' => Customer::REGULAR,
            'customer_walk_in_name' => ''
        ), $info));

        $detail = Factory::times(2)->create($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => $detail
        );
    }

    /**
     * Build reference data for the transaction
     * 
     * @return $this        
     */
    protected function references()
    {
        parent::references();

        for ($i=0; $i < 3; $i++) { 
            $this->customers[]['info'] = Factory::create('customer');
        }

        for ($i=0; $i < count($this->customers); $i++) { 
            $customer = $this->customers[$i]['info'];

            $this->customers[$i]['details'] = Factory::times(2)->create('customer_detail', array(
                'head_id' => $customer->id
            ));
        }

        return $this;
    }

    protected function transactionBasicTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'sheet_number'          => $model->sheet_number,
            'created_from'          => $model->from->name,
            'created_for'           => $model->for->name,
            'for_location'          => $model->location->name,
            'salesman'              => $model->salesman->full_name,
            'customer'              => !is_null($model->customer) 
                ? $model->customer->name
                : $model->customer_walk_in_name,
            'customer_detail'       => !is_null($model->customerDetail)
                ? $model->customerDetail->name
                : '',
            'remarks'               => $model->remarks,
            'created_by'            => $model->creator->full_name,
            'created_date'          => $model->created_at->toDateTimeString(),
            'audited_by'            => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'audited_date'          => !is_null($model->auditor) ? $model->audited_date->toDateTimeString() : '',
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_status'    => $model->presenter()->transaction,
            'amount'                => $model->number()->total_amount,
            'deleted'               => $model->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'term'                  => $model->term,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'requested_by'          => $model->requested_by,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'created_by'            => $model->creator->full_name,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'term'                  => $model->term,
            'for_location'          => $model->for_location,
        ];
    }

    protected function customerDetailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}