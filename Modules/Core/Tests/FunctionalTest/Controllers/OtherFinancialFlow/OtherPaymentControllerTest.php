<?php

use Laracasts\TestDummy\Factory;

class OtherPaymentControllerTest extends OtherFinancialFlowTestStructure
{
    protected $uri = '/other-payment';

    protected $table = [
        'info' => 'other_payment',
        'detail' => 'other_payment_detail',
        'check' => 'issued_check'
    ];

    protected $factories = [
        'info' => 'other_payment',
        'detail' => 'other_payment_detail',
        'type' => 'payment_type'
    ];
}