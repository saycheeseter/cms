<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\SystemCode;
use Carbon\Carbon;
use Modules\Core\Enums\PaymentMethod;

abstract class OtherFinancialFlowTestStructure extends FunctionalTest
{
    protected $uri;
    protected $factories;
    protected $table;

    protected $references = [
        'payment_methods' => [],
        'types' => [],
        'bank_accounts' => []
    ];

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();

        $this->createReferences();
    }

    /**
     * @test
     */
    public function accessing_other_financial_flow_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_other_financial_flow_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas(['types', 'paymentMethods', 'bankAccounts', 'permissions']);
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_reference_name_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->reference_name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'reference_name' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'data' => $this->collect([$data[2]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->remarks, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'remarks' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'data' => $this->collect([$data[2]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = collect($this->collection())->pluck('info');
        
        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->id, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'data' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 100),
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'reference_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'data' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_incomplete_head_or_details_info_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data = array_merge($data['info'], [
            'reference_name' => '',
            'transaction_date' => '',
            'payment_method' => ''
        ]);

        $data['details'][0]['type'] = '';
        $data['details'][0]['amount'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.reference_name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                ),
                'details.0.type' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.type.required')
                ),
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_check_without_bank_details_will_trigger_an_error_response()
    {
        $build = $this->build(['payment_method' => PaymentMethod::CHECK]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.bank_account_id' => array(
                    Lang::get('core::validation.bank.account.required')
                ),
                'info.check_date' => array(
                    Lang::get('core::validation.check.date.required')
                ),
                'info.check_number' => array(
                    Lang::get('core::validation.check.no.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_bank_deposit_without_bank_details_will_trigger_an_error_response()
    {
        $build = $this->build(['payment_method' => PaymentMethod::BANK_DEPOSIT]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.bank_account_id' => array(
                    Lang::get('core::validation.bank.account.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_transaction_date_and_detail_amount_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['transaction_date'] = (string) $this->faker->name;
        $data['details'][0]['amount'] = (string) $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format')
                ),
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.numeric')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existent_type_and_method_will_trigger_an_error_response()
    {
        $build = $this->build();
        
        $data = $this->toArray($build);

        $data['info']['payment_method'] = $this->faker->numberBetween(1000, 9999);
        $data['details'][0]['type'] = $this->faker->numberBetween(1000, 9999);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists')
                ),
                'details.0.type' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.type.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_transaction_without_details_will_trigger_an_error_response()
    {
        $build = $this->build();
        
        $data = $this->toArray($build);

        $data['details'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_transaction_with_amount_less_than_zero_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['amount'] = -1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.cannot.be.zero')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'data' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array(
                        'id' => 1,
                        'total_amount' => $data['details'][0]['amount']
                    )
                )
            ),
            'database' => array(
                'info' => array_merge(
                    $data['info'],
                    array('id' => 1)
                ),
                'details' => array_merge(
                    $data['details'][0],
                    array(
                        'id' => 1,
                        'transaction_id' => 1
                    )
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.created')))
            ->seeInDatabase($this->table['info'], $expected['database']['info'])
            ->seeInDatabase($this->table['detail'], $expected['database']['details']);
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_with_payment_method_check_will_successfully_save_the_data_in_database_and_return_an_id_and_create_in_check_table()
    {
        $build = $this->build([
            'payment_method' => PaymentMethod::CHECK,
            'bank_account_id' => $this->references['bank_accounts'][rand(0, 2)]->id,
            'check_number' => 'abcd',
            'check_date' => Carbon::now()->toDateString()
        ]);

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'data' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array(
                        'id' => 1,
                        'total_amount' => $data['details'][0]['amount']
                    )
                )
            ),
            'database' => array(
                'info' => array_merge(
                    $data['info'],
                    array('id' => 1)
                ),
                'details' => array_merge(
                    $data['details'][0],
                    array(
                        'id' => 1,
                        'transaction_id' => 1
                    )
                )
            ),
            'check' => [
                'transactionable_id' => 1,
                'transactionable_type' => $this->table['info'],
                'bank_account_id' => $data['info']['bank_account_id'],
                'check_no' => $data['info']['check_number'],
                'check_date' => $data['info']['check_date'],
                'amount' => $data['details'][0]['amount']
            ]
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.created')))
            ->seeInDatabase($this->table['info'], $expected['database']['info'])
            ->seeInDatabase($this->table['detail'], $expected['database']['details'])
            ->seeInDatabase($this->table['check'], $expected['check']);
    }

    /**
     * @test
     */
    public function updating_transaction_with_payment_method_check_will_update_corresponding_check()
    {
        $build = $this->build([
            'payment_method' => PaymentMethod::CHECK,
            'bank_account_id' => $this->references['bank_accounts'][rand(0, 2)]->id,
            'check_number' => 'abcd',
            'check_date' => Carbon::now()->toDateString()
        ]);

        $data = $this->toArray($build);
        $updateData = array_merge($data, [
            'details' => [[
                'amount' => '1000.00',
                'type' => 1
            ]]
        ]);

        $expected = [
            'transactionable_id' => 1,
            'transactionable_type' => $this->table['info'],
            'bank_account_id' => $data['info']['bank_account_id'],
            'check_no' => $data['info']['check_number'],
            'check_date' => $data['info']['check_date'],
            'amount' => $updateData['details'][0]['amount']
        ];

        $this->postJson($this->uri, $data)
            ->patchJson(sprintf('%s/%s', $this->uri, 1), $updateData)
            ->seeInDatabase($this->table['check'], $expected);
        ;
    }

    /**
     * @test
     */
    public function deleting_transaction_with_payment_method_check_will_update_corresponding_check()
    {
        $build = $this->build([
            'payment_method' => PaymentMethod::CHECK,
            'bank_account_id' => $this->references['bank_accounts'][rand(0, 2)]->id,
            'check_number' => 'abcd',
            'check_date' => Carbon::now()->toDateString()
        ]);

        $data = $this->toArray($build);

        $expected = [
            'transactionable_id' => 1,
            'transactionable_type' => $this->table['info'],
            'bank_account_id' => $data['info']['bank_account_id'],
            'check_no' => $data['info']['check_number'],
            'check_date' => $data['info']['check_date'],
            'deleted_at' => null
        ];

        $this->postJson($this->uri, $data)
            ->deleteJson(sprintf('%s/%s', $this->uri, 1))
            ->notSeeInDatabase($this->table['check'], $expected);
        ;
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incomplete_head_or_details_info_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data = array_merge($data['info'], [
            'reference_name' => '',
            'transaction_date' => '',
            'payment_method' => ''
        ]);

        $data['details'][0]['type'] = '';
        $data['details'][0]['amount'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.reference_name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                ),
                'details.0.type' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.type.required')
                ),
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_invalid_transaction_date_and_amout_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['transaction_date'] = (string) $this->faker->name;
        $data['details'][0]['amount'] = (string) $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format')
                ),
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.numeric')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_amount_less_than_zero_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][0]['amount'] = -1;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.amount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.amount.cannot.be.zero')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_bank_deposit_transaction_without_bank_details_will_trigger_an_error_response()
    {   
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['payment_method'] = PaymentMethod::BANK_DEPOSIT;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.bank_account_id' => array(
                    Lang::get('core::validation.bank.account.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_check_transaction_without_bank_details_will_trigger_an_error_response()
    {   
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['payment_method'] = PaymentMethod::CHECK;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.bank_account_id' => array(
                    Lang::get('core::validation.bank.account.required')
                ),
                'info.check_date' => array(
                    Lang::get('core::validation.check.date.required')
                ),
                'info.check_number' => array(
                    Lang::get('core::validation.check.no.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_without_details_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'] = [];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_existent_type_and_method_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['payment_method'] = $this->faker->numberBetween(1000, 9999);
        $data['details'][0]['type'] = $this->faker->numberBetween(1000, 9999);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists')
                ),
                'details.0.type' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.type.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_a_proper_format_of_data_will_successfully_update_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([
                'data' => $this->item($created['info'], 'basicTransformer')
            ], Lang::get('core::success.updated')));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/99', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase($this->table['info'], [
                'id' => 99
            ]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_and_its_relationship_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $expected = array(
            'info' => array(
                'id' => $created['info']->id,
                'deleted_at' => null
            ),
            'detail' => array(
                'id' => $created['detail']->first()->id,
            )
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase($this->table['info'], $expected['info'])
            ->notSeeInDatabase($this->table['detail'], $expected['detail']);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {
        $this->deleteJson(sprintf('%s/99', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase($this->table['info'], array(
                    'id' => 99,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_other_financial_flow_will_return_the_full_info_of_the_data()
    {
        $created = $this->create();

        $expected = array(
            'data' => $this->item($created['info'], 'fullInfoTransformer'),
            'details' => array(
                'data' => $this->collect($created['detail'], 'detailTransformer'),
                'meta' => array(
                    'pagination' => array(
                        'total' => count($created['detail']),
                        'count' => count($created['detail']),
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    )
                )
            )
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_rate_will_return_an_error()
    {
        $this->getJson(sprintf('%s/99', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }


    /**
     * Build non-persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {   
        $info = Factory::build($this->factories['info'], array_merge(array(
            'payment_method' => PaymentMethod::CASH,
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'type' => $this->references['types'][rand(0, 2)]->id
        ), $detail));

        return array(
            'info' => $info,
            'detail' => collect([$detail])
        );
    }

    /**
     * Build persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function create($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['info'], array_merge(array(
            'payment_method' => $this->references['payment_methods'][rand(0, 2)]->id,
        ), $info));
        
        $detail = Factory::create($this->factories['detail'], array_merge(array(
            'type' => $this->references['types'][rand(0, 2)]->id,
            'transaction_id' => $info->id,
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => collect([$detail])
        );
    }

    /**
     * Build a collection of customer
     * 
     * @param  int  $time  
     * @return array        
     */
    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function basicTransformer($model)
    {   
        return [
            'id'               => $model->id,
            'reference_name'   => $model->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks ?? '',
            'method'           => $model->method->name,
            'created_by'       => $model->creator->full_name,
            'created_from'     => $model->from->name,
            'total_amount'     => $model->number()->total_amount
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'               => $model->id,
            'reference_name'   => $model->reference_name,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks ?? '',
            'payment_method'   => $model->payment_method,
            'created_by'       => $model->created_by,
            'created_from'     => $model->created_from,
            'bank_account_id'  => $model->bank_account_id,
            'check_date'       => $model->check_date ? $model->check_date->toDateTimeString() : null,
            'check_number'     => $model->check_number
        ];
    }

    protected function detailTransformer($model)
    {
        return [
            'id'             => $model->id,
            'transaction_id' => $model->transaction_id,
            'amount'         => $model->number()->amount,
            'type'           => $model->type,
        ];
    }

    public function createReferences()
    {
        $this->references = [
            'payment_methods' => Factory::times(3)->create('payment_method'),
            'types'           => Factory::times(3)->create($this->factories['type']),
            'bank_accounts'   => Factory::times(3)->create('bank_account'),
        ];
    }

    /**
     * Convert data to Array
     * 
     * @param  array $data
     * @return array
     */
    protected function toArray($data)
    {
        $details = [];

        $collection = ! $data['detail'] instanceof Collection 
            ? collect($data['detail']) 
            : $data['detail'];

        foreach ($collection as $key => $model) {
            $details[] = $this->toArrayCast($model);
        }

        return array(
            'info' => $this->toArrayCast($data['info']),
            'details' => $details
        ) ;
    }
}