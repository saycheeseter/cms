<?php

use Laracasts\TestDummy\Factory;

class OtherIncomeControllerTest extends OtherFinancialFlowTestStructure
{
    protected $uri = '/other-income';

    protected $table = [
        'info' => 'other_income',
        'detail' => 'other_income_detail',
        'check' => 'received_check'
    ];

    protected $factories = [
        'info' => 'other_income',
        'detail' => 'other_income_detail',
        'type' => 'income_type'
    ];
}