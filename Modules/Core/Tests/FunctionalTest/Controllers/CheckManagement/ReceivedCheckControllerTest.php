<?php 

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class ReceivedCheckControllerTest extends CheckManagementControllerTest
{
    protected $uri = '/received-check';
    protected $factory = 'received_check';
    protected $table = 'received_check';
}