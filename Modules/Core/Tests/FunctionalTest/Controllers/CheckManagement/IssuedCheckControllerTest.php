<?php 

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class IssuedCheckControllerTest extends CheckManagementControllerTest
{
    protected $uri = '/issued-check';
    protected $factory = 'issued_check';
    protected $table = 'issued_check';
}