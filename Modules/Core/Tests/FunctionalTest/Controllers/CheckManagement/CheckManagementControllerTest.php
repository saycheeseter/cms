<?php 

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

use Modules\Core\Enums\CheckStatus;

abstract class CheckManagementControllerTest extends FunctionalTest
{
    private $bankAccounts;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function accessing_issued_check_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_issued_check_page_while_user_is_authenticated_will_successfully_display_the_page_with_its_reference()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('bankAccounts');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->transaction_date,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_bank_account_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->bank_account_id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'bank_account_id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data, 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_check_no_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->check_no,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'check_no'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_check_date_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->check_date,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'check_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->remarks,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'remarks'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transfer_date_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->transfer_date,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'transfer_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => $this->collect($data, 'transform')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '9999',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'bank_account_id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'checks' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function transferring_checks_without_data_will_trigger_an_error()
    {
        $this->getJson($this->uri.'/transfer', [])
            ->seeJsonEquals($this->errorResponse([
                'ids' => array(
                    Lang::get('core::validation.check.min.one')
                ),
                'transfer_date' => array(
                    Lang::get('core::validation.transfer.date.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function transferring_checks_without_selection_will_trigger_an_error()
    {
        $data = [
            'ids' => [],
            'transfer_date' => Carbon::now()->toDateString()
        ];

        $this->getJson($this->uri.'/transfer', $data)
            ->seeJsonEquals($this->errorResponse([
                'ids' => array(
                    Lang::get('core::validation.check.min.one')
                ),
            ]));
    }

    /**
     * @test
     */
    public function transferring_checks_with_incorrect_ids_format_will_trigger_an_error()
    {
        $data = [
            'ids' => 'this is a string',
            'transfer_date' => Carbon::now()->toDateString()
        ];

        $this->getJson($this->uri.'/transfer', $data)
            ->seeJsonEquals($this->errorResponse([
                'ids' => array(
                    Lang::get('core::validation.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function transferring_checks_with_incorrect_transfer_date_format_will_trigger_an_error()
    {
        $data = [
            'ids' => [1],
            'transfer_date' => 99
        ];

        $this->getJson($this->uri.'/transfer', $data)
            ->seeJsonEquals($this->errorResponse([
                'transfer_date' => array(
                    Lang::get('core::validation.transfer.date.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function transferring_checks_with_correct_data_will_successfully_transfer_checks()
    {
        $data = $this->create();

        $parameters = [
            'ids' => [$data->id],
            'transfer_date' => Carbon::now()->toDateString()
        ];

        $this->getJson($this->uri.'/transfer', $parameters)
            ->seeJsonEquals($this->successfulResponse(
                [], 
                Lang::get('core::success.transferred')
            ))
            ->seeInDatabase($this->table, [
                'id' => $data->id,
                'transfer_date' => Carbon::now()->toDateString(),
                'status' => CheckStatus::TRANSFERRED
            ]);
    }

    protected function references($times = 4)
    {
        $this->bankAccounts = Factory::times($times)->create('bank_account');
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, array_merge(array(
            'bank_account_id' => $this->bankAccounts[rand(0, 3)]->id,
            'created_from' => 1
        ), $data));
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, array_merge(array(
            'bank_account_id' => $this->bankAccounts[rand(0, 3)]->id,
            'created_from' => 1,
        ), $data));
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'bank_account_id' => $this->bankAccounts[rand(0, 3)]->id,
            'created_from' => 1,
        ));
    }

    protected function transform($data)
    {
        return [
            'id' => $data->id,
            'transaction_date' => $data->transaction_date->toDateString(),
            'bank' => $data->bank->account_name,
            'check_no' => $data->check_no,
            'check_date' => $data->check_date->toDateString(),
            'transfer_date' => is_null($data->transfer_date) ? '' : $data->transfer_date->toDateString(),
            'amount' => $data->number()->amount,
            'remarks' => $data->remarks,
            'type' => $data->transactionable_type,
            'transaction_id' => $data->redirect_id
        ];
    }
}