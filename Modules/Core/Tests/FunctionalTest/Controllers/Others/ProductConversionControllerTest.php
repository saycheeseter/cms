<?php

use Laracasts\TestDummy\Factory;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;
use Litipk\BigNumbers\Decimal;
use Carbon\Carbon;

class ProductConversionControllerTest extends FunctionalTest
{
    protected $uri = '/product-conversion';
    protected $prefix = 'PCV';

    protected $tables = [
        'info' => 'product_conversion',
        'detail' => 'product_conversion_detail',
    ];

    protected $factories = [
        'info' => 'product_conversion',
        'detail' => 'product_conversion_detail',
    ];

    protected $grouping;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function visiting_the_transaction_page_while_not_logged_in_will_redirect_the_user_to_login_page()
    {
        $this->logout()->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_transaction_page_while_logged_in_will_successfully_display_the_page()
    {
        $this->visit($this->uri)->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_created_from_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->created_from,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'created_from'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_created_for_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->created_for,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'created_for'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_for_location_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->for_location,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'for_location'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->transaction_date->toDateTimeString(),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_approval_status_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->approval_status,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'approval_status'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_id_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->product_id,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'product_id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_sheet_number_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->sheet_number,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_deleted_at_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->deleted_at,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'deleted_at'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_using_a_filter_not_in_searchable_fields_will_be_ignored_and_return_the_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '',
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_that_doesnt_meet_the_filter_conditions_will_return_an_empty_collection()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 100),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array()
        ), Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_empty_data_will_trigger_an_error()
    {
        $this->postJson($this->uri, [])
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
                'info' => array(
                    Lang::get('core::validation.info.required')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.required')
                ),
                'info.requested_by' => array(
                    Lang::get('core::validation.request_by.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
                'info.product_id' => array(
                    Lang::get('core::validation.product.required')
                ),
                'info.qty' => array(
                    Lang::get('core::validation.qty.required')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_non_existing_created_for_for_location_requested_by_product_id_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['created_for'] = 99;
        $data['info']['for_location'] = 99;
        $data['info']['requested_by'] = 99;
        $data['info']['product_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.doesnt.exists')
                ),
                'info.requested_by' => array(
                    Lang::get('core::validation.user.doesnt.exists')
                ),
                'info.product_id' => array(
                    Lang::get('core::validation.product.doesnt.exists')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_incorrect_date_format_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['transaction_date'] = 'abc';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_incorrect_qty_format_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['qty'] = 'abc';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.qty' => array(
                    Lang::get('core::validation.qty.numeric')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_detail_with_empty_component_id_and_required_qty_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0]['component_id'] = '';
        $data['details'][0]['required_qty'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.component_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.component.id.required')
                ),
                'details.0.required_qty' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.required.qty.required')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_detail_with_non_existent_component_id_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0]['component_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.component_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.component.doesnt.exists')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_detail_with_incorrect_required_qty_format_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0]['required_qty'] = 'abc';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.required_qty' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.required.qty.numeric')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_detail_with_incorrect_format_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'] = 'abc';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.invalid.format')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_detail_with_empty_array_will_trigger_an_error()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                )
            )));
    }

    /**
     * @test
     */
    public function saving_correct_data_will_return_success_message()
    {
        $data = $this->build();
        $infoObject = $data['info'];

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $expected = array_merge(
            $this->item($infoObject, 'store'),
            array(
                'id' => '1',
                'sheet_number' => $this->generateReferenceNumber(1, Auth::branch()->id)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                ['record' => $expected],
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('product_conversion', $expected);
    }

        /**
     * @test
     */
    public function approval_status_cannot_be_change_if_transaction_is_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.deleted" => Lang::get(
                    'core::validation.transaction.number.deleted',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_for_approval_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.for.approval',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_for_approval_if_current_status_is_draft()
    {
        $created = $this->create();

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_approved_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.approve',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_approved_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_declined_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.decline',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_declined_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DECLINED
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_approved()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_approved()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DECLINED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_automatically_be_generated_if_transaction_is_approved_or_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => Auth::user()->id
            ))
            ->notSeeInDatabase($this->tables['info'], array(
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_be_set_to_null_if_transaction_is_reverted()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => null,
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_its_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            )));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            )));
    }

    /**
     * @test
     */
    public function transaction_will_be_successfully_soft_deleted_if_current_status_is_draft_and_not_yet_deleted()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.deleted')))
            ->notSeeInDatabase($this->tables['info'], array(
                'deleted_at' => null
            ));
    }

    /**
     * @test
     */
    public function approving_transaction_with_manual_grouping_type_will_convert_component_inventory_into_group_inventory()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL,
            'group_type' => GroupType::MANUAL_GROUP
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = [
            'group' => [
                'branch_id' => $created['info']->created_for,
                'product_id' => $this->grouping['grouping']->id,
                'branch_detail_id' => $created['info']->for_location,
                'qty' => $created['info']->qty->innerValue()
            ],
            'component' => [
                'branch_id' => $created['info']->created_for,
                'product_id' => $this->grouping['component']->id,
                'branch_detail_id' => $created['info']->for_location,
                'qty' => $created['info']->qty->mul($created['detail']->required_qty)->mul(Decimal::fromInteger(-1))->innerValue()
            ]
        ];

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase('product_branch_summary', $expected['group'])
            ->seeInDatabase('product_branch_summary', $expected['component']);
    }

    /**
     * @test
     */
    public function approving_transaction_with_manual_ungrouping_type_will_convert_component_inventory_into_group_inventory()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL,
            'group_type' => GroupType::MANUAL_UNGROUP
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = [
            'group' => [
                'branch_id' => $created['info']->created_for,
                'product_id' => $this->grouping['grouping']->id,
                'branch_detail_id' => $created['info']->for_location,
                'qty' => $created['info']->qty->mul(Decimal::fromInteger(-1))->innerValue()
            ],
            'component' => [
                'branch_id' => $created['info']->created_for,
                'product_id' => $this->grouping['component']->id,
                'branch_detail_id' => $created['info']->for_location,
                'qty' => $created['info']->qty->mul($created['detail']->required_qty)->innerValue()
            ]
        ];

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase('product_branch_summary', $expected['group'])
            ->seeInDatabase('product_branch_summary', $expected['component']);
    }

    protected function references()
    {
        $this->grouping = $this->createProductGrouping();
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], array_merge($info, [
            'product_id' => $this->grouping['grouping']->id
        ]));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'component_id' => $this->grouping['component']->id
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    /**
     * Build persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function create($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['info'], array_merge($info, [
            'product_id' => $this->grouping['grouping']->id
        ]));

        $detail = Factory::create($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'component_id' => $this->grouping['component']->id
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => $detail
        );
    }

    protected function list($data)
    {
        return [
            'id' => $data->id,
            'transaction_date' => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'requested_by'          => $data->requested->full_name,
            'created_for' => $data->for->name,
            'product' => $data->product->name,
            'qty' => $data->number()->qty,
            'created_by' => $data->creator->full_name,
            'for_location'          => $data->location->name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'deleted'               => $data->presenter()->deleted,
        ];
    }

    protected function store($model)
    {
        return [
            'id'               => $model->id,
            'sheet_number'     => $model->sheet_number,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
        ];
    }

    protected function createProductGrouping($grouping = array(), $component = array(), $productComponent = array())
    {
        $grouping = Factory::create('product_group', $grouping);
        $component = Factory::create('component', $component);

        $productComponent = Factory::create('product_component', 
            array_merge([
                'group_id' => $grouping->id,
                'component_id' => $component->id,
            ], 
            $productComponent));

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $grouping->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $component->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        return [
            'grouping' => $grouping,
            'component' => $component,
            'productComponent' => $productComponent
        ];
    }

    protected function generateReferenceNumber($id, $branch)
    {
        return sprintf('%s%s%s', 
            $this->prefix, 
            str_pad($branch, 3, '0', STR_PAD_LEFT),
            str_pad($id, 7, '0', STR_PAD_LEFT)
        );
    }
}