<?php 

use Laracasts\TestDummy\Factory;

use Modules\Core\Enums\SelectType;
use Modules\Core\Enums\TargetType;

use Carbon\Carbon;

class ProductDiscountControllerTest extends FunctionalTest
{
    private $uri = '/product-discount';

    private $factory = array(
        'info' => 'product_discount',
        'detail' => 'product_discount_detail',
        'target' => 'product_discount_target'
    );

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_product_discount_list_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_product_discount_create_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get(sprintf('%s/create', $this->uri))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_product_discount_detail_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get(sprintf('%s/1/edit', $this->uri))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_product_discount_list_route_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas(['branches', 'permissions']);
    }

    /**
     * @test
     */
    public function searching_for_data_with_no_filters_will_return_data()
    {
        $data = $this->create();

        $this->getJson($this->uri)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data[0]['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data[0]['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_target_type_will_return_the_corresponding_collection_of_data()
    {
        $this->collection();
        $data = $this->create(['info' => ['target_type' => 2]]);

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->target_type,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'target_type'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_discount_scheme_will_return_the_corresponding_collection_of_data()
    {
        $this->collection();
        $data = $this->create(['info' => ['discount_scheme' => 2]]);

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->discount_scheme,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'discount_scheme'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_select_type_will_return_the_corresponding_collection_of_data()
    {
        $this->collection();
        $data = $this->create(['info' => ['select_type' => 2]]);

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->select_type,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'select_type'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_status_will_return_the_corresponding_collection_of_data()
    {
        $this->collection();
        $data = $this->create(['info' => ['status' => 0]]);

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->status,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'status'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_created_for_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->created_for,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'created_for'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect($data, 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_valid_from_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->valid_from->toDateTimeString(),
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'valid_from'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data[0]['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_valid_to_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->valid_to->toDateTimeString(),
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'valid_to'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array($this->item($data[0]['info'], 'list'))
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect(array_pluck($data, 'info'), 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 'random string',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_empty_data_will_trigger_error_response()
    {
        $this->postJson($this->uri, [])
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'info.valid_from' => array(
                    Lang::get('core::validation.valid_from.required')
                ),
                'info.valid_to' => array(
                    Lang::get('core::validation.valid_to.required')
                ),
                'info.status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'info.mon' => array(
                    Lang::get('core::validation.mon.required')
                ),
                'info.tue' => array(
                    Lang::get('core::validation.tue.required')
                ),
                'info.wed' => array(
                    Lang::get('core::validation.wed.required')
                ),
                'info.thur' => array(
                    Lang::get('core::validation.thur.required')
                ),
                'info.fri' => array(
                    Lang::get('core::validation.fri.required')
                ),
                'info.sat' => array(
                    Lang::get('core::validation.sat.required')
                ),
                'info.sun' => array(
                    Lang::get('core::validation.sun.required')
                ),
                'info.status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'info.target_type' => array(
                    Lang::get('core::validation.target.type.required')
                ),
                'info.discount_scheme' => array(
                    Lang::get('core::validation.discount.scheme.required')
                ),
                'info.select_type' => array(
                    Lang::get('core::validation.select.type.required')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'details' => array(
                    Lang::get('core::validation.details.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_non_existing_branch_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['created_for'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_existing_code_or_name_will_trigger_error_response()
    {
        $created = $this->create();

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['code'] = $created['info']->code;
        $data['info']['name'] = $created['info']->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_code_or_name_format_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['code'] = 99;
        $data['info']['name'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.string')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.string')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_valid_to_or_from_format_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = 'string';
        $data['info']['valid_to'] = 'string';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.valid_from' => array(
                    Lang::get('core::validation.valid_from.date')
                ),
                'info.valid_to' => array(
                    Lang::get('core::validation.valid_to.date')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_target_type_discount_scheme_or_select_type_format_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = 'string';
        $data['info']['discount_scheme'] = 'string';
        $data['info']['select_type'] = 'string';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.target_type' => array(
                    Lang::get('core::validation.target.type.numeric')
                ),
                'info.discount_scheme' => array(
                    Lang::get('core::validation.discount.scheme.numeric')
                ),
                'info.select_type' => array(
                    Lang::get('core::validation.select.type.numeric')
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_details_with_no_data_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['details'] = [[]];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.entity_type' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.entity.type.required')
                ),
                'details.0.discount_type' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.type.required')
                ),
                'details.0.discount_value' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.value.required')
                ),
                'details.0.qty' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.qty.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_format_of_discount_type_discount_value_or_qty_in_details_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['details'][0]['discount_type'] = 'string';
        $data['details'][0]['discount_value'] = 'string';
        $data['details'][0]['qty'] = 'string';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.discount_type' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.type.numeric')
                ),
                'details.0.discount_value' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.discount.value.numeric')
                ),
                'details.0.qty' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.qty.numeric')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_empty_targets_with_target_type_specific_member_type_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER_TYPE;
        $data['targets'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'targets' => array(
                    Lang::get('core::validation.targets.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_empty_targets_with_target_type_specific_member_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER;
        $data['targets'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'targets' => array(
                    Lang::get('core::validation.targets.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_empty_targets_with_target_type_specific_customer_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = TargetType::SPECIFIC_CUSTOMER;
        $data['targets'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'targets' => array(
                    Lang::get('core::validation.targets.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_target_with_no_data_with_specific_target_type_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER_TYPE;
        $data['targets'] = [[]];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'targets.0.target_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.target.id.required')
                ),
                'targets.0.target_type' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.target.type.required')
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_target_id_or_target_type_format_with_specific_target_type_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER_TYPE;
        $data['targets'][0]['target_id'] = 'string';
        $data['targets'][0]['target_type'] = 'string';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'targets.0.target_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.target.id.numeric')
                ),
                'targets.0.target_type' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.target.type.numeric')
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_valid_to_before_valid_from_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateTimeString();
        $data['info']['valid_to'] = Carbon::yesterday()->toDateString();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'invalid.date.range' => array(Lang::get('core::validation.invalid.date.range'))
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_no_selected_day_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['mon'] = false;
        $data['info']['tue'] = false;
        $data['info']['wed'] = false;
        $data['info']['thur'] = false;
        $data['info']['fri'] = false;
        $data['info']['sat'] = false;
        $data['info']['sun'] = false;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'min.day' => array(Lang::get('core::validation.min.day'))
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_overlapping_info_select_type_all_products_and_target_type_all_customers_will_trigger_error_response()
    {
        $created = $this->create([
            'info' => [
                'valid_from' => Carbon::today()->toDateTimeString(),
                'valid_to' => Carbon::now()->toDateString().' 23:59:59',
                'target_type' => TargetType::ALL_CUSTOMERS,
                'select_type' => SelectType::ALL_PRODUCTS,
                'status' => true,
                'mon' => true
            ]
        ]);

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateString();
        $data['info']['valid_to'] = Carbon::now()->toDateString();
        $data['info']['target_type'] = TargetType::ALL_CUSTOMERS;
        $data['info']['select_type'] = SelectType::ALL_PRODUCTS;
        $data['info']['status'] = true;
        $data['info']['mon'] = true;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.discount.overlap' => array(
                    Lang::get('core::validation.product.discount.overlap').' '.$created['info']->code.' '.$created['info']->name
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_overlapping_info_select_type_all_products_and_target_type_all_members_will_trigger_error_response()
    {
        $created = $this->create([
            'info' => [
                'valid_from' => Carbon::today()->toDateTimeString(),
                'valid_to' => Carbon::now()->toDateString().' 23:59:59',
                'target_type' => TargetType::ALL_MEMBERS,
                'select_type' => SelectType::ALL_PRODUCTS,
                'status' => true,
                'mon' => true
            ]
        ]);

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateString();
        $data['info']['valid_to'] = Carbon::now()->toDateString();
        $data['info']['target_type'] = TargetType::ALL_MEMBERS;
        $data['info']['select_type'] = SelectType::ALL_PRODUCTS;
        $data['info']['status'] = true;
        $data['info']['mon'] = true;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.discount.overlap' => array(
                    Lang::get('core::validation.product.discount.overlap').' '.$created['info']->code.' '.$created['info']->name
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_overlapping_info_specific_select_type_and_specific_target_type_will_trigger_error_response()
    {
        $created = $this->create([
            'info' => [
                'valid_from' => Carbon::today()->toDateTimeString(),
                'valid_to' => Carbon::now()->toDateString().' 23:59:59',
                'target_type' => TargetType::SPECIFIC_MEMBER,
                'select_type' => SelectType::SPECIFIC_PRODUCT,
                'status' => true,
                'mon' => true
            ],
            'target' => [
                'target_id' => 2
            ],
            'detail' => [
                'entity_id' => 2
            ]
        ]);

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateString();
        $data['info']['valid_to'] = Carbon::now()->toDateString();
        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER;
        $data['info']['select_type'] = SelectType::SPECIFIC_PRODUCT;
        $data['info']['status'] = true;
        $data['info']['mon'] = true;
        $data['targets'][0]['target_id'] = 2;
        $data['details'][0]['entity_id'] = 2;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.discount.overlap' => array(
                    Lang::get('core::validation.product.discount.overlap').' '.$created['info']->code.' '.$created['info']->name
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_overlapping_info_specific_select_type_will_trigger_error_response()
    {
        $created = $this->create([
            'info' => [
                'valid_from' => Carbon::today()->toDateTimeString(),
                'valid_to' => Carbon::now()->toDateString().' 23:59:59',
                'target_type' => TargetType::ALL_CUSTOMERS,
                'select_type' => SelectType::SPECIFIC_PRODUCT,
                'status' => true,
                'mon' => true
            ],
            'detail' => [
                'entity_id' => 2
            ]
        ]);

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateString();
        $data['info']['valid_to'] = Carbon::now()->toDateString();
        $data['info']['target_type'] = TargetType::ALL_CUSTOMERS;
        $data['info']['select_type'] = SelectType::SPECIFIC_PRODUCT;
        $data['info']['status'] = true;
        $data['info']['mon'] = true;
        $data['details'][0]['entity_id'] = 2;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.discount.overlap' => array(
                    Lang::get('core::validation.product.discount.overlap').' '.$created['info']->code.' '.$created['info']->name
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_overlapping_info_specific_target_type_will_trigger_error_response()
    {
        $created = $this->create([
            'info' => [
                'valid_from' => Carbon::today()->toDateTimeString(),
                'valid_to' => Carbon::now()->toDateString().' 23:59:59',
                'target_type' => TargetType::SPECIFIC_MEMBER,
                'select_type' => SelectType::ALL_PRODUCTS,
                'status' => true,
                'mon' => true
            ],
            'target' => [
                'target_id' => 2
            ]
        ]);

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::today()->toDateString();
        $data['info']['valid_to'] = Carbon::now()->toDateString();
        $data['info']['target_type'] = TargetType::SPECIFIC_MEMBER;
        $data['info']['select_type'] = SelectType::ALL_PRODUCTS;
        $data['info']['status'] = true;
        $data['info']['mon'] = true;
        $data['targets'][0]['target_id'] = 2;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.discount.overlap' => array(
                    Lang::get('core::validation.product.discount.overlap').' '.$created['info']->code.' '.$created['info']->name
                )
        ]));
    }

    /**
     * @test
     */
    public function saving_correct_data_will_return_successful_message()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::now()->toDateTimeString();
        $data['info']['valid_to'] = Carbon::now()->toDateString().' 23:59:59';

        $expectedInfo = $data['info'];

        $data['info']['valid_from'] = Carbon::parse($data['info']['valid_from'])->toDateString();
        $data['info']['valid_to'] = Carbon::parse($data['info']['valid_to'])->toDateString();

        $expected = array_merge(
            array_except($expectedInfo, [
                'created_by',
                'created_from',
            ]), [
                'id' => 1
            ]
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                ['record' => $expected],
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('product_discount', array_except($expected, [
                'valid_from',
                'valid_to'
            ]));
    }

    /**
     * @test
     */
    public function updating_correct_data_will_return_successful_message()
    {
        $created = $this->create();

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['details'])],
            'targets' => [$this->toArrayCast($data['targets'])]
        ];

        $data['info']['valid_from'] = Carbon::now()->toDateTimeString();
        $data['info']['valid_to'] = Carbon::now()->toDateString().' 23:59:59';

        $expectedInfo = $data['info'];

        $data['info']['valid_from'] = Carbon::parse($data['info']['valid_from'])->toDateString();
        $data['info']['valid_to'] = Carbon::parse($data['info']['valid_to'])->toDateString();

        $expected = array_merge(
            array_except($expectedInfo, [
                'created_by',
                'created_from',
            ]), [
                'id' => $created['info']->id,
            ]
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->seeInDatabase('product_discount', array_except($expected, [
                'valid_from',
                'valid_to'
            ]));
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('counter', array(
                    'id' => $created['info']->id,
                    'deleted_at' => NULL
                )
            );
    }

    protected function build($data = array())
    {
        $discount = Factory::build(
            $this->factory['info'], 
            array_merge([
                    'valid_from' => Carbon::now()->toDateString(),
                    'valid_to' => Carbon::tomorrow()->toDateString()
                ],
                isset($data['info']) 
                    ? $data['info']
                    : []
            )
        );

        $details = Factory::build(
            $this->factory['detail'],
            array_merge([
                    'product_discount_id' => $discount->id
                ], 
                isset($data['detail']) 
                    ? $data['detail']
                    : []
            )
        );

        $targets = Factory::build(
            $this->factory['target'],
            array_merge([
                    'product_discount_id' => $discount->id
                ],
                isset($data['target']) 
                    ? $data['target']
                    : []
            )
        );

        return array(
            'info' => $discount,
            'details' => $details,
            'targets' => $targets
        );
    }

    protected function create($data = array())
    {   
        $discount = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('product_discount_id' => $discount->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        $targets = Factory::times(2)->create(
            $this->factory['target'], 
            array_merge(
                array('product_discount_id' => $discount->id),
                isset($data['target']) 
                ? $data['target']
                : []
            )
        );

        return array(
            'info' => $discount,
            'details' => $details,
            'targets' => $targets
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function list($data)
    {
        if($data['info']) {
            $data = $data['info'];
        }

        return [
            'id' => $data->id,
            'code' => $data->code,
            'name' => $data->name,
            'valid_from' => $data->valid_from->toDateTimeString(),
            'valid_to' => $data->valid_to->toDateTimeString(),
            'target_type' => $data->presenter()->targetType(),
            'discount_scheme' => $data->presenter()->discountScheme(),
            'select_type' => $data->presenter()->selectType(),
            'status' => $data->presenter()->status(),
            'created_for' => $data->for->name
        ];
    }

    protected function info($data)
    {
        return [
            'id'              => $data->id,
            'code'            => $data->code,
            'name'            => $data->name,
            'valid_from'      => $data->valid_from->toDateTimeString(),
            'valid_to'        => $data->valid_to->toDateTimeString(),
            'status'          => $data->status,
            'mon'             => $data->mon,
            'tue'             => $data->tue,
            'wed'             => $data->wed,
            'thur'            => $data->thur,
            'fri'             => $data->fri,
            'sat'             => $data->sat,
            'sun'             => $data->sun,
            'target_type'     => $data->target_type,
            'discount_scheme' => $data->discount_scheme,
            'select_type'     => $data->select_type,
            'created_for'     => $data->created_for
        ];
    }

    protected function details($data)
    {
        return [
            'entity_id' => $data->entity_id,
            'entity_type' => $data->discount->select_type,
            'entity_type_label' => $data->presenter()->entityTypeLabel(),
            'name' => $data->entity->name ?? ($data->entity->full_name ?? Lang::get('core::label.all.products')),
            'discount_type' => $data->discount_type,
            'discount_value' => $data->number()->discount_value,
            'qty' => $data->number()->qty,
        ];
    }

    protected function targets($data)
    {
        return [
            'target_id' => $data->target_id,
            'target_type' => $data->discount->target_type,
            'target_type_label' => $data->presenter()->targetTypeLabel(),
            'target_name' => $data->target->name ?? ($data->target->full_name ?? '')
        ];
    }
}