<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\GiftCheckStatus;

class GiftCheckControllerTest extends FunctionalTest
{
    private $uri = '/gift-check';

    private $factory = 'gift_check';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_gift_check_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_the_gift_check_page_will_successfully_display_the_page_and_return_the_list_of_permissions()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('permissions');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'gift_checks' => $this->collect([$data[1]], 'basic')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_check_number_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->check_number,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'check_number'
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'gift_checks' => $this->collect([$data[2]], 'basic')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_status_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection(1);

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->status,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'status'
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'gift_checks' => $this->collect([$data], 'basic')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'gift_checks' => $this->collect($data, 'basic')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'check_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'gift_checks' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_check_number_amount_date_from_or_date_to_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $data['code'] = '';
        $data['check_number'] = '';
        $data['amount'] = '';
        $data['date_from'] = '';
        $data['date_to'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'check_number' => array(
                    Lang::get('core::validation.check.number.required')
                ),
                'amount' => array(
                    Lang::get('core::validation.amount.required')
                ),
                'date_from' => array(
                    Lang::get('core::validation.date.from.required')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_amount_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $data['amount'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'amount' => array(
                    Lang::get('core::validation.amount.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_amount_less_than_or_equal_to_zero_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $data['amount'] = -1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'amount' => array(
                    Lang::get('core::validation.amount.greater.than.zero')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_date_from_or_date_to_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $data['date_from'] = Carbon::parse($data['date_from'])->toDateTimeString();
        $data['date_to'] = Carbon::parse($data['date_to'])->toDateTimeString();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'date_from' => array(
                    Lang::get('core::validation.date.from.invalid.format')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_date_from_or_date_to_range_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $buffer = $data['date_from'];

        $data['date_from'] = $data['date_to'];
        $data['date_to'] = $buffer;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'date_from' => array(
                    Lang::get('core::validation.date.from.invalid.range')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.invalid.range')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_already_existing_code_or_check_number_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->toArrayCast($build);

        $data['code'] = $created->code;
        $data['check_number'] = $created->check_number;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'check_number' => array(
                    Lang::get('core::validation.check.number.exists')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $expected = array(
            'response' => array(
                'gift_check' => array_merge(
                    $this->item($build, 'basic'),
                    array('id' => 1)
                )
            ),
            'database' => array_merge(
                $data,
                array(
                    'id' => 1,
                    'date_from' => $build->date_from->startOfDay()->toDateTimeString(),
                    'date_to' => $build->date_to->endOfDay()->toDateTimeString(),
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('gift_check', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_code_check_number_amount_date_from_or_date_to_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArrayCast($created);

        $data['code'] = '';
        $data['check_number'] = '';
        $data['amount'] = '';
        $data['date_from'] = '';
        $data['date_to'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'check_number' => array(
                    Lang::get('core::validation.check.number.required')
                ),
                'amount' => array(
                    Lang::get('core::validation.amount.required')
                ),
                'date_from' => array(
                    Lang::get('core::validation.date.from.required')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_non_numeric_amount_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArrayCast($created);

        $data['amount'] = 'A';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'amount' => array(
                    Lang::get('core::validation.amount.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_amount_less_than_or_equal_to_zero_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArrayCast($created);

        $data['amount'] = -1;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'amount' => array(
                    Lang::get('core::validation.amount.greater.than.zero')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_invalid_date_from_or_date_to_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArrayCast($created);

        $data['date_from'] = Carbon::parse($data['date_from'])->toDateTimeString();
        $data['date_to'] = Carbon::parse($data['date_to'])->toDateTimeString();

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'date_from' => array(
                    Lang::get('core::validation.date.from.invalid.format')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_invalid_date_from_or_date_to_range_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArrayCast($created);

        $buffer = $data['date_from'];

        $data['date_from'] = $data['date_to'];
        $data['date_to'] = $buffer;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'date_from' => array(
                    Lang::get('core::validation.date.from.invalid.range')
                ),
                'date_to' => array(
                    Lang::get('core::validation.date.to.invalid.range')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_already_existing_code_or_check_number_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->toArrayCast($created);

        $data['code'] = $existing->code;
        $data['check_number'] = $existing->check_number;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'check_number' => array(
                    Lang::get('core::validation.check.number.exists')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_an_already_redeemed_gift_check_will_return_an_error_response()
    {
        $created = $this->create(['status' => GiftCheckStatus::REDEEMED]);

        $data = $this->toArrayCast($created);

        $data['code'] = $this->faker->numberBetween(10000000, 19999999);
        $data['check_number'] = $this->faker->numberBetween(10000000, 19999999);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'cannot.update.redeemed.gift.check' => Lang::get('core::validation.cannot.update.redeemed.gift.check')
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArrayCast($build);

        $this->patchJson(sprintf('%s/5', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('gift_check', [
                'id' => 5
            ]);
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_proper_format_will_successfully_update_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->toArrayCast($build);

        $expected = array(
            'response' => array(
                'gift_check' => array_merge(
                    $this->item($build, 'basic'),
                    array('id' => $created->id)
                )
            ),
            'database' => array_merge(
                $data,
                array(
                    'id' => $created->id,
                    'date_from' => $build->date_from->startOfDay()->toDateTimeString(),
                    'date_to' => $build->date_to->endOfDay()->toDateTimeString(),
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase('gift_check', $expected['database']);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $expected = array(
            'id' => $created->id,
            'deleted_at' => null
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('gift_check', $expected);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('gift_check', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_show_api_will_return_the_full_info_of_the_corresponding_member()
    {
        $created = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([
                'gift_check' => $this->item($created, 'full')
            ]));
    }

    private function build($data = [])
    {
        return Factory::build($this->factory, $data);
    }

    private function create($data = [])
    {
        return Factory::create($this->factory, $data);
    }

    private function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function toArrayCast($model)
    {
        $data = parent::toArrayCast($model);

        $data['date_from'] = Carbon::parse($data['date_from'])->toDateString();
        $data['date_to'] = Carbon::parse($data['date_to'])->toDateString();

        return $data;
    }

    protected function basic($model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'check_number' => $model->check_number,
            'date_from'   => $model->date_from->toDateString(),
            'date_to'      => $model->date_to->toDateString(),
            'amount'       => $model->number()->amount,
            'status'       => $model->presenter()->status
        ];
    }

    protected function full($model)
    {
        return [
            'id'               => $model->id,
            'code'             => $model->code,
            'check_number'     => $model->check_number,
            'date_from'        => $model->date_from->toDateString(),
            'date_to'          => $model->date_to->toDateString(),
            'amount'           => $model->number()->amount,
            'status'           => $model->status,
            'status_label'     => $model->presenter()->status,
            'redeemed_by'      => $model->redeemer->full_name ?? '',
            'redeemed_date'    => is_null($model->redeemed_date) ? '' : $model->redeemed_date->toDateTimeString(),
            'reference_number' => $model->reference_number ?? '',
        ];
    }
}
