<?php 

use Laracasts\TestDummy\Factory;

class CounterControllerTest extends FunctionalTest
{
    private $uri = '/counter';
    private $factory = [
        'info' => 'counter',
        'detail' => 'counter_detail'
    ];
    private $references = [];
    private $prefix = 'CTR';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->createReferences();
    }

    /**
     * @test
     */
    public function accessing_counter_list_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_counter_create_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get(sprintf('%s/create', $this->uri))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_counter_detail_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get(sprintf('%s/1/edit', $this->uri))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_counter_list_route_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas(['branches', 'customers', 'permissions', 'templates']);
    }

    /**
     * @test
     */
    public function searching_for_data_with_no_filters_will_return_data()
    {
        $data = $this->create();

        $this->getJson($this->uri)->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_data_with_sheet_number_will_return_filtered_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $collection[0]['info']->sheet_number,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($collection[0]['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_data_with_customer_will_return_filtered_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $collection[0]['info']->customer_id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'customer_id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($collection[0]['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_data_with_created_for_will_return_filtered_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['info']->created_for,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'created_for'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_data_with_transaction_date_will_return_filtered_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $collection[0]['info']->transaction_date->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                ),
                array(
                    'value' => $collection[0]['info']->transaction_date->toDateTimeString(),
                    'operator' => '<=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($collection[0]['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_data_with_remarks_will_return_filtered_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $collection[0]['info']->remarks,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'remarks'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($collection[0]['info'], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($collection),
                    'count' => count($collection),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect(array_pluck($collection, 'info'), 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $collection = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '999',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_empty_data_will_trigger_error_response()
    {
        $this->postJson($this->uri, [])
            ->seeJsonEquals($this->errorResponse([
                'info' => array(
                    Lang::get('core::validation.info.required')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_non_existing_branch_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['created_for'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_non_existing_customer_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['customer_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.doesnt.exists')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_invalid_transaction_date_format_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['info']['transaction_date'] = 'aa';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_non_existent_reference_id_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0]['reference_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.reference_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.reference.doesnt.exists')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_incorrect_remaining_amount_format_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0]['remaining_amount'] = 'a';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.remaining_amount' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.remaining.amount.numeric')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_data_with_empty_reference_id_and_remaining_amount_will_trigger_error_response()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $data['details'][0] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.reference_id' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.reference.required')
                ),
                'details.0.remaining_amount' => array(
                    Lang::get('core::error.row').'[1] '.Lang::get('core::validation.remaining.amount.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_correct_data_will_return_successful_message()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $expected = array_merge(
            array_except($data['info'], [
                'created_by',
                'created_from'
            ]), [
                'id' => 1,
                'sheet_number' => $this->generateReferenceNumber(1, 1),
                'total_amount' => $data['details'][0]['remaining_amount']
            ]
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                ['record' => $expected],
                Lang::get('core::success.created'))
            )
            ->seeInDatabase('counter', $expected);
    }

    /**
     * @test
     */
    public function updating_correct_data_will_return_successful_message()
    {
        $created = $this->create();

        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $expected = array_merge(
            array_except($data['info'], [
                'created_by',
                'created_from'
            ]), [
                'id' => $created['info']->id,
                'sheet_number' => $this->generateReferenceNumber(1, 1),
                'total_amount' => $data['details'][0]['remaining_amount']
            ]
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->seeInDatabase('counter', $expected);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('counter', array(
                    'id' => $created['info']->id,
                    'deleted_at' => NULL
                )
            );
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_saving_the_transaction_details()
    {
        $data = $this->build();

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $expected = array(
            'id' => 1,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['remaining_amount'];
            }),
        );

        $this->postJson($this->uri, $data)
           ->seeInDatabase('counter', $expected);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_updating_the_transaction_details()
    {
        $data = $this->create();

        $build = $this->build([], [
            'transaction_id' => $data['info']->id
        ]);

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'])],
        ];

        $build = [
            'info' => $this->toArrayCast($build['info']),
            'details' => [$this->toArrayCast($build['detail'])],
        ];

        $data['details'][] = $build['details'][0];

        $expected = array(
            'id' => $data['info']['id'],
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['remaining_amount'];
            }),
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $data['info']['id']), $data)
            ->seeInDatabase('counter', $expected);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_deleting_the_transaction_details()
    {
        $data = $this->create([], [], 2);

        $data = [
            'info' => $this->toArrayCast($data['info']),
            'details' => [$this->toArrayCast($data['detail'][0])],
        ];

        $expected = array(
            'id' => $data['info']['id'],
            'total_amount' => $data['details'][0]['remaining_amount']
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $data['info']['id']), $data)
            ->seeInDatabase('counter', $expected);
    }

    protected function createReferences()
    {
        $this->references['customer'] = Factory::create('customer');

        $this->references['customer_detail'] = Factory::create('customer_detail', [
            'head_id' => $this->references['customer']->id
        ]);

        $this->references['sales_outbound'] = Factory::create('sales_outbound', [
            'remaining_amount' => 100, 
            'approval_status' => 3,
            'customer_type' => 1,
            'customer_id' => $this->references['customer']->id
        ]);

        $this->references['sales_outbound_detail'] = Factory::create('sales_outbound_detail', [
            'transaction_id' => $this->references['sales_outbound']->id
        ]);
    }

    protected function create($data = array(), $detailData = array(), $detailTimes = 1)
    {
        $info = Factory::create($this->factory['info'], $data);
        $detail = Factory::times($detailTimes)->create($this->factory['detail'], array_merge([
                'transaction_id' => $info->id
            ], $detailData)
        );

        return [
            'info' => $info->fresh(),
            'detail' => $detail
        ];
    }

    protected function build($data = array(), $detailData = array())
    {
        $info = Factory::build($this->factory['info'], array_merge($data, [
            'customer_id' => $this->references['customer']->id
        ]));
        $detail = Factory::build($this->factory['detail'], array_merge([
                'transaction_id' => $info->id,
                'reference_id' => $this->references['sales_outbound']->id
            ], $detailData)
        );

        return [
            'info' => $info,
            'detail' => $detail
        ];
    }

    protected function collection($times = 4)
    {
        $data = [];

        for($i = 0; $i < $times; $i++){
            array_push($data, $this->create());
        }

        return $data;
    }

    protected function list($data)
    {
        return [
            'id'               => $data->id,
            'created_for'      => $data->for->name,
            'customer'         => $data->customer->name,
            'transaction_date' => $data->transaction_date->toDateTimeString(),
            'sheet_number'     => $data->sheet_number,
            'remarks'          => $data->remarks,
            'total_amount'     => $data->number()->total_amount
        ];
    }

    protected function full($data)
    {
        return [
            'id'               => $data->id,
            'created_for'      => $data->created_for,
            'customer_id'      => $data->customer_id,
            'transaction_date' => $data->transaction_date->toDateTimeString(),
            'sheet_number'     => $data->sheet_number,
            'remarks'          => $data->remarks
        ];
    }

    protected function details($data)
    {
        return [
            'id'               => $data->id,
            'transaction_date' => $data->reference->transaction_date->toDateString(),
            'sheet_number'     => $data->reference->sheet_number,
            'remaining_amount' => $data->number()->remaining_amount,
            'reference_id'     => $data->reference_id
        ];
    }

    protected function generateReferenceNumber($id, $branch)
    {
        return sprintf('%s%s%s', 
            $this->prefix, 
            str_pad($branch, 3, '0', STR_PAD_LEFT),
            str_pad($id, 7, '0', STR_PAD_LEFT)
        );
    }
}