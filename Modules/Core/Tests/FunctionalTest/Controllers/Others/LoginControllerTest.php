<?php

use Laracasts\TestDummy\Factory;

class LoginControllerTest extends FunctionalTest
{
    public function setUp()
    {
        parent::setUp();

        $this->init();
    }

    /**
     * @test
     */
    public function displays_login_page()
    {
        $this->visit('/')
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function logging_in_without_username_or_password_will_trigger_error_validation()
    {
        $this->postJson('/', $this->data('', ''))
            ->seeJsonEquals($this->errorResponse([
                'username' => array(
                    Lang::get('core::validation.username.required')
                ),
                'password' => array(
                    Lang::get('core::validation.password.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function logging_in_incorrect_credentials_will_return_an_error_message()
    {
        $this->postJson('/', $this->data())
            ->seeJsonEquals($this->noEntryFoundResponse([
                'auth' => Lang::get('core::validation.auth.failed')
            ]));
    }

    /**
     * @test
     */
    public function logging_in_after_five_unsuccessful_attempt_will_trigger_throttle_response()
    {
        for ($i=0; $i < 5; $i++) { 
            $this->postJson('/', $this->data())
                ->seeJsonEquals($this->noEntryFoundResponse([
                    'auth' => Lang::get('core::validation.auth.failed')
                ]));
        }

        $this->postJson('/', $this->data())
            ->seeJsonEquals($this->errorResponse([
                'throttle' => Lang::get('core::validation.auth.throttle', [
                    'seconds' => 60
                ])
            ]));
    }

    /**
     * @test
     */
    public function logging_in_with_valid_credentials_but_no_assigned_branch_will_trigger_an_error_response()
    {
        $username = $this->faker->userName();
        $password = $this->faker->password();

        $user = Factory::create('user', [
            'username' => $username,
            'password' => $password
        ]);

        $this->postJson('/', [
            'username' => $username,
            'password' => $password,
        ])
        ->seeJsonEquals($this->noEntryFoundResponse([
            'no.branch.found' => Lang::get('core::validation.no.branch.found')
        ]));
    }

    /**
     * @test
     */
    public function logging_in_with_valid_credentials_and_assigned_branch_will_display_the_list_of_branches()
    {
        $this->postJson('/', $this->data('superadmin', '121586'))
            ->seeJsonEquals($this->successfulResponse([
                'branches' => array(
                    array(
                        'id' => 1,
                        'label' => 'Main'
                    )
                )
            ]));
    }

    /**
     * @test
     */
    public function selecting_a_branch_will_successfully_login_the_user_and_return_a_successful_response()
    {
        $this->login()
            ->seeJsonEquals($this->successfulResponse(
                [], 
                Lang::get('core::success.connected')
            ))
            ->assertTrue(Auth::check());
    }

    /**
     * @test
     */
    public function returns_an_error_message_if_passed_branch_id_is_zero()
    {
        Auth::loginUsingId(1);

        $this->postJson('/auth', ['id' => 0])
            ->seeJsonEquals($this->errorResponse([
                'auth' => Lang::get('core::validation.no.branch.selected')
            ]));
    }

    /**
     * @test
     */
    public function returns_an_error_message_if_passed_branch_id_is_not_existing_in_database()
    {
        Auth::loginUsingId(1);

        $this->postJson('/auth', ['id' => 3])
            ->seeJsonEquals($this->errorResponse([
                'auth' => Lang::get('core::validation.branch.not.found')
            ]));
    }

    /**
     * @test
     */
    public function logging_out_will_clear_user_session_and_redirect_to_login()
    {
        $this->login();

        $this->getJson('/logout')
            ->assertRedirectedTo('/')
            ->assertFalse(Auth::check());
    }

    /**
     * @test
     */
    public function accessing_login_page_while_still_authenticated_will_redirect_the_user_to_dashboard()
    {
        $this->login();

        $this->get('/')->assertRedirectedTo('/dashboard');
    }

    protected function data($username = null, $password = null)
    {
        return array(
            'username' => $username ?? 'foo', 
            'password' => $password ?? 'bar'
        );
    }
}
