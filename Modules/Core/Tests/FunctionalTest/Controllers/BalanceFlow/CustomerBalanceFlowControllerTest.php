<?php

class CustomerBalanceFlowControllerTest extends BalanceFlowControllerTest
{
    protected $uri = 'reports/customer/balance';
    protected $factory = 'customer_balance_flow';


    /**
     * @test
     */
    public function visiting_the_balance_flow_page_while_user_is_authenticated_will_successfully_display_the_page_with_its_reference()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('customers');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_customer_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->collection(3);

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => [$this->item($data, 'transform')]
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->customer->name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'customer.name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }
}