<?php 

use Laracasts\TestDummy\Factory;
use Illuminate\Support\Collection;
use Modules\Core\Enums\FlowType;

use Litipk\BigNumbers\Decimal;
use Carbon\Carbon;

abstract class BalanceFlowControllerTest extends FunctionalTest
{
    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function visiting_the_balance_flow_page_user_while_user_is_not_logged_in_will_redirect_the_user_to_login_page()
    {
        $this->logout()->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function searching_for_a_data_without_filters_will_return_no_data()
    {
        $data = $this->create();

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => []
        ];

        $this->getJson($this->uri)
            ->seeJsonEquals($this->successfulResponse(
                $expected, 
                Lang::get('core::info.no.results.found')
            ));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_will_return_the_corresponding_collection_of_data_within_the_specified_date()
    {
        $collection = $this->collection()->sortByDesc('transaction_date');

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => $this->collect($collection, 'transform')
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $collection->last()->transaction_date->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                ),
                array(
                    'value' => $collection->first()->transaction_date->toDateTimeString(),
                    'operator' => '<=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_that_is_not_the_earliest_date_will_return_a_collection_of_data_within_the_specified_data_with_a_beginning_amount()
    {
        $collection = $this->collection()->sortBy('transaction_date')->values();

        $filtered = $collection->except(0);

        $expected = [
            'beginning' => [
                'amount' => number_format($collection->first()->flow_type === FlowType::IN
                    ? $collection->first()->amount->innerValue()
                    : $collection->first()->amount->additiveInverse()->innerValue()
                , setting('monetary.precision'))
            ],
            'records' => $this->collect($filtered, 'transform')
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $filtered->first()->transaction_date->toDateTimeString(),
                    'operator' => '>=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                ),
                array(
                    'value' => $filtered->last()->transaction_date->toDateTimeString(),
                    'operator' => '<=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_flow_type_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create([
            'flow_type' => 1
        ]);

        $this->collection(3);

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => [$this->item($data, 'transform')]
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 1,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'flow_type'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_using_a_filter_not_in_searchable_fields_will_be_ignored_and_return_the_data()
    {
        $collection = $this->collection();

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => $this->collect($collection, 'transform')
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 99,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'amount'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }

     /**
     * @test
     */
    public function searching_for_a_data_that_doesnt_meet_the_filter_conditions_will_return_an_empty_collection()
    {
        $collection = $this->collection();

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => []
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 1,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'flow_type'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, Lang::get('core::info.no.results.found')));
    }

    /**
     * Build a collection of transaction
     * 
     * @param  int  $time  
     * @return array        
     */
    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function create($data = [])
    {
        return Factory::create($this->factory, $data);
    }

    protected function transform($model)
    {
        return [
            'date'        => $model->transaction_date->toDateTimeString(),
            'particular'  => $model->presenter()->particulars,
            'in'          => $model->presenter()->in,
            'out'         => $model->presenter()->out,
        ];
    }

    public abstract function visiting_the_balance_flow_page_while_user_is_authenticated_will_successfully_display_the_page_with_its_reference();
}