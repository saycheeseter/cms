<?php 

class SupplierBalanceFlowControllerTest extends BalanceFlowControllerTest
{
    protected $uri = 'reports/supplier/balance';
    protected $factory = 'supplier_balance_flow';

     /**
     * @test
     */
    public function visiting_the_balance_flow_page_while_user_is_authenticated_will_successfully_display_the_page_with_its_reference()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('suppliers');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_supplier_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->collection(3);

        $expected = [
            'beginning' => [
                'amount' => number_format(0, setting('monetary.precision'))
            ],
            'records' => [$this->item($data, 'transform')]
        ];

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->supplier->full_name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'supplier.full_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse($expected, ''));
    }
}