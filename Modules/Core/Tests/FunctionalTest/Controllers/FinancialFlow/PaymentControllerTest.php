<?php

use Laracasts\TestDummy\Factory;

use Illuminate\Support\Collection;

use Modules\Core\Enums\ApprovalStatus;

class PaymentControllerTest extends FinancialFlowControllerTest
{
    protected $supplier;

    protected $referenceAmountColumn = 'payable';
    protected $uri = 'payment';
    protected $prefix = 'PA';

    protected $factories = [
        'info' => 'payment',
        'detail' => 'payment_detail',
        'reference' => 'payment_reference'
    ];

    protected $transactionable_type = 'payment_detail';

    protected $tables = [
        'info' => 'payment',
        'detail' => 'payment_detail',
        'reference' => 'payment_reference',
        'check' => 'issued_check',
        'reference_table' => 'purchase_inbound',
        'reference_table_detail' => 'purchase_inbound_detail',
        'account_balance_table' => 'supplier_balance_flow'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_created_for_or_supplier_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['supplier_id'] = '';
        $data['info']['created_for'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_the_data_with_a_created_for_or_supplier_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'created_for' => 99
        ]);

        $data = $this->toArray($build);

        $data['info']['supplier_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_created_for_or_supplier_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = '';
        $data['info']['created_for'] = '';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_created_for_or_supplier_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = 99;
        $data['info']['created_for'] = 99;

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_correct_transaction_format_will_successfull_saved_the_data_in_database_and_return_the_details_of_the_transaction()
    {
        $create = $this->create();

        $detail = $this->createSingle($this->factories['detail'], [
            'transaction_id' => $create['info']->id
        ]);

        $reference = $this->createSingle($this->factories['reference'], [
            'transaction_id' => $create['info']->id,
            'purchase_inbound_id' => $create['reference']->purchase_inbound_id
        ]);

        $detailPrevious = $create['detail'];
        $referencePrevious = $create['reference'];

        $create['detail'] = $detail;
        $create['reference'] = $reference;

        $data = $this->toArray($create);

        $data['references'][0]['inventory_chain_id'] = $data['references'][0]['purchase_inbound_id'];

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'created_at',
                    'updated_at'
                )), array(
                    'id' => 1
                )),
                'details' => array(
                    array_merge(
                        array_except($data['details'][0], array(
                            'transaction',
                            'created_at',
                            'updated_at'
                        )), array(
                            'transaction_id' => 1
                        )
                    )
                ),
                'references' => array(
                    array_merge(
                        array_except($data['references'][0], array(
                            'inventory_chain_id',
                            'transaction',
                            'created_at',
                            'updated_at'
                        )),
                        array(
                            'transaction_id' => 1
                        )
                    )
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0])
            ->seeInDatabase($this->tables['reference'], $expected['database']['references'][0])
            ->notSeeInDatabase($this->tables['detail'], array(
                    'id' => $detailPrevious->id,
                    'deleted_at' => null
                )
            )
            ->notSeeInDatabase($this->tables['reference'], array(
                    'id' => $referencePrevious->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function approving_a_transaction_will_decrement_the_remaining_amount_in_reference_transaction()
    {
        //sum factory generated reference table detail amount
        $remaining_amount = $this->referenceTableDetail->sum(function ($details) {
                return $details['amount']->innerValue();
            });

        //update remainining amount in head
        DB::table($this->tables['reference_table'])
            ->where('id', $this->referenceTable->id)
            ->update(array(
                'remaining_amount' => $remaining_amount
            ));

        //create transaction with same amount
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ), array(), array(
            'purchase_inbound_id' => $this->referenceTable->id,
            $this->referenceAmountColumn => $remaining_amount
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = array(
            'id' => $this->referenceTable->id,
            'remaining_amount' => 0
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ))
            ->seeInDatabase($this->tables['reference_table'], $expected);
    }

    protected function references()
    {
        parent::references();

        $this->supplier = Factory::create('supplier');
    }

    /**
     * Build non-persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array(), $reference = array())
    {
        $info = Factory::build($this->factories['info'], array_merge($info, [
            'supplier_id' => $this->supplier->id,
        ]));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
        ), $detail));

        $reference = Factory::build($this->factories['reference'], array_merge(array(
            'transaction_id' => $info->id,
            'purchase_inbound_id' => $this->referenceTable->id
        ), $reference));

        return array(
            'info' => $info,
            'detail' => $detail,
            'reference' => $reference
        );
    }

    /**
     * Convert transaction to Array
     * 
     * @param  array $transaction
     * @return array
     */
    protected function toArray($transaction)
    {
        $details = [];
        $references = [];

        $detailCollection = ! $transaction['detail'] instanceof Collection 
            ? collect([])->push($transaction['detail']) 
            : $transaction['detail'];

        $referenceCollection = ! $transaction['reference'] instanceof Collection 
            ? collect([])->push($transaction['reference']) 
            : $transaction['reference'];

        foreach ($detailCollection as $key => $model) {
            $details[$key] = array_merge($model->toArray(), [
                'id'              => $model->id ?? 0,
                'amount'          => $model->amount->innerValue(),
                'bank_account_id' => $model->bank_account_id ?? null,
                'check_no'        => $model->check_no ?? null,
                'check_date'      => $model->check_date ?? null,
            ]);
        }

        foreach ($referenceCollection as $key => $model) {
            $references[$key] = array_merge($model->toArray(), [
                'id' => $model->id ?? 0,
                'payable' => $model->payable->innerValue(),
                'inventory_chain_id' => $this->referenceTableDetail[0]->id
            ]);
        }

        return array(
            'info' => array_merge($transaction['info']->toArray(), [
                'total_amount' => $transaction['info']->total_amount->innerValue(),
                'transaction_date' => $transaction['info']->transaction_date->toDateTimeString(),
            ]),
            'details' => $details,
            'references' => $references
        );
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'location'              => $data->for->name,
            'date'                  => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'supplier'              => $data->supplier->full_name,
            'amount'                => $data->number()->total_amount,
            'remarks'               => $data->remarks,
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'deleted'               => $data->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'sheet_number'          => $data->sheet_number,
            'created_for'           => $data->created_for,
            'requested_by'          => $data->requested_by,
            'supplier_id'           => $data->supplier_id,
            'entity_id'             => $data->supplier_id,
            'remarks'               => $data->remarks,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'approval_status'       => $data->approval_status,
            'approval_status_label' => $data->presenter()->approval,
        ];
    }

    protected function transactionReferencesFullInfoTransformer($data)
    {
        return [
            'id'                 => $data->id,
            'inventory_chain_id' => $data->inventoryChainReference->id,
            'transaction_date'   => $data->inventoryChainReference->transaction_date->toDateTimeString(),
            'sheet_number'       => $data->inventoryChainReference->sheet_number,
            'term'               => $data->inventoryChainReference->term,
            'dr_no'              => $data->inventoryChainReference->dr_no,
            'remarks'            => $data->inventoryChainReference->remarks,
            'amount'             => $data->inventoryChainReference->number()->total_amount,
            'paid_amount'        => $data->inventoryChainReference->number()->paid_amount,
            'payable'            => $data->number()->payable,
            'remaining'          => $data->inventoryChainReference->number()->remaining_amount,
        ];
    }
}