<?php

use Laracasts\TestDummy\Factory;

use Illuminate\Support\Collection;
use Modules\Core\Enums\Voided;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Enums\FlowType;
use Carbon\Carbon;

abstract class FinancialFlowControllerTest extends FunctionalTest
{
    protected $referenceTable;
    protected $referenceTableDetail;
    protected $bankAccount;
    protected $referenceAmountColumn;
    protected $uri;
    protected $prefix;
    protected $factories;
    protected $tables;

    /**
     * @test
     */
    public function visiting_the_transaction_page_while_not_logged_in_will_redirect_the_user_to_login_page()
    {
        $this->logout()->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_the_transaction_list_page()
    {
        $this->visit($this->uri)->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_sheet_number_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');
        
        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->sheet_number,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->transaction_date->toDateTimeString(),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->remarks,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'remarks'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_approval_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => ApprovalStatus::DRAFT,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'approval_status'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect($data, 'transactionBasicTransformer')
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_deleted_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $data[1]->delete();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => Voided::YES,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => $this->uri.'.deleted_at'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_unddeleted_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $data[0]->delete();

        $data->shift();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => Voided::NO,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => $this->uri.'.deleted_at'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 3,
                    'count' => 3,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect($data, 'transactionBasicTransformer')
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_using_a_filter_not_in_searchable_fields_will_be_ignored_and_return_the_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->total_amount,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'total_amount'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collect($data, 'transactionBasicTransformer')
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_that_doesnt_meet_the_filter_conditions_will_return_an_empty_collection()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 100),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array()
        ), Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_amount_or_payment_method_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['amount'] = '';
        $data['details'][0]['payment_method'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.amount' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.required')
                    )
                ),
                'details.0.payment_method' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.payment.method.required')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_amount_not_numeric_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['amount'] = 'a';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.amount' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.numeric')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function saving_a_payment_method_not_existing_in_database_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['payment_method'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.payment_method' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.payment.method.doesnt.exists')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_bank_account_or_check_no_with_payment_method_check_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['payment_method'] = 3;
        $data['details'][0]['bank_account_id'] = '';
        $data['details'][0]['check_no'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.bank_account_id.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.bank.account.required')
                    ),
                'details.0.check_no.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.check.no.required')
                    ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_bank_account_with_payment_method_bank_deposit_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['payment_method'] = 4;
        $data['details'][0]['bank_account_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.bank_account_id.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.bank.account.required')
                    ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_details_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = 1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_details_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_amount_or_inventory_chain_id_in_transaction_reference_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['references'][0]['inventory_chain_id'] = '';
        $data['references'][0][$this->referenceAmountColumn] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.inventory_chain_id' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.reference.required')
                    )
                ),
                'references.0.'.$this->referenceAmountColumn => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.required')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_inventory_chain_id_that_doesnt_exists_in_the_database_in_transaction_reference_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['references'][0]['inventory_chain_id'] = 99;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.inventory_chain_id' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.reference.doesnt.exists')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function saving_a_amount_that_isnt_numeric_in_transaction_reference_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['references'][0][$this->referenceAmountColumn] = 'a';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.'.$this->referenceAmountColumn => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.numeric')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_references_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['references'] = 1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_references_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['references'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references' => array(
                    Lang::get('core::validation.references.min.one')
                ),
            )));
    }


    /**
     * @test
     */
    public function saving_a_correct_transaction_format_will_successfull_saved_the_data_in_database_generate_a_transaction_number_and_return_the_details_of_the_transaction()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'customer'
                )), array(
                    'id' => 1
                )),
                'details' => array(
                    array_merge(
                        $data['details'][0],
                        array(
                            'id' => 1,
                            'transaction_id' => 1
                        )
                    )
                ),
                'references' => array(
                    array_merge(
                        array_except($data['references'][0], array(
                            'inventory_chain_id',
                        )),
                        array(
                            'id' => 1,
                            'transaction_id' => 1
                        )
                    )
                )
            ),
            'response' => array_merge(
                $this->item($build['info'], 'transactionFullInfoTransformer'), 
                array(
                    'id' => 1,
                    'sheet_number' => $this->generateReferenceNumber(1, $data['info']['created_from'])
                )
            )
        );
        
        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'record' => $expected['response']
            ], Lang::get('core::success.created')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0])
            ->seeInDatabase($this->tables['reference'], $expected['database']['references'][0]);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_saving_the_transaction_details()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'id' => 1,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['amount'];
            }),
        );

        $this->postJson($this->uri, $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_data_in_the_edit_api_will_return_an_error_message()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_through_the_edit_api_will_display_the_full_details_of_the_transaction()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'details' => ['data' => $this->collect([$data['detail']], 'transactionDetailFullInfoTransformer')],
                'references' => ['data' => $this->collect([$data['reference']], 'transactionReferencesFullInfoTransformer')],
            ]));
    }

    /**
     * @test
     */
    public function updating_an_empty_amount_or_payment_method_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['amount'] = '';
        $data['details'][0]['payment_method'] = '';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.amount' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.required')
                    )
                ),
                'details.0.payment_method' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.payment.method.required')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_amount_not_numeric_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['amount'] = 'a';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.amount' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.numeric')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function updating_a_payment_method_not_existing_in_database_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['payment_method'] = 99;

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.payment_method' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.payment.method.doesnt.exists')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_incorrect_format_of_details_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'] = 1;

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_empty_details_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'] = array();

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_empty_bank_account_or_check_no_with_payment_method_check_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['payment_method'] = 3;
        $data['details'][0]['bank_account_id'] = '';
        $data['details'][0]['check_no'] = '';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.bank_account_id.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.bank.account.required')
                    ),
                'details.0.check_no.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.check.no.required')
                    ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_empty_bank_account_with_payment_method_bank_deposit_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['payment_method'] = 4;
        $data['details'][0]['bank_account_id'] = '';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.bank_account_id.required' => 
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.bank.account.required')
                    ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_empty_amount_or_inventory_chain_id_in_transaction_reference_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['references'][0]['inventory_chain_id'] = '';
        $data['references'][0][$this->referenceAmountColumn] = '';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.inventory_chain_id' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.reference.required')
                    )
                ),
                'references.0.'.$this->referenceAmountColumn => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.required')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_inventory_chain_id_that_doesnt_exists_in_the_database_in_transaction_reference_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['references'][0]['inventory_chain_id'] = 99;

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.inventory_chain_id' => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.reference.doesnt.exists')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function updating_a_amount_that_isnt_numeric_in_transaction_reference_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['references'][0][$this->referenceAmountColumn] = 'a';

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references.0.'.$this->referenceAmountColumn => array(
                    sprintf('%s[%u] %s', 
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.amount.numeric')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_incorrect_format_of_references_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['references'] = 1;

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_an_empty_references_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['references'] = array();

        $this->patchJSON(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'references' => array(
                    Lang::get('core::validation.references.min.one')
                ),
            )));
    }

    /**
     * @test
     */
    public function removing_a_transaction_detail_will_successfully_soft_delete_the_data_in_database()
    {
        $create = $this->create();

        $detail = $this->createSingle($this->factories['detail'], [
            'transaction_id' => $create['info']->id
        ]);

        $data = $this->toArray($create);

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->notSeeInDatabase($this->tables['detail'], array(
                    'id' => $detail->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function removing_a_transaction_reference_will_successfully_soft_delete_the_data_in_database()
    {
        $create = $this->create();

        $reference = $this->createSingle($this->factories['reference'], [
            'transaction_id' => $create['info']->id
        ]);

        $data = $this->toArray($create);

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->notSeeInDatabase($this->tables['reference'], array(
                    'id' => $reference->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_updating_the_transaction_details()
    {
        $create = $this->create();

        $detail = $this->buildSingle($this->factories['detail'], [
            'transaction_id' => $create['info']->id
        ]);

        $create['detail'] = collect([$create['detail'], $detail]);

        $data = $this->toArray($create);

        $expected = array(
            'id' => $create['info']->id,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['amount'];
            }),
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_recomputed_upon_removing_a_transaction_detail()
    {
        $create = $this->create();

        $detail = $this->buildSingle($this->factories['detail'], [
            'transaction_id' => $create['info']->id
        ]);

        $original = $create['detail'];
        $create['detail'] = collect([$create['detail'], $detail]);

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $this->toArray($create));

        $create['detail'] = $original;

        $data = $this->toArray($create);

        $expected = array(
            'id' => $create['info']->id,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['amount'];
            }),
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

    /**
     * @test
     */
    public function transaction_cannot_be_updated_if_its_already_deleted()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        DB::table($this->tables['info'])
            ->where('id', $create['info']->id)
            ->update(array(
                'deleted_at' => Carbon::now()
            ));

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted'),
            )));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_updated_if_approval_status_is_not_draft()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        DB::table($this->tables['info'])
            ->where('id', $create['info']->id)
            ->update(array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_change_if_transaction_is_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.deleted" => Lang::get(
                    'core::validation.transaction.number.deleted',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_for_approval_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.for.approval',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_approved_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.approve',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_declined_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.decline',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_for_approval_if_current_status_is_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_approved_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_declined_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DECLINED
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_approved()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_approved()
    {
        $created = $this->create();

        $created['info']->approval_status = ApprovalStatus::APPROVED;
        
        $created['info']->save();

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DECLINED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_automatically_be_generated_if_transaction_is_approved_or_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => Auth::user()->id
            ))
            ->notSeeInDatabase($this->tables['info'], array(
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_be_set_to_null_if_transaction_is_reverted()
    {
        $created = $this->create();

        $created['info']->approval_status = ApprovalStatus::APPROVED;
        
        $created['info']->save();

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => null,
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_its_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            )));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            )));
    }

    /**
     * @test
     */
    public function transaction_will_be_successfully_soft_deleted_if_current_status_is_draft_and_not_yet_deleted()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.deleted')))
            ->notSeeInDatabase($this->tables['info'], array(
                'deleted_at' => null
            ));
    }

    /**
     * @test
     */
    public function approving_a_transaction_with_check_in_detail_will_insert_in_check_table()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ), array(
            'payment_method' => PaymentMethod::CHECK,
            'bank_account_id' => $this->bankAccount->id,
            'check_no' => $this->faker->bankAccountNumber,
            'check_date' => Carbon::now()->toDateString(),
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = array(
            'transactionable_id' => $created['detail']->id,
            'amount' => $created['detail']->amount,
            'transactionable_type' => $this->transactionable_type
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ))
            ->seeInDatabase($this->tables['check'], $expected);
    }

    /**
     * @test 
     */
    public function approving_a_transaction_with_excess_amount_will_insert_in_account_balance_table_with_flow_type_in()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ), array(
            'payment_method' => PaymentMethod::CASH,
            'amount' => 1500
        ), array(
            $this->referenceAmountColumn => 1000
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = array(
            'transactionable_id' => $created['info']->id,
            'transactionable_type' => $this->tables['info'],
            'flow_type' => FlowType::IN,
            'amount' => 500
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['account_balance_table'], $expected);
    }

    /**
     * @test
     */
    public function approving_a_transaction_with_payment_from_balance_will_insert_in_account_balance_table_with_flow_type_out()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ), array(
            'payment_method' => PaymentMethod::FROM_BALANCE
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = array(
            'transactionable_id' => $created['info']->id,
            'transactionable_type' => $this->tables['info'],
            'flow_type' => FlowType::OUT,
            'amount' => $created['detail']->amount
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['account_balance_table'], $expected);
    }

    /**
     * @test
     */
    public function reverting_a_transaction_with_payment_type_check_will_delete_check_in_check_table()
    {

        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ), array(
            'payment_method' => PaymentMethod::CHECK,
            'bank_account_id' => $this->bankAccount->id,
            'check_no' => $this->faker->bankAccountNumber,
            'check_date' => Carbon::now()->toDateString(),
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $revertData = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $expected = array(
            'transactionable_id' => $created['detail']->id,
            'amount' => $created['detail']->amount,
            'transactionable_type' => $this->transactionable_type,
            'deleted_at' => null
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->postJson(sprintf('%s/approval', $this->uri), $revertData)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->notSeeInDatabase($this->tables['check'], $expected);
    }

    /**
     * Build persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function create($info = array(), $detail = array(), $reference = array())
    {
        $info = Factory::create($this->factories['info'], $info);

        $detail = Factory::create($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'bank_account_id' => null,
            'check_no' => null,
            'check_date' => null,
        ), $detail));

        $reference = Factory::create($this->factories['reference'], array_merge(array(
            'transaction_id' => $info->id,
        ), $reference));

        return array(
            'info' => $info->fresh(),
            'detail' => $detail,
            'reference' => $reference
        );
    }

    protected function references()
    {
        $this->referenceTable = Factory::create($this->tables['reference_table']);

        $this->referenceTableDetail = Factory::times(2)->create($this->tables['reference_table_detail'], [
            'transaction_id' => $this->referenceTable->id
        ]);

        $this->bankAccount = Factory::create('bank_account');
    }

    /**
     * Build a collection of transaction
     * 
     * @param  int  $time  
     * @return array        
     */
    protected function transactions($times = 4)
    {
        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function createSingle($table, $data)
    {
        return Factory::create($table, $data);
    }

    protected function buildSingle($table, $data)
    {
        return Factory::build($table, $data);
    }

    protected function transactionDetailFullInfoTransformer($data)
    {
        return [
            'id'              => $data->id,
            'payment_method'  => $data->payment_method,
            'bank_account_id' => $data->bank_account_id,
            'check_no'        => $data->check_no,
            'check_date'      => $data->check_date,
            'amount'          => $data->number()->amount
        ];
    }

    protected function generateReferenceNumber($id, $branch)
    {
        return sprintf('%s%s%s', 
            $this->prefix, 
            str_pad($branch, 3, '0', STR_PAD_LEFT),
            str_pad($id, 7, '0', STR_PAD_LEFT)
        );
    }

    abstract protected function build($info = array(), $detail = array(), $reference = array());
    abstract protected function toArray($transaction);
    abstract protected function transactionBasicTransformer($data);
    abstract protected function transactionFullInfoTransformer($data);
    abstract protected function transactionReferencesFullInfoTransformer($data);
}