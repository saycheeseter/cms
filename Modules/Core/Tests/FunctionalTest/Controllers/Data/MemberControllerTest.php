<?php

use Laracasts\TestDummy\Factory;

class MemberControllerTest extends FunctionalTest
{
    private $uri = '/member';
    private $factory = 'member';
    private $rates;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function accessing_member_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_the_member_page_will_successfully_display_the_page_and_return_the_rate_references()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('rates');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_full_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->full_name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'full_name' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'members' => $this->collect([$data[1]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_card_id_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->card_id, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'card_id' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'members' => $this->collect([$data[2]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_barcode_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->barcode, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'barcode' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'members' => $this->collect([$data[0]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->id, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'members' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'full_name' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'members' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_rate_full_name_card_id_barcode_status_registration_date_or_expiration_date_will_trigger_an_error_response()
    {
        $build = $this->build([
            'full_name' => '',
            'card_id' => '',
            'barcode' => '',
        ]);

        $data = $this->toArray($build);

        $data['registration_date'] = '';
        $data['expiration_date'] = '';
        $data['status'] = '';
        $data['rate_head_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'rate_head_id' => array(
                    Lang::get('core::validation.rate.required')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'card_id' => array(
                    Lang::get('core::validation.card.id.required')
                ),
                'barcode' => array(
                    Lang::get('core::validation.barcode.required')
                ),
                'status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'registration_date' => array(
                    Lang::get('core::validation.registration.date.required')
                ),
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_full_name_card_id_or_barcode_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $build = $this->build([
            'full_name' => $collection[0]->full_name,
            'card_id' => $collection[0]->card_id,
            'barcode' => $collection[0]->barcode,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                ),
                'card_id' => array(
                    Lang::get('core::validation.card.id.unique')
                ),
                'barcode' => array(
                    Lang::get('core::validation.barcode.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_barcode_status_or_email_format_will_trigger_an_error_response()
    {
        $build = $this->build([
            'barcode' => $this->faker->name,
            'email' => $this->faker->name,
        ]);

        $data = $this->toArray($build);

        $data['status'] = $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'barcode' => array(
                    Lang::get('core::validation.barcode.numeric')
                ),
                'status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
                'email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function using_a_non_existing_rate_will_trigger_an_error_response()
    {
        $build = $this->build([
            'rate_head_id' => $this->faker->numberBetween(10, 100)
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'rate_head_id' => array(
                    Lang::get('core::validation.rate.deleted')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_date_of_registration_date_or_expiration_date_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['registration_date'] = $this->faker->name;
        $data['expiration_date'] = $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'registration_date' => array(
                    Lang::get('core::validation.registration.date.format')
                ),
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.format')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_expiration_date_earlier_that_registration_date_will_trigger_an_error()
    {
        $build = $this->build([
            'registration_date' => '2017-02-16',
            'expiration_date' => '2017-02-15'
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.invalid.range')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'member' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    array('id' => 1)
                )
            ),
            'database' => array_merge(
                $this->item($build, 'fullInfoTransformer'),
                array('id' => 1)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('member', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_rate_full_name_card_id_barcode_status_registration_date_or_expiration_date_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['rate_head_id'] = '';
        $data['full_name'] = '';
        $data['card_id'] = '';
        $data['barcode'] = '';
        $data['status'] = '';
        $data['registration_date'] = '';
        $data['expiration_date'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'rate_head_id' => array(
                    Lang::get('core::validation.rate.required')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'card_id' => array(
                    Lang::get('core::validation.card.id.required')
                ),
                'barcode' => array(
                    Lang::get('core::validation.barcode.required')
                ),
                'status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'registration_date' => array(
                    Lang::get('core::validation.registration.date.required')
                ),
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_an_existing_full_name_card_id_or_barcode_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $created = $this->create();

        $build = $this->build([
            'full_name' => $collection[0]->full_name,
            'card_id' => $collection[0]->card_id,
            'barcode' => $collection[0]->barcode
        ]);

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                ),
                'card_id' => array(
                    Lang::get('core::validation.card.id.unique')
                ),
                'barcode' => array(
                    Lang::get('core::validation.barcode.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_an_incorrect_barcode_status_or_email_format_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build([
            'barcode' => $this->faker->name,
            'email' => $this->faker->name
        ]);

        $data = $this->toArray($build);

        $data['status'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcode' => array(
                    Lang::get('core::validation.barcode.numeric')
                ),
                'status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
                'email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_an_incorrect_format_date_of_registration_date_or_expiration_date_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['registration_date'] = $this->faker->name;
        $data['expiration_date'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'registration_date' => array(
                    Lang::get('core::validation.registration.date.format')
                ),
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.format')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_an_expiration_date_earlier_that_registration_date_will_trigger_an_error()
    {
        $created = $this->create();

        $build = $this->build([
            'registration_date' => '2017-02-16',
            'expiration_date' => '2017-02-15'
        ]);

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'expiration_date' => array(
                    Lang::get('core::validation.expiration.date.invalid.range')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/5', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('member', [
                'id' => 5
            ]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {   
        $created = $this->create();

        $expected = array(
            'id' => $created->id,
            'deleted_at' => null
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('member', $expected);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('member', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_show_api_will_return_the_full_info_of_the_corresponding_member()
    {
        $created = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([
                'member' => $this->item($created, 'fullInfoTransformer')
            ]));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_member_will_return_an_error()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed') 
            ]));
    }

    protected function create($attributes = array())
    {
        return Factory::create($this->factory, array_merge(
            array('rate_head_id' => $this->rates[rand(0, 1)]->id),
            $attributes
        ));
    }

    protected function build($attributes = array())
    {
        return Factory::build($this->factory, array_merge(
            array('rate_head_id' => $this->rates[rand(0, 1)]->id),
            $attributes
        ));
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'rate_head_id' => $this->rates[rand(0, 1)]->id
        ));
    }

    protected function references()
    {
        $this->rates = Factory::times(2)->create('member_rate');
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'full_name'     => $model->full_name,
            'member_type'   => $model->rate->name,
            'card_id'       => $model->card_id,
            'barcode'       => $model->barcode,
            'mobile'        => $model->mobile,
            'telephone'     => $model->telephone
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'rate_head_id'          => $model->rate_head_id,
            'card_id'               => $model->card_id,
            'barcode'               => $model->barcode,
            'full_name'             => $model->full_name,
            'mobile'                => $model->mobile,
            'telephone'             => $model->telephone,
            'address'               => $model->address,
            'email'                 => $model->email,
            'gender'                => $model->gender,
            'birth_date'            => $model->birth_date->toDateString(),
            'registration_date'     => $model->registration_date->toDateString(),
            'expiration_date'       => $model->expiration_date->toDateString(),
            'status'                => $model->status,
            'memo'                  => $model->memo
        ];
    }

    protected function toArray($model)
    {
        return array_except(array_merge($model->toArray(), [
            'birth_date' => $model->birth_date->toDateString(),
            'registration_date' => $model->registration_date->toDateString(),
            'expiration_date' => $model->expiration_date->toDateString(),
        ]), [
            'created_from',
            'created_by'
        ]);
    }
}
