<?php

use Laracasts\TestDummy\Factory;

class CategoryControllerTest extends FunctionalTest
{
    private $uri = '/category';
    private $factory = 'category';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_the_category_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_the_category_page_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->code, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'code' 
                )
            ),
            'parent_id' => null
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => $this->collect([$data[1]], 'treeTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            ),
            'parent_id' => null
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => $this->collect([$data[0]], 'treeTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_parent_id_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->children->first()->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            ),
            'parent_id' => $data[1]->id
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => $this->collect($data[1]->children, 'treeTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_that_doesnt_meet_the_proper_condition_on_a_parent_category_will_return_an_empty_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            ),
            'parent_id' => $data[0]->id
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => array()
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1', 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            ),
            'parent_id' => null
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => $this->collect($data, 'treeTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'code' 
                )
            ),
            'parent_id' => null
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => array()
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function searching_for_a_child_of_a_category_which_doesnt_have_a_children_will_return_an_empty_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'parent_id' => $data[0]
                ->children
                ->first()
                ->children
                ->first()
                ->id
        ))->seeJsonEquals($this->successfulResponse([
            'categories' => array()
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'code' => '',
            'name' => ''
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'name' => array(
                    Lang::get('core::validation.name.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'code' => $created->code,
            'name' => $created->name,
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function using_a_non_existing_parent_category_will_trigger_an_error()
    {
        $build = $this->build(array(
            'parent_id' => $this->faker->numberBetween(1, 100)
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'parent_id' => array(
                    Lang::get('core::validation.parent.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_category_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->item($build, 'basicTransformer');

        $expected = array(
            'response' => array(
                'category' => array_merge(
                    $data,
                    array('id' => 1)
                )
            ),
            'database' => array_merge(
                $data,
                array('id' => 1)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created'
            )))
            ->seeInDatabase('category', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'code' => '',
            'name' => ''
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'name' => array(
                    Lang::get('core::validation.name.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $build = $this->build(array(
            'code' => $collection[1]->code,
            'name' => $collection[1]->name
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->patchJson(sprintf('%s/%s', $this->uri, $collection[0]->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function upon_updating_using_a_non_existing_parent_category_will_trigger_an_error()
    {
        $created = $this->create();

        $build = $this->build(array(
            'parent_id' => '10'
        ));

        $data = $this->item($build, 'basicTransformer');

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'parent_id' => array(
                    Lang::get('core::validation.parent.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'basicTransformer');

        $this->patchJson(sprintf('%s/1', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('category', array('id' => 1));
    }

    /**
     * @test
     */
    public function deleting_a_category_with_children_will_return_an_error_response()
    {
        $collection = $this->collection();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $collection[0]->id))
            ->seeJsonEquals($this->errorResponse([
                'category.cant.delete' => Lang::get('core::validation.parent.cannot.delete')
            ]))
            ->seeInDatabase('category', array(
                    'id' => $collection[0]->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {   
        $collection = $this->collection();

        $this->deleteJson(sprintf('%s/%s',
                $this->uri,
                $collection[1]
                    ->children
                    ->first()
                    ->children
                    ->first()
                    ->id
            ))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('category', array(
                    'id' => $collection[1]
                        ->children
                        ->first()
                        ->children
                        ->first()
                        ->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/%s', $this->uri, $this->faker->numberBetween(1, 100)))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('category', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function collection($times = 2, $deep = 2)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) { 

            $category = Factory::create($this->factory);

            $current = $category;

            for ($x=0; $x < $deep; $x++) { 
                $current = Factory::create($this->factory, array(
                    'parent_id' => $current->id
                ));
            }

            $data[] = $category;
        }

        return $data;
    }

    protected function treeTransformer($model)
    {
        $transformable = [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id,
            'children'      => array()
        ];

        foreach ($model->children as $key => $child) {
            $transformable['children'][] = $this->treeTransformer($child);
        }

        return $transformable;
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'parent_id'     => $model->parent_id
        ];
    }
}
