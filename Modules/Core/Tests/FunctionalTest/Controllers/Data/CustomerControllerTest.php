<?php 

use Laracasts\TestDummy\Factory;

class CustomerControllerTest extends FunctionalTest
{
    private $uri = '/customer';
    
    private $factories = array(
        'info' => 'customer',
        'detail' => 'customer_detail'
    );

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_customer_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_customer_rate_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('salesmen');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->code, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'code' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'customers' => $this->collect([$data[1]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'customers' => $this->collect([$data[2]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->numberBetween(1, 100), 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'customers' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_collection()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'customers' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_name_pricing_type_or_salesman_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'name' => '', 
            'code' => '',
        ), array(
            'name' => ''
        ));

        $data = $this->toArray($build);

        $data['info']['pricing_type'] = '';
        $data['info']['salesman_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.required')
                ),
                'info.code' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.required')
                ),
                'info.pricing_type' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.type.required')
                ),
                'info.salesman_id' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.salesman.required')
                ),
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'name' => $created['info']->name, 
            'code' => $created['info']->code,
        ), array(
            'name' => $created['detail'][0]->name
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.unique')
                ),
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_credit_limit_pricing_rate_or_term_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['credit_limit'] = $this->faker->name;
        $data['info']['pricing_rate'] = $this->faker->name;
        $data['info']['term'] = $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.credit_limit' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.credit.limit.numeric')
                ),
                'info.pricing_rate' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.rate.numeric')
                ),
                'info.term' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.term.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::validation.details.min.one')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_duplicate_name_in_details_array_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][1] = $data['details'][0];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.array.unique')
                ),
                'details.1.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.name.array.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_data_and_selecting_a_non_existing_salesman_will_trigger_an_error()
    {
        $build = $this->build(array(
            'salesman_id' => $this->faker->numberBetween(2, 100)
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.salesman_id' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.salesman.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_email_in_customer_details_will_trigger_an_error()
    {
        $build = $this->build([], [
            'email' => $this->faker->name
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.email' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.email.invalid')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = [
            'response' => array(
                'customer' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array('id' => 1)
                )
            ),
            'database' => [
                'info' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array('id' => 1)
                ),
                'details' => array_merge(
                    $this->item($build['detail']->first(), 'detailTransformer'),
                    array(
                        'id' => 1,
                        'head_id' => 1
                    )
                )
            ]
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created'
            )))
            ->seeInDatabase('customer', $expected['database']['info'])
            ->seeInDatabase('customer_detail', $expected['database']['details']);
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_empty_code_name_pricing_type_or_salesman_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['name'] = '';
        $data['info']['code'] = '';
        $data['info']['pricing_type'] = '';
        $data['info']['salesman_id'] = '';
        $data['details'][0]['name'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.required')
                ),
                'info.code' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.required')
                ),
                'info.pricing_type' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.type.required')
                ),
                'info.salesman_id' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.salesman.required')
                ),
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['name'] = $collection[1]['info']->name;
        $data['info']['code'] = $collection[1]['info']->code;
        $data['details'][0]['name'] = $collection[1]['detail'][0]->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::label.customer.detail').' : '.Lang::get('core::validation.name.unique')
                ),
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_incorrect_format_of_credit_limit_pricing_rate_and_term_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['credit_limit'] = $this->faker->name;
        $data['info']['pricing_rate'] = $this->faker->name;
        $data['info']['term'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.credit_limit' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.credit.limit.numeric')
                ),
                'info.pricing_rate' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.pricing.rate.numeric')
                ),
                'info.term' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.term.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_existing_name_in_details_array_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][0] = $data['details'][1];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.name.array.unique')
                ),
                'details.1.name' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.name.array.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_and_selecting_a_non_existing_salesman_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['salesman_id'] = $this->faker->numberBetween(2, 100);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.salesman_id' => array(
                    Lang::get('core::label.advance.info').' : '.Lang::get('core::validation.salesman.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_an_invalid_email_in_branch_details_will_trigger_an_error()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][0]['email'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.email' => array(
                    Lang::get('core::label.customer.branches').' : '.Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.email.invalid')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_with_a_proper_format_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $expected = [
            'response' => array(
                'customer' => $this->item($created['info'], 'basicTransformer')
            ),
            'database' => [
                'info' => $this->item($created['info'], 'fullInfoTransformer'),
                'details' => array(
                    $data['details'][0],
                    $data['details'][1],
                )
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated'
            )))
            ->seeInDatabase('customer', $expected['database']['info'])
            ->seeInDatabase('customer_detail', $expected['database']['details'][0])
            ->seeInDatabase('customer_detail', $expected['database']['details'][1]);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/1', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('customer', [
                'id' => 1
            ]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_and_its_relationship_in_database_and_return_a_successful_response()
    {   
        $created = $this->create();

        $expected = array(
            'info' => array(
                'id' => $created['info']->id,
                'deleted_at' => null
            ),
            'detail' => array(
                array(
                    'id' => $created['detail'][0]->id,
                    'deleted_at' => null
                ),
                array(
                    'id' => $created['detail'][1]->id,
                    'deleted_at' => null
                )
            )
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('customer', $expected['info'])
            ->notSeeInDatabase('customer_detail', $expected['detail'][0])
            ->notSeeInDatabase('customer_detail', $expected['detail'][1]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('customer', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_the_info_api_will_return_the_full_info_of_the_customer()
    {
        $created = $this->create();

        $expected = array(
            'customer' => $this->item($created['info'], 'fullInfoTransformer'),
            'details' => array(
                'collection' => $this->collect($created['detail'], 'detailTransformer'),
                'meta' => array(
                    'pagination' => array(
                        'total' => count($created['detail']),
                        'count' => count($created['detail']),
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    )
                )
            )
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_customer_will_return_an_error()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed') 
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_details_of_a_non_existing_customer_will_return_an_error()
    {
        $this->getJson(sprintf('%s/1/details', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed') 
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_details_api_will_return_the_corresponding_customer_branch_of_the_specified_customer()
    {
        $created = $this->create();

        $this->getJson(sprintf('%s/%s/details', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'details' => $this->collect($created['detail'], 'detailChosenTransformer') 
            ]));
    }

    /**
     * Build non-persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], $info);

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'head_id' => $info->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => collect([$detail])
        );
    }

    /**
     * Build persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function create($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['info'], $info);

        $detail = Factory::times(2)->create($this->factories['detail'], array_merge(array(
            'head_id' => $info->id,
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => $detail
        );
    }

    /**
     * Build a collection of customer
     * 
     * @param  int  $time  
     * @return array        
     */
    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function basicTransformer($model)
    {
        return [
            'id'         => $model->id,
            'code'       => $model->code,
            'name'       => $model->name
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'                        => $model->id,
            'name'                      => $model->name,
            'code'                      => $model->code,
            'memo'                      => $model->memo,
            'credit_limit'              => $model->number()->credit_limit,
            'pricing_type'              => $model->pricing_type,
            'historical_change_revert'  => $model->historical_change_revert,
            'historical_default_price'  => $model->historical_default_price,
            'pricing_rate'              => $model->number()->pricing_rate,
            'salesman_id'               => $model->salesman_id,
            'website'                   => $model->website,
            'term'                      => $model->term,
            'status'                    => $model->status
        ];
    }

    protected function detailTransformer($model)
    {
        return [
            'id'             => $model->id,
            'name'           => $model->name,
            'address'        => $model->address,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'contact_person' => $model->contact_person,
            'tin'            => $model->tin
        ];
    }

    protected function detailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }

    /**
     * Convert customer to Array
     * 
     * @param  array $customer
     * @return array
     */
    protected function toArray($customer)
    {
        $details = [];

        $collection = ! $customer['detail'] instanceof Collection 
            ? collect($customer['detail']) 
            : $customer['detail'];

        foreach ($collection as $key => $model) {
            $details[] = array_merge(
                $model->toArray(),
                array(
                    'id' => $model->id ?? 0
                )
            );
        }

        return array(
            'info' => array_merge($customer['info']->toArray(), [
                'credit_limit' => $customer['info']->credit_limit->innerValue(),
                'pricing_rate' => $customer['info']->pricing_rate->innerValue(),
            ]),
            'details' => $details
        ) ;
    }
}