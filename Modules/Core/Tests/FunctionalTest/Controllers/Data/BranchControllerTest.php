<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\Branch;
use Illuminate\Support\Collection;

class BranchControllerTest extends FunctionalTest
{
    private $uri = '/branch';

    private $factory = array(
        'info' => 'branch',
        'detail' => 'branch_detail'
    );

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_branch_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_branch_page_while_user_is_authenticated_will_successfully_display_the_page()
    {
        $this->visit($this->uri)->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['info']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'branches' => array(
                $this->item($data[0]['info'], 'branchBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]['info']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'branches' => array(
                $this->item($data[1]['info'], 'branchBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_business_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]['info']->business_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'business_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'branches' => array(
                $this->item($data[3]['info'], 'branchBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $branch = Branch::all()->first();

        $data = array_merge(
            array_pluck($this->collection(), 'info'),
            array($branch)
        );

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1', 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 5,
                    'count' => 5,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'branches' => $this->collect($data, 'branchBasicTransformer')
        ], ''));
    }

     /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 1000), 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'branches' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'info' => array(
                'code' => '',
                'name' => ''
            ),
            'detail' => array(
                'code' => '',
                'name' => ''
            )
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.required')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'info' => array(
                'code' => $created['info']->code,
                'name' => $created['info']->name
            ),
            'detail' => array(
                'code' => $created['details'][0]->code,
                'name' => $created['details'][0]->name
            )
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                ),
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.unique')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.unique')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_in_data_set_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $code = $this->faker->numberBetween(1, 1000);
        $name = $this->faker->name;

        $data['details'][0]['code'] = $code;
        $data['details'][0]['name'] = $name;
        $data['details'][1] = $data['details'][0];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.array.unique')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.array.unique')
                    )
                ),
                'details.1.code' => array(
                    sprintf('%s[2]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.array.unique')
                    )
                ),
                'details.1.name' => array(
                    sprintf('%s[2]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.array.unique')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = '.';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);
        
        $data['details'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'branch' => array_merge(
                    $this->item($build['info'], 'branchBasicTransformer'), 
                    array('id' => 2)
                ),
            ),
            'database' => array(
                'info' => array_merge(
                    $this->item($build['info'], 'branchBasicTransformer'), 
                    array('id' => 2)
                ),
                'detail' => array_merge(
                    $this->item($build['details'], 'branchDetailBasicTransformer'), 
                    array('id' => 2)
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('branch', $expected['database']['info'])
            ->seeInDatabase('branch_detail', $expected['database']['detail']);
    }

    /**
     * @test
     */ 
    public function updating_a_data_with_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['code'] = '';
        $data['info']['name'] = '';
        $data['details'][0]['code'] = '';
        $data['details'][0]['name'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.required')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_using_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['code'] = $collection[2]['info']->code;
        $data['info']['name'] = $collection[2]['info']->name;
        $data['details'][0]['code'] = $collection[2]['details'][0]->code;
        $data['details'][0]['name'] = $collection[2]['details'][0]->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                ),
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.unique')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.unique')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_which_contains_an_existing_code_or_name_in_data_set_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $code = $this->faker->numberBetween(1, 1000);
        $name = $this->faker->name;

        $data['details'][0]['code'] = $code;
        $data['details'][0]['name'] = $name;
        $data['details'][1]['code'] = $code;
        $data['details'][1]['name'] = $name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.code' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.array.unique')
                    )
                ),
                'details.0.name' => array(
                    sprintf('%s[1]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.array.unique')
                    )
                ),
                'details.1.code' => array(
                    sprintf('%s[2]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.code.array.unique')
                    )
                ),
                'details.1.name' => array(
                    sprintf('%s[2]. %s', 
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.name.array.unique')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect_format_of_details_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'] = '.';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.invalid.format')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_and_deleting_its_child_data_will_be_soft_deleted_in_the_database()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'] = [$data['details'][0]];

        $expected = array(
            'response' => array(
                'branch' => $this->item($created['info'], 'branchFullInfoTransformer')
            ),
            'database' => array(
                'id' => $created['details'][1]->id,
                'deleted_at' => null
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ))
            ->notSeeInDatabase('branch_detail', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_a_proper_format_of_data_will_successfully_update_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][1] = $this->item($this->build()['details'], 'branchDetailBasicTransformer');
        $data['details'][0]['code'] = $this->faker->numberBetween(1, 10000);
        $data['details'][0]['name'] = $this->faker->name;

        $expected = array(
            'response' => array(
                'branch' => $data['info']
            ),
            'database' => array(
                'info' =>  $data['info'],
                'detail' => array(
                    'created' => array_merge(
                        $data['details'][1],
                        array('id' => 4)
                    ),
                    'updated' => $data['details'][0],
                    'deleted' => array(
                        'id' => $created['details'][1]->id,
                        'deleted_at' => null
                    )
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase('branch', $expected['database']['info'])
            ->seeInDatabase('branch_detail', $expected['database']['detail']['created'])
            ->seeInDatabase('branch_detail', $expected['database']['detail']['updated'])
            ->notSeeInDatabase('branch_detail', $expected['database']['detail']['deleted']);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/5', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('branch', [
                'id' => 5
            ]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_and_its_relationship_in_database_and_return_a_successful_response()
    {   
        $created = $this->create();

        $expected = array(
            'database' => array(
                'info' => array(
                    'id' => $created['info']->id,
                    'deleted_at' => null
                ),
                'details' => array(
                    array(
                        'id' => $created['details'][0]->id,
                        'deleted_at' => null
                    ),
                    array(
                        'id' => $created['details'][1]->id,
                        'deleted_at' => null
                    )
                )
            )
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('branch', $expected['database']['info'])
            ->notSeeInDatabase('branch_detail', $expected['database']['details'][0])
            ->notSeeInDatabase('branch_detail', $expected['database']['details'][1]);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/2', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('branch', array(
                    'id' => 2,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_branch_show_api_will_return_its_corresponding_info()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $expected = array(
            'branch' => $data['info'],
            'details' => array(
                'collection' => $data['details'],
                'meta' => array(
                    'pagination' => array(
                        'total' => count($data['details']),
                        'count' => count($data['details']),
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    )
                )
            )
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_branch_will_return_an_error()
    {
        $this->getJson(sprintf('%s/2', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed') 
            ]));
    }

    /**
     * @test
     */
    public function accessing_details_will_return_details_in_chosen_format()
    {
        $created = $this->create();

        $expected = array(
            'details' => $this->collect($created['details'], 'branchDetailChosenTransformer')
        );

        $this->getJson(sprintf('%s/%s/details', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    protected function build($data = array())
    {
        $branch = Factory::build(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::build(
            $this->factory['detail'], 
            isset($data['detail']) 
                ? $data['detail']
                : []
        );

        return array(
            'info' => $branch,
            'details' => $details
        );
    }

    protected function create($data = array())
    {
        $branch = Factory::create(
            $this->factory['info'], 
            isset($data['info']) 
                ? $data['info']
                : []
        );

        $details = Factory::times(2)->create(
            $this->factory['detail'], 
            array_merge(
                array('branch_id' => $branch->id),
                isset($data['detail']) 
                ? $data['detail']
                : []
            )
        );

        return array(
            'info' => $branch,
            'details' => $details
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function branchBasicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }

    protected function branchFullInfoTransformer($model)
    {
        return [
            'id'                 => $model->id,
            'code'               => $model->code,
            'name'               => $model->name,
            'address'            => $model->address ?? '',
            'contact'            => $model->contact ?? '',
            'business_name'      => $model->business_name ?? ''
        ];
    }

    protected function branchDetailBasicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'name'          => $model->name,
            'address'       => $model->address ?? '',
            'contact'       => $model->contact ?? '',
            'business_name' => $model->business_name ?? ''
        ];
    }

    protected function branchDetailChosenTransformer($model)
    {
        return [
            'id'            => $model->id,
            'label'         => $model->name,
        ];
    }

    protected function toArray($array)
    {
        if (!is_array($array['details']) 
            && !$array['details'] instanceof Collection
        ) {
            $array['details'] = [$array['details']];
        }

        return array(
            'info' => $this->item($array['info'], 'branchFullInfoTransformer'),
            'details' => $this->collect($array['details'], 'branchDetailBasicTransformer')
        );
    }
}
