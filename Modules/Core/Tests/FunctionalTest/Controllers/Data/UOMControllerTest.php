<?php

use Modules\Core\Enums\SystemCodeType;

class UOMControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/uom';
    protected $factory = 'uom';
    protected $type = SystemCodeType::UOM;
}