<?php

use Laracasts\TestDummy\Factory;

class UserControllerTest extends FunctionalTest
{
    private $uri = '/user';
    private $factory = 'user';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_user_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_user_list_route_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => array(
                $this->item($data[0], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_username_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->username,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'username'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => array(
                $this->item($data[1], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_full_name_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->full_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'full_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => array(
                $this->item($data[2], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_position_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]->position,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'position'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => array(
                $this->item($data[3], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => $data->count(),
                    'count' => $data->count(),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'users' => array()
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function visiting_the_create_page_will_render_the_form_for_creating_with_a_list_of_branches()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas(['branches', 'sections']);
    }

    /**
     * @test
     */
    public function saving_an_empty_code_username_password_full_name_or_status_will_trigger_an_error_response()
    {
        $build = $this->build([
            'code' => '',
            'username' => '',
            'full_name' => '',
        ]);

        $data = $this->toArray($build);

        $data['info']['password'] = '';
        $data['info']['status'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.username' => array(
                    Lang::get('core::validation.username.required')
                ),
                'info.password' => array(
                    Lang::get('core::validation.password.required')
                ),
                'info.full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'info.status' => array(
                    Lang::get('core::validation.status.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_role_id_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['role_id'] = 1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.role_id' => array(
                    Lang::get('core::validation.role.doesnt.exists')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_password_with_a_character_less_than_six_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['password'] = '12345';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.password' => array(
                    Lang::get('core::validation.password.min.length')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_status_value_other_that_one_or_zero_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'status' => 2
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_email_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'email' => $this->faker->name
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_permission_format_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['permissions'] = '.';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'permissions' => array(
                    Lang::get('core::validation.permissions.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_permission_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['permissions'] = [];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'permissions' => array(
                    Lang::get('core::validation.permissions.min.length')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_username_or_full_name_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $build = $this->build(array(
            'code' => $existing['info']->code,
            'username' => $existing['info']->username,
            'full_name' => $existing['info']->full_name
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                ),
                'info.username' => array(
                    Lang::get('core::validation.username.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_user_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'user' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array('id' => 2)
                )
            ),
            'database' => array(
                'user' => array_merge(
                    array_except($data['info'], 'password'),
                    array('id' => 2)
                ),
                'permissions' => array(
                    array(
                        'user_id' => 2,
                        'branch_id' => $build['permissions'][0]['branch'],
                        'permission_id' => $build['permissions'][0]['code'][0]
                    ),
                    array(
                        'user_id' => 2,
                        'branch_id' => $build['permissions'][0]['branch'],
                        'permission_id' => $build['permissions'][0]['code'][1]
                    ),
                    array(
                        'user_id' => 2,
                        'branch_id' => $build['permissions'][0]['branch'],
                        'permission_id' => $build['permissions'][0]['code'][2]
                    ),
                    array(
                        'user_id' => 2,
                        'branch_id' => $build['permissions'][0]['branch'],
                        'permission_id' => $build['permissions'][0]['code'][3]
                    )
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('user', $expected['database']['user'])
            ->seeInDatabase('branch_permission_user', $expected['database']['permissions'][0])
            ->seeInDatabase('branch_permission_user', $expected['database']['permissions'][1])
            ->seeInDatabase('branch_permission_user', $expected['database']['permissions'][2])
            ->seeInDatabase('branch_permission_user', $expected['database']['permissions'][3]);
    }

    /**
     * @test
     */
    public function visiting_the_edit_page_will_render_the_form_for_editing_with_a_list_of_branches()
    {
        $created = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $created['info']->id))
            ->assertResponseOk()
            ->assertViewHas(['branches', 'sections']);
    }

    /**
     * @test
     */
    public function accessing_the_show_route_via_ajax_json_will_return_the_full_info_of_a_user()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $this->getJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'user' => $this->item($created['info'], 'fullInfoTransformer'),
                'permissions' => $data['permissions']
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_username_full_name_or_status_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['password'] = '';
        $data['info']['code'] = '';
        $data['info']['username'] = '';
        $data['info']['full_name'] = '';
        $data['info']['status'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.username' => array(
                    Lang::get('core::validation.username.required')
                ),
                'info.full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'info.status' => array(
                    Lang::get('core::validation.status.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_password_less_than_six_characters_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['password'] = '12345';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.password' => array(
                    Lang::get('core::validation.password.min.length')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_status_value_other_that_one_or_zero_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['status'] = '4';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_invalid_email_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['email'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_invalid_permission_format_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['permissions'] = '.';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'permissions' => array(
                    Lang::get('core::validation.permissions.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_permission_details_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['permissions'] = [];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'permissions' => array(
                    Lang::get('core::validation.permissions.min.length')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_username_or_full_name_will_trigger_an_error_response()
    {
        $existing = $this->create();
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['code'] = $existing['info']->code;
        $data['info']['username'] = $existing['info']->username;
        $data['info']['full_name'] = $existing['info']->full_name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                ),
                'info.username' => array(
                    Lang::get('core::validation.username.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $random = rand(2, 10);

        $this->patchJson(sprintf('%s/%s', $this->uri, $random), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('user', array('id' => $random));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_proper_format_will_successfully_update_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $this->toArray($created);

        // Override new values from build
        $data['info'] = array_merge(
            $data['info'],
            $this->toArray($build)['info']
        );

        // Remove other permission to see if sync is working
        $removed[] = array_pull($data['permissions'][0]['code'], 0);
        $removed[] = array_pull($data['permissions'][0]['code'], 1);
        
        $data['permissions'][0]['code'] = array_values($data['permissions'][0]['code']);

        $expected = array(
            'response' => array(
                'user' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array('id' => $created['info']->id)
                )
            ), 
            'database' => array(
                'user' => array_merge(
                    array_except($data['info'], 'password'),
                    array('id' => $created['info']->id)
                ), 
                'branch_permission_user' => array(
                    array(
                        'user_id' => $created['info']->id,
                        'branch_id' => $data['permissions'][0]['branch'],
                        'permission_id' => $data['permissions'][0]['code'][0]
                    ),
                    array(
                        'user_id' => $created['info']->id,
                        'branch_id' => $data['permissions'][0]['branch'],
                        'permission_id' => $data['permissions'][0]['code'][1]
                    ),
                ),
                'dont_see' => array(
                    array(
                        'user_id' => $created['info']->id,
                        'branch_id' => $data['permissions'][0]['branch'],
                        'permission_id' => $removed[0]
                    ),
                    array(
                        'user_id' => $created['info']->id,
                        'branch_id' => $data['permissions'][0]['branch'],
                        'permission_id' => $removed[1]
                    )
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase('user', $expected['database']['user'])
            ->seeInDatabase('branch_permission_user', $expected['database']['branch_permission_user'][0])
            ->seeInDatabase('branch_permission_user', $expected['database']['branch_permission_user'][1])
            ->dontSeeInDatabase('branch_permission_user', $expected['database']['dont_see'][1])
            ->dontSeeInDatabase('branch_permission_user', $expected['database']['dont_see'][1]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('user', array(
                    'id' => $created['info']->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {
        $random = rand(2, 10);

        $this->deleteJson(sprintf('%s/%s', $this->uri, $random))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('user', array(
                    'id' => $random,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function changing_password_with_incorrect_old_password_will_trigger_an_error()
    {
        $created = $this->create([
            'password' => 'password'
        ]);

        $data = [
            'old_password' => strrev('password'),
            'new_password' => 'abcdef',
            'new_password_confirmation' => 'abcdef'
        ];

        $this->patchJson(sprintf('%s/change-password', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'old.password.invalid' => Lang::get('core::validation.old.password.incorrect')
            ]));
    }

    /**
     * @test
     */
    public function changing_password_with_same_new_password_will_trigger_an_error()
    {
        $created = $this->create([
            'password' => 'password'
        ]);

        $this->logout();
        $this->login($created['info']->id);

        $data = [
            'old_password' => 'password',
            'new_password' => 'password',
            'new_password_confirmation' => 'password'
        ];

        $this->patchJson(sprintf('%s/change-password', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'cant.change.same.password' => Lang::get('core::validation.cant.change.same.password')
            ]));
    }

    /**
     * @test
     */
    public function changing_password_with_different_new_password_and_confirm_password_will_trigger_an_error()
    {
        $created = $this->create([
            'password' => 'password'
        ]);

        $this->logout();
        $this->login($created['info']->id);

        $data = [
            'old_password' => 'password',
            'new_password' => 'abcdef',
            'new_password_confirmation' => '123456'
        ];

        $this->patchJson(sprintf('%s/change-password', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'new_password' => array(
                    Lang::get('core::validation.confirm.password.incorrect')
                )
            ]));
    }

    /**
     * @test
     */
    public function changing_password_with_password_shorter_than_six_characters_will_trigger_an_error()
    {
        $created = $this->create([
            'password' => 'password'
        ]);

        $this->logout();
        $this->login($created['info']->id);

        $data = [
            'old_password' => 'password',
            'new_password' => '1234',
            'new_password_confirmation' => '1234'
        ];

        $this->patchJson(sprintf('%s/change-password', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'new_password' => array(
                    Lang::get('core::validation.new.password.min.length')
                )
            ]));
    }

    /**
     * @test
     */
    public function changing_password_with_empty_data_will_trigger_an_error()
    {
        $created = $this->create();

        $this->logout();
        $this->login($created['info']->id);

        $this->patchJson(sprintf('%s/change-password', $this->uri), [])
            ->seeJsonEquals($this->errorResponse([
                'old_password' => array(
                    Lang::get('core::validation.password.required')
                ),
                'new_password' => array(
                    Lang::get('core::validation.new.password.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function changing_password_with_correct_data_will_return_success_message()
    {
        $created = $this->create([
            'password' => 'password'
        ]);

        $this->logout();
        $this->login($created['info']->id);

        $data = [
            'old_password' => 'password',
            'new_password' => '123456',
            'new_password_confirmation' => '123456'
        ];

        $this->patchJson(sprintf('%s/change-password', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.changed')));
    }

    protected function build($data = array())
    {
        $user = Factory::build($this->factory, $data);

        $permissions = [];
        $codes = [];

        for ($i=1; $i <= 4; $i++) { 
            $codes[] = $i;
        }

        $permissions[] = array(
            'branch' => 1,
            'code' => $codes
        );

        return array(
            'info' => $user,
            'permissions' => collect($permissions)
        );
    }

    protected function create($data = array())
    {
        $user = Factory::create($this->factory, $data);

        $permissions = [];
        $codes = [];

        for ($i=1; $i <= 4; $i++) { 
            $codes[] = $i;

            DB::table('branch_permission_user')->insert(array(
                'branch_id' => 1,
                'user_id' => $user->id,
                'permission_id' => $i
            ));
        }

        $permissions[] = array(
            'branch' => 1,
            'code' => $codes
        );

        return array(
            'info' => $user,
            'permissions' => collect($permissions)
        );
    }

    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) { 
            $data[] = $this->create();
        }

        return collect($data);
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'full_name'     => $model->full_name,
            'username'      => $model->username,
            'status'        => $model->presenter()->status
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'        => $model->id,
            'code'      => $model->code,
            'username'  => $model->username,
            'full_name' => $model->full_name,
            'status'    => $model->status,
            'telephone' => $model->telephone,
            'mobile'    => $model->mobile,
            'position'  => $model->position,
            'email'     => $model->email,
            'address'   => $model->address,
            'memo'      => $model->memo,
        ];
    }

    protected function toArray($model)
    {
        return [
            'info' => [
                'id'        => $model['info']->id,
                'code'      => $model['info']->code,
                'username'  => $model['info']->username,
                'full_name' => $model['info']->full_name,
                'password'  => $model['info']->password,
                'status'    => $model['info']->status,
                'telephone' => $model['info']->telephone,
                'mobile'    => $model['info']->mobile,
                'position'  => $model['info']->position,
                'email'     => $model['info']->email,
                'address'   => $model['info']->address,
                'memo'      => $model['info']->memo,
                'role_id'      => $model['info']->role_id,
            ],
            'permissions' => $model['permissions']->toArray(),
        ];
    }
}
