<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;
use Modules\Core\Enums\ModuleGroup;

class ReportBuilderControllerTest extends FunctionalTest
{
    private $uri = 'reports/builder';
    private $factory = 'report_builder';
    private $module;
    private $templateData;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->module = ModuleGroup::PURCHASE_INBOUND;
    }

    /**
     * @test
     */
    public function accessing_report_builder_module_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_report_builder_list_route_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_template_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'templates' => array(
                $this->item($data[0], 'transformList')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_template_module_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection(1);
        
        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data->group_id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'module'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'templates' => array(
                $this->item($data, 'transformList')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'templates' => $this->collect($data, 'transformList')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '999',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'group_id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'templates' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function visiting_the_create_page_will_render_the_form_for_creating_with_a_list_of_branches()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_an_empty_name_or_module_will_trigger_an_error_response()
    {
        $build = $this->build([
            'name' => '',
            'format' => $this->buildTemplateData(),
        ]);

        $data = $this->toArray($build);

        $data['group_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'group_id' => array(
                    Lang::get('core::validation.module.required')
                ),
                'name' => array(
                    Lang::get('core::validation.name.required')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_format_for_format_will_trigger_an_error_response()
    {
        $build = $this->build([
            'group_id' => $this->module,
            'format' => "aaa",
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'format' => array(
                    Lang::get('core::validation.invalid.template.data.format')
                ),
        ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_name_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $build = $this->build(array(
            'name' => $existing->name,
            'group_id' => $existing->group_id,
            'format' => $this->buildTemplateData(),
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_report_builder_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build([
            'group_id' => $this->module,
            'format' => [],
        ]);

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'template' => array_merge(
                    $this->item($build, 'transformDetail'),
                    array("id" => 1)
                )
            ),
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
        ));
    }

    /**
     * @test
     */
    public function visiting_the_edit_page_will_render_the_form_for_editing_with_a_list_of_templates()
    {
        $created = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $created->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function updating_a_template_with_an_empty_name_or_module_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['name'] = '';
        $data['group_id'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'group_id' => array(
                    Lang::get('core::validation.module.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_template_with_an_invalid_format_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['format'] = 'aaaa';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'format' => array(
                    Lang::get('core::validation.invalid.template.data.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_template_with_an_existing_template_name_will_trigger_an_error_response()
    {   
        $existing = $this->create();
        $created = $this->create([
            'name' => $existing->name,
            'group_id' => $existing->group_id,
            'format' => $this->buildTemplateData(),
        ]);

        $data = $this->toArray($created);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'name' => array(
                    Lang::get('core::validation.name.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_proper_format_will_successfully_update_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $build = $this->build([
            'name' => 'Template One'
        ]);

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'template' => array_merge(
                    $this->item($build, 'transformDetail'),
                    array('id' => $created->id)
                )
            ), 
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ));
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('report_builder', array(
                    'id' => $created->id
                )
            );
    }

    protected function create($data = array())
    {
        if(count($data) == 0) {
            $data = array(
                'group_id' => $this->module,
                'format' => $this->buildTemplateData()
            );
        }

        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'group_id' => $this->module,
            'format' => $this->buildTemplateData()
        ));
    }

    protected function build($data = array())
    {   
        return Factory::build($this->factory, $data); 
    }

    protected function toArray($model)
    {
        return [
            'id'       => $model->id,
            'name'     => $model->name,
            'group_id' => $model->group_id,
            'format'   => $model->format,
        ];
    }

    protected function buildTemplateData() {
        return  [
            "header_style" => [
                "bold" => false,
                "textAlign" => "center"
            ],
            "columns" => [
                "transaction_status" => [
                  "header_name" => "Transaction Status",
                  "width" => "40",
                  "textAlign" => "left",
                  "type" => 1,
                ]
            ]
        ];
    }

    protected function transformList($model)
    {   
        return [
            'id'           => $model->id,
            'name'         => $model->name,
            'module_group' => $model->presenter()->moduleGroup,
            'created_by'   => is_null($model->userCreated) ? '' : $model->userCreated->full_name,
            'created_at'   => $model->created_at->toDateTimeString(),
            'modified_by'  => is_null($model->userModified) ? '' : $model->userModified->full_name,
            'updated_at'   => $model->updated_at->toDateTimeString()
        ];
    }

    protected function transformDetail($model)
    {   
        return [
            'id'       => $model->id,
            'name'     => $model->name,
            'group_id' => $model->group_id,
            'format'   => $model->format,
        ];
    }
}