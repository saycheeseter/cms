<?php

use Modules\Core\Enums\SystemCodeType;

class PaymentMethodControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/payment-method';
    protected $factory = 'payment_method';
    protected $type = SystemCodeType::PAYMENT_METHOD;
}
