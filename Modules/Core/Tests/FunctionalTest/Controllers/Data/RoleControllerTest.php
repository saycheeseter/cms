<?php

use Laracasts\TestDummy\Factory;

class RoleControllerTest extends FunctionalTest
{
    private $uri = '/role';
    private $factory = 'role';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_user_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_user_list_route_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['role']->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['role'], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['role']->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['role'], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->create();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data['role']->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data['role'], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function saving_an_empty_data_will_trigger_an_error_response()
    {
        $this->postJson($this->uri, [])
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'permissions' => array(
                    Lang::get('core::validation.permissions.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = [
            'info' => [
                'code' => $created['role']->code,
                'name' => $created['role']->name,
            ],
            'permissions' => [$created['permission']->id]
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_permissions_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build();

        $data = [
            'info' => [
                'code' => $build->code,
                'name' => $build->name,
            ],
            'permissions' => 'abc'
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'permissions' => array(
                    Lang::get('core::validation.permissions.invalid.format')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_correct_data_will_insert_into_database()
    {
        $build = $this->build();

        $data = [
            'info' => [
                'code' => $build->code,
                'name' => $build->name,
            ],
            'permissions' => [$this->reference()['permission']->id]
        ];

        $expected = [
            'role' => [
                'code' => $build->code,
                'name' => $build->name,
            ],
            'permission_role' => [
                'role_id' => 1,
                'permission_id' => $data['permissions'][0]
            ]
        ];

        $this->postJson($this->uri, $data)
            ->seeInDatabase('role', $expected['role'])
            ->seeInDatabase('permission_role', $expected['permission_role']);
    }

    /**
     * @test
     */
    public function updating_with_correct_data_will_save_changes_into_database()
    {
        $created = $this->create();

        $build = $this->build();

        $data = [
            'apply' => false,
            'info' => [
                'code' => $build->code,
                'name' => $build->name,
            ],
            'permissions' => [$created['permission']->id]
        ];

        $expected = [
            'role' => [
                'code' => $build->code,
                'name' => $build->name,
            ],
            'permission_role' => [
                'role_id' => 1,
                'permission_id' => $data['permissions'][0]
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['role']->id), $data)
            ->seeInDatabase('role', $expected['role'])
            ->seeInDatabase('permission_role', $expected['permission_role']);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['role']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('role', array(
                    'id' => $created['role']->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_an_existing_used_data_will_trigger_an_error_response()
    {
        $created = $this->create();

        $user = Factory::create('user', [
            'role_id' => $created['role']->id
        ]);

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['role']->id))
            ->seeJsonEquals($this->errorResponse([
                'role.already.used' => Lang::get('core::validation.role.already.used')
            ]));
    }

    /**
     * @test
     */
    public function adding_a_permission_in_role_and_applying_it_to_user_will_add_in_branch_permission_table()
    {
        $created = $this->create();

        $permission = $this->reference()['permission'];

        $user = Factory::create('user', [
            'role_id' => $created['role']->id
        ]);

        DB::table('branch_permission_user')->insert([
            'branch_id' => 1,
            'permission_id' => $created['permission']->id,
            'user_id' => $user->id
        ]);

        $data = [
            'apply' => true,
            'info' => [
                'code' => $created['role']->code,
                'name' => $created['role']->name,
            ],
            'permissions' => [$created['permission']->id, $permission->id]
        ];

        $expected = [
            'permissions' => [
                [
                    'branch_id' => 1,
                    'permission_id' => $created['permission']->id,
                    'user_id' => $user->id
                ],
                [
                    'branch_id' => 1,
                    'permission_id' => $permission->id,
                    'user_id' => $user->id
                ]
            ]
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['role']->id), $data)
            ->seeInDatabase('branch_permission_user', $expected['permissions'][0])
            ->seeInDatabase('branch_permission_user', $expected['permissions'][1]);
    }

    /**
     * @test
     */
    public function removing_a_permission_in_role_and_applying_it_to_user_will_remove_in_branch_permission_table()
    {
        $created = $this->create();

        $permission = $this->reference()['permission'];

        $user = Factory::create('user', [
            'role_id' => $created['role']->id
        ]);

        DB::table('branch_permission_user')->insert([
            [
                'branch_id' => 1,
                'permission_id' => $created['permission']->id,
                'user_id' => $user->id
            ],
            [
                'branch_id' => 1,
                'permission_id' => $permission->id,
                'user_id' => $user->id
            ]
        ]);

        DB::table('permission_role')->insert([
            'role_id' => $created['role']->id,
            'permission_id' => $permission->id
        ]);

        $data = [
            'apply' => true,
            'info' => [
                'code' => $created['role']->code,
                'name' => $created['role']->name,
            ],
            'permissions' => [$created['permission']->id]
        ];

        $expected = [
            'permissions' => [
                [
                    'branch_id' => 1,
                    'permission_id' => $created['permission']->id,
                    'user_id' => $user->id
                ],
                [
                    'branch_id' => 1,
                    'permission_id' => $permission->id,
                    'user_id' => $user->id
                ]
            ]
        ];

        $this->seeInDatabase('branch_permission_user', $expected['permissions'][1])
            ->patchJson(sprintf('%s/%s', $this->uri, $created['role']->id), $data)
            ->seeInDatabase('branch_permission_user', $expected['permissions'][0])
            ->notSeeInDatabase('branch_permission_user', $expected['permissions'][1]);
    }

    protected function build($data = [])
    {
        return Factory::build('role', $data);
    }

    protected function create($data = [])
    {
        $role = Factory::create('role', $data);

        $permission = Factory::create('permission');

        $permission_role = DB::table('permission_role')->insert([
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);

        return [
            'role' => $role,
            'permission' => $permission,
            'permission_role' => $permission_role
        ];
    }

    protected function reference()
    {
        return [
            'permission' => Factory::create('permission')
        ];
    }

    protected function basicTransformer($data)
    {
        return [
            'id'   => $data->id,
            'code' => $data->code,
            'name' => $data->name
        ];
    }
}