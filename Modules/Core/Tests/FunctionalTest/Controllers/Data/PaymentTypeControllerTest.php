<?php

use Modules\Core\Enums\SystemCodeType;

class PaymentTypeControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/payment-type';
    protected $factory = 'payment_type';
    protected $type = SystemCodeType::PAYMENT_TYPE;
}