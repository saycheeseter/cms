<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\Status;
use Carbon\Carbon;

class MemberRateControllerTest extends FunctionalTest
{
    private $uri = '/member-rate';

    private $factories = array(
        'info' => 'member_rate',
        'detail' => 'member_rate_detail'
    );

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_member_rate_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_member_rate_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'name' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'rates' => $this->collect([$data[1]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_memo_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->memo, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'memo' 
                )
            )
        ))
        ->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'rates' => $this->collect([$data[2]], 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->id,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'rates' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = collect($this->collection())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 100),
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'rates' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_incomplete_head_or_details_info_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'name' => '',
            'memo' => ''
        ));

        $data = $this->toArray($build);

        $data['details'][0]['amount_from'] = '';
        $data['details'][0]['amount_to'] = '';
        $data['details'][0]['amount_per_point'] = '';
        $data['details'][0]['discount'] = '';
        $data['details'][0]['date_from'] = '';
        $data['details'][0]['date_to'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'details.0.amount_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.required')
                ),
                'details.0.amount_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.required')
                ),
                'details.0.date_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.required')
                ),
                'details.0.date_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.required')
                ),
                'details.0.amount_per_point' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.required')
                ),
                'details.0.discount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_name_in_data_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'name' => $created['info']->name
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_rate_details_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['amount_from'] = $this->faker->name;
        $data['details'][0]['amount_to'] = $this->faker->name;
        $data['details'][0]['amount_per_point'] = $this->faker->name;
        $data['details'][0]['discount'] = $this->faker->name;
        $data['details'][0]['date_from'] = $this->faker->name;
        $data['details'][0]['date_to'] = $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.amount_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.invalid.format')
                ),
                'details.0.amount_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.invalid.format')
                ),
                'details.0.date_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.invalid.format')
                ),
                'details.0.date_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.invalid.format')
                ),
                'details.0.amount_per_point' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.invalid.format')
                ),
                'details.0.discount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_overlapping_date_of_rate_details_will_trigger_an_error_response()
    {
        $build = $this->build([], array(
            'date_from' => '2016-10-11',
            'date_to' => '2016-10-13'
        ));

        $data = $this->toArray($build);

        $data['details'][1] = $data['details'][0];
        $data['details'][1]['date_from'] = '2016-10-12';
        $data['details'][1]['date_to'] = '2016-10-14';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.date_to.overlap.state' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.overlap.state')
                ),
                'details.1.date_from.overlap.state' => array(
                    Lang::get('core::error.row').'[2]. '.Lang::get('core::validation.details.date.from.overlap.state')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build([], array(
            'date_from' => Carbon::tomorrow()->toDateString(),
            'date_to' => Carbon::tomorrow()->addDay()->toDateString()
        ));

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'rate' => array_merge(
                    $this->item($build['info'], 'basicTransformer'),
                    array('id' => 1)
                )
            ),
            'database' => array(
                'info' => array_merge(
                    $data['info'],
                    array('id' => '1')
                ),
                'details' => array_merge(
                    $data['details'][0],
                    array(
                        'id' => '1',
                        'member_rate_head_id' => '1',
                        'date_from' => Carbon::tomorrow()
                            ->startOfDay()
                            ->toDateTimeString(),
                        'date_to' => Carbon::tomorrow()
                            ->addDay()
                            ->endOfDay()
                            ->toDateTimeString(),
                    )
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('member_rate', $expected['database']['info'])
            ->seeInDatabase('member_rate_detail', $expected['database']['details']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incomplete_head_or_details_info_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['name'] = '';
        $data['details'][0]['amount_from'] = '';
        $data['details'][0]['amount_to'] = '';
        $data['details'][0]['amount_per_point'] = '';
        $data['details'][0]['discount'] = '';
        $data['details'][0]['date_from'] = '';
        $data['details'][0]['date_to'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'details.0.amount_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.required')
                ),
                'details.0.amount_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.required')
                ),
                'details.0.date_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.required')
                ),
                'details.0.date_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.required')
                ),
                'details.0.amount_per_point' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.required')
                ),
                'details.0.discount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect_format_of_rate_details_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['details'][0]['amount_from'] = $this->faker->name;
        $data['details'][0]['amount_to'] = $this->faker->name;
        $data['details'][0]['amount_per_point'] = $this->faker->name;
        $data['details'][0]['discount'] = $this->faker->name;
        $data['details'][0]['date_from'] = $this->faker->name;
        $data['details'][0]['date_to'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'details.0.amount_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.from.invalid.format')
                ),
                'details.0.amount_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.to.invalid.format')
                ),
                'details.0.date_from' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.from.invalid.format')
                ),
                'details.0.date_to' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.date.to.invalid.format')
                ),
                'details.0.amount_per_point' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.amount.per.point.invalid.format')
                ),
                'details.0.discount' => array(
                    Lang::get('core::error.row').'[1]. '.Lang::get('core::validation.details.discount.invalid.format')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_name_in_data_will_trigger_an_error_response()
    {
        $collection = collect($this->collection())->pluck('info');

        $created = $this->create();

        $data = $this->toArray($created);

        $data['info']['name'] = $collection[1]->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'info.name' => array(
                    Lang::get('core::validation.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_the_data_and_deleting_its_child_data_will_soft_delete_the_data_in_the_database()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $date = Carbon::parse($data['details'][0]['date_to']);

        $data['details'][0]['id'] = 0;
        $data['details'][0]['date_from'] = $date->tomorrow()->toDateString();
        $data['details'][0]['date_to'] = $date->tomorrow()->addDay()->toDateString();

        $expected = array(
            'response' => array(
                'rate' => $this->item($created['info'], 'basicTransformer'),
            ),
            'database' => array(
                'id' => $created['detail']->first()->id,
                'deleted_at' => null
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.updated')
            ))
            ->notSeeInDatabase('member_rate_detail', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_an_existing_data_with_a_proper_format_of_data_will_successfully_update_the_data_in_database_and_return_an_id()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([
                'rate' => $this->item($created['info'], 'basicTransformer')
            ], Lang::get('core::success.updated')));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/1', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('member_rate', [
                'id' => 1
            ]);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_and_its_relationship_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $expected = array(
            'info' => array(
                'id' => $created['info']->id,
                'deleted_at' => null
            ),
            'detail' => array(
                'id' => $created['detail']->first()->id,
                'deleted_at' => null
            )
        );

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('member_rate', $expected['info'])
            ->notSeeInDatabase('member_rate_detail', $expected['detail']);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('member_rate', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_an_already_used_rate_will_trigger_an_error_response()
    {
        $created = $this->create();

        Factory::create('member', array(
            'rate_head_id' => $created['info']->id
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse([
                Lang::get('core::validation.delete.used.rate')
            ]));
    }

    /**
     * @test
     */
    public function accessing_member_rate_will_return_the_full_info_of_the_rate()
    {
        $created = $this->create();

        $expected = array(
            'rate' => $this->item($created['info'], 'fullInfoTransformer'),
            'details' => array(
                'collection' => $this->collect($created['detail'], 'detailTransformer'),
                'meta' => array(
                    'pagination' => array(
                        'total' => count($created['detail']),
                        'count' => count($created['detail']),
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    )
                )
            )
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_rate_will_return_an_error()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed')
            ]));
    }

    /**
     * Build non-persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], $info);

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'member_rate_head_id' => $info->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => collect([$detail])
        );
    }

    /**
     * Build persisting customer model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function create($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['info'], $info);

        $detail = Factory::create($this->factories['detail'], array_merge(array(
            'member_rate_head_id' => $info->id,
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => collect([$detail])
        );
    }

    /**
     * Build a collection of customer
     * 
     * @param  int  $time  
     * @return array        
     */
    protected function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function basicTransformer($model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'memo'          => $model->memo ?? '',
            'status'        => $model->presenter()->status
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'memo'          => $model->memo ?? '',
            'status'        => $model->status
        ];
    }

    protected function detailTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'member_rate_head_id'   => $model->member_rate_head_id,
            'amount_from'           => $model->number()->amount_from,
            'amount_to'             => $model->number()->amount_to,
            'date_from'             => $model->date_from->toDateString(),
            'date_to'               => $model->date_to->toDateString(),
            'amount_per_point'      => $model->number()->amount_per_point,
            'discount'              => $model->number()->discount
        ];
    }

    protected function detailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }

    /**
     * Convert rate to Array
     * 
     * @param  array $rate
     * @return array
     */
    protected function toArray($rate)
    {
        $details = [];

        $collection = ! $rate['detail'] instanceof Collection 
            ? collect($rate['detail']) 
            : $rate['detail'];

        foreach ($collection as $key => $model) {
            $details[] = array_merge(
                $model->toArray(),
                array(
                    'id' => $model->id ?? 0,
                    'discount' => $model->discount !== '' 
                        ? $model->discount->innerValue()
                        : '',
                    'amount_from' => $model->amount_from !== '' 
                        ? $model->amount_from->innerValue()
                        : '',
                    'amount_to' => $model->amount_to !== '' 
                        ? $model->amount_to->innerValue()
                        : '',
                    'amount_per_point' => $model->amount_per_point !== '' 
                        ? $model->amount_per_point->innerValue()
                        : '',
                    'date_from' => $model->date_from->toDateString(),
                    'date_to' => $model->date_to->toDateString(),
                )
            );
        }

        return array(
            'info' => $rate['info']->toArray(),
            'details' => $details
        ) ;
    }
}
