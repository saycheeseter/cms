<?php

use Modules\Core\Enums\SystemCodeType;

class BrandControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/brand';
    protected $factory = 'brand';
    protected $type = SystemCodeType::BRAND;
}