<?php

use Modules\Core\Enums\SystemCodeType;

class IncomeTypeControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/income-type';
    protected $factory = 'income_type';
    protected $type = SystemCodeType::INCOME_TYPE;
}