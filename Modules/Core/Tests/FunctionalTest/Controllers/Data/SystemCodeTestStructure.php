<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Laracasts\TestDummy\Factory;
use Modules\Core\Entities\SystemCode;

abstract class SystemCodeTestStructure extends FunctionalTest
{
    protected $uri;
    protected $factory;
    protected $type;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->clean();
    }

    /**
     * @test
     */
    public function accessing_system_code_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_system_code_page_will_successfully_display_the_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHasAll(array('title'));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'system_codes' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'system_codes' => array(
                $this->item($data[2], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]->remarks,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'remarks'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'system_codes' => array(
                $this->item($data[3], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1', 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'system_codes' => $this->collect($data, 'transform')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 'Here', 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'code' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'system_codes' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $build = $this->create(array(
            'code' => '',
            'name' => ''
        ));

        $data = $build->toArray();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'name' => array(
                    Lang::get('core::validation.name.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $build = $this->build(array(
            'code' => $collection[0]->code,
            'name' => $collection[0]->name
        ));

        $data = $build->toArray();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'name' => array(
                    Lang::get('core::validation.name.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_unique_code_and_name_will_successfully_save_the_system_code_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $build->toArray();

        $expected = array(
            'response' => array(
                'system_code' => array_merge(
                    $this->item($build, 'transform'),
                    array('id' => 14)
                )
            ),
            'database' => array_merge(
                $data,
                array('type_id' => $this->type)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created'
            )))
            ->seeInDatabase('system_code', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_or_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $created->toArray();

        $data['code'] = '';
        $data['name'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'name' => array(
                    Lang::get('core::validation.name.required')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_or_name_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $created = $this->create();

        $data = $created->toArray();

        $data['code'] = $collection[1]->code;
        $data['name'] = $collection[1]->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'name' => array(
                    Lang::get('core::validation.name.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_unique_code_and_name_will_successfully_save_the_system_code_in_database_and_return_an_id()
    {
        $created = $this->create();

        $build = $this->build();

        $data = $build->toArray();

        $expected = array(
            'response' => array(
                'system_code' => array_merge(
                    $this->item($build, 'transform'),
                    array('id' => $created->id)
                )
            ),
            'database' => array_merge(
                $data,
                array(
                    'type_id' => $this->type,
                    'id' => $created->id
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.updated'
            )))
            ->seeInDatabase('system_code', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $build->toArray();

        $this->patchJson(sprintf('%s/5', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('system_code', array_merge(
                $data, 
                array('type_id' => $this->type)
            ));
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('system_code', array(
                    'id' => 1
                )
            );
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_delete_the_data_in_database_and_return_a_successful_response()
    {   
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->dontSeeInDatabase('system_code', array(
                    'id' => $created->id
                )
            );
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function transform($model)
    {
        return array(
            'id'      => $model->id,
            'code'    => $model->code,
            'name'    => $model->name,
            'remarks' => $model->remarks,
        );
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function clean()
    {
        SystemCode::getQuery()->delete();
    }
}
