<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Entities\GenericSetting;
use Laracasts\TestDummy\Factory;

class GenericSettingControllerTest extends FunctionalTest
{
    private $uri = '/settings/generic';
    private $factory = 'generic_setting';
    
    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->clean();
    }

    /**
     * @test
     */
    public function accessing_settings_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();
        
        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_index_url_will_display_the_list_of_settings()
    {
        $data = $this->create();

        $expected = $data->pluck('value', 'key')->toArray();

        $this->getJson($this->uri)->seeJsonEquals(
            $this->successfulResponse([
                'settings' => $expected
            ])
        );
    }
    
    /**
     * @test
     */
    public function saving_the_setting_will_successfully_saved_the_data_in_database_and_return_a_successful_response()
    {
        $collection = $this->build(4);

        $data = $collection->mapWithKeys(function ($item) {
            return [$item->key => $item->value];
        })->toArray();
        
        $expected = $collection->map(function ($item) {
            return [
                'key' => $item->key,
                'value' => $item->value
            ];
        })->toArray();
        
        $this->postJson($this->uri, $data)->seeJsonEquals(
            $this->successfulResponse([], Lang::get('core::success.created'))
        )->seeInDatabase('generic_settings', $expected[0])
        ->seeInDatabase('generic_settings', $expected[1])
        ->seeInDatabase('generic_settings', $expected[2])
        ->seeInDatabase('generic_settings', $expected[3]);
    }


    /**
     * @test
     */
    public function updating_the_setting_will_successfully_update_the_data_in_database_and_return_a_successful_response()
    {
        $that = $this;

        $collection = $this->create();

        $data = $collection->mapWithKeys(function ($item) use ($that) {
            return [$item->key => 1];
        });

        $expected = $data->map(function ($value, $key) use ($collection) {
            return [
                'key' => $key,
                'value' => $value
            ];
        })->values()->toArray();
        
        $data = $data->toArray();
        
        $this->patchJson($this->uri, $data)->seeJsonEquals(
            $this->successfulResponse([], Lang::get('core::success.updated'))
        )->seeInDatabase('generic_settings', $expected[0])
        ->seeInDatabase('generic_settings', $expected[1])
        ->seeInDatabase('generic_settings', $expected[2])
        ->seeInDatabase('generic_settings', $expected[3]);
    }

    protected function build($times = 1)
    {
        if ($times === 1) {
            return Factory::build($this->factory);
        }

        $collection = [];

        for ($i=0; $i < $times; $i++) { 
            $collection[] = Factory::build($this->factory);
        }

        return collect($collection);
    }

    protected function create($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function clean()
    {
        GenericSetting::getQuery()->delete();
    }
}