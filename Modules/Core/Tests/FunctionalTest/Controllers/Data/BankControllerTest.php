<?php

use Modules\Core\Enums\SystemCodeType;

class BankControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/bank';
    protected $factory = 'bank';
    protected $type = SystemCodeType::BANK;
}
