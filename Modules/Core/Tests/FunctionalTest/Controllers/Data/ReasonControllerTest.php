<?php

use Modules\Core\Enums\SystemCodeType;

class ReasonControllerTest extends SystemCodeTestStructure
{
    protected $uri = '/reason';
    protected $factory = 'reason';
    protected $type = SystemCodeType::REASON;
}
