<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\ProductStatus;
use Modules\Core\Enums\InventoryStatus;
use Modules\Core\Enums\BranchPrice;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\CustomerPriceSettings;
use Modules\Core\Enums\SellingPriceBasis;
use Modules\Core\Enums\WalkInPriceSettings;
use Modules\Core\Enums\PriceScheme;
use Modules\Core\Enums\TransactionSummary;
use Litipk\BigNumbers\Decimal;
use Carbon\Carbon;

class ProductControllerTest extends FunctionalTest
{
    private $uri = 'product';

    private $factory = [
        'product' => 'product',
        'unit'   => 'product_unit',
        'barcode' => 'product_barcode',
        'price' => 'branch_price',
        'branch' => 'product_branch_info',
        'location' => 'product_branch_detail',
        'inventory' => 'product_branch_summary',
        'supplier_price' => 'product_supplier',
        'customer_price' => 'product_customer',
        'transaction_summary' => 'transaction_summary',
        'stock_card' => 'stock_card'
    ];

    private $references = [
        'uoms' => [],
        'categories' => [],
        'suppliers' => [],
        'taxes' => [],
        'brands' => [],
        'customers' => []
    ];

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->createReferences();
    }

    /**
     * @test
     */
    public function accessing_the_product_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_product_page_while_authenticated_will_successfully_display_the_page_and_return_a_permission_references()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('permissions');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_stock_no_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]['product']->stock_no,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'stock_no'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => array(
                $this->item($data[1], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_unit_barcode_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]['barcodes']->first()->code,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'barcode'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => array(
                $this->item($data[2], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]['product']->name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => array(
                $this->item($data[3], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_chinese_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]['product']->chinese_name,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'chinese_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => array(
                $this->item($data[0], 'list')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_product_status_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => ProductStatus::ACTIVE,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'status'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => $this->collect($data, 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_inventory_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => InventoryStatus::WITH_INVENTORY,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'inventory'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => $this->collect($data, 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => 1,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => $this->collect($data, 'list')
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_that_doesnt_meet_any_possible_match_will_return_an_empty_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->firstName,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'products' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function accessing_the_product_form_creation_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get(sprintf('%s/create', $this->uri))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_the_product_form_creation_while_authenticated_will_successfully_display_the_page_and_return_its_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHasAll([
                'suppliers', 
                'brands', 
                'uoms', 
                'branches', 
                'categories', 
                'taxes', 
                'locations', 
                'permissions'
            ]);
    }

    /**
     * @test
     */
    public function saving_an_empty_product_name_stock_number_supplier_id_category_id_brand_id_or_tax_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['product']['name'] = '';
        $data['product']['stock_no'] = '';
        $data['product']['supplier_id'] = '';
        $data['product']['category_id'] = '';
        $data['product']['brand_id'] = '';
        $data['product']['taxes'] = [];

        $this->postJson($this->uri, $data)
             ->seeJsonEquals($this->errorResponse([
                'product.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'product.stock_no' => array(
                    Lang::get('core::validation.stock.no.required')
                ),
                'product.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'product.category_id' => array(
                    Lang::get('core::validation.category.required')
                ),
                'product.brand_id' => array(
                    Lang::get('core::validation.brand.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_brand_category_or_supplier_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['product']['supplier_id'] = 100;
        $data['product']['category_id'] = 100;
        $data['product']['brand_id'] = 100;

        $this->postJson($this->uri, $data)
             ->seeJsonEquals($this->errorResponse([
                'product.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists')
                ),
                'product.category_id' => array(
                    Lang::get('core::validation.category.doesnt.exists')
                ),
                'product.brand_id' => array(
                    Lang::get('core::validation.brand.doesnt.exists')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_product_name_or_stock_number_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['product']['name'] = $existing['product']->name;
        $data['product']['stock_no'] = $existing['product']->stock_no;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'product.name' => array(
                    Lang::get('core::validation.name.unique')
                ),
                'product.stock_no' => array(
                    Lang::get('core::validation.stock.no.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_uom_id_or_qty_in_the_units_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['units'][0]['uom_id'] = '';
        $data['units'][0]['qty'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.required')
                    )
                ),
                'units.0.qty' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.qty.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_uom_id_in_the_units_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['units'][0]['uom_id'] = 100;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_qty_in_the_units_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['units'][0]['qty'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.qty' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.qty.numeric')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_already_existing_uom_id_of_the_product_in_the_units_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['units'][] = $data['units'][0];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.already.exists')
                    )
                ),
                'units.1.uom_id' => array(
                    sprintf("%s: %s[2]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.already.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_branch_id_purchase_price_selling_price_or_wholesale_price_in_the_prices_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['prices'][0]['branch_id'] = '';
        $data['prices'][0]['purchase_price'] = '';
        $data['prices'][0]['selling_price'] = '';
        $data['prices'][0]['wholesale_price'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.branch_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.branch.required')
                    )
                ),
                'prices.0.purchase_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.purchase.price.required')
                    )
                ),
                'prices.0.selling_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.selling.price.required')
                    )
                ),
                'prices.0.wholesale_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.wholesale.price.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_branch_id_in_the_prices_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['prices'][0]['branch_id'] = 100;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.branch_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.branch.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_purchase_selling_wholesale_or_price_a_to_e_in_the_prices_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['prices'][0]['purchase_price'] = 'A';
        $data['prices'][0]['selling_price'] = 'A';
        $data['prices'][0]['wholesale_price'] = 'A';
        $data['prices'][0]['price_a'] = 'A';
        $data['prices'][0]['price_b'] = 'A';
        $data['prices'][0]['price_c'] = 'A';
        $data['prices'][0]['price_d'] = 'A';
        $data['prices'][0]['price_e'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.purchase_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.purchase.price.numeric')
                    )
                ),
                'prices.0.selling_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.selling.price.numeric')
                    )
                ),
                'prices.0.wholesale_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.wholesale.price.numeric')
                    )
                ),
                'prices.0.price_a' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'A'])
                    )
                ),
                'prices.0.price_b' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'B'])
                    )
                ),
                'prices.0.price_c' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'C'])
                    )
                ),
                'prices.0.price_d' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'D'])
                    )
                ),
                'prices.0.price_e' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'E'])
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_empty_uom_id_or_code_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['barcodes'][0]['uom_id'] = '';
        $data['barcodes'][0]['code'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.required')
                    )
                ),
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_uom_id_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['barcodes'][0]['uom_id'] = 100;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_barcode_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $existing = $this->item($created, 'whole');

        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['barcodes'][0]['code'] = $existing['barcodes'][0]['code'];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
            ]));

        $data['barcodes'][] = $data['barcodes'][0];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
                'barcodes.1.code' => array(
                    sprintf("%s: %s[2]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_min_or_max_in_the_locations_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['locations'][0]['min'] = 'A';
        $data['locations'][0]['max'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'locations.0.min' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.min.numeric')
                    )
                ),
                'locations.0.max' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.max.numeric')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_invalid_format_of_units_prices_branches_locations_or_alternatives_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['units'] = 'A';
        $data['locations'] = 'A';
        $data['prices'] = 'A';
        $data['branches'] = 'A';
        $data['barcodes'] = 'A';
        $data['alternatives'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'units' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'prices' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'barcodes' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'branches' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.branch'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'locations' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.location'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'alternatives' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.alternative.items'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_min_greater_than_the_max_in_locations_section_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');

        $data['locations'][0]['min'] = 100;
        $data['locations'][0]['max'] = 1;
        
        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'locations.0.min' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.min.max.incorrect')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_corret_product_format_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $build = $this->build();

        $data = $this->item($build, 'whole');
        
        $id = 1;
        
        $expected = [
            'response' => [
                'product' => array_merge($this->item($build['product'], 'basic'), [
                    'id' => $id,
                ])
            ],
            'database' => [
                'product' => array_merge(
                    array_except($data['product'], ['created', 'modified']), 
                    ['id' => $id]
                ),
                'tax' => [
                    'product_id' => $id,
                    'tax_id' => $data['taxes'][0]
                ],
                'unit' => array_merge($data['units'][0], ['product_id' => $id]),
                'price' => array_merge(array_except($data['prices'][0], ['branch_name']), ['product_id' => $id]),
                'branch' => array_merge(array_except($data['branches'][0], ['branch_name']), ['product_id' => $id]),
                'location' => array_merge(array_except($data['locations'][0], ['branch_name', 'branch_detail_name']), ['product_id' => $id]),
            ]
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.created')))
            ->seeInDatabase('product', $expected['database']['product'])
            ->seeInDatabase('product_tax', $expected['database']['tax'])
            ->seeInDatabase('product_unit', $expected['database']['unit'])
            ->seeInDatabase('branch_price', $expected['database']['price'])
            ->seeInDatabase('product_branch_info', $expected['database']['branch'])
            ->seeInDatabase('product_branch_detail', $expected['database']['location']);
    }

    /**
     * @test
     */
    public function accessing_the_product_edit_form_while_user_is_not_authenticated_will_redirect_to_login()
    {
        $data = $this->create();

        Auth::logout();

        $this->get(sprintf('%s/%s/edit', $this->uri, $data['product']->id))->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function accessing_the_product_edit_form_while_authenticated_will_successfully_display_the_page_and_return_its_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['product']->id))
            ->assertResponseOk()
            ->assertViewHasAll([
                'suppliers', 
                'brands', 
                'uoms', 
                'branches', 
                'categories', 
                'taxes', 
                'locations', 
                'permissions'
            ]);
    }

    /**
     * @test
     */
    public function accessing_the_show_api_will_successfully_return_the_data_of_the_selected_product()
    {
        $data = $this->create();

        $transformed = $this->item($data, 'whole');

        $this->getJson(sprintf("%s/%s", $this->uri, $data['product']->id))
            ->seeJsonEquals($this->successfulResponse($transformed));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_product_will_return_an_error_response()
    {
        $this->getJson(sprintf("%s/1", $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::common.show.failed')
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_empty_product_name_stock_number_supplier_id_category_id_brand_id_or_tax_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['product']['name'] = '';
        $data['product']['stock_no'] = '';
        $data['product']['supplier_id'] = '';
        $data['product']['category_id'] = '';
        $data['product']['brand_id'] = '';
        $data['product']['taxes'] = [];

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
             ->seeJsonEquals($this->errorResponse([
                'product.name' => array(
                    Lang::get('core::validation.name.required')
                ),
                'product.stock_no' => array(
                    Lang::get('core::validation.stock.no.required')
                ),
                'product.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'product.category_id' => array(
                    Lang::get('core::validation.category.required')
                ),
                'product.brand_id' => array(
                    Lang::get('core::validation.brand.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_existing_brand_category_or_supplier_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['product']['supplier_id'] = 100;
        $data['product']['category_id'] = 100;
        $data['product']['brand_id'] = 100;

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
             ->seeJsonEquals($this->errorResponse([
                'product.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists')
                ),
                'product.category_id' => array(
                    Lang::get('core::validation.category.doesnt.exists')
                ),
                'product.brand_id' => array(
                    Lang::get('core::validation.brand.doesnt.exists')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_existing_product_name_or_stock_number_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['product']['name'] = $existing['product']->name;
        $data['product']['stock_no'] = $existing['product']->stock_no;

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'product.name' => array(
                    Lang::get('core::validation.name.unique')
                ),
                'product.stock_no' => array(
                    Lang::get('core::validation.stock.no.unique')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_empty_uom_id_or_qty_in_the_units_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['units'][0]['uom_id'] = '';
        $data['units'][0]['qty'] = '';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.required')
                    )
                ),
                'units.0.qty' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.qty.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_existing_uom_id_in_the_units_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['units'][0]['uom_id'] = 100;

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_numeric_qty_in_the_units_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['units'][0]['qty'] = 'A';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.qty' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.qty.numeric')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_already_existing_uom_id_of_the_product_in_the_units_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['units'][] = $data['units'][0];

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'units.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.already.exists')
                    )
                ),
                'units.1.uom_id' => array(
                    sprintf("%s: %s[2]. %s", 
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.already.exists')
                    )
                ),
            ]));
    }


    /**
     * @test
     */
    public function updating_the_product_with_an_empty_branch_id_purchase_price_selling_price_or_wholesale_price_in_the_prices_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['prices'][0]['branch_id'] = '';
        $data['prices'][0]['purchase_price'] = '';
        $data['prices'][0]['selling_price'] = '';
        $data['prices'][0]['wholesale_price'] = '';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.branch_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.branch.required')
                    )
                ),
                'prices.0.purchase_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.purchase.price.required')
                    )
                ),
                'prices.0.selling_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.selling.price.required')
                    )
                ),
                'prices.0.wholesale_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.wholesale.price.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_existing_branch_id_in_the_prices_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['prices'][0]['branch_id'] = 100;

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.branch_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.branch.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_numeric_purchase_selling_wholesale_or_price_a_to_e_in_the_prices_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['prices'][0]['purchase_price'] = 'A';
        $data['prices'][0]['selling_price'] = 'A';
        $data['prices'][0]['wholesale_price'] = 'A';
        $data['prices'][0]['price_a'] = 'A';
        $data['prices'][0]['price_b'] = 'A';
        $data['prices'][0]['price_c'] = 'A';
        $data['prices'][0]['price_d'] = 'A';
        $data['prices'][0]['price_e'] = 'A';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'prices.0.purchase_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.purchase.price.numeric')
                    )
                ),
                'prices.0.selling_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.selling.price.numeric')
                    )
                ),
                'prices.0.wholesale_price' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.wholesale.price.numeric')
                    )
                ),
                'prices.0.price_a' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'A'])
                    )
                ),
                'prices.0.price_b' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'B'])
                    )
                ),
                'prices.0.price_c' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'C'])
                    )
                ),
                'prices.0.price_d' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'D'])
                    )
                ),
                'prices.0.price_e' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.price.letter.numeric', ['letter' => 'E'])
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_empty_uom_id_or_code_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['barcodes'][0]['uom_id'] = '';
        $data['barcodes'][0]['code'] = '';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.required')
                    )
                ),
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.required')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_existing_uom_id_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['barcodes'][0]['uom_id'] = 100;

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.uom_id' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.uom.doesnt.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_existing_barcode_in_the_barcodes_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $existing = $this->item($created, 'whole');

        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['barcodes'][0]['code'] = $existing['barcodes'][0]['code'];

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
            ]));

        $data['barcodes'][] = $data['barcodes'][0];

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcodes.0.code' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
                'barcodes.1.code' => array(
                    sprintf("%s: %s[2]. %s", 
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.barcode.exists')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_non_numeric_min_or_max_in_the_locations_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['locations'][0]['min'] = 'A';
        $data['locations'][0]['max'] = 'A';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'locations.0.min' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.min.numeric')
                    )
                ),
                'locations.0.max' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.max.numeric')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_an_invalid_format_of_units_prices_branches_locations_or_alternatives_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['units'] = 'A';
        $data['locations'] = 'A';
        $data['prices'] = 'A';
        $data['branches'] = 'A';
        $data['barcodes'] = 'A';
        $data['alternatives'] = 'A';

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'units' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.product.unit'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'prices' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.branch.price'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'barcodes' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.product.unit.barcode'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'branches' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.branch'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'locations' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.location'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
                'alternatives' => array(
                    sprintf("%s: %s",
                        Lang::get('core::label.alternative.items'),
                        Lang::get('core::validation.invalid.format')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_min_greater_than_the_max_in_locations_section_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');

        $data['locations'][0]['min'] = 100;
        $data['locations'][0]['max'] = 1;
        
        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'locations.0.min' => array(
                    sprintf("%s: %s[1]. %s", 
                        Lang::get('core::label.location'),
                        Lang::get('core::error.row'),
                        Lang::get('core::validation.min.max.incorrect')
                    )
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_the_product_with_a_corret_product_format_will_successfully_save_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $data = $this->item($created, 'whole');
        
        $id = $created['product']->id;
        
        $expected = [
            'response' => [
                'product' => array_merge($this->item($created['product'], 'basic'))
            ],
            'database' => [
                'product' => array_except($data['product'], ['created', 'modified']),
                'tax' => [
                    'product_id' => $id,
                    'tax_id' => $data['taxes'][0]
                ],
                'unit' => array_merge($data['units'][0], ['product_id' => $id]),
                'price' => array_merge(array_except($data['prices'][0], ['branch_name']), ['product_id' => $id]),
                'branch' => array_merge(array_except($data['branches'][0], ['branch_name']), ['product_id' => $id]),
                'location' => array_merge(array_except($data['locations'][0], ['branch_name', 'branch_detail_name']), ['product_id' => $id]),
            ]
        ];

        $this->patchJson(sprintf("%s/%s", $this->uri, $created['product']->id), $data)
            ->seeJsonEquals($this->successfulResponse($expected['response'], Lang::get('core::success.updated')))
            ->seeInDatabase('product', $expected['database']['product'])
            ->seeInDatabase('product_tax', $expected['database']['tax'])
            ->seeInDatabase('product_unit', $expected['database']['unit'])
            ->seeInDatabase('branch_price', $expected['database']['price'])
            ->seeInDatabase('product_branch_info', $expected['database']['branch'])
            ->seeInDatabase('product_branch_detail', $expected['database']['location']);
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_product_will_trigger_an_error_response()
    {
        $this->deleteJson(sprintf("%s/1", $this->uri))
            ->seeJsonEquals($this->errorResponse(array(
                'delete.failed' => Lang::get('core::error.delete.failed')
            )));
    }

    /**
     * @test
     */
    public function deleting_a_product_will_successfully_soft_delete_it_in_the_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf("%s/%s", $this->uri, $created['product']->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('product', [
                'id' => $created['product']->id,
                'deleted_at' => null
            ]);
    }

    /**
     * @test
     */
    public function sending_an_empty_product_id_supplier_id_or_branch_id_upon_retrieving_the_list_prices_for_a_supplier_will_return_an_error_response()
    {
        $data = [];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'product_ids' => [
                    Lang::get('core::validation.product.ids.required')
                ],
                'supplier_id' => [
                    Lang::get('core::validation.supplier.required'),
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_supplier_id_or_branch_id_upon_retrieving_the_list_prices_for_a_supplier_will_return_an_error_response()
    {
        $data = [
            'supplier_id' => 100,
            'branch_id' => 100,
            'product_ids' => [1, 2]
        ];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'supplier_id' => [
                    Lang::get('core::validation.supplier.doesnt.exists')
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.doesnt.exists'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_product_ids_upon_retrieving_the_list_prices_for_a_supplier_will_return_an_error_response()
    {
        $data = [
            'supplier_id' => $this->references['suppliers'][rand(0, 2)]->id,
            'branch_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_supplier_price_while_historical_price_and_unit_is_not_enabled_will_return_its_purchase_price_with_the_first_unit_and_its_qty()
    {
        $this->genericSetting(['poi.price.based.on.history', 'poi.unit.based.on.history'], 'false');

        $created = $this->create();

        $data = [
            'supplier_id' => $this->references['suppliers'][rand(0, 2)]->id,
            'branch_id' => 1,
            'product_ids' => [$created['product']->id]
        ];

        $product = $created['product'];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $created['prices']->first()->number()->purchase_price,
                'price'        => $created['prices']->first()->number()->purchase_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_supplier_price_while_historical_unit_is_enabled_will_return_its_corresponding_historical_unit_with_its_qty()
    {
        $supplier = $this->references['suppliers'][rand(0, 2)]->id;
        $created = $this->create();
        $product = $created['product'];

        $this->genericSetting('poi.unit.based.on.history', 'true');

        $productSupplier = Factory::create($this->factory['supplier_price'], [
            'product_id' => $product->id,
            'supplier_id' => $supplier,
            'latest_unit_id' => $created['units']->first()->uom_id
        ]);

        $data = [
            'supplier_id' => $supplier,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$productSupplier->latest_unit_id],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                'options'      => $product->presenter()->unit_list,
                'value'        => $productSupplier->latest_unit_id
                ],
                'old_unit'     => [
                    'id'  => $productSupplier->latest_unit_id,
                    'qty' => $productSupplier->number()->latest_unit_qty
                ],
                'unit_qty'     => $productSupplier->number()->latest_unit_qty,
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $created['prices']->first()->number()->purchase_price,
                'price'        => $created['prices']->first()->number()->purchase_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_supplier_price_while_historical_price_is_enabled_will_return_its_corresponding_historical_purchase_price_with_a_default_unit_and_its_qty()
    {
        $supplier = $this->references['suppliers'][rand(0, 2)]->id;
        $created = $this->create();
        $product = $created['product'];

        $this->genericSetting('poi.price.based.on.history', 'true');
        
        $productSupplier = Factory::create($this->factory['supplier_price'], [
            'product_id' => $product->id,
            'supplier_id' => $supplier,
            'latest_unit_id' => $created['units']->first()->uom_id
        ]);

        $data = [
            'supplier_id' => $supplier,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $productSupplier->number()->latest_price,
                'price'        => $productSupplier->number()->latest_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/supplier/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function sending_an_empty_product_id_price_scheme_or_branch_id_upon_retrieving_the_list_of_product_prices_will_return_an_error_response()
    {
        $data = [];

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'product_ids' => [
                    Lang::get('core::validation.product.ids.required')
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.required'),
                ],
                'price' => [
                    Lang::get('core::validation.price.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_branch_id_upon_retrieving_the_list_of_product_prices_will_return_an_error_response()
    {
        $data = [
            'price' => 1,
            'branch_id' => 100,
            'product_ids' => [1, 2]
        ];

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'branch_id' => [
                    Lang::get('core::validation.branch.doesnt.exists'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_product_ids_upon_retrieving_the_list_of_product_prices_will_return_an_error_response()
    {
        $data = [
            'price' => 1,
            'branch_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function sending_a_price_scheme_that_is_not_within_the_range_of_choices_upon_retrieving_the_list_of_product_prices_will_return_an_error_response()
    {
        $data = [
            'price' => 12,
            'branch_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'price' => [
                    Lang::get('core::validation.price.scheme.invalid'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function sending_an_existing_product_id_branch_id_and_a_price_scheme_will_successfully_return_its_the_product_with_its_corresponding_price_based_on_the_price_scheme()
    {
        $created = $this->create();
        $product = $created['product'];
        $prices = $created['prices']->first();

        $data = [
            'price' => BranchPrice::PURCHASE,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $prices->number()->purchase_price,
                'price'        => $prices->number()->purchase_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::SELLING;

        $expected[0]['oprice'] = $prices->number()->selling_price;
        $expected[0]['price'] = $prices->number()->selling_price;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::WHOLESALE;

        $expected[0]['oprice'] = $prices->number()->wholesale_price;
        $expected[0]['price'] = $prices->number()->wholesale_price;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::A;

        $expected[0]['oprice'] = $prices->number()->price_a;
        $expected[0]['price'] = $prices->number()->price_a;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::B;

        $expected[0]['oprice'] = $prices->number()->price_b;
        $expected[0]['price'] = $prices->number()->price_b;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::C;

        $expected[0]['oprice'] = $prices->number()->price_c;
        $expected[0]['price'] = $prices->number()->price_c;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::D;

        $expected[0]['oprice'] = $prices->number()->price_d;
        $expected[0]['price'] = $prices->number()->price_d;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::E;

        $expected[0]['oprice'] = $prices->number()->price_e;
        $expected[0]['price'] = $prices->number()->price_e;

        $this->getJson(sprintf("%s/select/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function sending_an_empty_product_id_customer_id_or_branch_id_upon_retrieving_the_list_prices_for_a_regular_customer_will_return_an_error_response()
    {
        $data = [
            'customer_type' => Customer::REGULAR
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'product_ids' => [
                    Lang::get('core::validation.product.ids.required')
                ],
                'customer_id' => [
                    Lang::get('core::validation.customer.required'),
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_customer_id_or_branch_id_upon_retrieving_the_list_prices_for_a_regular_customer_will_return_an_error_response()
    {
        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => 100,
            'branch_id' => 100,
            'product_ids' => [1, 2]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'customer_id' => [
                    Lang::get('core::validation.customer.doesnt.exists')
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.doesnt.exists'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_product_ids_upon_retrieving_the_list_prices_for_a_regular_customer_will_return_an_error_response()
    {
        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $this->references['customers'][rand(0, 2)]->id,
            'branch_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_wholesale_will_return_the_wholesale_price_of_the_product_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::WHOLESALE);

        $created = $this->create();

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $this->references['customers'][rand(0, 2)]->id,
            'branch_id' => 1,
            'product_ids' => [$created['product']->id]
        ];

        $product = $created['product'];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $created['prices']->first()->number()->wholesale_price,
                'price'        => $created['prices']->first()->number()->wholesale_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_selling_will_return_the_selling_price_of_the_product_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::RETAIL);

        $created = $this->create();

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $this->references['customers'][rand(0, 2)]->id,
            'branch_id' => 1,
            'product_ids' => [$created['product']->id]
        ];

        $product = $created['product'];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                'options'      => $product->presenter()->unit_list,
                'value'        => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $created['prices']->first()->number()->selling_price,
                'price'        => $created['prices']->first()->number()->selling_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_custom_and_based_on_historical_and_while_there_are_no_historical_records_of_the_customer_will_return_the_set_default_price_scheme_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::CUSTOM);

        $customer = $this->references['customers'][rand(0, 2)];

        $created = $this->create();

        $product = $created['product'];

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::HISTORICAL,
                'historical_default_price' => 0
            ]);

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $customer->id,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $created['prices']->first()->number()->wholesale_price,
                'price'        => $created['prices']->first()->number()->wholesale_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::HISTORICAL,
                'historical_default_price' => 1
            ]);

        $expected[0]['oprice'] = $created['prices']->first()->number()->selling_price;
        $expected[0]['price'] = $created['prices']->first()->number()->selling_price;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_custom_and_based_on_historical_and_if_change_revert_is_set_as_off_will_return_the_latest_historical_price_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::CUSTOM);

        $customer = $this->references['customers'][rand(0, 2)];

        $created = $this->create();

        $product = $created['product'];

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::HISTORICAL,
                'historical_default_price' => 0,
                'historical_change_revert' => 0
            ]);

        $productCustomer = Factory::create($this->factory['customer_price'], [
            'product_id' => $product->id,
            'customer_id' => $customer->id,
            'latest_unit_id' => $created['units']->first()->uom_id
        ]);

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $customer->id,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $productCustomer->number()->latest_price,
                'price'        => $productCustomer->number()->latest_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_custom_and_based_on_historical_and_if_change_revert_is_set_as_on_will_return_the_latest_price_as_is_if_default_and_latest_price_are_the_same_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::CUSTOM);

        $customer = $this->references['customers'][rand(0, 2)];

        $created = $this->create();

        $product = $created['product'];

        $prices = $created['prices']->first();
        
        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::HISTORICAL,
                'historical_default_price' => 0,
                'historical_change_revert' => 1
            ]);

        $productCustomer = Factory::create($this->factory['customer_price'], [
            'product_id' => $product->id,
            'customer_id' => $customer->id,
            'latest_unit_id' => $created['units']->first()->uom_id,
            'latest_price' => $prices->wholesale_price
        ]);

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $customer->id,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $prices->number()->wholesale_price,
                'price'        => $prices->number()->wholesale_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_custom_and_based_on_historical_and_if_change_revert_is_set_as_on_will_return_the_price_as_zero_if_default_and_latest_price_are_not_the_same_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::CUSTOM);

        $customer = $this->references['customers'][rand(0, 2)];

        $created = $this->create();

        $product = $created['product'];

        $prices = $created['prices']->first();

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::HISTORICAL,
                'historical_default_price' => 0,
                'historical_change_revert' => 1
            ]);

        $productCustomer = Factory::create($this->factory['customer_price'], [
            'product_id' => $product->id,
            'customer_id' => $customer->id,
            'latest_unit_id' => $created['units']->first()->uom_id,
            'latest_price' => $prices->wholesale_price->add(Decimal::create(1))
        ]);

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $customer->id,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => number_format(0, $precision),
                'price'        => number_format(0, $precision),
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_regular_customer_price_while_customer_price_settings_is_set_as_custom_and_based_on_other_customer_settings_aside_from_historical_will_return_its_corresponding_price_with_a_default_unit_and_its_qty()
    {
        $this->genericSetting('soi.regular.customers.price.basis', CustomerPriceSettings::CUSTOM);

        $customer = $this->references['customers'][rand(0, 2)];

        $created = $this->create();

        $product = $created['product'];

        $prices = $created['prices']->first();

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::WHOLESALE]);

        $data = [
            'customer_type' => Customer::REGULAR,
            'customer_id' => $customer->id,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'old_unit'     => [
                    'id'  => $unit['id'],
                    'qty' => $unit['attribute']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $prices->number()->wholesale_price,
                'price'        => $prices->number()->wholesale_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::RETAIL]);

        $expected[0]['price'] = $prices->number()->selling_price;
        $expected[0]['oprice'] = $prices->number()->selling_price;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::PERCENT_PURCHASE,
                'pricing_rate' => 50
            ]);

        $price = Decimal::create(50, $precision)
            ->add(Decimal::create(100, $precision))
            ->div(Decimal::create(100, $precision))
            ->mul($prices->purchase_price, $precision);

        $expected[0]['price'] = number_format($price->innerValue(), $precision);
        $expected[0]['oprice'] = number_format($price->innerValue(), $precision);

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update([
                'pricing_type' => SellingPriceBasis::PERCENT_WHOLESALE,
                'pricing_rate' => 50
            ]);

        $price = Decimal::create(50, $precision)
            ->add(Decimal::create(100, $precision))
            ->div(Decimal::create(100, $precision))
            ->mul($prices->wholesale_price, $precision);

        $expected[0]['price'] = number_format($price->innerValue(), $precision);
        $expected[0]['oprice'] = number_format($price->innerValue(), $precision);

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::PRICE_A]);

        $expected[0]['price'] = $prices->number()->price_a;
        $expected[0]['oprice'] = $prices->number()->price_a;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::PRICE_B]);

        $expected[0]['price'] = $prices->number()->price_b;
        $expected[0]['oprice'] = $prices->number()->price_b;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::PRICE_C]);

        $expected[0]['price'] = $prices->number()->price_c;
        $expected[0]['oprice'] = $prices->number()->price_c;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::PRICE_D]);

        $expected[0]['price'] = $prices->number()->price_d;
        $expected[0]['oprice'] = $prices->number()->price_d;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        DB::table('customer')
            ->where('id', $customer->id)
            ->update(['pricing_type' => SellingPriceBasis::PRICE_E]);

        $expected[0]['price'] = $prices->number()->price_e;
        $expected[0]['oprice'] = $prices->number()->price_e;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function retrieving_the_list_of_products_with_its_corresponding_walk_in_customer_price_will_return_the_corresponding_prices_based_on_settings_with_a_default_unit_and_its_qty()
    {
        $created = $this->create();
        $product = $created['product'];
        $prices = $created['prices']->first();

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::WHOLESALE);

        $data = [
            'customer_type' => Customer::WALK_IN,
            'branch_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'serials'      => [],
                'batches'      => [],
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'total_qty'    => 0,
                'oprice'       => $prices->number()->wholesale_price,
                'price'        => $prices->number()->wholesale_price,
                'discount1'    => number_format(0, $precision),
                'discount2'    => number_format(0, $precision),
                'discount3'    => number_format(0, $precision),
                'discount4'    => number_format(0, $precision),
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::RETAIL);

        $expected[0]['price'] = $prices->number()->selling_price;
        $expected[0]['oprice'] = $prices->number()->selling_price;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::PRICE_A);

        $expected[0]['price'] = $prices->number()->price_a;
        $expected[0]['oprice'] = $prices->number()->price_a;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::PRICE_B);

        $expected[0]['price'] = $prices->number()->price_b;
        $expected[0]['oprice'] = $prices->number()->price_b;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::PRICE_C);

        $expected[0]['price'] = $prices->number()->price_c;
        $expected[0]['oprice'] = $prices->number()->price_c;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::PRICE_D);

        $expected[0]['price'] = $prices->number()->price_d;
        $expected[0]['oprice'] = $prices->number()->price_d;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $this->genericSetting('soi.walkin.customers.price.basis', WalkInPriceSettings::PRICE_E);

        $expected[0]['price'] = $prices->number()->price_e;
        $expected[0]['oprice'] = $prices->number()->price_e;

        $this->getJson(sprintf("%s/select/customer/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function sending_an_empty_product_id_price_scheme_branch_id_or_branch_detail_id_upon_retrieving_the_list_of_product_prices_with_inventory_will_return_an_error_response()
    {
        $data = [];

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'product_ids' => [
                    Lang::get('core::validation.product.ids.required')
                ],
                'branch_id' => [
                    Lang::get('core::validation.branch.required'),
                ],
                'branch_detail_id' => [
                    Lang::get('core::validation.for.location.required'),
                ],
                'price' => [
                    Lang::get('core::validation.price.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_branch_id_or_branch_detail_id_upon_retrieving_the_list_of_product_prices_with_inventory_will_return_an_error_response()
    {
        $data = [
            'price' => 1,
            'branch_id' => 100,
            'branch_detail_id' => 100,
            'product_ids' => [1, 2]
        ];

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'branch_id' => [
                    Lang::get('core::validation.branch.doesnt.exists'),
                ],
                'branch_detail_id' => [
                    Lang::get('core::validation.for.location.doesnt.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function sending_a_non_existing_product_ids_upon_retrieving_the_list_of_product_prices_with_inventory_will_return_an_error_response()
    {
        $data = [
            'price' => 1,
            'branch_id' => 1,
            'branch_detail_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function sending_a_price_scheme_that_is_not_within_the_range_of_choices_upon_retrieving_the_list_of_product_prices_with_inventory_will_return_an_error_response()
    {
        $data = [
            'price' => 12,
            'branch_id' => 1,
            'branch_detail_id' => 1,
            'product_ids' => [100, 200]
        ];

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'price' => [
                    Lang::get('core::validation.price.scheme.invalid'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function sending_an_existing_product_id_branch_id_branch_detail_id_and_a_price_scheme_will_successfully_return_its_the_product_with_its_corresponding_price_and_inventory_based_on_the_price_scheme()
    {
        $created = $this->create();
        $product = $created['product'];
        $prices = $created['prices']->first();

        $data = [
            'price' => BranchPrice::PURCHASE,
            'branch_id' => 1,
            'branch_detail_id' => 1,
            'product_ids' => [$product->id]
        ];

        $precision = setting('monetary.precision');

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            [
                'id'           => 0,
                'product_id'   => $product->id,
                'manage_type'  => $product->manage_type,
                'barcode_list' => $product->presenter()->barcode_list,
                'barcode'      => $product->presenter()->barcode_list[$unit['id']],
                'name'         => $product->name,
                'chinese_name' => $product->chinese_name,
                'uom'          => [
                    'options' => $product->presenter()->unit_list,
                    'value'   => $unit['id']
                ],
                'inventory'    => $created['inventory']->number()->qty,
                'unit_qty'     => $unit['attribute'],
                'qty'          => 0,
                'pc_qty'       => 0,
                'total_qty'    => 0,
                'diff_qty'     => 0,
                'price'        => $prices->number()->purchase_price,
                'amount'       => number_format(0, $precision),
                'remarks'      => ''
            ]
        ];

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::SELLING;

        $expected[0]['price'] = $prices->number()->selling_price;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::WHOLESALE;

        $expected[0]['price'] = $prices->number()->wholesale_price;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::A;

        $expected[0]['price'] = $prices->number()->price_a;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::B;

        $expected[0]['price'] = $prices->number()->price_b;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::C;

        $expected[0]['price'] = $prices->number()->price_c;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::D;

        $expected[0]['price'] = $prices->number()->price_d;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));

        $data['price'] = BranchPrice::E;

        $expected[0]['price'] = $prices->number()->price_e;

        $this->getJson(sprintf("%s/select/inventory/price", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'products' => $expected
            ]));
    }

    /**
     * @test
     */
    public function sending_an_empty_barcode_or_scheme_will_return_an_error_response()
    {
        $data = [];

        $this->getJson(sprintf("%s/find/by/barcode", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'barcode' => [
                    Lang::get('core::validation.barcode.required')
                ],
                'scheme' => [
                    Lang::get('core::validation.price.scheme.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_an_invalid_scheme_value_will_return_an_error_response()
    {
        $data = [
            'barcode' => $this->faker->numberBetween(1, 100),
            'scheme' => 4
        ];

        $this->getJson(sprintf("%s/find/by/barcode", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'scheme' => [
                    Lang::get('core::validation.price.scheme.invalid')
                ]
            ]));
    }

    /**
     * @test
     */
    public function finding_a_product_by_barcode_that_doesnt_exist_will_return_an_error_response()
    {
        $created = $this->create();

        $data = [
            'barcode' => ((int) $created['barcodes']->first()->code) + 1,
            'scheme' => 0
        ];

        $this->getJson(sprintf("%s/find/by/barcode", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function finding_a_product_by_barcode_that_exists_will_return_its_corresponding_prices_based_on_the_scheme()
    {
        $created = $this->create();

        $prices = $created['prices']->first();

        $precision = setting('monetary.precision');

        $product = $created['product'];

        $data = [
            'barcode' => $created['barcodes']->first()->code,
            'scheme' => PriceScheme::DEFAULT,
            'branch_id' => 1,
            'price' => BranchPrice::WHOLESALE
        ];

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            'id'           => 0,
            'product_id'   => $product->id,
            'serials'      => [],
            'batches'      => [],
            'manage_type'  => $product->manage_type,
            'barcode_list' => $product->presenter()->barcode_list,
            'barcode'      => $product->presenter()->barcode_list[$unit['id']],
            'name'         => $product->name,
            'chinese_name' => $product->chinese_name,
            'uom'          => [
                'options' => $product->presenter()->unit_list,
                'value'   => $unit['id']
            ],
            'unit_qty'     => $unit['attribute'],
            'qty'          => 0,
            'total_qty'    => 0,
            'oprice'       => $prices->number()->wholesale_price,
            'price'        => $prices->number()->wholesale_price,
            'discount1'    => number_format(0, $precision),
            'discount2'    => number_format(0, $precision),
            'discount3'    => number_format(0, $precision),
            'discount4'    => number_format(0, $precision),
            'amount'       => number_format(0, $precision),
            'remarks'      => ''
        ];

        $this->getJson(sprintf("%s/find/by/barcode", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'product' => $expected
            ]));
    }

    /**
     * @test
     */
    public function visiting_the_report_page_of_the_product_will_successfully_return_the_page_with_branches_reference()
    {
        $created = $this->create();

        $this->visit(sprintf("%s/%s/report", $this->uri, $created['product']->id))
            ->assertResponseOk()
            ->assertViewHas('branches');
    }

    /**
     * @test
     */
    public function accessing_the_transaction_summary_routes_will_return_zero_values_of_the_transaction_summary_data_of_the_product_if_no_transactions_have_been_made()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $expected = [
            'summary' => [
                'purchase_inbound' => number_format(0, $precision),
                'stock_delivery_inbound' => number_format(0, $precision),
                'stock_return_inbound' => number_format(0, $precision),
                'sales_return_inbound' => number_format(0, $precision),
                'adjustment_increase' => number_format(0, $precision),
                'purchase_return_outbound' => number_format(0, $precision),
                'stock_delivery_outbound' => number_format(0, $precision),
                'stock_return_outbound' => number_format(0, $precision),
                'sales_outbound' => number_format(0, $precision),
                'damage_outbound' => number_format(0, $precision),
                'adjustment_decrease' => number_format(0, $precision),
                'beginning' => number_format(0, $precision)
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/summary", $this->uri, $created['product']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_the_transaction_summary_routes_will_the_transaction_summary_data_of_the_product()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $summary = Factory::create($this->factory['transaction_summary'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1
        ]);

        $expected = [
            'summary' => [
                'purchase_inbound' => $summary->number()->purchase_inbound,
                'stock_delivery_inbound' => $summary->number()->stock_delivery_inbound,
                'stock_return_inbound' => $summary->number()->stock_return_inbound,
                'sales_return_inbound' => $summary->number()->sales_return_inbound,
                'adjustment_increase' => $summary->number()->adjustment_increase,
                'purchase_return_outbound' => $summary->number()->purchase_return_outbound,
                'stock_delivery_outbound' => $summary->number()->stock_delivery_outbound,
                'stock_return_outbound' => $summary->number()->stock_return_outbound,
                'sales_outbound' => $summary->number()->sales_outbound,
                'damage_outbound' => $summary->number()->damage_outbound,
                'adjustment_decrease' => $summary->number()->adjustment_decrease,
                'beginning' => number_format(0, $precision)
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/summary", $this->uri, $created['product']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function sending_a_date_limit_upon_accessing_the_transaction_summmary_routes_will_return_the_transaction_summary_starting_from_the_given_date_up_to_the_present_and_return_the_inventory_value_before_the_given_date_as_the_beginning()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $previous = Factory::create($this->factory['transaction_summary'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'transaction_date' => Carbon::yesterday()->toDateString()
        ]);

        $summary = Factory::create($this->factory['transaction_summary'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'transaction_date' => Carbon::now()->toDateString()
        ]);

        $expected = [
            'summary' => [
                'purchase_inbound' => $summary->number()->purchase_inbound,
                'stock_delivery_inbound' => $summary->number()->stock_delivery_inbound,
                'stock_return_inbound' => $summary->number()->stock_return_inbound,
                'sales_return_inbound' => $summary->number()->sales_return_inbound,
                'adjustment_increase' => $summary->number()->adjustment_increase,
                'purchase_return_outbound' => $summary->number()->purchase_return_outbound,
                'stock_delivery_outbound' => $summary->number()->stock_delivery_outbound,
                'stock_return_outbound' => $summary->number()->stock_return_outbound,
                'sales_outbound' => $summary->number()->sales_outbound,
                'damage_outbound' => $summary->number()->damage_outbound,
                'adjustment_decrease' => $summary->number()->adjustment_decrease,
                'beginning' => $previous->number()->total
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/summary", $this->uri, $created['product']->id), 
            [
                'filters' => array(
                    array(
                        'value' => Carbon::now()->toDateString(),
                        'operator' => '>=',
                        'join' => 'AND' ,
                        'column' => 'transaction_date'
                    )
                )
            ])
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_the_stock_card_routes_with_no_current_transaction_will_return_an_empty_data()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $expected = [
            'stock_card' => [
                'data' => [],
                'meta' => [
                    'pagination' => [
                        'total' => 0,
                        'count' => 0,
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    ]
                ]
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/stock-card", $this->uri, $created['product']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_the_stock_card_routes_will_return_all_the_stock_card_data_of_the_product()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $card = Factory::create($this->factory['stock_card'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'transaction_date' => Carbon::now(),
            'reference_type' => TransactionSummary::DAMAGE_OUTBOUND,
            'reference_id' => 'factory:damage_outbound'
        ]);

        $expected = [
            'stock_card' => [
                'data' => [
                    [
                        'audited_date'     => $card->created_at->toDateTimeString(),
                        'created_for'      => $card->branch->name,
                        'for_location'     => $card->location->name,
                        'product_id'       => $card->product->name,
                        'sequence_no'      => $card->sequence_no,
                        'reference_type'   => $card->presenter()->reference_name,
                        'transaction_date' => $card->transaction_date->toDateTimeString(),
                        'qty'              => $card->number()->qty,
                        'price'            => $card->number()->price,
                        'amount'           => $card->number()->amount,
                        'cost'             => $card->number()->cost,
                        'pre_inventory'    => $card->number()->pre_inventory,
                        'pre_avg_cost'     => $card->number()->pre_avg_cost,
                        'pre_amount'       => $card->number()->pre_amount,
                        'post_inventory'   => $card->number()->post_inventory,
                        'post_avg_cost'    => $card->number()->post_avg_cost,
                        'post_amount'      => $card->number()->post_amount
                    ]
                ],
                'meta' => [
                    'pagination' => [
                        'total' => 1,
                        'count' => 1,
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    ]
                ]
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/stock-card", $this->uri, $created['product']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_the_stock_card_routes_and_sending_a_date_filter_will_return_stock_card_data_of_the_product_starting_from_the_indicated_date_filter()
    {
        $created = $this->create();

        $precision = setting('monetary.precision');

        $previous = Factory::create($this->factory['stock_card'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'transaction_date' => Carbon::yesterday(),
            'reference_type' => TransactionSummary::DAMAGE_OUTBOUND,
            'reference_id' => 'factory:damage_outbound'
        ]);

        $card = Factory::create($this->factory['stock_card'], [
            'product_id' => $created['product']->id,
            'created_for' => 1,
            'for_location' => 1,
            'transaction_date' => Carbon::now(),
            'reference_type' => TransactionSummary::DAMAGE_OUTBOUND,
            'reference_id' => 'factory:damage_outbound'
        ]);

        $expected = [
            'stock_card' => [
                'data' => [
                    [
                        'audited_date'     => $card->created_at->toDateTimeString(),
                        'created_for'      => $card->branch->name,
                        'for_location'     => $card->location->name,
                        'product_id'       => $card->product->name,
                        'sequence_no'      => $card->sequence_no,
                        'reference_type'   => $card->presenter()->reference_name,
                        'transaction_date' => $card->transaction_date->toDateTimeString(),
                        'qty'              => $card->number()->qty,
                        'price'            => $card->number()->price,
                        'amount'           => $card->number()->amount,
                        'cost'             => $card->number()->cost,
                        'pre_inventory'    => $card->number()->pre_inventory,
                        'pre_avg_cost'     => $card->number()->pre_avg_cost,
                        'pre_amount'       => $card->number()->pre_amount,
                        'post_inventory'   => $card->number()->post_inventory,
                        'post_avg_cost'    => $card->number()->post_avg_cost,
                        'post_amount'      => $card->number()->post_amount
                    ]
                ],
                'meta' => [
                    'pagination' => [
                        'total' => 1,
                        'count' => 1,
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    ]
                ]
            ]
        ];

        $this->getJson(sprintf("%s/%s/report/stock-card", $this->uri, $created['product']->id), [
                'filters' => array(
                    array(
                        'value' => Carbon::now()->toDateString(),
                        'operator' => '>=',
                        'join' => 'AND' ,
                        'column' => 'transaction_date'
                    )
                )
            ])
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function accessing_component_route_with_group_id_will_return_components()
    {
        $grouping = $this->createProductGrouping();

        $expected = [
            'components' => [[
                'barcode' => $grouping['component']->barcode,
                'component_id' => $grouping['component']->id,
                'name' => $grouping['component']->name,
                'required_qty' => $grouping['productComponent']->number()->qty,
                'stock_number' => $grouping['component']->stock_no
            ]]
        ];

        $this->getJson(sprintf("%s/%s/components", $this->uri, $grouping['grouping']->id))
            ->seeJsonEquals($this->successfulResponse($expected));
    }

    /**
     * @test
     */
    public function sending_an_empty_stock_no_or_scheme_in_find_by_stock_no_will_return_an_error_response()
    {
        $data = [];

        $this->getJson(sprintf("%s/find/by/stock-no", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'stock_no' => [
                    Lang::get('core::validation.stock.no.required')
                ],
                'scheme' => [
                    Lang::get('core::validation.price.scheme.required')
                ]
            ]));
    }

    /**
     * @test
     */
    public function sending_an_invalid_scheme_value_in_find_by_stock_no_will_return_an_error_response()
    {
        $data = [
            'stock_no' => $this->faker->numberBetween(1, 100),
            'scheme' => 4
        ];

        $this->getJson(sprintf("%s/find/by/stock-no", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'scheme' => [
                    Lang::get('core::validation.price.scheme.invalid')
                ]
            ]));
    }

    /**
     * @test
     */
    public function finding_a_product_by_stock_no_that_doesnt_exist_will_return_an_error_response()
    {
        $created = $this->create();

        $data = [
            'stock_no' => ((int) $created['product']->stock_no) + 1,
            'scheme' => 0
        ];

        $this->getJson(sprintf("%s/find/by/stock-no", $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'no.products.found' => Lang::get('core::error.no.products.found')
            ]));
    }

    /**
     * @test
     */
    public function finding_a_product_by_stock_no_that_exists_will_return_its_corresponding_prices_based_on_the_scheme()
    {
        $created = $this->create();

        $prices = $created['prices']->first();

        $precision = setting('monetary.precision');

        $product = $created['product'];

        $data = [
            'stock_no' => $created['product']->first()->stock_no,
            'scheme' => PriceScheme::DEFAULT,
            'branch_id' => 1,
            'price' => BranchPrice::WHOLESALE
        ];

        $unit = $product->presenter()->unit_list[0];

        $expected = [
            'id'           => 0,
            'product_id'   => $product->id,
            'serials'      => [],
            'batches'      => [],
            'manage_type'  => $product->manage_type,
            'barcode_list' => $product->presenter()->barcode_list,
            'barcode'      => $product->presenter()->barcode_list[$unit['id']],
            'name'         => $product->name,
            'chinese_name' => $product->chinese_name,
            'uom'          => [
                'options' => $product->presenter()->unit_list,
                'value'   => $unit['id']
            ],
            'unit_qty'     => $unit['attribute'],
            'qty'          => 0,
            'total_qty'    => 0,
            'oprice'       => $prices->number()->wholesale_price,
            'price'        => $prices->number()->wholesale_price,
            'discount1'    => number_format(0, $precision),
            'discount2'    => number_format(0, $precision),
            'discount3'    => number_format(0, $precision),
            'discount4'    => number_format(0, $precision),
            'amount'       => number_format(0, $precision),
            'remarks'      => ''
        ];

        $this->getJson(sprintf("%s/find/by/stock-no", $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse([
                'product' => $expected
            ]));
    }

    private function createReferences()
    {
        $this->references = [
            'uoms'       => Factory::times(3)->create('uom'),
            'categories' => Factory::times(3)->create('category'),
            'suppliers'  => Factory::times(3)->create('supplier'),
            'taxes'      => Factory::times(3)->create('tax'),
            'brands'     => Factory::times(3)->create('brand'),
            'customers'  => Factory::times(3)->create('customer'),
        ];
    }

    private function create($data = array())
    {
        $product = Factory::create($this->factory['product'], array_merge(array(
            'supplier_id' => $this->references['suppliers'][rand(0, 2)]->id,
            'brand_id' => $this->references['brands'][rand(0, 2)]->id,
            'category_id' => $this->references['categories'][rand(0, 2)]->id,
        ), $data['product'] ?? []));

        $tax = [
            'product_id' => $product->id,
            'tax_id' => $this->references['taxes'][rand(0, 2)]->id
        ];

        DB::table('product_tax')->insert($tax);

        $uom = $this->references['uoms'][rand(0, 2)];

        $unit = Factory::create($this->factory['unit'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['unit'] ?? []));

        $barcode = Factory::create($this->factory['barcode'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['barcode'] ?? []));

        $price = Factory::create($this->factory['price'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['price'] ?? []));

        $branch = Factory::create($this->factory['branch'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['branch'] ?? []));

        $location = Factory::create($this->factory['location'], array_merge(array(
            'product_id' => $product->id,
            'branch_detail_id' => 1,
        ), $data['location'] ?? []));

        $inventory = Factory::create($this->factory['inventory'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
            'branch_detail_id' => 1,
        ), $data['inventory'] ?? []));

        return [
            'product'      => $product->fresh(),
            'taxes'        => collect([$tax]),
            'units'        => collect([$unit]),
            'barcodes'     => collect([$barcode]),
            'prices'       => collect([$price]),
            'branches'     => collect([$branch]),
            'locations'    => collect([$location]),
            'inventory'    => $inventory,
            'alternatives' => []
        ];
    }

    private function build($data = array())
    {
        $product = Factory::build($this->factory['product'], array_merge(array(
            'supplier_id' => $this->references['suppliers'][rand(0, 2)]->id,
            'brand_id' => $this->references['brands'][rand(0, 2)]->id,
            'category_id' => $this->references['categories'][rand(0, 2)]->id,
        ), $data['product'] ?? []));

        $tax = [
            'product_id' => $product->id,
            'tax_id' => $this->references['taxes'][rand(0, 2)]->id
        ];

        $uom = $this->references['uoms'][rand(0, 2)];

        $unit = Factory::build($this->factory['unit'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['unit'] ?? []));

        $barcode = Factory::build($this->factory['barcode'], array_merge(array(
            'product_id' => $product->id,
            'uom_id' => $uom->id,
        ), $data['barcode'] ?? []));

        $price = Factory::build($this->factory['price'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['price'] ?? []));

        $branch = Factory::build($this->factory['branch'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
        ), $data['branch'] ?? []));

        $location = Factory::build($this->factory['location'], array_merge(array(
            'product_id' => $product->id,
            'branch_detail_id' => 1,
        ), $data['location'] ?? []));

        $inventory = Factory::build($this->factory['inventory'], array_merge(array(
            'product_id' => $product->id,
            'branch_id' => 1,
            'branch_detail_id' => 1,
        ), $data['inventory'] ?? []));

        return [
            'product'      => $product,
            'taxes'        => collect([$tax]),
            'units'        => collect([$unit]),
            'barcodes'     => collect([$barcode]),
            'prices'       => collect([$price]),
            'branches'     => collect([$branch]),
            'locations'    => collect([$location]),
            'inventory'    => $inventory,
            'alternatives' => []
        ];
    }

    /**
     * Build a collection of transaction
     * 
     * @param  int  $time  
     * @return array        
     */
    private function collection($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    protected function list($array)
    {
        $model = $array['product'];

        return [
            'id'                    => $model->id,
            'stock_no'              => $model->stock_no,
            'supplier_sku'          => $model->supplier_sku,
            'description'           => $model->description ?? '',
            'name'                  => $model->name,
            'chinese_name'          => $model->chinese_name ?? '',
            'memo'                  => $model->memo ?? '',
            'barcode'               => $model->barcodes()->first()->code,
            'category'              => $model->category->name,
            'brand'                 => $model->brand->name,
            'supplier'              => $model->supplier->full_name,
            'purchase_price'        => $model->prices->first()->number()->purchase_price,
            'selling_price'         => $model->prices->first()->number()->selling_price,
            'wholesale_price'       => $model->prices->first()->number()->wholesale_price,
            'price_a'               => $model->prices->first()->number()->price_a,
            'price_b'               => $model->prices->first()->number()->price_b,
            'price_c'               => $model->prices->first()->number()->price_c,
            'price_d'               => $model->prices->first()->number()->price_d,
            'price_e'               => $model->prices->first()->number()->price_e,
            'inventory'             => $array['inventory']->number()->qty,
            'group_type' => $model->group_type,
            'group_type_label' => $model->presenter()->group_type_label
        ];
    }

    protected function whole($array)
    {
        return [
            'product'      => $this->item($array['product'], 'product'),
            'prices'       => $this->collect($array['prices'], 'prices'),
            'units'        => $this->collect($array['units'], 'units'),
            'barcodes'     => $this->collect($array['barcodes'], 'barcodes'),
            'branches'     => $this->collect($array['branches'], 'branches'),
            'locations'    => $this->collect($array['locations'], 'locations'),
            'taxes'        => $array['taxes']->pluck('tax_id')->toArray(),
            'alternatives' => $this->collect($array['alternatives'], 'basic'),
        ];
    }

    protected function prices($model)
    {
        return [
            'branch_id'         => $model->branch_id,
            'branch_name'       => $model->branch->name,
            'purchase_price'    => $model->number()->purchase_price,
            'selling_price'     => $model->number()->selling_price,
            'wholesale_price'   => $model->number()->wholesale_price,
            'price_a'           => $model->number()->price_a,
            'price_b'           => $model->number()->price_b,
            'price_c'           => $model->number()->price_c,
            'price_d'           => $model->number()->price_d,
            'price_e'           => $model->number()->price_e,
            'editable'          => $model->editable
        ];
    }

    protected function units($model)
    {
        return [
            'uom_id'        => $model->uom_id,
            'qty'           => $model->number()->qty,
            'length'        => $model->number()->length,
            'width'         => $model->number()->width,
            'height'        => $model->number()->height,
            'weight'        => $model->number()->weight,
            'default'       => $model->default ?? 0
        ];
    }

    protected function barcodes($model)
    {
        return [
            'uom_id'     => $model->uom_id,
            'code'       => $model->code,
            'memo'       => $model->memo ?? ''
        ];
    }

    protected function branches($model)
    {
        return [
            'branch_id'         => $model->branch_id,
            'branch_name'       => $model->branch->name ?? '',
            'status'            => $model->status
        ];
    }

    protected function locations($model)
    {
        return [
            'branch_name'        => $model->location->branch->name ?? '',
            'branch_detail_id'   => $model->branch_detail_id,
            'branch_detail_name' => $model->location->name ?? '',
            'min'                => $model->number()->min,
            'max'                => $model->number()->max
        ];
    }

    protected function product($model)
    {
        return [
            'id'            => $model->id ?? 0,
            'stock_no'      => $model->stock_no,
            'supplier_sku'  => $model->supplier_sku,
            'name'          => $model->name,
            'description'   => $model->description ?? '',
            'chinese_name'  => $model->chinese_name ?? '',
            'supplier_id'   => $model->supplier_id,
            'brand_id'      => $model->brand_id,
            'category_id'   => $model->category_id,
            'senior'        => $model->senior,
            'status'        => $model->status,
            'memo'          => $model->memo ?? '',
            'manage_type'   => $model->manage_type,
            'group_type'    => $model->group_type,
            'manageable'     => $model->manageable,
            'created'       => $model->presenter()->created_by_and_date,
            'modified'      => $model->presenter()->modified_by_and_date,
        ];
    }

    protected function basic($model)
    {
        return [
            'id'                    => $model->id,
            'stock_no'              => $model->stock_no,
            'supplier_sku'          => $model->supplier_sku,
            'description'           => $model->description ?? '',
            'name'                  => $model->name,
            'chinese_name'          => $model->chinese_name ?? '',
            'memo'                  => $model->memo ?? ''
        ];
    }

    protected function createProductGrouping($grouping = array(), $component = array(), $productComponent = array())
    {
        $grouping = Factory::create('product_group', $grouping);
        $component = Factory::create('component', $component);

        $productComponent = Factory::create('product_component', 
            array_merge([
                'group_id' => $grouping->id,
                'component_id' => $component->id,
            ], 
            $productComponent));

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $grouping->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $component->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        return [
            'grouping' => $grouping,
            'component' => $component,
            'productComponent' => $productComponent
        ];
    }
}