<?php

use Laracasts\TestDummy\Factory;

class SupplierControllerTest extends FunctionalTest
{
    private $uri = '/supplier';
    private $factory = 'supplier';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_supplier_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_supplier_page()
    {
        $this->visit($this->uri)
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_code_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->code,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'code'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'suppliers' => array(
                $this->item($data[2], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_full_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->full_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'full_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'suppliers' => array(
                $this->item($data[0], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_chinese_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->chinese_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'chinese_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'suppliers' => array(
                $this->item($data[1], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_memo_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]->memo,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'memo'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'suppliers' => array(
                $this->item($data[3], 'basicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1', 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'id' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'suppliers' => $this->collect($data, 'basicTransformer')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $this->faker->name, 
                    'operator' => '=', 
                    'join' => 'OR' , 
                    'column' => 'code' 
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [] 
                )
            ),
            'suppliers' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_code_full_name_status_or_term_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['term'] = '';
        $data['code'] = '';
        $data['full_name'] = '';
        $data['status'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'term' => array(
                    Lang::get('core::validation.term.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_code_or_full_name_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'code' => $created->code,
            'full_name' => $created->full_name,
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_status_or_term_will_trigger_an_error_response()
    {
        $build = $this->build();
        $data = $this->toArray($build);

        $data['term'] = $this->faker->name;
        $data['status'] = $this->faker->name;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
                'term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_email_format_will_trigger_an_error_response()
    {
        $build = $this->build(array(
            'email' => $this->faker->name
        ));

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_a_proper_format_of_data_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array(
                'supplier' => array_merge(
                    $this->item($build, 'basicTransformer'),
                    array('id' => 1)
                )
            ),
            'database' => array_merge(
                $data,
                array('id' => 1)
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'],
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('supplier', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_code_full_name_status_or_term_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['term'] = '';
        $data['code'] = '';
        $data['full_name'] = '';
        $data['status'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.required')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.required')
                ),
                'status' => array(
                    Lang::get('core::validation.status.required')
                ),
                'term' => array(
                    Lang::get('core::validation.term.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_code_or_full_name_will_trigger_an_error_response()
    {
        $existing = $this->create();

        $created = $this->create();

        $data = $this->toArray($created);

        $data['code'] = $existing->code;
        $data['full_name'] = $existing->full_name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'code' => array(
                    Lang::get('core::validation.code.unique')
                ),
                'full_name' => array(
                    Lang::get('core::validation.full.name.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/5', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('supplier', [
                'id' => 5
            ]);
    }

    /**
     * @test
     */
    public function updating_an_incorrect_status_or_term_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['status'] = $this->faker->name;
        $data['term'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'status' => array(
                    Lang::get('core::validation.status.numeric')
                ),
                'term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_an_incorrect_email_format_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = $this->toArray($created);

        $data['email'] = $this->faker->name;

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'email' => array(
                    Lang::get('core::validation.email.invalid')
                ),
            ]));
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.deleted')))
            ->notSeeInDatabase('supplier', array(
                    'id' => $created->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {   
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('supplier', array(
                    'id' => 1,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function accessing_supplier_will_return_the_full_info_of_the_supplier()
    {
        $created = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([
                'supplier' => $this->item($created, 'fullInfoTransformer') 
            ]));
    }

    /**
     * @test
     */
    public function accessing_a_non_existing_supplier_will_return_an_error()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'show.failed' => Lang::get('core::error.show.failed') 
            ]));
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, $data);
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, $data);
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory);
    }

    protected function basicTransformer($model)
    {
        return [
            'id'           => $model->id,
            'code'         => $model->code,
            'full_name'    => $model->full_name,
            'chinese_name' => $model->chinese_name,
            'contact'      => $model->contact
        ];
    }

    protected function fullInfoTransformer($model)
    {
        return [
            'id'             => $model->id,
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'contact'        => $model->contact,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'telephone'      => $model->telephone,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'term'           => $model->term,
            'status'         => $model->status,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'owner_name'     => $model->owner_name
        ];
    }

    protected function toArray($model)
    {
        return [
            'code'           => $model->code,
            'full_name'      => $model->full_name,
            'chinese_name'   => $model->chinese_name,
            'owner_name'     => $model->owner_name,
            'contact'        => $model->contact,
            'address'        => $model->address,
            'landline'       => $model->landline,
            'fax'            => $model->fax,
            'mobile'         => $model->mobile,
            'email'          => $model->email,
            'website'        => $model->website,
            'term'           => $model->term,
            'contact_person' => $model->contact_person,
            'memo'           => $model->memo,
            'status'         => $model->status,
        ];
    }
}
