<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Entities\UserDefinedField;
use Modules\Core\Enums\ModuleDefinedFields;
use Modules\Core\Enums\UserDefinedFieldType;
use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class UDFControllerTest extends FunctionalTest
{
    private $uri = '/udf';
    private $factory = 'udf';

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
    }

    /**
     * @test
     */
    public function accessing_udf_route_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function visiting_the_index_url_will_display_the_list_of_udfs_based_on_the_modules_selected()
    {
        $created = $this->create(['module_id' => ModuleDefinedFields::PURCHASE]);

        $expected = [
            'udfs' => [
                $this->item($created, 'basic')
            ],
            'meta' => [
                'pagination' => [
                    'count' => 1,
                    'current_page' => 1,
                    'links' => [],
                    'per_page' => 10,
                    'total' => 1,
                    'total_pages' => 1
                ]
            ]
        ];

        $this->getJson($this->uri, [
            'module' => ModuleDefinedFields::PURCHASE,
            'per_page' => 10
        ])->seeJsonEquals($this->successfulResponse($expected, ''));
    }


    /**
     * @test
     */
    public function saving_an_empty_label_type_visible_or_module_id_will_trigger_an_error_response()
    {
        $build = $this->build(['label' => '']);

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => '',
            'type' => '',
            'visible' => ''
        ]), ['id', 'alias']);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'label' => [
                    Lang::get('core::validation.label.required')
                ],
                'module_id' => [
                    Lang::get('core::validation.module.required')
                ],
                'type' => [
                    Lang::get('core::validation.type.required'),
                ],
                'visible' => [
                    Lang::get('core::validation.visible.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function saving_a_module_id_that_doesnt_exist_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => 100
        ]), ['id', 'alias']);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'module_id' => [
                    Lang::get('core::validation.module.doesnt.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function saving_an_already_existing_label_will_trigger_an_error_response()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $build = $this->build(['label' => $created->label]);

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE
        ]), ['id', 'alias']);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'label' => [
                    Lang::get('core::validation.label.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_type_will_trigger_an_error_response()
    {
        $build = $this->build([
            'type' => 100,
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE
        ]), ['id', 'alias']);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'type' => [
                    Lang::get('core::validation.type.doesnt.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_non_boolean_visible_will_trigger_an_error_response()
    {
        $build = $this->build([
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE,
            'visible' => 'A'
        ]), ['id', 'alias']);
        
        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'visible' => [
                    Lang::get('core::validation.visible.invalid.value'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function saving_a_valid_udf_will_successfully_save_the_data_in_database_generate_an_alias_and_return_a_successful_response()
    {
        $build = $this->build();

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE
        ]), ['id', 'alias']);

        $expected = [
            'response' => [
                'udf' => array_merge($this->item($build, 'basic'), [
                    'id' => 1
                ])
            ],
            'database' => array_merge($this->item($build, 'basic'), [
                'id' => 1,
                'module_id' => ModuleDefinedFields::PURCHASE
            ])
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('user_defined_fields', $expected['database']);
    }

    /**
     * @test
     */
    public function saving_an_already_existing_label_but_within_a_different_module_will_successfully_save_the_udf_and_return_a_successful_response()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE
        ]);

        $build = $this->build(['label' => $created->label]);

        $build->alias = str_slug('udf_'.$build->label.'_'.Carbon::now()->toDateString(), '_');

        $data = array_except(array_merge($this->item($build, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE
        ]), ['id', 'alias']);

        $expected = [
            'response' => [
                'udf' => array_merge($this->item($build, 'basic'), [
                    'id' => 2
                ])
            ],
            'database' => array_merge($this->item($build, 'basic'), [
                'id' => 2,
                'module_id' => ModuleDefinedFields::PURCHASE
            ])
        ];

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.created')
            ))
            ->seeInDatabase('user_defined_fields', $expected['database']);
    }

    /**
     * @test
     */
    public function saving_a_new_udf_will_automatically_create_a_new_field_in_the_specified_module()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::INTR
        ]);

        $this->assertTableHasColumn($created->table, $created->alias)
            ->assertTableColumnIsOfType($created->table, $created->alias, 'integer');
    }

    /**
     * @test
     */
    public function updating_the_udf_with_an_empty_label_or_module_id_will_trigger_an_error_response()
    {
        $created = $this->create(['label' => '']);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => '',
            'module_id' => '',
            'type' => '',
            'visible' => ''
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'label' => [
                    Lang::get('core::validation.label.required')
                ],
                'module_id' => [
                    Lang::get('core::validation.module.required')
                ],
                'type' => [
                    Lang::get('core::validation.type.required'),
                ],
                'visible' => [
                    Lang::get('core::validation.visible.required'),
                ]
            ]));
    }

    /**
     * @test
     */
    public function updating_the_udf_with_a_module_id_that_doesnt_exist_will_trigger_an_error_response()
    {
        $created = $this->create();

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => 100
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'module_id' => [
                    Lang::get('core::validation.module.doesnt.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_the_udf_with_an_already_existing_label_will_trigger_an_error_response()
    {
        $existing = $this->create([
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $created = $this->create([
            'label' => $existing->label,
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'label' => [
                    Lang::get('core::validation.label.exists')
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_udf_with_a_non_existing_type_will_trigger_an_error_response()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE,
            'type' => 100
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'type' => [
                    Lang::get('core::validation.type.doesnt.exists'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_a_udf_with_a_non_boolean_visible_will_trigger_an_error_response()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::PURCHASE
        ]);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE,
            'visible' => 'A'
        ]), ['id', 'alias']);

        
        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'visible' => [
                    Lang::get('core::validation.visible.invalid.value'),
                ],
            ]));
    }

    /**
     * @test
     */
    public function updating_the_udf_with_a_valid_data_will_successfully_update_the_data_in_database_and_alias_would_not_be_updated_and_return_a_successful_response()
    {
        $created = $this->create();

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE,
            'label' => $this->faker->word()
        ]), ['id', 'alias']);

        $expected = [
            'response' => [
                'udf' => array_merge($this->item($created, 'basic'), [
                    'label' => $data['label']
                ])
            ],
            'database' => array_merge($this->item($created, 'basic'), [
                'label' => $data['label']
            ])
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase('user_defined_fields', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_an_already_existing_label_but_within_a_different_module_will_successfully_update_the_udf_and_return_a_successful_response()
    {
        $existing = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE
        ]);

        $created = $this->create();

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::PURCHASE,
            'label' => $existing->label
        ]), ['id', 'alias']);

        $expected = [
            'response' => [
                'udf' => array_merge($this->item($created, 'basic'), [
                    'label' => $existing->label
                ])
            ],
            'database' => array_merge($this->item($created, 'basic'), [
                'label' => $existing->label
            ])
        ];

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse(
                $expected['response'], 
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase('user_defined_fields', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_the_udf_label_will_not_generate_a_new_field_and_will_not_change_the_previous_field_name_created()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::INTR
        ]);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::DAMAGE,
            'label' => $this->faker->word()
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeInDatabase('user_defined_fields', [
                'id' => $created->id,
                'alias' => $created->alias,
                'label' => $data['label']
            ])
            ->assertTableHasColumn($created->table, $created->alias)
            ->assertTableColumnIsOfType($created->table, $created->alias, 'integer');
    }

    /**
     * @test
     */
    public function deleting_the_a_non_existing_id_will_return_an_error_response()
    {
        $this->deleteJson(sprintf('%s/%s', $this->uri, 1))
            ->seeJsonEquals($this->errorResponse(array(
                'udf.doesnt.exists' => Lang::get('core::validation.udf.doesnt.exists')
            )));
    }

    /**
     * @test
     */
    public function deleting_the_specified_udf_id_will_successfully_delete_the_data_in_database_remove_the_column_in_the_specified_module_and_return_a_successful_response()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE
        ]);

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.deleted')))
            ->notSeeInDatabase('user_defined_fields', array(
                'id' => $created->id
            ))
            ->assertTableHasNoColumn($created->table, $created->alias);
    }

    /**
     * @test
     */
    public function udf_cannot_be_deleted_if_the_specified_modules_already_has_a_data()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE
        ]);

        Factory::create('damage');

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->errorResponse(array(
                'udf.delete.invalid' => Lang::get('core::validation.cannot.delete.udf')
            )));
    }

    /**
     * @test
     */
    public function udf_type_cannot_be_updated_if_the_specified_modules_already_has_a_data()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::STR
        ]);

        Factory::create('damage');

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::INTR
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'udf.update.invalid' => Lang::get('core::validation.cannot.change.udf.type')
            )));
    }

    /**
     * @test
     */
    public function updating_the_udf_type_while_there_are_no_data_in_the_specified_module_will_drop_the_column_and_create_a_new_one_with_the_corresponding_data_type()
    {
        $created = $this->create([
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::STR
        ]);

        $data = array_except(array_merge($this->item($created, 'basic'), [
            'module_id' => ModuleDefinedFields::DAMAGE,
            'type' => UserDefinedFieldType::INTR
        ]), ['id', 'alias']);

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->assertTableColumnIsOfType($created->table, $created->alias, 'integer');
    }

    protected function create($data = [])
    {
        return Factory::create($this->factory, $data);
    }

    protected function build($data = [])
    {
        return Factory::build($this->factory, $data);
    }

    protected function basic($model)
    {
        return [
            'id'      => $model->id,
            'alias'   => $model->alias,
            'label'   => $model->label,
            'type'    => $model->type,
            'visible' => $model->visible,
        ];
    }
}
