<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class BankAccountControllerTest extends FunctionalTest
{
    private $uri = '/bank-account';
    private $factory = 'bank_account';
    private $banks;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function accessing_bank_account_page_while_user_is_not_authenticated_will_redirect_to_login()
    {
        Auth::logout();

        $this->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_system_code_page_with_uri()
    {
        $this->visit($this->uri)
            ->assertResponseOk()
            ->assertViewHas('banks');
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_account_name_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[0]->account_name,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'account_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'accounts' => array(
                $this->item($data[0], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_account_number_will_return_the_corresponding_collection_of_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->account_number,
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'account_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'accounts' => array(
                $this->item($data[2], 'transform')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_a_data_using_a_filter_not_listed_in_searchable_fields_will_be_disregarded_and_return_the_full_collection()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '1',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'id'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => count($data),
                    'count' => count($data),
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'accounts' => $this->collect($data, 'transform')
        ], ''));
    }

    /**
     * @test
     */
    public function setting_a_criteria_that_doesnt_meet_the_proper_condition_will_return_an_empty_data()
    {
        $data = $this->collection();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => '999',
                    'operator' => '=',
                    'join' => 'OR' ,
                    'column' => 'account_name'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'accounts' => []
        ], Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_an_empty_account_name_account_number_or_bank_id_will_trigger_an_error()
    {
        $build = $this->build(array(
            'account_name' => '',
            'account_number' => ''
        ));

        $data = $this->toArray($build);

        $data['bank_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'bank_id' => array(
                    Lang::get('core::validation.bank.required')
                ),
                'account_number' => array(
                    Lang::get('core::validation.account.number.required')
                ),
                'account_name' => array(
                    Lang::get('core::validation.account.name.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function saving_an_existing_account_number_will_trigger_an_error_response()
    {
        $data = $this->create();

        $data = $this->toArray($data);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse([
                'account_number' => array(
                    Lang::get('core::validation.account.number.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function saving_a_unique_bank_account_name_and_proper_account_number_will_successfully_save_the_data_in_database_and_return_an_id()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'response' => array_merge($this->item($build, 'transform'), array(
                'id' => 1
            )),
            'database' => array_merge($data, array(
                'id' => 1
            ))
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'accounts' => $expected['response']
            ], Lang::get('core::success.created')))
            ->seeInDatabase('bank_account', $expected['database']);
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_bank_account_name_or_account_number_will_trigger_an_error_response()
    {
        $created = $this->create();

        $build = $this->build(array(
            'account_name' => '',
            'account_number' => ''
        ));

        $data = $this->toArray($build);

        $data['bank_id'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'bank_id' => array(
                    Lang::get('core::validation.bank.required')
                ),
                'account_number' => array(
                    Lang::get('core::validation.account.number.required')
                ),
                'account_name' => array(
                    Lang::get('core::validation.account.name.required')
                ),
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_existing_account_number_will_trigger_an_error_response()
    {
        $collection = $this->collection();

        $build = $this->build(array(
            'account_number' => $collection[1]->account_number
        ));

        $data = $this->toArray($build);

        $this->patchJson(sprintf('%s/%s', $this->uri, $collection[2]->id), $data)
            ->seeJsonEquals($this->errorResponse([
                'account_number' => array(
                    Lang::get('core::validation.account.number.unique')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_non_existing_data_will_trigger_an_error_response()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array_merge(array(
            'id' => 1
        ), $data);

        $this->patchJson(sprintf('%s/1', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse([
                'update.failed' => Lang::get('core::error.update.failed')
            ]))
            ->dontSeeInDatabase('bank_account', $expected);
    }

    /**
     * @test
     */
    public function deleting_an_existing_data_will_soft_delete_the_data_in_database_and_return_a_successful_response()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created->id))
            ->seeJsonEquals($this->successfulResponse([] , Lang::get('core::success.deleted')))
            ->notSeeInDatabase('bank_account', array(
                'id' => 1,
                'deleted_at' => null
            ));
    }

    /**
     * @test
     */
    public function deleting_a_non_existing_data_will_trigger_an_error()
    {
        $this->deleteJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]))
            ->dontSeeInDatabase('bank_account', array(
                'id' => 1,
                'deleted_at' => null
            ));
    }

    /**
     * @test
     */
    public function validation_on_data_upon_creation_will_only_include_non_soft_deleted_data()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array_merge($this->item($build, 'transform'), array(
            'id' => ($created->id + 1)
        ));

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'accounts' => $expected
            ], Lang::get('core::success.created')));
    }

    /**
     * @test
     */
    public function validation_on_data_upon_updating_will_only_include_non_soft_deleted_data()
    {
        $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $created = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array_merge($this->item($build, 'transform'), array(
            'id' => $created->id
        ));

        $this->patchJson(sprintf('%s/%s', $this->uri, $created->id), $data)
            ->seeJsonEquals($this->successfulResponse([
                'accounts' => $expected
            ], Lang::get('core::success.updated')));
    }

    protected function references($times = 4)
    {
        $this->banks = Factory::times($times)->create('bank');
    }

    protected function build($data = array())
    {
        return Factory::build($this->factory, array_merge(array(
            'bank_id' => $this->banks[rand(0, 3)]->id,
            'created_from' => 1
        ), $data));
    }

    protected function create($data = array())
    {
        return Factory::create($this->factory, array_merge(array(
            'bank_id' => $this->banks[rand(0, 3)]->id,
            'created_from' => 1,
        ), $data));
    }

    protected function collection($times = 4)
    {
        return Factory::times($times)->create($this->factory, array(
            'bank_id' => $this->banks[rand(0, 3)]->id,
            'created_from' => 1,
        ));
    }

    protected function transform($model)
    {
        return [
            'id'                => $model->id,
            'bank_id'           => $model->bank_id,
            'account_name'      => $model->account_name,
            'account_number'    => $model->account_number,
            'opening_balance'   => $model->number()->opening_balance,
        ];
    }

    protected function toArray($model)
    {
        return array_merge($model->toArray(), [
            'opening_balance' => $model->opening_balance->innerValue()
        ]);
    }
}
