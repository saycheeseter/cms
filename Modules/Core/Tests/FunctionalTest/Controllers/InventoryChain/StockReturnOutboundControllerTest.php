<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\ApprovalStatus;
use Carbon\Carbon;
use Illuminate\Support\Str;

class StockReturnOutboundControllerTest extends InventoryChainControllerTest
{
    protected $uri = '/stock-return-outbound';
    protected $prefix = 'SDRO';

    protected $tables = [
        'info' => 'stock_return_outbound',
        'detail' => 'stock_return_outbound_detail',
        'invoice_info' => 'stock_return',
        'invoice_detail' => 'stock_return_detail'
    ];

    protected $factories = [
        'info' => 'stock_return_outbound',
        'detail' => 'stock_return_outbound_detail',
        'invoice_info' => 'stock_return',
        'invoice_detail' => 'stock_return_detail'
    ];

    private $branches;
    private $users;
    
    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_deliver_to_or_deliver_to_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_deliver_to_and_deliver_to_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'deliver_to' => '99',
            'deliver_to_location' => '99',
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_deliver_to_or_deliver_to_location_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = '';
        $data['info']['deliver_to_location'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.required')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_deliver_to_and_deliver_to_location_that_doesnt_exist_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['deliver_to'] = '99';
        $data['info']['deliver_to_location'] = '99';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.deliver_to' => array(
                    Lang::get('core::validation.deliver.to.doesnt.exists')
                ),
                'info.deliver_to_location' => array(
                    Lang::get('core::validation.deliver.to.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_with_direct_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array(),
                'locations' => $this->collection($data['info']->for->details, 'branchDetailChosenTransformer'),
                'delivery_locations' => $this->collection($data['info']->deliverTo->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                )
            ]));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_with_from_invoice_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $createdInvoice = $this->createInvoice();

        $data = $this->create(
            $this->invoiceInfoTransformer($createdInvoice['info'])
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array($this->item($createdInvoice['info'], 'invoiceChosenTransformer')),
                'locations' => $this->collection($data['info']->for->details, 'branchDetailChosenTransformer'),
                'delivery_locations' => $this->collection($data['info']->deliverTo->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                )
            ]));
    }

    /**
     * @test
     */
    public function accessing_the_remaining_api_and_providing_a_data_with_the_same_created_for_and_provided_similar_pattern_of_sheet_number_will_return_the_list_of_invoice_in_its_chosen_format()
    {
        $transactions = $this->transactions();

        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $data = array(
            'sheet_number' => $created['info']->sheet_number,
            'branch' => $created['info']->deliver_to
        );

        $this->getJson(sprintf('%s/remaining', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'stock_return_outbounds' => [$this->item($created['info'], 'remainingTransformer')]
            )));
    }

    /**
     * @test
     */
    public function accessing_the_remaining_api_and_providing_a_data_that_doesnt_match_any_sheet_number_within_the_specified_branch_will_return_an_error_response()
    {
        $created = $this->create();

        $data = array(
            'sheet_number' => $this->faker->word,
            'branch' => $created['info']->created_for
        );

        $this->getJson(sprintf('%s/remaining', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            )));
    }

    /**
     * @test
     */
    public function remaining_api_should_only_return_the_invoices_with_approved_status_and_no_delivery_or_incomplete_transaction_status()
    {
        $this->transactions();

        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $prefix = Str::substr($created['info']->sheet_number, 0, -10);

        $data = array(
            'sheet_number' => $prefix,
            'branch' => $created['info']->deliver_to
        );

        $this->getJson(sprintf('%s/remaining', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'stock_return_outbounds' => [$this->item($created['info'], 'remainingTransformer')]
            )));
    }

    /**
     * @test
     */
    public function retrieving_a_non_existing_data_in_retrieve_api_will_return_an_error_response()
    {
        $data = array(
            'id' => 1,
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            )));
    }

    /**
     * @test
     */
    public function retrieving_a_transaction_on_which_all_the_remaining_qty_is_already_zero_will_return_an_error_response()
    {
        $created = $this->create();

        DB::table($this->tables['detail'])->update(['remaining_qty' => 0]);

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'no.remaining.products.found' => Lang::get('core::error.no.remaining.products.found')
            )));
    }

    /**
     * @test
     */
    public function retrieving_an_existing_transaction_with_remaining_qty_will_return_its_corresponding_transaction_and_details()
    {
        $created = $this->create();

        $data = array(
            'id' => $created['info']->id
        );

        $this->getJson(sprintf('%s/retrieve', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'stock_return_outbound' => $this->item($created['info'], 'retrieveInfoTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => 2,
                            'count' => 2,
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($created['detail'], 'retrieveDetailTransformer')
                )
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        foreach ($this->branches as $key => $branch) {
            Factory::times(2)->create('branch_detail', array(
                'branch_id' => $branch->id
            ));
        }

        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $deliver = $this->branches[rand(0, 2)];
        $for = $this->branches[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for' => $for->id,
            'for_location' => $for->details->first()->id,
            'deliver_to' => $deliver->id,
            'deliver_to_location' => $deliver->details->first()->id,
            'requested_by' => $this->users[rand(0, 2)]->id
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    public function invoiceInfoTransformer($model)
    {
        return [
            'reference_id'        => $model->id,
            'for_location'        => $model->for_location,
            'deliver_to'          => $model->deliver_to,
            'deliver_to_location' => $model->deliver_to_location,
            'requested_by'        => $model->requested_by,
            'remarks'             => $model->remarks,
        ];
    }

    protected function retrieveInfoTransformer($model)
    {
        return [
            'delivery_from'                => $model->created_for,
            'delivery_from_location'       => $model->for_location,
            'delivery_from_label'          => $model->for->name,
            'delivery_from_location_label' => $model->location->name,
            'for_location'                 => $model->deliver_to_location,
            'requested_by'                 => $model->requested_by,
            'remarks'                      => $model->remarks,
        ];
    }

    protected function remainingTransformer($model)
    {
        return [
            'id'      => $model->id,
            'label'   => $model->sheet_number,
        ];
    }

    protected function retrieveDetailTransformer($model)
    {
        return [
            'id'            => 0,
            'reference_id'  => $model->id,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcodeList,
            'barcode'       => $model->presenter()->computedUnitBarcode,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'uom'           => [
                'options' => $model->presenter()->unitList,
                'value'   => $model->computed_unit_id
            ],
            'unit_qty'      => $model->number()->computed_unit_qty,
            'qty'           => $model->number()->computed_qty,
            'total_qty'     => $model->number()->computed_total_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'amount'        => $model->number()->computed_amount,
            'remarks'       => $model->remarks,
            'remaining_qty' => $model->number()->remaining_qty,
            'invoice_qty'   => $model->number()->total_qty,
            'manage_type'   => $model->product->manage_type,
        ];
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                       => $data->id,
            'transaction_date'         => $data->transaction_date->toDateTimeString(),
            'sheet_number'             => $data->sheet_number,
            'created_for'              => $data->for->name,
            'for_location'             => $data->location->name,
            'deliver_to'               => $data->deliverTo->name,
            'deliver_to_location'      => $data->deliverToLocation->name,
            'remarks'                  => $data->remarks,
            'requested_by'             => $data->requested->full_name,
            'created_by'               => $data->creator->full_name,
            'created_date'             => $data->created_at->toDateTimeString(),
            'audited_by'               => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'             => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label'    => $data->presenter()->approval,
            'approval_status'          => $data->approval_status,
            'transaction_status_label' => $data->presenter()->transaction,
            'transaction_status'       => $data->transaction_status,
            'amount'                   => $data->number()->total_amount,
            'reference'                => $model->reference->sheet_number ?? '',
            'deleted'                  => $data->presenter()->deleted,
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'deliver_to'            => $model->deliver_to,
            'deliver_to_location'   => $model->deliver_to_location,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'reference_id'          => $model->reference_id != null ? $model->reference_id : null,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }
}
