<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\TransactionType;

class StockDeliveryInboundControllerTest extends InventoryChainControllerTest
{
    protected $uri = '/stock-delivery-inbound';
    protected $prefix = 'SDI';

    protected $tables = [
        'info' => 'stock_delivery_inbound',
        'detail' => 'stock_delivery_inbound_detail',
        'invoice_info' => 'stock_delivery_outbound',
        'invoice_detail' => 'stock_delivery_outbound_detail'
    ];

    protected $factories = [
        'info' => 'stock_delivery_inbound',
        'detail' => 'stock_delivery_inbound_detail',
        'invoice_info' => 'stock_delivery_outbound',
        'invoice_detail' => 'stock_delivery_outbound_detail'
    ];

    private $branches;
    private $users;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk();
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_delivery_from_or_delivery_from_will_trigger_an_error()
    {
        $build = $this->build([
            'transaction_type' => TransactionType::FROM_INVOICE,
        ]);

        $data = $this->toArray($build);

        $data['info']['delivery_from'] = '';
        $data['info']['delivery_from_location'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.delivery_from' => array(
                    Lang::get('core::validation.delivery.from.required')
                ),
                'info.delivery_from_location' => array(
                    Lang::get('core::validation.delivery.from.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_delivery_from_and_delivery_from_location_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'transaction_type' => TransactionType::FROM_INVOICE,
            'delivery_from' => '99',
            'delivery_from_location' => '99',
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.delivery_from' => array(
                    Lang::get('core::validation.delivery.from.doesnt.exists')
                ),
                'info.delivery_from_location' => array(
                    Lang::get('core::validation.delivery.from.location.doesnt.exists')
                ),
            )));
    }

     /**
     * @test
     */
    public function updating_a_data_with_an_empty_delivery_from_or_delivery_from_location_will_trigger_an_error()
    {
        $create = $this->create(array(
            'transaction_type' => TransactionType::FROM_INVOICE
        ));

        $data = $this->toArray($create);

        $data['info']['delivery_from'] = '';
        $data['info']['delivery_from_location'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.delivery_from' => array(
                    Lang::get('core::validation.delivery.from.required')
                ),
                'info.delivery_from_location' => array(
                    Lang::get('core::validation.delivery.from.location.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_delivery_from_and_delivery_from_location_that_doesnt_exist_will_trigger_an_error()
    {
        $create = $this->create(array(
            'transaction_type' => TransactionType::FROM_INVOICE
        ));

        $data = $this->toArray($create);

        $data['info']['delivery_from'] = '99';
        $data['info']['delivery_from_location'] = '99';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.delivery_from' => array(
                    Lang::get('core::validation.delivery.from.doesnt.exists')
                ),
                'info.delivery_from_location' => array(
                    Lang::get('core::validation.delivery.from.location.doesnt.exists')
                ),
            )));
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        foreach ($this->branches as $key => $branch) {
            Factory::times(2)->create('branch_detail', array(
                'branch_id' => $branch->id
            ));
        }

        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $deliver = $this->branches[rand(0, 2)];
        $for = $this->branches[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for' => $for->id,
            'for_location' => $for->details->first()->id,
            'delivery_from' => $deliver->id,
            'delivery_from_location' => $deliver->details->first()->id,
            'requested_by' => $this->users[rand(0, 2)]->id
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 1)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    public function invoiceInfoTransformer($model)
    {
        return [
            'reference_id'           => $model->id,
            'for_location'           => $model->for_location,
            'delivery_from'          => $model->deliver_to,
            'delivery_from_location' => $model->deliver_to_location,
            'requested_by'           => $model->requested_by,
            'remarks'                => $model->remarks,
        ];
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                       => $data->id,
            'transaction_date'         => $data->transaction_date->toDateTimeString(),
            'sheet_number'             => $data->sheet_number,
            'remarks'                  => $data->remarks,
            'requested_by'             => $data->requested->full_name,
            'created_by'               => $data->creator->full_name,
            'created_for'              => $data->for->name,
            'for_location'             => $data->location->name,
            'delivery_from'            => $data->deliveryFrom->name,
            'delivery_from_location'   => $data->deliveryFromLocation->name,
            'created_date'             => $data->created_at->toDateTimeString(),
            'audited_by'               => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'             => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label'    => $data->presenter()->approval,
            'approval_status'          => $data->approval_status,
            'transaction_status_label' => $data->presenter()->transaction,
            'amount'                   => $data->number()->total_amount,
            'reference'                => $data->reference->sheet_number ?? '',
            'deleted'                  => $data->presenter()->deleted,
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                           => $model->id,
            'sheet_number'                 => $model->sheet_number,
            'transaction_date'             => $model->transaction_date->toDateTimeString(),
            'deliver_to'                   => $model->deliver_to,
            'remarks'                      => $model->remarks,
            'requested_by'                 => $model->requested_by,
            'created_date'                 => $model->created_at->toDateTimeString(),
            'modified_date'                => $model->updated_at->toDateTimeString(),
            'requested_by'                 => $model->requested_by,
            'delivery_from'                => $model->delivery_from ?? null,
            'delivery_from_location'       => $model->delivery_from_location ?? null,
            'delivery_from_label'          => $model->deliveryFrom->name ?? '',
            'delivery_from_location_label' => $model->deliveryFromLocation->name ?? '',
            'approval_status_label'        => $model->presenter()->approval,
            'approval_status'              => $model->approval_status,
            'transaction_type'             => $model->transaction_type,
            'created_by'                   => $model->creator->full_name,
            'created_for'                  => $model->created_for,
            'for_location'                 => $model->for_location,
            'reference_id'                 => $model->reference_id ?? null,
            'modified_by'                  => $model->modifier->full_name ?? '',
        ];
    }
}