<?php

use Laracasts\TestDummy\Factory;
use Carbon\Carbon;

class PurchaseReturnOutboundControllerTest extends InventoryChainControllerTest
{
    protected $uri = '/purchase-return-outbound';
    protected $prefix = 'PORO';

    protected $tables = [
        'info' => 'purchase_return_outbound',
        'detail' => 'purchase_return_outbound_detail',
        'invoice_info' => 'purchase_return',
        'invoice_detail' => 'purchase_return_detail'
    ];

    protected $factories = [
        'info' => 'purchase_return_outbound',
        'detail' => 'purchase_return_outbound_detail',
        'invoice_info' => 'purchase_return',
        'invoice_detail' => 'purchase_return_detail'
    ];

    private $suppliers;
    private $paymentMethods;
    private $branches;
    private $users;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas('tsuppliers')
            ->assertViewHas('paymentMethods');
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk()
            ->assertViewHas('tsuppliers')
            ->assertViewHas('paymentMethods');
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_supplier_id_or_payment_method_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['supplier_id'] = '';
        $data['info']['payment_method'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_a_supplier_id_or_payment_method_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'supplier_id' => 99,
            'payment_method' => 99,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists'),
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists'),
                )
            )));
    }
    
    /**
     * @test
     */
    public function saving_a_non_numeric_term_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['term'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_supplier_id_or_payment_method_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = '';
        $data['info']['payment_method'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.required')
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.required')
                )
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_supplier_id_requested_by_payment_method_or_created_for_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['supplier_id'] = 99;
        $data['info']['payment_method'] = 99;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.supplier_id' => array(
                    Lang::get('core::validation.supplier.doesnt.exists'),
                ),
                'info.payment_method' => array(
                    Lang::get('core::validation.payment.method.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_numeric_term_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['term'] = 'A';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
            )));
    }

    /**
     * Build reference data for the transaction
     * 
     * @return $this        
     */
    protected function references()
    {
        parent::references();

        $this->branches = Factory::times(3)->create('branch');

        foreach ($this->branches as $key => $branch) {
            Factory::times(2)->create('branch_detail', array(
                'branch_id' => $branch->id
            ));
        }

        $this->suppliers = Factory::times(3)->create('supplier');
        $this->paymentMethods = Factory::times(3)->create('payment_method');
        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    /**
     * Build non-persisting transaction model
     * 
     * @param  array  $info  
     * @param  array  $detail
     * @return array        
     */
    protected function build($info = array(), $detail = array())
    {
        $for       = $this->branches[rand(0, 2)];
        $suppliers = $this->suppliers[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for'    => $for->id,
            'for_location'   => $for->details->first()->id,
            'supplier_id'    => $suppliers->id,
            'payment_method' => $this->paymentMethods[rand(0, 2)]->id,
            'requested_by'   => $this->users[rand(0, 2)]->id
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id'     => $this->products[rand(0, 1)]->id,
            'unit_id'        => $this->units[rand(0, 1)]->id
        ), $detail));

        return array(
            'info'   => $info,
            'detail' => $detail
        );
    }

    public function invoiceInfoTransformer($model)
    {
        return [
            'reference_id'   => $model->id,
            'supplier_id'    => $model->supplier_id,
            'payment_method' => $model->payment_method,
            'requested_by'   => $model->requested_by,
            'remarks'        => $model->remarks,
            'term'           => $model->term
        ];
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'created_by'            => $data->creator->full_name,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'supplier'              => $data->supplier->full_name,
            'term'                  => $data->term,
            'payment_method'        => $data->payment->name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor) ? $data->auditor->full_name : '',
            'audited_date'          => !is_null($data->auditor) ? $data->audited_date->toDateTimeString() : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'amount'                => $data->number()->total_amount,
            'reference'             => $data->reference->sheet_number ?? '',
            'deleted'               => $data->presenter()->deleted,
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'supplier_id'           => $model->supplier_id,
            'payment_method'        => $model->payment_method,
            'term'                  => $model->term,
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'requested_by'          => $model->requested_by,
            'approval_status_label' => $model->presenter()->approval,
            'approval_status'       => $model->approval_status,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'reference_id'          => $model->reference_id != null ? $model->reference_id : null,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }
}