<?php

use Laracasts\TestDummy\Factory;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Enums\Voided;
use Modules\Core\Enums\GroupType;
use Modules\Core\Enums\ProductType;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use Litipk\BigNumbers\Decimal;

abstract class InventoryChainControllerTest extends FunctionalTest
{
    protected $uri;
    protected $prefix;

    protected $tables = [];

    protected $factories = [];

    protected $products;
    protected $units;
    protected $productUnits;
    protected $productBarcodes;

    public function setUp()
    {
        parent::setUp();

        $this->init();
        $this->login();
        $this->references();
    }

    /**
     * @test
     */
    public function visiting_the_transaction_page_while_not_logged_in_will_redirect_the_user_to_login_page()
    {
        $this->logout()->get($this->uri)->assertRedirectedTo('/');
    }

    /**
     * @test
     */
    public function displays_the_transaction_list_page()
    {
        $this->visit($this->uri)->assertResponseOk();
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_sheet_number_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->sheet_number,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ], ''));
    }

     /**
     * @test
     */
    public function searching_for_a_data_based_on_transaction_date_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[2]->transaction_date->toDateTimeString(),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'transaction_date'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[2], 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_remarks_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[3]->remarks,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'remarks'
                )
            )
        ))->seeJsonEquals($this->successfulResponse([
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[3], 'transactionBasicTransformer')
            )
        ], ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_approval_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => ApprovalStatus::DRAFT,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'approval_status'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collection($data, 'transactionBasicTransformer')
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_deleted_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $data[1]->delete();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => Voided::YES,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'deleted_at'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 1,
                    'count' => 1,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array(
                $this->item($data[1], 'transactionBasicTransformer')
            )
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_based_on_unddeleted_status_will_return_the_corresponding_collection_of_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $data[0]->delete();

        $data->shift();

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => Voided::NO,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'deleted_at'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 3,
                    'count' => 3,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collection($data, 'transactionBasicTransformer')
        ), ''));
    }

    /**
     * @test
     */
    public function searching_for_a_data_using_a_filter_not_in_searchable_fields_will_be_ignored_and_return_the_data()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => $data[1]->total_amount,
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'total_amount'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 4,
                    'count' => 4,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => $this->collection($data, 'transactionBasicTransformer')
        ), ''));
    }

     /**
     * @test
     */
    public function searching_for_a_data_that_doesnt_meet_the_filter_conditions_will_return_an_empty_collection()
    {
        $data = collect($this->transactions())->pluck('info');

        $this->getJson($this->uri, array(
            'filters' => array(
                array(
                    'value' => (string) $this->faker->numberBetween(1, 100),
                    'operator' => '=',
                    'join' => 'AND' ,
                    'column' => 'sheet_number'
                )
            )
        ))->seeJsonEquals($this->successfulResponse(array(
            'meta' => array(
                'pagination' => array(
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 10,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => []
                )
            ),
            'records' => array()
        ), Lang::get('core::info.no.results.found')));
    }

    /**
     * @test
     */
    public function saving_the_data_with_an_empty_requested_by_created_for_or_transaction_date_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['requested_by'] = '';
        $data['info']['created_for'] = '';
        $data['info']['for_location'] = '';
        $data['info']['transaction_date'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.requested_by' => array(
                    Lang::get('core::validation.request_by.required')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_data_with_requested_by_or_created_for_or_reference_id_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([
            'requested_by' => '99',
            'created_for' => '99',
            'for_location' => '99',
            'reference_id' => '99',
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.requested_by' => array(
                    Lang::get('core::validation.user.doesnt.exists')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.doesnt.exists')
                ),
                'info.reference_id' => array(
                    Lang::get('core::validation.reference.doesnt.exists')
                ),
            )));
    }

      /**
     * @test
     */
    public function saving_an_incorrect_date_format_for_transaction_date_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['transaction_date'] = Carbon::now()->toDateString();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_product_id_unit_id_unit_qty_detail_qty_price_or_oprice_in_transaction_detail_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['product_id'] = '';
        $data['details'][0]['uom']['value'] = '';
        $data['details'][0]['unit_qty'] = '';
        $data['details'][0]['qty'] = '';
        $data['details'][0]['oprice'] = '';
        $data['details'][0]['price'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.product_id' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.product.required')
                    )
                ),
                'details.0.uom.value' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.required')
                    )
                ),
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.required')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.required')
                    )
                ),
                'details.0.oprice' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.oprice.required')
                    )
                ),
                'details.0.price' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.price.required')
                    )
                ),
            )));
    }

    
    /**
     * @test
     */
    public function saving_a_product_id_or_unit_id_or_reference_in_transaction_detail_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $build = $this->build([], [
            'product_id' => 111,
            'reference_id' => 111,
        ]);

        $data = $this->toArray($build);

        $data['details'][0]['uom']['value'] = 112;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.product_id' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.product.doesnt.exists')
                    )
                ),
                'details.0.uom.value' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.doesnt.exists')
                    )
                ),
                'details.0.reference_id' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.reference.doesnt.exists')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_unit_qty_detail_qty_oprice_price_or_discounts_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['unit_qty'] = 'A';
        $data['details'][0]['qty'] = 'A';
        $data['details'][0]['oprice'] = 'A';
        $data['details'][0]['price'] = 'A';
        $data['details'][0]['discount1'] = 'A';
        $data['details'][0]['discount2'] = 'A';
        $data['details'][0]['discount3'] = 'A';
        $data['details'][0]['discount4'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.numeric')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.numeric')
                    )
                ),
                'details.0.oprice' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.oprice.numeric')
                    )
                ),
                'details.0.price' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.price.numeric')
                    )
                ),
                'details.0.discount1' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '1'])
                    )
                ),
                'details.0.discount2' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '2'])
                    )
                ),
                'details.0.discount3' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '3'])
                    )
                ),
                'details.0.discount4' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '4'])
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_unit_qty_or_detail_qty_less_than_or_equal_to_zero_will_trigger_an_error()
    {
        $build = $this->build([], [
            'unit_qty' => 0,
            'qty' => 0
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.greater.than.zero')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.greater.than.zero')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function saving_an_incorrect_format_of_details_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = 1;

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_an_empty_details_holder_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'] = array();

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_from_invoice_transaction_with_a_non_existing_invoice_will_trigger_an_error_response()
    {
        $build = $this->build([
            'transaction_type' => TransactionType::FROM_INVOICE,
            'reference_id' => '99',
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.reference_id' => array(
                    Lang::get('core::validation.reference.doesnt.exists')
                ),
        )));
    }

    /**
     * @test
     */
    public function saving_a_correct_from_invoice_transaction_format_will_successfull_saved_the_data_in_database_generate_a_transaction_number_and_return_the_details_of_the_transaction()
    {
        $createdInvoice = $this->createInvoice();

        $build = $this->build(
            array_merge(
                array('transaction_type' => TransactionType::FROM_INVOICE),
                $this->invoiceInfoTransformer($createdInvoice['info'])
            ),
            $this->invoiceDetailTransformer($createdInvoice['detail'])
        );

        $data = $this->toArray($build);

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'transaction_status',
                )), array(
                    'id' => 1,
                )),
                'details' => array(
                    array_merge(
                        array_except($data['details'][0], array(
                            'uom',
                            'barcode_list',
                            'invoice_qty',
                            'amount',
                            'remaining_qty',
                            'total_qty',
                            'barcode',
                            'name',
                            'chinese_name'
                       )),
                        array(
                            'id' => 1,
                            'transaction_id' => 1
                        )
                    )
                )
            ),
            'response' => array_merge(
                $this->item($build['info'], 'inventoryChainBasicTransformer'),
                array(
                    'id' => 1,
                    'sheet_number' => $this->generateReferenceNumber(1, $build['info']->created_from)
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'record' => $expected['response']
            ], Lang::get('core::success.created')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0]);
    }

    /**
     * @test
     */
    public function saving_a_correct_direct_transaction_format_will_successfull_saved_the_data_in_database_generate_a_transaction_number_and_return_the_details_of_the_transaction()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'transaction_status',
                )), array(
                    'id' => 1,
                )),
                'details' => array(
                    array_merge(
                        array_except($data['details'][0], 'uom'),
                        array(
                            'id' => 1,
                            'transaction_id' => 1
                        )
                    )
                )
            ),
            'response' => array_merge(
                $this->item($build['info'], 'inventoryChainBasicTransformer'),
                array(
                    'id' => 1,
                    'sheet_number' => $this->generateReferenceNumber(1, $build['info']->created_from)
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'record' => $expected['response']
            ], Lang::get('core::success.created')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0]);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_saving_the_transaction_details()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $expected = array(
            'id' => 1,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['qty'] * $details['unit_qty'] * $details['price'];
            }),
        );

        $this->postJson($this->uri, $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

     /**
     * @test
     */
    public function accessing_a_non_existing_data_in_the_edit_api_will_return_an_error_message()
    {
        $this->getJson(sprintf('%s/1', $this->uri))
            ->seeJsonEquals($this->errorResponse(array(
                'show.failed' => Lang::get('core::error.show.failed')
            )));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_with_direct_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $data = $this->create();

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array(),
                'locations' => $this->collection($data['info']->for->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                )
            ]));
    }

    /**
     * @test
     * @group 1111
     */
    public function accessing_an_existing_data_with_from_invoice_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $createdInvoice = $this->createInvoice();

        $data = $this->create(
            $this->invoiceInfoTransformer($createdInvoice['info'])
        );

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array($this->item($createdInvoice['info'], 'invoiceChosenTransformer')),
                'locations' => $this->collection($data['info']->for->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                )
            ]));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_requested_by_created_for_or_transaction_date_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['requested_by'] = '';
        $data['info']['created_for'] = '';
        $data['info']['for_location'] = '';
        $data['info']['transaction_date'] = null;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.requested_by' => array(
                    Lang::get('core::validation.request_by.required')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.required')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.required')
                ),
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_requested_by_or_created_for_that_doesnt_exist_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['requested_by'] = '99';
        $data['info']['created_for'] = '99';
        $data['info']['for_location'] = '99';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.requested_by' => array(
                    Lang::get('core::validation.user.doesnt.exists')
                ),
                'info.created_for' => array(
                    Lang::get('core::validation.created.for.doesnt.exists')
                ),
                'info.for_location' => array(
                    Lang::get('core::validation.for.location.doesnt.exists')
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect_date_format_for_transaction_date_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['info']['transaction_date'] = Carbon::now()->toDateString();

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.transaction_date' => array(
                    Lang::get('core::validation.transaction.date.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_product_id_unit_id_unit_qty_detail_qty_price_or_oprice_in_transaction_detail_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['product_id'] = '';
        $data['details'][0]['uom']['value'] = '';
        $data['details'][0]['unit_qty'] = '';
        $data['details'][0]['qty'] = '';
        $data['details'][0]['oprice'] = '';
        $data['details'][0]['price'] = '';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.product_id' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.product.required')
                    )
                ),
                'details.0.uom.value' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.required')
                    )
                ),
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.required')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.required')
                    )
                ),
                'details.0.oprice' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.oprice.required')
                    )
                ),
                'details.0.price' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.price.required')
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_product_id_or_unit_id_in_transaction_detail_that_doesnt_exist_in_database_will_trigger_an_error()
    {
        $create = $this->create([]);

        $data = $this->toArray($create);

        $data['details'][0]['product_id'] = 112;
        $data['details'][0]['uom']['value'] = 112;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.product_id' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.product.doesnt.exists')
                    )
                ),
                'details.0.uom.value' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.doesnt.exists')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_non_numeric_unit_qty_detail_qty_oprice_price_or_discounts_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['unit_qty'] = 'A';
        $data['details'][0]['qty'] = 'A';
        $data['details'][0]['oprice'] = 'A';
        $data['details'][0]['price'] = 'A';
        $data['details'][0]['discount1'] = 'A';
        $data['details'][0]['discount2'] = 'A';
        $data['details'][0]['discount3'] = 'A';
        $data['details'][0]['discount4'] = 'A';

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.numeric')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.numeric')
                    )
                ),
                'details.0.oprice' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.oprice.numeric')
                    )
                ),
                'details.0.price' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.price.numeric')
                    )
                ),
                'details.0.discount1' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '1'])
                    )
                ),
                'details.0.discount2' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '2'])
                    )
                ),
                'details.0.discount3' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '3'])
                    )
                ),
                'details.0.discount4' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.discount.numeric', ['num' => '4'])
                    )
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_a_unit_qty_or_detail_qty_less_than_or_equal_to_zero_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'][0]['unit_qty'] = 0;
        $data['details'][0]['qty'] = 0;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details.0.unit_qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.unit.qty.greater.than.zero')
                    )
                ),
                'details.0.qty' => array(
                    sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        1,
                        Lang::get('core::validation.qty.greater.than.zero')
                    )
                )
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_incorrect_format_of_details_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'] = 1;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.invalid.format'),
                ),
            )));
    }

    /**
     * @test
     */
    public function updating_a_data_with_an_empty_details_holder_will_trigger_an_error()
    {
        $create = $this->create();

        $data = $this->toArray($create);

        $data['details'] = array();

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'details' => array(
                    Lang::get('core::validation.details.min.one')
                ),
            )));
    }

    /**
     * @test
     */
    public function removing_a_transaction_detail_will_successfully_soft_delete_the_data_in_database()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['id'] = $create['detail'][1]->id;

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->notSeeInDatabase($this->tables['detail'], array(
                    'id' => $create['detail'][0]->id,
                    'deleted_at' => null
                )
            );

    }

    /**
     * @test
     */
    public function updating_a_data_with_a_correct_transaction_format_will_successfull_saved_the_data_in_database_and_return_the_details_of_the_transaction()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['id'] = $create['detail'][0]->id;
        $data['details'][0]['transaction_id'] = $create['info']->id;

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'transaction_status',
                )), array(
                    'id' => $create['info']->id,
                )),
                'details' => array(
                    array_except($data['details'][0], 'uom')
                )
            ),
            'response' => array_merge(
                $this->item($build['info'], 'inventoryChainBasicTransformer'),
                array(
                    'id' => $create['info']->id,
                    'sheet_number' => $create['info']->sheet_number
                )
            )
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->successfulResponse([], Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0])
            ->notSeeInDatabase($this->tables['detail'], array(
                    'id' => $create['detail'][1]->id,
                    'deleted_at' => null
                )
            );
    }

    /**
     * @test
     */
    public function approving_a_from_invoice_transaction_with_all_invoice_quantity_will_update_invoice_transaction_status_to_complete()
    {
        $invoice = $this->createInvoice(array(
            'approval_status' => ApprovalStatus::APPROVED,
            'transaction_status' => TransactionStatus::NO_DELIVERY
        ));

        $created = $this->create(
            array_merge(
                array(
                    'transaction_type' => TransactionType::FROM_INVOICE,
                    'approval_status' => ApprovalStatus::FOR_APPROVAL
                ),
                $this->invoiceInfoTransformer($invoice['info'])
            ),
            $this->invoiceDetailTransformer($invoice['detail'])
        );

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ))
            ->seeInDatabase($this->tables['invoice_info'], array(
                'transaction_status' => TransactionStatus::COMPLETE
            ))
            ->seeInDatabase($this->tables['invoice_detail'], array(
                'id' => $created['detail']->id,
                'remaining_qty' => '0'
            ));
    }

    /**
     * @test
     */
    public function approving_a_from_invoice_transaction_with_incomplete_invoice_quantity_will_update_invoice_transaction_status_to_incomplete()
    {
        $invoice = $this->createInvoice(array(
            'approval_status' => ApprovalStatus::APPROVED,
            'transaction_status' => TransactionStatus::NO_DELIVERY
        ));

        $created = $this->create(
            array_merge(
                array(
                    'transaction_type' => TransactionType::FROM_INVOICE,
                    'approval_status' => ApprovalStatus::FOR_APPROVAL
                ),
                $this->invoiceInfoTransformer($invoice['info'])
            ),
            $this->invoiceDetailTransformer($invoice['detail'], -1)
        );

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );
        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );
        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ))
            ->seeInDatabase($this->tables['invoice_info'], array(
                'transaction_status' => TransactionStatus::INCOMPLETE
            ))
            ->seeInDatabase($this->tables['invoice_detail'], array(
                'id' => $created['detail']->id,
                'remaining_qty' => $this->computeRemaining(1, $invoice['detail']->unit_qty)
            ));
    }

    /**
     * @test
     */
    public function approving_a_from_invoice_transaction_with_excess_invoice_quantity_will_update_invoice_transaction_status_to_excess()
    {
        $invoice = $this->createInvoice(array(
            'approval_status' => ApprovalStatus::APPROVED,
            'transaction_status' => TransactionStatus::NO_DELIVERY
        ));

        $created = $this->create(
            array_merge(
                array(
                    'transaction_type' => TransactionType::FROM_INVOICE,
                    'approval_status' => ApprovalStatus::FOR_APPROVAL
                ),
                $this->invoiceInfoTransformer($invoice['info'])
            ),
            $this->invoiceDetailTransformer($invoice['detail'], 1)
        );

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );
        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );
        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ))
            ->seeInDatabase($this->tables['invoice_info'], array(
                'transaction_status' => TransactionStatus::EXCESS
            ))
            ->seeInDatabase($this->tables['invoice_detail'], array(
                'id' => $created['detail']->id,
                'remaining_qty' => $this->computeRemaining(-1, $invoice['detail']->unit_qty)
            ));
    }

   /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_computed_upon_updating_the_transaction_details()
    {
        $create = $this->create();
        $build = $this->build();

        $data = $this->toArray($build);
        $created = $this->toArray($create);

        $data['details'][0]['id'] = $create['detail'][0]->id;
        $data['details'][] = $created['details'][1];

        $expected = array(
            'id' => $create['info']->id,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['qty'] * $details['unit_qty'] * $details['price'];
            }),
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

    /**
     * @test
     */
    public function total_amount_in_transaction_will_automatically_be_recomputed_upon_removing_a_transaction_detail()
    {
        $create = $this->create();
        $build = $this->build();

        $data = $this->toArray($build);

        $data['details'][0]['id'] = $create['detail'][0]->id;

        $expected = array(
            'id' => $create['info']->id,
            'total_amount' => collect($data['details'])->sum(function ($details) {
                return $details['qty'] * $details['unit_qty'] * $details['price'];
            }),
        );

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeInDatabase($this->tables['info'], $expected);
    }

    /**
     * @test
     */
    public function transaction_cannot_be_updated_if_its_already_deleted()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        DB::table($this->tables['info'])
            ->where('id', $create['info']->id)
            ->update(array(
                'deleted_at' => Carbon::now()
            ));

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted'),
            )));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_updated_if_approval_status_is_not_draft()
    {
        $create = $this->create();

        $build = $this->build();

        $data = $this->toArray($build);

        DB::table($this->tables['info'])
            ->where('id', $create['info']->id)
            ->update(array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));

        $this->patchJson(sprintf('%s/%s', $this->uri, $create['info']->id), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            )));
    }

    /**
     * @test
     */
    public function using_a_non_existing_reference_number_for_importing_will_return_an_error_message()
    {
        $data = array(
            'reference_number' => $this->faker->numberBetween(1, 100000),
            'id' => 0
        );

        $this->getJson(sprintf('%s/import', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            )));
    }

    /**
     * @test
     */
    public function using_the_transactions_own_reference_number_for_import_will_return_an_error_message()
    {
        $transactions = $this->transactions();

        $data = array(
            'reference_number' => $transactions[2]['info']->sheet_number,
            'id' => $transactions[2]['info']->id
        );

        $this->getJson(sprintf('%s/import', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            )));
    }

    /**
     * @test
     */
    public function using_an_existing_reference_number_for_importing_will_return_the_transaction_details_of_the_corresponding_reference_number()
    {
        $transactions = $this->transactions();

        $data = array(
            'reference_number' => $transactions[0]['info']->sheet_number,
            'id' => $transactions[2]['info']->id
        );

        $this->getJson(sprintf('%s/import', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $transactions[0]['detail']->count(),
                            'count' => $transactions[0]['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($transactions[0]['detail'], 'inventoryChainDetailImportTransformer')
                )
            ), Lang::get('core::success.transaction.successfully.imported')));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_change_if_transaction_is_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.deleted" => Lang::get(
                    'core::validation.transaction.number.deleted',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_for_approval_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.for.approval',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_for_approval_if_current_status_is_draft()
    {
        $created = $this->create();

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::FOR_APPROVAL
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_approved_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.approve',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_approved_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::APPROVED
            ));
    }

    /**
     * @test
     */
    public function approval_status_cannot_be_set_to_declined_if_current_status_is_not_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DRAFT
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.set.to.decline',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function approval_status_can_be_set_to_declined_if_current_status_is_for_approval()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DECLINED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DECLINED
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_approved()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_approved()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_reverted_if_current_status_is_not_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->errorResponse(array(
                "transaction.0.approval.invalid" => Lang::get(
                    'core::validation.cannot.revert.transaction',
                    ['number' => $created['info']->sheet_number]
                )
            )));
    }

    /**
     * @test
     */
    public function transaction_can_be_reverted_if_current_status_is_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::DECLINED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.updated')))
            ->seeInDatabase($this->tables['info'], array(
                'approval_status' => ApprovalStatus::DRAFT
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_automatically_be_generated_if_transaction_is_approved_or_declined()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => Auth::user()->id
            ))
            ->notSeeInDatabase($this->tables['info'], array(
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function audited_by_and_date_will_be_set_to_null_if_transaction_is_reverted()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::APPROVED
        ));

        $data = array(
            'transactions' => $created['info']->id,
            'status' => ApprovalStatus::DRAFT
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeJsonEquals($this->successfulResponse(
                array(),
                Lang::get('core::success.updated')
            ))
            ->seeInDatabase($this->tables['info'], array(
                'audited_by' => null,
                'audited_date' => null
            ));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_its_already_deleted()
    {
        $created = $this->create(array(
            'deleted_at' => Carbon::now()
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            )));
    }

    /**
     * @test
     */
    public function transaction_cannot_be_deleted_if_current_status_is_not_draft()
    {
        $created = $this->create(array(
            'approval_status' => ApprovalStatus::FOR_APPROVAL
        ));

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->errorResponse(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            )));
    }

    /**
     * @test
     */
    public function transaction_will_be_successfully_soft_deleted_if_current_status_is_draft_and_not_yet_deleted()
    {
        $created = $this->create();

        $this->deleteJson(sprintf('%s/%s', $this->uri, $created['info']->id))
            ->seeJsonEquals($this->successfulResponse(array(), Lang::get('core::success.deleted')))
            ->notSeeInDatabase($this->tables['info'], array(
                'deleted_at' => null
            ));
    }

    /**
     * @test
     */
    public function approving_a_from_invoice_transaction_will_decrement_the_pending_quantity_qty_in_product_branch_summary()
    {
        $invoice = $this->createInvoice(array(
            'approval_status' => \Modules\Core\Enums\ApprovalStatus::FOR_APPROVAL,
            'transaction_status' => \Modules\Core\Enums\TransactionStatus::NO_DELIVERY
        ));

        $invoice['info']->approval_status = \Modules\Core\Enums\ApprovalStatus::APPROVED;
        $invoice['info']->save();
        
        $created = $this->create(
            array_merge(
                array(
                    'transaction_type' => \Modules\Core\Enums\TransactionType::FROM_INVOICE,
                    'approval_status' => \Modules\Core\Enums\ApprovalStatus::FOR_APPROVAL
                ),
                $this->invoiceInfoTransformer($invoice['info'])
            ),
            $this->invoiceDetailTransformer($invoice['detail'])
        );

        $data = array(
            'transactions' => $created['info']->id,
            'status' => \Modules\Core\Enums\ApprovalStatus::APPROVED
        );

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase('product_branch_summary', array(
                'product_id' => $invoice['detail']->product_id,
                'branch_id' => $invoice['info']->created_for,
                'branch_detail_id' => $invoice['info']->for_location,
                $this->getPendingQuantityColumnByModel($invoice['info']) => 0
            ));
    }

    /**
     * @test
     */
    public function approving_a_transaction_with_product_with_automatic_grouping_will_deduct_components_inventory()
    {
        $grouping = $this->createProductGrouping([
            'group_type' => GroupType::AUTOMATIC_GROUP
        ]);

        $transaction = $this->create([
                'transaction_type' => TransactionType::DIRECT, 
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ], 
            ['product_id' => $grouping['grouping']->id]
        );

        $data = array(
            'transactions' => $transaction['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = [
            'branch_id' => $transaction['info']->id,
            'product_id' => $grouping['component']->id,
            'branch_detail_id' => $transaction['info']->for_location,
            'qty' => $transaction['info']->total_qty->mul($transaction['info']->multiplier)->mul($grouping['productComponent']->qty)->innerValue()
        ];

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase('product_branch_summary', $expected);
    }

    /**
     * @test
     */
    public function approving_a_transaction_with_component_of_product_with_automatic_ungrouping_will_deduct_product_group_inventory()
    {
        $grouping = $this->createProductGrouping([
            'group_type' => GroupType::AUTOMATIC_UNGROUP
        ]);

        $transaction = $this->create([
                'transaction_type' => TransactionType::DIRECT, 
                'approval_status' => ApprovalStatus::FOR_APPROVAL
            ], 
            ['product_id' => $grouping['component']->id]
        );

        $data = array(
            'transactions' => $transaction['info']->id,
            'status' => ApprovalStatus::APPROVED
        );

        $expected = [
            'branch_id' => $transaction['info']->id,
            'product_id' => $grouping['grouping']->id,
            'branch_detail_id' => $transaction['info']->for_location,
            'qty' => $transaction['info']->total_qty->mul($transaction['info']->multiplier)->mul($grouping['productComponent']->qty)->innerValue()
        ];

        $this->postJson(sprintf('%s/approval', $this->uri), $data)
            ->seeInDatabase('product_branch_summary', $expected);
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $info = Factory::build($this->factories['info'], $info);

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 2)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info,
            'detail' => $detail
        );
    }

    /**
     * Build persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function create($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['info'], $info);

        if($info->transaction_type == TransactionType::FROM_INVOICE){

            $detail = Factory::create($this->factories['detail'], array_merge(array(
                'transaction_id' => $info->id
            ), $detail));

        }else{
            $detail = Factory::times(2)->create($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 2)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
            ), $detail));
        }

        return array(
            'info' => $info->fresh(),
            'detail' => $detail
        );
    }

    protected function createInvoice($info = array(), $detail = array())
    {
        $info = Factory::create($this->factories['invoice_info'], $info);

        $detail = Factory::create($this->factories['invoice_detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id' => $this->products[rand(0, 2)]->id,
            'unit_id' => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info' => $info->fresh(),
            'detail' => $detail
        );
    }

    /**
     * Build a collection of transaction
     *
     * @param  int  $time
     * @return array
     */
    protected function transactions($times = 4)
    {
        $data = [];

        for ($i=0; $i < $times; $i++) {
            $data[] = $this->create();
        }

        return $data;
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        $this->products = Factory::times(3)->create('product');
        $this->units = Factory::times(2)->create('uom');

        foreach ($this->products as $key => $product) {
            foreach ($this->units as $unit) {
                $qty = $this->faker->numberBetween(1, 10);
                $code = $this->faker->numberBetween(1, 1000000);

                DB::table('product_unit')->insert([
                    [
                        'product_id' => $product->id,
                        'uom_id' => $unit->id,
                        'qty' => $qty,
                        'default' => 0
                    ],
                ]);

                $this->productUnits[$key][] = [
                    'product_id' => $product->id,
                    'uom_id' => $unit->id,
                    'qty' => $qty,
                    'default' => 0
                ];

                DB::table('product_barcode')->insert([
                    [
                        'product_id' => $product->id,
                        'uom_id' => $unit->id,
                        'code' => $code
                    ],
                ]);

                $this->productBarcodes[$key][] = [
                    'product_id' => $product->id,
                    'uom_id' => $unit->id,
                    'code' => $code
                ];
            }

            DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $product->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);
        }

        return $this;
    }

    /**
     * API for transformer single item
     *
     * @param  array $data
     * @param  string $transformer
     * @return array
     */
    protected function item($data, $transformer)
    {
        return $this->{$transformer}($data, $transformer);
    }

    /**
     * API for transformer collection
     *
     * @param  array $data
     * @param  string $transformer
     * @return array
     */
    protected function collection($data, $transformer)
    {
        $new = [];

        foreach ($data as $key => $value) {
            $new[] = $this->item($value, $transformer);
        }

        return $new;
    }

    protected function inventoryChainBasicTransformer($model)
    {
        return [
            'id'               => $model->id,
            'sheet_number'     => $model->sheet_number,
            'transaction_type' => $model->transaction_type,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'remarks'          => $model->remarks,
        ];
    }

    protected function inventoryChainDetailFullTransformer($model)
    {
        $precision = setting('monetary.precision');

        $serials = [];
        $batches = [];

        foreach ($model->serials as $key => $serial) {
            $serials[] = [
                'id'                          => $serial->id,
                'serial_number'               => $serial->serial_number,
                'expiration_date'             => $serial->expiration_date,
                'manufacturing_date'          => $serial->manufacturing_date,
                'admission_date'              => $serial->admission_date,
                'manufacturer_warranty_start' => $serial->manufacturer_warranty_start,
                'manufacturer_warranty_end'   => $serial->manufacturer_warranty_end,
                'remarks'                     => $serial->remarks,
            ];
        }

        return [
            'id'            => $model->id,
            'product_id'    => $model->product_id,
            'serials'       => $serials,
            'batches'       => $batches,
            'manage_type'   => $model->product->manage_type,
            'barcode_list'  => $model->presenter()->barcodeList,
            'barcode'       => $model->presenter()->unitBarcode,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'uom'           => [
                'options' => $model->presenter()->unitList,
                'value'   => $model->unit_id
            ],
            'old_unit'      => [
                'id'  => $model->unit_id,
                'qty' => $model->number()->unit_qty
            ],
            'unit_qty'      => $model->number()->unit_qty,
            'qty'           => $model->number()->qty,
            'total_qty'     => $model->number()->total_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'amount'        => $model->number()->amount,
            'remarks'       => $model->remarks,
            'reference_id'  => $model->reference_id ?? null,
            'invoice_qty'   => is_null($model->reference) ? number_format(0, $precision) : $model->reference->number()->total_qty,
            'remaining_qty' => is_null($model->reference) ? number_format(0, $precision) : $model->reference->number()->remaining_qty,
            'spare_qty'     => is_null($model->reference)
                ? number_format(0, $precision)
                : $model->number()->spare_qty
        ];
    }

    protected function inventoryChainDetailImportTransformer($model)
    {
        return [
            'id'            => 0,
            'product_id'    => $model->product_id,
            'barcode_list'  => $model->presenter()->barcodeList,
            'barcode'       => $model->presenter()->unitBarcode,
            'name'          => $model->product->name,
            'chinese_name'  => $model->product->chinese_name,
            'uom'           => [
                'options' => $model->presenter()->unitList,
                'value'   => $model->unit_id
            ],
            'old_unit'      => [
                'id'  => $model->unit_id,
                'qty' => $model->number()->unit_qty
            ],
            'unit_qty'      => $model->number()->unit_qty,
            'qty'           => $model->number()->qty,
            'total_qty'     => $model->number()->total_qty,
            'oprice'        => $model->number()->oprice,
            'price'         => $model->number()->price,
            'discount1'     => $model->number()->discount1,
            'discount2'     => $model->number()->discount2,
            'discount3'     => $model->number()->discount3,
            'discount4'     => $model->number()->discount4,
            'amount'        => $model->number()->amount,
            'remarks'       => $model->remarks,
            'reference_id'  => null,
            'remaining_qty' => 0,
            'invoice_qty '  => 0,
            'manage_type'   => $model->product->manage_type,
        ];
    }

    protected function invoiceDetailTransformer($model, $addQty = 0)
    {
        return [
            'product_id'   => $model->product_id,
            'unit_id'      => $model->computed_unit_id,
            'reference_id' => $model->id,
            'unit_qty'     => $model->computed_unit_qty,
            'qty'          => $model->computed_qty->add(Decimal::create($addQty, 0)),
            'oprice'       => $model->oprice,
            'price'        => $model->price,
            'discount1'    => $model->number()->discount1,
            'discount2'    => $model->number()->discount2,
            'discount3'    => $model->number()->discount3,
            'discount4'    => $model->number()->discount4,
            'remarks'      => $model->remarks
        ];
    }

    protected function invoiceChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->sheet_number,
        ];
    }

    protected function branchDetailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }

    protected function generateReferenceNumber($id, $branch)
    {
        return sprintf('%s%s%s',
            $this->prefix,
            str_pad($branch, 3, '0', STR_PAD_LEFT),
            str_pad($id, 7, '0', STR_PAD_LEFT)
        );
    }

    /**
     * Convert transaction to Array
     *
     * @param  array $transaction
     * @return array
     */
    protected function toArray($transaction)
    {
        $details = [];

        $collection = ! $transaction['detail'] instanceof Collection
            ? collect([])->push($transaction['detail'])
            : $transaction['detail'];

        foreach ($collection as $key => $model) {
            $details[$key] = array_merge($model->toArray(), [
                'uom'       => ['value' => $model->unit_id],
                'price'     => $model->price->innerValue(),
                'oprice'    => $model->oprice->innerValue(),
                'discount1' => $model->discount1->innerValue(),
                'discount2' => $model->discount2->innerValue(),
                'discount3' => $model->discount3->innerValue(),
                'discount4' => $model->discount4->innerValue(),
                'id'        => $model->id ?? 0,
                'qty'       => $model->qty->innerValue(),
                'unit_qty'  => $model->unit_qty->innerValue()
            ]);
        }

        return array(
            'info' => array_merge($transaction['info']->toArray(), [
                'total_amount' => $transaction['info']->total_amount->innerValue(),
                'transaction_date' => $transaction['info']->transaction_date->toDateTimeString(),
            ]),
            'details' => $details
        ) ;
    }

    /**
     * Compute remaining quantity
     *
     * @param int qty
     * @param int unitQty
     * @return int
     */
    public function computeRemaining($qty, $unitQty)
    {
        $precision = setting('monetary.precision');

        return $unitQty->mul(Decimal::create($qty, $precision), $precision);
    }

    /**
     * Determine the column to be used upon updating the product's
     * reserved or requested qty
     * 
     * @param  Invoice $model
     * @return int
     */
    protected function getPendingQuantityColumnByModel($model)
    {
        return in_array(get_class($model), array(
            \Modules\Core\Entities\SalesReturn::class,
            \Modules\Core\Entities\Purchase::class,
            \Modules\Core\Entities\StockRequest::class,
        )) ? 'requested_qty' : 'reserved_qty';
    }

    protected function createProductGrouping($grouping = array(), $component = array(), $productComponent = array())
    {
        $grouping = Factory::create('product_group', $grouping);
        $component = Factory::create('component', $component);

        $productComponent = Factory::create('product_component', 
            array_merge([
                'group_id' => $grouping->id,
                'component_id' => $component->id,
            ], 
            $productComponent));

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $grouping->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        DB::table('product_branch_summary')->insert([
                [
                    'product_id' => $component->id, 
                    'branch_id' => 1, 
                    'branch_detail_id' => 1,
                    'qty' => 0
                ],
            ]);

        return [
            'grouping' => $grouping,
            'component' => $component,
            'productComponent' => $productComponent
        ];
    }
    
    public abstract function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references();
    public abstract function visiting_the_update_page_will_display_the_form_with_its_corresponding_references();
    protected abstract function invoiceInfoTransformer($invoice);
}
