<?php

use Laracasts\TestDummy\Factory;

use Carbon\Carbon;

use Modules\Core\Enums\Customer;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\TransactionType;

class SalesReturnInboundControllerTest extends InventoryChainControllerTest
{
    protected $uri = '/sales-return-inbound';
    protected $prefix = 'SRI';

    protected $tables = [
        'info' => 'sales_return_inbound',
        'detail' => 'sales_return_inbound_detail',
        'invoice_info' => 'sales_return',
        'invoice_detail' => 'sales_return_detail'
    ];

    protected $factories = [
        'info' => 'sales_return_inbound',
        'detail' => 'sales_return_inbound_detail',
        'invoice_info' => 'sales_return',
        'invoice_detail' => 'sales_return_detail'
    ];

    private $customers;
    private $users;

    /**
     * @test
     */
    public function visiting_the_creation_page_will_display_the_creation_form_with_its_corresponding_references()
    {
        $this->visit(sprintf('%s/create', $this->uri))
            ->assertResponseOk()
            ->assertViewHas([
                'customers',
            ]);
    }

    /**
     * @test
     */
    public function saving_an_empty_customer_type_salesman_id_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['customer_type'] = '';
        $data['info']['salesman_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.required')
                ),
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.required')
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_existing_salesman_id_will_trigger_an_error()
    {
        $build = $this->build([
            'salesman_id' => 100,
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.salesman_id' => array(
                    Lang::get('core::validation.salesman.doesnt.exists'),
                )
            )));
    }

    /**
     * @test
     */
    public function saving_a_non_numeric_customer_type_or_term_will_trigger_an_error()
    {
        $build = $this->build();

        $data = $this->toArray($build);

        $data['info']['customer_type'] = 'A';
        $data['info']['term'] = 'A';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.term' => array(
                    Lang::get('core::validation.term.numeric')
                ),
                'info.customer_type' => array(
                    Lang::get('core::validation.customer.type.numeric')
                ),
            )));
    }

    /**
     * @test
     */
    public function customer_id_and_customer_detail_id_will_only_be_required_if_customer_type_is_set_as_regular()
    {
        $build = $this->build([
            'customer_type' => '1'
        ]);

        $data = $this->toArray($build);

        $data['info']['customer_id'] = '';
        $data['info']['customer_detail_id'] = '';

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.required'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_non_existing_customer_id_and_customer_detail_validation_will_only_trigger_if_customer_type_is_set_as_regular()
    {
        $build = $this->build([
            'customer_id' => '99',
            'customer_detail_id' => '99',
            'customer_type' => '1'
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_id' => array(
                    Lang::get('core::validation.customer.doesnt.exists'),
                ),
                'info.customer_detail_id' => array(
                    Lang::get('core::validation.customer.detail.doesnt.exists'),
                ),
            )));
    }

    /**
     * @test
     */
    public function customer_walk_in_name_is_only_required_if_customer_type_is_set_as_walk_in()
    {
        $build = $this->build([
            'customer_id' => 0,
            'customer_detail_id' => 0,
            'customer_walk_in_name' => '',
            'customer_type' => Customer::WALK_IN
        ]);

        $data = $this->toArray($build);

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->errorResponse(array(
                'info.customer_walk_in_name' => array(
                    Lang::get('core::validation.customer.walk.in.name.required'),
                ),
            )));
    }

    /**
     * @test
     */
    public function saving_a_correct_from_invoice_transaction_format_will_successfull_saved_the_data_in_database_generate_a_transaction_number_and_return_the_details_of_the_transaction()
    {

        $customer = $this->customers[rand(0, 2)];

        $createdInvoice = $this->createInvoice(array(
            'customer_type' => Customer::REGULAR ,
            'customer_id'        => $customer->id,
            'customer_detail_id' => $customer->details->first()->id,
        ));

        $build = $this->build(
            array_merge(
                array('transaction_type' => TransactionType::FROM_INVOICE),
                $this->invoiceInfoTransformer($createdInvoice['info'])
            ),
            $this->invoiceDetailTransformer($createdInvoice['detail'])
        );

        $data = $this->toArray($build);

        $expected = array(
            'database' => array(
                'info' => array_merge(array_except($data['info'], array(
                    'total_amount',
                    'audited_by',
                    'audited_date',
                    'transaction_status',
                )), array(
                    'id' => 1,
                )),
                'details' => array(
                    array_merge(
                        array_except($data['details'][0], array(
                            'uom',
                            'barcode_list',
                            'invoice_qty',
                            'amount',
                            'remaining_qty',
                            'total_qty',
                            'barcode',
                            'name',
                            'chinese_name'
                       )),
                        array(
                            'id' => 1,
                            'transaction_id' => 1
                        )
                    )
                )
            ),
            'response' => array_merge(
                $this->item($build['info'], 'inventoryChainBasicTransformer'),
                array(
                    'id' => 1,
                    'sheet_number' => $this->generateReferenceNumber(1, Auth::branch()->id)
                )
            )
        );

        $this->postJson($this->uri, $data)
            ->seeJsonEquals($this->successfulResponse([
                'record' => $expected['response']
            ], Lang::get('core::success.created')))
            ->seeInDatabase($this->tables['info'], $expected['database']['info'])
            ->seeInDatabase($this->tables['detail'], $expected['database']['details'][0]);
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_with_direct_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $data = $this->create(array('customer_id' => Customer::REGULAR));

        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array(),
                'locations' => $this->collection(Auth::branch()->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                ),
                'customer_details' => is_null($data['info']->customer)
                    ? []
                    : $this->collection($data['info']->customer->details, 'customerDetailChosenTransformer'),
            ]));
    }

    /**
     * @test
     */
    public function accessing_an_existing_data_with_from_invoice_transaction_type_through_the_edit_api_will_display_the_full_details_of_the_transaction_with_empty_reference()
    {
        $createdInvoice = $this->createInvoice(array('customer_type' => Customer::REGULAR));

        $data = $this->create(
            $this->invoiceInfoTransformer($createdInvoice['info'])
        );


        $this->getJson(sprintf('%s/%s', $this->uri, $data['info']->id))
            ->seeJsonEquals($this->successfulResponse([
                'record' => $this->item($data['info'], 'transactionFullInfoTransformer'),
                'reference' => array($this->item($createdInvoice['info'], 'invoiceChosenTransformer')),
                'locations' => $this->collection(Auth::branch()->details, 'branchDetailChosenTransformer'),
                'details' => array(
                    'meta' => array(
                        'pagination' => array(
                            'total' => $data['detail']->count(),
                            'count' => $data['detail']->count(),
                            'per_page' => 10,
                            'current_page' => 1,
                            'total_pages' => 1,
                            'links' => []
                        )
                    ),
                    'data' => $this->collection($data['detail'], 'inventoryChainDetailFullTransformer')
                ),
                'customer_details' => is_null($data['info']->customer)
                    ? []
                    : $this->collection($data['info']->customer->details, 'customerDetailChosenTransformer'),
            ]));
    }

    /**
     * @test
     */
    public function visiting_the_update_page_will_display_the_form_with_its_corresponding_references()
    {
        $data = $this->create();

        $this->visit(sprintf('%s/%s/edit', $this->uri, $data['info']->id))
            ->assertResponseOk()
            ->assertViewHas([
                'customers',
            ]);
    }

    /**
     * Build reference data for the transaction
     *
     * @return $this
     */
    protected function references()
    {
        parent::references();

        $this->customers = Factory::times(3)->create('customer');

        foreach ($this->customers as $key => $customer) {
            Factory::times(2)->create('customer_detail', array(
                'head_id' => $customer->id
            ));
        }

        $this->users = Factory::times(3)->create('user');

        return $this;
    }

    /**
     * Build non-persisting transaction model
     *
     * @param  array  $info
     * @param  array  $detail
     * @return array
     */
    protected function build($info = array(), $detail = array())
    {
        $customer = $this->customers[rand(0, 2)];

        $info = Factory::build($this->factories['info'], array_merge(array(
            'created_for'        => Auth::branch()->id,
            'for_location'       => Auth::branch()->details->first()->id,
            'customer_id'        => $customer->id,
            'customer_detail_id' => $customer->details->first()->id,
            'requested_by'       => $this->users[rand(0, 2)]->id,
            'salesman_id'        => $this->users[rand(0, 2)]->id,
            'customer_type'      => Customer::REGULAR
        ), $info));

        $detail = Factory::build($this->factories['detail'], array_merge(array(
            'transaction_id' => $info->id,
            'product_id'     => $this->products[rand(0, 1)]->id,
            'unit_id'        => $this->units[rand(0, 1)]->id,
        ), $detail));

        return array(
            'info'   => $info,
            'detail' => $detail
        );
    }

    public function invoiceInfoTransformer($model)
    {
        return [
            'reference_id'          => $model->id,
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id'    => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'remarks'               => $model->remarks,
            'term'                  => $model->term,
            'requested_by'          => $model->requested_by,

        ];
    }

    protected function transactionBasicTransformer($data)
    {
        return [
            'id'                    => $data->id,
            'transaction_date'      => $data->transaction_date->toDateTimeString(),
            'sheet_number'          => $data->sheet_number,
            'created_for'           => $data->for->name,
            'for_location'          => $data->location->name,
            'salesman'              => $data->salesman->full_name,
            'customer'              => !is_null($data->customer)
                ? $data->customer->name
                : $data->customer_walk_in_name,
            'customer_detail'       => !is_null($data->customerDetail)
                ? $data->customerDetail->name
                : '',
            'remarks'               => $data->remarks,
            'requested_by'          => $data->requested->full_name,
            'created_by'            => $data->creator->full_name,
            'created_date'          => $data->created_at->toDateTimeString(),
            'audited_by'            => !is_null($data->auditor)
                ? $data->auditor->full_name
                : '',
            'audited_date'          => !is_null($data->auditor)
                ? $data->audited_date->toDateTimeString()
                : '',
            'approval_status_label' => $data->presenter()->approval,
            'approval_status'       => $data->approval_status,
            'amount'                => $data->number()->total_amount,
            'reference'             => $data->reference->sheet_number ?? '',
            'deleted'               => $data->presenter()->deleted
        ];
    }

    protected function transactionFullInfoTransformer($model)
    {
        return [
            'id'                    => $model->id,
            'sheet_number'          => $model->sheet_number,
            'transaction_date'      => $model->transaction_date->toDateTimeString(),
            'remarks'               => $model->remarks,
            'requested_by'          => $model->requested_by,
            'salesman_id'           => $model->salesman_id,
            'customer_id'           => !is_null($model->customer_id)
                ? $model->customer_id
                : null,
            'customer_detail_id' => !is_null($model->customer_detail_id)
                ? $model->customer_detail_id
                : null,
            'customer_type'         => $model->customer_type,
            'customer_walk_in_name' => $model->customer_walk_in_name,
            'term'                  => $model->term,
            'created_for'           => $model->created_for,
            'for_location'          => $model->for_location,
            'created_date'          => $model->created_at->toDateTimeString(),
            'modified_date'         => $model->updated_at->toDateTimeString(),
            'approval_status'       => $model->approval_status,
            'approval_status_label' => $model->presenter()->approval,
            'transaction_type'      => $model->transaction_type,
            'created_by'            => $model->creator->full_name,
            'reference_id'          => $model->reference_id != null ? $model->reference_id : null,
            'modified_by'           => is_null($model->modifier) ? '' : $model->modifier->full_name,
        ];
    }

    protected function customerDetailChosenTransformer($model)
    {
        return [
            'id'    => $model->id,
            'label' => $model->name,
        ];
    }
}
