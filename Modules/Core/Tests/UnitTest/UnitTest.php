<?php

use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Modules\Core\Tests\Traits\Application;

abstract class UnitTest extends BaseTestCase
{
    use Application;

    public function setUp()
    {
        parent::setUp();
        $this->baseUrl = env('APP_URL', 'http://localhost/');
    }
    
    /**
     * Assert whether the client was redirected to a given URI given the response
     * 
     * @param  string $uri 
     * @param  Illuminate\Http\RedirectResponse $resp
     * @param  array  $with
     * @return $this      
     */
    protected function assertRedirected($uri, $response, $with = [])
    {
        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $response);

        $this->assertEquals($this->app['url']->to($uri), $response->headers->get('Location'));

        $this->assertSessionHasAll($with);

        return $this;
    }

    protected function mock($class, $args = [])
    {
        if (count($args) > 0) {
            $mock = Mockery::mock($class, $args);
        } else {
            $mock = Mockery::mock($class);
        }

        $this->app->instance($class, $mock);

        return $mock;
    }
}
