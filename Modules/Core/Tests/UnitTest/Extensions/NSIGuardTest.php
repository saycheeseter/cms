<?php

class NSIGuardTest extends UnitTest
{
    protected $guard;
    protected $session;
    protected $provider;
    protected $cookie;
    protected $request;

    protected $guardName = 'nsi';

    public function setUp()
    {
        parent::setUp();

        $this->withoutEvents();

        $this->session = $this->mock(\Illuminate\Session\Store::class);
        $this->provider = $this->mock(\Illuminate\Contracts\Auth\UserProvider::class);
        $this->cookie = $this->mock(\Symfony\Component\HttpFoundation\ParameterBag::class);
        $this->request = $this->mock(\Symfony\Component\HttpFoundation\Request::class);
        $this->request->cookies = $this->cookie;

        $this->guard = new Modules\Core\Extensions\Guard\NSIGuard(
            $this->guardName, 
            $this->provider, 
            $this->session,
            $this->request
        );
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    /**
     * @test
     */
    public function set_config_stores_data_to_session()
    {
        list($branch, $collection) = $this->getMocks();

        $this->session
            ->shouldReceive('put')
            ->with($this->salt('branch'), $branch);

        $this->session
            ->shouldReceive('put')
            ->with($this->salt('permissions'), $collection);
        
        $this->session
            ->shouldReceive('migrate')
            ->twice()
            ->with(true);
        
        $this->guard->setConfig($branch, $collection);
    }

    /**
     * @test
     */
    public function call_to_guard_method_based_on_list_of_config_will_return_a_value_from_session()
    {
        list($branch, $collection) = $this->getMocks();

        $this->session
            ->shouldReceive('get')
            ->with($this->salt('branch'))
            ->andReturn($branch);
        
        $this->session
            ->shouldReceive('get')
            ->with($this->salt('permissions'))
            ->andReturn($collection);
        
        $this->assertEquals($branch, $this->guard->branch());
        $this->assertEquals($collection, $this->guard->permissions());
    }

    /**
     * @test
     */
    public function call_to_undefined_method_which_is_also_not_in_list_of_config_will_throw_an_exception()
    {
        $this->expectException('Exception', 'Undefined method call.');
        $this->guard->leverage();
    }

    /**
     * @test
     */
    public function call_to_methods_in_list_of_config_will_retrieve_the_data_once_in_session_storage_and_cache()
    {
        list($branch, $collection) = $this->getMocks();

        $this->session
            ->shouldReceive('get')
            ->with($this->salt('branch'))
            ->once()
            ->andReturn($branch);

        $this->guard->branch();
        $this->guard->branch();
    }

    /**
     * @test
     */
    public function call_to_guard_check_will_return_false_if_user_is_not_authenticated_or_all_config_session_is_not_set()
    {
        // Partial mock user method from SessionGuard
        $guard = $this->mock('Modules\Core\Extensions\Guard\NSIGuard[user]', array(
            $this->guardName, 
            $this->provider, 
            $this->session,
            $this->request
        ));

        $guard->shouldReceive('user')->once()->andReturn(null);

        $this->session
            ->shouldReceive('get')
            ->with($this->salt('branch', $guard))
            ->andReturn('text');
        
        $this->session
            ->shouldReceive('get')
            ->with($this->salt('permissions', $guard))
            ->andReturn(null);
        
        $this->assertFalse($guard->check());
    }

    /**
     * @test
     */
    public function call_to_guard_check_will_return_true_if_user_is_authenticated_and_all_config_session_is_set()
    {
        // Partial mock user method from SessionGuard
        $guard = $this->mock('Modules\Core\Extensions\Guard\NSIGuard[user]', array(
            $this->guardName, 
            $this->provider, 
            $this->session,
            $this->request
        ));

        $guard->shouldReceive('user')->once()->andReturn('user');

        $this->session
            ->shouldReceive('get')
            ->with($this->salt('branch', $guard))
            ->andReturn('text');
        
        $this->session
            ->shouldReceive('get')
            ->with($this->salt('permissions', $guard))
            ->andReturn('text');
        
        $this->assertTrue($guard->check());
    }

    /**
     * @test
     */
    public function logout_will_clear_session_data()
    {
        // Partial mock user method from SessionGuard
        $guard = $this->mock('Modules\Core\Extensions\Guard\NSIGuard[user]', array(
            $this->guardName, 
            $this->provider, 
            $this->session,
            $this->request
        ));

        $guard->shouldReceive('user')->andReturn('user');

        // Mock parent logout behavior
        $this->session
            ->shouldReceive('remove')
            ->with($this->salt('login', $guard));

        $this->cookie
            ->shouldReceive('get')
            ->with($this->salt('remember', $guard))
            ->andReturn(null);

        $this->session
            ->shouldReceive('remove')
            ->with($this->salt('branch', $guard));

        $this->session
            ->shouldReceive('remove')
            ->with($this->salt('permissions', $guard));
        
        $guard->logout();
    }

    protected function getMocks()
    {
        return array(
            $this->mock(\Modules\Core\Entities\Branch::class),
            $this->mock(\Illuminate\Database\Eloquent\Collection::class)
        );
    }

    protected function salt($name, $guard = null)
    {   
        $guard = $guard ?? $this->guard;

        return $name.'_'.$this->guardName.'_'.sha1(get_class($guard));
    }
}
