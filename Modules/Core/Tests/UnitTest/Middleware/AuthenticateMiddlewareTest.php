<?php

use Modules\Core\Http\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateMiddlewareTest extends UnitTest
{
    protected $request;
    protected $middleware;
    protected $closure;
    protected $auth;

    public function setUp()
    {
        parent::setUp();
        $this->request = $this->mock(Request::class);
        $this->middleware = new Authenticate;
        $this->closure = (function() { return 'next'; });
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    /**
     * @test
     */
    public function allow_request_if_current_url_is_excluded()
    {
        $urls = ['auth', 'logout'];

        foreach ($urls as $key => $value) {
            $this->setRequestSegment($value);
            $this->assertEquals('next', $this->runMiddleware());
        }
    }

    /**
     * @test
     */
    public function redirect_to_dashboard_if_url_is_login_and_still_authenticated()
    {
        $this->setRequestSegment(null, 2);
        $this->setGuardCheck(true);
        $this->assertRedirected('/dashboard', $this->runMiddleware());
    }

    /**
     * @test
     */
    public function allow_request_if_url_is_login_and_not_authenticated()
    {
        $this->setRequestSegment(null, 2);
        $this->setGuardCheck(false);
        $this->assertEquals('next', $this->runMiddleware());
    }

    /**
     * @test
     */
    public function allow_request_if_url_is_not_login_and_excluded_given_that_user_is_still_authenticated()
    {
        $this->setRequestSegment('brand', 2);
        $this->setGuardCheck(true);
        $this->assertEquals('next', $this->runMiddleware());
    }

    /**
     * @test
     */
    public function redirect_to_login_if_not_excluded_url_and_not_authenticated()
    {
        $this->setRequestSegment('brand', 2);
        $this->setGuardCheck(false);
        $this->assertRedirected('/', $this->runMiddleware());
    }

    /**
     * Execute middleware handle
     * 
     * @return Closure | Illuminate\Http\RedirectResponse
     */
    protected function runMiddleware()
    {
        return $this->middleware->handle($this->request, $this->closure);
    }

    /**
     * Set mock path for request
     * 
     * @param string $path 
     * @param string $times
     */
    protected function setRequestSegment($path, $times = 1)
    {
        $this->request
            ->shouldReceive('segment')
            ->times($times)
            ->andReturn($path);
    }

    /**
     * Set mock check to given boolean value
     * 
     * @param boolean $value
     */
    protected function setGuardCheck($value = true)
    {
        Auth::shouldReceive('guard')
            ->with(null)
            ->once()
            ->andReturnSelf()
            ->shouldReceive('check')
            ->andReturn($value);
    }
}
