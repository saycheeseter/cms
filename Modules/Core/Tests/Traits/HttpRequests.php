<?php

namespace Modules\Core\Tests\Traits;

trait HttpRequests
{
    /**
     * Visit the given URI with a GET request, expecting a JSON response.
     * (Override)
     * 
     * @param  string  $uri
     * @param  array  $headers
     * @return $this
     */
    public function getJson($uri, array $data = [], array $headers = [])
    {
        return $this->json('GET', $uri, $data, $headers);
    }
}