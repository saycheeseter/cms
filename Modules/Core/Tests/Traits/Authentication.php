<?php

namespace Modules\Core\Tests\Traits;

use Auth;

trait Authentication
{
    /**
     * Automatically log-in dummy user
     * 
     * @return $this
     */
    protected function login($id = 1, $branch = 1)
    {
        Auth::loginUsingId($id);

        $this->postJson('/auth', ['id' => $branch]);

        return $this;
    }

    /**
     * Logout user
     * 
     * @return $this
     */
    protected function logout()
    {
        $this->getJson('/logout');

        return $this;
    }
}