<?php

namespace Modules\Core\Tests\Traits;

trait Capture
{
    protected $screenshotDir;
    protected $method;

    protected function snap()
    {
        if (!is_dir($this->screenshotDir)) {
            mkdir($this->screenshotDir);
        }

        $filedata   = $this->currentScreenshot();
        $file       = $this->screenshotDir.'/'.get_class($this).'_'.$this->method.'.png';
        
        file_put_contents($file, $filedata);

        return $this;
    }
}