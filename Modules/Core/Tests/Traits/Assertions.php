<?php

namespace Modules\Core\Tests\Traits;

use Schema;

trait Assertions
{
    /**
     * Assert whether the client was redirected to a given URI given the response
     * 
     * @param  string $uri 
     * @param  Illuminate\Http\RedirectResponse $resp
     * @param  array  $with
     * @return $this
     */
    protected function assertRedirected($uri, $response, $with = [])
    {
        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $response);

        $this->assertEquals($this->app['url']->to($uri), $response->headers->get('Location'));

        $this->assertSessionHasAll($with);

        return $this;
    }

    /**
     * Assert if the given column exists in the specified table
     * 
     * @param  string $table
     * @param  string $field
     * @return $this
     */
    protected function assertTableHasColumn($table, $field)
    {
        $this->assertTrue(Schema::hasColumn($table, $field), "Failed to assert that column $field exists in the specified table $table");

        return $this;
    }

    /**
     * Assert if the given column does not exists in the specified table
     * 
     * @param  string $table
     * @param  string $field
     * @return $this
     */
    protected function assertTableHasNoColumn($table, $field)
    {
        $this->assertFalse(Schema::hasColumn($table, $field), "Failed to assert that column $field doesn\'t exists in the specified table $table");

        return $this;
    }

    /**
     * Assert the the given column is of the specified data type
     * 
     * @param  string $table
     * @param  string $column
     * @param  string $type
     * @return Boolean
     */
    protected function assertTableColumnIsOfType($table, $column, $type)
    {
        $this->assertTrue(Schema::getColumnType($table, $column) === $type, "Failed to assert that column $column is of type $type in the specified table $table");

        return $this;
    }

    /**
     * Assert the the given column is not of the specified data type
     * 
     * @param  string $table
     * @param  string $column
     * @param  string $type
     * @return Boolean
     */
    protected function assertTableColumnIsNotOfType($table, $column, $type)
    {
        $this->assertFalse(Schema::getColumnType($table, $column) === $type, "Failed to assert that column $column is not of type $type in the specified table $table");

        return $this;
    }
}