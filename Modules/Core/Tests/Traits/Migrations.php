<?php

namespace Modules\Core\Tests\Traits;

use Illuminate\Database\QueryException;
use DB;

trait Migrations
{
    /**
     * Execute table migrations
     * 
     */
    protected function runMigrations()
    {
        $this->artisan('module:migrate', [
            'module' => 'Core'
        ]);

        return $this;
    }

    /**
     * Run initial data seeder
     * 
     */
    protected function init()
    {
        $this->artisan('module:seed', [
            'module' => 'Core'
        ]);

        return $this;
    }

    protected function resetTableIndex()
    {
        $connection = env('DB_CONNECTION');

        if (is_null($connection)) {
            throw new Exception('No connection provided!');
        }

        foreach ($this->getTableList() as $key => $table) {
            try {
                switch ($connection) {
                    case 'mysql':
                        DB::statement(sprintf('ALTER TABLE %s AUTO_INCREMENT=1', $table));
                        break;
                    
                    case 'sqlsrv':
                        DB::statement(sprintf('DBCC CHECKIDENT (%s, RESEED, 1)', $table));
                        break;

                    case 'sqlite':
                        DB::statement(sprintf('UPDATE SQLITE_SEQUENCE SET SEQ=1 WHERE NAME=\'%s\'', $table));
                        break;
                }
            } catch (QueryException $e) {
                continue;
            }
        }
    }

    protected function getTableList()
    {
        $connection = env('DB_CONNECTION');

        $tables = [];

        switch ($connection) {
            case 'mysql':
                $result = DB::select('SHOW TABLES');
                $property = 'Tables_in_test';
                break;
            
            case 'sqlite':
                $result = DB::select('SELECT name FROM sqlite_master WHERE type=\'table\'');
                $property = 'name';
                break;

            case 'sqlsrv':
                $result = DB::select('SELECT * FROM INFORMATION_SCHEMA.TABLES');
                $property = 'TABLE_NAME';
                break;
        }

        foreach ($result as $key => $table) {
            $tables[] = $table->{$property};
        }

        return $tables;
    }
}