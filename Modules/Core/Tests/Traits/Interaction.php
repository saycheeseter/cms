<?php

namespace Modules\Core\Tests\Traits;

trait Interaction 
{
    /**
     * Assert that we see text within the specified selector
     *
     * @param $text
     * @param string $selector
     * @return $this
     */
    protected function see($text, $selector = '')
    {
        $this->assertContains($text, $this->findElement($selector)->text());

        return $this;
    }

    /**
     * User should not be able to see element.
     *
     * @param $text
     * @param string $selector
     * @return $this
     */
    protected function notSee($text, $selector = '')
    {
        $this->assertContains($text, $this->byselector($selector)->text());

        return $this;
    }

    /**
     * User should not be able to see element.
     *
     * @param string $text
     * @param string $selector
     * @return $this
     */
    protected function dontSee($text, $selector = '')
    {
        $this->assertNotContains($text, $this->findElement($selector)->text());

        return $this;
    }

    /**
     * Assert if the title of the page is equal to the expected 
     * value
     * 
     * @param  string $title
     * @return $this
     */
    protected function seeTitle($title)
    {
        $this->assertEquals($title, $this->title());

        return $this;
    }

    /**
     * Trigger click by the given name
     * 
     * @param  string $name
     * @return $this
     */
    protected function clickByName($name)
    {
        $this->triggerClick($this->byName($name));

        return $this;
    }

    /**
     * Trigger click by the given id
     * 
     * @param  string $id
     * @return $this
     */
    protected function clickById($id)
    {
        $this->triggerClick($this->byId($id));

        return $this;
    }

    /**
     * Trigger click by the given css selector
     * 
     * @param  string $css
     */
    protected function clickByCss($css)
    {
        $this->triggerClick($this->byCssSelector($css));

        return $this;
    }

    /**
     * Trigger click by the given xpath
     * 
     * @param  string $xpath
     * @return $this
     */
    protected function clickByXpath($xpath)
    {
        $this->triggerClick($this->byXPath($xpath));

        return $this;
    }

    /**
     * Find the dropdown field by xpath and select the value
     * @param  string $xpath
     * @param  string $value 
     * @return $this         
     */
    protected function selectByXpath($xpath, $value)
    {
        $this->findElement($value, $xpath)->value($value);

        return $this;
    }

    /**
     * Type a value into a form input by that inputs xpath
     *
     * @param $value
     * @param $xpath
     * @param bool $clear Whether or not to clear the input first on say an edit form
     *
     * @return $this
     */
    protected function typeByXpath($value, $xpath, $clear = false)
    {
        $element = $this->findElement($value, $xpath);

        if ($clear) {
            $element->clear();
        }

        $element->value($value);

        return $this;
    }

    /**
     * Execute click event for the specified element
     * 
     * @param  string $element
     */
    private function triggerClick($element)
    {
        try {
            $element->click();
        } catch (\Exception $e) {
            throw new CannotClickElement('Cannot click the element with the text: '.$textOrId);
        }
    }
}