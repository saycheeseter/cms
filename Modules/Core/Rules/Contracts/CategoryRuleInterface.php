<?php

namespace Modules\Core\Rules\Contracts;

interface CategoryRuleInterface extends RuleInterface
{
    public function destroy($id = 0);
}