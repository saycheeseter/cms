<?php

namespace Modules\Core\Rules\Contracts;

interface InventoryAdjustRuleInterface extends InventoryFlowRuleInterface
{
    public function checkInventoryDiscrepancy($request);
}
