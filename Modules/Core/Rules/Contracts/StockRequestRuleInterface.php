<?php

namespace Modules\Core\Rules\Contracts;

interface StockRequestRuleInterface extends InvoiceRuleInterface
{
    public function warning($data);
}
