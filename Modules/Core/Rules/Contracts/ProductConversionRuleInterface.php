<?php

namespace Modules\Core\Rules\Contracts;

interface ProductConversionRuleInterface extends RuleInterface
{
    public function store($args);
    public function update($args, $id);
    public function destroy($id);
    public function approval($transactions, $status);
}
