<?php

namespace Modules\Core\Rules\Contracts;

interface TransactionSummaryPostingRuleInterface extends RuleInterface
{
    public function generateSummary($date);
}