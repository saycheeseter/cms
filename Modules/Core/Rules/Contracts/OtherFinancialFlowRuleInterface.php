<?php

namespace Modules\Core\Rules\Contracts;

interface OtherFinancialFlowRuleInterface
{
    public function store($request = []);
    public function update($request = [], $id = 0);
    public function destroy($id = 0);
}