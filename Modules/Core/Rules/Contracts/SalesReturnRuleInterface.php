<?php 

namespace Modules\Core\Rules\Contracts;

interface SalesReturnRuleInterface extends InvoiceRuleInterface
{
    public function store($data);
}