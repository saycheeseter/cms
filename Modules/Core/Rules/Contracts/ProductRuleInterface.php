<?php 

namespace Modules\Core\Rules\Contracts;

interface ProductRuleInterface extends RuleInterface
{
    public function selectWithSupplierPrice($request);
    public function selectWithCustomerPrice($request);
    public function selectWithSummaryAndPrice($request);
    public function update($request, $id = 0);
    public function store($args = []);
}