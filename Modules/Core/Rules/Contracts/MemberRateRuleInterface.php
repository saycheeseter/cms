<?php 

namespace Modules\Core\Rules\Contracts;

interface MemberRateRuleInterface extends RuleInterface
{
    public function store($request);
    public function update($request, $id = 0);
    public function destroy($id = 0);
}
