<?php

namespace Modules\Core\Rules\Contracts;

interface RoleRuleInterface extends RuleInterface
{
    public function destroy($id);
}
