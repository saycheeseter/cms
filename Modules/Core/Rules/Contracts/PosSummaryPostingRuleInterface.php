<?php

namespace Modules\Core\Rules\Contracts;

interface PosSummaryPostingRuleInterface extends RuleInterface
{
    public function generateSummary($id, $date);
}