<?php

namespace Modules\Core\Rules\Contracts;

interface PosSalesRuleInterface extends RuleInterface
{
    public function sync($request = []);
}