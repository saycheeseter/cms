<?php

namespace Modules\Core\Rules\Contracts;

interface RuleInterface
{
    public function passes();
    public function fails();
    public function errors();
}