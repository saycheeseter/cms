<?php

namespace Modules\Core\Rules\Contracts;

interface GiftCheckRuleInterface extends RuleInterface
{
    public function update($request, $id);
    public function bulkEntry($data);
}