<?php 

namespace Modules\Core\Rules\Contracts;

interface ProductDiscountRuleInterface extends RuleInterface
{
    public function store($request = []);
    public function update($request = [], $id = 0);
}