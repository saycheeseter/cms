<?php

namespace Modules\Core\Rules\Contracts;

interface UserRuleInterface extends RuleInterface
{
    public function store($data);
    public function update($data, $id);
    public function changeCurrentUserPassword($data);
}