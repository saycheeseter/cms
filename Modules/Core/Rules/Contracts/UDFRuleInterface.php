<?php

namespace Modules\Core\Rules\Contracts;

interface UDFRuleInterface extends RuleInterface
{
    public function update($data, $id);
    public function destroy($id);
}
