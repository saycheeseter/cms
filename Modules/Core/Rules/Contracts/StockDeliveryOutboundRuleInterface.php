<?php

namespace Modules\Core\Rules\Contracts;

interface StockDeliveryOutboundRuleInterface extends InventoryChainRuleInterface
{
    public function warning($data);
}
