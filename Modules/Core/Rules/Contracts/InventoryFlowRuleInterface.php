<?php

namespace Modules\Core\Rules\Contracts;

interface InventoryFlowRuleInterface extends RuleInterface
{
    public function update($args, $id);
    public function destroy($id);
    public function approval($transactions, $status);
}
