<?php 

namespace Modules\Core\Rules\Contracts;

interface SalesRuleInterface extends InvoiceRuleInterface
{
    public function store($data);
}