<?php

namespace Modules\Core\Rules\Contracts;

interface GenericSettingRuleInterface extends RuleInterface
{
    public function update($data);
}