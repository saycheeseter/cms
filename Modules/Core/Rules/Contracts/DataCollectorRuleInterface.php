<?php

namespace Modules\Core\Rules\Contracts;

interface DataCollectorRuleInterface extends RuleInterface
{
    public function destroyByReferenceNumber($referenceNumber = 0);
    public function destroy($id);
}