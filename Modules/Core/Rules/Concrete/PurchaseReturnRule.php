<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\PurchaseReturnRuleInterface;
use Modules\Core\Repositories\Contracts\PurchaseReturnRepository;

class PurchaseReturnRule extends InvoiceRule implements PurchaseReturnRuleInterface
{
    public function __construct(PurchaseReturnRepository $repository)
    {
        $this->repository = $repository;
    }
}