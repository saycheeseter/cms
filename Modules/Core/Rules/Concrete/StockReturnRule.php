<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\StockReturnRuleInterface;
use Modules\Core\Repositories\Contracts\StockReturnRepository;

class StockReturnRule extends InvoiceRule implements StockReturnRuleInterface
{
    public function __construct(StockReturnRepository $repository)
    {
        $this->repository = $repository;
    }
}