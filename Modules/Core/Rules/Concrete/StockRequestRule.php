<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\StockRequestRuleInterface;
use Modules\Core\Repositories\Contracts\StockRequestRepository;
use Modules\Core\Repositories\Contracts\StockDeliveryRepository;
use Modules\Core\Enums\TransactionStatus;
use Carbon\Carbon;
use App;
use Lang;

class StockRequestRule extends InvoiceRule implements StockRequestRuleInterface
{
    public function __construct(StockRequestRepository $repository)
    {
        $this->repository = $repository;
    }

    public function warning($data)
    {   
        $this->checkRequestedButNotDeliveredProducts($data);
        $this->checkRequestedAndDeliveredProducts($data);

        return $this;
    }

    private function checkRequestedButNotDeliveredProducts($data)
    {
        if(!setting('request.warning.already.requested.not.delivered.toggle')) {
            return false;
        }

        $messages = [];

        $days = setting('request.warning.already.requested.not.delivered.days');

        $productIds = array_pluck($data['details'], 'product_id');

        $details = collect($data['details'])->mapWithKeys(function ($item) {
            return [$item['product_id'] => (object) $item];
        });

        $results = $this->repository->findOtherRequestedProducts($productIds, [
            'branch' => $data['info']['created_for'],
            'days' => $days,
            'delivery_status' => TransactionStatus::NO_DELIVERY
        ], $data['info']['id']);

        foreach ($results as $key => $transaction) {
            $messages[] = sprintf('%s (%s) : %s', 
                $details->get($transaction->product_id)->name,
                $transaction->sheet_number,
                Lang::get('core::validation.warning.product.already.requested.not.delivered')
            );
        }

        if(count($messages) >  0) {
            $this->message(array(
                'warning.requested.not.delivered.message' => $messages
            ));
        }
        
        return $this;
    }

    private function checkRequestedAndDeliveredProducts($data)
    {
        if(!setting('request.warning.already.requested.delivered.toggle')) {
            return false;
        }

        $messages = [];

        $days = setting('request.warning.already.requested.delivered.days');

        $productIds = array_pluck($data['details'], 'product_id');

        $details = collect($data['details'])->mapWithKeys(function ($item) {
            return [$item['product_id'] => (object) $item];
        });

        $results = $this->repository->findOtherRequestedProducts($productIds, [
            'branch' => $data['info']['created_for'],
            'days' => $days,
            'delivery_status' => TransactionStatus::INCOMPLETE
        ], $data['info']['id']);

        foreach ($results as $key => $transaction) {
            $messages[] = sprintf('%s (%s) : %s', 
                $details->get($transaction->product_id)->name,
                $transaction->sheet_number,
                Lang::get('core::validation.warning.product.already.requested.delivered')
            );
        }

        if(count($messages) >  0) {
            $this->message(array(
                'warning.requested.and.delivered.message' => $messages
            ));
        }
        
        return $this;
    }
}
