<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\CategoryRuleInterface;
use Lang;

class CategoryRule extends Rule implements CategoryRuleInterface
{
    public function destroy($id = 0)
    {
        if ($this->withChildren($id)) {
            $this->message(array(
                'category.cant.delete' => Lang::get('core::validation.parent.cannot.delete')
            ));
        }

        return $this;
    }

    private function withChildren($id)
    {
        $repository = resolve(\Modules\Core\Repositories\Contracts\CategoryRepository::class);

        $count = $repository->children($id);

        if ($count > 0) {
            return true;
        }

        return false;
    }
}