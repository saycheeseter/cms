<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Repositories\Contracts\OtherIncomeRepository;
use Modules\Core\Rules\Contracts\OtherIncomeRuleInterface;

class OtherIncomeRule extends OtherFinancialFlowRule implements OtherIncomeRuleInterface
{
    public function __construct(OtherIncomeRepository $repository)
    {
        $this->repository = $repository;
    }
}