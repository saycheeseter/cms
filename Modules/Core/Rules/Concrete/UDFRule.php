<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\UDFRuleInterface;
use Modules\Core\Repositories\Contracts\UserDefinedFieldRepository;
use DB;
use Lang;

class UDFRule extends Rule implements UDFRuleInterface
{
    public function __construct(UserDefinedFieldRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Validates update method if current udf module already had a data
     * 
     * @param  array $data
     * @param  int $id
     * @return $this
     */
    public function update($data, $id)
    {
        $old = $this->repository->find($id);

        if ($this->isTableHasTransactions($old->table) && $old->type != $data['type']) {
            $this->message(array(
                'udf.update.invalid' => Lang::get('core::validation.cannot.change.udf.type')
            ));
        }

        return $this;
    }

    /**
     * Validates destroy method if current udf module already had a data
     * 
     * @param  array $data
     * @param  int $id
     * @return $this
     */
    public function destroy($id)
    {
        $old = $this->repository->find($id);

        if (!$old) {
            $this->message(array(
                'udf.doesnt.exists' => Lang::get('core::validation.udf.doesnt.exists')
            ));
            return $this;
        }

        if ($this->isTableHasTransactions($old->table)) {
            $this->message(array(
                'udf.delete.invalid' => Lang::get('core::validation.cannot.delete.udf')
            ));
        }

        return $this;
    }

    private function isTableHasTransactions($table)
    {
        $data = DB::table($table)->take(1)->get();

        return $data->count() > 0;
    }
}
