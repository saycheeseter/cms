<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Repositories\Contracts\OtherPaymentRepository;
use Modules\Core\Rules\Contracts\OtherPaymentRuleInterface;

class OtherPaymentRule extends OtherFinancialFlowRule implements OtherPaymentRuleInterface
{
    public function __construct(OtherPaymentRepository $repository)
    {
        $this->repository = $repository;
    }
}