<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\SalesRuleInterface;
use Modules\Core\Repositories\Contracts\SalesRepository;
use Modules\Core\Enums\Customer;
use Validator;
use Lang;

class SalesRule extends InvoiceRule implements SalesRuleInterface
{
    public function __construct(SalesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store($request = [])
    {
        $this->validateCustomerExists($request);

        return $this;
    }

    public function update($request = [], $id = 0)
    {
        parent::update($request, $id);

        $this->validateCustomerExists($request);

        return $this;
    }

    private function validateCustomerExists($request)
    {
        $info = $request['info'];

        if ((int) $info['customer_type'] !== Customer::REGULAR) {
            return $this;
        }

        $validation = Validator::make($request, [
            'info.customer_id' => 'exists_in:customer,id,deleted_at',
            'info.customer_detail_id' => 'exists_in:customer_detail,id,deleted_at',
        ], [
            'info.customer_id.exists_in' => Lang::get('core::validation.customer.doesnt.exists'),
            'info.customer_detail_id.exists_in' => Lang::get('core::validation.customer.detail.doesnt.exists'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }
}