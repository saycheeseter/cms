<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\DamageRuleInterface;
use Modules\Core\Repositories\Contracts\DamageRepository;

class DamageRule extends InvoiceRule implements DamageRuleInterface
{
    public function __construct(DamageRepository $repository)
    {
        $this->repository = $repository;
    }
}
