<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\CollectionRuleInterface;
use Modules\Core\Repositories\Contracts\CollectionRepository;
use Modules\Core\Enums\Customer;

class CollectionRule extends FinancialFlowRule implements CollectionRuleInterface
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function checkRequiredFields($request)
    {
        parent::checkRequiredFields($request);

        $this->validateCustomerExists($request);

        return $this;
    }

    private function validateCustomerExists($request)
    {
        $info = $request['info'];

        if ((int) $info['customer_type'] !== Customer::REGULAR) {
            return $this;
        }

        $validation = Validator::make($request, [
            'info.customer_id' => 'exists_in:customer,id,deleted_at',
        ], [
            'info.customer_id.exists_in' => Lang::get('core::validation.customer.doesnt.exists'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }
}