<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\PaymentRuleInterface;
use Modules\Core\Repositories\Contracts\PaymentRepository;

class PaymentRule extends FinancialFlowRule implements PaymentRuleInterface
{
    protected $repository;

    public function __construct(PaymentRepository $repository)
    {
        $this->repository = $repository;
    }
}