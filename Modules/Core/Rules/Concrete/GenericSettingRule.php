<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Rules\Contracts\GenericSettingRuleInterface;
use Lang;

class GenericSettingRule extends Rule implements GenericSettingRuleInterface
{
    public function update($data)
    {
        $this->validateTransactionSummaryComputation($data['transaction_summary_computation_mode']);

        return $this;
    }

    private function validateTransactionSummaryComputation($mode)
    {
        if ($this->isFromManualToAutomaticComputation($mode)) {
            $posting = TransactionSummaryPosting::first();

            if ($posting) {
                $this->message([
                    'cannot.change.computation.mode' => Lang::get('core::validation.cannot.revert.mode.due.to.pending.transaction')
                ]);
            }
        }
    }

    private function isFromManualToAutomaticComputation($mode)
    {
        return $mode == TransactionSummaryComputation::AUTOMATIC
            && setting('transaction.summary.computation.mode') == TransactionSummaryComputation::MANUAL;
    }
}