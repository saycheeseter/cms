<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Entities\User;
use Modules\Core\Rules\Contracts\UserRuleInterface;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface as LicenseApiService;
use Lang;
use App;
use Hash;
use Auth;


class UserRule extends Rule implements UserRuleInterface
{
    public function changeCurrentUserPassword($data)
    {
        $this->validateOldPassword($data['old_password']);
        $this->validateSamePassword($data['old_password'], $data['new_password']);

        return $this;
    }

    public function store($data)
    {
        $this->validateLicenseKey($data);

        return $this;
    }

    public function update($data, $id)
    {
        $this->validateLicenseKey($data, $id);

        return $this;
    }

    private function validateLicenseKey($data, $id = null)
    {
        if (config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false) {
            return;
        }

        $user = is_null($id)
            ? null
            : User::find($id);

        if(!empty($data['info']['license_key'])
            && (is_null($user) || (!is_null($user) && $user->license_key !== trim($data['info']['license_key'])))
        ) {
            $validate = App::make(LicenseApiService::class)->isValidKey($data['info']['license_key']);

            if(!$validate) {
                $this->message(array(
                    'invalid.license.key' => Lang::get('core::error.license.key.invalid')
                ));
            }
        }
    }

    private function validateOldPassword(string $oldPassword)
    {
        if(!Hash::check($oldPassword, Auth::user()->password)) {
            $this->message(array(
                'old.password.invalid' => Lang::get('core::validation.old.password.incorrect')
            ));
        }
    }

    private function validateSamePassword($oldPassword, $newPassword)
    {
        if($oldPassword == $newPassword) {
            $this->message(array(
                'cant.change.same.password' => Lang::get('core::validation.cant.change.same.password')
            ));
        }
    }
}