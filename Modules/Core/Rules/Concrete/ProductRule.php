<?php 

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Repositories\Contracts\ProductRepository;
use Modules\Core\Rules\Contracts\ProductRuleInterface;
use Modules\Core\Enums\ProductType;
use Modules\Core\Enums\GroupType;
use Validator;
use Lang;

class ProductRule extends Rule implements ProductRuleInterface
{
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function selectWithSupplierPrice($request)
    {
        $validation = Validator::make($request, [
            'product_ids' => [
                'required',
                'min:1'
            ],
            'supplier_id' => [
                'required',
                'exists_in:supplier,id,deleted_at'
            ],
            'branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'location_id' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
        ], [
            'product_ids.required' => Lang::get('core::validation.product.ids.required'),
            'product_ids.min' => Lang::get('core::validation.product.ids.min.length'),
            'supplier_id.required' => Lang::get('core::validation.supplier.required'),
            'supplier_id.exists_in' => Lang::get('core::validation.supplier.doesnt.exists'),
            'branch_id.required' => Lang::get('core::validation.branch.required'),
            'branch_id.exists_in' => Lang::get('core::validation.branch.doesnt.exists'),
            'location_id.required' => Lang::get('core::validation.for.location.required'),
            'location_id.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }

    public function selectWithCustomerPrice($request)
    {
        $validation = Validator::make($request, [
            'product_ids' => [
                'required',
                'min:1'
            ],
            'customer_id' => [
                'required',
                'exists_in:customer,id,deleted_at',
            ],
            'branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'location_id' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
        ], [
            'product_ids.required' => Lang::get('core::validation.product.ids.required'),
            'product_ids.min' => Lang::get('core::validation.product.ids.min.length'),
            'customer_id.required' => Lang::get('core::validation.customer.required'),
            'customer_id.exists_in' => Lang::get('core::validation.customer.doesnt.exists'),
            'branch_id.required' => Lang::get('core::validation.branch.required'),
            'branch_id.exists_in' => Lang::get('core::validation.branch.doesnt.exists'),
            'location_id.required' => Lang::get('core::validation.for.location.required'),
            'location_id.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }

    public function selectWithSummaryAndPrice($request)
    {
        $validation = Validator::make($request, [
            'product_ids' => [
                'required',
                'min:1'
            ],
            'branch_id' => [
                'required',
                'exists_in:branch,id,deleted_at'
            ],
            'location_id' => [
                'required',
                'exists_in:branch_detail,id,deleted_at'
            ],
            'price' => [
                'required',
                'digits_range:1,8'
            ]
        ], [
            'product_ids.required' => Lang::get('core::validation.product.ids.required'),
            'product_ids.min' => Lang::get('core::validation.product.ids.min.length'),
            'branch_id.required' => Lang::get('core::validation.branch.required'),
            'branch_id.exists_in' => Lang::get('core::validation.branch.doesnt.exists'),
            'location_id.required' => Lang::get('core::validation.for.location.required'),
            'location_id.exists_in' => Lang::get('core::validation.for.location.doesnt.exists'),
            'price.required' => Lang::get('core::validation.price.required'),
            'price.digits_range' => Lang::get('core::validation.price.scheme.invalid'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }

    public function store($request = [])
    {
        $this->validateMinMax($request);

        if($request['product']['group_type'] != GroupType::NONE) {
            $this->validateComponentsArrayBasedOnGroupType($request);
            $this->validateComponentsIfAlreadyGroup($request);
        }

        return $this;
    }

    public function update($request = [], $id = 0)
    {
        $this->validateMinMax($request);

        if($request['product']['group_type'] != GroupType::NONE) {
            $this->validateComponentsArrayBasedOnGroupType($request);
            $this->validateIfAlreadyComponent($id);
            $this->validateComponentsIfAlreadyGroup($request);
        }

        $this->validateManagementTypeIsEditable($request, $id);

        return $this;
    }

    private function validateMinMax($request)
    {
        foreach ($request['locations'] as $key => $value) {
            if($value['min'] > $value['max']) {
                $this->message(array(
                    'locations.'.$key.'.min' => [
                        sprintf("%s: %s[%s]. %s",
                            Lang::get('core::label.location'),
                            Lang::get('core::error.row'),
                            ($key + 1),
                            Lang::get('core::validation.min.max.incorrect')
                        )
                    ]
                ));
            }
        }
    }

    private function validateManagementTypeIsEditable($request, $id)
    {
        $product = $this->repository->find($id);

        if ($product->is_used && $product->manage_type != $request['product']['manage_type']) {
            $this->message(array(
                'cannot.change.manage.type' => Lang::get('core::validation.cannot.change.manage.type')
            ));
        }

        return $this;
    }

    private function validateComponentsArrayBasedOnGroupType($request) 
    {
        if(sizeof($request['components']) == 0) {
            $this->message(array(
                'components.min.one' => Lang::get('core::validation.components.min.one')
            ));
        }

        return $this;
    }

    private function validateComponentsIfAlreadyGroup($request)
    {   
        $ids = array_pluck($request['components'], 'component_id');

        $products = $this->repository->findGroupsById($ids);

        foreach ($products as $key => $product) {
            $this->message(array(
                'product.already.group.['.$key.']' => $product->name.': '.Lang::get('core::validation.product.already.group')
            ));
        }

        return $this;
    }

    private function validateIfAlreadyComponent($id)
    {
        $product = $this->repository->find($id);

        if($product->type == ProductType::COMPONENT) {
            $this->message(array(
                'product.already.component' => Lang::get('core::validation.product.already.component')
            ));
        }

        return $this;
    }
}