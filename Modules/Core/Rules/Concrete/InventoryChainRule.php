<?php

namespace Modules\Core\Rules\Concrete;

use Litipk\BigNumbers\Decimal;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Lang;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Traits\HasMorphMap;

abstract class InventoryChainRule extends InventoryFlowRule
{
    use HasMorphMap;

    protected $reference;

    public function store($data)
    {
        $this->validateDateBasedFromPosting($data['info']['transaction_date']);
        $this->validateInvoiceTransaction($data['info']['transaction_type'] ?? TransactionType::FROM_INVOICE, $data['info']['reference_id']);
        $this->validateCreate($data);

        return $this;
    }

    public function update($data = [], $id = 0)
    {
        $this->checkTransaction($id);

        $this->validateDateBasedFromPosting($data['info']['transaction_date']);
        $this->validateInvoiceTransaction($data['info']['transaction_type'] ?? TransactionType::FROM_INVOICE, $data['info']['reference_id']);
        $this->validateUpdate($data, $id);

        return $this;
    }

    public function approval($transactions, $status)
    {
        parent::approval($transactions, $status);

        switch ($status) {
            case ApprovalStatus::FOR_APPROVAL:
            case ApprovalStatus::APPROVED:
                $serials = $this->getSerialsModifiedConstraint();

                $this->checkIfSerialsAreNotModified($transactions, $serials['status'], $serials['transaction']);
                $this->checkIfBatchesAreModified($transactions);
                break;

            case ApprovalStatus::DRAFT:
                $type = $this->getSerialsRevertConstraint();
                $this->checkIfSerialsAreRevertable($transactions, $type);
                $this->checkIfBatchesAreRevertable($transactions);
                break;
        }

        return $this;
    }

    /**
     * Checks if the given transaction's serial numbers doesn't matched the specified status and transaction
     * 
     * @param  array  $transactions
     * @param  integer $status
     * @param  integer $transaction
     * @return $this
     */
    protected function checkIfSerialsAreNotModified($transactions, $status = 1, $transaction = null)
    {
        $models = $this->repository
            ->with([
                'details.serials' => function($query) use ($status, $transaction) {
                    if (!is_null($status)) {
                        $query->where('status', '<>', $status);
                    }

                    if (!is_null($transaction)) {
                        $query->where('transaction', '<>', $transaction);
                    }
                }
            ])
            ->findMany($transactions);

        $serials = $models->pluck('details')
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        if ($serials->count() === 0) {
            return $this;
        }

        if ($serials->count() > 0) {
            foreach ($serials as $key => $serial) {
                $this->message(array(
                    "serial.$key.modified" => Lang::get('core::validation.serial.status.modified', ['serial' => $serial->serial_number])
                ));
            }
        }

        return $this;
    }

    /**
     * Checks if the given transaction's batches is modified and not sufficient anymore to deduct the indicated qty in
     * the transaction
     *
     * @param  array  $transactions
     * @return $this
     */
    protected function checkIfBatchesAreModified($transactions)
    {
        $models = $this->repository
            ->with(['details.batches.owners'])
            ->findMany($transactions);

        $zero = Decimal::create(0, setting('monetary.precision'));

        foreach ($models as $key => $transaction) {
            $batches = $transaction->details->pluck('batches')
                ->flatten()
                ->filter(function($value, $key){
                    return  $value != null ;
                });

            if ($batches->count() === 0) {
                continue;
            }

            $ownerType = $transaction->batch_base_owner_type_assignment;
            $ownerId = $transaction->batch_base_owner_id_assignment;
            $qtyColumn = $transaction->batch_base_owner_qty;

            if (is_null($ownerType)) {
                continue;
            }

            foreach ($batches as $k => $batch) {
                $owner = $batch->owners
                    ->where('owner_type', $ownerType)
                    ->where('owner_id', $ownerId)
                    ->first();

                if ($owner->{$qtyColumn}->sub($batch->pivot->qty)->isLessThan($zero)) {
                    $this->message(array(
                        "batch.$key.$k.modified" => Lang::get('core::validation.batch.qty.insufficient', [
                            'batch' => $batch->name
                        ])
                    ));
                }
            }
        }

        return $this;
    }

    protected function getSerialsModifiedConstraint()
    {
        return [
            'status' => SerialStatus::AVAILABLE,
            'transaction' => null
        ];
    }

    protected function validateInvoiceTransaction($type, $reference)
    {
        if ($type === TransactionType::DIRECT) {
            return $this;
        }

        $transaction = $this->reference->find($reference);

        if (!$transaction || $transaction->approval_status !== ApprovalStatus::APPROVED) {
            $this->message([
                'invoice.not.found' => Lang::get('core::validation.invoice.not.found')
            ]);
        }

        return $this;
    }

    protected function validateCreate($data)
    {
        return $this;
    }

    protected function checkIfBatchesAreRevertable($transactions)
    {
        $models = $this->repository
            ->with(['details.batches.transactionLogs' => function ($query) {
                $query->orderBy('approval_date', 'ASC');
            }])
            ->whereApproved()
            ->findMany($transactions);

        foreach ($models as $key => $transaction) {
            $batches = $transaction->details->pluck('batches')
                ->flatten()
                ->filter(function ($value, $key) {
                    return $value != null;
                });

            if ($batches->count() === 0) {
                continue;
            }

            foreach ($batches as $k => $batch) {
                $log = $batch->transactionLogs->last();

                if ($log->transaction_id !== $transaction->id
                    && $log->transaction_type !== $this->getMorphMapKey(get_class($transaction))
                ) {
                    $this->message(array(
                        "batch.$key.$k.modified" => Lang::get('core::validation.batch.modified', ['batch' => $batch->name])
                    ));
                }
            }
        }
    }

    protected function checkIfSerialsAreRevertable($transactions, $type)
    {
        $models = $this->repository
            ->with([
                'details.serials' => function($query) use ($type) {
                    $query->where('transaction', '<>', $type);
                }
            ])
            ->whereApproved()
            ->findMany($transactions);

        $serials = $models->pluck('details')
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        if ($serials->count() === 0) {
            return $this;
        }

        if ($serials->count() > 0) {
            foreach ($serials as $key => $serial) {
                $this->message(array(
                    "serial.$key.modified" => Lang::get('core::validation.serial.status.modified', ['serial' => $serial->serial_number])
                ));
            }
        }

        return $this;
    }

    protected abstract function getSerialsRevertConstraint();
}
