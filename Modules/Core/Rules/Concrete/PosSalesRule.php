<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\PosSalesRuleInterface;
use Modules\Core\Entities\BranchDetail;
use Lang;

class PosSalesRule extends Rule implements PosSalesRuleInterface
{
    public function sync($request = [])
    {
        $this->validateLocation($request);

        return $this;
    }

    private function validateLocation($request)
    {
        foreach ($request as $key => $value) {
            $info = $value['info'];

            $detail = BranchDetail::where('id', $info['for_location'])->where('branch_id', $info['created_for'])->get();

            if($detail->count() == 0) {
                $this->message([
                    'location.not.found' => Lang::get('core::error.location.not.found')
                ]);
            }
        }

        return $this;
    }
}