<?php

namespace Modules\Core\Rules\Concrete;

use Carbon\Carbon;
use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Rules\Contracts\TransactionSummaryPostingRuleInterface;
use Lang;

class TransactionSummaryPostingRule extends Rule implements TransactionSummaryPostingRuleInterface
{
    public function generateSummary($date)
    {
        $date = Carbon::parse($date);
        $lastPostDate = Carbon::parse(setting('transaction.summary.computation.last.post.date'));

        if (!is_null($lastPostDate) && $date->lessThanOrEqualTo($lastPostDate)) {
            $this->message([
                'date.invalid.range' => Lang::get('core::validation.invalid.date.range')
            ]);

            return $this;
        }

        $posting = TransactionSummaryPosting::orderBy('transaction_date', 'DESC')
            ->first();

        if (!$posting) {
            $this->message([
                'no.data.to.posts' => Lang::get('core::validation.no.data.to.posts')
            ]);

            return $this;
        }

        if ($date->greaterThan($posting->transaction_date)) {
            $this->message([
                'date.greater.than.last.transaction' => Lang::get('core::validation.date.greater.than.last.transaction')
            ]);

            return $this;
        }

        return $this;
    }
}