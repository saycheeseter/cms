<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\GiftCheckStatus;
use Modules\Core\Repositories\Contracts\GiftCheckRepository;
use Modules\Core\Rules\Contracts\GiftCheckRuleInterface;
use Lang;

class GiftCheckRule extends Rule implements GiftCheckRuleInterface
{
    private $repository;

    /**
     * GiftCheckRule constructor.
     */
    public function __construct(GiftCheckRepository $repository)
    {
        $this->repository = $repository;
    }

    public function update($request, $id)
    {
        $giftCheck = $this->repository->find($id);

        if ($giftCheck && $giftCheck->status !== GiftCheckStatus::AVAILABLE) {
            $this->message([
                'cannot.update.redeemed.gift.check' => Lang::get('core::validation.cannot.update.redeemed.gift.check')
            ]);
        }

        return $this;
    }

    public function bulkEntry($data)
    {
        // Need to subtract 1 so that we can include the initial start
        $qty = ((int) $data['qty']) - 1;

        $giftCheckStart = $data['check_number_series'];
        $giftCheckSeries = array_map("strval", range($giftCheckStart, bcadd($giftCheckStart, $qty)));

        $codeStart = $data['code_series'];
        $codeSeries = array_map("strval", range($codeStart, bcadd($codeStart, $qty)));

        $this->checkCheckNumberSeriesExists($giftCheckSeries);
        $this->checkCodeSeriesExists($codeSeries);

        return $this;
    }

    private function checkCheckNumberSeriesExists($series)
    {
        $collection = $this->repository->findWhereIn('check_number', $series);

        if ($collection->count() > 0) {
            $collection->each(function ($item, $key) {
                $this->message(array(
                    "$key.check.number" => Lang::get('core::validation.specific.check.number.exists', ['number' => $item->check_number])
                ));
            });
        }
    }

    private function checkCodeSeriesExists($series)
    {
        $collection = $this->repository->findWhereIn('code', $series);

        if ($collection->count() > 0) {
            $collection->each(function ($item, $key) {
                $this->message(array(
                    "$key.code" => Lang::get('core::validation.specific.code.exists', ['code' => $item->code])
                ));
            });
        }
    }
}