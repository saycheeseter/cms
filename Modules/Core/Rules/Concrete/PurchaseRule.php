<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\PurchaseRuleInterface;
use Modules\Core\Repositories\Contracts\PurchaseRepository;

class PurchaseRule extends InvoiceRule implements PurchaseRuleInterface
{
    public function __construct(PurchaseRepository $repository)
    {
        $this->repository = $repository;
    }
}