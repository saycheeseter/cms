<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ItemManagementOwner;
use Modules\Core\Repositories\Contracts\StockReturnOutboundRepository;
use Modules\Core\Rules\Contracts\StockReturnInboundRuleInterface;
use Modules\Core\Repositories\Contracts\StockReturnInboundRepository;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;

class StockReturnInboundRule extends InventoryChainRule implements StockReturnInboundRuleInterface
{
    public function __construct(StockReturnInboundRepository $repository, StockReturnOutboundRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function getSerialsModifiedConstraint()
    {
        return [
            'status' => SerialStatus::ON_TRANSIT,
            'transaction' => SerialTransaction::STOCK_RETURN_OUTBOUND
        ];
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::STOCK_RETURN_INBOUND;
    }
}
