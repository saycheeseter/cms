<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ItemManagementOwner;
use Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository;
use Modules\Core\Rules\Contracts\StockDeliveryInboundRuleInterface;
use Modules\Core\Repositories\Contracts\StockDeliveryInboundRepository;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;

class StockDeliveryInboundRule extends InventoryChainRule implements StockDeliveryInboundRuleInterface
{
    public function __construct(StockDeliveryInboundRepository $repository, StockDeliveryOutboundRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function getSerialsModifiedConstraint()
    {
        return [
            'status' => SerialStatus::ON_TRANSIT,
            'transaction' => SerialTransaction::STOCK_DELIVERY_OUTBOUND
        ];
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::STOCK_DELIVERY_INBOUND;
    }
}
