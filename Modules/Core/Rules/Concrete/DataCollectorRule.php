<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\DataCollectorRuleInterface;
use Modules\Core\Entities\DataCollectorTransaction;
use Lang;

class DataCollectorRule extends Rule implements DataCollectorRuleInterface
{
    public function destroyByReferenceNumber($referenceNumber = 0)
    {
        $result = DataCollectorTransaction::where('reference_no', $referenceNumber)->first();

        if(!$result) {
            $this->message([
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ]);
        }

        return $this;
    }

    public function destroy($id)
    {
        $result = DataCollectorTransaction::find($id);

        if(!$result) {
            $this->message([
                'transaction.not.found' => Lang::get('core::error.transaction.not.found')
            ]);
        }

        return $this;
    }
}