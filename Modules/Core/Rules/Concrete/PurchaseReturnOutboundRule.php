<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Repositories\Contracts\PurchaseReturnRepository;
use Modules\Core\Rules\Contracts\PurchaseReturnOutboundRuleInterface;
use Modules\Core\Repositories\Contracts\PurchaseReturnOutboundRepository;
use Lang;

class PurchaseReturnOutboundRule extends InventoryChainRule implements PurchaseReturnOutboundRuleInterface
{
    public function __construct(PurchaseReturnOutboundRepository $repository, PurchaseReturnRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && !$transaction->remaining_amount->equals($transaction->total_amount)
            )
            || $transaction->payments->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.paid.transaction" => Lang::get('core::validation.cannot.revert.paid.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::PURCHASE_RETURN_OUTBOUND;
    }
}
