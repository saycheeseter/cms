<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Repositories\Contracts\SalesRepository;
use Modules\Core\Rules\Contracts\SalesOutboundRuleInterface;
use Modules\Core\Repositories\Contracts\SalesOutboundRepository;
use Modules\Core\Enums\Customer;
use Validator;
use Lang;

class SalesOutboundRule extends InventoryChainRule implements SalesOutboundRuleInterface
{
    public function __construct(SalesOutboundRepository $repository, SalesRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    public function validateCreate($data)
    {
        $this->validateCustomerExists($data);

        return $this;
    }

    protected function validateUpdate($request, $id)
    {
        $this->validateCustomerExists($request);

        return $this;
    }

    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && !$transaction->remaining_amount->equals($transaction->total_amount)
            )
            || $transaction->collections->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.collected.transaction" => Lang::get('core::validation.cannot.revert.collected.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }

    private function validateCustomerExists($request)
    {
        $info = $request['info'];

        if ((int) $info['customer_type'] !== Customer::REGULAR) {
            return $this;
        }

        $validation = Validator::make($request, [
            'info.customer_id' => 'exists_in:customer,id,deleted_at',
            'info.customer_detail_id' => 'exists_in:customer_detail,id,deleted_at',
        ], [
            'info.customer_id.exists_in' => Lang::get('core::validation.customer.doesnt.exists'),
            'info.customer_detail_id.exists_in' => Lang::get('core::validation.customer.detail.doesnt.exists'),
        ]);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::SALES_OUTBOUND;
    }
}
