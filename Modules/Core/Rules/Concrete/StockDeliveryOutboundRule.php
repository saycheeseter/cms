<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Repositories\Contracts\StockDeliveryRepository;
use Modules\Core\Rules\Contracts\StockDeliveryOutboundRuleInterface;
use Modules\Core\Repositories\Contracts\StockDeliveryOutboundRepository;
use Modules\Core\Repositories\Contracts\ProductRepository;
use Litipk\BigNumbers\Decimal;
use App;
use Lang;

class StockDeliveryOutboundRule extends InventoryChainRule implements StockDeliveryOutboundRuleInterface
{
    public function __construct(StockDeliveryOutboundRepository $repository, StockDeliveryRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    /**
     * Check if each product encoded in the transaction reaches the minimum set in the product info
     * 
     * @param  array $data
     * @return $this
     */
    public function warning($data)
    {
        $branch = $data['info']['created_for'];
        $location = $data['info']['for_location'];
        $productIds = array_pluck($data['details'], 'product_id');
        $precision = setting('monetary.precision');

        $messages = [];
        
        $products = App::make(ProductRepository::class)->getSummaries($productIds, $branch, $location)->keyBy('id');

        foreach ($data['details'] as $detail) {
            $detail = (object) $detail;

            $totalQty = Decimal::create($detail->qty, $precision)->mul(
                Decimal::create($detail->unit_qty, $precision), 
                $precision
            );

            $inventory = $products->get($detail->product_id)->summaries->first()->qty;

            $min = $products->get($detail->product_id)->locations->first()->min;

            if($min->isGreaterOrEqualTo($inventory->sub($totalQty))) {
                $messages[] = sprintf('%s : %s (%s: %s)', 
                    $detail->name,   
                    Lang::get('core::validation.warning.product.already.reaches.minimum'),
                    Lang::get('core::label.min'),
                    $min
                );
            }
        }

        if(count($messages) >  0) {
            $this->message(array(
                'warning.message' => $messages
            ));
        }

        return $this;
    }

    protected function getSerialsRevertConstraint()
    {
        return [
            'status' => null,
            'transaction' => SerialTransaction::STOCK_DELIVERY_OUTBOUND
        ];
    }

    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && $transaction->transaction_status !== TransactionStatus::NO_DELIVERY
            )
            || $transaction->imports->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.imported.transaction" => Lang::get('core::validation.cannot.revert.imported.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }
}
