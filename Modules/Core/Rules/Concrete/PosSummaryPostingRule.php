<?php

namespace Modules\Core\Rules\Concrete;

use Carbon\Carbon;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\PosSummaryPosting;
use Modules\Core\Enums\Voided;
use Modules\Core\Rules\Contracts\PosSummaryPostingRuleInterface;
use Lang;

class PosSummaryPostingRule extends Rule implements PosSummaryPostingRuleInterface
{
    public function generateSummary($id, $date)
    {
        $posting = PosSummaryPosting::findOrFail($id);

        $date = Carbon::parse($date);

        if (!is_null($posting->last_post_date) && $date->lessThanOrEqualTo($posting->last_post_date)) {
            $this->message([
                'date.invalid.range' => Lang::get('core::validation.invalid.date.range')
            ]);

            return $this;
        }

        $count = PosSales::where('terminal_number', $posting->terminal_number)
            ->where('created_for', $posting->branch_id)
            ->where('voided', Voided::NO)
            ->where(function($query) use ($date, $posting) {
                if(!is_null($posting->last_post_date)) {
                    $query->where('transaction_date', '>=', $posting->last_post_date);
                }

                $query->where('transaction_date', '<=', $date);
            })
            ->count();

        if ($count === 0) {
            $this->message([
                'no.transaction.found' => Lang::get('core::error.no.transaction.found')
            ]);
        }

        return $this;
    }
}