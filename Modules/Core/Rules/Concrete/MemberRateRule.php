<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\MemberRateRuleInterface;
use Modules\Core\Repositories\Contracts\MemberRateRepository;
use Lang;
use Validator;
use Carbon\Carbon;

class MemberRateRule extends Rule implements MemberRateRuleInterface
{
    private $repository;

    public function __construct(MemberRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store($request = [])
    {
        $this->save($request);

        return $this;
    }

    public function update($request = [], $id = 0)
    {
        $this->save($request, $id);

        return $this;
    }

    public function destroy($id = 0)
    {
        if ($this->isRateAlreadyUsed($id)) {
            $this->message(array(
                'rate.in.used' => Lang::get('core::validation.delete.used.rate')
            ));
        }

        return $this;
    }

    private function save($request, $id = 0)
    {
        $details = $request['details'];

        foreach ($details as $index => $array) {
            $row = $index + 1;

            if(!isset($array['date_to']) || !isset($array['date_from'])) {
                continue;
            }

            if ($this->isDateOverlappingInSet($details, 'date_from', $array['date_from'], $index)) {
                $this->message(array(
                    'details.'.$index.'.date_from.overlap.state' => array(
                        sprintf('%s[%s]. %s',
                            Lang::get('core::error.row'),
                            $row,
                            Lang::get('core::validation.details.date.from.overlap.state')
                        )
                    )
                ));
            }

            if ($this->isDateOverlappingInSet($details, 'date_to', $array['date_to'], $index)) {
                $this->message(array(
                    'details.'.$index.'.date_to.overlap.state' => array(
                        sprintf('%s[%s]. %s',
                            Lang::get('core::error.row'),
                            $row,
                            Lang::get('core::validation.details.date.to.overlap.state')
                        )
                    )
                ));
            }

            if ($this->isValidDate($array['date_from'], $array['date_to'])) {
                $this->message(array(
                    'details.'.$index.'.invalid.date.range' => array(
                        sprintf('%s[%s]. %s',
                            Lang::get('core::error.row'),
                            $row,
                            Lang::get('core::validation.invalid.date.range')
                        )
                    )
                ));
            }
        }
    }

    private function isDateOverlappingInSet($set, $key, $value, $except)
    {
        $fails = false;
        $data = [$key => $value];

        foreach ($set as $index => $array) {
            if ($index === $except) {
                continue;
            }

            $validation = Validator::make($data, [
                $key => sprintf('date_between:%s,%s',
                    $array['date_from'],
                    $array['date_to']
                )
            ]);

            if ($validation->fails()) {
                $fails = true;
                break;
            }
        }

        return $fails;
    }

    private function isValidDate($dateFrom, $dateTo)
    {   
        return Carbon::parse($dateFrom)->greaterThan(Carbon::parse($dateTo));
    }

    private function isRateAlreadyUsed($id)
    {
        $rate = $this->repository
            ->with(['members'])
            ->find($id);

        return ($rate && $rate->members->count() > 0);
    }
}
