<?php

namespace Modules\Core\Rules\Concrete;

use Illuminate\Http\Request;

use Modules\Core\Rules\Contracts\ProductDiscountRuleInterface;

use Modules\Core\Repositories\Contracts\ProductDiscountRepository;

use Modules\Core\Enums\TargetType;
use Modules\Core\Enums\SelectType;

use Carbon\Carbon;
use Lang;
use App;

class ProductDiscountRule extends Rule implements ProductDiscountRuleInterface
{
    protected $repository;
    protected $code = '';
    protected $name = '';

    public function __construct(ProductDiscountRepository $repository) 
    {
        $this->repository = $repository;
    }

    public function store($request = [])
    {
        $this->validate($request);

        return $this;
    }

    public function update($request = [], $id = 0)
    {
        $this->validate($request, $id);

        return $this;
    }

    private function validate($request, $id = null) 
    {
        $info = $request['info'];

        if($this->isValidDate($info['valid_from'], $info['valid_to'])) {
            $this->message(array(
                'invalid.date.range' => array(Lang::get('core::validation.invalid.date.range'))
            ));
        } else if(!$this->isMinDate($info)) {
            $this->message(array(
                'min.day' => array(Lang::get('core::validation.min.day'))
            ));
        } else if($this->checkDate($info, $id) 
                && $this->checkSelectType($info, $id) 
                && $this->checkTargetType($info, $id) 
                && $this->checkDay($info, $id)) {

            if($info['select_type'] == SelectType::ALL_PRODUCTS && in_array($info['target_type'], TargetType::all())) {
                $this->setOverlapMessage($info, $id);
            } else {
                $details = array_pluck($request['details'], 'entity_id');
                $targets = array_pluck($request['targets'], 'target_id');

                if($info['select_type'] != SelectType::ALL_PRODUCTS && !in_array($info['target_type'], TargetType::all())) {
                    if($this->checkDetails($info, $details, $id) && $this->checkTargets($info, $targets, $id)) {
                        $this->setOverlapMessage($info, $id);
                    }
                } else if($info['select_type'] != SelectType::ALL_PRODUCTS) {
                    if($this->checkDetails($info, $details, $id)) {
                        $this->setOverlapMessage($info, $id);
                    }
                } else if(!in_array($info['target_type'], TargetType::all())) {
                    if($this->checkTargets($info, $targets, $id)) {
                        $this->setOverlapMessage($info, $id);
                    }
                }
            }
        }

        return $this;
    }

    private function checkDate($data, $id) 
    {
        $data = [
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to']
        ];

        if(count($this->repository->filter($data, $id)) > 0) {
            return true;
        }

        return false;
    }

    private function checkSelectType($data, $id) 
    {
        $data = [
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to'],
            'select_type' => $data['select_type']
        ];

        if(count($this->repository->filter($data, $id)) > 0) {
            return true;
        }

        return false;
    }

    private function checkTargetType($data, $id) 
    {
        $data = [
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to'],
            'select_type' => $data['select_type'],
            'target_type' => $data['target_type']
        ];

        if(count($this->repository->filter($data, $id)) > 0) {
            return true;
        }

        return false;
    }

    private function checkDay($data, $id) 
    {
        $data = $this->getFullFilters($data);

        $conflict = $this->repository->filter($data, $id);

        if(count($conflict) > 0) {
            return true;
        }

        return false;
    }

    private function checkDetails($data, $details, $id)
    {
        $data = $this->getFullFilters($data);

        $model = $this->repository->filter($data, $id);

        if(count($this->filterDetails($model, $details)) > 0) {
            return true;
        }

        return false;
    }

    private function checkTargets($data, $targets, $id)
    {
        $data = $this->getFullFilters($data);

        $model = $this->repository->filter($data, $id);

        if(count($this->filterTargets($model, $targets)) > 0) {
            return true;
        }

        return false;
    }

    private function isValidDate($dateFrom, $dateTo)
    {   
        return Carbon::parse($dateFrom)->greaterThan(Carbon::parse($dateTo));
    }

    private function isMinDate($data)
    {
        $days = [
            $data['mon'] ?? 0,
            $data['tue'] ?? 0,
            $data['wed'] ?? 0,
            $data['thur'] ?? 0,
            $data['fri'] ?? 0,
            $data['sat'] ?? 0,
            $data['sun'] ?? 0,
        ];

        return !empty(array_diff($days, [0, false]));
    }

    private function getFullFilters($data)
    {
        return [
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to'],
            'select_type' => $data['select_type'],
            'target_type' => $data['target_type'],
            'mon' => $data['mon'] ?? 0,
            'tue' => $data['tue'] ?? 0,
            'wed' => $data['wed'] ?? 0,
            'thur' => $data['thur'] ?? 0,
            'fri' => $data['fri'] ?? 0,
            'sat' => $data['sat'] ?? 0,
            'sun' => $data['sun'] ?? 0,
        ];
    }

    private function setOverlapMessage($data, $id)
    {
        $data = $this->getFullFilters($data);

        $conflict = $this->repository->filter($data, $id)->first();

        $this->message(array(
            'product.discount.overlap' => array(Lang::get('core::validation.product.discount.overlap').' '.$conflict->code.' '.$conflict->name)
        ));
    }

    public function filterDetails($model, $ids)
    {
        return $model->first()->details->whereIn('entity_id', $ids);
    }

    public function filterTargets($model, $ids)
    {
        return $model->first()->targets->whereIn('target_id', $ids);
    }
}