<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Repositories\Contracts\StockRequestRepository;
use Modules\Core\Rules\Contracts\StockDeliveryRuleInterface;
use Modules\Core\Repositories\Contracts\StockDeliveryRepository;
use Lang;

class StockDeliveryRule extends InvoiceRule implements StockDeliveryRuleInterface
{
    private $reference;

    public function __construct(StockDeliveryRepository $repository, StockRequestRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function validateCreate($data)
    {
        $this->validateStockRequestTransaction($data['info']['transaction_type'], $data['info']['reference_id']);
    }

    private function validateStockRequestTransaction($type, $reference)
    {
        if ($type === TransactionType::DIRECT) {
            return $this;
        }

        $transaction = $this->reference->find($reference);

        if (!$transaction || $transaction->approval_status !== ApprovalStatus::APPROVED) {
            $this->message([
                'stock.request.not.found' => Lang::get('core::validation.stock.request.not.found')
            ]);
        }

        return $this;
    }
}