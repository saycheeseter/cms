<?php

namespace Modules\Core\Rules\Concrete;

use Carbon\Carbon;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Rules\Contracts\ProductConversionRuleInterface;
use Modules\Core\Repositories\Contracts\ProductConversionRepository;
use App;
use Lang;

class ProductConversionRule extends Rule implements ProductConversionRuleInterface
{
    private $repository;

    public function __construct(ProductConversionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Validates update process.
     * Cannot update if:
     *     - Transaction is already deleted
     *     - Transaction's approval status is not Draft
     *
     * @param  array $args
     * @param  int $id
     * @return $this
     */
    public function update($args = [], $id = 0)
    {
        $this->checkTransaction($id);

        $this->validateDateBasedFromPosting($args['info']['transaction_date']);

        return $this;
    }

    public function updateApproved($data = [], $id = 0)
    {
        $transaction = $this->repository->find($id);

        if ($transaction->approval_status !== ApprovalStatus::APPROVED) {
            $this->message(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            ));
        }

        return $this;
    }

    /**
     * Validates destroy process.
     * Cannot destroy data if approval status is not Draft.
     *
     * @param  int $id
     * @return $this
     */
    public function destroy($id = 0)
    {
        $this->checkTransaction($id);

        return $this;
    }

    /**
     * Validates approval process
     * Checks current status of the transaction
     *
     * @param  array|int $transactions
     * @param  int $status
     * @return $this
     */
    public function approval($transactions, $status)
    {
        if (!is_array($transactions)) {
            $transactions = [$transactions];
        }

        $transactions = $this->repository
            ->withTrashed()
            ->findWhereIn('id', $transactions);

        foreach ($transactions as $key => $transaction) {
            if ($transaction->trashed()) {
                $this->message(array(
                    "transaction.$key.deleted" => Lang::get('core::validation.transaction.number.deleted', ['number' => $transaction->sheet_number])
                ));
            } else if ($status === ApprovalStatus::DRAFT && setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC){
                $this->message(array(
                    "cannot.revert.$key.automatic" => Lang::get('core::validation.cannot.revert.in.automatic.summary', ['number' => $transaction->sheet_number])
                ));
            } else if (!$this->isValidDateBasedFromPosting($transaction->transaction_date)) {
                $this->message(array(
                    "sheet.number.$key.past.post.date" => Lang::get('core::validation.sheet.number.date.past.post.date', ['number' => $transaction->sheet_number])
                ));
            } else {
                $this->checkApprovalStatus($transaction, $status, $key);
            }
        }

        return $this;
    }

    /**
     * Checks if approval status is draft
     *
     * @param  int $id
     * @return bool
     */
    protected function drafted($id)
    {
        $data = $this->repository->find($id);

        return $data->approval_status === ApprovalStatus::DRAFT;
    }

    /**
     * Checks if transaction is already deleted
     *
     * @param  int $id
     * @return $this
     */
    protected function deleted($id)
    {
        $data = $this->repository->find($id);

        return is_null($data);
    }

    /**
     * Check current transaction if deleted and not drafted
     *
     * @param  int $id
     * @return $this
     */
    protected function checkTransaction($id)
    {
        if ($this->deleted($id)) {
            $this->message(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            ));
        } else if (!$this->drafted($id)) {
            $this->message(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            ));
        }

        return $this;
    }

    /**
     * Check approval status to be set based on the transaction's current
     * approval status
     *
     * @param  Model $transaction
     * @param  int $status
     * @param  int $index
     * @return $this
     */
    protected function checkApprovalStatus($transaction, $status, $index)
    {
        switch ($status) {
            case ApprovalStatus::FOR_APPROVAL:
                if ($transaction->approval_status !== ApprovalStatus::DRAFT) {
                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get('core::validation.cannot.set.to.for.approval', ['number' => $transaction->sheet_number])
                    ));
                }
                break;

            case ApprovalStatus::APPROVED:
            case ApprovalStatus::DECLINED:
                if (!in_array($transaction->approval_status, [ApprovalStatus::FOR_APPROVAL, ApprovalStatus::DRAFT])) {
                    $message = $status == ApprovalStatus::APPROVED ? 'core::validation.cannot.set.to.approve' : 'core::validation.cannot.set.to.decline';

                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get($message, ['number' => $transaction->sheet_number])
                    ));
                }
                break;

            case ApprovalStatus::DRAFT:
                if (!in_array((int) $transaction->approval_status,
                    [ApprovalStatus::APPROVED, ApprovalStatus::DECLINED])
                ) {
                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get('core::validation.cannot.revert.transaction', ['number' => $transaction->sheet_number])
                    ));
                }
                break;
        }

        return $this;
    }

    private function validateDateBasedFromPosting($date)
    {
        if (!$this->isValidDateBasedFromPosting($date)) {
            $this->message([
                'transaction.date.past.post.date' => Lang::get('core::validation.transaction.date.past.post.date', ['number'])
            ]);
        }

        return $this;
    }

    private function isValidDateBasedFromPosting($date)
    {
        if (setting('transaction.summary.computation.mode') !== TransactionSummaryComputation::MANUAL) {
            return true;
        }

        $date = Carbon::parse($date)->startOfDay();

        $posting = Carbon::parse(setting('transaction.summary.computation.last.post.date'));

        return !$date->lessThanOrEqualTo($posting);
    }

    public function store($args)
    {
        $this->validateDateBasedFromPosting($args['info']['transaction_date']);

        return $this;
    }
}
