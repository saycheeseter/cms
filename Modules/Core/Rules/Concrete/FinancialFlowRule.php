<?php

namespace Modules\Core\Rules\Concrete;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\PaymentMethod;
use Validator;
use Lang;

abstract class FinancialFlowRule extends Rule
{
    protected $repository;
    protected $references;

    public function store($request = [])
    {
        $this->validatePaymentDetails($request['details']);
        $this->validateReferences($request['references']);

        return $this;
    }

    /**
     * Validates update process.
     * Cannot update if:
     *     - Transaction is already deleted
     *     - Transaction's approval status is not Draft
     *     
     * @param  array $request
     * @param  int $id     
     * @return $this         
     */
    public function update($request = [], $id = 0)
    {
        $this->checkTransaction($id);
        $this->validatePaymentDetails($request['details']);
        $this->validateReferences($request['references']);

        return $this;
    }

    /**
     * Validates destroy process.
     * Cannot destroy data if approval status is not Draft.
     * 
     * @param  int $id
     * @return $this    
     */
    public function destroy($id = 0)
    {
        $this->checkTransaction($id);

        return $this;
    }

    /**
     * Validates approval process
     * Checks current status of the transaction
     * 
     * @param  array|int $transactions
     * @param  int $status      
     * @return $this              
     */
    public function approval($transactions, $status)
    {
        if (!is_array($transactions)) {
            $transactions = [$transactions];
        }

        $transactions = $this->repository
            ->withTrashed()
            ->findWhereIn('id', $transactions);

        foreach ($transactions as $key => $transaction) {
            if ($transaction->trashed()) {
                $this->message(array(
                    "transaction.$key.deleted" => Lang::get('core::validation.transaction.number.deleted', ['number' => $transaction->sheet_number])
                ));
            } else {
                $this->checkApprovalStatus($transaction, $status, $key);
            }
        }

        return $this;
    }

    /**
     * Checks if approval status is draft
     * 
     * @param  int $id
     * @return bool
     */
    protected function drafted($id)
    {
        $data = $this->repository->find($id);

        return $data->approval_status === ApprovalStatus::DRAFT;
    }

    /**
     * Checks if transaction is already deleted
     * 
     * @param  int $id
     * @return $this    
     */
    protected function deleted($id)
    {
        $data = $this->repository->find($id);

        return is_null($data);
    }

    /**
     * Check current transaction if deleted and not drafted
     *
     * @param  int $id
     * @return $this
     */
    protected function checkTransaction($id)
    {
        if ($this->deleted($id)) {
            $this->message(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            ));
        } else if (!$this->drafted($id)) {
            $this->message(array(
                'transaction.approval.changed' => Lang::get('core::validation.cannot.update.transaction')
            ));
        }

        return $this;
    }

    /**
     * Check approval status to be set based on the transaction's current
     * approval status
     * 
     * @param  Model $transaction
     * @param  int $status     
     * @param  int $index      
     * @return $this             
     */
    protected function checkApprovalStatus($transaction, $status, $index)
    {
        switch ($status) {
            case ApprovalStatus::FOR_APPROVAL:
                if ((int)$transaction->approval_status !== ApprovalStatus::DRAFT) {
                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get('core::validation.cannot.set.to.for.approval', ['number' => $transaction->sheet_number])
                    ));
                }
                break;
            
            case ApprovalStatus::APPROVED:
            case ApprovalStatus::DECLINED:
                if (!in_array($transaction->approval_status, [ApprovalStatus::FOR_APPROVAL, ApprovalStatus::DRAFT])) {
                    $message = $status == ApprovalStatus::APPROVED ? 'core::validation.cannot.set.to.approve' : 'core::validation.cannot.set.to.decline';

                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get($message, ['number' => $transaction->sheet_number])
                    ));
                }
                break;

            case ApprovalStatus::DRAFT:
                if (!in_array((int) $transaction->approval_status, 
                    [ApprovalStatus::APPROVED, ApprovalStatus::DECLINED])
                ) {
                    $this->message(array(
                        "transaction.$index.approval.invalid" => Lang::get('core::validation.cannot.revert.transaction', ['number' => $transaction->sheet_number])
                    ));
                }
                break;
        }

        return $this;
    }

    protected function validatePaymentDetails($details)
    {
        foreach($details as $key => $detail) {
            $rules = [];
            $messages = [];
            $row = $key + 1;

            if(in_array((int) $detail['payment_method'], [PaymentMethod::CHECK, PaymentMethod::BANK_DEPOSIT])) {
                $rules['bank_account_id'] = [
                    'exists_in:bank_account,id,deleted_at'
                ];

                $messages['bank_account_id.exists_in'] = sprintf('%s[%u] %s',
                    Lang::get('core::error.row'),
                    $row,
                    Lang::get('core::validation.bank.account.doesnt.exists')
                );

                if ((int) $detail['payment_method'] === PaymentMethod::CHECK) {
                    $rules['check_date'] = ['date_format:Y-m-d'];

                    $messages['check_date.date_format'] = sprintf('%s[%u] %s',
                        Lang::get('core::error.row'),
                        $row,
                        Lang::get('core::validation.check.date.invalid')
                    );
                }
            }

            if (count($rules) > 0 && count($messages) > 0) {
                $validation = Validator::make($detail, $rules, $messages);

                if ($validation->fails()) {
                    $errors = [];

                    // Formats key so that error messages will not overlap with one another
                    foreach ($validation->errors()->get('*') as $column => $message) {
                        $errors[sprintf('details.%s.%s', $key, $column)] = $message;
                    }

                    $this->message($errors);
                }
            }
        }

        return $this;
    }

    protected function validateReferences($references)
    {
        foreach($references as $key => $reference) {
            $alias = $reference['reference_type'];
            $row = $key + 1;

            $model = Relation::getMorphedModel($alias);

            $transaction = App::make($model)
                ->withTrashed()
                ->find($reference['reference_id']);

            if ($transaction->trashed() || $transaction->approval_status !== ApprovalStatus::APPROVED) {
                $this->message([
                    "references.$key.reference_id" => sprintf('%s[%u][] %s',
                        Lang::get('core::error.row'),
                        $row,
                        Lang::get('core::validation.reference.transaction.doesnt.exists')
                    )
                ]);
            }
        }
    }
}