<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Rules\Contracts\RuleInterface;
use Illuminate\Http\Request;
 
abstract class Rule
{
    private $errors = [];

    public function passes()
    {
        return count($this->errors) === 0;
    }

    public function fails()
    {
        return count($this->errors) > 0;
    }

    public function errors()
    {
        return $this->errors;
    }

    protected function message($message)
    {
        $this->errors = array_merge($this->errors, $message);
    }
}