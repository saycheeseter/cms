<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\PaymentMethod;
use Validator;
use Lang;

abstract class OtherFinancialFlowRule extends Rule
{
    protected $repository;

    public function store($request = [])
    {
        $this->validatePayment($request);

        return $this;
    }

    /**
     * Validates update process.
     * Cannot update if:
     *     - Transaction is already deleted
     *     - Transaction's approval status is not Draft
     *
     * @param  array $request
     * @param  int $id
     * @return $this
     */
    public function update($request = [], $id = 0)
    {
        $this->checkTransaction($id);
        $this->validatePayment($request);

        return $this;
    }

    /**
     * Validates destroy process.
     * Cannot destroy data if approval status is not Draft.
     *
     * @param  int $id
     * @return $this
     */
    public function destroy($id = 0)
    {
        $this->checkTransaction($id);

        return $this;
    }

    /**
     * Checks if transaction is already deleted
     *
     * @param  int $id
     * @return $this
     */
    protected function deleted($id)
    {
        $data = $this->repository->find($id);

        return is_null($data);
    }

    /**
     * Check current transaction if deleted and not drafted
     *
     * @param  int $id
     * @return $this
     */
    protected function checkTransaction($id)
    {
        if ($this->deleted($id)) {
            $this->message(array(
                'transaction.deleted' => Lang::get('core::validation.transaction.deleted')
            ));
        }

        return $this;
    }

    protected function validatePayment($request)
    {
        $info = $request['info'];

        if(!in_array((int) $info['payment_method'], [PaymentMethod::CHECK, PaymentMethod::BANK_DEPOSIT])) {
            return $this;
        }

        $rules['bank_account_id'] = ['exists_in:bank_account,id,deleted_at'];
        $messages['bank_account_id.exists_in'] = Lang::get('core::validation.bank.account.doesnt.exists');

        if ((int) $info['payment_method'] === PaymentMethod::CHECK) {
            $rules['check_date'] = ['date_format:Y-m-d'];
            $messages['check_date.date_format'] = Lang::get('core::validation.check.date.invalid');
        }

        $validation = Validator::make($info, $rules, $messages);

        if ($validation->fails()) {
            $this->message($validation->errors()->get('*'));
        }

        return $this;
    }
}