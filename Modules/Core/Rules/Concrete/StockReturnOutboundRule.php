<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Repositories\Contracts\StockReturnRepository;
use Modules\Core\Rules\Contracts\StockReturnOutboundRuleInterface;
use Modules\Core\Repositories\Contracts\StockReturnOutboundRepository;
use Lang;

class StockReturnOutboundRule extends InventoryChainRule implements StockReturnOutboundRuleInterface
{
    public function __construct(StockReturnOutboundRepository $repository, StockReturnRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::STOCK_RETURN_OUTBOUND;
    }

    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && $transaction->transaction_status !== TransactionStatus::NO_DELIVERY
            )
            || $transaction->imports->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.imported.transaction" => Lang::get('core::validation.cannot.revert.imported.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }
}
