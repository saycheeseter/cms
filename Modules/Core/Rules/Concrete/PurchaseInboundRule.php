<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Repositories\Contracts\PurchaseRepository;
use Modules\Core\Rules\Contracts\PurchaseInboundRuleInterface;
use Modules\Core\Repositories\Contracts\PurchaseInboundRepository;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Lang;

class PurchaseInboundRule extends InventoryChainRule implements PurchaseInboundRuleInterface
{
    public function __construct(PurchaseInboundRepository $repository, PurchaseRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && !$transaction->remaining_amount->equals($transaction->total_amount)
            )
            || $transaction->payments->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.paid.transaction" => Lang::get('core::validation.cannot.revert.paid.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }

    protected function getSerialsModifiedConstraint()
    {
        return [
            'status' => SerialStatus::NOT_AVAILABLE,
            'transaction' => 0
        ];
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::PURCHASE_INBOUND;
    }
}
