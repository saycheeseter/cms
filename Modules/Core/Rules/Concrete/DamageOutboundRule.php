<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Repositories\Contracts\DamageRepository;
use Modules\Core\Rules\Contracts\DamageOutboundRuleInterface;
use Modules\Core\Repositories\Contracts\DamageOutboundRepository;

class DamageOutboundRule extends InventoryChainRule implements DamageOutboundRuleInterface
{
    public function __construct(DamageOutboundRepository $repository, DamageRepository $reference)
    {
        $this->repository = $repository;
        $this->reference = $reference;
    }

    protected function getSerialsRevertConstraint()
    {
        return SerialTransaction::DAMAGE_OUTBOUND;
    }
}
