<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\SerialStatus;
use Modules\Core\Enums\SerialTransaction;
use Modules\Core\Rules\Contracts\InventoryAdjustRuleInterface;
use Modules\Core\Repositories\Contracts\InventoryAdjustRepository;
use Modules\Core\Repositories\Contracts\ProductBranchSummaryRepository;
use Litipk\BigNumbers\Decimal;
use App;
use Lang;

class InventoryAdjustRule extends InventoryFlowRule implements InventoryAdjustRuleInterface
{
    public function __construct(InventoryAdjustRepository $repository)
    {
        $this->repository = $repository;
    }

    public function approval($transactions, $status)
    {
        parent::approval($transactions, $status);

        switch ($status) {
            case ApprovalStatus::FOR_APPROVAL:
            case ApprovalStatus::APPROVED:
                $this->checkIfSerialsForApproveAreNotModified($transactions);
                break;

            case ApprovalStatus::DRAFT:
                $this->checkIfSerialsForRevertAreNotModified($transactions);
                break;
        }

        return $this;
    }

    public function checkInventoryDiscrepancy($request)
    {
        $warnings = [];

        $products = App::make(ProductBranchSummaryRepository::class)
            ->with(['product'])
            ->findByFilters(
                array_pluck($request['details'], 'product_id'),
                $request['info']['created_for'], 
                $request['info']['for_location']
            );

        foreach ($request['details'] as $key => $product) {
            $current = Decimal::create(
                $product['inventory'], 
                setting('monetary.precision')
            );

            $actual = $products
                ->where('product_id', $product['product_id'])
                ->first();

            if ($actual->qty->equals($current)) {
                continue;
            }

            $warnings[] = sprintf('%s: %s [%s], %s [%s]',
                $actual->product->name,
                Lang::get('core::label.current'),
                $product['inventory'],
                Lang::get('core::label.actual'),
                $actual->qty
            );
        }

        if (count($warnings) > 0) {
            $this->message(array(
                'inventory.warning' => $warnings
            ));
        }

        return $this;
    }

    /**
     * Checks if the given transaction's serial numbers status and transaction is not modified
     *
     * @param  array  $transactions
     * @return $this
     */
    private function checkIfSerialsForApproveAreNotModified($transactions)
    {
        $models = $this->repository
            ->with(['details.serials'])
            ->findMany($transactions);

        $details = $models->pluck('details')->flatten();

        $increase = $details->filter(
            function($value, $key){
                return  $value->process_flow === 'adjustment_increase';
            })
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        $decrease = $details->filter(
            function($value, $key){
                return  $value->process_flow === 'adjustment_decrease';
            })
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        if ($increase->count() === 0 && $decrease->count() === 0) {
            return $this;
        }

        $increase = $increase->where('status', SerialStatus::AVAILABLE);

        $decrease = $decrease->where('status', '<>', SerialStatus::AVAILABLE);

        $modified = $increase->merge($decrease);

        if ($modified->count() > 0) {
            foreach ($modified as $key => $serial) {
                $this->message(array(
                    "serial.$key.modified" => Lang::get('core::validation.serial.status.modified', ['serial' => $serial->serial_number])
                ));
            }
        }

        return $this;
    }

    private function checkIfSerialsForRevertAreNotModified($transactions)
    {
        $models = $this->repository
            ->with(['details.serials'])
            ->whereApproved()
            ->findMany($transactions);

        $details = $models->pluck('details')->flatten();

        $increase = $details->filter(
            function($value, $key) {
                return $value->process_flow === 'adjustment_increase';
            })
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        $decrease = $details->filter(
            function($value, $key) {
                return  $value->process_flow === 'adjustment_decrease';
            })
            ->flatten()
            ->pluck('serials')
            ->flatten()
            ->filter(function($value, $key){
                return  $value != null ;
            });

        if ($increase->count() === 0 && $decrease->count() === 0) {
            return $this;
        }

        $increase = $increase->where('transaction', '<>', SerialTransaction::INVENTORY_ADJUST_INCREASE);

        $decrease = $decrease->where('transaction', '<>', SerialTransaction::INVENTORY_ADJUST_DECREASE);

        $modified = $increase->merge($decrease);

        if ($modified->count() > 0) {
            foreach ($modified as $key => $serial) {
                $this->message(array(
                    "serial.$key.modified" => Lang::get('core::validation.serial.status.modified', ['serial' => $serial->serial_number])
                ));
            }
        }

        return $this;
    }
}
