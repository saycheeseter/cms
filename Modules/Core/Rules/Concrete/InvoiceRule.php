<?php

namespace Modules\Core\Rules\Concrete;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Enums\TransactionSummaryComputation;
use Lang;

abstract class InvoiceRule extends InventoryFlowRule
{
    protected function isValidForApproval($transaction, $status, $key)
    {
        if (($status === ApprovalStatus::DRAFT
                && $transaction->approval_status === ApprovalStatus::APPROVED
                && $transaction->transaction_status !== TransactionStatus::NO_DELIVERY
            )
            || $transaction->imports->count() > 0
        ) {
            $this->message(array(
                "cannot.revert.$key.imported.transaction" => Lang::get('core::validation.cannot.revert.imported.transaction', ['number' => $transaction->sheet_number])
            ));

            return false;
        }

        return true;
    }
}