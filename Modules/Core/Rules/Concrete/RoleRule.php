<?php

namespace Modules\Core\Rules\Concrete;

use Exception;
use Modules\Core\Rules\Contracts\RoleRuleInterface;
use Modules\Core\Repositories\Contracts\RoleRepository;
use Lang;
use App;

class RoleRule extends Rule implements RoleRuleInterface
{
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function destroy($id)
    {
        try {
            $role = $this->repository->with(['users'])->findOrFail($id);

            if($role->users->count() > 0) {
                $this->message([
                    'role.already.used' => Lang::get('core::validation.role.already.used')
                ]);
            }
        } catch (Exception $e) {
            $this->message([
                'delete.failed' => Lang::get('core::error.delete.failed')
            ]);
        }

        return $this;
    }
}