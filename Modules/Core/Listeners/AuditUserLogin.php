<?php

namespace Modules\Core\Listeners;

use Illuminate\Support\Facades\Request;
use Modules\Core\Events\UserLoggedIn;

class AuditUserLogin
{
    public function handle(UserLoggedIn $event)
    {
        $model = $event->model;

        $model->loginAudits()->create([
            'ip_address' => Request::ip(),
            'user_agent' => Request::userAgent()
        ]);
    }
}