<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Voided;
use Modules\Core\Events\PosSalesEvent;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface as ProductStockCardService;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use App;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;

class PosSalesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PosSalesEvent  $event
     * @return void
     */
    public function handle(PosSalesEvent $event)
    {
        $model = $event->model;

        if ($model->voided === Voided::YES) {
            return;
        }

        App::make(TransactionMovingAverageCostServiceInterface::class)->updateTransactionDetails($model);
        App::make(ProductBranchSummaryService::class)->computeInventory($model);
        App::make(ProductTransactionSummaryService::class)->summarizeTransaction($model);
        App::make(ProductStockCardService::class)->create($model);
        // App::make(ProductSerialNumberService::class)->updateStatusFromTransaction($model);
    }
}
