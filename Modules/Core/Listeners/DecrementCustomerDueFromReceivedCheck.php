<?php

namespace Modules\Core\Listeners;

use Modules\Core\Entities\OtherIncome;
use Modules\Core\Enums\Customer;
use Modules\Core\Events\ReceivedCheckTransferred;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;
use App;

class DecrementCustomerDueFromReceivedCheck
{
    public function handle(ReceivedCheckTransferred $event)
    {
        $model = $event->model;

        if ($model->transactionable instanceof OtherIncome
            || $model->transactionable->transaction === Customer::WALK_IN
        ) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->deductDueFromReceivedCheck($model);
    }
}