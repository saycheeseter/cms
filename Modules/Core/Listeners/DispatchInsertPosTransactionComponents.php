<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Voided;
use Modules\Core\Events\PosTransactionSynced;
use Modules\Core\Jobs\InsertPosTransactionComponents;

class DispatchInsertPosTransactionComponents
{
    /**
     * Handle the event.
     *
     * @param  PosTransactionSynced  $event
     * @return void
     */
    public function handle(PosTransactionSynced $event)
    {
        $model = $event->model;

        if ($model->voided === Voided::YES) {
            return;
        }

        dispatch((new InsertPosTransactionComponents($model))->onQueue('normal'));
    }
}