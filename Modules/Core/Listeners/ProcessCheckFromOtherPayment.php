<?php

namespace Modules\Core\Listeners;

use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Events\OtherPaymentUpdated;
use App;
use Modules\Core\Services\Concrete\IssuedCheckService;
use Modules\Core\Services\Concrete\ReceivedCheckService;

class ProcessCheckFromOtherPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtherPaymentUpdated $event
     * @return void
     */
    public function handle(OtherPaymentUpdated $event)
    {
        $model = $event->model;

        $service = App::make($this->getServiceByModel($model));

        // Delete the check if the payment method is changed from check to other type
        if ($model->isDirty('payment_method')
            && $model->getOriginal()['payment_method'] == PaymentMethod::CHECK
        ) {
            $service->deleteFromOtherPayment($model);
            return;
        }

        if ($model->payment_method !== PaymentMethod::CHECK) {
            return;
        }

        // If there's still an existing instance of the check, update the amount, else create a new check
        if(!is_null($model->check) && !$model->check->trashed()) {
            $service->updateFromOtherPayment($model);
        } else {
            $service->createFromOtherPayment($model);
        }
    }

    private function getServiceByModel($model)
    {
        if($model instanceof OtherIncome) {
            return ReceivedCheckService::class;
        } else if($model instanceof OtherPayment) {
            return IssuedCheckService::class;
        }
    }
}
