<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use App;
use Modules\Core\Events\CollectionReverted;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;

class IncrementCustomerDueFromCollection
{
    public function handle(CollectionReverted $event)
    {
        if ($event->model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->addDueFromCollection($event->model);
    }
}