<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use Modules\Core\Events\SalesOutboundApproved;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;
use App;

class IncrementCustomerDueFromSalesOutbound
{
    public function handle(SalesOutboundApproved $event)
    {
        $model = $event->model;

        if ($model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->addDueFromSalesOutbound($model);
    }
}