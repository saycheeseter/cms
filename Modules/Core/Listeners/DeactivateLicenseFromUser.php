<?php

namespace Modules\Core\Listeners;

use Modules\Core\Events\UserDeleted;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface as LicenseApiService;
use App;

class DeactivateLicenseFromUser
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserDeleted $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $model = $event->model;

        if ((config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false)
            || $model->license_key === ''
        ) {
            return;
        }

        App::make(LicenseApiService::class)->deactivate($model->license_key);

        $model->license_key = null;
        $model->save();
    }
}