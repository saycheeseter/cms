<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Events\AutoProductConversionDeleted;
use App;
use Modules\Core\Jobs\RevertProductMonthlySummaryFromAutoConversion;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;

class RevertAutoConversionSummaryData
{
    public function handle(AutoProductConversionDeleted $event)
    {
        $model = $event->model;

        App::make(ProductBranchSummaryServiceInterface::class)->revertAutoConversion($model);
        App::make(ProductTransactionSummaryService::class)->revertAutoProductConversion($model);

        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
            App::make(TransactionSummaryPostingServiceInterface::class)->delete($model);
        }
    }
}