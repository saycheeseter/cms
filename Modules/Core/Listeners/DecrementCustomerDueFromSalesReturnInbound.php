<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use Modules\Core\Events\SalesReturnInboundApproved;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;
use App;

class DecrementCustomerDueFromSalesReturnInbound
{
    public function handle(SalesReturnInboundApproved $event)
    {
        $model = $event->model;

        if ($model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->deductDueFromSalesReturnInbound($model);
    }
}