<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use Modules\Core\Events\SalesReturnInboundReverted;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;
use App;

class IncrementCustomerDueFromSalesReturnInbound
{
    public function handle(SalesReturnInboundReverted $event)
    {
        $model = $event->model;

        if ($model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->addDueFromSalesReturnInbound($model);
    }
}