<?php

namespace Modules\Core\Listeners;

use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Events\OtherPaymentDeleted;
use App;
use Modules\Core\Services\Concrete\IssuedCheckService;
use Modules\Core\Services\Concrete\ReceivedCheckService;

class DeleteCheckFromOtherPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtherPaymentDeleted $event
     * @return void
     */
    public function handle(OtherPaymentDeleted $event)
    {
        $model = $event->model;

        if ($model->payment_method !== PaymentMethod::CHECK) {
            return;
        }

        App::make($this->getServiceByModel($model))->deleteFromOtherPayment($model);
    }

    private function getServiceByModel($model)
    {
        if($model instanceof OtherIncome) {
            return ReceivedCheckService::class;
        } else if($model instanceof OtherPayment) {
            return IssuedCheckService::class;
        }
    }
}
