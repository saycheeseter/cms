<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Events\AutoProductConversionCreated;
use Modules\Core\Jobs\GenerateProductMonthlySummaryFromAutoConversion;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface as ProductStockCardService;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use App;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;

class AutoProductConversionListener
{
    /**
     * Handle the event.
     *
     * @param  AutoProductConversionCreated $event
     * @return void
     */
    public function handle(AutoProductConversionCreated $event)
    {   
        $model = $event->model;

        App::make(ProductBranchSummaryService::class)->autoConversion($model);
        App::make(ProductTransactionSummaryService::class)->summarizeAutoProductConversion($model);

        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
            App::make(ProductStockCardService::class)->createFromProductConversion($model);
            dispatch((new GenerateProductMonthlySummaryFromAutoConversion($model))->onQueue('high'));
        } else {
            App::make(TransactionSummaryPostingServiceInterface::class)->insert($model);
        }
    }
}