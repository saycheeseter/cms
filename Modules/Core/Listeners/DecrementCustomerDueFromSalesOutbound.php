<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use Modules\Core\Events\SalesOutboundApproved;
use Modules\Core\Events\SalesOutboundReverted;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;
use App;

class DecrementCustomerDueFromSalesOutbound
{
    public function handle(SalesOutboundReverted $event)
    {
        $model = $event->model;

        if ($model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->deductDueFromSalesOutbound($model);
    }
}