<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\Customer;
use Modules\Core\Events\CollectionApproved;
use App;
use Modules\Core\Services\Contracts\CustomerCreditServiceInterface;

class DecrementCustomerDueFromCollection
{
    public function handle(CollectionApproved $event)
    {
        if ($event->model->customer_type === Customer::WALK_IN) {
            return;
        }

        App::make(CustomerCreditServiceInterface::class)->deductDueFromCollection($event->model);
    }
}