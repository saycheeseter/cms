<?php

namespace Modules\Core\Listeners;

use Modules\Core\Events\UserCreated;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface as LicenseApiService;
use App;

class ActivateLicenseFromCreateUser
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        if (config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false) {
            return;
        }

        $model = $event->model;

        if (!empty($model->license_key)) {
            App::make(LicenseApiService::class)->activate($model);
        }
    }
}