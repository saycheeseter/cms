<?php

namespace Modules\Core\Listeners;

use Illuminate\Support\Facades\Session;
use Modules\Core\Events\UserLoggedIn;

class SetAuthUserSessionId
{
    public function handle(UserLoggedIn $event)
    {
        $model = $event->model;

        $model->session_id = Session::getId();
        $model->save();
    }
}