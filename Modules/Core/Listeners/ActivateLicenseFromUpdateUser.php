<?php

namespace Modules\Core\Listeners;

use Modules\Core\Events\UserUpdating;
use Modules\Core\Services\Contracts\LicenseApiServiceInterface as LicenseApiService;
use App;

class ActivateLicenseFromUpdateUser
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdating $event
     * @return void
     */
    public function handle(UserUpdating $event)
    {
        if (config('app.env') === 'local' && env('LICENSE_VERIFICATION') === false) {
            return;
        }

        $model = $event->model;

        $service = App::make(LicenseApiService::class);

        if(!empty($model->getOriginal('license_key')) && empty($model->license_key)) {
            $service->deactivate($model->getOriginal('license_key'));
        } else if(empty($model->getOriginal('license_key')) && !empty($model->license_key)) {
            $service->activate($model);
        } else if($model->getOriginal('license_key') != $model->license_key) {
            $service->deactivate($model->getOriginal('license_key'));
            $service->activate($model);
        }
    }
}