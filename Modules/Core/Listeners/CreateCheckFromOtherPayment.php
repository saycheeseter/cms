<?php

namespace Modules\Core\Listeners;

use Modules\Core\Enums\PaymentMethod;
use Modules\Core\Events\OtherPaymentCreated;
use Modules\Core\Entities\OtherIncome;
use Modules\Core\Entities\OtherPayment;
use Modules\Core\Services\Contracts\IssuedCheckServiceInterface as IssuedCheckService;
use Modules\Core\Services\Contracts\ReceivedCheckServiceInterface as ReceivedCheckService;
use App;

class CreateCheckFromOtherPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtherPaymentCreated $event
     * @return void
     */
    public function handle(OtherPaymentCreated $event)
    {
        $model = $event->model;

        if ($model->payment_method !== PaymentMethod::CHECK) {
            return;
        }

        App::make($this->getServiceByModel($model))->createFromOtherPayment($model);
    }

    private function getServiceByModel($model)
    {
        if($model instanceof OtherIncome) {
            return ReceivedCheckService::class;
        } else if($model instanceof OtherPayment) {
            return IssuedCheckService::class;
        }
    }
}
