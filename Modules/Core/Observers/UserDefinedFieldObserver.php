<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\UserDefinedFieldMigrationServiceInterface;
use App;

class UserDefinedFieldObserver
{
    public function created($model)
    {
        App::make(UserDefinedFieldMigrationServiceInterface::class)->migrate($model);
    }

    public function updated($model)
    {
        if ($model->isDirty('type')) {
            $service = App::make(UserDefinedFieldMigrationServiceInterface::class);

            $service->dropColumn($model);
            $service->migrate($model);
        }
    }

    public function deleted($model)
    {
        App::make(UserDefinedFieldMigrationServiceInterface::class)->dropColumn($model);
    }
}