<?php

namespace Modules\Core\Observers;

class HasPayableRemainingAmountObserver
{
    public function updating($model)
    {
        if ($model->isDirty(['total_amount'])) {
            $model->remaining_amount = $model->total_amount;
        }
    }
}