<?php

namespace Modules\Core\Observers;

class InventoryChainDetailItemManagementObserver
{
    public function deleted($model)
    {
        $model->serials()->detach();
        $model->batches()->detach();
    }
}