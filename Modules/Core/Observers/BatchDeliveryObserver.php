<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\BatchDeliveryServiceInterface;
use App;

class BatchDeliveryObserver
{
    /**
     * Listen to the Model updated event.
     *
     * @param  Model  $model
     * @return void
     */
    public function updated($model)
    {
        if ($model->isDirty(['approval_status']) && $model->approval_status === ApprovalStatus::APPROVED) {
            App::make(BatchDeliveryServiceInterface::class)->updateReferenceBatchRemainingQty($model);
        }
    }
}