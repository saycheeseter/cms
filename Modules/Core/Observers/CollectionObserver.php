<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\CollectionServiceInterface;
use Modules\Core\Services\Contracts\ReceivedCheckServiceInterface as ReceivedCheckService;
use Modules\Core\Services\Contracts\CustomerBalanceFlowServiceInterface as CustomerBalanceFlowService;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\Customer;
use Litipk\BigNumbers\Decimal;
use App;

class CollectionObserver extends FinancialFlowObserver
{
    /**
     * Listen to the Model updated event.
     *
     * Set model detail reference table remaining quantity
     *
     * @param  Model  $model
     * @return void
     */
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            $zero = Decimal::create(0, setting('monetary.precision'));

            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(CollectionServiceInterface::class)->updateTransactionsRemainingAmount($model);
                    App::make(ReceivedCheckService::class)->createFromPayment($model);

                    if($model->customer_type == Customer::REGULAR) {
                        if($model->total_excess_amount->isGreaterThan($zero)) {
                            App::make(CustomerBalanceFlowService::class)->fromExcess($model);
                        }

                        if ($model->total_from_balance->isGreaterThan($zero)) {
                            App::make(CustomerBalanceFlowService::class)->fromBalance($model);
                        }
                    }
                    break;
                
                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal()['approval_status'] == ApprovalStatus::APPROVED) {
                        App::make(CollectionServiceInterface::class)->revertTransactionsRemainingAmount($model);
                        App::make(ReceivedCheckService::class)->deleteFromPayment($model);
                    
                        if($model->customer_type == Customer::REGULAR
                            && ($model->total_excess_amount->isGreaterThan($zero)
                                || $model->total_from_balance->isGreaterThan($zero))
                        ) {
                            App::make(CustomerBalanceFlowService::class)->deleteByCollection($model->id);
                        }
                    }
                    break;
            }
            
        }
    }
}