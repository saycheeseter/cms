<?php

namespace Modules\Core\Observers;

class AutoProductConversionDetailObserver
{
    public function created($model)
    {
        $model->product->is_used = 1;
        $model->product->save();

        $model->transaction->ma_cost = $model->transaction->ma_cost->add($model->amount);
        $model->transaction->save();
    }
}