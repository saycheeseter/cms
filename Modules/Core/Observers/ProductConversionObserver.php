<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface as ProductStockCardService;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;
use Modules\Core\Enums\ApprovalStatus;
use App;
use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;

class ProductConversionObserver
{
    public function created($model) 
    {
        $model->product->is_used = 1;
        $model->product->save();
    }

    public function updating($model)
    {
        if ($model->isDirty(['approval_status'])
            && $model->approval_status == ApprovalStatus::APPROVED
            && setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC
        ) {
            App::make(TransactionMovingAverageCostServiceInterface::class)->updateManualConversion($model);
        }
    }

    /**
     * Listen to the Model updated event.
     *
     * Set model detail reference table remaining quantity
     *
     * @param  Model  $model
     * @return void
     */
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(ProductBranchSummaryService::class)->manualConversion($model);
                    App::make(ProductTransactionSummaryService::class)->summarizeProductConversion($model);

                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                        App::make(ProductStockCardService::class)->createFromProductConversion($model);
                    } else {
                        App::make(TransactionSummaryPostingServiceInterface::class)->insert($model);
                    }
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                        App::make(ProductBranchSummaryService::class)->revertManualConversion($model);
                        App::make(ProductTransactionSummaryService::class)->revertProductConversion($model);

                        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
                            App::make(TransactionSummaryPostingServiceInterface::class)->delete($model);
                        }
                    }
                    break;
            }
        }
    }
}