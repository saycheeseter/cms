<?php

namespace Modules\Core\Observers;

use Carbon\Carbon;
use Modules\Core\Enums\ApprovalStatus;
use Auth;

class ApprovalAuditObserver
{
    public function updating($model)
    {
        if ($model->isDirty('approval_status')) {
            if (in_array((int) $model->approval_status, [
                ApprovalStatus::APPROVED,
                ApprovalStatus::DECLINED,
            ])) {
                $model->audited_date = new Carbon();
                $model->audited_by = Auth::user()->id;
            }  else {
                $model->audited_date = null;
                $model->audited_by = null;
            }
        }
    }
}