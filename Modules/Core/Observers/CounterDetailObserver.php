<?php

namespace Modules\Core\Observers;

use Litipk\BigNumbers\Decimal;

class CounterDetailObserver
{
    public function created($model)
    {
        if (! is_null($model->transaction)) {
            $model->transaction->total_amount = $model->transaction->total_amount->add($model->remaining_amount, setting('monetary.precision'));

            $model->transaction->save();
        }
    }

    public function updated($model)
    {
        if ($model->isDirty(['remaining_amount'])
            && ! is_null($model->transaction)
        ) {
            $precision = setting('monetary.precision');

            $old = $model->getOriginal();

            $oldAmount = Decimal::create((string)$old['remaining_amount'], $precision);

            $model->transaction->total_amount = $model->transaction->total_amount
                ->add($model->remaining_amount->sub($oldAmount, $precision), $precision);

            $model->transaction->save();
        }
    }

    public function deleted($model)
    {
        if (! $model->transaction->trashed()) {
            $model->transaction->total_amount = $model->transaction->total_amount->sub($model->remaining_amount, setting('monetary.precision'));

            $model->transaction->save();
        }
    }
}