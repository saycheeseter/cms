<?php

namespace Modules\Core\Observers;

class InventoryChainDetailReferenceObserver
{
    /**
     * Listen to the Model creating event.
     * Updates remaining_qty field
     *
     * @param  Model  $model
     * @return void
     */
    public function creating($model)
    {
        $model->remaining_qty = $model->total_qty;
    }

    /**
     * Listen to the Model updating event.
     * Updates remaining_qty field
     *
     * @param  Model  $model
     * @return void
     */
    public function updating($model)
    {
        if ($model->isDirty(['unit_qty', 'qty'])) {
            $model->remaining_qty = $model->total_qty;
        }
    }
}
