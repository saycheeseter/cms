<?php

namespace Modules\Core\Observers;

class TransactionDetailRemainingQtyObserver
{
    public function creating($model)
    {
        $model->remaining_qty = $model->total_qty;
    }

    public function updating($model)
    {
        if ($model->isDirty(['unit_qty', 'qty'])) {
            $model->remaining_qty = $model->total_qty;
        }
    }
}