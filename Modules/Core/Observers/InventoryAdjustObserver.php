<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface as ProductStockCardService;
use Modules\Core\Enums\ApprovalStatus;
use App;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;

class InventoryAdjustObserver
{
    public function updated($model)
    {

        switch ($model->approval_status) {
            case ApprovalStatus::APPROVED:
                App::make(ProductBranchSummaryService::class)->computeInventory($model);
                App::make(ProductSerialNumberServiceInterface::class)->updateStatusFromInventoryAdjust($model);
                App::make(ProductTransactionSummaryService::class)->summarizeInventoryAdjust($model);

                if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                    App::make(ProductStockCardService::class)->create($model);
                } else {
                    App::make(TransactionSummaryPostingServiceInterface::class)->insert($model);
                }
                break;

            case ApprovalStatus::DRAFT:
                if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                    App::make(ProductBranchSummaryService::class)->revertInventory($model);
                    App::make(ProductSerialNumberServiceInterface::class)->revertStatusFromInventoryAdjust($model);
                    App::make(ProductTransactionSummaryService::class)->revertInventoryAdjust($model);

                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
                        App::make(TransactionSummaryPostingServiceInterface::class)->delete($model);
                    }
                }
                break;
        }
    }
}
