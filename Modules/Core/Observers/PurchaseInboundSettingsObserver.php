<?php

namespace Modules\Core\Observers;

use App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\BranchPriceServiceInterface;
use Modules\Core\Enums\PurchaseInboundAutoUpdateCostSettings;

class PurchaseInboundSettingsObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status']) && $model->approval_status == ApprovalStatus::APPROVED) {
            if(setting('purchase.inbound.auto.update.cost') != PurchaseInboundAutoUpdateCostSettings::NO) {
                App::make(BranchPriceServiceInterface::class)->updatePurchasePriceFromTransaction($model);
            }
        }
    }
}