<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Concrete\ReferenceNumberService;
use Modules\Core\Enums\ApprovalStatus;
use Carbon\Carbon;

use Auth;
use Ramsey\Uuid\Uuid;

abstract class FinancialFlowObserver
{
    /**
     * Listen to the Model updating event.
     *
     * Set audited by and audited date
     *
     * @param  Model  $model
     * @return void
     */
    public function updating($model)
    {
        $model->audited_date = null;
        $model->audited_by = null;

        if (in_array((int) $model->approval_status, [
            ApprovalStatus::APPROVED,
            ApprovalStatus::DECLINED,
        ])) {
            $model->audited_date = new Carbon();
            $model->audited_by = Auth::user()->id;
        }
    }
}