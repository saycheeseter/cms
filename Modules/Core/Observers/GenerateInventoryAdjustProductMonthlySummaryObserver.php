<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\Contracts\GenerateInventoryAdjustmentProductMonthlySummary;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Jobs\GenerateProductMonthlySummaryFromInventoryAdjust;
use Modules\Core\Jobs\RevertProductMonthlySummaryFromInventoryAdjust;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;
use App;

class GenerateInventoryAdjustProductMonthlySummaryObserver
{
    public function updated(GenerateInventoryAdjustmentProductMonthlySummary $model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                        dispatch((new GenerateProductMonthlySummaryFromInventoryAdjust($model))->onQueue('high'));
                    }
                    break;
            }
        }
    }
}