<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Enums\TransactionSummary;
use App;

class ProductStockCardObserver
{
    public function created($model)
    {
        if(in_array($model->reference_type, [
            TransactionSummary::PURCHASE_INBOUND,
            TransactionSummary::STOCK_DELIVERY_INBOUND,
            TransactionSummary::STOCK_RETURN_INBOUND
        ])) {
            App::make(ProductBranchSummaryService::class)->updateCurrentCostFromStockCard($model);
        }
    }
}