<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionType;
use Modules\Core\Services\Contracts\RemainingQuantityServiceInterface;
use App;

class TransactionWithReferenceObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])
            && (
                in_array(get_class($model), [StockReturnInbound::class, StockDeliveryInbound::class])
                || $model->transaction_type === TransactionType::FROM_INVOICE
            )
        ) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(RemainingQuantityServiceInterface::class)
                        ->deductRemaining($model)
                        ->updateTransactionStatus($model);
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                        App::make(RemainingQuantityServiceInterface::class)
                            ->addRemaining($model)
                            ->updateTransactionStatus($model);
                    }
                    break;
            }
        }
    }
}