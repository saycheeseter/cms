<?php

namespace Modules\Core\Observers;

use Auth;

class UserAuditObserver
{
    /**
     * Listen to the Model creating event.
     *
     * @param  Model  $model
     * @return void
     */
    public function creating($model)
    {
        $model->created_by = Auth::user()->id;
        $model->modified_by = Auth::user()->id;
    }

    /**
     * Listen to the Model updating event.
     *
     * @param  Model  $model
     * @return void
     */
    public function updating($model)
    {
        $model->modified_by = Auth::user()->id;
    }
}