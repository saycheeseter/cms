<?php

namespace Modules\Core\Observers;

class InventoryChainDetailObserver extends InventoryFlowDetailObserver
{
    public function deleted($model)
    {
        parent::deleted($model);

        $model->serials()->detach();
        $model->batches()->detach();
    }
}
