<?php

namespace Modules\Core\Observers;

class ProductConversionDetailObserver
{
    public function created($model)
    {
        $model->component->is_used = 1;
        $model->component->save();
    }

    public function updated($model)
    {
        if ($model->isDirty(['ma_cost'])) {
            $model->transaction->ma_cost = $model->transaction->ma_cost->add($model->amount);
            $model->transaction->save();
        }
    }
}