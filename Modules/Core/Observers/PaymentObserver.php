<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\PaymentServiceInterface;
use Modules\Core\Services\Contracts\IssuedCheckServiceInterface as IssuedCheckService;
use Modules\Core\Services\Contracts\SupplierBalanceFlowServiceInterface as SupplierBalanceFlowService;
use Modules\Core\Enums\ApprovalStatus;
use Litipk\BigNumbers\Decimal;
use App;

class PaymentObserver extends FinancialFlowObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            $zero = Decimal::create(0, setting('monetary.precision'));

            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(PaymentServiceInterface::class)->updateTransactionsRemainingAmount($model);
                    App::make(IssuedCheckService::class)->createFromPayment($model);

                    if($model->total_excess_amount->isGreaterThan($zero)) {
                        App::make(SupplierBalanceFlowService::class)->fromExcess($model);
                    }

                    if ($model->total_from_balance->isGreaterThan($zero)) {
                        App::make(SupplierBalanceFlowService::class)->fromBalance($model);
                    }
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal()['approval_status'] == ApprovalStatus::APPROVED) {
                        App::make(PaymentServiceInterface::class)->revertTransactionsRemainingAmount($model);
                        App::make(IssuedCheckService::class)->deleteFromPayment($model);
                    
                        if($model->total_excess_amount->isGreaterThan($zero) 
                            || $model->total_from_balance->isGreaterThan($zero)
                        ) {
                            App::make(SupplierBalanceFlowService::class)->deleteByPayment($model->id);
                        }
                    }
                    break;
            }
        }
    }
}