<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Services\Contracts\ProductBatchServiceInterface;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface as ProductTransactionSummaryService;
use Modules\Core\Services\Contracts\ProductStockCardServiceInterface as ProductStockCardService;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Services\Contracts\ProductSerialNumberServiceInterface as ProductSerialNumberService;
use Modules\Core\Enums\ApprovalStatus;
use App;
use Modules\Core\Services\Contracts\TransactionSummaryPostingServiceInterface;

class InventoryChainObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(ProductBranchSummaryService::class)->computeInventory($model);
                    App::make(ProductSerialNumberService::class)->updateStatusFromTransaction($model);
                    App::make(ProductBatchServiceInterface::class)->createOrUpdateBatchOwnerFromTransaction($model);
                    App::make(ProductTransactionSummaryService::class)->summarizeTransaction($model);

                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                        App::make(ProductStockCardService::class)->create($model);
                    } else {
                        App::make(TransactionSummaryPostingServiceInterface::class)->insert($model);
                    }
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                        App::make(ProductBranchSummaryService::class)->revertInventory($model);
                        App::make(ProductSerialNumberService::class)->revertStatusFromTransaction($model);
                        App::make(ProductBatchServiceInterface::class)->revertBatchOwnerFromTransaction($model);
                        App::make(ProductTransactionSummaryService::class)->revertTransaction($model);

                        if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::MANUAL) {
                            App::make(TransactionSummaryPostingServiceInterface::class)->delete($model);
                        }
                    }
                    break;
            }
        }
    }
}
