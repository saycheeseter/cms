<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\PermissionSectionServiceInterface as PermissionSectionService;
use Modules\Core\Services\Contracts\PermissionModuleServiceInterface as PermissionModuleService;
use Modules\Core\Services\Contracts\PermissionServiceInterface as PermissionService;
use App;

class ReportBuilderObserver
{
    public function created($model)
    {   
        $section = App::make(PermissionSectionService::class)->create();

        $module = App::make(PermissionModuleService::class)->storeFromReportBuilder($model, $section->id);

        App::make(PermissionService::class)->storeFromReportBuilder($model, $module->id);
    }

    public function updated($model)
    {   
        if ($model->isDirty(['name'])) {
            App::make(PermissionModuleService::class)->updateFromReportBuilder($model);
            App::make(PermissionService::class)->updateFromReportBuilder($model);
        }
    }

    public function deleted($model)
    {
        App::make(PermissionService::class)->deleteFromReportBuilder($model);
        App::make(PermissionModuleService::class)->deleteFromReportBuilder($model);
        App::make(PermissionSectionService::class)->deleteFromReportBuilder();
    }
}
