<?php

namespace Modules\Core\Observers;

use Auth;

class BranchAuditObserver
{
    public function creating($model)
    {
        $model->created_from = branch();
    }
}