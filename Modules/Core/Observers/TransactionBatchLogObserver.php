<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\ApprovalStatus;
use App;
use Modules\Core\Services\Contracts\TransactionBatchLogServiceInterface;

class TransactionBatchLogObserver
{
    public function updating($model)
    {
        if ($model->isDirty('approval_status')
            && $model->getOriginal('approval_status') == ApprovalStatus::APPROVED
            && $model->approval_status === ApprovalStatus::DRAFT
        ) {
            App::make(TransactionBatchLogServiceInterface::class)->remove($model);
        }
    }

    public function updated($model)
    {
        if ($model->isDirty('approval_status') && $model->approval_status === ApprovalStatus::APPROVED) {
            App::make(TransactionBatchLogServiceInterface::class)->log($model);
        }
    }
}