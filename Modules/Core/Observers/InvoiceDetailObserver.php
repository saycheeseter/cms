<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Litipk\BigNumbers\Decimal;
use App;

class InvoiceDetailObserver extends InventoryFlowDetailObserver
{
    /**
     * Listen to the Model creating event.
     * Updates remaining_qty field
     *
     * @param  Model  $model
     * @return void
     */
    public function creating($model)
    {
        $model->remaining_qty = $model->total_qty;
    }

    /**
     * Listen to the Model updating event.
     * Updates remaining_qty field
     *
     * @param  Model  $model
     * @return void
     */
    public function updating($model)
    {
        if ($model->isDirty(['unit_qty', 'qty'])) {
            $model->remaining_qty = $model->total_qty;
        }

        if ($this->isRemainingQtyDeducted($model)) {
            $zero = Decimal::create(bcadd(0, 0, 6), setting('monetary.precision'));
            $orig = Decimal::create($model->getParsedOriginal('remaining_qty'), setting('monetary.precision'));

            $qty = $model->remaining_qty->isLessThan($zero)
                ? $orig
                : $orig->sub($model->remaining_qty, setting('monetary.precision'));

            App::make(ProductBranchSummaryService::class)->decrementPendingQuantity($model, $qty);
        }
    }

    private function isRemainingQtyDeducted($model)
    {
        return $model->isDirty('remaining_qty')
            && ! $model->remaining_qty->equals($model->total_qty);
    }
}