<?php

namespace Modules\Core\Observers;

use App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\SupplierProductPricingServiceInterface;

class TransactionSupplierHistoricalDataObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            App::make(SupplierProductPricingServiceInterface::class)->updateHistory($model);
        }
    }
}