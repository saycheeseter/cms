<?php

namespace Modules\Core\Observers;

use Litipk\BigNumbers\Decimal;

class InventoryFlowDetailObserver
{
    /**
     * Listen to the Model created event.
     * Updates total amount of the parent model
     *
     * @param  Model  $model
     * @return void
     */
    public function created($model)
    {
        if (! is_null($model->transaction)) {
            $model->transaction->total_amount = $model->transaction->total_amount->add($model->amount, setting('monetary.precision'));

            $model->transaction->save();
        }

        $model->product->is_used = 1;
        $model->product->save();
    }

    /**
     * Listen to the Model updated event.
     * Updates total amount of the parent model if qty, unit_qty or price
     * is updated
     *
     * @param  Model  $model
     * @return void
     */
    public function updated($model)
    {
        if ($model->isDirty(['unit_qty', 'qty', 'price'])
            && ! is_null($model->transaction)
        ) {
            $precision = setting('monetary.precision');

            $old = $model->getParsedOriginal();

            $oldQty = Decimal::create($old['qty'], $precision);
            $oldUnitQty = Decimal::create($old['unit_qty'], $precision);
            $oldPrice = Decimal::create($old['price'], $precision);
            $oldTotalQty = $oldQty->mul($oldUnitQty, $precision);
            $oldAmount = $oldPrice->mul($oldTotalQty, $precision);

            $newAmount = $model->transaction->total_amount->add($model->amount->sub($oldAmount, $precision), $precision);

            $model->transaction->total_amount = $newAmount;

            $model->transaction->save();
        }
    }

    /**
     * Listen to the Model deleted event.
     *
     * @param  Model  $model
     * @return void
     */
    public function deleted($model)
    {
        if (! $model->transaction->trashed()) {
            $model->transaction->total_amount = $model->transaction->total_amount->sub($model->amount, setting('monetary.precision'));

            $model->transaction->save();
        }

        $model->components()->detach();
    }
}
