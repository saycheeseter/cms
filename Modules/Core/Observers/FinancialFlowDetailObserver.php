<?php

namespace Modules\Core\Observers;

use Litipk\BigNumbers\Decimal;

abstract class FinancialFlowDetailObserver
{
    public function created($model)
    {
        if (! is_null($model->transaction)) {
            $model->transaction->total_amount = $model->transaction->total_amount->add($model->amount, setting('monetary.precision'));

            $model->transaction->save();
        }
    }

    public function updated($model)
    {
        if ($model->isDirty(['amount'])
            && ! is_null($model->transaction)
        ) {
            $precision = setting('monetary.precision');

            $old = $model->getOriginal();

            $oldAmount = Decimal::create((string)$old['amount'], $precision);

            $model->transaction->total_amount = $model->transaction->total_amount
                ->add($model->amount->sub($oldAmount, $precision), $precision);

            $model->transaction->save();
        }
    }

    public function deleted($model)
    {
        if (! $model->transaction->trashed()) {
            if($model->check) {
                App::make($this->services['check'])->deleteFromPaymentDetail($model);
            }

            $model->transaction->total_amount = $model->transaction->total_amount->sub($model->amount, setting('monetary.precision'));

            $model->transaction->save();
        }
    }
}