<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\ReceivedCheckServiceInterface;

class CollectionDetailObserver extends FinancialFlowDetailObserver
{
    protected $services = [
        'check' => ReceivedCheckServiceInterface::class
    ];
}