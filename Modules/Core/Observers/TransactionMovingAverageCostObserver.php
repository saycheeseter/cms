<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\TransactionMovingAverageCostServiceInterface;
use Modules\Core\Enums\ApprovalStatus;
use App;

class TransactionMovingAverageCostObserver
{
    public function updating($model)
    {
        if ($model->isDirty(['approval_status'])
            && $model->approval_status == ApprovalStatus::APPROVED
        ) {
            App::make(TransactionMovingAverageCostServiceInterface::class)->updateTransactionDetails($model);
        }
    }
}