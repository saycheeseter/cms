<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\CustomerProductPricingServiceInterface as CustomerProductPricingService;
use Modules\Core\Enums\CustomerPricingType;
use App;

class CustomerObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['pricing_type'])
            && $model->pricing_type  == CustomerPricingType::HISTORICAL
        ) {
            App::make(CustomerProductPricingService::class)->populate($model->id);
        }
    }
}
