<?php

namespace Modules\Core\Observers;

class AutoProductConversionObserver
{
    public function created($model)
    {
        $model->product->is_used = 1;
        $model->product->save();
    }

    public function updated($model)
    {
        if ($model->isDirty('ma_cost')) {
            $model->transaction->ma_cost = $model->ma_cost;
            $model->transaction->save();
        }
    }
}