<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\IssuedCheckServiceInterface;

class PaymentDetailObserver extends FinancialFlowDetailObserver
{
    protected $services = [
        'check' => IssuedCheckServiceInterface::class
    ];
}