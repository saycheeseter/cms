<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\Contracts\ModuleSeries;
use Modules\Core\Services\Contracts\ModuleSeriesServiceInterface;

use App;

class ModuleSeriesObserver
{
    public function creating(ModuleSeries $model)
    {
        App::make(ModuleSeriesServiceInterface::class)->execute($model);
    }

    public function updating(ModuleSeries $model)
    {
        if ($model->isDirty($model->series_column)) {
            App::make(ModuleSeriesServiceInterface::class)->execute($model);
        }
    }
}