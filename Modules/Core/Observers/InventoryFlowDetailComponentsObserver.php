<?php

namespace Modules\Core\Observers;

class InventoryFlowDetailComponentsObserver
{
    public function deleted($model)
    {
        $model->components()->detach();
    }
}