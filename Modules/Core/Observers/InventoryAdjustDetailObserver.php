<?php

namespace Modules\Core\Observers;

use Litipk\BigNumbers\Decimal;

class InventoryAdjustDetailObserver
{
    public function created($model)
    {
        if (! is_null($model->transaction)) {
            $zero = Decimal::create(0, setting('monetary.precision'));

            if ($model->amount->isLessThan($zero)) {
                $model->transaction->total_lost_amount = $model->transaction->total_lost_amount->add($model->amount->abs(), setting('monetary.precision'));
            } else {
                $model->transaction->total_gain_amount = $model->transaction->total_gain_amount->add($model->amount, setting('monetary.precision'));
            }

            $model->transaction->save();
        }
    }

    public function updated($model)
    {
        if ($model->isDirty(['unit_qty', 'qty', 'pc_qty', 'price'])
            && ! is_null($model->transaction)
        ) {
            $precision = setting('monetary.precision');
            $zero = Decimal::create(0, $precision);

            $oldAmount = $this->getOldAmount($model);
            
            // Subtract the old data
            if ($oldAmount->isLessThan($zero)) {
                $model->transaction->total_lost_amount = $model->transaction->total_lost_amount->sub($oldAmount->abs());
            } else {
                $model->transaction->total_gain_amount = $model->transaction->total_gain_amount->sub($oldAmount);
            }

            // Add the new data
            if ($model->amount->isLessThan($zero)) {
                $model->transaction->total_lost_amount = $model->transaction->total_lost_amount->add($model->amount->abs(), setting('monetary.precision'));
            } else {
                $model->transaction->total_gain_amount = $model->transaction->total_gain_amount->add($model->amount, setting('monetary.precision'));
            }

            $model->transaction->save();
        }
    }

    public function deleted($model)
    {
        if (! $model->transaction->trashed()) {
            $zero = Decimal::create(0, setting('monetary.precision'));

            if ($model->amount->isLessThan($zero)) {
                $model->transaction->total_lost_amount = $model->transaction->total_lost_amount->sub($model->amount->abs(), setting('monetary.precision'));
            } else {
                $model->transaction->total_gain_amount = $model->transaction->total_gain_amount->sub($model->amount, setting('monetary.precision'));
            }

            $model->transaction->save();
        }
    }

    private function getOldAmount($model)
    {
        $precision = setting('monetary.precision');

        $old = $model->getParsedOriginal();

        $oldQty = Decimal::create($old['qty'], $precision);
        $oldUnitQty = Decimal::create($old['unit_qty'], $precision);
        $oldPcQty = Decimal::create($old['pc_qty'], $precision);
        $oldPrice = Decimal::create($old['price'], $precision);
        $oldInventory = Decimal::create($old['inventory'], $precision);
        
        $oldTotalQty = $oldQty->mul($oldUnitQty, $precision)->add($oldPcQty);
        $oldDiffQty = $oldTotalQty->sub($oldInventory);

        return $oldPrice->mul($oldDiffQty, $precision);
    }
}
