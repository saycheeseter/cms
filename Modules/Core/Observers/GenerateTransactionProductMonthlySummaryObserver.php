<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\Contracts\GenerateTransactionProductMonthlySummary;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Jobs\GenerateProductMonthlySummaryFromTransactions;
use App;
use Modules\Core\Jobs\RevertProductMonthlySummaryFromTransactions;

class GenerateTransactionProductMonthlySummaryObserver
{
    public function updated(GenerateTransactionProductMonthlySummary $model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                        dispatch((new GenerateProductMonthlySummaryFromTransactions($model))->onQueue('high'));
                    }
                    break;
            }
        }
    }
}