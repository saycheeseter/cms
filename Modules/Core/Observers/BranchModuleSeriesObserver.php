<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\Contracts\BranchModuleSeries;
use Modules\Core\Services\Contracts\BranchModuleSeriesServiceInterface;

use App;

class BranchModuleSeriesObserver
{
    public function creating(BranchModuleSeries $model)
    {
        App::make(BranchModuleSeriesServiceInterface::class)->execute($model);
    }

    public function updating(BranchModuleSeries $model)
    {
        if ($model->isDirty($model->series_column)) {
            App::make(BranchModuleSeriesServiceInterface::class)->execute($model);
        }
    }
}