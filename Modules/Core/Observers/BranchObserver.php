<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\BranchModuleSeriesServiceInterface as BranchModuleSeriesService;
use Modules\Core\Services\Contracts\BranchSettingServiceInterface as BranchSettingService;
use Modules\Core\Services\Contracts\BranchPriceServiceInterface as BranchPriceService;
use Modules\Core\Services\Contracts\ProductBranchServiceInterface as ProductBranchService;
use App;

class BranchObserver
{
    /**
     * Listen to the Model created event.
     *
     * @param  Model  $branch
     * @return void
     */
    public function created($branch)
    {
        App::make(BranchSettingService::class)->copy($branch);
        App::make(BranchPriceService::class)->copy($branch);
        App::make(ProductBranchService::class)->createInitialData($branch);
        App::make(BranchModuleSeriesService::class)->createInitialData($branch);
    }

    public function deleted($branch)
    {
        App::make(BranchSettingService::class)->deleteFromBranch($branch);
        App::make(BranchPriceService::class)->deleteFromBranch($branch);
        App::make(ProductBranchService::class)->deleteFromBranch($branch);
        App::make(BranchModuleSeriesService::class)->deleteFromBranch($branch);
    }
}
