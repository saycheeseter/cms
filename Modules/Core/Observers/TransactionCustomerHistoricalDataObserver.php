<?php

namespace Modules\Core\Observers;

use App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\Customer;
use Modules\Core\Enums\CustomerPricingType;
use Modules\Core\Services\Contracts\CustomerProductPricingServiceInterface;

class TransactionCustomerHistoricalDataObserver
{
    public function updated($model)
    {
        if($model->isDirty(['approval_status'])
            && $model->approval_status == ApprovalStatus::APPROVED
            && $model->customer_type != Customer::WALK_IN
            && ! is_null($model->customer)
            && $model->customer->pricing_type  == CustomerPricingType::HISTORICAL
        ) {
            App::make(CustomerProductPricingServiceInterface::class)->updateHistory($model);
        }
    }
}