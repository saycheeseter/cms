<?php

namespace Modules\Core\Observers;

use App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface;

class SummaryPendingQuantityTransactionObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(ProductBranchSummaryServiceInterface::class)->computeTransactionPendingQuantity($model);
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                        App::make(ProductBranchSummaryServiceInterface::class)->revertTransactionPendingQuantity($model);
                    }
                    break;
            }
        }
    }
}