<?php

namespace Modules\Core\Observers;

use Modules\Core\Entities\Contracts\GenerateConversionProductMonthlySummary;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionSummaryComputation;
use Modules\Core\Jobs\GenerateProductMonthlySummaryFromConversion;
use Modules\Core\Jobs\RevertProductMonthlySummaryFromConversion;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;
use App;

class GenerateConversionProductMonthlySummaryObserver
{
    public function updated(GenerateConversionProductMonthlySummary $model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    if (setting('transaction.summary.computation.mode') === TransactionSummaryComputation::AUTOMATIC) {
                        dispatch((new GenerateProductMonthlySummaryFromConversion($model))->onQueue('high'));
                    }
                    break;
            }
        }
    }
}