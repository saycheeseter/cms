<?php

namespace Modules\Core\Observers;

use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface as ProductBranchSummaryService;
use Modules\Core\Services\Contracts\ProductBranchDetailServiceInterface as ProductBranchDetailService;
use App;

class BranchDetailObserver
{
    public function created($model)
    {
        App::make(ProductBranchSummaryService::class)->createInitialData($model);
        App::make(ProductBranchDetailService::class)->createInitialData($model);
    }

    public function deleted($model)
    {
        App::make(ProductBranchSummaryService::class)->deleteFromBranchDetail($model);
        App::make(ProductBranchDetailService::class)->deleteFromBranchDetail($model);
    }
}
