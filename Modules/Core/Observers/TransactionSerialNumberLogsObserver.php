<?php

namespace Modules\Core\Observers;

use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\TransactionSerialNumberLogsServiceInterface;
use App;

class TransactionSerialNumberLogsObserver
{
    public function updating($model)
    {
        if ($model->isDirty('approval_status')
            && $model->getOriginal('approval_status') == ApprovalStatus::APPROVED
            && $model->approval_status === ApprovalStatus::DRAFT
        ) {
            App::make(TransactionSerialNumberLogsServiceInterface::class)->remove($model);
        }
    }

    public function updated($model)
    {
        if ($model->isDirty('approval_status') && $model->approval_status === ApprovalStatus::APPROVED) {
            App::make(TransactionSerialNumberLogsServiceInterface::class)->log($model);
        }
    }
}