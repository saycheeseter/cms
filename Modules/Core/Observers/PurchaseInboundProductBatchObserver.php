<?php

namespace Modules\Core\Observers;

use App;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Services\Contracts\ProductBatchServiceInterface;

class PurchaseInboundProductBatchObserver
{
    public function updated($model)
    {
        if ($model->isDirty(['approval_status'])) {
            switch ($model->approval_status) {
                case ApprovalStatus::APPROVED:
                    App::make(ProductBatchServiceInterface::class)->setAvailableStatusFromPurchaseInbound($model);
                    break;

                case ApprovalStatus::DRAFT:
                    if ($model->getOriginal('approval_status') == ApprovalStatus::APPROVED) {
                        App::make(ProductBatchServiceInterface::class)->setNotAvailableStatusFromPurchaseInbound($model);
                    }
                    break;
            }
        }
    }
}