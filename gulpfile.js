const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

let assetsPath = './Modules/Core/Assets';
let sassPath = assetsPath + '/sass';
let jsPath = assetsPath + '/js';

elixir.config.notifications = false;

elixir((mix) => {

    mix.sass([
        sassPath + '/base.sass'
    ], 'public/css/core.css');

    mix.webpack('bootstrap.js', 'public/js/core.js', jsPath)
        .webpack('Bank/transaction.js', 'public/js/Bank/transaction.js', jsPath)
        .webpack('BankAccount/list.js', 'public/js/BankAccount/list.js', jsPath)
        .webpack('Branch/list.js', 'public/js/Branch/list.js', jsPath)
        .webpack('Category/list.js', 'public/js/Category/list.js', jsPath)
        .webpack('ChangeLogs/changelogs.js', 'public/js/ChangeLogs/changelogs.js', jsPath)
        .webpack('CheckManagement/list.js', 'public/js/CheckManagement/list.js', jsPath)
        .webpack('Collection/detail.js', 'public/js/Collection/detail.js', jsPath)
        .webpack('Collection/list.js', 'public/js/Collection/list.js', jsPath)
        .webpack('Counter/detail.js', 'public/js/Counter/detail.js', jsPath)
        .webpack('Counter/list.js', 'public/js/Counter/list.js', jsPath)
        .webpack('Customer/list.js', 'public/js/Customer/list.js', jsPath)
        .webpack('Damage/detail.js', 'public/js/Damage/detail.js', jsPath)
        .webpack('Damage/list.js', 'public/js/Damage/list.js', jsPath)
        .webpack('DamageOutbound/detail.js', 'public/js/DamageOutbound/detail.js', jsPath)
        .webpack('DamageOutbound/list.js', 'public/js/DamageOutbound/list.js', jsPath)
        .webpack('Dashboard/dashboard.js', 'public/js/Dashboard/dashboard.js', jsPath)
        .webpack('DataCollector/transaction-list.js', 'public/js/DataCollector/transaction-list.js', jsPath)
        .webpack('Error/unauthorized.js', 'public/js/Error/unauthorized.js', jsPath)
        .webpack('ExcelTemplate/detail.js', 'public/js/ExcelTemplate/detail.js', jsPath)
        .webpack('ExcelTemplate/list.js', 'public/js/ExcelTemplate/list.js', jsPath)
        .webpack('GiftCheck/list.js', 'public/js/GiftCheck/list.js', jsPath)
        .webpack('InventoryAdjust/detail.js', 'public/js/InventoryAdjust/detail.js', jsPath)
        .webpack('InventoryAdjust/list.js', 'public/js/InventoryAdjust/list.js', jsPath)
        .webpack('IssuedCheck/list.js', 'public/js/IssuedCheck/list.js', jsPath)
        .webpack('Login/auth.js', 'public/js/Login/auth.js', jsPath)
        .webpack('Member/list.js', 'public/js/Member/list.js', jsPath)
        .webpack('MemberRate/list.js', 'public/js/MemberRate/list.js', jsPath)
        .webpack('OtherIncome/list.js', 'public/js/OtherIncome/list.js', jsPath)
        .webpack('OtherPayment/list.js', 'public/js/OtherPayment/list.js', jsPath)
        .webpack('Payment/detail.js', 'public/js/Payment/detail.js', jsPath)
        .webpack('Payment/list.js', 'public/js/Payment/list.js', jsPath)
        .webpack('PosSales/detail.js', 'public/js/PosSales/detail.js', jsPath)
        .webpack('PosSales/list.js', 'public/js/PosSales/list.js', jsPath)
        .webpack('PosSummaryPosting/list.js', 'public/js/PosSummaryPosting/list.js', jsPath)
        .webpack('Printout/detail.js', 'public/js/Printout/detail.js', jsPath)
        .webpack('Printout/list.js', 'public/js/Printout/list.js', jsPath)
        .webpack('Product/detail.js', 'public/js/Product/detail.js', jsPath)
        .webpack('Product/list.js', 'public/js/Product/list.js', jsPath)
        .webpack('Product/summary.js', 'public/js/Product/summary.js', jsPath)
        .webpack('ProductConversion/detail.js', 'public/js/ProductConversion/detail.js', jsPath)
        .webpack('ProductConversion/list.js', 'public/js/ProductConversion/list.js', jsPath)
        .webpack('ProductDiscount/detail.js', 'public/js/ProductDiscount/detail.js', jsPath)
        .webpack('ProductDiscount/list.js', 'public/js/ProductDiscount/list.js', jsPath)
        .webpack('PurchaseInbound/detail.js', 'public/js/PurchaseInbound/detail.js', jsPath)
        .webpack('PurchaseInbound/list.js', 'public/js/PurchaseInbound/list.js', jsPath)
        .webpack('PurchaseOrder/detail.js', 'public/js/PurchaseOrder/detail.js', jsPath)
        .webpack('PurchaseOrder/list.js', 'public/js/PurchaseOrder/list.js', jsPath)
        .webpack('PurchaseReturn/detail.js', 'public/js/PurchaseReturn/detail.js', jsPath)
        .webpack('PurchaseReturn/list.js', 'public/js/PurchaseReturn/list.js', jsPath)
        .webpack('PurchaseReturnOutbound/detail.js', 'public/js/PurchaseReturnOutbound/detail.js', jsPath)
        .webpack('PurchaseReturnOutbound/list.js', 'public/js/PurchaseReturnOutbound/list.js', jsPath)
        .webpack('ReceivedCheck/list.js', 'public/js/ReceivedCheck/list.js', jsPath)
        .webpack('Reports/Brand/monthly-sales-ranking.js', 'public/js/Reports/Brand/monthly-sales-ranking.js', jsPath)
        .webpack('Reports/Builder/detail.js', 'public/js/Reports/Builder/detail.js', jsPath)
        .webpack('Reports/Builder/list.js', 'public/js/Reports/Builder/list.js', jsPath)
        .webpack('Reports/Category/monthly-sales-ranking.js', 'public/js/Reports/Category/monthly-sales-ranking.js', jsPath)
        .webpack('Reports/Custom/template.js', 'public/js/Reports/Custom/template.js', jsPath)
        .webpack('Reports/Customer/balance.js', 'public/js/Reports/Customer/balance.js', jsPath)
        .webpack('Reports/Customer/product-price.js', 'public/js/Reports/Customer/product-price.js', jsPath)
        .webpack('Reports/Member/report.js', 'public/js/Reports/Member/report.js', jsPath)
        .webpack('Reports/Pos/discounted-summary.js', 'public/js/Reports/Pos/discounted-summary.js', jsPath)
        .webpack('Reports/Product/branch-inventory.js', 'public/js/Reports/Product/branch-inventory.js', jsPath)
        .webpack('Reports/Product/inventory-value.js', 'public/js/Reports/Product/inventory-value.js', jsPath)
        .webpack('Reports/Product/inventory-value.js', 'public/js/Reports/Product/inventory-value.js', jsPath)
        .webpack('Reports/Product/inventory-warning.js', 'public/js/Reports/Product/inventory-warning.js', jsPath)
        .webpack('Reports/Product/monthly-sales-ranking.js', 'public/js/Reports/Product/monthly-sales-ranking.js', jsPath)
        .webpack('Reports/Product/negative-profit.js', 'public/js/Reports/Product/negative-profit.js', jsPath)
        .webpack('Reports/Product/periodic-sales.js', 'public/js/Reports/Product/periodic-sales.js', jsPath)
        .webpack('Reports/Product/serial-transaction.js', 'public/js/Reports/Product/serial-transaction.js', jsPath)
        .webpack('Reports/Product/transaction-list.js', 'public/js/Reports/Product/transaction-list.js', jsPath)
        .webpack('Reports/Sales/detail-report.js', 'public/js/Reports/Sales/detail-report.js', jsPath)
        .webpack('Reports/Salesman/report.js', 'public/js/Reports/Salesman/report.js', jsPath)
        .webpack('Reports/Senior/transaction-report.js', 'public/js/Reports/Senior/transaction-report.js', jsPath)
        .webpack('Reports/Supplier/balance.js', 'public/js/Reports/Supplier/balance.js', jsPath)
        .webpack('Reports/Supplier/monthly-sales-ranking.js', 'public/js/Reports/Supplier/monthly-sales-ranking.js', jsPath)
        .webpack('Reports/Supplier/product-price.js', 'public/js/Reports/Supplier/product-price.js', jsPath)
        .webpack('Reports/audit-trail.js', 'public/js/Reports/audit-trail.js', jsPath)
        .webpack('Reports/audit-trail.js', 'public/js/Reports/audit-trail.js', jsPath)
        .webpack('Reports/bank-transaction.js', 'public/js/Reports/bank-transaction.js', jsPath)
        .webpack('Reports/bank-transaction.js', 'public/js/Reports/bank-transaction.js', jsPath)
        .webpack('Reports/cash-transaction.js', 'public/js/Reports/cash-transaction.js', jsPath)
        .webpack('Reports/collection-due-warning.js', 'public/js/Reports/collection-due-warning.js', jsPath)
        .webpack('Reports/income-statement.js', 'public/js/Reports/income-statement.js', jsPath)
        .webpack('Reports/income-statement.js', 'public/js/Reports/income-statement.js', jsPath)
        .webpack('Reports/payment-due-warning.js', 'public/js/Reports/payment-due-warning.js', jsPath)
        .webpack('Reports/pos-sales.js', 'public/js/Reports/pos-sales.js', jsPath)
        .webpack('Role/detail.js', 'public/js/Role/detail.js', jsPath)
        .webpack('Role/list.js', 'public/js/Role/list.js', jsPath)
        .webpack('Sales/detail.js', 'public/js/Sales/detail.js', jsPath)
        .webpack('Sales/list.js', 'public/js/Sales/list.js', jsPath)
        .webpack('SalesOutbound/detail.js', 'public/js/SalesOutbound/detail.js', jsPath)
        .webpack('SalesOutbound/list.js', 'public/js/SalesOutbound/list.js', jsPath)
        .webpack('SalesReturn/detail.js', 'public/js/SalesReturn/detail.js', jsPath)
        .webpack('SalesReturn/list.js', 'public/js/SalesReturn/list.js', jsPath)
        .webpack('SalesReturnInbound/detail.js', 'public/js/SalesReturnInbound/detail.js', jsPath)
        .webpack('SalesReturnInbound/list.js', 'public/js/SalesReturnInbound/list.js', jsPath)
        .webpack('Settings/settings.js', 'public/js/Settings/settings.js', jsPath)
        .webpack('StockDelivery/detail.js', 'public/js/StockDelivery/detail.js', jsPath)
        .webpack('StockDelivery/list.js', 'public/js/StockDelivery/list.js', jsPath)
        .webpack('StockDeliveryInbound/detail.js', 'public/js/StockDeliveryInbound/detail.js', jsPath)
        .webpack('StockDeliveryInbound/list.js', 'public/js/StockDeliveryInbound/list.js', jsPath)
        .webpack('StockDeliveryOutboundFrom/detail.js', 'public/js/StockDeliveryOutboundFrom/detail.js', jsPath)
        .webpack('StockDeliveryOutboundFrom/list.js', 'public/js/StockDeliveryOutboundFrom/list.js', jsPath)
        .webpack('StockDeliveryOutboundTo/detail.js', 'public/js/StockDeliveryOutboundTo/detail.js', jsPath)
        .webpack('StockDeliveryOutboundTo/list.js', 'public/js/StockDeliveryOutboundTo/list.js', jsPath)
        .webpack('StockLocation/list.js', 'public/js/StockLocation/list.js', jsPath)
        .webpack('StockRequestFrom/detail.js', 'public/js/StockRequestFrom/detail.js', jsPath)
        .webpack('StockRequestFrom/list.js', 'public/js/StockRequestFrom/list.js', jsPath)
        .webpack('StockRequestTo/detail.js', 'public/js/StockRequestTo/detail.js', jsPath)
        .webpack('StockRequestTo/list.js', 'public/js/StockRequestTo/list.js', jsPath)
        .webpack('StockReturn/detail.js', 'public/js/StockReturn/detail.js', jsPath)
        .webpack('StockReturn/list.js', 'public/js/StockReturn/list.js', jsPath)
        .webpack('StockReturnInbound/detail.js', 'public/js/StockReturnInbound/detail.js', jsPath)
        .webpack('StockReturnInbound/list.js', 'public/js/StockReturnInbound/list.js', jsPath)
        .webpack('StockReturnOutbound/detail.js', 'public/js/StockReturnOutbound/detail.js', jsPath)
        .webpack('StockReturnOutbound/list.js', 'public/js/StockReturnOutbound/list.js', jsPath)
        .webpack('Supplier/list.js', 'public/js/Supplier/list.js', jsPath)
        .webpack('SystemCode/list.js', 'public/js/SystemCode/list.js', jsPath)
        .webpack('Testing/testing.js', 'public/js/Testing/testing.js', jsPath)
        .webpack('TransactionSummaryPosting/list.js', 'public/js/TransactionSummaryPosting/list.js', jsPath)
        .webpack('TranslationManager/list.js', 'public/js/TranslationManager/list.js', jsPath)
        .webpack('User/detail.js', 'public/js/User/detail.js', jsPath)
        .webpack('User/list.js', 'public/js/User/list.js', jsPath)
        .webpack('UserMenu/user-menu.js', 'public/js/UserMenu/user-menu.js', jsPath)
    ;

    mix.version([
        'css/core.css',
        'js/core.js',
        'js/Bank/transaction.js',
        'js/BankAccount/list.js',
        'js/Branch/list.js',
        'js/Category/list.js',
        'js/ChangeLogs/changelogs.js',
        'js/CheckManagement/list.js',
        'js/Collection/detail.js',
        'js/Collection/list.js',
        'js/Counter/detail.js',
        'js/Counter/list.js',
        'js/Customer/list.js',
        'js/Damage/detail.js',
        'js/Damage/list.js',
        'js/DamageOutbound/detail.js',
        'js/DamageOutbound/list.js',
        'js/Dashboard/dashboard.js',
        'js/DataCollector/transaction-list.js',
        'js/Error/unauthorized.js',
        'js/ExcelTemplate/detail.js',
        'js/ExcelTemplate/list.js',
        'js/GiftCheck/list.js',
        'js/InventoryAdjust/detail.js',
        'js/InventoryAdjust/list.js',
        'js/IssuedCheck/list.js',
        'js/Login/auth.js',
        'js/Member/list.js',
        'js/MemberRate/list.js',
        'js/OtherIncome/list.js',
        'js/OtherPayment/list.js',
        'js/Payment/detail.js',
        'js/Payment/list.js',
        'js/PosSales/detail.js',
        'js/PosSales/list.js',
        'js/PosSummaryPosting/list.js',
        'js/Printout/detail.js',
        'js/Printout/list.js',
        'js/Product/branch-inventory.js',
        'js/Product/detail.js',
        'js/Product/list.js',
        'js/Product/periodic-sales.js',
        'js/Product/summary.js',
        'js/ProductConversion/detail.js',
        'js/ProductConversion/list.js',
        'js/ProductDiscount/detail.js',
        'js/ProductDiscount/list.js',
        'js/PurchaseInbound/detail.js',
        'js/PurchaseInbound/list.js',
        'js/PurchaseOrder/detail.js',
        'js/PurchaseOrder/list.js',
        'js/PurchaseReturn/detail.js',
        'js/PurchaseReturn/list.js',
        'js/PurchaseReturnOutbound/detail.js',
        'js/PurchaseReturnOutbound/list.js',
        'js/ReceivedCheck/list.js',
        'js/Reports/Brand/monthly-sales-ranking.js',
        'js/Reports/Builder/detail.js',
        'js/Reports/Builder/list.js',
        'js/Reports/Category/monthly-sales-ranking.js',
        'js/Reports/Custom/template.js',
        'js/Reports/Custom/template.js',
        'js/Reports/Customer/balance.js',
        'js/Reports/Customer/product-price.js',
        'js/Reports/Member/report.js',
        'js/Reports/Pos/discounted-summary.js',
        'js/Reports/Product/inventory-value.js',
        'js/Reports/Product/inventory-warning.js',
        'js/Reports/Product/monthly-sales-ranking.js',
        'js/Reports/Product/negative-profit.js',
        'js/Reports/Product/serial-transaction.js',
        'js/Reports/Product/transaction-list.js',
        'js/Reports/Sales/detail-report.js',
        'js/Reports/Salesman/report.js',
        'js/Reports/Senior/transaction-report.js',
        'js/Reports/Supplier/balance.js',
        'js/Reports/Supplier/monthly-sales-ranking.js',
        'js/Reports/Supplier/product-price.js',
        'js/Reports/audit-trail.js',
        'js/Reports/audit-trail.js',
        'js/Reports/bank-transaction.js',
        'js/Reports/bank-transaction.js',
        'js/Reports/cash-transaction.js',
        'js/Reports/collection-due-warning',
        'js/Reports/income-statement.js',
        'js/Reports/income-statement.js',
        'js/Reports/payment-due-warning.js',
        'js/Reports/pos-sales.js',
        'js/Role/detail.js',
        'js/Role/list.js',
        'js/Sales/detail.js',
        'js/Sales/list.js',
        'js/SalesOutbound/detail.js',
        'js/SalesOutbound/list.js',
        'js/SalesReturn/detail.js',
        'js/SalesReturn/list.js',
        'js/SalesReturnInbound/detail.js',
        'js/SalesReturnInbound/list.js',
        'js/Settings/settings.js',
        'js/StockDelivery/detail.js',
        'js/StockDelivery/list.js',
        'js/StockDeliveryInbound/detail.js',
        'js/StockDeliveryInbound/list.js',
        'js/StockDeliveryOutboundFrom/detail.js',
        'js/StockDeliveryOutboundFrom/list.js',
        'js/StockDeliveryOutboundTo/detail.js',
        'js/StockDeliveryOutboundTo/list.js',
        'js/StockLocation/list.js',
        'js/StockRequestFrom/detail.js',
        'js/StockRequestFrom/list.js',
        'js/StockRequestTo/detail.js',
        'js/StockRequestTo/list.js',
        'js/StockReturn/detail.js',
        'js/StockReturn/list.js',
        'js/StockReturnInbound/detail.js',
        'js/StockReturnInbound/list.js',
        'js/StockReturnOutbound/detail.js',
        'js/StockReturnOutbound/list.js',
        'js/Supplier/list.js',
        'js/SystemCode/list.js',
        'js/Testing/testing.js',
        'js/TransactionSummaryPosting/list.js',
        'js/TranslationManager/list.js',
        'js/User/detail.js',
        'js/User/list.js',
        'js/UserMenu/user-menu.js',
    ]);

    mix.copy([
        'node_modules/font-awesome/fonts',
        assetsPath + '/fonts'
    ], 'public/fonts');

    mix.task('exit');
});

gulp.task('exit', function () {
    process.exit(0);
});