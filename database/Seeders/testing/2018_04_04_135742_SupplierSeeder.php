<?php

use Modules\Core\Entities\Supplier;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Entities\Superadmin;
use Carbon\Carbon;

class SupplierSeeder extends SmartSeeder
{
    protected function execute()
    {
        $now = Carbon::now();
        $superadmin = Superadmin::first();

        $supplier = Supplier::make([
            'code' => '0',
            'full_name' => 'None',
            'status' => '1'
        ]);

        $supplier->created_at = $now;
        $supplier->updated_at = $now;
        $supplier->created_from = setting('main.branch');
        $supplier->created_by = $superadmin->id;
        $supplier->modified_by = $superadmin->id;
        $supplier->save();
    }

    protected function back()
    {
        $supplier = Supplier::where('code', 0)
            ->where('full_name', 'None')
            ->first();

        $supplier->forceDelete();
    }
}
