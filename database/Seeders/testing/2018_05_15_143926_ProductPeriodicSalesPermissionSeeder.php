<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductPeriodicSalesPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Product Periodic Sales',
            'alias' => 'product_periodic_sales',
            'translation' => 'label.product.periodic.sales.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Product Periodic Sales',
            'alias' => 'view_product_periodic_sales',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Export Product Periodic Sales',
            'alias' => 'export_product_periodic_sales',
            'translation' => 'label.export.excel',
            'sequence' => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'export_product_periodic_sales')->delete();
        Permission::where('alias', 'view_product_periodic_sales')->delete();
        PermissionModule::where('alias', 'product_periodic_sales')->delete();
    }
}
