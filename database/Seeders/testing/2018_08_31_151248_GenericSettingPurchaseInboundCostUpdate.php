<?php

use Modules\Core\Enums\PurchaseInboundAutoUpdateCostSettings;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class GenericSettingPurchaseInboundCostUpdate extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $settings = [
            'type' => 'int',
            'comments' => 'Auto-update Cost (0 - No, 1 - Own Branch, 2 - All Branches, 3 - Copied to Branch)',
            'value' => 0
        ];

        $existing = DB::table('generic_settings')
            ->where('key', 'purchase.inbound.auto.update.cost')
            ->first();

        if($existing->value) {
            $settings['value'] = PurchaseInboundAutoUpdateCostSettings::OWN_BRANCH;
        }

        DB::table('generic_settings')
            ->where('key', 'purchase.inbound.auto.update.cost')
            ->update($settings);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $settings = [
            'type' => 'boolean',
            'comments' => 'Auto-update Cost',
            'value' => false
        ];

        $existing = DB::table('generic_settings')
            ->where('key', 'purchase.inbound.auto.update.cost')
            ->first();

        if($existing->value !== PurchaseInboundAutoUpdateCostSettings::NO) {
            $settings['value'] = true;
        }

        DB::table('generic_settings')
            ->where('key', 'purchase.inbound.auto.update.cost')
            ->update($settings);
    }
}
