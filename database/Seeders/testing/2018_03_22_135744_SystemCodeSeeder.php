<?php

use Modules\Core\Entities\PaymentMethod;
use Modules\Core\Entities\UOM;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SystemCodeSeeder extends SmartSeeder
{
    protected function execute()
    {
        UOM::create([
            'code' => 'pc',
            'name' => 'Piece',
            'remarks' => ''
        ]);

        PaymentMethod::create([
            'name' => 'Cash',
            'code' => '1',
        ]);

        PaymentMethod::create([
            'name' => 'Check',
            'code' => '2',
        ]);

        PaymentMethod::create([
            'name' => 'Bank Deposit',
            'code' => '3',
        ]);

        PaymentMethod::create([
            'name' => 'From Balance',
            'code' => '4',
        ]);

        PaymentMethod::create([
            'name' => 'Credit Card',
            'code' => '5',
        ]);

        PaymentMethod::create([
            'name' => 'Debit Card',
            'code' => '6',
        ]);

        PaymentMethod::create([
            'name' => 'Gift Check',
            'code' => '7',
        ]);

        PaymentMethod::create([
            'name' => 'Member Points',
            'code' => '8',
        ]);

        PaymentMethod::create([
            'name' => 'Eplus',
            'code' => '9',
        ]);

        PaymentMethod::create([
            'name' => 'Smac',
            'code' => '10',
        ]);

        PaymentMethod::create([
            'name' => 'Online Deal',
            'code' => '11',
        ]);

        PaymentMethod::create([
            'name' => 'Coupon',
            'code' => '12',
        ]);
    }

    protected function back()
    {
        UOM::getQuery()->delete();
        PaymentMethod::getQuery()->delete();
    }
}
