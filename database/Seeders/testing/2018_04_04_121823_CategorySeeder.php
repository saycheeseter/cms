<?php

use Modules\Core\Entities\Category;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Entities\Superadmin;
use Carbon\Carbon;

class CategorySeeder extends SmartSeeder
{
    protected function execute()
    {
        $now = Carbon::now();
        $superadmin = Superadmin::first();

        $category = Category::make([
            'code' => '0',
            'name' => 'None'
        ]);

        $category->created_at = $now;
        $category->updated_at = $now;
        $category->created_by = $superadmin->id;
        $category->modified_by = $superadmin->id;
        $category->save();
    }

    protected function back()
    {
        $category = Category::where('code', 0)
            ->where('name', 'None')
            ->first();

        $category->forceDelete();
    }
}
