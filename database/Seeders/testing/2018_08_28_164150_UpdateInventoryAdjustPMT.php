<?php

use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Services\Contracts\ProductMonthlyTransactionServiceInterface;

class UpdateInventoryAdjustPMT extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $service = App::make(ProductMonthlyTransactionServiceInterface::class);

        ProductMonthlyTransactions::getQuery()
            ->update([
                'adjustment_increase_total_qty'     => 0,
                'adjustment_increase_total_amount'  => 0,
                'adjustment_increase_total_cost'    => 0,
                'adjustment_increase_total_ma_cost' => 0,
                'adjustment_decrease_total_qty'     => 0,
                'adjustment_decrease_total_amount'  => 0,
                'adjustment_decrease_total_cost'    => 0,
                'adjustment_decrease_total_ma_cost' => 0
            ]);

        $adjustments = InventoryAdjust::with(['details'])
            ->where('approval_status', ApprovalStatus::APPROVED)
            ->cursor();

        foreach ($adjustments as $adjustment) {
            $service->generateFromInventoryAdjust($adjustment);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
