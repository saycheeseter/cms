<?php

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchSetting;
use Modules\Core\Entities\GenericSetting;
use Modules\Core\Entities\UOM;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SettingSeeder extends SmartSeeder
{
    protected function execute()
    {
        $this->seedGenericSettings();
        $this->seedBranchSettings();
    }

    protected function back()
    {
        BranchSetting::getQuery()->delete();
        GenericSetting::getQuery()->delete();
    }

    protected function seedBranchSettings()
    {
        $branches = Branch::all();

        foreach ($branches as $branch) {
            BranchSetting::create([
                'type' => 'boolean',
                'branch_id' => $branch->id,
                'key' => 'purchase.inbound.display.purchase.other.branches',
                'value' => 'false',
                'comments' => 'Display purchase of other branches'
            ]);

            BranchSetting::create([
                'type' => 'boolean',
                'branch_id' => $branch->id,
                'key' => 'payment.display.purchase.inbound.of.other.branches',
                'value' => 'false',
                'comments' => 'Display purchase inbound of other branches'
            ]);

            BranchSetting::create([
                'type' => 'boolean',
                'branch_id' => $branch->id,
                'key' => 'collection.display.sales.outbound.of.other.branches',
                'value' => 'false',
                'comments' => 'Display sales outbound of other branches'
            ]);
        }
    }

    protected function seedGenericSettings()
    {
        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        GenericSetting::create([
            'type' => 'int',
            'key' => 'main.branch',
            'value' => $branch->id,
            'comments' => 'id of main branch'
        ]);

        $uom = UOM::where('code', 'pc')
            ->where('name', 'Piece')
            ->first();

        GenericSetting::create([
            'type' => 'int',
            'key' => 'default.unit',
            'value' => $uom->id,
            'comments' => 'ID of Piece UOM'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'monetary.precision',
            'value' => '2',
            'comments' => 'Decimal precision for all (amount, price, discounts)'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'product.view.prices',
            'value' => '1',
            'comments' => 'View product prices of ( 0 - This Branch Only, 1 - All Branches)'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'product.show.price.abnormality.warning',
            'value' => 'false',
            'comments' => 'Show pricing abnormality warnings'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'poi.price.based.on.history',
            'value' => 'false',
            'comments' => 'Price shown is based on history'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'poi.unit.based.on.history',
            'value' => 'false',
            'comments' => 'Unit shown is based on history'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'purchase.inbound.auto.update.cost',
            'value' => 'false',
            'comments' => 'Auto-update Cost'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'soi.regular.customers.price.basis',
            'value' => '2',
            'comments' => 'For regular customers, prices are based from (0 - customer setting, 1 - wholesale, 2 - retail)'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'soi.walkin.customers.price.basis',
            'value' => '1',
            'comments' => 'For walk-in customers, prices are based from (0 - wholesale, 1 - retail, 2 - price A, 3 - price b, 4 - price c, 5 - price d, 6 - price e)'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'soi.unit.shown.base.on.history',
            'value' => 'false',
            'comments' => 'Unit shown base on history'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'request.warning.already.requested.delivered.toggle',
            'value' => 'false',
            'comments' => 'Already requested and delivered'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'request.warning.already.requested.delivered.days',
            'value' => '0',
            'comments' => 'Already requested and delivered'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'request.warning.already.requested.not.delivered.toggle',
            'value' => 'false',
            'comments' => 'Already requested but not yet delivered'
        ]);

        GenericSetting::create([
            'type' => 'int',
            'key' => 'request.warning.already.requested.not.delivered.days',
            'value' => '0',
            'comments' => 'Already requested but not yet delivered'
        ]);

        GenericSetting::create([
            'type' => 'boolean',
            'key' => 'delivery.outbound.warn.if.inventory.reaches.minimum',
            'value' => 'false',
            'comments' => 'Warn if inventory reaches minimum'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'purchase.sheet.number.prefix',
            'value' => 'PO',
            'comments' => 'Sheet Number prefix for Purchase module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'purchase.inbound.sheet.number.prefix',
            'value' => 'PR',
            'comments' => 'Sheet Number prefix for Purchase Inbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'purchase.return.sheet.number.prefix',
            'value' => 'PRT',
            'comments' => 'Sheet Number prefix for Purchase Return module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'purchase.return.outbound.sheet.number.prefix',
            'value' => 'PRO',
            'comments' => 'Sheet Number prefix for Purchase Return Outbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'damage.sheet.number.prefix',
            'value' => 'DMG',
            'comments' => 'Sheet Number prefix for Damage module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'damage.outbound.sheet.number.prefix',
            'value' => 'DMO',
            'comments' => 'Sheet Number prefix for Damage Outbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'sales.sheet.number.prefix',
            'value' => 'SO',
            'comments' => 'Sheet Number prefix for Sales module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'sales.outbound.sheet.number.prefix',
            'value' => 'SD',
            'comments' => 'Sheet Number prefix for Sales Outbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'sales.return.sheet.number.prefix',
            'value' => 'SRT',
            'comments' => 'Sheet Number prefix for Sales Return module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'sales.return.inbound.sheet.number.prefix',
            'value' => 'SRI',
            'comments' => 'Sheet Number prefix for Sales Return Inbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.request.sheet.number.prefix',
            'value' => 'TRQ',
            'comments' => 'Sheet Number prefix for Stock Request module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.delivery.sheet.number.prefix',
            'value' => 'TDL',
            'comments' => 'Sheet Number prefix for Stock Delivery module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.delivery.outbound.sheet.number.prefix',
            'value' => 'TDO',
            'comments' => 'Sheet Number prefix for Stock Delivery Outbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.delivery.inbound.sheet.number.prefix',
            'value' => 'TDI',
            'comments' => 'Sheet Number prefix for Stock Delivery Inbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.return.sheet.number.prefix',
            'value' => 'TRT',
            'comments' => 'Sheet Number prefix for Stock Return module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.return.outbound.sheet.number.prefix',
            'value' => 'TRO',
            'comments' => 'Sheet Number prefix for Stock Return Outbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'stock.return.inbound.sheet.number.prefix',
            'value' => 'TRI',
            'comments' => 'Sheet Number prefix for Stock Return Inbound module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'inventory.adjust.sheet.number.prefix',
            'value' => 'ADJ',
            'comments' => 'Sheet Number prefix for Inventory Adjust module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'payment.sheet.number.prefix',
            'value' => 'PMT',
            'comments' => 'Sheet Number prefix for Payment module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'collection.sheet.number.prefix',
            'value' => 'CLN',
            'comments' => 'Sheet Number prefix for Collection module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'counter.sheet.number.prefix',
            'value' => 'CTR',
            'comments' => 'Sheet Number prefix for Counter module'
        ]);

        GenericSetting::create([
            'type' => 'string',
            'key' => 'product.conversion.sheet.number.prefix',
            'value' => 'PRC',
            'comments' => 'Sheet Number prefix for Product Conversion module'
        ]);
    }
}
