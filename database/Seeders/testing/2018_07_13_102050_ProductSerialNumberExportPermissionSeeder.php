<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductSerialNumberExportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'product')->first();

        $count = Permission::where('module_id', $module->id)->get()->count();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Export Excel Product Serial Number',
            'alias' => 'export_excel_product_serial_number',
            'translation' => 'label.export.serial.number',
            'sequence' => $count + 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'export_excel_product_serial_number')->delete();
    }
}
