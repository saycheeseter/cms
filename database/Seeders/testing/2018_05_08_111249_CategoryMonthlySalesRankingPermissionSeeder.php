<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class CategoryMonthlySalesRankingPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Category Monthly Sales Ranking',
            'alias' => 'category_monthly_sales_ranking',
            'translation' => 'label.category.monthly.sales.ranking',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Category Monthly Sales Ranking',
            'alias' => 'view_category_monthly_sales_ranking',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_category_monthly_sales_ranking')->delete();
        PermissionModule::where('alias', 'category_monthly_sales_ranking')->delete();
    }
}
