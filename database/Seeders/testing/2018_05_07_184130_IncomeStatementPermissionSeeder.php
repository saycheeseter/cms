<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class IncomeStatementPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Income Statement',
            'alias' => 'income_statement',
            'translation' => 'label.income.statement',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Income Statement',
            'alias' => 'view_income_statement',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_income_statement')->delete();
        PermissionModule::where('alias', 'income_statement')->delete();
    }
}
