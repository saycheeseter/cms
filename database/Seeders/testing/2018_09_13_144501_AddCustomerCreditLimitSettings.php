<?php

use Modules\Core\Entities\GenericSetting;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddCustomerCreditLimitSettings extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        GenericSetting::create([
            'key' => 'sales.customer.credit.limit',
            'type' => 'boolean',
            'value' => false,
            'comments' => 'Enable or disable customer credit limit display and warning'
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        GenericSetting::getQuery()
            ->where('key', 'sales.customer.credit.limit')
            ->delete();
    }
}
