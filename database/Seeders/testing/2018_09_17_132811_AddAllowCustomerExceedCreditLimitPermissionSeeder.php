<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddAllowCustomerExceedCreditLimitPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'sales_outbound')->first();

        $count = Permission::where('module_id', $module->id)->get()->count();

        Permission::create([
            'alias' => 'allow_customer_exceed_credit_limit_sales_outbound',
            'module_id' => $module->id,
            'name' => 'Allow customer exceed credit limit in Sales Outbound',
            'translation' => 'label.allow.customer.exceed.credit.limit',
            'sequence' => $count + 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::geteQuery()->where('alias', 'allow_customer_exceed_credit_limit_sales_outbound')->delete();
    }
}
