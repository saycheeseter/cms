<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Entities\TransactionSummaryPosting;
use Modules\Core\Services\Contracts\ProductTransactionSummaryServiceInterface;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\ProductConversion;

class ManualPostingAutomateProductTransactionSummarySeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $postings = TransactionSummaryPosting::all();
        $productTransactionSummaryService = App::make(ProductTransactionSummaryServiceInterface::class);

        foreach ($postings as $posting) {
            $model = $posting->transaction;

            if($model instanceof InventoryAdjust) {
                $productTransactionSummaryService->summarizeInventoryAdjust($model);
            } else if($model instanceof AutoProductConversion) {
                $productTransactionSummaryService->summarizeAutoProductConversion($model);
            } else if($model instanceof ProductConversion) {
                $productTransactionSummaryService->summarizeProductConversion($model);
            } else {
                $productTransactionSummaryService->summarizeTransaction($model);
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        $postings = TransactionSummaryPosting::all();
        $productTransactionSummaryService = App::make(ProductTransactionSummaryServiceInterface::class);

        foreach ($postings as $posting) {
            $model = $posting->transaction;

            if($model instanceof InventoryAdjust) {
                $productTransactionSummaryService->revertInventoryAdjust($model);
            } else if($model instanceof AutoProductConversion) {
                $productTransactionSummaryService->revertAutoProductConversion($model);
            } else if($model instanceof ProductConversion) {
                $productTransactionSummaryService->revertProductConversion($model);
            } else {
                $productTransactionSummaryService->revertTransaction($model);
            }
        }
    }
}
