<?php

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchDetail;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class BranchSeeder extends SmartSeeder
{
    protected function execute()
    {
        $branch = Branch::create([
            'code' => '1',
            'name' => 'Main',
        ]);

        BranchDetail::create([
            'code' => '1',
            'name' => 'Main Office',
            'branch_id' => $branch->id
        ]);
    }

    protected function back()
    {
        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        $location = BranchDetail::where('code', 1)
            ->where('name', 'Main Office')
            ->where('branch_id', $branch->id)
            ->first();

        $location->forceDelete();
        $branch->forceDelete();
    }
}
