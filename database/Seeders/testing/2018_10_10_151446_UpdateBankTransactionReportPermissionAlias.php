<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class UpdateBankTransactionReportPermissionAlias extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'bank_transaction')->first();

        $module->alias = 'bank_transaction_report';
        $module->translation = 'label.bank.transaction.report';
        $module->save();

        $permission = Permission::where('alias', 'view_bank_transaction')->first();
        $permission->name = 'View Bank Transaction Report';
        $permission->alias = 'view_bank_transaction_report';
        $permission->save();
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $module = PermissionModule::where('alias', 'bank_transaction_report')->first();

        $module->alias = 'bank_transaction';
        $module->translation = 'label.bank.transaction';
        $module->save();

        $permission = Permission::where('alias', 'view_bank_transaction_report')->first();
        $permission->name = 'View Bank Transaction';
        $permission->alias = 'view_bank_transaction';
        $permission->save();
    }
}
