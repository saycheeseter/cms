<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SystemCodeImportExcelPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $this->createImportExcelPermissionForBrand();
        $this->createImportExcelPermissionForUOM();
        $this->createImportExcelPermissionForBank();
        $this->createImportExcelPermissionForPaymentMethod();
        $this->createImportExcelPermissionForReason();
        $this->createImportExcelPermissionForPaymentType();
        $this->createImportExcelPermissionForIncomeType();
    }

    protected function createImportExcelPermissionForBrand()
    {
        $module = PermissionModule::where('alias', 'brand')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Brand',
            'alias' => 'import_excel_brand',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForUOM()
    {
        $module = PermissionModule::where('alias', 'uom')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel UOM',
            'alias' => 'import_excel_uom',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForBank()
    {
        $module = PermissionModule::where('alias', 'bank')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Bank',
            'alias' => 'import_excel_bank',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForPaymentMethod()
    {
        $module = PermissionModule::where('alias', 'payment_method')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Payment Method',
            'alias' => 'import_excel_payment_method',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForReason()
    {
        $module = PermissionModule::where('alias', 'reason')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Reason',
            'alias' => 'import_excel_reason',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForPaymentType()
    {
        $module = PermissionModule::where('alias', 'payment_type')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Payment Type',
            'alias' => 'import_excel_payment_type',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function createImportExcelPermissionForIncomeType()
    {
        $module = PermissionModule::where('alias', 'income_type')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Income Type',
            'alias' => 'import_excel_income_type',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function getNextSequenceNumberForModule(PermissionModule $module)
    {
        return PermissionModule::where('id', $module->id)
            ->get()
            ->count() + 1;
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::whereIn('alias', [
            'import_excel_brand',
            'import_excel_uom',
            'import_excel_bank',
            'import_excel_payment_method',
            'import_excel_reason',
            'import_excel_payment_type',
            'import_excel_income_type',
        ])->delete();
    }
}
