<?php

use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class UpdateNullLimitOfBranchModuleSeriesSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $references = BranchModuleSeries::whereNull('limit')->get();

        foreach ($references as $reference) {
            $reference->limit = sprintf("1%s9999999", str_pad($reference->branch_id,3, '0', STR_PAD_LEFT));
            $reference->save();
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        BranchModuleSeries::getQuery()->update(['limit' => null]);
    }
}
