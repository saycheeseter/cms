<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductExportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'product')->first();

        $count = Permission::where('module_id', $module->id)->get()->count();

        Permission::create([
            'alias' => 'export_excel_product',
            'module_id' => $module->id,
            'name' => 'Export Excel Product',
            'translation' => 'label.export.excel',
            'sequence' => $count + 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'export_excel_product')->delete();
    }
}
