<?php

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class FixInventoryAdjustTotalLostGainAmountSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $zero = Decimal::create(0, setting('monetary.precision'));

        $adjustments = InventoryAdjust::with(['details'])->cursor();

        foreach ($adjustments as $adjustment) {
            $lost = Decimal::create(0, setting('monetary.precision'));
            $gain = Decimal::create(0, setting('monetary.precision'));

            foreach ($adjustment->details as $detail) {
                if ($detail->amount->isLessThan($zero)) {
                    $lost = $lost->add($detail->amount->abs(), setting('monetary.precision'));
                } else {
                    $gain = $gain->add($detail->amount->abs(), setting('monetary.precision'));
                }
            }

            $adjustment->total_lost_amount = $lost;
            $adjustment->total_gain_amount = $gain;
            $adjustment->save();
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
