<?php

use Modules\Core\Enums\CustomerPriceSettings;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class DefaultRegularCustomerPriceBasisSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        DB::table('generic_settings')
            ->where('key', 'soi.regular.customers.price.basis')
            ->update(['value' => CustomerPriceSettings::CUSTOM]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {

    }
}
