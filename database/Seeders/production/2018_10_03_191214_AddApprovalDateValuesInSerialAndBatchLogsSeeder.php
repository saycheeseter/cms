<?php

use Modules\Core\Entities\TransactionBatchLog;
use Modules\Core\Entities\TransactionSerialNumberLogs;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddApprovalDateValuesInSerialAndBatchLogsSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $loggers = [
            TransactionSerialNumberLogs::class,
            TransactionBatchLog::class
        ];

        foreach ($loggers as $logger) {
            $logs = app($logger)->cursor();

            foreach ($logs as $log) {
                $log->getQuery()
                    ->where('id', $log->id)
                    ->update(['approval_date' => $log->transaction->audited_date]);
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $loggers = [
            TransactionSerialNumberLogs::class,
            TransactionBatchLog::class
        ];

        foreach ($loggers as $logger) {
            app($logger)->getQuery()
                ->update(['approval_date' => null]);
        }
    }
}
