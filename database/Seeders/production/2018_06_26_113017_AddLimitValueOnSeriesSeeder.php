<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddLimitValueOnSeriesSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $this->addValueInModuleSeriesTable();
        $this->addValueInBranchModuleSeriesTable();
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $this->setLimitToNullInModuleSeriesTable();
        $this->setLimitToNullInBranchModuleSeriesTable();
    }

    private function addValueInModuleSeriesTable()
    {
        $defaults = [
            'product'          => 'stock_no',
            'brand'            => 'code',
            'income_type'      => 'code',
            'payment_type'     => 'code',
            'UOM'              => 'code',
            'bank'             => 'code',
            'branch'           => 'code',
            'branch_detail'    => 'code',
            'category'         => 'code',
            'supplier'         => 'code',
            'customer'         => 'code',
            'member'           => 'card_id',
            'user'             => 'code',
            'role'             => 'code',
            'reason'           => 'code',
        ];

        $builder = DB::table('module_series');

        foreach ($defaults as $type => $column) {
            $builder = $builder->orWhere(function($query) use ($type, $column) {
                $query->where('type', $type)
                    ->where('column', $column);
            });
        }

        $builder->update(['limit' => '999999999999']);

        $series = DB::table('module_series')
            ->where('type', 'product_barcode')
            ->where('column', 'code')
            ->first();

        $length = strlen($series->series);

        $limit = '';

        for ($i = 0; $i < $length; $i++) {
            if ($i < 2) {
                $limit .= $series->series[$i];
            } else {
                $limit .= '9';
            }
        }

        DB::table('module_series')
            ->where('type', 'product_barcode')
            ->where('column', 'code')
            ->update(['limit' => $limit]);
    }

    private function addValueInBranchModuleSeriesTable()
    {
        $defaults = [
            'purchase',
            'purchase_inbound',
            'purchase_return',
            'purchase_return_outbound',
            'damage',
            'damage_outbound',
            'sales',
            'sales_outbound',
            'sales_return',
            'sales_return_inbound',
            'stock_request',
            'stock_delivery',
            'stock_delivery_outbound',
            'stock_delivery_inbound',
            'stock_return',
            'stock_return_outbound',
            'stock_return_inbound',
            'inventory_adjust',
            'product_conversion',
            'payment',
            'collection',
            'counter',
        ];

        $data = DB::table('branch_module_series')
            ->whereIn('type', $defaults)
            ->where('column', 'sheet_number')
            ->get();

        foreach ($data as $row) {
            $length = strlen($row->series);

            $limit = '';

            for ($i = 0; $i < $length; $i++) {
                if ($i < 4) {
                    $limit .= $row->series[$i];
                } else {
                    $limit .= '9';
                }
            }

            DB::table('branch_module_series')
                ->where('id', $row->id)
                ->update(['limit' => $limit]);
        }
    }

    private function setLimitToNullInModuleSeriesTable()
    {
        $defaults = [
            'product'          => 'stock_no',
            'brand'            => 'code',
            'income_type'      => 'code',
            'payment_type'     => 'code',
            'UOM'              => 'code',
            'bank'             => 'code',
            'branch'           => 'code',
            'branch_detail'    => 'code',
            'category'         => 'code',
            'supplier'         => 'code',
            'customer'         => 'code',
            'member'           => 'card_id',
            'user'             => 'code',
            'role'             => 'code',
            'reason'           => 'code',
            'product_barcode'  => 'code'
        ];

        $builder = DB::table('module_series');

        foreach ($defaults as $type => $column) {
            $builder = $builder->orWhere(function($query) use ($type, $column) {
                $query->where('type', $type)
                    ->where('column', $column);
            });
        }

        $builder->update(['limit' => null]);
    }

    private function setLimitToNullInBranchModuleSeriesTable()
    {
        $defaults = [
            'purchase',
            'purchase_inbound',
            'purchase_return',
            'purchase_return_outbound',
            'damage',
            'damage_outbound',
            'sales',
            'sales_outbound',
            'sales_return',
            'sales_return_inbound',
            'stock_request',
            'stock_delivery',
            'stock_delivery_outbound',
            'stock_delivery_inbound',
            'stock_return',
            'stock_return_outbound',
            'stock_return_inbound',
            'inventory_adjust',
            'product_conversion',
            'payment',
            'collection',
            'counter',
        ];

        $data = DB::table('branch_module_series')
            ->whereIn('type', $defaults)
            ->where('column', 'sheet_number')
            ->update(['limit' => null]);
    }
}
