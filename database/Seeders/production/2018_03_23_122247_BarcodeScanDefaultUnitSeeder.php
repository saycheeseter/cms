<?php

use Modules\Core\Entities\GenericSetting;
use Modules\Core\Enums\BarcodeScanUnit;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class BarcodeScanDefaultUnitSeeder extends SmartSeeder
{
    protected function execute()
    {
        GenericSetting::create([
            'type' => 'int',
            'key' => 'barcode.scan.default.unit',
            'value' => BarcodeScanUnit::DEFAULT_UNIT,
            'comments' => 'Default Unit of the scanned product'
        ]);
    }

    protected function back()
    {
        GenericSetting::where('key', 'barcode.scan.default.unit')->delete();
    }
}
