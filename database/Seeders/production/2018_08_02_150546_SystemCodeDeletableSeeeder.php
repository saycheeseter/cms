<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SystemCodeDeletableSeeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $defaultSystemCodes = [
            'Piece',
            'Cash',
            'Check',
            'Bank Deposit',
            'From Balance',
            'Credit Card',
            'Debit Card',
            'Gift Check',
            'Member Points',
            'Eplus',
            'Smac',
            'Online Deal',
            'Coupon',
            'GC Excess',
            'GCash',
            'Paymaya',
            'AliPay',
            'WeChat Pay',
            'None',
        ];

        DB::table('system_code')->whereIn('name', $defaultSystemCodes)->update(['deletable' => 0]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $defaultSystemCodes = [
            'Piece',
            'Cash',
            'Check',
            'Bank Deposit',
            'From Balance',
            'Credit Card',
            'Debit Card',
            'Gift Check',
            'Member Points',
            'Eplus',
            'Smac',
            'Online Deal',
            'Coupon',
            'GC Excess',
            'GCash',
            'Paymaya',
            'AliPay',
            'WeChat Pay',
            'None',
        ];

        DB::table('system_code')->whereIn('name', $defaultSystemCodes)->update(['deletable' => 1]);
    }
}
