<?php

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Services\Contracts\BranchModuleSeriesServiceInterface;

class BranchModuleSeriesSeeder extends SmartSeeder
{
    protected function execute()
    {
        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        App::make(BranchModuleSeriesServiceInterface::class)->createInitialData($branch);
    }

    protected function back()
    {
        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        BranchModuleSeries::where('branch_id', $branch->id)->delete();
    }
}
