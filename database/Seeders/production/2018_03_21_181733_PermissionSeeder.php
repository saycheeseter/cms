<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class PermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    protected function execute()
    {
        $sections = $this->createSections();

        $this->createCredentialsPermission($sections['general'], 1);
        $this->createProductPermission($sections['data'], 1);
        $this->createBrandPermission($sections['data'], 2);
        $this->createBranchPermission($sections['data'], 3);
        $this->createCategoryPermission($sections['data'], 4);
        $this->createSupplierPermission($sections['data'], 5);
        $this->createCustomerPermission($sections['data'], 6);
        $this->createMemberPermission($sections['data'], 7);
        $this->createMemberRatePermission($sections['data'], 8);
        $this->createUserPermission($sections['data'], 9);
        $this->createRolePermission($sections['data'], 10);
        $this->createBankPermission($sections['data'], 11);
        $this->createBankAccountPermission($sections['data'], 12);
        $this->createUOMPermission($sections['data'], 13);
        $this->createReasonPermission($sections['data'], 14);
        $this->createPaymentMethodPermission($sections['data'], 15);
        $this->createPaymentTypePermission($sections['data'], 16);
        $this->createIncomeTypePermission($sections['data'], 17);

        $this->createPurchaseOrderPermission($sections['poi'], 1);
        $this->createPurchaseInboundPermission($sections['poi'], 2);
        $this->createPurchaseReturnPermission($sections['poi'], 3);
        $this->createPurchaseReturnOutboundPermission($sections['poi'], 4);
        $this->createPaymentPermission($sections['poi'], 5);
        $this->createOtherPaymentPermission($sections['poi'], 6);
        $this->createIssuedCheckPermission($sections['poi'], 7);
        $this->createDamagePermission($sections['poi'], 8);
        $this->createDamageOutboundPermission($sections['poi'], 9);

        $this->createSalesPermission($sections['soi'], 1);
        $this->createSalesOutboundPermission($sections['soi'], 2);
        $this->createSalesReturnPermission($sections['soi'], 3);
        $this->createSalesReturnInboundPermission($sections['soi'], 4);
        $this->createCounterPermission($sections['soi'], 5);
        $this->createCollectionPermission($sections['soi'], 6);
        $this->createOtherIncomePermission($sections['soi'], 7);
        $this->createReceivedCheckPermission($sections['soi'], 8);

        $this->createStockRequestToPermission($sections['stock_transferring'], 1);
        $this->createStockRequestFromPermission($sections['stock_transferring'], 2);
        $this->createStockDeliveryPermission($sections['stock_transferring'], 3);
        $this->createStockDeliveryOutboundPermission($sections['stock_transferring'], 4);
        $this->createStockDeliveryOutboundFromPermission($sections['stock_transferring'], 5);
        $this->createStockDeliveryInboundPermission($sections['stock_transferring'], 6);
        $this->createStockReturnPermission($sections['stock_transferring'], 7);
        $this->createStockReturnOutboundPermission($sections['stock_transferring'], 8);
        $this->createStockReturnInboundPermission($sections['stock_transferring'], 9);

        $this->createInventoryAdjustPermission($sections['others'], 1);
        $this->createGiftCheckPermission($sections['others'], 2);
        $this->createPrintoutTemplatePermission($sections['others'], 3);
        $this->createExcelTemplatePermission($sections['others'], 4);
        $this->createProductConversionPermission($sections['others'], 5);
        $this->createReportBuilderPermission($sections['others'], 6);
        $this->createProductDiscountPermission($sections['others'], 7);

        $this->createCustomerProductPriceReportPermission($sections['reports'], 1);
        $this->createCustomerBalanceReportPermission($sections['reports'], 2);
        $this->createSupplierProductPriceReportPermission($sections['reports'], 3);
        $this->createSupplierBalanceReportPermission($sections['reports'], 4);
        $this->createAuditTrailPermission($sections['reports'], 5);
        $this->createInventoryValueReportPermission($sections['reports'], 6);

        $this->createPosPermission($sections['pos'], 1);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::getQuery()->delete();
        PermissionModule::getQuery()->delete();
        PermissionSection::getQuery()->delete();
    }

    protected function createSections()
    {
        $general = PermissionSection::create([
            'name' => 'General',
            'alias' => 'general',
            'translation' => 'label.general',
            'sequence_number' => 1
        ]);

        $pos = PermissionSection::create([
            'name' => 'POS',
            'alias' => 'pos',
            'translation' => 'label.pos',
            'sequence_number' => 2
        ]);

        $data = PermissionSection::create([
            'name' => 'Data',
            'alias' => 'data',
            'translation' => 'label.data',
            'sequence_number' => 3
        ]);

        $poi = PermissionSection::create([
            'name' => 'Purchase and Other Income',
            'alias' => 'purchase_and_other_income',
            'translation' => 'label.poi',
            'sequence_number' => 4
        ]);

        $soi = PermissionSection::create([
            'name' => 'Sales and Other Income',
            'alias' => 'sales_and_other_income',
            'translation' => 'label.soi',
            'sequence_number' => 5
        ]);

        $stockTransferring = PermissionSection::create([
            'name' => 'Stock Transferring',
            'alias' => 'stock_transferring',
            'translation' => 'label.stock.transferring',
            'sequence_number' => 6
        ]);

        $others = PermissionSection::create([
            'name' => 'Others',
            'alias' => 'others',
            'translation' => 'label.others',
            'sequence_number' => 7
        ]);

        $reports = PermissionSection::create([
            'name' => 'Reports',
            'alias' => 'reports',
            'translation' => 'label.reports',
            'sequence_number' => 8
        ]);

        return [
            'general'            => $general->id,
            'pos'                => $pos->id,
            'data'               => $data->id,
            'poi'                => $poi->id,
            'soi'                => $soi->id,
            'stock_transferring' => $stockTransferring->id,
            'others'             => $others->id,
            'reports'            => $reports->id
        ];
    }

    protected function createCredentialsPermission($sections, $sequence)
    {
        $credentials = PermissionModule::create([
            'section_id' => $sections,
            'name' => 'Credentials',
            'alias' => 'credentials',
            'translation' => 'label.credentials',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $credentials->id,
            'name' => 'Login',
            'alias' => 'login',
            'translation' => 'label.login',
            'sequence' => 1
        ]);
    }

    protected function createBranchPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Branch',
            'alias' => 'branch',
            'translation' => 'label.branch',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'branch');
    }

    protected function createCategoryPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Category',
            'alias' => 'category',
            'translation' => 'label.category',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'category');
    }

    protected function createSupplierPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Supplier',
            'alias' => 'supplier',
            'translation' => 'label.supplier',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'supplier');
    }

    protected function createCustomerPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Customer',
            'alias' => 'customer',
            'translation' => 'label.customer',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'customer');
    }

    protected function createMemberPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Member',
            'alias' => 'member',
            'translation' => 'label.member',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'member');
    }

    protected function createMemberRatePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Member Rate',
            'alias' => 'member_rate',
            'translation' => 'label.member.rate',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'member_rate');
    }

    protected function createBankAccountPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Bank Account',
            'alias' => 'bank_account',
            'translation' => 'label.bank.account',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'bank_account');
    }

    protected function createBrandPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Brand',
            'alias' => 'brand',
            'translation' => 'label.brand',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'brand');
    }

    protected function createPaymentTypePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Payment Type',
            'alias' => 'payment_type',
            'translation' => 'label.payment.type.label',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'payment_type');
    }

    protected function createIncomeTypePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Income Type',
            'alias' => 'income_type',
            'translation' => 'label.income.type',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'income_type');
    }

    protected function createBankPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Bank',
            'alias' => 'bank',
            'translation' => 'label.bank',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'bank');
    }

    protected function createReasonPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Reason',
            'alias' => 'reason',
            'translation' => 'label.reason',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'reason');
    }

    protected function createPaymentMethodPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Payment Method',
            'alias' => 'payment_method',
            'translation' => 'label.payment.method',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'payment_method');
    }

    protected function createUOMPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Unit of Measurement',
            'alias' => 'uom',
            'translation' => 'label.uom',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'uom');
    }

    protected function createProductPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Product',
            'alias' => 'product',
            'translation' => 'label.product',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'product');
        $this->createShowCostPermissions('product');
        $this->createColumnSettingsPermissions('product');
    }

    protected function createUserPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'User',
            'alias' => 'user',
            'translation' => 'label.user',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'user');
    }

    protected function createPurchaseOrderPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Purchase Order',
            'alias' => 'purchase_order',
            'translation' => 'label.purchase.order',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'purchase_order');
    }

    protected function createPurchaseInboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Purchase Inbound',
            'alias' => 'purchase_inbound',
            'translation' => 'label.purchase.inbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'purchase_inbound');
    }

    protected function createPurchaseReturnPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Purchase Return',
            'alias' => 'purchase_return',
            'translation' => 'label.purchase.return',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'purchase_return');
    }

    protected function createPurchaseReturnOutboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Purchase Return Outbound',
            'alias' => 'purchase_return_outbound',
            'translation' => 'label.purchase.return.outbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'purchase_return_outbound');
    }

    protected function createDamagePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Damage',
            'alias' => 'damage',
            'translation' => 'label.damage',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'damage');
    }

    protected function createDamageOutboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Damage Outbound',
            'alias' => 'damage_outbound',
            'translation' => 'label.damage.outbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'damage_outbound');
    }

    protected function createSalesPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Sales',
            'alias' => 'sales',
            'translation' => 'label.sales',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'sales');
    }

    protected function createSalesOutboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Sales Outbound',
            'alias' => 'sales_outbound',
            'translation' => 'label.sales.outbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'sales_outbound');
    }

    protected function createSalesReturnPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Sales Return',
            'alias' => 'sales_return',
            'translation' => 'label.sales.return',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'sales_return');
    }

    protected function createSalesReturnInboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Sales Return Inbound',
            'alias' => 'sales_return_inbound',
            'translation' => 'label.sales.return.inbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'sales_return_inbound');
    }

    protected function createStockRequestToPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Request (To Other Branches)',
            'alias' => 'stock_request_to',
            'translation' => 'label.stock.requests.to.other.branches',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_request_to');
    }

    protected function createStockRequestFromPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Request (From Other Branches)',
            'alias' => 'stock_request_from',
            'translation' => 'label.stock.requests.from.other.branches',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Stock Request From List',
            'alias' => 'view_stock_request_from',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Stock Request Detail',
            'alias' => 'view_stock_request_from_detail',
            'translation' => 'label.view.detail',
            'sequence' => 2
        ]);

        $this->createExportExcelPermissions('stock_request_from');
        $this->createPrintoutPermissions('stock_request_from');
        $this->createColumnSettingsPermissions('stock_request_from');
    }

    protected function createStockDeliveryPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Delivery',
            'alias' => 'stock_delivery',
            'translation' => 'label.stock.delivery',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_delivery');
    }

    protected function createStockDeliveryOutboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Delivery Outbound',
            'alias' => 'stock_delivery_outbound',
            'translation' => 'label.stock.delivery.outbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_delivery_outbound');
    }

    protected function createStockDeliveryOutboundFromPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Delivery Outbound (From Other Branches)',
            'alias' => 'stock_delivery_outbound_from',
            'translation' => 'label.stock.delivery.outbound.from.other.branches',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Stock Delivery Outbound From List',
            'alias' => 'view_stock_delivery_outbound_from',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Stock Delivery Outbound From Detail',
            'alias' => 'view_stock_delivery_outbound_from_detail',
            'translation' => 'label.view.detail',
            'sequence' => 2
        ]);

        $this->createExportExcelPermissions('stock_delivery_outbound_from');
        $this->createPrintoutPermissions('stock_delivery_outbound_from');
        $this->createColumnSettingsPermissions('stock_delivery_outbound_from');
    }

    protected function createStockDeliveryInboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Delivery Inbound',
            'alias' => 'stock_delivery_inbound',
            'translation' => 'label.stock.delivery.inbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_delivery_inbound');
    }

    protected function createStockReturnPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Return',
            'alias' => 'stock_return',
            'translation' => 'label.stock.return',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_return');
    }

    protected function createStockReturnOutboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Return Outbound',
            'alias' => 'stock_return_outbound',
            'translation' => 'label.stock.return.outbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_return_outbound');
    }

    protected function createStockReturnInboundPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Stock Return Inbound',
            'alias' => 'stock_return_inbound',
            'translation' => 'label.stock.return.inbound',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'stock_return_inbound');
    }

    protected function createInventoryAdjustPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Inventory Adjust',
            'alias' => 'inventory_adjust',
            'translation' => 'label.inventory.adjust',
            'sequence_number' => $sequence
        ]);

        $this->createInventoryFlowPermission($module, 'inventory_adjust');
    }

    protected function createPrintoutTemplatePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Printout Template',
            'alias' => 'printout_template',
            'translation' => 'label.printout.template',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'printout_template');
    }

    protected function createExcelTemplatePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Excel Template',
            'alias' => 'excel_template',
            'translation' => 'label.excel.template',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'excel_template');
    }

    protected function createReportBuilderPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Custom Report Builder',
            'alias' => 'report_builder',
            'translation' => 'label.custom.report.builder',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'report_builder');
    }

    protected function createGiftCheckPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Gift Check',
            'alias' => 'gift_check',
            'translation' => 'label.gift.check',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module, 'gift_check');
    }

    protected function createPaymentPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Payment',
            'alias' => 'payment',
            'translation' => 'label.payment',
            'sequence_number' => $sequence
        ]);

        $this->createTransactionPermission($module, 'payment');
        $this->createColumnSettingsPermissions('payment');
    }

    protected function createOtherPaymentPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Other Payment',
            'alias' => 'other_payment',
            'translation' => 'label.other.payment',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'other_payment');
    }

    protected function createIssuedCheckPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Issued Check',
            'alias' => 'issued_check',
            'translation' => 'label.issued.check',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Issued Check List',
            'alias' => 'view_issued_check',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Transfer Issued Check',
            'alias' => 'transfer_issued_check',
            'translation' => 'label.transfer.check',
            'sequence' => 2
        ]);
    }

    protected function createReceivedCheckPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Received Check',
            'alias' => 'received_check',
            'translation' => 'label.received.check',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Received Check List',
            'alias' => 'view_received_check',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Transfer Received Check',
            'alias' => 'transfer_received_check',
            'translation' => 'label.transfer.check',
            'sequence' => 2
        ]);

        $this->createColumnSettingsPermissions('received_check');
    }

    protected function createCollectionPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Collection',
            'alias' => 'collection',
            'translation' => 'label.collection',
            'sequence_number' => $sequence
        ]);

        $this->createTransactionPermission($module, 'collection');
        $this->createColumnSettingsPermissions('collection');
    }

    protected function createOtherIncomePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Other Income',
            'alias' => 'other_income',
            'translation' => 'label.other.income',
            'sequence_number' => $sequence
        ]);

        $this->createSimpleCrudPermission($module,'other_income');
    }

    protected function createCounterPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Counter',
            'alias' => 'counter',
            'translation' => 'label.counter',
            'sequence_number' => $sequence
        ]);

        $this->createTransactionWithExportPrintColumnPermission($module, 'counter');
    }

    protected function createProductDiscountPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Product Discount',
            'alias' => 'product_discount',
            'translation' => 'label.product.discount',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'product_discount');
        $this->createColumnSettingsPermissions('product_discount');
    }

    protected function createRolePermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Role',
            'alias' => 'role',
            'translation' => 'label.role',
            'sequence_number' => $sequence
        ]);

        $this->createAdvanceCrudPermission($module, 'role');
    }

    protected function createProductConversionPermission($id, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $id,
            'name' => 'Product Conversion',
            'alias' => 'product_conversion',
            'translation' => 'label.product.conversion',
            'sequence_number' => $sequence
        ]);

        $this->createTransactionPermission($module, 'product_conversion');
    }

    protected function createCustomerProductPriceReportPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Customer Product Price',
            'alias' => 'customer_product_price',
            'translation' => 'label.customer.product.price',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Customer Product Price',
            'alias' => 'view_customer_product_price',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createCustomerBalanceReportPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Customer Balance',
            'alias' => 'customer_balance',
            'translation' => 'label.customer.balance',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Customer Balance',
            'alias' => 'view_customer_balance',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createSupplierProductPriceReportPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Supplier Product Price',
            'alias' => 'supplier_product_price',
            'translation' => 'label.supplier.product.price',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Supplier Product Price',
            'alias' => 'view_supplier_product_price',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createSupplierBalanceReportPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Supplier Balance',
            'alias' => 'supplier_balance',
            'translation' => 'label.supplier.balance',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Supplier Balance',
            'alias' => 'view_supplier_balance',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createAuditTrailPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Audit Trail',
            'alias' => 'audit_trail',
            'translation' => 'label.audit.trail',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Audit Trail',
            'alias' => 'view_audit_trail',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createInventoryValueReportPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'Product Inventory Value',
            'alias' => 'product_inventory_value',
            'translation' => 'label.product.inventory.value',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Product Inventory Value',
            'alias' => 'view_product_inventory_value',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    protected function createPosPermission($reports, $sequence)
    {
        $module = PermissionModule::create([
            'section_id' => $reports,
            'name' => 'POS',
            'alias' => 'pos',
            'translation' => 'label.pos',
            'sequence_number' => $sequence
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Login',
            'alias' => 'pos_login',
            'translation' => 'label.login',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Re-Print OR',
            'alias' => 'pos_reprint_or',
            'translation' => 'label.reprint.or',
            'sequence' => 2
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Allow Manual Barcode',
            'alias' => 'pos_allow_manual_barcode',
            'translation' => 'label.allow.manual.barcode',
            'sequence' => 3
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Void Transaction',
            'alias' => 'pos_void_transaction',
            'translation' => 'label.void.transaction',
            'sequence' => 4
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Allow Wholesale',
            'alias' => 'pos_allow_wholesale',
            'translation' => 'label.allow.wholesale',
            'sequence' => 5
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'POSD Re-print',
            'alias' => 'pos_posd_reprint',
            'translation' => 'label.posd.reprint',
            'sequence' => 6
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Cancel Product',
            'alias' => 'pos_cancel_product',
            'translation' => 'label.cancel.product',
            'sequence' => 7
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Discount',
            'alias' => 'pos_discount',
            'translation' => 'label.discount',
            'sequence' => 8
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'POSD XZ Print',
            'alias' => 'pos_posd_xz_print',
            'translation' => 'label.posd.xz.print',
            'sequence' => 9
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Sales Return',
            'alias' => 'pos_sales_return',
            'translation' => 'label.sales.return',
            'sequence' => 10
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Zero-Rated',
            'alias' => 'pos_zero_rated',
            'translation' => 'label.zero.rated',
            'sequence' => 11
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Force Refund',
            'alias' => 'pos_force_refund',
            'translation' => 'label.force.refund',
            'sequence' => 12
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Open Drawer',
            'alias' => 'pos_open_drawer',
            'translation' => 'label.open.drawer',
            'sequence' => 13
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Allow Senior Discount',
            'alias' => 'pos_allow_senior_discount',
            'translation' => 'label.allow.senior.discount',
            'sequence' => 14
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'XZ Print',
            'alias' => 'pos_xz_print',
            'translation' => 'label.xz.print',
            'sequence' => 15
        ]);
    }

    protected function createSimpleCrudPermission($module, $alias)
    {
        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('View %s List', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('view_%s', $alias),
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Create %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('create_%s', $alias),
            'translation' => 'label.create',
            'sequence' => 2
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Update %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('update_%s', $alias),
            'translation' => 'label.update',
            'sequence' => 3
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Delete %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('delete_%s', $alias),
            'translation' => 'label.delete',
            'sequence' => 4
        ]);
    }

    protected function createAdvanceCrudPermission($module, $alias)
    {
        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('View %s List', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('view_%s', $alias),
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('View %s Detail', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('view_%s_detail', $alias),
            'translation' => 'label.view.detail',
            'sequence' => 2
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Create %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('create_%s', $alias),
            'translation' => 'label.create',
            'sequence' => 3
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Update %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('update_%s', $alias),
            'translation' => 'label.update',
            'sequence' => 4
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Delete %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('delete_%s', $alias),
            'translation' => 'label.delete',
            'sequence' => 5
        ]);
    }

    protected function createTransactionPermission($module, $alias)
    {
        $this->createAdvanceCrudPermission($module, $alias);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Approve %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('approve_%s', $alias),
            'translation' => 'label.approve',
            'sequence' => 6
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Decline %s', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('decline_%s', $alias),
            'translation' => 'label.decline',
            'sequence' => 7
        ]);
    }

    protected function createInventoryFlowPermission($module, $alias)
    {
        $this->createTransactionPermission($module, $alias);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Export %s List', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('export_%s', $alias),
            'translation' => 'label.export.excel',
            'sequence' => 8
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Print %s Detail', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('print_%s_detail', $alias),
            'translation' => 'label.print',
            'sequence' => 9
        ]);

        $this->createColumnSettingsPermissions($alias);
        $this->createShowPriceAmountPermissions($alias);
    }

    protected function createTransactionWithExportPrintColumnPermission($module, $alias)
    {
        $this->createTransactionPermission($module, $alias);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Export %s List', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('export_%s', $alias),
            'translation' => 'label.export.excel',
            'sequence' => 8
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => sprintf('Print %s Detail', ucwords(str_replace('_', ' ', $alias))),
            'alias' => sprintf('print_%s_detail', $alias),
            'translation' => 'label.print',
            'sequence' => 9
        ]);

        $this->createColumnSettingsPermissions($alias);
    }

    protected function createShowCostPermissions($aliases)
    {
        $this->autoCreatePermission($aliases, function($module, $alias, $count) {
            Permission::create([
                'module_id' => $module->id,
                'name' => sprintf('Show Cost %s', ucwords(str_replace('_', ' ', $alias))),
                'alias' => sprintf('show_cost_%s', $alias),
                'translation' => 'label.show.cost',
                'sequence' => $count + 1
            ]);
        });
    }

    protected function createShowPriceAmountPermissions($aliases)
    {
        $this->autoCreatePermission($aliases, function($module, $alias, $count) {
            Permission::create([
                'module_id' => $module->id,
                'name' => sprintf('Show Price and Amount %s', ucwords(str_replace('_', ' ', $alias))),
                'alias' => sprintf('show_price_amount_%s', $alias),
                'translation' => 'label.show.price.and.amount',
                'sequence' => $count + 1
            ]);
        });
    }

    protected function createColumnSettingsPermissions($aliases)
    {
        $this->autoCreatePermission($aliases, function($module, $alias, $count) {
            Permission::create([
                'module_id' => $module->id,
                'name' => sprintf('Column Settings %s', ucwords(str_replace('_', ' ', $alias))),
                'alias' => sprintf('column_settings_%s', $alias),
                'translation' => 'label.column.settings',
                'sequence' => $count + 1
            ]);
        });
    }

    protected function createExportExcelPermissions($aliases)
    {
        $this->autoCreatePermission($aliases, function($module, $alias, $count) {
            Permission::create([
                'module_id' => $module->id,
                'name' => sprintf('Export %s List', ucwords(str_replace('_', ' ', $alias))),
                'alias' => sprintf('export_%s', $alias),
                'translation' => 'label.export.excel',
                'sequence' => $count + 1
            ]);
        });
    }

    protected function createPrintoutPermissions($aliases)
    {
        $this->autoCreatePermission($aliases, function($module, $alias, $count) {
            Permission::create([
                'module_id' => $module->id,
                'name' => sprintf('Print %s Detail', ucwords(str_replace('_', ' ', $alias))),
                'alias' => sprintf('print_%s_detail', $alias),
                'translation' => 'label.print',
                'sequence' => $count + 1
            ]);
        });
    }

    protected function autoCreatePermission($aliases, $hook)
    {
        $aliases = array_wrap($aliases);

        foreach ($aliases as $key => $alias) {
            $module = PermissionModule::where('alias', $alias)
                ->get()
                ->first();

            $count = Permission::where('module_id', $module->id)
                ->get()
                ->count();

            $hook($module, $alias, $count);
        }
    }
}
