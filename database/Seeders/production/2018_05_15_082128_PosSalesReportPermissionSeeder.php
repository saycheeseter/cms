<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class PosSalesReportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)->get()->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Pos Sales Report',
            'alias' => 'pos_sales_report',
            'translation' => 'label.pos.sales.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Pos Sales Report',
            'alias' => 'view_pos_sales_report',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_pos_sales_report')->delete();
        PermissionModule::where('alias', 'pos_sales_report')->delete();
    }
}
