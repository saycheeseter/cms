<?php

use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\TransactionStatus;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AlterAuditedDateForStockDeliveryAndReturnOutboundSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $modules = [
            StockDeliveryOutbound::class,
            StockReturnOutbound::class
        ];

        foreach ($modules as $module) {
            $transactions = StockDeliveryOutbound::where('approval_status', ApprovalStatus::APPROVED)
                ->where('transaction_status', '<>', TransactionStatus::NO_DELIVERY)
                ->cursor();

            foreach ($transactions as $transaction) {
                $transaction->getQuery()
                    ->where('id', $transaction->id)
                    ->update(['audited_date' => $transaction->transaction_date->addMinutes(2)]);
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
