<?php

use Modules\Core\Entities\Brand;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Carbon\Carbon;

class BrandSeeder extends SmartSeeder
{
    protected function execute()
    {
        $now = Carbon::now();

        $brand = Brand::make([
            'code' => '0',
            'name' => 'None'
        ]);

        $brand->created_at = $now;
        $brand->updated_at = $now;
        $brand->save();
    }

    protected function back()
    {
        $brand = Brand::where('code', 0)
            ->where('name', 'None')
            ->first();

        $brand->forceDelete();
    }
}
