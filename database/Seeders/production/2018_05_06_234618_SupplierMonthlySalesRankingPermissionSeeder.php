<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SupplierMonthlySalesRankingPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Supplier Monthly Sales Ranking',
            'alias' => 'supplier_monthly_sales_ranking',
            'translation' => 'label.supplier.monthly.sales.ranking',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Supplier Monthly Sales Ranking',
            'alias' => 'view_supplier_monthly_sales_ranking',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_supplier_monthly_sales_ranking')->delete();
        PermissionModule::where('alias', 'supplier_monthly_sales_ranking')->delete();
    }
}
