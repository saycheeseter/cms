<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Entities\Branch;

class DataCollectorBranchModuleSeriesSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $branches = Branch::all();

        foreach($branches as $index => $row) {
            $moduleSeries = '1'.str_pad((string)$row['id'], 3, '0', STR_PAD_LEFT).'0000001';
            
            BranchModuleSeries::create([
                'branch_id' => $row['id'],
                'type' => 'data_collector_transaction',
                'column' => 'reference_no',
                'series' => $moduleSeries,
                'limit' => '9999999'
            ]);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        BranchModuleSeries::where('type', 'data_collector_transaction')->delete();
    }
}
