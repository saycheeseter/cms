<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class PaymentReferenceReferenceTypeSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        DB::table('payment_reference')
            ->update([
                'reference_type' => 'purchase_inbound'
            ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        DB::table('payment_reference')
            ->where('reference_type', 'purchase_inbound')
            ->update([
                'reference_type' => 'NULL'
            ]);
    }
}
