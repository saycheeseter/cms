<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddMissingGiftCheckViewDetailPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $permission = Permission::where('alias', 'create_gift_check')->first();
        $permission->sequence = 3;
        $permission->save();

        $permission = Permission::where('alias', 'update_gift_check')->first();
        $permission->sequence = 4;
        $permission->save();

        $permission = Permission::where('alias', 'delete_gift_check')->first();
        $permission->sequence = 5;
        $permission->save();

        Permission::create([
            'alias'       => 'view_gift_check_detail',
            'module_id'   => $permission->module_id,
            'name'        => 'View Gift Check Detail',
            'translation' => 'label.view.detail',
            'sequence'    => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_gift_check_detail')->delete();

        $permission = Permission::where('alias', 'create_gift_check')->first();
        $permission->sequence = 2;
        $permission->save();

        $permission = Permission::where('alias', 'update_gift_check')->first();
        $permission->sequence = 3;
        $permission->save();

        $permission = Permission::where('alias', 'delete_gift_check')->first();
        $permission->sequence = 4;
        $permission->save();
    }
}
