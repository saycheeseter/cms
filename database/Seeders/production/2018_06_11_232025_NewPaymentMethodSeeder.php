<?php

use Carbon\Carbon;
use Modules\Core\Entities\PaymentMethod;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class NewPaymentMethodSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $connection = config('database.default');
        $driver = config('database.connections.'.$connection.'.driver');

        switch ($driver) {
            case 'sqlsrv':
                DB::statement("
                    SET IDENTITY_INSERT system_code ON;
                    INSERT INTO system_code(id, type_id, code, name, remarks, created_at, updated_at) VALUES
                    (1001, 4, '13', 'GC Excess', NULL, GETDATE(), GETDATE()),
                    (1002, 4, '14', 'GCash', 'Mobile Payments', GETDATE(), GETDATE()),
                    (1003, 4, '15', 'Paymaya', 'Mobile Payments', GETDATE(), GETDATE()),
                    (1004, 4, '16', 'AliPay', 'Mobile Payments', GETDATE(), GETDATE()),
                    (1005, 4, '17', 'WeChat Pay', 'Mobile Payments', GETDATE(), GETDATE());
                    SET IDENTITY_INSERT system_code OFF;
                ");
                break;

            default:
                $date = Carbon::now();

                $data = [
                    [
                        'id' => 1001,
                        'type_id' => 4,
                        'code' => '13',
                        'name' => 'GC Excess',
                        'remarks' => null,
                        'created_at' => $date,
                        'updated_at' => $date,
                    ],
                    [
                        'id' => 1002,
                        'type_id' => 4,
                        'code' => '14',
                        'name' => 'GCash',
                        'remarks' => 'Mobile Payments',
                        'created_at' => $date,
                        'updated_at' => $date,
                    ],
                    [
                        'id' => 1003,
                        'type_id' => 4,
                        'code' => '15',
                        'name' => 'Paymaya',
                        'remarks' => 'Mobile Payments',
                        'created_at' => $date,
                        'updated_at' => $date,
                    ],
                    [
                        'id' => 1004,
                        'type_id' => 4,
                        'code' => '16',
                        'name' => 'AliPay',
                        'remarks' => 'Mobile Payments',
                        'created_at' => $date,
                        'updated_at' => $date,
                    ],
                    [
                        'id' => 1005,
                        'type_id' => 4,
                        'code' => '17',
                        'name' => 'WeChatPay',
                        'remarks' => 'Mobile Payments',
                        'created_at' => $date,
                        'updated_at' => $date,
                    ],
                ];

                DB::table('system_code')->insert($data);
                break;
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        PaymentMethod::whereIn('id', [1001, 1002, 1003, 1004, 1005])->forceDelete();
    }
}
