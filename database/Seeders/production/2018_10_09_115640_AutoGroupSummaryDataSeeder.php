<?php

use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\PosSales;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Entities\ProductStockCard;
use Modules\Core\Entities\ProductTransactionSummary;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\TransactionSummary;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Modules\Core\Services\Contracts\ProductBranchSummaryServiceInterface;

class AutoGroupSummaryDataSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $conversions = AutoProductConversion::orderBy('created_at', 'ASC')->cursor();

        foreach ($conversions as $conversion) {
            $this->createProductTransactionSummary($conversion);
            $this->createProductMonthlyTransactionSummary($conversion);
            $this->createStockCard($conversion);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $conversions = AutoProductConversion::all();

        $ids = $conversions->pluck('product_id');

        DB::table('product_transaction_summary')->whereIn('product_id', $ids)->delete();
        DB::table('product_monthly_transactions')
            ->whereIn('product_id', $ids)
            ->update([
                'auto_product_conversion_increase_total_qty' => 0,
                'auto_product_conversion_increase_total_cost' => 0,
                'auto_product_conversion_increase_total_ma_cost' => 0,
                'auto_product_conversion_decrease_total_qty' => 0,
                'auto_product_conversion_decrease_total_cost' => 0,
                'auto_product_conversion_decrease_total_ma_cost' => 0,
            ]);

        DB::table('product_stock_card')->whereIn('product_id', $ids)->delete();
    }

    private function createProductTransactionSummary($model)
    {
        $identifiers = [
            'created_for'      => $model->created_for,
            'for_location'     => $model->for_location,
            'transaction_date' => $model->transaction_date->toDateTimeString(),
            'product_id'       => $model->product_id
        ];

        $record = ProductTransactionSummary::firstOrNew($identifiers);

        if(!$record->exists) {
            $record->{ $model->transaction->transaction->transaction_summary_field } = $model->qty;
            $record->{ $model->process_flow } = $model->qty;
            $record->save();
        } else {
            $record->getQuery()
                ->where($identifiers)
                ->update([
                    $model->transaction->transaction->transaction_summary_field => DB::raw($model->transaction->transaction->transaction_summary_field.' + '.$model->qty->innerValue()),
                    $model->process_flow => DB::raw($model->process_flow.' + '.$model->qty->innerValue()),
                ]);
        }
    }

    private function createProductMonthlyTransactionSummary($model)
    {
        $identifiers = [
            'created_for'      => $model->product_monthly_transaction_created_for,
            'for_location'     => $model->product_monthly_transaction_for_location,
            'transaction_date' => $model->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id'       => $model->product_monthly_transaction_product_id
        ];

        $fields = [];

        foreach ($model->getProductMonthlyTransactionAttributeMapping() as $summaryField => $itemField) {
            $fields[$summaryField] = $model->{ $itemField };
        }

        $summary = ProductMonthlyTransactions::take(1)
            ->where($identifiers)
            ->first();

        if (is_null($summary)) {
            $summary = new ProductMonthlyTransactions($identifiers);

            foreach ($fields as $attribute => $value) {
                $summary->{ $attribute } = $value;
            }

            $summary->save();
        } else {
            $columns = [];

            foreach ($fields as $attribute => $value) {
                $columns[$attribute] = DB::raw($attribute.' + '.$value);
            }

            $summary->where($identifiers)->update($columns);
        }
    }

    private function createStockCard($model)
    {
        $this->createStockCardForAutoConversion($model);
        $this->createStockCardForTransaction($model);
    }

    private function createStockCardForAutoConversion($model)
    {
        $identifiers = [
            'product_id'   => $model->product_id,
            'created_for'  => $model->created_for,
            'for_location' => $model->for_location
        ];

        $previousAttributes = $this->getPreviousStockCardAttribute($identifiers);

        $multiplier = Decimal::create(
            $model->process_flow === 'auto_product_conversion_increase'
                ? 1
                : -1,
            setting('monetary.precision')
        );

        $attributes = [
            'created_for'      => $model->created_for,
            'for_location'     => $model->for_location,
            'reference_type'   => $this->getTransactionTypeModel($model),
            'reference_id'     => $model->id,
            'transaction_date' => $model->transaction_date,
            'product_id'       => $model->product_id,
            'qty'              => $model->qty->mul($multiplier, setting('monetary.precision')),
            'price'            => $model->ma_cost,
            'cost'             => $model->ma_cost,
        ];

        $attributes['amount'] = $attributes['cost']->mul($attributes['qty'], setting('monetary.precision'));

        $postAttributes = $this->computeStockCardPostAttribute($model, $attributes, $previousAttributes);

        $attributes = array_merge($attributes, $previousAttributes, $postAttributes);

        $card = ProductStockCard::create($attributes);

        App::make(ProductBranchSummaryServiceInterface::class)->updateCurrentCostFromStockCard($card);
    }

    private function createStockCardForTransaction($model)
    {
        $identifiers = [
            'product_id'   => $model->product_id,
            'created_for'  => $model->created_for,
            'for_location' => $model->for_location
        ];

        $item = $model->transaction;
        $transaction = $item->transaction;

        $previousAttributes = $this->getPreviousStockCardAttribute($identifiers);

        $attributes = [
            'created_for'      => $transaction->created_for,
            'for_location'     => $transaction->for_location,
            'reference_type'   => $this->getTransactionTypeModel($transaction),
            'reference_id'     => $transaction->id,
            'transaction_date' => $transaction->transaction_date,
            'product_id'       => $item->product_id,
            'qty'              => $item->added_inventory_value->mul($transaction->multiplier, setting('monetary.precision')),
            'price'            => $item->price,
            'cost'             => $item->ma_cost,
        ];

        $attributes['amount'] = $attributes['cost']->mul($attributes['qty'], setting('monetary.precision'));

        $postAttributes = $this->computeStockCardPostAttribute($transaction, $attributes, $previousAttributes);

        $attributes = array_merge($attributes, $previousAttributes, $postAttributes);

        ProductStockCard::create($attributes);
    }

    private function getPreviousStockCardAttribute($identifiers)
    {
        $previous = ProductStockCard::where($identifiers)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();

        if(!$previous) {
            $zero = Decimal::create(0, setting('monetary.precision'));

            return [
                'previous_id'   => null,
                'sequence_no'   => 1,
                'pre_inventory' => $zero,
                'pre_avg_cost'  => $zero,
                'pre_amount'    => $zero
            ];
        }

        return [
            'previous_id'   => $previous->id,
            'sequence_no'   => $previous->sequence_no + 1,
            'pre_inventory' => $previous->post_inventory,
            'pre_avg_cost'  => $previous->post_avg_cost,
            'pre_amount'    => $previous->post_amount
        ];
    }

    private function computeStockCardPostAttribute($model, $attributes, $previous)
    {
        $postInventory = $previous['pre_inventory']->add($attributes['qty'], setting('monetary.precision'));
        $postAvgCost = $this->computeStockCardPostAvgCost($model, $postInventory, $attributes, $previous);
        $postAmount = is_null($previous['previous_id'])
            ? $attributes['amount']
            : $postInventory->mul($postAvgCost, setting('monetary.precision'));

        return [
            'post_inventory' => $postInventory,
            'post_avg_cost'  => $postAvgCost,
            'post_amount'    => $postAmount
        ];
    }

    private function computeStockCardPostAvgCost($model, $postInventory, $attributes, $previous)
    {
        $postAvgCost = null;
        $zero = Decimal::create(0, setting('monetary.precision'));

        $modules = [
            PurchaseInbound::class,
            StockDeliveryInbound::class,
            StockReturnInbound::class,
            InventoryAdjust::class,
            AutoProductConversion::class
        ];

        if($this->isEitherInstanceOf($model, $modules)) {
            if($postInventory->isZero()) {
                $postAvgCost = $zero;
            } else if($previous['pre_inventory']->isLessOrEqualTo($zero)) {
                $postAvgCost = $attributes['price'];
            } else {
                $postAvgCost = $attributes['amount']
                    ->add($previous['pre_amount'], setting('monetary.precision'))
                    ->div($postInventory, setting('monetary.precision'));
            }
        } else {
            $postAvgCost = $previous['pre_avg_cost'];
        }

        return $postAvgCost;
    }

    private function isEitherInstanceOf($class, $instances)
    {
        foreach ($instances as $instance) {
            if($class instanceof $instance) {
                return true;
            }
        }

        return false;
    }

    private function getTransactionTypeModel($model)
    {
        if($model instanceof PurchaseInbound) {
            return TransactionSummary::PURCHASE_INBOUND;
        } else if($model instanceof StockDeliveryInbound) {
            return TransactionSummary::STOCK_DELIVERY_INBOUND;
        } else if($model instanceof StockReturnInbound) {
            return TransactionSummary::STOCK_RETURN_INBOUND;
        } else if($model instanceof SalesReturnInbound) {
            return TransactionSummary::SALES_RETURN_INBOUND;
        } else if($model instanceof PurchaseReturnOutbound) {
            return TransactionSummary::PURCHASE_RETURN_OUTBOUND;
        } else if($model instanceof StockDeliveryOutbound) {
            return TransactionSummary::STOCK_DELIVERY_OUTBOUND;
        } else if($model instanceof StockReturnOutbound) {
            return TransactionSummary::STOCK_RETURN_OUTBOUND;
        } else if($model instanceof SalesOutbound) {
            return TransactionSummary::SALES_OUTBOUND;
        } else if($model instanceof DamageOutbound) {
            return TransactionSummary::DAMAGE_OUTBOUND;
        } else if($model instanceof InventoryAdjust) {
            return TransactionSummary::INVENTORY_ADJUST;
        } else if($model instanceof PosSales) {
            return TransactionSummary::POS_SALES;
        } else if($model instanceof ProductConversion) {
            return TransactionSummary::PRODUCT_CONVERSION;
        } else if($model instanceof AutoProductConversion) {
            return TransactionSummary::AUTO_PRODUCT_CONVERSION;
        }
    }
}
