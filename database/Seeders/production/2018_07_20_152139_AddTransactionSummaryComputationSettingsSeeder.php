<?php

use Modules\Core\Entities\GenericSetting;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddTransactionSummaryComputationSettingsSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        GenericSetting::create([
            'type' => 'int',
            'key' => 'transaction.summary.computation.mode',
            'value' => '1',
            'comments' => 'Summary Data Computation (1 - Automatic, 2 - Manual)'
        ]);

        GenericSetting::create([
            'type' => 'date',
            'key' => 'transaction.summary.computation.last.post.date',
            'value' => '2000-01-01',
            'comments' => 'Manual Computation\'s last post date check'
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        GenericSetting::whereIn('key', [
            'transaction.summary.computation.mode',
            'transaction.summary.computation.last.post.date'
        ])->delete();
    }
}
