<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductTransactionListPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Product Transaction List',
            'alias' => 'product_transaction_list',
            'translation' => 'label.product.transaction.list.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Product Transaction List',
            'alias' => 'view_product_transaction_list',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Export Product Transaction List',
            'alias' => 'export_product_transaction_list',
            'translation' => 'label.export.excel',
            'sequence' => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'export_product_transaction_list')->delete();
        Permission::where('alias', 'view_product_transaction_list')->delete();
        PermissionModule::where('alias', 'product_transaction_list')->delete();
    }
}