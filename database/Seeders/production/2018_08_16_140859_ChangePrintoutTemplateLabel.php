<?php

use Modules\Core\Entities\PrintoutTemplate;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ChangePrintoutTemplateLabel extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $modules = [
            'Purchase Inbound',
            'Stock Request'
        ];

        foreach ($modules as $module) {
            $template = PrintoutTemplate::where('name', $module)->first();

            $format = json_decode(json_encode($template->format), true);

            $format['header']['labels']['static_text_1']['label'] = $module;

            $template->format = $format;

            $template->save();
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $modules = [
            'Purchase Inbound' => 'Purchase Return',
            'Stock Request' => 'Sales Return Inbound'
        ];

        foreach ($modules as $module => $old) {
            $template = PrintoutTemplate::where('name', $module)->first();

            $format = json_decode(json_encode($template->format), true);

            $format['header']['labels']['static_text_1']['label'] = $old;

            $template->format = $format;

            $template->save();
        }
    }
}
