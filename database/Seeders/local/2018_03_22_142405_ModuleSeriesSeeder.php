<?php

use Modules\Core\Entities\ModuleSeries;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ModuleSeriesSeeder extends SmartSeeder
{
    protected function execute()
    {
        ModuleSeries::create([
            'type' => 'product',
            'column' => 'stock_no',
            'series' => '100001'
        ]);

        ModuleSeries::create([
            'type' => 'product_barcode',
            'column' => 'code',
            'series' => date('y').'0000000001'
        ]);

        ModuleSeries::create([
            'type' => 'brand',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'income_type',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'payment_type',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'UOM',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'bank',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'branch',
            'column' => 'code',
            'series' => '2'
        ]);

        ModuleSeries::create([
            'type' => 'branch_detail',
            'column' => 'code',
            'series' => '2'
        ]);

        ModuleSeries::create([
            'type' => 'category',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'supplier',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'customer',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'member',
            'column' => 'card_id',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'user',
            'column' => 'code',
            'series' => '2'
        ]);

        ModuleSeries::create([
            'type' => 'role',
            'column' => 'code',
            'series' => '1'
        ]);

        ModuleSeries::create([
            'type' => 'reason',
            'column' => 'code',
            'series' => '1'
        ]);
    }

    protected function back()
    {
        ModuleSeries::getQuery()->delete();
    }
}
