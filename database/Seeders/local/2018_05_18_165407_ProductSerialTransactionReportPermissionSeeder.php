<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductSerialTransactionReportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Product Serial Transaction Report',
            'alias' => 'product_serial_transaction_report',
            'translation' => 'label.product.serial.transaction.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Product Serial Transaction Report',
            'alias' => 'view_product_serial_transaction_report',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_product_serial_transaction_report')->delete();
        PermissionModule::where('alias', 'product_serial_transaction_report')->delete();
    }
}
