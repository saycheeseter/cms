<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;
use Litipk\BigNumbers\Decimal;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;

class UpdateInventoryChainTransactionTotalAmount extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $modules = [
            PurchaseInbound::class,
            PurchaseReturnOutbound::class,
            DamageOutbound::class,
            SalesOutbound::class,
            SalesReturnInbound::class,
            StockDeliveryOutbound::class,
            StockDeliveryInbound::class,
            StockReturnOutbound::class,
            StockReturnInbound::class
        ];

        foreach ($modules as $module) {
            $transactions = App::make($module)
                ->with(['details'])
                ->withTrashed()
                ->cursor();

            foreach ($transactions as $transaction) {
                $amount = Decimal::create(0, setting('monetary.precision'));

                foreach ($transaction->details as $detail) {
                    $amount = $amount->add($detail->amount, setting('monetary.precision'));
                }

                $transaction->total_amount = $amount;

                $transaction->save();
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
