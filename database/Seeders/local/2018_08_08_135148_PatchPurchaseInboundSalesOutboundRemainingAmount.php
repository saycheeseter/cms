<?php

use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class PatchPurchaseInboundSalesOutboundRemainingAmount extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $modules = [
            PurchaseInbound::class => 'payments',
            SalesOutbound::class => 'collections'
        ];

        foreach ($modules as $module => $relationship) {
            $transactions = App::make($module)
                ->with([
                    $relationship => function($query) {
                        $query->where('approval_status', ApprovalStatus::APPROVED);
                    }
                ])
                ->cursor();

            foreach ($transactions as $transaction) {
                if ($transaction->$relationship->count() > 0) {
                    continue;
                }

                $transaction->remaining_amount = $transaction->total_amount;
                $transaction->save();
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
