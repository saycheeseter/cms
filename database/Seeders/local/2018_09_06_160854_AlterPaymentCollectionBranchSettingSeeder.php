<?php

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchSetting;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AlterPaymentCollectionBranchSettingSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $branches = Branch::all();

        foreach ($branches as $branch) {
            DB::table('branch_settings')
                ->where('key', 'payment.display.purchase.inbound.of.other.branches')
                ->where('branch_id', $branch->id)
                ->update([
                    'key' => 'payment.display.transactions.of.other.branches',
                    'comments' => 'Display transactions of other branches'
                ]);

            DB::table('branch_settings')
                ->where('key', 'collection.display.sales.outbound.of.other.branches')
                ->where('branch_id', $branch->id)
                ->update([
                    'key' => 'collection.display.transactions.of.other.branches',
                    'comments' => 'Display transactions of other branches'
                ]);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        $branches = Branch::all();

        foreach ($branches as $branch) {
            DB::table('branch_settings')
                ->where('key', 'payment.display.transactions.of.other.branches')
                ->where('branch_id', $branch->id)
                ->update([
                    'key' => 'payment.display.purchase.inbound.of.other.branches',
                    'comments' => 'Display purchase inbound of other branches'
                ]);

            DB::table('branch_settings')
                ->where('key', 'collection.display.transactions.of.other.branches')
                ->where('branch_id', $branch->id)
                ->update([
                    'key' => 'collection.display.sales.outbound.of.other.branches',
                    'comments' => 'Display sales outbound of other branches'
                ]);
        }
    }
}
