<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SeniorTransactionPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)->get()->count();

        $module = Permissionmodule::create([
            'section_id' => $report->id,
            'name' => 'Senior Transaction',
            'alias' => 'senior_transaction',
            'translation' => 'label.senior.transaction',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Senior Transaction',
            'alias' => 'view_senior_transaction',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_senior_transaction')->delete();
        PermissionModule::where('alias', 'senior_transaction')->delete();
    }
}
