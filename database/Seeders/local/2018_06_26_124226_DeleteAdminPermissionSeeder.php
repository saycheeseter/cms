<?php

use Modules\Core\Entities\Admin;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\Permission;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class DeleteAdminPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $admin = Admin::where('code', '1')
            ->where('username', 'admin')
            ->first();

        DB::table('branch_permission_user')->where('user_id', $admin->id)->delete();
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $admin = Admin::where('code', '1')
            ->where('username', 'admin')
            ->first();

        $permissions = Permission::all();

        $branches = Branch::all();

        foreach ($branches as $branch) {
            $collection = [];

            foreach ($permissions as $permission) {
                $collection[] = [
                    'user_id' => $admin->id,
                    'branch_id' => $branch->id,
                    'permission_id' => $permission->id
                ];
            }

            DB::table('branch_permission_user')->insert($collection);
        }
    }
}
