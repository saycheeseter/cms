<?php

use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class UpdateSalesOutboundPurchaseInboundZeroTermDueDate extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $modules = [
            PurchaseInbound::class,
            SalesOutbound::class,
        ];

        foreach ($modules as $module) {
            $transactions = App::make($module)
                ->where('term', 0)
                ->cursor();

            foreach ($transactions as $transaction) {
                $transaction->due_date = $transaction->transaction_date;
                $transaction->save();
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $modules = [
            PurchaseInbound::class,
            SalesOutbound::class,
        ];

        foreach ($modules as $module) {
            $transactions = App::make($module)
                ->where('term', 0)
                ->cursor();

            foreach ($transactions as $transaction) {
                $transaction->due_date = null;
                $transaction->save();
            }
        }
    }
}
