<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class BankTransactionModulePermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $report = PermissionSection::where('alias', 'others')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Bank Transaction',
            'alias' => 'bank_transaction',
            'translation' => 'label.bank.transaction',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Bank Transaction',
            'alias' => 'view_bank_transaction',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Create Bank Transaction',
            'alias' => 'create_bank_transaction',
            'translation' => 'label.create',
            'sequence' => 2
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Update Bank Transaction',
            'alias' => 'update_bank_transaction',
            'translation' => 'label.update',
            'sequence' => 3
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Delete Bank Transaction',
            'alias' => 'delete_bank_transaction',
            'translation' => 'label.delete',
            'sequence' => 4
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        Permission::where('alias', 'delete_bank_transaction')->delete();
        Permission::where('alias', 'update_bank_transaction')->delete();
        Permission::where('alias', 'create_bank_transaction')->delete();
        Permission::where('alias', 'view_bank_transaction')->delete();
        PermissionModule::where('alias', 'bank_transaction')->delete();
    }
}
