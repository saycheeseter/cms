<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddMissingOtherIncomeViewDetailPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $permission = Permission::where('alias', 'create_other_income')->first();
        $permission->sequence = 3;
        $permission->save();

        $permission = Permission::where('alias', 'update_other_income')->first();
        $permission->sequence = 4;
        $permission->save();

        $permission = Permission::where('alias', 'delete_other_income')->first();
        $permission->sequence = 5;
        $permission->save();

        Permission::create([
            'alias'       => 'view_other_income_detail',
            'module_id'   => $permission->module_id,
            'name'        => 'View Other Income Detail',
            'translation' => 'label.view.detail',
            'sequence'    => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_other_income_detail')->delete();

        $permission = Permission::where('alias', 'create_other_income')->first();
        $permission->sequence = 2;
        $permission->save();

        $permission = Permission::where('alias', 'update_other_income')->first();
        $permission->sequence = 3;
        $permission->save();

        $permission = Permission::where('alias', 'delete_other_income')->first();
        $permission->sequence = 4;
        $permission->save();
    }
}
