<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Modules\Core\Entities\AutoProductConversion;
use Modules\Core\Entities\BranchPrice;
use Modules\Core\Entities\Damage;
use Modules\Core\Entities\DamageOutbound;
use Modules\Core\Entities\InventoryAdjust;
use Modules\Core\Entities\InventoryChain;
use Modules\Core\Entities\Invoice;
use Modules\Core\Entities\ProductConversion;
use Modules\Core\Entities\ProductMonthlyTransactions;
use Modules\Core\Entities\Purchase;
use Modules\Core\Entities\PurchaseInbound;
use Modules\Core\Entities\PurchaseReturn;
use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Entities\Sales;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturn;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Entities\StockDelivery;
use Modules\Core\Entities\StockDeliveryInbound;
use Modules\Core\Entities\StockDeliveryOutbound;
use Modules\Core\Entities\StockRequest;
use Modules\Core\Entities\StockReturn;
use Modules\Core\Entities\StockReturnInbound;
use Modules\Core\Entities\StockReturnOutbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Enums\GroupType;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddCostAndComputePMTOnTransactions extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $modules = [
            Purchase::class,
            PurchaseReturn::class,
            Damage::class,
            Sales::class,
            SalesReturn::class,
            StockRequest::class,
            StockDelivery::class,
            StockReturn::class,
            PurchaseInbound::class,
            PurchaseReturnOutbound::class,
            DamageOutbound::class,
            SalesOutbound::class,
            SalesReturnInbound::class,
            StockDeliveryOutbound::class,
            StockDeliveryInbound::class,
            StockReturnOutbound::class,
            StockReturnInbound::class,
            ProductConversion::class,
            AutoProductConversion::class,
            InventoryAdjust::class
        ];

        foreach ($modules as $module) {
            $transactions = App::make($module)->cursor();

            foreach ($transactions as $transaction) {
                $transaction->load(['details']);

                foreach ($transaction->details as $item) {
                    $item = $this->updateItemCost($item);

                    $this->computePMTTotalCost($item);
                }

                // Compute cost for head transactions
                if ($transaction instanceof ProductConversion
                    || $transaction instanceof AutoProductConversion
                ) {
                    $transaction->cost = $this->getPurchasePrice($transaction->created_for, $transaction->product_id);
                    $transaction->save();

                    if ($transaction instanceof ProductConversion) {
                        $this->computeConversionPMTTotalCost($transaction);
                    }
                }
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        $modules = [
            Purchase::class,
            PurchaseReturn::class,
            Damage::class,
            Sales::class,
            SalesReturn::class,
            StockRequest::class,
            StockDelivery::class,
            StockReturn::class,
            PurchaseInbound::class,
            PurchaseReturnOutbound::class,
            DamageOutbound::class,
            SalesOutbound::class,
            SalesReturnInbound::class,
            StockDeliveryOutbound::class,
            StockDeliveryInbound::class,
            StockReturnOutbound::class,
            StockReturnInbound::class,
            ProductConversion::class,
            AutoProductConversion::class,
            InventoryAdjust::class
        ];

        foreach ($modules as $module) {
            $transactions = App::make($module)->cursor();

            foreach ($transactions as $transaction) {
                $transaction->load(['details']);

                foreach ($transaction->details as $item) {
                    $item->cost = 0;
                    $item->save();
                }

                // Compute cost for head transactions
                if ($transaction instanceof ProductConversion
                    || $transaction instanceof AutoProductConversion
                ) {
                    $transaction->cost = 0;
                    $transaction->save();
                }
            }
        }

        $columns = Schema::getColumnListing((new ProductMonthlyTransactions)->getTable());
        $fields = [];

        foreach ($columns as $column) {
            if (Str::contains($column, 'total_cost')) {
                $fields[$column] = 0;
            }
        }

        ProductMonthlyTransactions::getQuery()->update($fields);
    }

    private function updateTotalCostOnPMT($identifiers, $fields)
    {
        $summary = ProductMonthlyTransactions::take(1)
            ->where($identifiers)
            ->first();

        if (is_null($summary)) {
            $summary = new ProductMonthlyTransactions($identifiers);

            foreach ($fields as $attribute => $value) {
                $summary->{ $attribute } = $value;
            }

            $summary->save();
        } else {
            $columns = [];

            foreach ($fields as $attribute => $value) {
                $columns[$attribute] = DB::raw($attribute.'+'.$value);
            }

            $summary->where($identifiers)->update($columns);
        }
    }

    private function getPurchasePrice($branch, $product)
    {
        return BranchPrice::where('branch_id', $branch)
            ->where('product_id', $product)
            ->first()
            ->purchase_price ?? 0;
    }

    private function updateItemCost($item)
    {
        $product = $item->transaction instanceof ProductConversion
            ? $item->component_id
            : $item->product_id;

        $item->cost = $this->getPurchasePrice($item->transaction->created_for, $product);
        $item->save();

        return $item;
    }

    private function computePMTTotalCost($item)
    {
        $transaction = $item->transaction;

        if (is_subclass_of($transaction, Invoice::class)
            || (is_subclass_of($transaction, InventoryChain::class) && $transaction->approval_status !== ApprovalStatus::APPROVED)
            || ($transaction instanceof ProductConversion && $transaction->approval_status !== ApprovalStatus::APPROVED)
            || ($transaction instanceof InventoryAdjust && in_array($item->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP]))
            || ($transaction instanceof ProductConversion && !in_array($transaction->group_type, [GroupType::MANUAL_GROUP, GroupType::MANUAL_UNGROUP]))
            || ($transaction instanceof AutoProductConversion && !in_array($transaction->group_type, [GroupType::AUTOMATIC_GROUP, GroupType::AUTOMATIC_UNGROUP]))
        ) {
            return $this;
        }

        $identifiers = [
            'created_for' => $transaction->product_monthly_transaction_created_for,
            'for_location' => $transaction->product_monthly_transaction_for_location,
            'transaction_date' => $transaction->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $item->product_id ?? 0
        ];

        $fields = [];

        if (is_subclass_of($transaction, InventoryChain::class)) {
            $prefix = $transaction->getProductMonthlyTransactionAttributeMappingPrefix();

            $fields[$prefix.'_total_cost'] = $item->total_cost_amount;
        } else if ($transaction instanceof InventoryAdjust) {
            $prefix = $item->process_flow;

            $fields[$prefix.'_total_cost'] = $item->total_cost_amount;
        } else if ($transaction instanceof ProductConversion) {
            $prefix = $item->process_flow;

            $fields[$prefix.'_total_cost'] = $item->total_cost;
            $identifiers['product_id'] = $item->component_id;
        } else if ($transaction instanceof AutoProductConversion) {
            $prefix = $transaction->process_flow;

            $fields[$prefix.'_total_cost'] = $item->total_cost;
        }

        $this->updateTotalCostOnPMT($identifiers, $fields);

        return $this;
    }

    private function computeConversionPMTTotalCost($transaction)
    {
        $identifiers = [
            'created_for' => $transaction->product_monthly_transaction_created_for,
            'for_location' => $transaction->product_monthly_transaction_for_location,
            'transaction_date' => $transaction->product_monthly_transaction_transaction_date->startOfMonth()->toDateString(),
            'product_id' => $transaction->product_id
        ];

        $prefix = $transaction->process_flow;

        $fields[$prefix.'_total_cost'] = $transaction->total_cost;

        $this->updateTotalCostOnPMT($identifiers, $fields);
    }
}
