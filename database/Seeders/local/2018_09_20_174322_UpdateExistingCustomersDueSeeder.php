<?php

use Modules\Core\Entities\Customer;
use Modules\Core\Entities\CustomerCredit;
use Modules\Core\Entities\SalesOutbound;
use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Enums\ApprovalStatus;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class UpdateExistingCustomersDueSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        ini_set('max_execution_time', 0);

        $customers = Customer::all();

        $modules = [
            SalesOutbound::class => 'add',
            SalesReturnInbound::class => 'sub'
        ];

        foreach ($customers as $customer) {
            $credit = new CustomerCredit();

            foreach ($modules as $module => $method) {
                $transactions = $module::where('approval_status', ApprovalStatus::APPROVED)
                    ->where('customer_id', $customer->id)
                    ->where('remaining_amount', '>', 0)
                    ->cursor();

                foreach ($transactions as $transaction) {
                    $credit->due = $credit->due->$method($transaction->remaining_amount);
                }
            }

            $customer->credit()->save($credit);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        CustomerCredit::getQuery()->delete();
    }
}
