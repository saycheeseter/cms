<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductImportExcelPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'product')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Import Excel Product',
            'alias' => 'import_excel_product',
            'translation' => 'label.import.excel',
            'sequence' => $this->getNextSequenceNumberForModule($module)
        ]);
    }

    protected function getNextSequenceNumberForModule(PermissionModule $module)
    {
        return PermissionModule::where('id', $module->id)
            ->get()
            ->count() + 1;
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'import_excel_product')->delete();
    }
}
