<?php

use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class CollectionReferenceReferenceTypeSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        DB::table('collection_reference')
            ->update([
                'reference_type' => 'sales_outbound'
            ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        DB::table('collection_reference')
            ->where('reference_type', 'sales_outbound')
            ->update([
                'reference_type' => 'NULL'
            ]);
    }
}
