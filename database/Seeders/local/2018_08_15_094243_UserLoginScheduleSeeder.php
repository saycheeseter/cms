<?php

use Modules\Core\Enums\UserType;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class UserLoginScheduleSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $default = [
            'mon' => 1,
            'tue' => 1,
            'wed' => 1,
            'thu' => 1,
            'fri' => 1,
            'sat' => 1,
            'sun' => 1,
            'start' => '00:00:00',
            'end' => '23:59:59'
        ];

        $users = DB::table('user')
            ->whereNotIn('type', [UserType::ADMIN, UserType::SUPERADMIN])
            ->whereNull('deleted_at')
            ->get();
        
        foreach ($users as $index => $user) {
            $default['user_id'] = $user->id;

            DB::table('user_login_schedule')->insert($default);
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        DB::table('user_login_schedule')->delete();
    }
}
