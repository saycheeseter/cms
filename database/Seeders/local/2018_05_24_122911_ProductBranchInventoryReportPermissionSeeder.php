<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class ProductBranchInventoryReportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Product Branch Inventory',
            'alias' => 'product_branch_inventory',
            'translation' => 'label.product.branch.inventory.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Product Branch Inventory',
            'alias' => 'view_product_branch_inventory',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Export Product Branch Inventory',
            'alias' => 'export_product_branch_inventory',
            'translation' => 'label.export.excel',
            'sequence' => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'export_product_branch_inventory')->delete();
        Permission::where('alias', 'view_product_branch_inventory')->delete();
        PermissionModule::where('alias', 'product_branch_inventory')->delete();
    }
}
