<?php

use Modules\Core\Entities\SystemCodeType;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SystemCodeTypeSeeder extends SmartSeeder
{
    protected function execute()
    {
        SystemCodeType::create([
            'name' => 'Brand',
        ]);

        SystemCodeType::create([
            'name' => 'UOM',
        ]);

        SystemCodeType::create([
            'name' => 'Bank',
        ]);

        SystemCodeType::create([
            'name' => 'Payment Method',
        ]);

        SystemCodeType::create([
            'name' => 'Reason',
        ]);

        SystemCodeType::create([
            'name' => 'Payment Type',
        ]);

        SystemCodeType::create([
            'name' => 'Income Type',
        ]);
    }

    protected function back()
    {
        SystemCodeType::getQuery()->delete();
    }
}
