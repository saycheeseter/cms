<?php

use Modules\Core\Entities\SalesReturnInbound;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SalesReturnInboundRemainingAmountSeeder extends SmartSeeder
{
    public function execute()
    {
        $table = SalesReturnInbound::cursor();

        foreach ($table as $row) {
            DB::table('sales_return_inbound')
                ->where('id', $row->id)
                ->update(['remaining_amount' => $row->total_amount]);
        }
    }

    public function back()
    {
        DB::table('sales_return_inbound')
            ->update([
                'remaining_amount' => '0'
            ]);
    }
}
