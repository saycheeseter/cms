<?php

use Modules\Core\Entities\Admin;
use Modules\Core\Entities\Branch;
use Modules\Core\Entities\Permission;
use Modules\Core\Entities\Superadmin;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class SuperadminAdminSeeder extends SmartSeeder
{
    protected function execute()
    {
        Superadmin::create([
            'code' => '0',
            'username' => 'superadmin',
            'password' => '121586',
            'full_name' => 'superadmin',
            'status' => 1
        ]);

        $admin = Admin::create([
            'code' => '1',
            'username' => 'admin',
            'password' => 'admin123',
            'full_name' => 'admin',
            'status' => 1
        ]);

        $permissions = Permission::all();
        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        $collection = [];

        foreach ($permissions as $permission) {
            $collection[] = [
                'user_id' => $admin->id,
                'branch_id' => $branch->id,
                'permission_id' => $permission->id
            ];
        }

        DB::table('branch_permission_user')->insert($collection);
    }

    protected function back()
    {
        $superadmin = Superadmin::where('code', '0')
            ->where('username', 'superadmin')
            ->where('full_name', 'superadmin')
            ->first();

        $admin = Admin::where('code', '1')
            ->where('username', 'admin')
            ->where('full_name', 'admin')
            ->first();

        $branch = Branch::where('code', 1)
            ->where('name', 'Main')
            ->first();

        DB::table('branch_permission_user')
            ->where('user_id', $admin->id)
            ->where('branch_id', $branch->id)
            ->delete();

        $admin->forceDelete();
        $superadmin->forceDelete();
    }
}
