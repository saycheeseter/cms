<?php

use Modules\Core\Entities\Tax;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class TaxSeeder extends SmartSeeder
{
    protected function execute()
    {
        Tax::create([
            'name' => 'VAT',
            'percentage' => 12,
            'is_inclusive' => 1
        ]);
    }

    protected function back()
    {
        Tax::where('name', 'VAT')->delete();
    }
}
