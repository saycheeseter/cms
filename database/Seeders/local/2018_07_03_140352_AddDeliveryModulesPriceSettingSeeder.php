<?php

use Modules\Core\Entities\GenericSetting;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddDeliveryModulesPriceSettingSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        GenericSetting::create([
            'type' => 'int',
            'key' => 'delivery.module.price.basis',
            'value' => '1',
            'comments' => 'Price basis ( 1 - Purchase Price, 2 - Wholesale Price, 3 - Retail/Selling Price)'
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        GenericSetting::where('key', 'delivery.module.price.basis')->delete();
    }
}
