<?php

use Modules\Core\Entities\Branch;
use Modules\Core\Entities\BranchModuleSeries;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AddMissingDataCollectorSeriesOnNewBranches extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $branches = Branch::get();

        foreach ($branches as $branch) {
            $reference = BranchModuleSeries::where('branch_id', $branch->id)
                ->where('type', 'data_collector_transaction')
                ->where('column', 'reference_no')
                ->first();

            if (!$reference) {
                BranchModuleSeries::create([
                    'branch_id' => $branch->id,
                    'type'      => 'data_collector_transaction',
                    'column'    => 'reference_no',
                    'series'    => sprintf("1%s0000001", str_pad($branch->id,3, '0', STR_PAD_LEFT)),
                    'limit'     => sprintf("1%s9999999", str_pad($branch->id,3, '0', STR_PAD_LEFT)),
                ]);
            } else {
                $reference->limit = sprintf("1%s9999999", str_pad($branch->id,3, '0', STR_PAD_LEFT));
                $reference->save();
            }
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
    }
}
