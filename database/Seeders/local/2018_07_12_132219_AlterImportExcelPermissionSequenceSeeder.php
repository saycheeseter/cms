<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class AlterImportExcelPermissionSequenceSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $aliases = [
            'import_excel_product',
            'import_excel_member',
            'import_excel_brand',
            'import_excel_uom',
            'import_excel_bank',
            'import_excel_payment_method',
            'import_excel_reason',
            'import_excel_payment_type',
            'import_excel_income_type',
        ];

        foreach ($aliases as $alias) {
            $permission = Permission::where('alias', $alias)->first();

            $count = Permission::where('alias', '<>', $alias)
                ->where('module_id', $permission->module_id)
                ->get()
                ->count() + 1;

            $permission->sequence = $count;

            $permission->save();
        }
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        $aliases = [
            'import_excel_product' => 2,
            'import_excel_member'  => 1,
            'import_excel_brand'  => 1,
            'import_excel_uom'  => 1,
            'import_excel_bank'  => 1,
            'import_excel_payment_method'  => 1,
            'import_excel_reason'  => 1,
            'import_excel_payment_type'  => 1,
            'import_excel_income_type'  => 1,
        ];

        foreach ($aliases as $alias => $sequence) {
            $permission = Permission::where('alias', $alias)->first();

            $permission->sequence = $sequence;

            $permission->save();
        }
    }
}
