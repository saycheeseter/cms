<?php

use Modules\Core\Entities\PurchaseReturnOutbound;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class PurchaseReturnOutboundRemainingAmountSeeder extends SmartSeeder
{
    public function execute()
    {
        $table = PurchaseReturnOutbound::cursor();

        foreach ($table as $row) {
            DB::table('purchase_return_outbound')
                ->where('id', $row->id)
                ->update(['remaining_amount' => $row->total_amount]);
        }
    }

    public function back()
    {
        DB::table('purchase_return_outbound')
            ->update([
                'remaining_amount' => '0'
            ]);
    }
}
