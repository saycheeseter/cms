<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class DetailSalesReportPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        // Seed your data
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();
        
        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Detail Sales Report',
            'alias' => 'detail_sales_report',
            'translation' => 'label.detail.sales.report',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Detail Sales Report',
            'alias' => 'view_detail_sales_report',
            'translation' => 'label.view',
            'sequence' => 1
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Export Detail Sales Report',
            'alias' => 'export_detail_sales_report',
            'translation' => 'label.export.excel',
            'sequence' => 2
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        // Remove your data
        Permission::where('alias', 'export_detail_sales_report')->delete();
        Permission::where('alias', 'view_detail_sales_report')->delete();
        PermissionModule::where('alias', 'detail_sales_report')->delete();
    }
}
