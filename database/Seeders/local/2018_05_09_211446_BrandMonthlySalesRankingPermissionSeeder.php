<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Entities\PermissionSection;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class BrandMonthlySalesRankingPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $report = PermissionSection::where('alias', 'reports')->first();

        $count = PermissionModule::where('section_id', $report->id)
            ->get()
            ->count();

        $module = PermissionModule::create([
            'section_id' => $report->id,
            'name' => 'Brand Monthly Sales Ranking',
            'alias' => 'brand_monthly_sales_ranking',
            'translation' => 'label.brand.monthly.sales.ranking',
            'sequence_number' => ($count + 1)
        ]);

        Permission::create([
            'module_id' => $module->id,
            'name' => 'View Brand Monthly Sales Ranking',
            'alias' => 'view_brand_monthly_sales_ranking',
            'translation' => 'label.view',
            'sequence' => 1
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'view_brand_monthly_sales_ranking')->delete();
        PermissionModule::where('alias', 'brand_monthly_sales_ranking')->delete();
    }
}
