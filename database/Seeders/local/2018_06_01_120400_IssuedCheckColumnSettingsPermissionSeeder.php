<?php

use Modules\Core\Entities\Permission;
use Modules\Core\Entities\PermissionModule;
use Modules\Core\Extensions\SmartSeeder\SmartSeeder;

class IssuedCheckColumnSettingsPermissionSeeder extends SmartSeeder
{
    /**
     * Run the database seeder.
     */
    public function execute()
    {
        $module = PermissionModule::where('alias', 'issued_check')->first();

        Permission::create([
            'module_id' => $module->id,
            'name' => 'Column Settings Issued Check',
            'alias' => 'column_settings_issued_check',
            'translation' => 'label.column.settings',
            'sequence' => 3
        ]);
    }

    /**
     * Reverts the database seeder.
     */
    public function back()
    {
        Permission::where('alias', 'column_settings_issued_check')->delete();
    }
}
