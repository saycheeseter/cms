<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Archive table clean
    |--------------------------------------------------------------------------
    |
    | This option will be used upon triggering the archive command.
    | The command will check this array to see if there are any tables that will
    | be cleaned after the archive process.
    |
    */

    'clean' => [
        'payment_reference',
        'payment_detail',
        'payment',
        'purchase_inbound_detail',
        'purchase_inbound',
        'purchase_detail',
        'purchase',
        'purchase_return_outbound_detail',
        'purchase_return_outbound',
        'purchase_return_detail',
        'purchase_return',
        'damage_outbound_detail',
        'damage_outbound',
        'damage_detail',
        'damage',
        'collection_reference',
        'collection_detail',
        'collection',
        'counter_detail',
        'counter',
        'sales_outbound_detail',
        'sales_outbound',
        'sales_detail',
        'sales',
        'sales_return_inbound_detail',
        'sales_return_inbound',
        'sales_return_detail',
        'sales_return',
        'stock_delivery_inbound_detail',
        'stock_delivery_inbound',
        'stock_delivery_outbound_detail',
        'stock_delivery_outbound',
        'stock_delivery_detail',
        'stock_delivery',
        'stock_request_detail',
        'stock_request',
        'stock_return_inbound_detail',
        'stock_return_inbound',
        'stock_return_outbound_detail',
        'stock_return_outbound',
        'stock_return_detail',
        'stock_return',
        'inventory_adjust_detail',
        'inventory_adjust',
    ],

    /*
    |--------------------------------------------------------------------------
    | Archive table ignore
    |--------------------------------------------------------------------------
    |
    | This option will be used upon triggering the archive command.
    | The command will check this array to see if there are any tables to be
    | ignore upon transferring the data the source to the archive table
    |
    */
    'ignore' => [
        'migrations'
    ],

    'batch' => 1000,

    'database_prefix' => 'archive',
];