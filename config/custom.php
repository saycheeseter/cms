<?php

return [

    /*
    |--------------------------------------------------------------------------
    | URL
    |--------------------------------------------------------------------------
    |
    | This value returns URLs for modules outside the app.
    */

    'url' => [
        'helpDesk' => 'http://nelsoftoffice.dyndns.org',
    ],
];
