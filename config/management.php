<?php
//replace this to correct values
return [
    'api' => 'https://api.cirms.ph/api/v1/',
    'token_server' => 'https://api.cirms.ph/oauth/token',
    'client_id' => env('CLIENT_ID'),
    'oauth_client_id' => env('OAUTH_CLIENT_ID'),
    'oauth_client_secret' => env('OAUTH_CLIENT_SECRET'),
];